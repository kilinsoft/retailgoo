USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesTaxTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSalesTaxTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesTaxTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		SalesTaxTypeId AS Value, 
		SalesTaxTypeName As Text 
	from 
		SalesTaxType

SET @ReturnValue = 0;
RETURN @ReturnValue;