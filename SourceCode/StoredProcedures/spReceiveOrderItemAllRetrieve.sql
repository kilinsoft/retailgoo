USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveOrderItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveOrderItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveOrderItemAllRetrieve]
	@ReceiveOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT 
		roi.*,
		CASE 
			WHEN roi.PurchaseOrderItemId IS NULL THEN N''
			ELSE po.PurchaseOrderNo
		END AS PurchaseOrderNo,
		(roi.Quantity * roi.Price) AS Amount, 
		dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName,
		CASE
			WHEN (roi.PurchaseOrderItemId IS NULL) OR (roi.PurchaseOrderItemId = -1) THEN 0
			ELSE poi.Quantity
		END AS QuantityOrdered,
		ig.Uom
	FROM [dbo].[ReceiveOrderItem] roi
		INNER JOIN [dbo].[Item] i
			ON roi.ItemId = i.ItemId
		INNER JOIN [dbo].[ItemGroup] ig
			ON i.ItemGroupId = ig.ItemGroupId
		LEFT JOIN [dbo].[PurchaseOrderItem] poi
			ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
		LEFT JOIN [dbo].[PurchaseOrder] po
			ON poi.PurchaseOrderId = po.PurchaseOrderId
	WHERE	roi.ReceiveOrderId = @ReceiveOrderId
		AND roi.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;