USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveItemTransferAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveItemTransferAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveItemTransferAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @sqlCommand AS NVARCHAR(MAX);
	DECLARE @filterSql AS NVARCHAR(250);
	DECLARE @sortingSql AS NVARCHAR(250);
	DECLARE @OutputColumnSQL AS NVARCHAR(250);

	SELECT 
		it.TransferId,
		it.TransferName,
		it.DateSent
		--it.Status
	FROM [ItemTransfer] it
	WHERE	it.TargetBusinessId = @BusinessId
		AND it.IsDisabled <> 1
	ORDER BY it.TransferId DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;