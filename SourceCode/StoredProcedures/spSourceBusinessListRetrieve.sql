USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSourceBusinessListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSourceBusinessListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSourceBusinessListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1
		other.BusinessId AS Value,
		b.BusinessName AS Text
	FROM 
	(
		SELECT
			br.BusinessId,
			br.RootBusinessId
		FROM [BusinessRelation] br
		WHERE br.BusinessId = @BusinessId -- retrieve the business, itself
	) other
	INNER JOIN [Business] b
		ON	other.BusinessId = b.BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;