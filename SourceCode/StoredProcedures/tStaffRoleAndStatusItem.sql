USE [retailgoo]
GO
IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tStaffRoleAndStatusItem')
   EXEC sp_droptype tStaffRoleAndStatusItem
GO
CREATE TYPE dbo.tStaffRoleAndStatusItem AS TABLE (
	[StaffId] [bigint] NOT NULL,
	[Role] [varchar](10) NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
