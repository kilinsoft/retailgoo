USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemAllRetrieve]
	@ItemGroupId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select i.*
	from Item i
	where
			i.ItemGroupId = @ItemGroupId
		and	i.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;