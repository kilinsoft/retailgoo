USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spRolesManage')
   EXEC('CREATE PROCEDURE [dbo].[spRolesManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spRolesManage]
	@BusinessId bigint,
	@RolesColumnName nvarchar(MAX),
	@RolesParameter nvarchar(MAX),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1)
AS 

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'M'
		BEGIN
			declare @sqlStr nvarchar (MAX);
			set @sqlStr = 'UPDATE [dbo].[ScreenPermission] set ' + CAST(@RolesColumnName AS NVARCHAR) + ' = ''' + CAST(@RolesParameter AS NVARCHAR(MAX)) + ''',';
			set @sqlStr = @sqlStr + ' [DateModified] = ''' + CAST(@dateNow AS NVARCHAR) + ''',';
			set @sqlStr = @sqlStr + ' [ModifiedBy] = ''' + CAST(@username AS NVARCHAR) + '''';
			set @sqlStr = @sqlStr + ' WHERE [BusinessId] = ' + CAST(@BusinessId AS NVARCHAR);

			exec sp_executesql @sqlStr;
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

