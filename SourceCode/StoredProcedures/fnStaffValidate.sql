IF OBJECT_ID (N'dbo.fnStaffValidate', N'FN') IS NOT NULL
    DROP FUNCTION fnStaffValidate;
GO
CREATE FUNCTION dbo.fnStaffValidate (@businessId bigint, @username identityName)
RETURNS bit
WITH EXECUTE AS CALLER
AS
BEGIN
	if not exists( select top 1 Id from dbo.Staff where BusinessId = @businessId and Username = @username )
		return 1;
	return 0;
END;
GO