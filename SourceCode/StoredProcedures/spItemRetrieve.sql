USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemRetrieve]
    @ItemId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT i.*, ig.Uom, ig.ItemGroupName, ig.ItemGroupCode, ig.ImagePath, ig.IsForSales as IsItemGroupForSales, 
			ic.ItemCategoryName, ib.ItemBrandName
	FROM [ITEM] i
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		on ig.ItemBrandId = ib.ItemBrandId
	WHERE i.ItemId = @ItemId

SET @ReturnValue = 0;
RETURN @ReturnValue;