USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spFinancialSettingManage')
   EXEC('CREATE PROCEDURE [dbo].[spFinancialSettingManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spFinancialSettingManage]
    @BusinessId bigint,
    @CurrencyId tinyint,
    @DefaultPriceTypeId tinyint,
    @UseServiceCharge bit,
    @ServiceChargeAmount decimal(18,4),
    @SalesTaxTypeId tinyint,
    @SalesTaxAmount decimal(18,4),
    @UseStockItemControl bit,
    @CostingTypeId tinyint,
	@ItemState char(1),
    @UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N' BEGIN

		INSERT INTO [dbo].[FinancialSetting]
			   ([BusinessId]
			   ,[CurrencyId]
			   ,[DefaultPriceTypeId]
			   ,[UseServiceCharge]
			   ,[ServiceChargeAmount]
			   ,[SalesTaxTypeId]
			   ,[SalesTaxAmount]
			   ,[UseStockItemControl]
			   ,[CostingTypeId]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
		 VALUES
			   (@BusinessId 
			   ,@CurrencyId
			   ,@DefaultPriceTypeId
			   ,@UseServiceCharge
			   ,@ServiceChargeAmount
			   ,@SalesTaxTypeId
			   ,@SalesTaxAmount
			   ,@UseStockItemControl
			   ,@CostingTypeId
			   ,@dateNow
			   ,@dateNow
			   ,@username
			   ,@username)

	END ELSE IF @ItemState = 'M' BEGIN

		UPDATE [dbo].[FinancialSetting]
		   SET 
			  [CurrencyId] = @CurrencyId
			  ,[DefaultPriceTypeId] = @DefaultPriceTypeId
			  ,[UseServiceCharge] = @UseServiceCharge
			  ,[ServiceChargeAmount] = @ServiceChargeAmount
			  ,[SalesTaxTypeId] = @SalesTaxTypeId
			  ,[SalesTaxAmount] = @SalesTaxAmount
			  ,[UseStockItemControl] = @UseStockItemControl
			  ,[CostingTypeId] = @CostingTypeId
			  ,[DateModified] = @dateNow
			  ,[UpdatedBy] = @username
		 WHERE [BusinessId] = @BusinessId

	END ELSE IF @ItemState = 'D' BEGIN
		DECLARE @tmp varchar;
	END

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO