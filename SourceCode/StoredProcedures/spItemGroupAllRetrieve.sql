USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemGroupAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemGroupAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemGroupAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, iCate.ItemCategoryName, iBrand.ItemBrandName, SUM(i.Quantity) AS Quantity
	from Item i 
	inner join ItemGroup ig
		on ig.ItemGroupId = i.ItemGroupId
	left join ItemCategory iCate
		on ig.ItemCategoryId = iCate.ItemCategoryId
	left join ItemBrand iBrand
		on ig.ItemBrandId = iBrand.ItemBrandId
	where
			ig.BusinessId = @BusinessId
		AND	ig.IsDisabled <> 1
	group by ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, iCate.ItemCategoryName, iBrand.ItemBrandName

SET @ReturnValue = 0;
RETURN @ReturnValue;