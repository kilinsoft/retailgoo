USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spOutgoingPaymentItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spOutgoingPaymentItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spOutgoingPaymentItemAllRetrieve]
	@OutgoingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		opi.*, 
		ro.ReceiveOrderNo, 
		ro.Total, 
		ro.Subtotal,
		ro.Tax,
		ro.AmountPaid,
		--(ro.Total - ro.AmountPaid) AS AmountDue
		ABS(ro.Subtotal - ro.AmountPaid) AS AmountDue
	FROM [dbo].[OutgoingPaymentItem] opi
	INNER JOIN [dbo].[ReceiveOrder] ro
		ON opi.ReceiveOrderId = ro.ReceiveOrderId
	WHERE	opi.OutgoingPaymentId = @OutgoingPaymentId
		AND opi.IsDisabled <> 1;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;