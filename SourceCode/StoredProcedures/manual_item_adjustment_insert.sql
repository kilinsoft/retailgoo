USE [retailgoo]
GO

DECLARE @dateNow DATETIME;
	SET @dateNow = GETDATE();
DECLARE @targetIaId BIGINT;
DECLARE @username varchar(50);
	EXEC @Username = GetUsername @UserId = 1;

INSERT INTO [dbo].[ItemAdjustment]
           ([BusinessId]
           ,[AdjustmentName]
           ,[AdjustmentReasonId]
           ,[AdjustmentNo]
           ,[Note]
           ,[Date]
		   ,[Status]
           ,[IsDisabled]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (1
           ,N'T-shirt'
           ,1
           ,N'AN-0000001'
           ,N'Note'
           ,@dateNow
		   ,N'New'
           ,0
           ,@dateNow
           ,@dateNow
           ,@Username
           ,@Username)

SET @targetIaId = SCOPE_IDENTITY();

INSERT INTO [dbo].[ItemAdjustmentItem]
           ([AdjustmentId]
           ,[ItemId]
           ,[QuantityBefore]
		   ,[QuantityAfter]
           ,[IsDisabled]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@targetIaId
           ,2
           ,100
		   ,100
           ,0
           ,@datenow
           ,@datenow
           ,@username
           ,@username)

INSERT INTO [dbo].[ItemAdjustmentItem]
           ([AdjustmentId]
           ,[ItemId]
           ,[QuantityBefore]
		   ,[QuantityAfter]
           ,[IsDisabled]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@targetIaId
           ,3
           ,200
		   ,200
           ,0
           ,@datenow
           ,@datenow
           ,@username
           ,@username)

GO


