IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tOutgoingPaymentItem')
   EXEC sp_droptype tOutgoingPaymentItem
GO
CREATE TYPE tOutgoingPaymentItem AS TABLE (
	[OutgoingPaymentItemId] [bigint] NOT NULL,
	[ReceiveOrderId] [bigint] NOT NULL,
	[SupplierInvoice] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
