USE [retailgoo]
GO
SET IDENTITY_INSERT [dbo].[IncomingPayment] ON 

INSERT [dbo].[IncomingPayment] ([IncomingPaymentId], [BusinessId], [IncomingPaymentNo], [CustomerId], [Reference], [AmountTypeId], [BankId], [BankAccNo], [Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (1, 1, N'PV-0000001', 1, N'Ref-00001', 1, 1, N'12345679', N'321/1', N'Suraj Rg.', N'Mueng', N'054-223001', CAST(N'2015-01-25' AS Date), 1, 0, CAST(0.0000 AS Decimal(18, 4)), N'Note', CAST(100.0000 AS Decimal(18, 4)), CAST(5.9500 AS Decimal(18, 4)), CAST(105.9500 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-01-25 13:16:53.240' AS DateTime), CAST(N'2015-01-25 13:16:53.240' AS DateTime), N'1', N'1')
INSERT [dbo].[IncomingPayment] ([IncomingPaymentId], [BusinessId], [IncomingPaymentNo], [CustomerId], [Reference], [AmountTypeId], [BankId], [BankAccNo], [Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (2, 1, N'PV-0000002', 2, N'Ref-00003', 0, 1, N'', N'44', N'Suanluang Rd.', N'Huay-Kwang', N'089-4357222, 02-2223311', CAST(N'2015-01-25' AS Date), 0, 0, CAST(0.0000 AS Decimal(18, 4)), N'Note', CAST(319.9500 AS Decimal(18, 4)), CAST(0.2200 AS Decimal(18, 4)), CAST(319.7300 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-01-25 13:18:50.917' AS DateTime), CAST(N'2015-01-25 13:18:50.917' AS DateTime), N'1', N'1')
SET IDENTITY_INSERT [dbo].[IncomingPayment] OFF
SET IDENTITY_INSERT [dbo].[IncomingPaymentItem] ON 

INSERT [dbo].[IncomingPaymentItem] ([IncomingPaymentItemId], [IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (1, 1, 1, CAST(105.9500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(N'2015-01-25 13:16:53.240' AS DateTime), CAST(N'2015-01-25 13:16:53.240' AS DateTime), N'1', N'1')
INSERT [dbo].[IncomingPaymentItem] ([IncomingPaymentItemId], [IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (2, 2, 2, CAST(319.9500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(N'2015-01-25 13:18:50.917' AS DateTime), CAST(N'2015-01-25 13:18:50.917' AS DateTime), N'1', N'1')
SET IDENTITY_INSERT [dbo].[IncomingPaymentItem] OFF
SET IDENTITY_INSERT [dbo].[SalesInvoice] ON 

INSERT [dbo].[SalesInvoice] ([SalesInvoiceId], [BusinessId], [SalesInvoiceNo], [IsCashSales], [CashSalesNo], [CustomerId], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [DestinationType], [PriceType], [AmountType], [Note], [SalesPersonId], [Subtotal], [Discount], [Charge], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (1, 1, N'IV-0000001', 1, N'CS-0000001', 1, N'Ref-00001', N'Person A1', N'321/1', N'Suraj Rg.', N'Mueng', N'054-223001', CAST(N'2015-01-25' AS Date), CAST(N'2015-01-25' AS Date), 1, 0, 1, N'Note', 1, CAST(100.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(5.9500 AS Decimal(18, 4)), CAST(105.9500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-01-25 13:16:53.240' AS DateTime), CAST(N'2015-01-25 13:16:53.240' AS DateTime), N'1', N'1')
INSERT [dbo].[SalesInvoice] ([SalesInvoiceId], [BusinessId], [SalesInvoiceNo], [IsCashSales], [CashSalesNo], [CustomerId], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [DestinationType], [PriceType], [AmountType], [Note], [SalesPersonId], [Subtotal], [Discount], [Charge], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (2, 1, N'IV-0000002', 0, N'', 2, N'Ref-00002', N'David Moyes', N'44', N'Suanluang Rd.', N'Huay-Kwang', N'089-4357222, 02-2223311', CAST(N'2015-01-25' AS Date), CAST(N'2015-01-25' AS Date), 2, 0, 1, N'Note', 1, CAST(300.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(19.9500 AS Decimal(18, 4)), CAST(319.9500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-01-25 13:17:57.187' AS DateTime), CAST(N'2015-01-25 13:17:57.187' AS DateTime), N'1', N'1')
SET IDENTITY_INSERT [dbo].[SalesInvoice] OFF
SET IDENTITY_INSERT [dbo].[SalesInvoiceItem] ON 

INSERT [dbo].[SalesInvoiceItem] ([SalesInvoiceItemId], [SalesInvoiceId], [ItemId], [Price], [Quantity], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (1, 1, 2, CAST(1.0000 AS Decimal(18, 4)), CAST(100.0000 AS Decimal(18, 4)), 0, CAST(N'2015-01-25 13:16:53.240' AS DateTime), CAST(N'2015-01-25 13:16:53.240' AS DateTime), N'1', N'1')
INSERT [dbo].[SalesInvoiceItem] ([SalesInvoiceItemId], [SalesInvoiceId], [ItemId], [Price], [Quantity], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	VALUES (2, 2, 3, CAST(3.0000 AS Decimal(18, 4)), CAST(100.0000 AS Decimal(18, 4)), 0, CAST(N'2015-01-25 13:17:57.187' AS DateTime), CAST(N'2015-01-25 13:17:57.187' AS DateTime), N'1', N'1')
SET IDENTITY_INSERT [dbo].[SalesInvoiceItem] OFF
