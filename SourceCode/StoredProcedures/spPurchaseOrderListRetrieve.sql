USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderListRetrieve]
    @BusinessId BIGINT,
	@SupplierId BIGINT = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT
		po.PurchaseOrderId AS Value,
		po.PurchaseOrderNo As Text
	FROM [PurchaseOrder] po
	WHERE BusinessId = @BusinessId
		AND (po.Status = 'New' OR po.Status = 'Partial')
		AND IsDisabled <> 1
		AND po.SupplierId = @SupplierId --Retrieve according to supplied SupplierId

SET @ReturnValue = 0;
RETURN @ReturnValue;