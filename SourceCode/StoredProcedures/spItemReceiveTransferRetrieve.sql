USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemReceiveTransferRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemReceiveTransferRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemReceiveTransferRetrieve]
    @TransferId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		it.*,
		b1.BusinessName AS SourceBusinessName,
		b2.BusinessName AS TargetBusinessName,
		itst.TransferStatusTypeName
	FROM [ItemTransfer] it
	INNER JOIN [Business] b1
		ON	it.SourceBusinessId = b1.BusinessId
	INNER JOIN [Business] b2
		ON	it.TargetBusinessId = b2.BusinessId
	INNER JOIN [ItemTransferStatusType] itst
		ON	it.TransferStatusTypeId = itst.TransferStatusTypeId
	WHERE	it.TransferId = @TransferId;

SET @ReturnValue = 0;
RETURN @ReturnValue;