USE [retailgoo]
GO

DECLARE @dateNow DATETIME;
	SET @dateNow = GETDATE();
DECLARE @targetId BIGINT;
DECLARE @username varchar(50);
	EXEC @Username = GetUsername @UserId = 1;

INSERT INTO [dbo].[ItemTransfer]
           ([TransferName]
           ,[SourceBusinessId]
           ,[TargetBusinessId]
           --,[TransfererId]
           --,[ReceiverId]
           ,[Reference]
           ,[DateSent]
           ,[DateReceived]
           --,[Status]
		   ,[TransferStatusTypeId]
           ,[IsDisabled]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (N'IT-0000001'
           ,1
           ,2
           --,1
           --,1
           ,N'REF-000001'
           ,@dateNow
           ,@dateNow
           --,N'New'
		   ,1 --Transferring
           ,0
           ,@dateNow
           ,@dateNow
           ,@username
           ,@username)

SET @targetId = SCOPE_IDENTITY();

INSERT INTO [dbo].[ItemTransferItem]
           ([TransferId]
           ,[ItemGroupCode]
           ,[ItemGroupName]
           ,[ItemName]
           ,[Uom]
           ,[ImagePath]
           ,[ItemCategoryName]
           ,[ItemBrandName]
           ,[Barcode]
           ,[Quantity]
           ,[Cost]
           ,[Price]
           ,[IsForSales]
           ,[IsDisabled]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@targetId
           ,N'1000001'
           ,N'T-shirt'
           ,N'XXXL'
           ,1000
           ,N''
           ,N'T-shirt'
           ,N'PUMA'
           ,N''
           ,100
           ,10
           ,10
           ,1
           ,0
           ,@dateNow
           ,@dateNow
           ,@username
           ,@username)

INSERT INTO [dbo].[ItemTransferItem]
           ([TransferId]
           ,[ItemGroupCode]
           ,[ItemGroupName]
           ,[ItemName]
           ,[Uom]
           ,[ImagePath]
           ,[ItemCategoryName]
           ,[ItemBrandName]
           ,[Barcode]
           ,[Quantity]
           ,[Cost]
           ,[Price]
           ,[IsForSales]
           ,[IsDisabled]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@targetId
           ,N'1000002'
           ,N'T-shirt'
           ,N'M'
           ,200
           ,N''
           ,N'T-shirt'
           ,N'Adidas'
           ,N''
           ,200
           ,20
           ,20
           ,0
           ,0
           ,@dateNow
           ,@dateNow
           ,@username
           ,@username)
GO


