USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPaymentTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spPaymentTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPaymentTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select PaymentTypeId AS Value, PaymentTypeName AS Text
	from PaymentType

SET @ReturnValue = 0;
RETURN @ReturnValue;