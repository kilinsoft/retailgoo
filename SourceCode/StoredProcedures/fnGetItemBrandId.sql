USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemBrandId]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnGetItemBrandId', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnGetItemBrandId];
GO
CREATE FUNCTION [dbo].[fnGetItemBrandId](@BusinessId BIGINT, @ItemBrandName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemBrandId BIGINT;
		SET @ItemBrandId = 0;

	SELECT TOP 1 @ItemBrandId = ib.ItemBrandId
	FROM ItemBrand ib
	WHERE ib.BusinessId = @BusinessId
		AND ib.ItemBrandName = @ItemBrandName

	RETURN @ItemBrandId;
END
