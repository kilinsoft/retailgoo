USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spUserProfileRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spUserProfileRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spUserProfileRetrieve]
    @UserId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT TOP 1 *, c.CountryName, c.CountryShortName
	FROM [UserProfile] uprofile
	INNER JOIN dbo.[User] usr
		ON uprofile.UserId = usr.UserId
	LEFT JOIN [Country] c
		ON uprofile.CountryId = c.CountryId	
	WHERE uprofile.UserId = @UserId;

SET @ReturnValue = 0;
RETURN @ReturnValue;