USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemGroupInfoRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemGroupInfoRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemGroupInfoRetrieve]
	@BusinessId	BIGINT,
	@ItemGroupId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, sum(i.Quantity) as ItemQuantity, ig.DateModified
	from Item i
	left join ItemGroup ig
		on ig.ItemGroupId = i.ItemGroupId
	full outer join ItemBrand ib
		on ib.ItemBrandId = ig.ItemBrandId
	where
		ig.ItemGroupId = @ItemGroupId
		AND ig.BusinessId = @BusinessId
		AND	ig.IsDisabled <> 1
	group by ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, ig.DateModified

SET @ReturnValue = 0;
RETURN @ReturnValue;
