USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemId]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnGetItemId', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnGetItemId];
GO
CREATE FUNCTION [dbo].[fnGetItemId](@BusinessId BIGINT, @ItemGroupCode VARCHAR(50), @ItemGroupName NVARCHAR(100), @ItemName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemId BIGINT;
		SET @ItemId = 0;

	SELECT TOP 1 @ItemId = i.ItemId
	FROM Item i
	INNER JOIN ItemGroup ig
		ON	ig.ItemGroupId = i.ItemGroupId
	WHERE i.ItemName = @ItemName
		AND ig.BusinessId = @BusinessId
		AND ig.ItemGroupCode = @ItemGroupCode
		AND ig.ItemGroupName = @ItemGroupName

	RETURN @ItemId;
END
