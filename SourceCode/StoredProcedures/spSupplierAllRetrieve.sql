USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSupplierAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSupplierAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSupplierAllRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.SupplierId, s.SupplierName, s.SupplierNo, s.ContactPerson, s.Phone, sg.SupplierGroupName
	from Supplier s
	left join SupplierGroup sg
		on s.SupplierGroupId = sg.SupplierGroupId
	where 
			s.BusinessId = @BusinessId
		and	s.IsDisabled = 0

SET @ReturnValue = 0;
RETURN @ReturnValue;