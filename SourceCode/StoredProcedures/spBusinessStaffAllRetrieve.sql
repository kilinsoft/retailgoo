USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBusinessStaffAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spBusinessStaffAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBusinessStaffAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT *
	FROM [Staff] stf
	INNER JOIN [System_BusinessRole] sys_role
		ON stf.Role = sys_role.BusinessRoleShortName
	LEFT JOIN dbo.[User] usr
		ON stf.UserId = usr.UserId
	WHERE	stf.BusinessId = @BusinessId
		AND stf.IsDisabled <> 1
	ORDER BY stf.DateModified DESC;

	
SET @ReturnValue = @@ROWCOUNT;
RETURN @ReturnValue;


GO