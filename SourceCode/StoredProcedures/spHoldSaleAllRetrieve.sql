USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spHoldSaleAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spHoldSaleAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spHoldSaleAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		b.BillId, b.CustomerId, c.CustomerName, b.Total,
		SUM(bi.Quantity) As Quantity
	FROM [Bill] b
	LEFT JOIN [BillItem] bi
		ON b.BillId = bi.BillId
	LEFT JOIN [Customer] c
		ON b.CustomerId = c.CustomerId
	WHERE	b.Status <> 'Done'
		AND b.IsDisabled <> 1
		AND bi.IsDisabled <> 1
		AND	b.BusinessId = @BusinessId
	GROUP BY b.BillId, b.CustomerId, c.CustomerName, b.Total;

SET @ReturnValue = 0;
RETURN @ReturnValue;