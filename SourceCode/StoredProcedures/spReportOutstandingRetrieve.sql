USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReportOutstandingRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReportOutstandingRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReportOutstandingRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @NumPendingPaymentSalesInvoice INT;
	DECLARE @NumOverdueSalesInvoice INT;
	DECLARE @NumItemToReorder INT;
	DECLARE @NumPendingReceivingPurchaseOrder INT;
	DECLARE @NumOverduePurchaseOrder INT;
	DECLARE @NumPendingPaymentReceiveOrder INT;
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	-- 1. Pending Sales Invoice
	SELECT 
		@NumPendingPaymentSalesInvoice = Count(si.SalesInvoiceId)
	FROM SalesInvoice si
	WHERE	si.Status = 'New'
		AND	si.BusinessId = @BusinessId;

	-- 2. Overdue Sales Invoice
	--Overdue Sales Invoice = SI with TodayDate > SI.Date+30
	SELECT 
		@NumOverdueSalesInvoice = Count(si.SalesInvoiceId) 
	FROM SalesInvoice si
	WHERE	@dateNow > DATEADD(DAY, 30, si.Date)
		AND si.Status = 'New'
		AND	si.BusinessId = @BusinessId;

	-- 3. Re-order Item
	SELECT 
		@NumItemToReorder = Count(i.ItemId) 
	FROM Item i
	INNER JOIN ItemGroup ig
		ON i.ItemGroupId = ig.ItemGroupId
	INNER JOIN ItemRestocking istock
		ON i.ItemId = istock.ItemId
	WHERE	i.Quantity < istock.Threshold
		AND	ig.BusinessId = @BusinessId;
	
	
	-- 4. Purchase Order that has not been received yet
	SELECT 
		@NumPendingReceivingPurchaseOrder = Count(po.PurchaseOrderId)
	FROM PurchaseOrder po
	WHERE	po.Status = 'New'
		AND	po.BusinessId = @BusinessId;

	-- 5. Overdue Purchase Order = PO with TodayDate > PO.DeliveryDate
	SELECT 
		@NumOverduePurchaseOrder = Count(po.PurchaseOrderId)
	FROM PurchaseOrder po
	WHERE	@dateNow > po.DateDelivery
		AND	po.BusinessId = @BusinessId;

	-- 6. Receive Order that has not been paid yet
	SELECT 
		@NumPendingPaymentReceiveOrder = Count(ro.ReceiveOrderId)
	FROM ReceiveOrder ro
	WHERE	ro.Status = 'New'
		AND	ro.BusinessId = @BusinessId;

	SELECT
		@NumPendingPaymentSalesInvoice AS PendingPaymentSalesInvoiceCount,
		@NumOverdueSalesInvoice AS OverdueSalesInvoiceCount,
		@NumItemToReorder AS ItemToReorderCount,
		@NumPendingReceivingPurchaseOrder AS PendingReceivingPurchaseOrderCount,
		@NumOverduePurchaseOrder AS OverduePurchaseOrderCount,
		@NumPendingPaymentReceiveOrder AS PendingPaymentReceiveOrderCount;

DECLARE @ReturnValue INT;
SET @ReturnValue = 0;
RETURN @ReturnValue;