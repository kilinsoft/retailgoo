USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBusinessTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spBusinessTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBusinessTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select b.BusinessTypeId AS Value, b.BusinessTypeName AS Text
	from BusinessType b

SET @ReturnValue = 0;
RETURN @ReturnValue;