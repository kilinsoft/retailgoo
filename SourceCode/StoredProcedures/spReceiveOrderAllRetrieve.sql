USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveOrderAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveOrderAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveOrderAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ro.*, s.SupplierName
	from [ReceiveOrder] ro
	left join [Supplier] s
		on ro.SupplierId = s.SupplierId
	where	ro.BusinessId = @BusinessId
		and ro.IsDisabled <> 1
		AND ro.IsCashPurchase <> 1
	order by ro.ReceiveOrderNo desc;

	---- 1. Select base RO
	--SELECT *, AmountDue = Total, CONVERT(VARCHAR(16), 'New') AS Status2 INTO #tmpRo
	--FROM [ReceiveOrder]
	--WHERE BusinessId = @BusinessId
	--	AND IsDisabled <> 1
	--ORDER BY ReceiveOrderId DESC

	---- 2. Select Sum(AmountPay) from OPI
	--SELECT opi.ReceiveOrderId, SUM(opi.Amount) AS AmountPay
	--INTO #tmpOpiAmt
	--FROM OutgoingPaymentItem opi
	--INNER JOIN OutgoingPayment op
	--	ON op.OutgoingPaymentId = opi.OutgoingPaymentId
	--WHERE 
	--		opi.IsDisabled <> 1
	--	AND op.BusinessId = @BusinessId
	--GROUP BY opi.ReceiveOrderId;

	---- 3. Update tmpRo with Sum(AmountPay) from tmpOpiAmt
	--UPDATE #tmpRo
	--SET 
	--	AmountDue = (Total - opi.AmountPay)
	--FROM #tmpOpiAmt opi
	--WHERE #tmpRo.ReceiveOrderId = opi.ReceiveOrderId;

	---- 4. Update Status2
	--UPDATE #tmpRo
	--SET
	--	Status2 = 
	--		CASE 
	--			WHEN AmountDue <= 0 THEN 'Done'
	--			WHEN AmountDue >= Total THEN 'New'
	--			ELSE 'Partial'
	--		END;

	--SELECT * FROM #tmpRo

SET @ReturnValue = 0;
RETURN @ReturnValue;