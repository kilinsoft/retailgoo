USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemGroupCreateInline')
   EXEC('CREATE PROCEDURE [dbo].[spItemGroupCreateInline] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemGroupCreateInline]
	@BusinessId bigint,
	@ItemGroupName bigint,
	@UserId bigint
AS
BEGIN TRY
BEGIN TRANSACTION
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	declare @dateNow datetime;
		set @dateNow = getdate();
	declare @username varchar(50);
		exec @username = GetUsername @UserId = @UserId;
	declare @targetItemGroupId bigint;

	insert into ItemGroup
		([BusinessId]
		,[ItemGroupCode]
		,[ItemGroupName]
		,[ImagePath]
		,[ItemCategoryId]
		,[ItemBrandId]
		,[Cost]
		,[Price]
		,[Uom]
		,[IsForSales]
		,[IsDisabled]
		,[DateCreated]
		,[DateModified]
		,[CreatedBy]
		,[UpdatedBy])
	VALUES 
	( 
		@BusinessId, N'', @ItemGroupName, '', NULL, NULL, 0, 0, 'Unit', 0, 0, 
		@dateNow, @dateNow, @username, @username
	);
	SET @targetItemGroupId = SCOPE_IDENTITY();


	--- Insert Item
	INSERT INTO Item
        ([ItemGroupId]
        ,[ItemName]
        ,[Barcode]
        ,[Quantity]
        ,[Cost]
        ,[Price]
        ,[Price02]
        ,[Price03]
        ,[Price04]
        ,[IsForSales]
        ,[IsDisabled]
        ,[DateCreated]
        ,[DateModified]
        ,[CreatedBy]
        ,[UpdatedBy])
	VALUES 
	(
		@targetItemGroupId, @ItemGroupName, '', 0, 0, 0, 0,  0, 0,
		0, 0, @dateNow, @dateNow, @username, @username 
	);

	SET @ReturnValue = 0;
	RETURN @ReturnValue;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO