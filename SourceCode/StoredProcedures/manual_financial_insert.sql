INSERT INTO [retailgoo].[dbo].[financial]
           ([business_id]
           ,[currency]
           ,[pricing_factor]
           ,[service_charge_include]
           ,[service_charge]
           ,[sales_tax_type]
           ,[sales_tax]
           ,[stock_item_control]
           ,[costing_method]
           ,[updated_by]
           ,[date_created]
           ,[date_modified])
     VALUES
           (1
           ,'THB'
           ,1.00
           ,1
           ,10.00
           ,'INC'
           ,7.00
           ,0
           ,'MA'
           ,'user_a1'
           ,GETDATE()
           ,GETDATE())
GO


