USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spIncomingPaymentVoid')
   EXEC('CREATE PROCEDURE [dbo].[spIncomingPaymentVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spIncomingPaymentVoid]
	@IncomingPaymentId BIGINT,
	@UserId BIGINT
AS
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
		SET @blockingItem = dbo.fnIsIncomingPaymentEditable(@IncomingPaymentId);
	DECLARE @ErrTxt NVARCHAR(MAX);

	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	UPDATE [dbo].[IncomingPayment]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE IncomingPaymentId = @IncomingPaymentId;

	-- Update SalesInvoice.Status of the SalesInvoice item that this IncomingPayment item refers to
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT result.SalesInvoiceId FROM (
		SELECT DISTINCT
			si.SalesInvoiceId
		FROM [SalesInvoice] si
		INNER JOIN [IncomingPaymentItem] ipi
			ON si.SalesInvoiceId = ipi.SalesInvoiceId
				AND si.IsDisabled <> 1
				AND si.Status <> 'Void'
		INNER JOIN [IncomingPayment] ip
			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
				AND ip.IsDisabled <> 1
				AND ip.Status <> 'Void'
		WHERE ip.IncomingPaymentId = @IncomingPaymentId
	) result;

	EXEC dbo.spSalesInvoiceStatusUpdate @updateItemList;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;