USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderVoid')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderVoid]
	@PurchaseOrderId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	SET NOCOUNT ON;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	SET @blockingItem = dbo.fnIsPurchaseOrderEditable(@PurchaseOrderId);
	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	UPDATE [dbo].[PurchaseOrder]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE 
		PurchaseOrderId = @PurchaseOrderId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
