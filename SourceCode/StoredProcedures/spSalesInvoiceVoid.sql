USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesInvoiceVoid')
   EXEC('CREATE PROCEDURE [dbo].[spSalesInvoiceVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesInvoiceVoid]
	@SalesInvoiceId BIGINT,
	@UserId BIGINT
AS

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	SET @blockingItem = dbo.fnIsSalesInvoiceEditable(@SalesInvoiceId);
	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	UPDATE [dbo].[SalesInvoice]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE SalesInvoiceId = @SalesInvoiceId;

	-- Update Item.Quantity
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + sii.Quantity
	FROM [SalesInvoiceItem] sii
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @SalesInvoiceId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;