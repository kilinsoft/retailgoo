USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCashPurchaseVoid')
   EXEC('CREATE PROCEDURE [dbo].[spCashPurchaseVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCashPurchaseVoid]
    @PurchaseOrderId BIGINT,
	@UserId BIGINT
AS
	DECLARE @ReturnValue INT;
	DECLARE @status VARCHAR(10);
		SET @status = 'Void';
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	SET NOCOUNT ON;

	-- 1. VOID PO, RO, and OP
	UPDATE [dbo].[PurchaseOrder]
	SET Status = @status, DateModified = @dateNow, UpdatedBy = @username
	WHERE	PurchaseOrderId = @PurchaseOrderId;

	UPDATE [dbo].[ReceiveOrder]
	SET Status = @status, DateModified = @dateNow, UpdatedBy = @username
	FROM [ReceiveOrder] 
	INNER JOIN 
	(
		SELECT ro.* 
		FROM [ReceiveOrder] ro
		INNER JOIN [ReceiveOrderItem] roi 
			ON ro.ReceiveOrderId = roi.ReceiveOrderId
		INNER JOIN [PurchaseOrderItem] poi
			ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
		WHERE poi.PurchaseOrderId = @PurchaseOrderId
	) ref1
	ON ref1.ReceiveOrderId = [ReceiveOrder].ReceiveOrderId

	UPDATE [dbo].[OutgoingPayment]
	SET Status = @status, DateModified = @dateNow, UpdatedBy = @username
	FROM [OutgoingPayment]
	INNER JOIN 
	(
		SELECT op.* FROM [OutgoingPayment] op
		INNER JOIN [OutgoingPaymentItem] opi
			ON op.OutgoingPaymentId = opi.OutgoingPaymentId
		INNER JOIN [ReceiveOrder] ro
			ON opi.ReceiveOrderId = ro.ReceiveOrderId
		INNER JOIN [ReceiveOrderItem] roi 
			ON ro.ReceiveOrderId = roi.ReceiveOrderId
		INNER JOIN [PurchaseOrderItem] poi
			ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
		WHERE poi.PurchaseOrderId = @PurchaseOrderId
	) ref2
	ON ref2.OutgoingPaymentId = [OutgoingPayment].OutgoingPaymentId

	-- 2. Update Item.Quantity to reflect SubItems of this void'ed CashPurchase item
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity - roi.Quantity
	FROM [ReceiveOrderItem] roi
	INNER JOIN [PurchaseOrderItem] po
		ON	roi.PurchaseOrderItemId = po.PurchaseOrderItemId
	WHERE	po.PurchaseOrderId = @PurchaseOrderId;

	
SET @ReturnValue = 0;
RETURN @ReturnValue;