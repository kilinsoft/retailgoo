USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCustomerListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCustomerListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCustomerListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT s.CustomerId AS Value, s.CustomerName AS Text
	FROM [dbo].[Customer] s
	WHERE
		s.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;