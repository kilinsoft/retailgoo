USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCashSalesAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCashSalesAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCashSalesAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT SI.*, CM.CustomerName
	FROM [SalesInvoice] SI
	LEFT JOIN [Customer] CM
		ON SI.CustomerId = CM.CustomerId
	WHERE SI.BusinessId = @BusinessId
		AND SI.IsDisabled <> 1
		AND SI.IsCashSales = 1
	ORDER BY SI.SalesInvoiceId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;


GO


