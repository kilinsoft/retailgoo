USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spIncomingPaymentItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spIncomingPaymentItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spIncomingPaymentItemAllRetrieve]
	@IncomingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	--SELECT ipItem.*, ip.Date, ip.Subtotal, ip.Tax,
	--	CASE WHEN si.IsCashSales <> 1
	--	    THEN si.SalesInvoiceNo
	--		ELSE si.CashSalesNo
	--	END AS SalesInvoiceNo
	--FROM [dbo].[IncomingPaymentItem] ipItem
	--	INNER JOIN [dbo].[IncomingPayment] ip
	--		ON ip.IncomingPaymentId = ipItem.IncomingPaymentId
	--	INNER JOIN [dbo].[SalesInvoice] si
	--		ON si.SalesInvoiceId = ipItem.SalesInvoiceId
	--WHERE ipItem.IncomingPaymentId = @IncomingPaymentId
	--    AND ipItem.IsDisabled <> 1

	SELECT 
		ipi.SalesInvoiceId,
		si.SalesInvoiceNo,
		si.Date,
		si.Total,
		si.Tax,
		si.Subtotal,
		(si.Total - si.AmountPaid) AS AmountDue,
		ipi.Amount AS AmountPaid
	FROM [IncomingPaymentItem] ipi
	INNER JOIN [IncomingPayment] ip
		ON ipi.IncomingPaymentId = ip.IncomingPaymentId
	INNER JOIN [SalesInvoice] si
		ON ipi.SalesInvoiceId = si.SalesInvoiceId
	WHERE	ipi.IncomingPaymentId = @IncomingPaymentId
		AND ipi.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;
