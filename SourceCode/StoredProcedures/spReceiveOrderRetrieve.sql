USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveOrderRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveOrderRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveOrderRetrieve]
    @ReceiveOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT ro.*, s.SupplierName
	FROM [ReceiveOrder] ro
	LEFT JOIN [Supplier] s
		ON ro.SupplierId = s.SupplierId
	WHERE	ro.ReceiveOrderId = @ReceiveOrderId;

SET @ReturnValue = 0;
RETURN @ReturnValue;