IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tItemTransfer')
   EXEC sp_droptype tItemTransfer
GO
CREATE TYPE tItemTransfer AS TABLE (
	[TransferItemId] [bigint] NOT NULL,
	[TransferId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	--[ItemGroupCode] [varchar](50) NOT NULL,
	--[ItemGroupName] [nvarchar](100) NOT NULL,
	--[ItemName] [nvarchar](100) NOT NULL,
	--[Uom] [nvarchar](100) NOT NULL,
	--[ImagePath] [varchar](max) NOT NULL,
	--[ItemCategoryName] [nvarchar](100) NOT NULL,
	--[ItemBrandName] [nvarchar](100) NOT NULL,
	--[Barcode] [varchar](50) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	--[Cost] [decimal](18, 4) NOT NULL,
	--[Price] [decimal](18, 4) NOT NULL,
	--[IsForSales] [bit] NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
