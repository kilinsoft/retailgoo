USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemTransferVoid')
   EXEC('CREATE PROCEDURE [dbo].[spItemTransferVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemTransferVoid]
	@TransferId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	SET NOCOUNT ON;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	UPDATE [dbo].[ItemTransfer]
	--SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	SET DateModified = GETDATE(), UpdatedBy = @username
	WHERE 
		TransferId = @TransferId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
