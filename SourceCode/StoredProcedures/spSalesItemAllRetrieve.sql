USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSalesItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesItemAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		i.ItemId, i.ItemName, i.Price, i.Barcode, i.ItemGroupId, ig.ItemGroupName, ig.ItemGroupCode, ig.Uom, ig.ImagePath,
		ic.ItemCategoryId, ic.ItemCategoryName, ib.ItemBrandId, ib.ItemBrandName
	FROM [ITEM] i
		INNER JOIN [ItemGroup] ig
			ON i.ItemGroupId = ig.ItemGroupId
		LEFT JOIN [ItemBrand] ib
			ON ig.ItemBrandId = ib.ItemBrandId
		LEFT JOIN [ItemCategory] ic
			ON ig.ItemCategoryId = ic.ItemCategoryId
	WHERE 
			i.IsDisabled <> 1
		AND ig.IsDisabled <> 1
		AND ig.IsForSales = 1
		AND i.IsForSales = 1
		AND ig.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;