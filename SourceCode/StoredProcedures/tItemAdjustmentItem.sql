USE [retailgoo]
GO
IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tItemAdjustmentItem')
   EXEC sp_droptype tItemAdjustmentItem
GO
CREATE TYPE dbo.tItemAdjustmentItem AS TABLE (
	[AdjustmentItemId] [bigint] NOT NULL,
	[AdjustmentId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[QuantityBefore] [decimal](18, 4) NOT NULL,
	[QuantityAfter] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
