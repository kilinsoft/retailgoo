USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemTransferStatusListAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemTransferStatusListAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemTransferStatusListAllRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		TransferStatusTypeId AS Value,
		TransferStatusTypeName AS Text
	FROM [ItemTransferStatusType];

SET @ReturnValue = 0;
RETURN @ReturnValue;