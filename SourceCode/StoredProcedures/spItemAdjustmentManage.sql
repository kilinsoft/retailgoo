USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemAdjustmentManage')
   EXEC('CREATE PROCEDURE [dbo].[spItemAdjustmentManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemAdjustmentManage]
	@AdjustmentId bigint,
	@BusinessId bigint,
    @AdjustmentName nvarchar(MAX),
	@AdjustmentReasonId tinyint,
    @Note nvarchar(MAX),
	@Date datetime,
	--@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tItemAdjustmentItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetAmId BIGINT;
		SET @targetAmId = @AdjustmentId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	-- 1. Manage records in [ItemAdjustment] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextAmName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'StockAdj', @RetDocName = @nextAmName OUT;
			-- Insert Item Adjustment
			INSERT INTO [dbo].[ItemAdjustment] 
				([BusinessId],[AdjustmentName],[AdjustmentReasonId], [AdjustmentNo], [Note], [Date], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @AdjustmentName, @AdjustmentReasonId, @nextAmName, @Note, @Date, 'New', 0, @dateNow, @dateNow, @username, @username
			SET @targetAmId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[ItemAdjustment]
			SET    
			[AdjustmentName] = @AdjustmentName, [AdjustmentReasonId] = @AdjustmentReasonId, 
			[Note] = @Note, [Date] = @Date,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[AdjustmentId] = @AdjustmentId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[ItemAdjustment]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[AdjustmentId] = @AdjustmentId
		END

	-- 1. Manage SubItems
	---- Insert Item Adjustment's items
	INSERT INTO [dbo].[ItemAdjustmentItem]
	    ([AdjustmentId], [ItemId], [QuantityBefore], [QuantityAfter], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT AdjustmentId = @targetAmId, ItemId, QuantityBefore, QuantityAfter, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Sales Invoice's items
	UPDATE iaItem
	SET iaItem.ItemId = i.ItemId, iaItem.QuantityBefore = i.QuantityBefore, iaItem.QuantityAfter = i.QuantityAfter,
	    iaItem.DateModified = @dateNow, iaItem.UpdatedBy = @username
	FROM [dbo].[ItemAdjustmentItem] AS iaItem
		INNER JOIN @Items AS i
		ON iaItem.AdjustmentId = i.AdjustmentId 
		WHERE i.ItemState = 'M'

	---- Disable Sales Invoice's items
	UPDATE iaItem
	SET iaItem.IsDisabled = 1, iaItem.DateModified = @dateNow, iaItem.UpdatedBy = @username
	FROM [dbo].[ItemAdjustmentItem] AS iaItem
		INNER JOIN @Items AS i
		ON iaItem.AdjustmentId = i.AdjustmentId 
		WHERE i.ItemState = 'D'

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO