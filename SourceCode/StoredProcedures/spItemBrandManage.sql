IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemBrandManage')
   EXEC('CREATE PROCEDURE [dbo].[spItemBrandManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemBrandManage]
	@ItemBrandId bigint,
	@BusinessId bigint,
    @ItemBrandName nvarchar(100),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tItemBrandItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetIbId BIGINT;
		SET @targetIbId = @ItemBrandId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	--DECLARE @updateItemList tIdTable;

	-- Manage records in [ItemBrand] table
	IF @ItemState = 'N'
		BEGIN
			-- Insert ItemBrand
			INSERT INTO [dbo].[ItemBrand] 
				([BusinessId],[ItemBrandName],[IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @ItemBrandName, 0, @dateNow, @dateNow, @username, @username
			SET @targetIbId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			-- Update ItemBrand
			UPDATE [dbo].[ItemBrand]
			SET    
				[ItemBrandName] = @ItemBrandName,
				[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[ItemBrandId] = @ItemBrandId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			-- Delete ItemBrand
			UPDATE [dbo].[ItemBrand]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[ItemBrandId] = @ItemBrandId
		END

	---- Insert ItemGroup
	--INSERT INTO [dbo].[ItemGroup]
	--    ([BusinessId], [ItemBrandId], [ItemGroupCode], [ItemGroupName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	--SELECT BusinessId = @BusinessId, ItemBrandId = @targetIbId, ItemGroupCode, ItemGroupName, 0, @dateNow, @dateNow, @username, @username
	--	FROM @Items
	--	WHERE ItemState = 'N';

	---- Update ItemGroup
	UPDATE igItem
	SET igItem.ItemBrandId = @targetIbId, 
	    igItem.DateModified = @dateNow, igItem.UpdatedBy = @username
	FROM [dbo].[ItemGroup] AS igItem
		INNER JOIN @Items AS i
		ON igItem.ItemGroupId = i.ItemGroupId 
		WHERE i.ItemState = 'M'

	---- Disable ItemGroup
	UPDATE igItem
	SET igItem.IsDisabled = 1, igItem.DateModified = @dateNow, igItem.UpdatedBy = @username
	FROM [dbo].[ItemGroup] AS igItem
		INNER JOIN @Items AS i
		ON igItem.ItemGroupId = i.ItemGroupId
		WHERE i.ItemState = 'D'


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO