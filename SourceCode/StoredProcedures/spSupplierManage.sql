USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSupplierManage')
   EXEC('CREATE PROCEDURE [dbo].[spSupplierManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSupplierManage]
	@BusinessId bigint,
	@SupplierId bigint,
	@SupplierName nvarchar(50),
	@SupplierNo nvarchar(50),
	@SupplierGroupId tinyint,
	@TaxNo nvarchar(50),
	@ContactPerson nvarchar(100),
	@Email varchar(320),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@CreditTerm nvarchar(max),
	@Address1 nvarchar(100),
	@Address2 nvarchar(100),
	@Address3 nvarchar(100),
	@City nvarchar(50),
	@CountryId tinyint,
	@ZipCode nvarchar(50),
	@Remark nvarchar(max),
	@UserId bigint,
	@ItemState char(1),
	@Version datetime
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @Username varchar(50);
	
	exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Supplier]
			   ([BusinessId]
			   ,[SupplierName]
			   ,[SupplierNo]
			   ,[SupplierGroupId]
			   ,[TaxNo]
			   ,[ContactPerson]
			   ,[Email]
			   ,[Phone]
			   ,[Fax]
			   ,[CreditTerm]
			   ,[Address1]
			   ,[Address2]
			   ,[Address3]
			   ,[City]
			   ,[CountryId]
			   ,[ZipCode]
			   ,[Remark]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
				(@BusinessId, @SupplierName, @SupplierNo, @SupplierGroupId, @TaxNo, @ContactPerson, @Email, @Phone, @Fax, @CreditTerm, @Address1, @Address2, @Address3,
				@City, @CountryId, @ZipCode, @Remark, 0, @dateNow, @dateNow, @Username, @Username);
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Supplier]
			SET   
				SupplierName = @SupplierName, SupplierNo = @SupplierNo, SupplierGroupId = @SupplierGroupId, TaxNo = @TaxNo, ContactPerson = @ContactPerson, 
				Email = @Email, Phone = @Phone, Fax = @Fax, CreditTerm = @CreditTerm, Address1 = @Address1, Address2 = @Address2, Address3 = @Address3, City = @City,
				CountryId = @CountryId, ZipCode = @ZipCode, Remark = @Remark, IsDisabled = 0, DateModified = @dateNow, UpdatedBy = @Username
			WHERE 
				SupplierId = @SupplierId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Supplier]
			SET    
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @Username
			WHERE  
				SupplierId = @SupplierId
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO