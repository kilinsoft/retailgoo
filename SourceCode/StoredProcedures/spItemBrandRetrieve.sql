USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemBrandRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemBrandRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemBrandRetrieve]
	@BusinessId BIGINT,
	@ItemBrandId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ib.*
	from ItemBrand ib
	where
		ib.ItemBrandId = @ItemBrandId
		AND ib.BusinessId = @BusinessId
		AND	ib.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;