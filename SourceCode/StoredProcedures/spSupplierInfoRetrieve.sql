USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSupplierInfoRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSupplierInfoRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSupplierInfoRetrieve]
    @BusinessId BIGINT,
	@SupplierId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT TOP 1 * 
	FROM [Supplier]
	WHERE BusinessId = @BusinessId
		AND SupplierId = @SupplierId
		AND IsDeleted <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;