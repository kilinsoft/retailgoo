USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCustomerRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCustomerRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCustomerRetrieve]
    @CustomerId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.*, sg.CustomerGroupName, c.CountryName AS CountryBillingName, c2.CountryName AS CountryShippingName
	from Customer s
	left join Country c
		on s.CountryBillingId = c.CountryId
	left join Country c2
	on s.CountryShippingId = c2.CountryId
	left join CustomerGroup sg
		on s.CustomerGroupId = sg.CustomerGroupId
	where 
		s.CustomerId = @CustomerId

SET @ReturnValue = 0;
RETURN @ReturnValue;