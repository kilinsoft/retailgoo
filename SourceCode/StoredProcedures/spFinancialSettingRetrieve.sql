USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spFinancialSettingRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spFinancialSettingRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spFinancialSettingRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1 fs.*, c.CurrencyName, dp.DefaultPriceTypeName, stt.SalesTaxTypeName, ct.CostingTypeName
	FROM [FinancialSetting] fs
	INNER JOIN [Currency] c
		ON fs.CurrencyId = c.CurrencyId
	INNER JOIN [DefaultPriceType] dp
		ON fs.DefaultPriceTypeId = dp.DefaultPriceTypeId
	INNER JOIN [SalesTaxType] stt
		ON fs.SalesTaxTypeId = stt.SalesTaxTypeId
	INNER JOIN [CostingType] ct
		ON fs.CostingTypeId = ct.CostingTypeId
	WHERE BusinessId = @BusinessId

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO