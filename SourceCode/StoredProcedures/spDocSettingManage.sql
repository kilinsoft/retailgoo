USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spDocSettingManage')
   EXEC('CREATE PROCEDURE [dbo].[spDocSettingManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spDocSettingManage]
	@DocSettingId bigint,
	@BusinessId bigint,
	@SalesOrderFormat nvarchar(50),
	@SalesOrderNo int,
	@SalesOrderTemplate tinyint,
	@InvoiceFormat nvarchar(50),
	@InvoiceNo int,
	@InvoiceTemplate tinyint,
	@ReceiptFormat nvarchar(50),
	@ReceiptNo int,
	@ReceiptTemplate tinyint,
	@StockAdjFormat nvarchar(50),
	@StockAdjNo int,
	@StockAdjTemplate tinyint,
	@ReceiveOrderFormat nvarchar(50),
	@ReceiveOrderNo int,
	@ReceiveOrderTemplate tinyint,
	@CashPurchaseFormat nvarchar(50),
	@CashPurchaseNo int,
	@CashPurchaseTemplate tinyint,
	@CashSalesFormat nvarchar(50),
	@CashSalesNo int,
	@CashSalesTemplate tinyint,
	@PurchaseOrderFormat nvarchar(50),
	@PurchaseOrderNo int,
	@PurchaseOrderTemplate tinyint,
	@PaymentFormat nvarchar(50),
	@PaymentNo int,
	@PaymentTemplate tinyint,
	@ExpenseFormat nvarchar(50),
	@ExpenseNo int,
	@ExpenseTemplate tinyint,
    @UserId bigint,
	@Version datetime,
	@ItemState char(1)
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[DocSetting]
			SET
			-- Do not update DocumentNumber
			--[SalesOrderFormat] = @SalesOrderFormat, [SalesOrderNo] = @SalesOrderNo, [SalesOrderTemplate] = @SalesOrderTemplate,
			--[InvoiceFormat] = @InvoiceFormat, [InvoiceNo] = @InvoiceNo, [InvoiceTemplate] = @InvoiceTemplate,
			--[ReceiptFormat] = @ReceiptFormat, [ReceiptNo] = @ReceiptNo, [ReceiptTemplate] = @ReceiptTemplate,
			--[StockAdjFormat] = @StockAdjFormat, [StockAdjNo] = @StockAdjNo, [StockAdjTemplate] = @StockAdjTemplate,
			--[ReceiveOrderFormat] = @ReceiveOrderFormat, [ReceiveOrderNo] = @ReceiveOrderNo, [ReceiveOrderTemplate] = @ReceiveOrderTemplate,
			--[CashPurchaseFormat] = @CashPurchaseFormat, [CashPurchaseNo] = @CashPurchaseNo, [CashPurchaseTemplate] = @CashPurchaseTemplate,
			--[CashSalesFormat] = @CashSalesFormat, [CashSalesNo] = @CashSalesNo, [CashSalesTemplate] = @CashSalesTemplate,
			--[PurchaseOrderFormat] = @PurchaseOrderFormat, [PurchaseOrderNo] = @PurchaseOrderNo, [PurchaseOrderTemplate] = @PurchaseOrderTemplate,
			--[PaymentFormat] = @PaymentFormat, [PaymentNo] = @PaymentNo, [PaymentTemplate] = @PaymentTemplate,
			--[ExpenseFormat] = @ExpenseFormat, [ExpenseNo] = @ExpenseNo, [ExpenseTemplate] = @ExpenseTemplate,
			--[DateModified] = @dateNow, [UpdatedBy] = @username
			[SalesOrderFormat] = @SalesOrderFormat, [SalesOrderTemplate] = @SalesOrderTemplate,
			[InvoiceFormat] = @InvoiceFormat, [InvoiceTemplate] = @InvoiceTemplate,
			[ReceiptFormat] = @ReceiptFormat,  [ReceiptTemplate] = @ReceiptTemplate,
			[StockAdjFormat] = @StockAdjFormat, [StockAdjTemplate] = @StockAdjTemplate,
			[ReceiveOrderFormat] = @ReceiveOrderFormat, [ReceiveOrderTemplate] = @ReceiveOrderTemplate,
			[CashPurchaseFormat] = @CashPurchaseFormat, [CashPurchaseTemplate] = @CashPurchaseTemplate,
			[CashSalesFormat] = @CashSalesFormat, [CashSalesTemplate] = @CashSalesTemplate,
			[PurchaseOrderFormat] = @PurchaseOrderFormat, [PurchaseOrderTemplate] = @PurchaseOrderTemplate,
			[PaymentFormat] = @PaymentFormat, [PaymentTemplate] = @PaymentTemplate,
			[ExpenseFormat] = @ExpenseFormat, [ExpenseTemplate] = @ExpenseTemplate,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[DocSettingId] = @DocSettingId
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO