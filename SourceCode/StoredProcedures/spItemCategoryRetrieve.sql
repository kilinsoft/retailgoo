USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemCategoryRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemCategoryRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemCategoryRetrieve]
	@BusinessId BIGINT,
	@ItemCategoryId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ic.*
	from ItemCategory ic
	where
		ic.ItemCategoryId = @ItemCategoryId
		AND ic.BusinessId = @BusinessId
		AND	ic.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;