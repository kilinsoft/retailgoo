USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spRoleListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spRoleListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spRoleListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		BusinessRoleShortName as Value,
		BusinessRoleName as Text
	FROM [System_BusinessRole];

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO