USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCurrentStockRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCurrentStockRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCurrentStockRetrieve]
    @BusinessId bigint,
	@ItemGroupId bigint NULL,
	@ItemBrandId bigint NULL,
	@ItemCategoryId bigint NULL
AS
	DECLARE @ReturnValue INT;
	DECLARE @sqlStr AS NVARCHAR(MAX);

	SELECT 
		CASE
			WHEN ig.ItemGroupCode = '' THEN ig.ItemGroupId
			ELSE ig.ItemGroupCode
		END AS ItemGroupCode,
		ic.ItemCategoryName,
		ib.ItemBrandName,
		SUM(i.Quantity) AS Quantity
	FROM [ItemGroup] ig
		INNER JOIN [Item] i 
			ON ig.ItemGroupId = i.ItemGroupId
		LEFT JOIN [ItemCategory] ic
			ON ig.ItemCategoryId = ic.ItemCategoryId
		LEFT JOIN [ItemBrand] ib
			ON ig.ItemBrandId = ib.ItemBrandId
		WHERE	ig.BusinessId = @BusinessId 
			AND	( (ig.ItemGroupId = @ItemGroupId OR @ItemGroupId IS NULL) AND
					(ig.ItemBrandId = @ItemBrandId OR @ItemBrandId IS NULL) AND
					(ig.ItemCategoryId = @ItemCategoryId OR @ItemCategoryId IS NULL) )
		GROUP BY	ig.ItemGroupCode, ig.ItemGroupId, ib.ItemBrandName, ic.ItemCategoryName;

	--SET @sqlStr = 'SELECT ig.ItemGroupCode, ic.ItemCategoryName, ib.ItemBrandName, SUM(i.Quantity) AS Quantity';
	----SET @sqlStr = 'SELECT *, fnGetItemFullName( ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName, ig.ItemGroupName, ic.ItemCategoryName FROM [Item] i';
	--SET @sqlStr = @sqlStr + ' FROM [ItemGroup] ig';
	--SET @sqlStr = @sqlStr + ' INNER JOIN [Item] i ON ig.ItemGroupId = i.ItemGroupId';
	--SET @sqlStr = @sqlStr + ' LEFT JOIN [ItemCategory] ic on ig.ItemCategoryId = ic.ItemCategoryId';
	--SET @sqlStr = @sqlStr + ' LEFT JOIN [ItemBrand] ib on ig.ItemGroupId = ib.ItemBrandId';
	----SET @sqlStr = @sqlStr + ' WHERE ig.BusinessId = @BusinessId';
	--SET @sqlStr = @sqlStr + ' WHERE ig.BusinessId = ' + CAST(@BusinessId AS NVARCHAR);

	--IF (@ItemGroupId IS NOT NULL) BEGIN
	--	SET @sqlStr = @sqlStr + ' AND ig.ItemGroupId = ' + CAST(@ItemGroupId AS NVARCHAR);
	--END

	--IF (@ItemBrandId IS NOT NULL) BEGIN
	--	SET @sqlStr = @sqlStr + ' AND ib.ItemBrandId = ' + CAST(@ItemBrandId AS NVARCHAR);
	--END

	--IF (@ItemCategoryId IS NOT NULL) BEGIN
	--	SET @sqlStr = @sqlStr + ' AND ic.ItemCategoryId = ' + CAST(@ItemCategoryId AS NVARCHAR);
	--END

	--SET @sqlStr = @sqlStr + ' GROUP BY ig.ItemGroupCode, ic.ItemCategoryName, ib.ItemBrandName';

	--EXECUTE sp_ExecuteSQL @sqlStr

SET @ReturnValue = 0;
RETURN @ReturnValue;