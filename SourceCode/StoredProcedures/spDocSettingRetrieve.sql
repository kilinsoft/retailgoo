USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spDocSettingRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spDocSettingRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spDocSettingRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1 * 
	FROM [DocSetting]
	WHERE BusinessId = @BusinessId

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO