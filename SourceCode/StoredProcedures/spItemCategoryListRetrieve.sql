USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemCategoryListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemCategoryListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemCategoryListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ItemCategoryId AS Value, 
		ItemCategoryName As Text 
	from 
		ItemCategory
	where 
			BusinessId = @BusinessId
		and IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;