USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemBrandItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemBrandItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemBrandItemAllRetrieve]
	@BusinessId	BIGINT,
	@ItemBrandId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, ic.ItemCategoryName, sum(i.Quantity) as ItemQuantity, ig.DateModified
	from Item i
	left join ItemGroup ig
		on ig.ItemGroupId = i.ItemGroupId
	left join ItemCategory ic
		on ic.ItemCategoryId = ig.ItemCategoryId
	full outer join ItemBrand ib
		on ib.ItemBrandId = ig.ItemBrandId
	where
		ig.ItemBrandId = @ItemBrandId
		AND ig.BusinessId = @BusinessId
		AND	ig.IsDisabled <> 1
	group by ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, ig.DateModified, ic.ItemCategoryName

SET @ReturnValue = 0;
RETURN @ReturnValue;
