USE [retailgoo]
GO
IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tSalesInvoiceItem')
   EXEC sp_droptype tSalesInvoiceItem
GO
CREATE TYPE dbo.tSalesInvoiceItem AS TABLE (
	[SalesInvoiceItemId] [bigint] NOT NULL,
	[SalesInvoiceId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
