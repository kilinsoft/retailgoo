USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetUsername]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnIsPurchaseOrderEditable', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnIsPurchaseOrderEditable];
GO
CREATE FUNCTION [dbo].[fnIsPurchaseOrderEditable](@PurchaseOrderId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void PurchaseOrder item that has been created by CashPurchase. Need to do it from CashPurchase screen.
	SELECT TOP 1 @blockingItem = po.CashPurchaseNo
	FROM [PurchaseOrder] po
	WHERE	po.PurchaseOrderId = @PurchaseOrderId
		AND po.IsCashPurchase = 1;

	IF @blockingItem IS NULL
		BEGIN
			-- The PurchaseOrder item cannot be edited if it has been referred to by any active ReceiveOrder item and that ReceiveOrder item
			-- has also been refered to by active OutgoingPayment item.
			SELECT @blockingItem = ro.ReceiveOrderId
			FROM [ReceiveOrder] ro
			INNER JOIN [ReceiveOrderItem] roi
				ON ro.ReceiveOrderId = roi.ReceiveOrderId
					AND ro.IsDisabled <> 1
					AND ro.Status <> 'Void'
					AND roi.IsDisabled <> 1
			INNER JOIN [PurchaseOrderItem] poi
				ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
					AND poi.IsDisabled <> 1
					AND poi.PurchaseOrderId = @PurchaseOrderId
			INNER JOIN [OutgoingPaymentItem] opi
				ON opi.ReceiveOrderId = ro.ReceiveOrderId
					AND opi.IsDisabled <> 1
			INNER JOIN [OutgoingPayment] op
				ON opi.OutgoingPaymentId = op.OutgoingPaymentId
					AND op.IsDisabled <> 1
					AND op.Status <> 'Void';

			IF @blockingItem IS NULL
				SET @blockingItem = N'';
		END

	return @blockingItem;
END
