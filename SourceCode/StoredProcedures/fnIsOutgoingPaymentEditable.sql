USE [retailgoo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnIsOutgoingPaymentEditable', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnIsOutgoingPaymentEditable];
GO
CREATE FUNCTION [dbo].[fnIsOutgoingPaymentEditable](@OutgoingPaymentId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void OutgoingPayment item that has been created by CashPurchase. Need to do it from CashPurchase screen.
	SELECT TOP 1 @blockingItem = po.CashPurchaseNo
	FROM [PurchaseOrder] po
	INNER JOIN [PurchaseOrderItem] poi
		ON	po.PurchaseOrderId = poi.PurchaseOrderId
			AND poi.IsDisabled <> 1
	INNER JOIN [ReceiveOrderItem] roi
		ON	roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
			AND roi.IsDisabled <> 1
	INNER JOIN [ReceiveOrder] ro
		ON	roi.ReceiveOrderId = ro.ReceiveOrderId
			AND ro.IsDisabled <> 1
			AND ro.Status <> 'Void'
	INNER JOIN [OutgoingPaymentItem] opi
		ON	ro.ReceiveOrderId = opi.ReceiveOrderId
			AND opi.IsDisabled <> 1
			AND opi.OutgoingPaymentId = @OutgoingPaymentId
	WHERE	po.IsCashPurchase = 1;

	IF @blockingItem IS NULL
		SET @blockingItem = N'';

	return @blockingItem;
END
