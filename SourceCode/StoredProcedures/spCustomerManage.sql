USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCustomerManage')
   EXEC('CREATE PROCEDURE [dbo].[spCustomerManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCustomerManage]
	@BusinessId bigint,
	@CustomerId bigint,
	@CustomerName nvarchar(50),
	@CustomerNo nvarchar(50),
	@CustomerGroupId tinyint,
	@DefaultPriceTypeId tinyint,
	@TaxNo nvarchar(50),
	@ContactPerson nvarchar(100),
	@Email varchar(320),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@CreditTerm nvarchar(max),
	@CreditLimit nvarchar(max),
	@AddressBilling1 nvarchar(100),
	@AddressBilling2 nvarchar(100),
	@AddressBilling3 nvarchar(100),
	@CityBilling nvarchar(50),
	@CountryBillingId tinyint,
	@ZipCodeBilling nvarchar(50),
	@RemarkBilling nvarchar(max),
	@AddressShipping1 nvarchar(100),
	@AddressShipping2 nvarchar(100),
	@AddressShipping3 nvarchar(100),
	@CityShipping nvarchar(50),
	@CountryShippingId tinyint,
	@ZipCodeShipping nvarchar(50),
	@RemarkShipping nvarchar(max),
	@UserId bigint,
	@ItemState char(1),
	@Version datetime
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @Username varchar(50);
	
	exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Customer]
			   ([BusinessId]
			   ,[CustomerName]
			   ,[CustomerNo]
			   ,[CustomerGroupId]
			   ,[TaxNo]
			   ,[ContactPerson]
			   ,[Email]
			   ,[Phone]
			   ,[Fax]
			   ,[CreditTerm]
			   ,[CreditLimit]
			   ,[DefaultPriceTypeId]
			   ,[AddressBilling1]
			   ,[AddressBilling2]
			   ,[AddressBilling3]
			   ,[CityBilling]
			   ,[CountryBillingId]
			   ,[ZipCodeBilling]
			   ,[RemarkBilling]
			   ,[AddressShipping1]
			   ,[AddressShipping2]
			   ,[AddressShipping3]
			   ,[CityShipping]
			   ,[CountryShippingId]
			   ,[ZipCodeShipping]
			   ,[RemarkShipping]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
				(@BusinessId, @CustomerName, @CustomerNo, @CustomerGroupId, @TaxNo, @ContactPerson, @Email, @Phone, @Fax, @CreditTerm, @CreditLimit, @DefaultPriceTypeId,
				@AddressBilling1, @AddressBilling2, @AddressBilling3, @CityBilling, @CountryBillingId, @ZipCodeBilling, @RemarkBilling, 
				@AddressShipping1, @AddressShipping2, @AddressShipping3, @CityShipping, @CountryShippingId, @ZipCodeShipping, @RemarkShipping, 
				0, @dateNow, @dateNow, @Username, @Username);
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Customer]
			SET   
				CustomerName = @CustomerName, CustomerNo = @CustomerNo, CustomerGroupId = @CustomerGroupId, TaxNo = @TaxNo, ContactPerson = @ContactPerson, 
				Email = @Email, Phone = @Phone, Fax = @Fax, CreditTerm = @CreditTerm, CreditLimit = @CreditLimit, DefaultPriceTypeId = @DefaultPriceTypeId,
				AddressBilling1 = @AddressBilling1, AddressBilling2 = @AddressBilling2, AddressBilling3 = @AddressBilling3, CityBilling = @CityBilling, 
				CountryBillingId = @CountryBillingId, ZipCodeBilling = @ZipCodeBilling, RemarkBilling = @RemarkBilling, 
				AddressShipping1 = @AddressShipping1, AddressShipping2 = @AddressShipping2, AddressShipping3 = @AddressShipping3, CityShipping = @CityShipping, 
				CountryShippingId = @CountryShippingId, ZipCodeShipping = @ZipCodeShipping, RemarkShipping = @RemarkShipping, 
				IsDisabled = 0, DateModified = @dateNow, UpdatedBy = @Username
			WHERE 
				CustomerId = @CustomerId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Customer]
			SET    
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @Username
			WHERE  
				CustomerId = @CustomerId
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO