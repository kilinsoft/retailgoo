USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spUserCreate')
   EXEC('CREATE PROCEDURE [dbo].[spUserCreate] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spUserCreate]
	@Username varchar(254),
	@Password varbinary(50),
	@Salt varbinary(50),
	@Email varchar(254),
	@Guid varchar(50),
	@DateGuidExpired datetime,
	@IsDisabled bit,
	@IsFacebookUser bit
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @newUserId BIGINT;
		SET @newUserId = -1;
	
	INSERT INTO [dbo].[User]
			([Username]
			,[Password]
			,[Salt]
			,[Email]
			,[Guid]
			,[DateGuidExpired]
			,[IsDisabled]
			,[IsFacebookUser]
			,[DateCreated]
			,[DateModified])
		VALUES
			(@Username, @Password, @Salt, @Email, @Guid, @DateGuidExpired, 
			@IsDisabled, @IsFacebookUser, @dateNow, @dateNow)
	SET @newUserId = SCOPE_IDENTITY();

	-- Create the user profile
	INSERT INTO [dbo].[UserProfile]
           ([UserId]
		   ,[Title]
           ,[Firstname]
           ,[Lastname]
           ,[Birthdate]
           ,[Gender]
           ,[Identification]
           ,[Phone]
           ,[Fax]
           ,[FacebookId]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[DateCreated]
           ,[DateModified])
     VALUES
           (@newUserId, NULL, '', '', NULL, NULL, '', '', ''
		   ,'', '', '', '', '', NULL, '', @dateNow, @dateNow )

COMMIT TRANSACTION;

RETURN @newUserId;

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO