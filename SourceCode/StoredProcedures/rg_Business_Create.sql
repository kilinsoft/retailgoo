USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'rg_Business_Create')
   EXEC('CREATE PROCEDURE [dbo].[rg_Business_Create] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[rg_Business_Create]
    @Username VARCHAR(50),
    @Password VARBINARY(50),
    @Salt VARBINARY(16),
    @Email VARCHAR(320),
    @Guid VARCHAR(50),
    @GuidExpired DATE,
    @IsActive BIT,
    @IsFacebookUser BIT,
    @ReturnValue INT OUT
    /*
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [dbo].[business_name] NOT NULL,
	[creator] [dbo].[ref_name] NOT NULL,
	[type] [tinyint] NOT NULL,
	[logo] [dbo].[path] NOT NULL,
	[address1] [dbo].[address] NOT NULL,
	[address2] [dbo].[address] NOT NULL,
	[address3] [dbo].[address] NOT NULL,
	[city] [dbo].[name] NOT NULL,
	[country] [dbo].[country] NOT NULL,
	[zip_code] [dbo].[postal] NOT NULL,
	[phone] [dbo].[phone] NOT NULL,
	[fax] [dbo].[phone] NOT NULL,
	[email] [dbo].[email] NOT NULL,
	[website] [dbo].[url] NOT NULL,
	[facebook_url] [dbo].[url] NOT NULL,
	[tax_id] [varchar](50) NOT NULL,
	[date_created] [datetime] NOT NULL,
	[date_modified] [datetime] NULL,
    */
AS
	BEGIN TRANSACTION
	BEGIN TRY
		SET NOCOUNT ON;
		-- Check for existing username
		SELECT 1 
		FROM [user] 
		WHERE username = @Username;
		IF @@ROWCOUNT <> 0
		BEGIN
			RAISERROR( 'Username already exists', 16, 1 );
		END;
		-- Check for existing email
		SELECT 1 
		FROM [user] 
		WHERE email = @Email;
		IF @@ROWCOUNT <> 0
		BEGIN
			RAISERROR( 'Email already exists', 16, 2 );
		END;
		
		DECLARE @UserID INT;
		DECLARE @DateCreated DATE, @DateModified DATE;
		SET @DateCreated = GETDATE();
		SET @DateModified = @DateCreated;
		-- Insert new record into [user]
		INSERT INTO [user] (
			[username],[password],[salt],[email],[guid],[guid_expired],[is_active],[is_facebook_user],
			[initial],[name],[surname],[birthdate],[gender],[phone],[fax],[website],[facebook_url],
			[address1],[address2],[address3],[city],[country],[zip_code],[date_created],[date_modified])
		VALUES (
			@Username, @Password, @Salt, @Email, @Guid, @GuidExpired, @IsActive, @IsFacebookUser, 
			'', '', '', null, '', '', '', '', '', 
			'', '', '', '', '', '', @DateCreated, @DateModified );
		/*
		INSERT INTO [user] (
			[username],[password],[salt],[email],[guid],[guid_expired],[is_active],[is_facebook_account],
			[initial],[name],[surname],[birthdate],[gender],[phone],[fax],[website],[facebook_url],
			[address1],[address2],[address3],[city],[country],[zip_code],[date_created],[date_modified])
		VALUES (
			@Username, @Password, @Salt, @Email, @Guid, @GuidExpired, @IsActive, @IsFacebookAccount, 
			@Initial, @Name, @Surname, @Birthdate, @Gender, @Phone, @Fax, @Website, @FacebookUrl, 
			@Address1, @Address2, @Address3, @City, @Country, @ZipCode, @DateCreated, @DateModified );
		*/
		/*
		SET @UserID = SCOPE_IDENTITY();
		-- Insert new record into [user_info]
		INSERT INTO [user_info] (
			[user_id],[initial],[name],[surname],[birthdate],[gender],[phone],[fax],[website],[facebook_url],
			[address1],[address2],[address3],[city],[country],[zip_code],[date_created],[date_modified])
		VALUES (
			@UserID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, @DateCreated, @DateModified);
		*/

	COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = 1; /* Failure */
		ROLLBACK TRANSACTION;
		DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
		SET @ErrMsg = ERROR_MESSAGE();
		SET @ErrServ = ERROR_SEVERITY();
		SET @ErrState = ERROR_STATE();
		RAISERROR( @ErrMsg, @ErrServ, @ErrState );
	END CATCH;

SELECT 0 AS ReturnValue;
SET @ReturnValue = 0; /* Success */
