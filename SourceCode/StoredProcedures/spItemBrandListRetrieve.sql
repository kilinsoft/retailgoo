USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemBrandListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemBrandListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemBrandListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ItemBrandId AS Value, 
		ItemBrandName As Text 
	from 
		ItemBrand
	where 
			BusinessId = @BusinessId
		and IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;