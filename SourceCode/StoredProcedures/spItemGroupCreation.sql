USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemGroupCreation')
   EXEC('CREATE PROCEDURE [dbo].[spItemGroupCreation] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemGroupCreation]
	@BusinessId bigint,
    @ItemGroupName nvarchar(50),
	@ItemGroupCode nvarchar(50),
	@ItemName nvarchar(100),
    @ItemCategoryName nvarchar(100) = null,
    @ItemBrandName nvarchar(100) = null,
	@Quantity decimal(18,4) = 1,
    @Cost decimal(18,4) = 1,
    @Price decimal(18,4) = 1,
    @Uom nvarchar(50) = 1,
    @IsForSales bit = TRUE,
	@ItemId bigint = -1 out,
    @UserId bigint,
	@ItemState char(1),
	@Version datetime
AS
BEGIN TRY

	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	declare @dateNow datetime;
		set @dateNow = getdate();
	declare @username varchar(50);
		exec @username = GetUsername @UserId = @UserId;
	declare @targetItemGroupId bigint;
		set @targetItemGroupId = null;
	--declare @targetItemBrandId bigint;
	--	set @targetItemBrandId = null;
	--declare @targetItemCategoryId bigint;
	--	set @targetItemCategoryId = null;

	IF @ItemState = 'N'
		--SELECT TOP 1 @targetItemCategoryId = ItemCategoryId
		--FROM ItemCategory
		--WHERE BusinessId = @BusinessId
		--	AND ItemCategoryName = @ItemCategoryName;
		--IF @@ROWCOUNT = 0
		--BEGIN
		--	---- Insert ItemsCategory
		--	INSERT INTO ItemCategory
		--		([BusinessId]
		--		,[ItemCategoryName]
		--		,[IsDisabled]
		--		,[DateCreated]
		--		,[DateModify]
		--		,[CreatedBy]
		--		,[UpdatedBy])
		--	VALUES 
		--	( 
		--		@BusinessId, @ItemCategoryName, 0,
		--		@dateNow, @dateNow, @username, @username
		--	);
		--	SET @targetItemCategoryId = SCOPE_IDENTITY();
		--END

		--SELECT TOP 1 @targetItemBrandId = ItemBrandId
		--FROM ItemBrand
		--WHERE BusinessId = @BusinessId
		--	AND ItemBrandName = @ItemBrandName;
		--IF @@ROWCOUNT = 0
		--BEGIN
		--	---- Insert ItemsBrand
		--	INSERT INTO ItemBrand
		--		([BusinessId]
		--		,[ItemBrandName]
		--		,[IsDisabled]
		--		,[DateCreated]
		--		,[DateModified]
		--		,[CreatedBy]
		--		,[UpdatedBy])
		--	VALUES 
		--	( 
		--		@BusinessId, @ItemBrandName, 0,
		--		@dateNow, @dateNow, @username, @username
		--	);
		--	SET @targetItemBrandId = SCOPE_IDENTITY();
		--END

		--SELECT 1 
		--FROM ItemGroup
		--WHERE BusinessId = @BusinessId
		--	AND ItemCategoryId = @targetItemCategoryId
		--	AND ItemBrandId = @targetItemBrandId
		--	AND ItemGroupName = @ItemGroupName;
		--IF @@ROWCOUNT <> 0
		--BEGIN
		--	RAISERROR( 'Itemname already exists', 16, 1 );
		--	RETURN 1;
		--END

		SELECT TOP 1 @targetItemGroupId = ItemGroupId
		FROM ItemGroup
		WHERE BusinessId = @BusinessId
			AND ItemGroupName = @ItemGroupName
			AND ItemGroupCode = @ItemGroupCode;
		IF @@ROWCOUNT = 0
		BEGIN
			---- Insert ItemsGroup
			INSERT INTO ItemGroup
				([BusinessId]
				,[ItemGroupCode]
				,[ItemGroupName]
				,[ImagePath]
				,[ItemCategoryId]
				,[ItemBrandId]
				,[Cost]
				,[Price]
				,[Uom]
				,[IsForSales]
				,[IsDisabled]
				,[DateCreated]
				,[DateModified]
				,[CreatedBy]
				,[UpdatedBy])
			VALUES 
			( 
				@BusinessId, @ItemGroupCode, @ItemGroupName, '',
				null, null, 
				@Cost, @Price, @Uom, @IsForSales, 0, 
				@dateNow, @dateNow, @username, @username
			);
			SET @targetItemGroupId = SCOPE_IDENTITY();
		END

		SELECT TOP 1 *
		FROM Item
		WHERE ItemGroupId = @targetItemGroupId
			AND ItemName = @ItemName;
		IF @@ROWCOUNT <> 0
		BEGIN
			RAISERROR( 'Itemname already exists', 16, 1 );
			RETURN 1;
		END
		ELSE
		BEGIN
			---- Insert Items
			INSERT INTO Item
				([ItemGroupId]
				,[ItemName]
				,[Barcode]
				,[Quantity]
				,[Cost]
				,[Price]
				,[Price02]
				,[Price03]
				,[Price04]
				,[IsForSales]
				,[IsDisabled]
				,[DateCreated]
				,[DateModified]
				,[CreatedBy]
				,[UpdatedBy])
			VALUES
			(
				@targetItemGroupId, @ItemName, '',
				@Quantity, @Cost, @Price, 0, 0, 0,
				@IsForSales, 0,
				@dateNow, @dateNow, @username, @username 
			);
		END

		SET @ItemId = SCOPE_IDENTITY();

		SET @ReturnValue = 0;
		RETURN @ReturnValue;
END TRY
BEGIN CATCH

	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO