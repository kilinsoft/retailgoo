USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSetImagePath')
   EXEC('CREATE PROCEDURE [dbo].[spSetImagePath] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSetImagePath]
    @BusinessId bigint,
	@ItemGroupId bigint,
	@ImagePath nvarchar(MAX),
	@ItemState char(1),
	@UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'M'
	BEGIN
		UPDATE [dbo].[ItemGroup]
			SET [ImagePath] = @ImagePath
				,[DateModified] = @dateNow
				,[UpdatedBy] = @username
			WHERE [BusinessId] = @BusinessId
				AND [ItemGroupId] = @ItemGroupId
	END

SET @ReturnValue = 0;
RETURN @ReturnValue;