IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tBillPaymentItem')
   EXEC sp_droptype tBillPaymentItem
GO
CREATE TYPE tBillPaymentItem AS TABLE (
	[BillPaymentItemId] [bigint] NOT NULL,
	[BillId] [bigint] NOT NULL,
	[PaymentTypeId] [tinyint] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
