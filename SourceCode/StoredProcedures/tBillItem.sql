IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tBillItem')
   EXEC sp_droptype tBillItem
GO
CREATE TYPE tBillItem AS TABLE (
	[BillItemId] [bigint] NOT NULL,
	[BillId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
