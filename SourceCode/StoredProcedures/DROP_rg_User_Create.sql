﻿CREATE PROCEDURE rg_Credential_Create
    @Username VARCHAR(50),
    @Password VARCHAR(50),
    @Salt CHAR(10),
    @Guid VARCHAR(50),
    @GuidExpired DATE,
    @IsActive BIT,
    @IsFacebookAccount BIT,
    /*
    @Initial CHAR(4),
    @Name NVARCHAR(50),
    @Surname NVARCHAR(50),
    @Birthdate DATE,
    @Gender CHAR(1),
    @Phone NVARCHAR(50),
    @Fax NVARCHAR(50),
    @Email NVARCHAR(100),
    @Website NVARCHAR(MAX),
    @FacebookUrl NVARCHAR(MAX),
    @Address1 NVARCHAR(100),
    @Address2 NVARCHAR(100),
    @Address3 NVARCHAR(100),
    @City NVARCHAR(50),
    @Country NCHAR(5),
    @ZipCode NVARCHAR(50),
    */
    @Version DATE OUT,
    @ID INT OUT
AS
	BEGIN TRY

	SET NOCOUNT ON;

	BEGIN TRANSACTION
		-- Check for existing username
		SELECT 1 
		FROM [user] 
		WHERE username = @Username;
		-- Return Error if the username already exists
		IF @@ROWCOUNT <> 0
		BEGIN
			RAISERROR( 'Username already exists', 16, 1 );
			RETURN 1;
		END;
		
		DECLARE @RetVersion DATE, @RetID INT;
		DECLARE @DateCreated DATE, @DateModified DATE;
		SET @DateCreated = GETDATE();
		SET @DateModified = @DateCreated;
		
		INSERT INTO [credential] (username, password, salt, guid, guid_expired, is_active, is_facebook_account, date_created, date_modified) VALUES ( @Username, @Password, @Salt, @Guid, @GuidExpired, @IsActive, @IsFacebookAccount, @DateCreated, @DateModified );
		
		SET @RetVersion = @DateModified;
		SET @RetID = SCOPE_IDENTITY();
		
	COMMIT TRANSACTION;
	
	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
		SET @ErrMsg = ERROR_MESSAGE();
		SET @ErrServ = ERROR_SEVERITY();
		SET @ErrState = ERROR_STATE();
		RAISERROR( @ErrMsg, @ErrServ, @ErrState )
		RETURN 1;
	END CATCH;
	
	RETURN 0;

