USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spOutgoingPaymentVoid')
   EXEC('CREATE PROCEDURE [dbo].[spOutgoingPaymentVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spOutgoingPaymentVoid]
	@OutgoingPaymentId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	SET NOCOUNT ON;

	-- 1. Check if this item is voidable
	DECLARE @blockingItem NVARCHAR(100);
		SET @blockingItem = dbo.fnIsOutgoingPaymentEditable(@OutgoingPaymentId);
	DECLARE @ErrTxt NVARCHAR(MAX);

	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId

	-- 2. VOID the item
	UPDATE [dbo].[OutgoingPayment]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE OutgoingPaymentId = @OutgoingPaymentId;


	-- 3. Update corresponding ReceiveOrder.Status and ReceiveOrder.AmountPaid
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT result.ReceiveOrderId FROM (
		SELECT DISTINCT ro.ReceiveOrderId
		FROM [ReceiveOrder] ro
		INNER JOIN [OutgoingPaymentItem] opi
			ON ro.ReceiveOrderId = opi.ReceiveOrderId
				AND ro.IsDisabled <> 1
				AND ro.Status <> 'Void'
		WHERE opi.OutgoingPaymentId = @OutgoingPaymentId
	) result;

	EXEC dbo.spReceiveOrderStatusUpdate @updateItemList;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;