USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spIncomingPaymentAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spIncomingPaymentAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spIncomingPaymentAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT ip.*,
			cust.CustomerName
	FROM [IncomingPayment] ip
	LEFT JOIN [Customer] cust
		ON ip.CustomerId = cust.CustomerId
	WHERE ip.BusinessId = @BusinessId
		AND ip.IsDisabled <> 1
		AND ip.IsCashSales <> 1
	ORDER BY ip.IncomingPaymentId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;


GO