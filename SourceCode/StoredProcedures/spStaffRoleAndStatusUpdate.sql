USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spStaffRoleAndStatusUpdate')
   EXEC('CREATE PROCEDURE [dbo].[spStaffRoleAndStatusUpdate] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spStaffRoleAndStatusUpdate]
	@Items tStaffRoleAndStatusItem READONLY,
	@UserId BIGINT
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
	   EXEC @username = GetUsername @UserId = @UserId;
	
	-- Modify for Item with ItemState='M'
	UPDATE staff
	SET	staff.Role = i.Role,
		staff.Status = i.Status,
		staff.DateModified = @dateNow,
		staff.UpdatedBy = @username
	FROM [dbo].[Staff] staff
		INNER JOIN	@Items as i
			ON staff.StaffId = i.StaffId
		WHERE	i.ItemState = 'M';
	
	-- Disable Staff for Item with ItemState='D'
	UPDATE staff
	SET	staff.IsDisabled = 1,
		staff.DateModified = @dateNow,
		staff.UpdatedBy = @username
	FROM [dbo].[Staff] staff
		INNER JOIN	@Items as i
			ON staff.StaffId = i.StaffId
		WHERE	i.ItemState = 'D';

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO