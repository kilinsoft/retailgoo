USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spAmountTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spAmountTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spAmountTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select AmountTypeId AS Value, AmountTypeName AS Text
	from AmountType

SET @ReturnValue = 0;
RETURN @ReturnValue;