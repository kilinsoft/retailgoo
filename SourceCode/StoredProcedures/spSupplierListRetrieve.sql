USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSupplierListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSupplierListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSupplierListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT s.SupplierId AS Value, s.SupplierName AS Text
	FROM [dbo].[Supplier] s
	WHERE
		s.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;