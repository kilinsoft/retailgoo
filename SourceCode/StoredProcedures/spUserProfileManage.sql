USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spUserProfileManage')
   EXEC('CREATE PROCEDURE [dbo].[spUserProfileManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spUserProfileManage]
	@UserId bigint,
	@Title varchar(4),
    @Firstname nvarchar(50),
    @Lastname nvarchar(50),
    @Birthdate date,
    @Gender char(1),
	@Identification nvarchar(50),
    @Phone nvarchar(50),
    @Fax nvarchar(50),
    @FacebookId varchar(50),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @City nvarchar(50),
    @CountryId tinyint,
    @ZipCode nvarchar(50),
	@ItemState varchar(1)
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[UserProfile]
				   ([UserId]
				   ,[Title]
				   ,[Firstname]
				   ,[Lastname]
				   ,[Birthdate]
				   ,[Gender]
				   ,[Identification]
				   ,[Phone]
				   ,[Fax]
				   ,[FacebookId]
				   ,[Address1]
				   ,[Address2]
				   ,[Address3]
				   ,[City]
				   ,[CountryId]
				   ,[ZipCode]
				   ,[DateCreated]
				   ,[DateModified])
			 VALUES
				   (@UserId, @Title, @Firstname, @Lastname, @Birthdate, @Gender, @Identification, @Phone, @Fax
				   ,@FacebookId, @Address1, @Address2, @Address3, @City, @CountryId, @ZipCode, @dateNow, @dateNow )
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[UserProfile]
			   SET [Title] = @Title
				  ,[Firstname] = @Firstname
				  ,[Lastname] = @Lastname
				  ,[Birthdate] = @Birthdate
				  ,[Gender] = @Gender
				  ,[Identification] = @Identification
				  ,[Phone] = @Phone
				  ,[Fax] = @Fax
				  ,[FacebookId] = @FacebookId
				  ,[Address1] = @Address1
				  ,[Address2] = @Address2
				  ,[Address3] = @Address3
				  ,[City] = @City
				  ,[CountryId] = @CountryId
				  ,[ZipCode] = @ZipCode
				  ,[DateModified] = @dateNow
			 WHERE UserId = @UserId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			DELETE FROM [dbo].[UserProfile]
			 WHERE UserId = @UserId
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO