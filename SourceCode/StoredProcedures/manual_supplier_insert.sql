INSERT INTO [retailgoo].[dbo].[supplier]
           ([business_id]
           ,[name]
           ,[supplier_no]
           ,[supplier_group]
           ,[tax_no]
           ,[contact]
           ,[email]
           ,[phone]
           ,[fax]
           ,[credit_term]
           ,[address1]
           ,[address2]
           ,[address3]
           ,[city]
           ,[country]
           ,[zip_code]
           ,[remark]
           ,[updated_by]
           ,[date_created]
           ,[date_modified])
     VALUES
           (1
           ,'Supplier A 1'
           ,''
           ,0
           ,''
           ,''
           ,'supplier_a1@retailgoo.com'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,'user_a1'
           ,GETDATE()
           ,GETDATE())
GO


