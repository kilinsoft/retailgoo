USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spOutgoingPaymentRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spOutgoingPaymentRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spOutgoingPaymentRetrieve]
    @OutgoingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		op.*, 
		s.SupplierName,
		pt.PaymentTypeName
	FROM [OutgoingPayment] op
	LEFT JOIN [Supplier] s
		ON op.SupplierId = s.SupplierId
	INNER JOIN [PaymentType] pt
		ON op.PaymentTypeId = pt.PaymentTypeId
	WHERE	op.OutgoingPaymentId = @OutgoingPaymentId;

SET @ReturnValue = 0;
RETURN @ReturnValue;