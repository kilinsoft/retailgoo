USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spStaffInfoRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spStaffInfoRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spStaffInfoRetrieve]
    @StaffId BIGINT,
	@Username VARCHAR(50),
	@UserEmail VARCHAR(255)
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT TOP 1 s.*, c.CountryName, c.CountryShortName, usr.Username, usr.Email AS UserEmail
	FROM [Staff] s
	LEFT JOIN dbo.[User] usr
		ON s.UserId = usr.UserId
	LEFT JOIN [Country] c
		ON s.CountryId = c.CountryId
	WHERE	(s.StaffId = @StaffId OR @StaffId is NULL)
		AND (usr.Username = @Username OR @Username is NULL)
		AND (usr.Email = @UserEmail OR @UserEmail is NULL);

SET @ReturnValue = 0;
RETURN @ReturnValue;