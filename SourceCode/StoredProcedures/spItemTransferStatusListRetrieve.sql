USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemTransferStatusListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemTransferStatusListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemTransferStatusListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		TransferStatusTypeId AS Value,
		TransferStatusTypeName AS Text
	FROM [ItemTransferStatusType]
	WHERE	IsForTransferItem = 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;