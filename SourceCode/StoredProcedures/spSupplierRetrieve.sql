USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSupplierRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSupplierRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSupplierRetrieve]
    @SupplierId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.*, sg.SupplierGroupName, c.CountryName, c.CountryShortName
	from Supplier s
	left join SupplierGroup sg
		on s.SupplierGroupId = sg.SupplierGroupId
	left join [Country] c
		on s.CountryId = c.CountryId
	where 
		s.SupplierId = @SupplierId

SET @ReturnValue = 0;
RETURN @ReturnValue;