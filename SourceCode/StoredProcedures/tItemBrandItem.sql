IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tItemBrandItem')
   EXEC sp_droptype tItemBrandItem
GO
CREATE TYPE tItemBrandItem AS TABLE (
	[ItemGroupId] [bigint] NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO