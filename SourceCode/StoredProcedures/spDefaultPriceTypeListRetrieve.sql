USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spDefaultPriceTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spDefaultPriceTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spDefaultPriceTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select d.DefaultPriceTypeId AS Value, d.DefaultPriceTypeName AS Text
	from DefaultPriceType d

SET @ReturnValue = 0;
RETURN @ReturnValue;