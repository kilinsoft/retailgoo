USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveOrderVoid')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveOrderVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveOrderVoid]
	@ReceiveOrderId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	SET @blockingItem = dbo.fnIsReceiveOrderEditable(@ReceiveOrderId);
	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	UPDATE [dbo].[ReceiveOrder]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE ReceiveOrderId = @ReceiveOrderId;

	-- Update corresponding PurchaseOrder.Status and PurchaseOrderItem.QuantityReceived
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT DISTINCT result.PurchaseOrderId AS Id FROM (
		SELECT DISTINCT po.PurchaseOrderId
			FROM [PurchaseOrder] po
			INNER JOIN [PurchaseOrderItem] poi
				ON po.PurchaseOrderId = poi.PurchaseOrderId
					AND po.IsDisabled <> 1
					AND po.Status <> 'Void'
					AND poi.IsDisabled <> 1
			INNER JOIN [ReceiveOrderItem] roi
				ON poi.PurchaseOrderItemId = roi.PurchaseOrderItemId
					AND roi.ReceiveOrderId = @ReceiveOrderId
	) result;

	EXEC dbo.spPurchaseOrderStatusUpdate @updateItemList;


	-- Update Item.Quantity
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity - roi.Quantity
	FROM [ReceiveOrderItem] roi
	WHERE	roi.IsDisabled <> 1
		AND roi.ReceiveOrderId = @ReceiveOrderId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;