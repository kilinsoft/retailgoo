USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spTest')
   EXEC('CREATE PROCEDURE [dbo].[spTest] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spTest]
	@BusinessId bigint,
	@CustomerId bigint null,
	@BillId	bigint,
    @DiscountAmount decimal(18,4),
    @DiscountPercent decimal(18,4),
    @IsDiscountPercent bit,
	@Subtotal decimal(18,4),
    @Total decimal(18,4),
	@Tax decimal(18,4),
    @AmountReceived decimal(18,4),
    @AmountChanged decimal(18,4),
	@BillItems tBillItem readonly,
	@BillPaymentItems tBillPaymentItem readonly,
	@UserId bigint,
	@ItemState char(1),
	@Version datetime,
	@NewBillId bigint OUTPUT
AS 
BEGIN TRY
BEGIN TRANSACTION

	SELECT @NewBillId = 10;	

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO