USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBillItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spBillItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBillItemAllRetrieve]
	@BillId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		bi.BillItemId, bi.BillId, bi.ItemId, bi.Quantity, bi.Discount, i.Price,
		dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName
	FROM [BillItem] bi
	INNER JOIN [Item] i
		ON bi.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	INNER JOIN [Bill] b
		ON bi.BillId = b.BillId
	WHERE	b.BillId = @BillId
		AND bi.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;