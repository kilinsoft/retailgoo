USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBillPaymentItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spBillPaymentItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBillPaymentItemAllRetrieve]
	@BillId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		bpi.*
	FROM BillPaymentItem bpi
	INNER JOIN Bill b
		ON bpi.BillId = b.BillId
	WHERE	bpi.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;