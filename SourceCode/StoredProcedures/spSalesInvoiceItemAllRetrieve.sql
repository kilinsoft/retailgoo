USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesInvoiceItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSalesInvoiceItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesInvoiceItemAllRetrieve]
	@SalesInvoiceId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT siItem.*, dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName, i.Price, si.Discount, si.SalesInvoiceId
	FROM [dbo].[SalesInvoiceItem] siItem
		, [dbo].[SalesInvoice] si
		, [dbo].[Item] i
		INNER JOIN [dbo].[ItemGroup] ig
		    ON i.ItemGroupId = ig.ItemGroupId
	WHERE siItem.SalesInvoiceId = @SalesInvoiceId
	    AND siItem.ItemId = i.ItemId
	    AND siItem.SalesInvoiceId = si.SalesInvoiceId
		AND siItem.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;
