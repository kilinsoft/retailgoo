USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesInvoiceRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSalesInvoiceRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesInvoiceRetrieve]
	@SalesInvoiceId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		si.*, 
		cust.CustomerName,
		at.AmountTypeName,
		pt.PaymentTypeName,
		dt.DestinationTypeName,
		stf.StaffId /* SalesPersonName */
	FROM [SalesInvoice] si
	LEFT JOIN [Customer] cust
		ON si.CustomerId = cust.CustomerId
	INNER JOIN [AmountType] at
		ON si.AmountTypeId = at.AmountTypeId
	INNER JOIN [PaymentType] pt
		ON si.PaymentTypeId = pt.PaymentTypeId
	INNER JOIN [DestinationType] dt
		ON si.DestinationTypeId = dt.DestinationTypeId
	LEFT JOIN [Staff] stf
		ON si.SalesPersonId = stf.StaffId
	WHERE si.SalesInvoiceId = @SalesInvoiceId


SET @ReturnValue = 0;
RETURN @ReturnValue;
