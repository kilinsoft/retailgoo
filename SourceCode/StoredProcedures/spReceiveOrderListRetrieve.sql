USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveOrderListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveOrderListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveOrderListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT
		ro.ReceiveOrderId AS Value,
		ro.ReceiveOrderNo As Text
	FROM [ReceiveOrder] ro
	WHERE BusinessId = @BusinessId
		AND (ro.Status = 'New' OR ro.Status = 'Partial')
		AND IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;