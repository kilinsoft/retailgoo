USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetUsername]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnIsSalesInvoiceEditable', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnIsSalesInvoiceEditable];
GO
CREATE FUNCTION [dbo].[fnIsSalesInvoiceEditable](@SalesInvoiceId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void SalesInvoice that has been created by CashSales. Need to do it from CashSales screen.
	SELECT @blockingItem = si.CashSalesNo
	FROM [SalesInvoice] si
	WHERE	si.IsCashSales = 1 
		AND si.SalesInvoiceId = @SalesInvoiceId;

	IF @blockingItem IS NULL
	BEGIN
		--User cannot edit SalesInvoice item if it has already been referred to by any active IncomingPayment item 
		SELECT TOP 1 @blockingItem = ip.IncomingPaymentNo
		FROM [IncomingPaymentItem] ipi
		INNER JOIN [SalesInvoice] si
			ON ipi.SalesInvoiceId = si.SalesInvoiceId
		INNER JOIN [IncomingPayment] ip
			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
		WHERE	si.SalesInvoiceId = @SalesInvoiceId
			AND si.IsDisabled <> 1
			AND si.Status <> 'Void'
			AND ip.IsDisabled <> 1
			AND ip.Status <> 'Void'
			AND ipi.IsDisabled <> 1;

		IF @blockingItem IS NULL
			SET @blockingItem = N'';
	END

	return @blockingItem;
END
