IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tIdTable')
   EXEC sp_droptype tIdTable
GO
CREATE TYPE tIdTable AS TABLE (
	[Id] [bigint] NOT NULL
);
GO
