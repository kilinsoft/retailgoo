USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCostingTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCostingTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCostingTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		CostingTypeId AS Value, 
		CostingTypeName As Text 
	from 
		CostingType

SET @ReturnValue = 0;
RETURN @ReturnValue;