USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemAdjustmentVoid')
   EXEC('CREATE PROCEDURE [dbo].[spItemAdjustmentVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemAdjustmentVoid]
	@AdjustmentId BIGINT,
	@UserId BIGINT
AS

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	UPDATE [dbo].[ItemAdjustment]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE AdjustmentId = @AdjustmentId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;