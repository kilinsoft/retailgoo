USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spStaffBusinessAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spStaffBusinessAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spStaffBusinessAllRetrieve]
    @UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT Business.BusinessId, Business.BusinessName, Business.DateExpired, Staff.Role
	FROM Staff
	INNER JOIN Business
		ON Staff.BusinessId = Business.BusinessId
	WHERE Staff.UserId = @UserId
		AND Staff.Status = 'active'
		AND Staff.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;