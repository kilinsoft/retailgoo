USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spGetNextDocId')
   EXEC('CREATE PROCEDURE [dbo].[spGetNextDocId] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spGetNextDocName]
    @BusinessId BIGINT,
    @DocType VARCHAR(50),
	@RetDocName NVARCHAR(50) OUTPUT
AS
BEGIN TRY
	DECLARE @nextName NVARCHAR(50);
	DECLARE @nextNo BIGINT;
	EXEC @nextNo = spGetNextDocId @BusinessId = @BusinessId, @Doctype = @Doctype;
	--SET @nextName = convert(NVARCHAR(50), @nextNo);

	DECLARE @colName VARCHAR(50)
	DECLARE @tmpSql NVARCHAR(MAX)
	DECLARE @tmpName NVARCHAR(50)
	SET @colName = @DocType + 'Format';
	SET @tmpSql = N'SELECT @tmpName = ' + CONVERT(NVARCHAR(MAX), @colName) + N' FROM DocSetting WHERE BusinessId = ' + CONVERT(NVARCHAR(50), @BusinessId) + N';';

	EXEC sp_executesql @tmpSql, N'@tmpName NVARCHAR(50) output', @tmpName output;

	--@tmpName
	DECLARE @numFormat NVARCHAR(50)
	DECLARE @docName NVARCHAR(50)
	DECLARE @numPart NVARCHAR(50)
	DECLARE @posStart INT
	DECLARE @posEnd INT
	DECLARE @pad NVARCHAR(50)
	SET @posStart = CHARINDEX('{', @tmpName);
	SET @posEnd = CHARINDEX('}', @tmpName) - 1;
	SET @numFormat = SUBSTRING(@tmpName, CHARINDEX('{', @tmpName) + 1, @posEnd - @posStart);
	SET @pad = REPLACE(@numFormat, 'X', '0');
	SET @numPart = RIGHT(@pad + CONVERT(NVARCHAR(50), @nextNo), LEN(@pad));
	SET @docName = REPLACE(@tmpName, N'{' + @numFormat + N'}', @numPart); 

	--SELECT @tmpName;
	--SELECT @docName AS DocumentName;

	SET @RetDocName = @docName;

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

RETURN @nextNo;