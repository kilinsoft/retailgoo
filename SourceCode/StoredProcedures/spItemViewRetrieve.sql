USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemViewRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemViewRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemViewRetrieve]
	@BusinessId BIGINT,
	@ItemId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT TOP 1 
		i.Quantity as NumRemaining,
		bi.Price as LastSalePrice
	FROM dbo.[Item] i
	LEFT JOIN BillItem bi
		ON bi.ItemId = i.ItemId
	INNER JOIN Bill b
		ON bi.BillId = b.BillId
	WHERE	i.ItemId = @ItemId
		AND bi.IsDisabled <> 1
		AND b.IsDisabled <> 1
	ORDER BY b.DateModified DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;