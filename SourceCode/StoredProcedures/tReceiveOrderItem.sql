IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tReceiveOrderItem')
   EXEC sp_droptype tReceiveOrderItem
GO
CREATE TYPE tReceiveOrderItem AS TABLE (
	[ReceiveOrderItemId] [bigint] NOT NULL,
	[ReceiveOrderId] [bigint] NOT NULL,
	[PurchaseOrderItemId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
