INSERT INTO [retailgoo].[dbo].[item]
           ([business_id]
           ,[name]
           ,[code]
           ,[category]
           ,[brand]
           ,[uom]
           ,[cost]
           ,[price]
           ,[file_path]
           ,[updated_by]
           ,[date_created]
           ,[date_modified])
     VALUES
           (1
           ,'Item A 1'
           ,'CODE-AAA'
           ,1
           ,1
           ,1
           ,10.50
           ,22.75
           ,''
           ,'user_a1'
           ,GETDATE()
           ,GETDATE())
GO


