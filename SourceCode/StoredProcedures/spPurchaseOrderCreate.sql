USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderCreate')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderCreate] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderCreate]
    @BusinessId bigint,
    --@PurchaseOrderNo nvarchar(50),
    @IsCashPurchase bit,
    @CashPurchaseNo nvarchar(50),
    @SupplierId bigint,
    @SupplierName nvarchar(100),
    @Reference nvarchar(50),
    @Contact nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @DateDelivered date,
    @AmountType varchar(16),
    @PaymentMethod varchar(16),
    @Note nvarchar(MAX),
    @CreditTerm nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
    @Status varchar(50),
    @IsVoided bit,
    @IsDeleted bit,
	--@Items tPurchaseOrderItem readonly,
    @DateCreated datetime,
    @DateModified datetime,
    @CreatedBy varchar(50),
	@UpdatedBy varchar(50)
AS

BEGIN TRY
BEGIN TRANSACTION

	-- Get the next Document Number first
	DECLARE @nextDocNo BIGINT;
	DECLARE @newPoId BIGINT;
	EXEC @nextDocNo = spGetNextDocId @BusinessId = @BusinessId, @Doctype = 'PurchaseOrder';

	-- Insert Purchase Order
	INSERT INTO [dbo].[PurchaseOrder] ([BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [SupplierName], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivered], [AmountType], [PaymentMethod], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsVoided], [IsDeleted], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT @BusinessId, @nextDocNo, @IsCashPurchase, @CashPurchaseNo, @SupplierId, @SupplierName, @Reference, @Contact, @Address1, @Address2, @Address3, @Phone, @Date, @DateDelivered, @AmountType, @PaymentMethod, @Note, @CreditTerm, @Subtotal, @Tax, @Total, @Status, @IsVoided, @IsDeleted, @DateCreated, @DateModified, @CreatedBy, @UpdatedBy
	
	SET @newPoId = SCOPE_IDENTITY();

	---- Insert Purchase Order's items
	--INSERT INTO [dbo].[PurchaseOrderItem] ([BusinessId], [PurchaseOrderId], [ItemId], [OptionName], [Quantity], [UnitOfMeasure], [Price], [Amount], [Description], [IsDeleted], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	--SELECT BusinessId = @BusinessId, PurchaseOrderId = @newPoId, ItemId, OptionName, Quantity, UnitOfMeasure, Price, Amount, Description, IsDeleted, DateCreated, DateModified, CreatedBy, UpdatedBy
	--	FROM @Items;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;
