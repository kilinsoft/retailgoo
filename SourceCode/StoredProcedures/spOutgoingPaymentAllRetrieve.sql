USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spOutgoingPaymentAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spOutgoingPaymentAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spOutgoingPaymentAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select op.*, s.SupplierName
	from [OutgoingPayment]op
	left join [Supplier] s
		on op.SupplierId = s.SupplierId
	where	op.BusinessId = @BusinessId
		and	op.IsDisabled <> 1
		AND op.IsCashPurchase <> 1
	order by op.OutgoingPaymentId desc;

	/*
	SELECT op.*, sp.SupplierName AS PayeeName
	FROM [dbo].[OutgoingPayment] op
	INNER JOIN [dbo].[Supplier] sp
		ON op.SupplierId = sp.SupplierId
	WHERE op.BusinessId = @BusinessId
		AND op.IsDisabled <> 1
	ORDER BY op.OutgoingPaymentId DESC
	*/

SET @ReturnValue = 0;
RETURN @ReturnValue;