USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCashPurchaseAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCashPurchaseAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCashPurchaseAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		po.*,
		s.SupplierName
	FROM [PurchaseOrder] po
	LEFT JOIN [Supplier] s
		ON po.SupplierId = s.SupplierId
	WHERE	po.BusinessId = @BusinessId
		AND po.IsCashPurchase = 1
		AND po.IsDisabled <> 1
	ORDER BY po.PurchaseOrderId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;