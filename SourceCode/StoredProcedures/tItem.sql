IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tItem')
   EXEC sp_droptype tItem
GO
CREATE TYPE tItem AS TABLE (
	[ItemId] [bigint] NOT NULL,
	[ItemGroupId] [bigint] NOT NULL,
	[ItemName] [nvarchar](100) NOT NULL,
	[Barcode] [varchar](50) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Cost] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[IsForSales] [bit] NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
