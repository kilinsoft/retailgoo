USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBillRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spBillRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBillRetrieve]
	@BillId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		b.*, c.CustomerName,
		CASE bpi_result.AmountReceived
			WHEN NULL THEN 0
			ELSE bpi_result.AmountReceived
		END AS AmountTendered
	FROM [Bill] b
	LEFT JOIN 
		( 
		SELECT bpi.BillId, SUM(bpi.Amount) As AmountReceived
		FROM [BillPaymentItem] bpi 
		WHERE bpi.BillId = @BillId
			AND bpi.IsDisabled <> 1
		GROUP BY bpi.BillId 
		) bpi_result
		ON b.BillId = bpi_result.BillId
	LEFT JOIN [Customer] c
		ON b.CustomerId = c.CustomerId
	WHERE	b.BillId = @BillId

SET @ReturnValue = 0;
RETURN @ReturnValue;
