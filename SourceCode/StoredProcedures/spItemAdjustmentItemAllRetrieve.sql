USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemAdjustmentItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemAdjustmentItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemAdjustmentItemAllRetrieve]
	@AdjustmentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT iaItem.*, dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName, (iaItem.QuantityAfter - iaItem.QuantityBefore) AS 'Difference'
	FROM [ItemAdjustmentItem] iaItem
		,[Item] i
	INNER JOIN [dbo].[ItemGroup] ig
	    ON i.ItemGroupId = ig.ItemGroupId
	WHERE iaItem.AdjustmentId = @AdjustmentId
		AND iaItem.ItemId = i.ItemId
		AND iaItem.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;
