USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spExpenseCategoryListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spExpenseCategoryListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spExpenseCategoryListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT
		ExpenseCategoryId AS VALUE,
		ExpenseCategoryName AS TEXT
	FROM [ExpenseCategory];

SET @ReturnValue = 0;
RETURN @ReturnValue;