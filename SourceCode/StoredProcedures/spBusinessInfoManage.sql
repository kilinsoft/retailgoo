USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBusinessInfoManage')
   EXEC('CREATE PROCEDURE [dbo].[spBusinessInfoManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBusinessInfoManage]
    @BusinessId bigint,
	@BusinessName nvarchar(100),
	@BusinessTypeId tinyint,
	--@UseTemplateBusiness bit,
	--@ImmediateBusinessId bigint,
	--@RootBusinssId bigint,
    @ImagePath varchar(max),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @City nvarchar(50),
    @CountryId tinyint,
    @ZipCode nvarchar(50),
    @Phone nvarchar(50),
    @Fax nvarchar(50),
    @Email varchar(320),
    @Website nvarchar(max),
    @FacebookUrl nvarchar(max),
    @TaxIdInfo varchar(50),
    @DateExpired datetime,
	@ItemState char(1),
	@UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N' BEGIN
		INSERT INTO [dbo].[Business]
           ([BusinessName]
           ,[BusinessTypeId]
           ,[ImagePath]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[Phone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[FacebookUrl]
           ,[TaxIdInfo]
           ,[DateExpired]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@BusinessName
           ,@BusinessTypeId
           ,@ImagePath
           ,@Address1
           ,@Address2
           ,@Address3
           ,@City
           ,@CountryId
           ,@ZipCode
           ,@Phone
           ,@Fax
           ,@Email
           ,@Website
           ,@FacebookUrl
           ,@TaxIdInfo
           ,@DateExpired
           ,@dateNow
           ,@dateNow
           ,@username
           ,@username)

	END ELSE IF @ItemState = 'M' BEGIN
		
		UPDATE [dbo].[Business]
		   SET [BusinessName] = @BusinessName
			  ,[BusinessTypeId] = @BusinessTypeId
			  ,[ImagePath] = @ImagePath
			  ,[Address1] = @Address1
			  ,[Address2] = @Address2
			  ,[Address3] = @Address3
			  ,[City] = @City
			  ,[CountryId] = @CountryId
			  ,[ZipCode] = @ZipCode
			  ,[Phone] = @Phone
			  ,[Fax] = @Fax
			  ,[Email] = @Email
			  ,[Website] = @Website
			  ,[FacebookUrl] = @FacebookUrl
			  ,[TaxIdInfo] = @TaxIdInfo
			  ,[DateExpired] = @DateExpired
			  ,[DateModified] = @dateNow
			  ,[UpdatedBy] = @UserId
		 WHERE [BusinessId] = @BusinessId;

	END ELSE IF @ItemState = 'D' BEGIN
		DECLARE @temp varchar; -- Do Nothing as of now
	END

SET @ReturnValue = 0;
RETURN @ReturnValue;