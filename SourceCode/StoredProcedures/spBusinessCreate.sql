USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBusinessCreate')
   EXEC('CREATE PROCEDURE [dbo].[spBusinessCreate] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBusinessCreate]
	@UserId bigint,
	@BusinessName nvarchar(100),
	@BusinessTypeId tinyint,
	--@UseTemplateBusiness bit,
	--@ImmediateBusinessId bigint,
	--@RootBusinessId bigint,
	@ImagePath varchar(MAX),
	@Address1 nvarchar(100),
	@Address2 nvarchar(100),
	@Address3 nvarchar(100),
	@City nvarchar(50),
	@CountryId tinyint,
	@ZipCode nvarchar(50),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@Email varchar(254),
	@Website nvarchar(MAX),
	@FacebookUrl nvarchar(MAX),
	@TaxIdInfo nvarchar(50),
	@DateExpired datetime
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @newBusinessId BIGINT;
		SET @newBusinessId = -1;
	
	declare @Username varchar(50);
	exec @Username = GetUsername @UserId = @UserId;

	declare @currencyId int;
		set @currencyId = 1;

	-- 1. Insert new business
	INSERT INTO [dbo].[Business]
           ([BusinessName]
           ,[BusinessTypeId]
           --,[UseTemplateBusiness]
           --,[ImmediateBusinessId]
           --,[RootBusinessId]
           ,[ImagePath]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[Phone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[FacebookUrl]
           ,[TaxIdInfo]
           ,[DateExpired]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@BusinessName
		   ,@BusinessTypeId
		   --,@UseTemplateBusiness
		   --,@ImmediateBusinessId
		   --,@RootBusinessId
		   ,@ImagePath
		   ,@Address1
		   ,@Address2
		   ,@Address3
		   ,@City
		   ,@CountryId
		   ,@ZipCode
		   ,@Phone
		   ,@Fax
		   ,@Email
		   ,@Website
		   ,@FacebookUrl
		   ,@TaxIdInfo
		   ,@DateExpired
		   ,@dateNow
		   ,@dateNow
		   ,@UserId
		   ,@UserId)
	SET @newBusinessId = SCOPE_IDENTITY();

	-- 2. Insert ScreenPermission for this business
	DECLARE @defaultPermission VARCHAR(MAX);
		SET @defaultPermission = 'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a';
	INSERT INTO [dbo].[ScreenPermission]
           ([BusinessId]
           ,[Admin]
           ,[Manager]
           ,[Clerk]
           ,[Purchase]
           ,[Sales]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[ModifiedBy])
     VALUES
           (@newBusinessId
           ,@defaultPermission
           ,@defaultPermission
           ,@defaultPermission
           ,@defaultPermission
           ,@defaultPermission
           ,@dateNow
           ,@dateNow
           ,@Username
           ,@Username)

	-- 3. Insert default DocSettings
	INSERT INTO [dbo].[DocSetting]
           ([BusinessId]
           ,[SalesOrderFormat]
           ,[SalesOrderNo]
           ,[SalesOrderTemplate]
           ,[InvoiceFormat]
           ,[InvoiceNo]
           ,[InvoiceTemplate]
           ,[ReceiptFormat]
           ,[ReceiptNo]
           ,[ReceiptTemplate]
           ,[StockAdjFormat]
           ,[StockAdjNo]
           ,[StockAdjTemplate]
           ,[ReceiveOrderFormat]
           ,[ReceiveOrderNo]
           ,[ReceiveOrderTemplate]
           ,[CashPurchaseFormat]
           ,[CashPurchaseNo]
           ,[CashPurchaseTemplate]
           ,[CashSalesFormat]
           ,[CashSalesNo]
           ,[CashSalesTemplate]
           ,[PurchaseOrderFormat]
           ,[PurchaseOrderNo]
           ,[PurchaseOrderTemplate]
           ,[PaymentFormat]
           ,[PaymentNo]
           ,[PaymentTemplate]
           ,[ExpenseFormat]
           ,[ExpenseNo]
           ,[ExpenseTemplate]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@newBusinessId
		   ,'SO-{XXXXXX}'
		   ,0
		   ,0
		   ,'IV-{XXXXXX}'
		   ,0
		   ,0
		   ,'RV-{XXXXXX}'
		   ,0
		   ,0
		   ,'SA-{XXXXXX}'
		   ,0
		   ,0
		   ,'RO-{XXXXXX}'
		   ,0
		   ,0
		   ,'CP-{XXXXXX}'
		   ,0
		   ,0
		   ,'CS-{XXXXXX}'
		   ,0
		   ,0
		   ,'PO-{XXXXXX}'
		   ,0
		   ,0
		   ,'PV-{XXXXXX}'
		   ,0
		   ,0
		   ,'EX-{XXXXXX}'
		   ,0
		   ,0
		   ,@dateNow
		   ,@dateNow
		   ,@Username
		   ,@Username
		   )

	-- 4. Insert Financial Settings
	INSERT INTO [dbo].[FinancialSetting]
           ([BusinessId]
           ,[CurrencyId]
           ,[DefaultPriceTypeId]
           ,[UseServiceCharge]
           ,[ServiceChargeAmount]
           ,[SalesTaxTypeId]
           ,[SalesTaxAmount]
           ,[UseStockItemControl]
           ,[CostingTypeId]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@newBusinessId, @currencyId, 1, 1, 10.00, 1, 7.00, 0, 1, @dateNow, @dateNow, @Username, @Username )

	-- 5. Insert the current user to the first busienss staff list
	INSERT INTO [dbo].[Staff]
           ([BusinessId]
           ,[UserId]
           ,[Title]
           ,[Firstname]
           ,[Lastname]
           ,[Birthdate]
           ,[Gender]
           ,[Identification]
           ,[Phone]
           ,[Fax]
		   ,[Email]
           ,[FacebookId]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[Role]
           ,[Status]
		   ,[IsDisabled]
           ,[Guid]
           ,[DateGuidExpired]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@newBusinessId, @UserId, NULL, '', '', NULL, NULL, '', '', '', '', ''
		   , '', '', '', '', NULL, '', 'adm', 'active', 0, '', NULL, @dateNow, @dateNow, @Username, @Username
		   )

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO