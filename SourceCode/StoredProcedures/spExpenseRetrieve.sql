USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spExpenseRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spExpenseRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spExpenseRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT e.*, s.SupplierName As VendorName, ec.ExpenseCategoryName
	FROM [Expense] e
	INNER JOIN [ExpenseCategory] ec
		ON e.ExpenseCategoryId = ec.ExpenseCategoryId
	INNER JOIN [Supplier] s 
		ON e.SupplierId = s.SupplierId
	WHERE e.BusinessId = @BusinessId
		AND e.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;