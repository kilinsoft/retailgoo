USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	--SELECT 
	--	i.ItemId AS Value, 
	--	CASE 
	--		WHEN ig.ItemGroupName = i.ItemName THEN CONCAT(ig.ItemGroupCode, N' ', ig.ItemGroupName) 
	--		ELSE CONCAT(ig.ItemGroupCode, N': ', ig.ItemGroupName, N' - ', i.ItemName) 
	--	END AS Text
	--FROM [ITEM] i
	--	INNER JOIN [ItemGroup] ig
	--	ON i.ItemGroupId = ig.ItemGroupId
	--WHERE 
	--		i.IsDisabled <> 1
	--	AND ig.IsDisabled <> 1

	SELECT 
		i.ItemId, i.ItemName, ig.ItemGroupName, ig.ItemGroupCode
	FROM [ITEM] i
		INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	WHERE 
			i.IsDisabled <> 1
		AND ig.IsDisabled <> 1
		AND ig.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;