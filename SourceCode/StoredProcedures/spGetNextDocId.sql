USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spGetNextDocId')
   EXEC('CREATE PROCEDURE [dbo].[spGetNextDocId] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spGetNextDocId]
    @BusinessId BIGINT,
    @DocType VARCHAR(50)
AS
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION
BEGIN TRY
	SELECT TOP 1 * 
	INTO #tmpDoc
	FROM [DocSetting]
	WITH (ROWLOCK) 
	WHERE BusinessId = @BusinessId

	IF NOT EXISTS(SELECT * FROM #tmpDoc)
	BEGIN
		RAISERROR( 'DocSetting item is missing for this business', 16, 1 );
	END

	DECLARE @colName VARCHAR(50);
	DECLARE @tmpSql NVARCHAR(MAX);
	DECLARE @numCurrent INT;
	DECLARE @tblNumCurrent table (PurchaseNo INT);
	
	SET @colName = @DocType + 'No';
	SET @tmpSql = N'SELECT @numCurrent = ' + @colName + N' FROM #tmpDoc;';
	
	declare @tmpOut int;
	EXEC sp_executesql @tmpSql, N'@numCurrent int output', @numCurrent output;
	EXEC (N'SELECT ' + @colName + ' = ' + @numCurrent);

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;