USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderStatusUpdate')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderStatusUpdate] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderStatusUpdate]
	@IdTable tIdTable READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	SELECT 
		po.PurchaseOrderId, po.Status
	INTO #Temp 
	FROM @IdTable ai
	INNER JOIN [PurchaseOrder] po
	ON ai.Id = po.PurchaseOrderId
		AND po.IsDisabled <> 1
		AND po.Status <> 'Void';

	IF EXISTS ( SELECT TOP 1 * FROM #Temp ) 
		BEGIN
			--DECLARE @qtyOrdered DECIMAL (18 ,4);
			--	SET @qtyOrdered = 0;
			--DECLARE @qtyReceived DECIMAL (18, 4);
			--	SET @qtyReceived = 0;	

			-- Update QuantityReceived in PurchaseOrderItem table
			UPDATE [PurchaseOrderItem]
			SET 
				QuantityReceived = 
					CASE 
						WHEN ret.QuantityReceived IS NULL THEN 0
						ELSE ret.QuantityReceived
					END
			FROM 
			(
				SELECT 
					poi.PurchaseOrderItemId, 
					SUM(result.Quantity) AS QuantityReceived
				FROM #Temp tmp
				INNER JOIN [PurchaseOrderItem] poi
					ON tmp.PurchaseOrderId = poi.PurchaseOrderId
						AND poi.IsDisabled <> 1
				LEFT JOIN (
					SELECT 
						roi.PurchaseOrderItemId,
						roi.Quantity
					FROM [ReceiveOrderItem] roi
					INNER JOIN [ReceiveOrder] ro
						ON roi.ReceiveOrderId = ro.ReceiveOrderId
							AND ro.IsDisabled <> 1
							AND ro.Status <> 'Void'
							AND roi.IsDisabled <> 1
				) result
					ON poi.PurchaseOrderItemId = result.PurchaseOrderItemId
				GROUP BY poi.PurchaseOrderItemId
			) ret
			WHERE [PurchaseOrderItem].PurchaseOrderItemId = ret.PurchaseOrderItemId;


			UPDATE [PurchaseOrder]
			SET 
				Status = CASE 
							WHEN (result.NumAllRow IS NULL OR result.NumDoneRow IS NULL) THEN 'New'
							WHEN result.NumDoneRow >= result.NumAllRow THEN 'Done'
							ELSE 'Partial'
						 END
			FROM 
			(
				SELECT 
					po.PurchaseOrderId,
					groupAllSubItem.NumRow AS NumAllRow,
					groupDoneSubItem.NumRow AS NumDoneRow
				FROM [PurchaseOrder] po
				INNER JOIN #Temp tmp
					ON po.PurchaseOrderId = tmp.PurchaseOrderId
				LEFT JOIN
				(
					SELECT
						po.PurchaseOrderId,
						COUNT(*) AS NumRow
					FROM [PurchaseOrder] po
					INNER JOIN [PurchaseOrderItem] poi
						ON	po.PurchaseOrderId = poi.PurchaseOrderId
							AND po.IsDisabled <> 1
							AND po.Status <> 'Void'
							AND poi.IsDisabled <> 1
					GROUP BY po.PurchaseOrderId
				) groupAllSubItem
					ON po.PurchaseOrderId = groupAllSubItem.PurchaseOrderId
				LEFT JOIN
				(
					SELECT 
						poi.PurchaseOrderId,
						COUNT(*) AS NumRow
					FROM [PurchaseOrderItem] poi
					WHERE	poi.IsDisabled <> 1
							AND poi.QuantityReceived >= poi.Quantity
					GROUP BY poi.PurchaseOrderId
				) groupDoneSubItem
					ON po.PurchaseOrderId = groupDoneSubItem.PurchaseOrderId

			) result
			WHERE [PurchaseOrder].PurchaseOrderId = result.PurchaseOrderId;

		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO