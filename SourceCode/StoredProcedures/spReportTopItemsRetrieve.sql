USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReportTopItemsRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReportTopItemsRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReportTopItemsRetrieve]
    @BusinessId BIGINT,
	@NumItem INT,
	@NumHistoryRange INT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	DECLARE @dateNow DATE;
	SET @dateNow = GETDATE();

	SELECT TOP(@NumItem) 
		i.ItemId, 
		dbo.fnGetItemFullName( ig.ItemGroupCode, ig.ItemGroupName, i.ItemName ) AS ItemFullName, 
		SUM(bi.Quantity * bi.Price) AS Amount
	FROM dbo.Item i
	INNER JOIN ItemGroup ig
		ON i.ItemGroupId = ig.ItemGroupId
	INNER JOIN BillItem bi
		ON bi.ItemId = i.ItemId
	INNER JOIN Bill b
		ON bi.BillId = b.BillId
	WHERE	ig.BusinessId = @BusinessId
		AND	b.Status = 'Done'
		AND b.DateCreated > DATEADD( DAY, -@NumHistoryRange, @dateNow)
	GROUP BY i.ItemId, i.ItemName, ig.ItemGroupCode, ig.ItemGroupName
	ORDER BY Amount DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;