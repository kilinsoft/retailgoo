USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSearchUserByEmail')
   EXEC('CREATE PROCEDURE [dbo].[spSearchUserByEmail] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSearchUserByEmail]
	@FilterKeyword VarChar(252)
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	DECLARE @Filter VarChar(255);
	
	SET @Filter = '%' + @FilterKeyword + '%';

	SELECT TOP 20 *
	FROM dbo.[User] usr
	WHERE	usr.Email LIKE @Filter
		AND usr.IsDisabled <> 1;

SET @ReturnValue = @@ROWCOUNT;
RETURN @ReturnValue;

GO