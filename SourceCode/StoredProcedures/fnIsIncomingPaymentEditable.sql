USE [retailgoo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnIsIncomingPaymentEditable', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnIsIncomingPaymentEditable];
GO
CREATE FUNCTION [dbo].[fnIsIncomingPaymentEditable](@IncomingPaymentId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void IncomingPayment item that has been created by CashSales. Need to do it from CashSales screen.
	SELECT @blockingItem = si.CashSalesNo
	FROM [SalesInvoice] si
	INNER JOIN [IncomingPaymentItem] ipi
		ON si.SalesInvoiceId = ipi.SalesInvoiceId
			AND ipi.IncomingPaymentId = @IncomingPaymentId 
	WHERE	si.IsCashSales = 1;

	IF @blockingItem IS NULL
		SET @blockingItem = N'';

	return @blockingItem;
END
