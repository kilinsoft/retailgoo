USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemReceiveTransferManage')
   EXEC('CREATE PROCEDURE [dbo].[spItemReceiveTransferManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemReceiveTransferManage]
	@BusinessId BIGINT,
	@TransferId BIGINT,
	@TransferName NVARCHAR(200),
	@SourceBusinessId BIGINT,
	@TargetBusinessId BIGINT,
	@Reference NVARCHAR(100),
	@DateSent DATETIME,
	@DateReceived DATETIME,
	@TransferStatusTypeId TINYINT,
	@SubItems tItemTransfer READONLY,
	@UserId BIGINT,
	@ItemState CHAR(1),
	@Version DATETIME
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetTfId BIGINT;
		SET @targetTfId = @TransferId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	--DECLARE @status VARCHAR(10);
	--	SET @status = 'New';

	--DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	--DECLARE @updateItemList tIdTable;

	DECLARE @AmendingItem BIT;
		SET @AmendingItem = 0;

	-- 1. Manage [ItemTransfer] table
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[ItemTransfer]
			   ([TransferName]
			   ,[SourceBusinessId]
			   ,[TargetBusinessId]
			   ,[Reference]
			   ,[DateSent]
			   ,[DateReceived]
			   ,[TransferStatusTypeId]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
			   (@TransferName
			   ,@SourceBusinessId
			   ,@TargetBusinessId
			   ,@Reference
			   ,@DateSent
			   ,@DateReceived
			   ,1 --'Transferring'
			   ,0
			   ,@dateNow
			   ,@dateNow
			   ,@username
			   ,@username)
			SET @targetTfId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			-- Check that only one of the following conditions of the TransferStatus is met, otherwise, raise error.
			-- 1. Transferring -> Transferring (Status can remain the same but there are some other changes to the record instead)
			-- 2. Transferring -> Canceled
			-- 3. Transferring -> Received
			-- 4. Transferring -> Rejected
			DECLARE @currentStatusTypeId TINYINT;
				SET @currentStatusTypeId = NULL;

			SELECT TOP 1 @currentStatusTypeId = it.TransferStatusTypeId
			FROM [ItemTransfer] it
			WHERE	it.TransferId = @TransferId;

			IF @currentStatusTypeId IS NULL
			BEGIN
				SET @ErrTxt = N'Missing ItemTransfer with TransferId = ' + CONVERT(NVARCHAR(20), @TransferId);
				RAISERROR( @ErrTxt, 16, 1 );
			END
		
			IF (@currentStatusTypeId <> 1) AND (@currentStatusTypeId <> @TransferStatusTypeId)
			BEGIN
				SET @ErrTxt = N'The status of this item can no longer be changed';
				RAISERROR( @ErrTxt, 16, 1 );
			END

			-- the transfer status changes from [Transferring] to [Received]
			IF (@currentStatusTypeId = 1) AND (@TransferStatusTypeId = 3)
				SET @AmendingItem = 1;

			UPDATE [dbo].[ItemTransfer]
			   SET [Reference] = @Reference
				  ,[DateReceived] = @DateReceived
				  ,[TransferStatusTypeId] = @TransferStatusTypeId
				  ,[DateModified] = @dateNow
				  ,[UpdatedBy] = @username
			 WHERE	[TransferId] = @TransferId;
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[ItemTransfer]
			   SET [IsDisabled] = 1
				  ,[UpdatedBy] = @username
			 WHERE	[TransferId] = @TransferId;
		END

	-- 2. Manage SubItems
	---- Insert Item Transfer's items
	INSERT INTO [dbo].[ItemTransferItem]
        ([TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT 
		TransferId = @targetTfId,
		ItemId = subItem.ItemId,
		ItemGroupCode = ig.ItemGroupCode, ItemGroupName = ig.ItemGroupName, ItemName = i.ItemName,
        Uom = ig.Uom, ImagePath = ig.ImagePath, ItemCategoryName = ic.ItemCategoryName, ItemBrandName = ib.ItemBrandName,
        Barcode = i.Barcode, Quantity = subItem.Quantity, Cost = i.Cost, Price = i.Price, IsForSales = i.IsForSales,
        IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
	FROM @SubItems subItem
	INNER JOIN [Item] i
		ON subItem.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		ON ig.ItemBrandId = ib.ItemBrandId
	WHERE subItem.ItemState = 'N';

	---- Update Item Transfer's items
	UPDATE itItem
	SET
		itItem.ItemGroupCode = ig.ItemGroupCode, itItem.ItemGroupName = ig.ItemGroupName, itItem.ItemName = i.ItemName,
        itItem.Uom = ig.Uom, itItem.ImagePath = ig.ImagePath, itItem.ItemCategoryName = ic.ItemCategoryName, itItem.ItemBrandName = ib.ItemBrandName,
        itItem.Barcode = i.Barcode, itItem.Quantity = subItem.Quantity, itItem.Cost = i.Cost, itItem.Price = i.Price, itItem.IsForSales = i.IsForSales,
	    itItem.DateModified = @dateNow, itItem.UpdatedBy = @username
	FROM [dbo].[ItemTransferItem] AS itItem
	INNER JOIN @SubItems subItem
		ON itItem.TransferId = subItem.TransferId
	INNER JOIN [Item] i
		ON subItem.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		ON ig.ItemBrandId = ib.ItemBrandId
	WHERE subItem.ItemState = 'M'

	---- Disable Item Transfer's items
	UPDATE itItem
	SET itItem.IsDisabled = 1, itItem.DateModified = @dateNow, itItem.UpdatedBy = @username
	FROM [dbo].[ItemTransferItem] AS itItem
	INNER JOIN @SubItems subItem
		ON itItem.TransferId = subItem.TransferId
	WHERE subItem.ItemState = 'D'

	IF (@AmendingItem = 1)
	BEGIN
		-- Adjust [Item].Quantity if Status changes from [Transferring] to [Received]
		-- ToDo : 1. Create Item if it does not exist ( if Brand and Category do not exist, assign <null> to them )
		--BEGIN TRANSACTION

		INSERT INTO ItemCategory
			([BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemCategoryName = itItem.ItemCategoryName,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemCategoryId(@TargetBusinessId, itItem.ItemCategoryName) = 0

		INSERT INTO ItemBrand
			([BusinessId], [ItemBrandName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemBrandName = itItem.ItemBrandName,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemBrandId(@TargetBusinessId, itItem.ItemBrandName) = 0

		--COMMIT TRANSACTION;
		--BEGIN TRANSACTION

		INSERT INTO ItemGroup
			([BusinessId] ,[ItemGroupCode] ,[ItemGroupName] ,[ImagePath] ,[ItemCategoryId] ,[ItemBrandId] ,[Cost] ,[Price] ,[Uom] ,[IsForSales] ,[IsDisabled] ,[DateCreated] ,[DateModified] ,[CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemGroupCode = itItem.ItemGroupCode, ItemGroupName = itItem.ItemGroupName, ImagePath = itItem.ImagePath,
			ItemCategoryId = dbo.fnGetItemCategoryId(@TargetBusinessId, itItem.ItemCategoryName),
			ItemBrandId = dbo.fnGetItemBrandId(@TargetBusinessId, itItem.ItemBrandName),
			Cost = itItem.Cost, Price = itItem.Price, Uom = itItem.Uom, IsForSales = itItem.IsForSales,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemGroupId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName) = 0

		--COMMIT TRANSACTION;
		--BEGIN TRANSACTION
		
		INSERT INTO [dbo].[Item]
           ([ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
        SELECT
			ItemGroupId = dbo.fnGetItemGroupId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName),
			ItemName = itItem.ItemName,
			Barcode = itItem.Barcode, Quantity = 0, Cost = itItem.Cost,
			Price = itItem.Price, Price02 = 0, Price03 = 0, Price04 = 0,
			IsForSales = itItem.IsForSales, 
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName, itItem.ItemName) = 0

		--COMMIT TRANSACTION;

		-- ToDo : 2. Adjust Item.Quantity
		-- recrease the item quantity from source business
		UPDATE i
			SET i.Quantity = i.Quantity - itItem.Quantity
		FROM [Item] i
		INNER JOIN ItemTransferItem itItem
			ON itItem.ItemName = i.ItemName
		INNER JOIN @SubItems subItem
			ON subItem.TransferItemId = itItem.TransferItemId
		INNER JOIN ItemGroup ig
			ON ig.BusinessId = @SourceBusinessId
			AND ig.ItemGroupId = i.ItemGroupId

		-- increase the item quantity from target business
		UPDATE i
		SET i.Quantity = i.Quantity + itItem.Quantity
		FROM [Item] i
		INNER JOIN ItemTransferItem itItem
			ON itItem.ItemName = i.ItemName
		INNER JOIN @SubItems subItem
			ON subItem.TransferItemId = itItem.TransferItemId
		INNER JOIN ItemGroup ig
			ON ig.BusinessId = @TargetBusinessId
			AND ig.ItemGroupId = i.ItemGroupId
	END


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO