USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemGroupId]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnGetItemGroupId', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnGetItemGroupId];
GO
CREATE FUNCTION [dbo].[fnGetItemGroupId](@BusinessId BIGINT, @ItemGroupCode VARCHAR(50), @ItemGroupName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemGroupId BIGINT;
		SET @ItemGroupId = 0;

	SELECT TOP 1 @ItemGroupId = ig.ItemGroupId
	FROM ItemGroup ig
	WHERE ig.BusinessId = @BusinessId
		AND ig.ItemGroupCode = @ItemGroupCode
		AND ig.ItemGroupName = @ItemGroupName

	RETURN @ItemGroupId;
END
