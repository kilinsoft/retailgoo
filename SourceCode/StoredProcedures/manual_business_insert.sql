INSERT INTO [retailgoo].[dbo].[business]
           ([name]
           ,[creator]
           ,[type]
           ,[logo_path]
           ,[address1]
           ,[address2]
           ,[address3]
           ,[city]
           ,[country]
           ,[zip_code]
           ,[phone]
           ,[fax]
           ,[email]
           ,[website]
           ,[facebook_url]
           ,[tax_id]
           ,[updated_by]
           ,[date_expired]
           ,[date_created]
           ,[date_modified])
     VALUES
           ('A Store'
           ,'user_a1'
           ,'0'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,'user_a1'
           ,DATEADD(month, 2, GETDATE())
           ,GETDATE()
           ,GETDATE())
GO


