USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetUsername]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnIsReceiveOrderEditable', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnIsReceiveOrderEditable];
GO
CREATE FUNCTION [dbo].[fnIsReceiveOrderEditable](@ReceiveOrderId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void ReceiveOrder item that has been created by CashPurchase. Need to do it from CashPurchase screen.
	SELECT TOP 1 @blockingItem = po.CashPurchaseNo
	FROM [PurchaseOrder] po
	INNER JOIN [PurchaseOrderItem] poi
		ON	po.PurchaseOrderId = poi.PurchaseOrderId
			AND poi.IsDisabled <> 1
	INNER JOIN [ReceiveOrderItem] roi
		ON	roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
			AND roi.IsDisabled <> 1
			AND roi.ReceiveOrderId = @ReceiveOrderId
	WHERE	po.IsCashPurchase = 1;

	IF @blockingItem IS NULL
		BEGIN
			SELECT TOP 1 @blockingItem = op.OutgoingPaymentNo
			FROM [OutgoingPayment] op
			INNER JOIN [OutgoingPaymentItem] opi
				ON op.OutgoingPaymentId = opi.OutgoingPaymentId
					AND op.IsDisabled <> 1
					AND op.Status <> 'Void'
					AND opi.IsDisabled <> 1
			WHERE	opi.ReceiveOrderId = @ReceiveOrderId;

			IF @blockingItem IS NULL
				SET @blockingItem = N'';
		END

	return @blockingItem;
END
