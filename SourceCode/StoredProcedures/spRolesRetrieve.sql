USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spRolesRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spRolesRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spRolesRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1 * 
	FROM [ScreenPermission]
	WHERE BusinessId = @BusinessId

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO