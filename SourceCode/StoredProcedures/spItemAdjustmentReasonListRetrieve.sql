USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemAdjustmentReasonListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemAdjustmentReasonListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemAdjustmentReasonListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT AdjustmentReasonId AS Value, AdjustmentReasonName AS Text
	FROM ItemAdjustmentReason

SET @ReturnValue = 0;
RETURN @ReturnValue;