USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemInfoRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemInfoRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemInfoRetrieve]
    @BusinessId BIGINT,
	@ItemId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT i.*, ig.UnitOfMeasure, ig.ItemGroupName, ig.ItemCode
	FROM [ITEM] i
		INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	WHERE i.BusinessId = @BusinessId
		AND i.ItemId = @ItemId

SET @ReturnValue = 0;
RETURN @ReturnValue;