USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPriceTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spPriceTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPriceTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select PriceTypeId AS Value, PriceTypeName AS Text
	from PriceType

SET @ReturnValue = 0;
RETURN @ReturnValue;