USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spIncomingPaymentManage')
   EXEC('CREATE PROCEDURE [dbo].[spIncomingPaymentManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spIncomingPaymentManage]
	@IncomingPaymentId bigint,
	@BusinessId bigint,
    @CustomerId bigint,
    @Reference nvarchar(50),
	@PaymentTypeId tinyint,
	@BankId bigint,
	@BankAccNo nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
	@Vat bit,
	@Wht bit,
	@WhtPercent decimal(18, 4),
    @Note nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
	--@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tIncomingPaymentItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetIpId BIGINT;
		SET @targetIpId = @IncomingPaymentId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextIpName NVARCHAR(50);
				SET @nextIpName = '';
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextIpName OUT;

			-- Insert Incoming Payment
			INSERT INTO [dbo].[IncomingPayment] 
				([BusinessId], [IncomingPaymentNo], [IsCashSales], [CustomerId], [Reference], [PaymentTypeId], [BankId], [BankAccNo],[Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextIpName, 0, @CustomerId, @Reference, @PaymentTypeId, @BankId, @BankAccNo, @Address1, @Address2, @Address3, @Phone, @Date, @Vat, @Wht, @WhtPercent, @Note, @Subtotal, @Tax, @Total, 'Paid', 0, @dateNow, @dateNow, @username, @username

			SET @targetIpId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[IncomingPayment]
			SET    
			[CustomerId] = @CustomerId, [Reference] = @Reference, [PaymentTypeId] = @PaymentTypeId,
			[BankId] = @BankId, [BankAccNo] = @BankAccNo,
			[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, [Phone] = @Phone, [Date] = @Date,
			[Vat] = @Vat, [Wht] = @Wht, [WhtPercent] = @WhtPercent, [Note] = @Note,
			[Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total, /*[Status] = @Status,*/
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[IncomingPaymentId] = @IncomingPaymentId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[IncomingPayment]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[IncomingPaymentId] = @IncomingPaymentId
		END

	-- Manage Items
	---- Insert Incoming Payment's items
	INSERT INTO [dbo].[IncomingPaymentItem] 
		([IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT IncomingPaymentId = @targetIpId, SalesInvoiceId, Amount, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Incoming Payment's items
	UPDATE ipItem
	SET ipItem.SalesInvoiceId = i.SalesInvoiceId, ipItem.Amount = i.Amount, 
	    ipItem.DateModified = @dateNow, ipItem.UpdatedBy = @username
	FROM [dbo].[IncomingPaymentItem] AS ipItem
		INNER JOIN @Items AS i
		ON ipItem.IncomingPaymentItemId = i.IncomingPaymentItemId 
		WHERE i.ItemState = 'M'

	---- Disable Incoming Payment's items
	UPDATE ipItem
	SET ipItem.IsDisabled = 1, ipItem.DateModified = @dateNow, ipItem.UpdatedBy = @username
	FROM [dbo].[IncomingPaymentItem] AS ipItem
		INNER JOIN @Items AS i
		ON ipItem.IncomingPaymentItemId = i.IncomingPaymentItemId 
		WHERE i.ItemState = 'D'

	-- Update SalesInvoice.Status of the SalesInvoice item that this IncomingPayment item refers to
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT result.SalesInvoiceId AS Id FROM (
		SELECT DISTINCT
			si.SalesInvoiceId
		FROM [SalesInvoice] si
		INNER JOIN [IncomingPaymentItem] ipi
			ON si.SalesInvoiceId = ipi.SalesInvoiceId
				AND si.IsDisabled <> 1
				AND si.Status <> 'Void'
		INNER JOIN [IncomingPayment] ip
			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
				AND ip.IsDisabled <> 1
				AND ip.Status <> 'Void'
		WHERE ip.IncomingPaymentId = @targetIpId
	) result;

	EXEC dbo.spSalesInvoiceStatusUpdate @updateItemList;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO