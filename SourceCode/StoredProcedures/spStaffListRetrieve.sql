USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spStaffListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spStaffListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spStaffListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT s.StaffId AS Value, u.Username AS Text FROM [dbo].[Staff] s
	LEFT JOIN [dbo].[User] u
		ON s.UserId = u.UserId
	WHERE s.BusinessId = @BusinessId
		AND	s.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;