USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemReceiveTransferItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemReceiveTransferItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemReceiveTransferItemAllRetrieve]
	@TransferId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT itItem.*,
		dbo.fnGetItemFullName(itItem.ItemGroupCode, itItem.ItemGroupName, itItem.ItemName) AS ItemFullName
	FROM [dbo].[ItemTransferItem] itItem
	WHERE itItem.TransferId = @TransferId
		AND itItem.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;
