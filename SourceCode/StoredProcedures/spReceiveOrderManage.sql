USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveOrderManage')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveOrderManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveOrderManage]
	@ReceiveOrderId bigint,
	@BusinessId bigint,
    @SupplierId bigint,
    --@SupplierName nvarchar(100),
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @Note nvarchar(MAX),
    @PaymentTerm nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
    --@Status varchar(50),
    --@IsVoided bit,
    --@IsDeleted bit,
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tReceiveOrderItem READONLY
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetRoId BIGINT;
		SET @targetRoId = @ReceiveOrderId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	DECLARE @updateItemList tIdTable;


	-- 1. Manage records in [ReceiveOrder] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextItemName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'ReceiveOrder', @RetDocName = @nextItemName OUT;

			-- Insert ReceiveOrder
			INSERT INTO [dbo].[ReceiveOrder] 
				([BusinessId], [ReceiveOrderNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], 
				[Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [AmountPaid], [Status], [IsDisabled],
				[DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@BusinessId, @nextItemName, 0, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, 
				@Phone, @Date, @Note, @PaymentTerm, @Subtotal, @Tax, @Total, 0.00, @status, 0, 
				@dateNow, @dateNow, @username, @username

			SET @targetRoId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @blockingItem = dbo.fnIsReceiveOrderEditable(@ReceiveOrderId);

			IF @blockingItem <> N''
				BEGIN
					SET @ErrTxt = N'The opearation is blocked by ' + @blockingItem;
					RAISERROR( @ErrTxt, 16, 1 );
				END

			UPDATE [dbo].[ReceiveOrder]
			SET
				[SupplierId] = @SupplierId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, 
				[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, 
				[Phone] = @Phone, [Date] = @Date, [Note] = @Note, [PaymentTerm] = @PaymentTerm, 
				[Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total,
				[DateCreated] = @dateNow, [DateModified] = @dateNow, [CreatedBy] = @username, [UpdatedBy] = @username
			WHERE	[ReceiveOrderId] = @ReceiveOrderId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[ReceiveOrder]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[ReceiveOrderId] = @ReceiveOrderId
		END


	-- 2. Revert [Item].Quantity for [ReceiveOrderItem] records in case they are modified. We will update with new
	--    ReceiveOrderItem.Quantity below
	UPDATE [Item]
	SET
		[Item].Quantity = ([Item].Quantity - subItems.Quantity)
	FROM @Items subItems
	WHERE	[Item].ItemId = subItems.ItemId
		AND (subItems.ItemState = 'M' OR subItems.ItemState = 'D');


	-- 3. Manage SubItems
	---- Insert Subitems
	INSERT INTO [dbo].[ReceiveOrderItem] 
		([ReceiveOrderId], [PurchaseOrderItemId], [ItemId], [Quantity], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT 
		@targetRoId, i.PurchaseOrderItemId, i.ItemId, i.Quantity, i.Price, 0, @dateNow, @dateNow, @username, @username
		FROM @Items i
		WHERE ItemState = 'N';

	---- Update Subitems
	UPDATE [ReceiveOrderItem]
	SET
		ItemId = subItems.ItemId, Quantity = subItems.Quantity, Price = subItems.Price,
		DateModified = @dateNow, UpdatedBy = @username
	FROM [dbo].[ReceiveOrderItem] roi
		INNER JOIN @Items subItems
			ON roi.ItemId = subItems.ItemId
		WHERE subitems.ItemState = 'M'

	---- Disable Subitems
	UPDATE [ReceiveOrderItem]
	SET 
		IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @username
	FROM [dbo].[ReceiveOrderItem] roi
		INNER JOIN @Items subItems
			ON roi.ItemId = subItems.ItemId 
		WHERE subItems.ItemState = 'D';

	-- Disable this because there is no way that the change of ReceiveOrderItem can cause ReceiveOrder.Status to change
	---- 4. Re-calculate the ReceiveOrder.Status again in case that the ReceiveOrderItem item is changed
	--IF @ItemState = 'M'
	--	BEGIN
	--		INSERT INTO @updateItemList
	--		SELECT ro.ReceiveOrderId FROM [ReceiveOrder] ro WHERE ro.ReceiveOrderId = @targetRoId;

	--		EXEC dbo.spReceiveOrderStatusUpdate @updateItemList;
	--	END

	-- 5. Re-calculate the PurchaseOrder.Status of the PurchaseOrder items that are refered to by this ReceiveOrder item
	DELETE FROM @updateItemList;

	INSERT INTO @updateItemList
	SELECT DISTINCT po.PurchaseOrderId AS Id
	FROM [PurchaseOrder] po
	INNER JOIN [PurchaseOrderItem] poi
		ON	po.PurchaseOrderId = poi.PurchaseOrderId
			AND po.IsDisabled <> 1
			AND po.Status <> 'Void'
			AND poi.IsDisabled <> 1
	INNER JOIN [ReceiveOrderItem] roi
		ON	poi.PurchaseOrderItemId = roi.PurchaseOrderItemId
			AND roi.IsDisabled <> 1
			AND roi.ReceiveOrderId = @targetRoId;

	EXEC dbo.spPurchaseOrderStatusUpdate @updateItemList;
		

	-- 6. Update Item.Quantity to reflect SubItems with ItemState = 'N' or ItemState = 'M'
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + roi.Quantity
	FROM [ReceiveOrderItem] roi
	INNER JOIN @Items i
		ON roi.ReceiveOrderItemId = i.ReceiveOrderItemId
			AND (i.ItemState = 'N' OR i.ItemState = 'M')
	WHERE	roi.IsDisabled <> 1
		AND roi.ReceiveOrderId = @targetRoId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO