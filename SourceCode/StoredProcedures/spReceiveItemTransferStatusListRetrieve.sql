USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveItemTransferStatusListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveItemTransferStatusListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveItemTransferStatusListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		TransferStatusTypeId AS Value,
		TransferStatusTypeName AS Text
	FROM [ItemTransferStatusType]
	WHERE	IsForReceiveTransferItem = 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;