USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBusinessRoleListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spBusinessRoleListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBusinessRoleListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		sbr.BusinessRoleId AS Value,
		sbr.BusinessRoleName AS Text
	FROM [System_BusinessRole] sbr

SET @ReturnValue = 0;
RETURN @ReturnValue;