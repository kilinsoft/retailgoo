USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPoRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spPoRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPoRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT * 
	FROM [PurchaseOrder]
	WHERE BusinessId = @BusinessId
		AND IsDeleted <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;