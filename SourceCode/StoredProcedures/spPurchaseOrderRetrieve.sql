USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderRetrieve]
    @PurchaseOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT po.*, s.SupplierName, pt.PaymentTypeName, at.AmountTypeName
	FROM [PurchaseOrder] po
	LEFT JOIN [Supplier] s
		ON po.SupplierId = s.SupplierId
	INNER JOIN [PaymentType] pt
		ON po.PaymentTypeId = pt.PaymentTypeId
	LEFT JOIN [AmountType] at
		ON po.AmountTypeId = at.AmountTypeId
	WHERE
		po.PurchaseOrderId = @PurchaseOrderId;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;