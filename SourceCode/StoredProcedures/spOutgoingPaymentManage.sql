USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spOutgoingPaymentManage')
   EXEC('CREATE PROCEDURE [dbo].[spOutgoingPaymentManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spOutgoingPaymentManage]
	@OutgoingPaymentId bigint,
	@BusinessId bigint,
    @OutgoingPaymentNo nvarchar(50),
    @SupplierId bigint,
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Detail1 nvarchar(100),
    @Detail2 nvarchar(100),
    @Detail3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @PaymentTypeId tinyint,
    @Note nvarchar(MAX),
    @TaxIdInfo nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
    --@Status varchar(16),
    --@IsVoided bit,
    --@IsDeleted bit,
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tOutgoingPaymentItem READONLY
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetOpId BIGINT;
		SET @targetOpId = @OutgoingPaymentId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextItemName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextItemName OUT;

			-- Insert 
			INSERT INTO [dbo].[OutgoingPayment] 
				([BusinessId], [OutgoingPaymentNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Detail1], [Detail2], [Detail3], [Phone], [Date], [PaymentTypeId], [Note], [TaxIdInfo], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@BusinessId, @nextItemName, 0, @SupplierId, @Reference, @ContactPerson, @Detail1, @Detail2, @Detail3, @Phone, @Date, @PaymentTypeId, @Note, @TaxIdInfo, @Subtotal, @Tax, @Total, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @targetOpId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[OutgoingPayment]
			SET    
			[SupplierId] = @SupplierId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, [Detail1] = @Detail1, [Detail2] = @Detail2, 
			[Detail3] = @Detail3, [Phone] = @Phone, [Date] = @Date, [PaymentTypeId] = @PaymentTypeId, [Note] = @Note, 
			[TaxIdInfo] = @TaxIdInfo, [Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[OutgoingPaymentId] = @OutgoingPaymentId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[OutgoingPayment]
			SET    
			[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[OutgoingPaymentId] = @OutgoingPaymentId
		END

	-- Manage subItems
	---- Insert Subitems
	INSERT INTO [dbo].[OutgoingPaymentItem] 
		([OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [Amount], [Description], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT 
		@targetOpId, ReceiveOrderId, SupplierInvoice, Amount, Description, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Subitems
	UPDATE opi
	SET	
		opi.SupplierInvoice = i.SupplierInvoice, 
		opi.ReceiveOrderId = i.ReceiveOrderId, 
		opi.Description = i.Description, 
		opi.Amount = i.Amount, 
		opi.DateModified = @dateNow, 
		opi.UpdatedBy = @username
	FROM [dbo].[OutgoingPaymentItem] AS opi
		INNER JOIN @Items AS i
		ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId
		WHERE i.ItemState = 'M'

	---- Disable Subitems
	UPDATE opi
	SET opi.IsDisabled = 1, opi.DateModified = @dateNow, opi.UpdatedBy = @username
	FROM [dbo].[OutgoingPaymentItem] AS opi
		INNER JOIN @Items AS i
		ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId 
		WHERE i.ItemState = 'D'


	-- Update ReceiveOrder.Status of the ReceiveOrder item that this OutgoingPayment item refers to
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT DISTINCT ro.ReceiveOrderId
		FROM [ReceiveOrder] ro
		INNER JOIN [OutgoingPaymentItem] opi
			ON ro.ReceiveOrderId = opi.ReceiveOrderId
				AND ro.IsDisabled <> 1
				AND ro.Status <> 'Void'
				AND opi.IsDisabled <> 1
				AND opi.OutgoingPaymentId = @targetOpId;

	EXEC dbo.spReceiveOrderStatusUpdate @updateItemList;


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO