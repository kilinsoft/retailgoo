USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spIncomingPaymentRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spIncomingPaymentRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spIncomingPaymentRetrieve]
    @IncomingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		ip.*,
		cust.CustomerName,
		pt.PaymentTypeName
	FROM [IncomingPayment] ip
	LEFT JOIN [Customer] cust
		ON ip.CustomerId = cust.CustomerId
	INNER JOIN [PaymentType] pt
		ON ip.PaymentTypeId = pt.PaymentTypeId
	WHERE	ip.IncomingPaymentId = @IncomingPaymentId;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;