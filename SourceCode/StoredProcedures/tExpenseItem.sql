IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tExpenseItem')
   EXEC sp_droptype tExpenseItem
GO
CREATE TYPE tExpenseItem AS TABLE (
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ExpenseNo] [nvarchar](50) NOT NULL,
	[ExpenseName] [nvarchar](100) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL
);
GO
