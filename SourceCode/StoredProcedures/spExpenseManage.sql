USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spExpenseManage')
   EXEC('CREATE PROCEDURE [dbo].[spExpenseManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spExpenseManage]
	@BusinessId bigint,
	@ExpenseId bigint,
	@ExpenseNo nvarchar(100),
	@SupplierId bigint,
    @ExpenseCategoryId bigint,
    @Amount decimal(18,4),
	@Date date,
	@Note nvarchar(max),
	@UseWht bit,
	@WhtPercent decimal(18,4),
	@WhtAmount decimal(18,4),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1)
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @insertedItemId BIGINT;
	SET @insertedItemId = @ExpenseId;

	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextItemName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Expense', @RetDocName = @nextItemName OUT;

			-- Insert 
			INSERT INTO [dbo].[Expense] ([BusinessId], [ExpenseNo], [Amount], [UseWht], [WhtPercent], [WhtAmount], [SupplierId], [ExpenseCategoryId], [Note], [Date], [IsVoided], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextItemName, @Amount, @UseWht, @WhtPercent, @WhtAmount, @SupplierId, @ExpenseCategoryId, @Note, @Date, 0, 0, @dateNow, @dateNow, @username, @username
	
			SET @insertedItemId = SCOPE_IDENTITY();
		END
	--ELSE IF @ItemState = 'M'
	--	BEGIN
	--		UPDATE [dbo].[OutgoingPayment]
	--		SET    
	--		[PayeeId] = @PayeeId, [Reference] = @Reference, [Contact] = @Contact, [Detail1] = @Detail1, [Detail2] = @Detail2, 
	--		[Detail3] = @Detail3, [Phone] = @Phone, [Date] = @Date, [PaymentMethod] = @PaymentMethod, [Note] = @Note, 
	--		[TaxId] = @TaxId, [Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total, [Status] = @Status, [IsVoided] = @IsVoided, 
	--		[IsDeleted] = @IsDeleted, [DateModified] = @dateNow, [UpdatedBy] = @User
	--		WHERE  
	--		[OutgoingPaymentId] = @OutgoingPaymentId
	--	END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Expense]
			SET    
			[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[ExpenseId] = @ExpenseId
		END

	---- Manage subItems
	------ Insert Subitems
	--INSERT INTO [dbo].[OutgoingPaymentItem] 
	--	([BusinessId], [OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [AmountPay], [Description], [IsDeleted], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	--SELECT 
	--	@BusinessId, @insertedItemId, ReceiveOrderId, SupplierInvoice, AmountPay, Description, IsDeleted, @dateNow, @dateNow, @User, @User
	--	FROM @Items
	--	WHERE ItemState = 'N';

	------ Update Subitems
	--UPDATE opi
	--SET	
	--	opi.SupplierInvoice = i.SupplierInvoice, 
	--	opi.ReceiveOrderId = i.ReceiveOrderId, 
	--	opi.Description = i.Description, 
	--	opi.AmountPay = i.AmountPay, 
	--	opi.DateModified = @dateNow, 
	--	opi.UpdatedBy = @User
	--FROM [dbo].[OutgoingPaymentItem] AS opi
	--	INNER JOIN @Items AS i
	--	ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId
	--	WHERE i.ItemState = 'M'

	------ Disable Subitems
	--UPDATE opi
	--SET opi.IsDeleted = 1, opi.DateModified = @dateNow, opi.UpdatedBy = @User
	--FROM [dbo].[OutgoingPaymentItem] AS opi
	--	INNER JOIN @Items AS i
	--	ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId 
	--	WHERE i.ItemState = 'D'

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO