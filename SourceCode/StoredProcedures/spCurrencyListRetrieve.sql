USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCurrencyListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCurrencyListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCurrencyListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		CurrencyId AS Value, 
		CurrencyName As Text 
	from 
		Currency

SET @ReturnValue = 0;
RETURN @ReturnValue;