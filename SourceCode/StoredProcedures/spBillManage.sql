USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBillManage')
   EXEC('CREATE PROCEDURE [dbo].[spBillManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBillManage]
	@BusinessId bigint,
	@CustomerId bigint,
	@BillId	bigint,
    @DiscountAmount decimal(18,4),
    @DiscountPercent decimal(18,4),
    @IsDiscountPercent bit,
	@Subtotal decimal(18,4),
    @Total decimal(18,4),
	@Tax decimal(18,4),
	@Note nvarchar(200),
	@Status varchar(10),
	@BillItems tBillItem readonly,
	@BillPaymentItems tBillPaymentItem readonly,
	@UserId bigint,
	@ItemState char(1),
	@Version datetime,
	@NewBillId bigint OUTPUT
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @Username varchar(50);
	DECLARE @billStatus VARCHAR(10);
		SET	@billStatus = @Status;

	DECLARE @targetId BIGINT;
		SET	@targetId = @BillId;

	exec @Username = GetUsername @UserId = @UserId;

	-- 1. Manage [Bill] table
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Bill]
			   ([BusinessId]
			   ,[CustomerId]
			   ,[DiscountAmount]
			   ,[DiscountPercent]
			   ,[IsDiscountPercent]
			   ,[Subtotal]
			   ,[Total]
			   ,[Tax]
			   ,[Status]
			   ,[Note]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
				(@BusinessId, @CustomerId, @DiscountAmount, @DiscountPercent, @IsDiscountPercent, @Subtotal, @Total, @Tax, @billStatus, @Note, 0, @dateNow, @dateNow, @Username, @Username);

			SET @targetId = SCOPE_IDENTITY();
			SELECT @NewBillId = @targetId;
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @billStatus = @Status;
			UPDATE [dbo].[Bill]
			   SET [CustomerId] = @CustomerId
				  ,[DiscountAmount] = @DiscountAmount
				  ,[DiscountPercent] = @DiscountPercent
				  ,[IsDiscountPercent] = @IsDiscountPercent
				  ,[Subtotal] = @Subtotal
				  ,[Total] = @Total
				  ,[Tax] = @Tax
				  ,[Status] = @billStatus
				  ,[Note] = @Note
				  ,[DateModified] = @dateNow
				  ,[UpdatedBy] = @dateNow
			 WHERE BillId = @BillId;
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Bill]
			SET    
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @Username
			WHERE  
				BillId = @BillId
		END


	-- 2. Manage BillItems
	---- Insert 
	INSERT INTO [dbo].[BillItem]
           ([BillId]
           ,[ItemId]
           ,[Quantity]
		   ,[Discount]
		   ,[Price]
           ,[IsDisabled])
	SELECT BillId = @targetId, ItemId, Quantity, Discount, Price, 0
		FROM @BillItems
		WHERE ItemState = 'N';

	---- Update 
	UPDATE bi
	SET bi.Quantity = i.Quantity, bi.Discount = i.Discount
		FROM [dbo].[BillItem] AS bi
		INNER JOIN @BillItems AS i
			ON bi.BillItemId = i.BillItemId 
		WHERE i.ItemState = 'M'

	---- Disable 
	UPDATE bi
	SET bi.IsDisabled = 1
		FROM [dbo].[BillItem] AS bi
		INNER JOIN @BillItems AS bi2
			ON bi.BillItemId = bi2.BillItemId 
		WHERE bi2.ItemState = 'D'

	-- 3. Manage BillPaymentItems
	---- Insert 
	INSERT INTO [dbo].[BillPaymentItem]
           ([BillId]
			,[PaymentTypeId]
			,[Amount]
			,[IsDisabled])
	SELECT BillId = @targetId, PaymentTypeId, Amount, 0
		FROM @BillPaymentItems
		WHERE ItemState = 'N';

	---- Update: There is no GUI to update BillPaymentItems

	---- Disable: 
	UPDATE bpi
	SET bpi.IsDisabled = 1
		FROM [dbo].[BillPaymentItem] AS bpi
		INNER JOIN @BillPaymentItems AS bpi2
			ON bpi.BillPaymentItemId = bpi2.BillPaymentItemId 
		WHERE bpi2.ItemState = 'D'

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO