USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesPersonListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSalesPersonListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesPersonListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT s.StaffId AS Value, s.Identification AS Text
	FROM [dbo].[Staff] s
	WHERE
		s.Status = 'active'

SET @ReturnValue = 0;
RETURN @ReturnValue;