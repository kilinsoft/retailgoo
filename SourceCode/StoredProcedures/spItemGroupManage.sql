USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemGroupManage')
   EXEC('CREATE PROCEDURE [dbo].[spItemGroupManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemGroupManage]
	@BusinessId bigint,
	@ItemGroupId bigint,
    @ItemGroupCode varchar(50),
    @ItemGroupName nvarchar(50),
    @ImagePath varchar(max),
    @ItemCategoryId bigint,
    @ItemBrandId bigint,
    @Cost decimal(18,4),
    @Price decimal(18,4),
    @Uom nvarchar(50),
    @IsForSales bit,
    @UserId bigint,
	@ItemState char(1),
	@Version datetime,
	@Items tItem readonly
AS
BEGIN TRY

	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	declare @dateNow datetime;
		set @dateNow = getdate();
	declare @username varchar(50);
		exec @username = GetUsername @UserId = @UserId;
	declare @targetItemGroupId bigint;
		set @targetItemGroupId = @ItemGroupId

	IF @ItemState = 'N'
		BEGIN
			insert into ItemGroup
				([BusinessId]
			   ,[ItemGroupCode]
			   ,[ItemGroupName]
			   ,[ImagePath]
			   ,[ItemCategoryId]
			   ,[ItemBrandId]
			   ,[Cost]
			   ,[Price]
			   ,[Uom]
			   ,[IsForSales]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
		   VALUES 
		   ( 
			   @BusinessId, @ItemGroupCode, @ItemGroupName,
			   @ImagePath,
			   @ItemCategoryId, @ItemBrandId, @Cost, @Price, @Uom, @IsForSales, 0, 
			   @dateNow, @dateNow, @username, @username
			);
			SET @targetItemGroupId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			update ItemGroup
			set 
			   ItemGroupCode = @ItemGroupCode, ItemGroupName = @ItemGroupName,
			   ImagePath = @ImagePath,
			   ItemCategoryId = @ItemCategoryId, 
			   ItemBrandId = @ItemBrandId, Cost = @Cost, Price = @Price, Uom = @Uom, IsForSales = @IsForSales,
			   DateCreated = @dateNow, DateModified = @dateNow, UpdatedBy = @username
			where
				ItemGroupId = @ItemGroupId;
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			update ItemGroup
			set
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @username
			where
				ItemGroupId = @ItemGroupId
		END

	-- Manage Items
	---- Insert Items
	INSERT INTO Item
        ([ItemGroupId]
        ,[ItemName]
        ,[Barcode]
        ,[Quantity]
        ,[Cost]
        ,[Price]
        ,[Price02]
        ,[Price03]
        ,[Price04]
        ,[IsForSales]
        ,[IsDisabled]
        ,[DateCreated]
        ,[DateModified]
        ,[CreatedBy]
        ,[UpdatedBy])
		SELECT 
			ItemGroupId = @targetItemGroupId, ItemName, Barcode, Quantity, Cost, Price, Price02 = 0, Price03 = 0, Price04 = 0,
			IsForSales, 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username 
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Items
	UPDATE Item
	SET
		ItemName = i.ItemName, Barcode = i.Barcode, Quantity = i.Quantity, Cost = i.Cost, Price = i.Price, IsForSales = i.IsForSales, IsDisabled = 0,
		DateModified = @dateNow, UpdatedBy = @username
	FROM @Items i
		WHERE 
			ItemState = 'M'
		And i.ItemId = Item.ItemId;

	---- Delete Items
	UPDATE Item
	SET
		IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @username
	FROM @Items i
		WHERE ItemState = 'D'
		And i.ItemId = Item.ItemId;


	SET @ReturnValue = 0;
	RETURN @ReturnValue;


END TRY
BEGIN CATCH

	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO