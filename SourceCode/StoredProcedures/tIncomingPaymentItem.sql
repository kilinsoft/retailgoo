USE [retailgoo]
GO
IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tIncomingPaymentItem')
   EXEC sp_droptype tIncomingPaymentItem
GO
CREATE TYPE dbo.tIncomingPaymentItem AS TABLE (
	[IncomingPaymentItemId] [bigint] NOT NULL,
	[IncomingPaymentId] [bigint] NOT NULL,
	[SalesInvoiceId] [bigint] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
