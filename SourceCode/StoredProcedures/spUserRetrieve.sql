USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spUserRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spUserRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spUserRetrieve]
    @UserId BIGINT,
	@Username VARCHAR(50),
	@Email VARCHAR(100)
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT * 
	FROM [User]
	WHERE
		(@UserId IS NULL OR (UserId = @UserId)) AND
		(@Username IS NULL OR (Username = @Username)) AND
		(@Email IS NULL OR (Email = @Email));

SET @ReturnValue = 0;
RETURN @ReturnValue;