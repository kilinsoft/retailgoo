USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderItemAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderItemAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderItemAllRetrieve]
	@PurchaseOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT 
		poi.*, 
		dbo.fnGetItemFullName( ig.ItemGroupCode, ig.ItemGroupName, i.ItemName ) AS ItemFullName,
		ig.Uom, 
		(poi.Quantity * poi.Price) AS Amount
	FROM PurchaseOrder po
	INNER JOIN PurchaseOrderItem poi
		ON poi.PurchaseOrderId = po.PurchaseOrderId
	INNER JOIN Item i
		ON poi.ItemId = i.ItemId
	INNER JOIN ItemGroup ig
		ON i.ItemGroupId = ig.ItemGroupId
	WHERE
		po.PurchaseOrderId = @PurchaseOrderId;


SET @ReturnValue = 0;
RETURN @ReturnValue;