USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesInvoiceManage')
   EXEC('CREATE PROCEDURE [dbo].[spSalesInvoiceManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesInvoiceManage]
	@SalesInvoiceId bigint,
	@BusinessId bigint,
    @IsCashSales bit,
    @CustomerId bigint,
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @DateDelivery date,
    @DestinationTypeId tinyint,
	@PriceTypeId tinyint,
	@AmountTypeId tinyint,
	@PaymentTypeId tinyint = 1,
    @Note nvarchar(MAX),
	@SalesPersonId bigint,
    @Subtotal decimal(18, 4),
	@Discount decimal(18, 4),
	@Charge decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
	@AmountPaid decimal(18, 4),
	--@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tSalesInvoiceItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetSiId BIGINT;
		SET @targetSiId = @SalesInvoiceId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	DECLARE @updateItemList tIdTable;

	-- 1. Manage records in [SalesInvoice] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextSiName NVARCHAR(50);
				SET @nextSiName = '';
			DECLARE @nextCsName NVARCHAR(50);
				SET @nextCsName = '';
			IF @IsCashSales = 1
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'CashSales', @RetDocName = @nextCsName OUT;
				END
			ELSE
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Invoice', @RetDocName = @nextSiName OUT;
				END

			-- Insert Sales Invoice
			INSERT INTO [dbo].[SalesInvoice] 
				([BusinessId],[SalesInvoiceNo],[IsCashSales], [CashSalesNo], [CustomerId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [DestinationTypeId], [PriceTypeId],[AmountTypeId], [PaymentTypeId], [Note], [SalesPersonId], [Subtotal], [Discount], [Charge], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextSiName, @IsCashSales, @nextCsName, @CustomerId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, @DateDelivery, @DestinationTypeId, @PriceTypeId, @AmountTypeId, @PaymentTypeId, @Note, @SalesPersonId, @Subtotal, @Discount, @Charge, @Tax, @Total, @AmountPaid, 'New', 0, @dateNow, @dateNow, @username, @username
			SET @targetSiId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @blockingItem = dbo.fnIsSalesInvoiceEditable(@SalesInvoiceId);

			IF @blockingItem <> N''
				BEGIN
					SET @ErrTxt = N'The opearation is blocked by ' + @blockingItem;
					RAISERROR( @ErrTxt, 16, 1 );
				END

			UPDATE [dbo].[SalesInvoice]
			SET    
			[CustomerId] = @CustomerId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, 
			[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, [Phone] = @Phone, [Date] = @Date, [DateDelivery] = @DateDelivery, 
			[DestinationTypeId] = @DestinationTypeId, [PriceTypeId] = @PriceTypeId, [AmountTypeId] = @AmountTypeId, [PaymentTypeId] = @PaymentTypeId,
			[Note] = @Note, [SalesPersonId] = @SalesPersonId, [Subtotal] = @Subtotal, [Discount] = @Discount, [Charge] = @Charge,
			[Tax] = @Tax, [Total] = @Total,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[SalesInvoiceId] = @SalesInvoiceId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[SalesInvoice]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[SalesInvoiceId] = @SalesInvoiceId
		END

	-- 2. Revert [Item].Quantity for [SalesInvoiceItem] records in case they are modified. We will update with new
	--    SalesInvoiceItem.Quantity below
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + sii.Quantity
	FROM [SalesInvoiceItem] sii
	INNER JOIN @Items i
		ON sii.SalesInvoiceItemId = i.SalesInvoiceItemId
			AND (i.ItemState = 'M' OR i.ItemState = 'D')
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @SalesInvoiceId;


	-- 3. Manage SubItems
	---- Insert Sales Invoice's items
	INSERT INTO [dbo].[SalesInvoiceItem]
	    ([SalesInvoiceId], [ItemId], [Price], [Quantity], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT SalesInvoiceId = @targetSiId, ItemId, Price, Quantity, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Sales Invoice's items
	UPDATE siItem
	SET siItem.ItemId = i.ItemId, siItem.Price = i.Price, siItem.Quantity = i.Quantity, 
	    siItem.DateModified = @dateNow, siItem.UpdatedBy = @username
	FROM [dbo].[SalesInvoiceItem] AS siItem
		INNER JOIN @Items AS i
		ON siItem.SalesInvoiceId = i.SalesInvoiceId 
		WHERE i.ItemState = 'M'

	---- Disable Sales Invoice's items
	UPDATE siItem
	SET siItem.IsDisabled = 1, siItem.DateModified = @dateNow, siItem.UpdatedBy = @username
	FROM [dbo].[SalesInvoiceItem] AS siItem
		INNER JOIN @Items AS i
		ON siItem.SalesInvoiceId = i.SalesInvoiceId 
		WHERE i.ItemState = 'D'


	-- 4. Re-calculate the SalesInvoice.Status again in case that the SalesInvoiceItem item is changed
	IF @ItemState = 'M'
		BEGIN
			INSERT INTO @updateItemList
			SELECT si.SalesInvoiceId FROM [SalesInvoice] si WHERE si.SalesInvoiceId = @targetSiId;

			EXEC dbo.spSalesInvoiceStatusUpdate @updateItemList;
		END

	-- 5. In case of IsCaseSales=1, we have to create IncomingPayment items associated to this SalesInvoice as well
	IF @IsCashSales = 1
	BEGIN
		IF @ItemState = 'N'
		BEGIN
		
			DECLARE @nextIpItemName NVARCHAR(50);
			DECLARE @newIpId BIGINT;

			-- Create OutgoingPayment item
			--EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextIpItemName OUT;

			INSERT INTO [dbo].[IncomingPayment] 
				([BusinessId], [IncomingPaymentNo], [IsCashSales], [CustomerId], [Reference], [PaymentTypeId], [BankId], [BankAccNo], [Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextCsName, 1, @CustomerId, @Reference, @PaymentTypeId, 1, N'', @Address1, @Address2, @Address3, @Phone, @Date, 0, 0, 0, @Note, @Subtotal, @Tax, @Total, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @newIpId = SCOPE_IDENTITY();

			INSERT INTO [dbo].[IncomingPaymentItem] 
				([IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@newIpId, @targetSiId, @Total, 0, @dateNow, @dateNow, @username, @username;

		END
	END

	-- 6. Update Item.Quantity to reflect SubItems with ItemState = 'N' or ItemState = 'M'
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity - sii.Quantity
	FROM [SalesInvoiceItem] sii
	INNER JOIN @Items i
		ON sii.SalesInvoiceItemId = i.SalesInvoiceItemId
			AND (i.ItemState = 'N' OR i.ItemState = 'M')
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @targetSiId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO