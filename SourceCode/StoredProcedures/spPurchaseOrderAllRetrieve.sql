USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderAllRetrieve]
    @BusinessId BIGINT,
	--@FilterColumn VARCHAR(50) = NULL,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50),
	--@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT,
	@MaxCount INT
	--@DateFrom Date = NULL,
	--@DateTo  Date = NULL
AS
	--SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	--DECLARE @sqlCommand AS NVARCHAR(MAX);
	--DECLARE @filterSql AS NVARCHAR(250);
	--DECLARE @sortingSql AS NVARCHAR(250);
	--DECLARE @OutputColumnSQL AS NVARCHAR(250);
	
	--DECLARE @TmpKeyword NVARCHAR(MAX);

	IF @FilterKeyword IS NOT NULL
		BEGIN
			SELECT @FilterKeyword = '%' + LTRIM(RTRIM(@FilterKeyword)) + '%';
		END

	DECLARE @TempTable TABLE 
	(
		PurchaseOrderId	BIGINT,
		PurchaseOrderNo NVarChar(100),
		Date Date,
		Status VarChar(10),
		SupplierName NVarChar(100),
		Total Decimal(18,4),
		DateCreated DateTime,
		DateModified DateTime
	);

	INSERT INTO 
		@TempTable
	SELECT
		po.PurchaseOrderId, po.PurchaseOrderNo, po.Date, po.Status, s.SupplierName, po.Total, po.DateCreated, po.DateModified
	FROM [PurchaseOrder] po
	LEFT JOIN [Supplier] s
		ON po.SupplierId = s.SupplierId
	INNER JOIN [PurchaseOrderItem] poi
		ON poi.PurchaseOrderId = po.PurchaseOrderId
	INNER JOIN [Item] item
		ON poi.ItemId = item.ItemId
	INNER JOIN [ItemGroup] itemGroup
		ON item.ItemGroupId = itemGroup.ItemGroupId
	WHERE	po.BusinessId = @BusinessId
		AND po.IsDisabled <> 1 
		AND po.IsCashPurchase <> 1
		AND (	(po.PurchaseOrderNo LIKE @FilterKeyword OR @FilterKeyword is NULL) OR 
				(s.SupplierName LIKE @FilterKeyword OR @FilterKeyword is NULL) OR 
				(item.ItemName LIKE @FilterKeyword OR @FilterKeyword IS NULL) OR
				(itemGroup.ItemGroupName LIKE @FilterKeyword OR @FilterKeyword IS NULL)
			)
	GROUP BY po.PurchaseOrderId, po.PurchaseOrderNo, po.Date, po.Status, s.SupplierName, po.Total, po.DateCreated, po.DateModified;
	
	SET @ReturnValue = @@ROWCOUNT;

	SELECT *
	FROM @TempTable
	ORDER BY 
		(
			CASE @SortingColumn
				WHEN 'DateCreated' THEN [DateCreated]
				WHEN 'DateModified' THEN [DateModified]
			END
		) DESC
	OFFSET @IndexStart ROWS
	FETCH NEXT @MaxCount ROWS ONLY;

	RETURN @ReturnValue;
	--ORDER BY po.PurchaseOrderId DESC;

	--SELECT
	--	po.PurchaseOrderId, po.PurchaseOrderNo, po.Date, po.Status, s.SupplierName,
	--	CASE 
	--		WHEN tmpSumPoi.PurchaseOrderId IS NULL THEN 0
	--		ELSE CONVERT( DECIMAL(18,4), tmpSumPoi.Amount )
	--	END AS Amount
	--FROM [PurchaseOrder] po
	--LEFT JOIN [Supplier] s
	--	ON po.SupplierId = s.SupplierId
	--LEFT JOIN (
	--	SELECT poi.PurchaseOrderId, SUM(poi.Quantity * poi.Price) as Amount
	--	FROM [PurchaseOrderItem] poi
	--	INNER JOIN [PurchaseOrder] po
	--		ON poi.PurchaseOrderId = po.PurchaseOrderId
	--	WHERE	po.BusinessId = @BusinessId
	--		AND poi.IsDisabled <> 1
	--	GROUP BY poi.PurchaseOrderId
	--) tmpSumPoi
	--	ON po.PurchaseOrderId = tmpSumPoi.PurchaseOrderId
	--WHERE	po.BusinessId = @BusinessId
	--	AND po.IsDisabled <> 1 
	

--DECLARE @SQLCommand AS NVARCHAR(750);
--DECLARE @FilterColumnSQL AS NVARCHAR(250);
--DECLARE @SortingSQL AS NVARCHAR(250);
--DECLARE @OutputColumnSQL AS NVARCHAR(250);

--SET @FilterColumnSQL = ' WHERE POITbl.BusinessId='+CONVERT(varchar(30),@BusinessId)

--SET @OutputColumnSQL =' POTbl.PurchaseOrderId,POTbl.PurchaseOrderNo,POTbl.Date ,POTbl.Status,POTbl.SupplierId,SPTbl.SupplierName '
--SET @OutputColumnSQL = @OutputColumnSQL + ' , ( SELECT SUM(Amount) FROM [dbo].[PurchaseOrderItem]  POITbl WITH (NOLOCK)  WHERE POTbl.PurchaseOrderId = POITbl.PurchaseOrderId )  As Amount '

--SET @OutputColumnSQL = @OutputColumnSQL + 	' FROM [dbo].[PurchaseOrder]  POTbl INNER JOIN [dbo].[Supplier] SPTbl ON POTbl.SupplierId=SPTbl.SupplierId '
--	-- Compose Filter Column 
--	IF ( @DateFrom IS NOT NULL)
--	BEGIN
--		SET @FilterColumnSQL = @FilterColumnSQL + ' AND ( POTbl.DateCreated BETWEEN CONVERT(datetime,'+ CONVERT(nvarchar(30),@DateFrom) +') AND CONVERT(datetime,'+ CONVERT(nvarchar(30),@DateTo) +') ) '
--	END

--	IF (@MaxCount > 0 ) 
--	BEGIN
--		SET @FilterColumnSQL = @FilterColumnSQL + ' AND (POTbl.ROWNUMBERS BETWEEN ' + CONVERT(nvarchar(30),@IndexStart) + ' AND ' + CONVERT(nvarchar(30),@MaxCount) + ' ) '
--	END

--	IF ( @FilterColumn IS NOT NULL)
--	BEGIN
--		SET @FilterColumnSQL = @FilterColumnSQL + ' AND '+ @FilterColumn +' LIKE ' +@FilterKeyword
--	END	
	
	
--	-- Compose Sorting 
--	IF (@SortingColumn IS NOT NULL)
--	BEGIN
--		SET @SortingSQL =' ORDER BY ' + @SortingColumn + @SortingDirection;
--	END 

--	SET @SQLCommand = 'SELECT ' + @OutputColumnSQL + @FilterColumnSQL + @SortingSQL

--	EXEC (@SQLCommand)








	---- 1. Select base PO
	--SELECT 
	--	*, 
	--	CONVERT(DECIMAL(18,4), 0) AS Quantity, 
	--	CONVERT(DECIMAL(18,4), 0) AS QuantityDue, 
	--	CONVERT(VARCHAR(16), 'New') AS Status2 INTO #tmpPo
	--FROM [PurchaseOrder]
	--WHERE BusinessId = @BusinessId
	--	AND IsDeleted <> 1
	--ORDER BY Id DESC

	---- 2. Select base POI
	--SELECT 
	--	PurchaseOrderId, 
	--	CONVERT(DECIMAL(18,4), Sum(Quantity)) AS Quantity, 
	--	CONVERT(DECIMAL(18,4), 0) AS QuantityReceived, 
	--	CONVERT(VARCHAR(16), 'New') AS Status2 INTO #tmpPoi
	--FROM [PurchaseOrderItem]
	--WHERE BusinessId = @BusinessId
	--GROUP BY PurchaseOrderId

	---- 3. Select Sum(QuantityReceived) from ROI
	--SELECT 
	--	roi.PurchaseOrderId, 
	--	CONVERT(DECIMAL(18,4), SUM(roi.Quantity)) AS QuantityReceived INTO #tmpRoiAmt
	--FROM [ReceiveOrderItem] roi
	--INNER JOIN [ReceiveOrder] ro
	--	ON roi.ReceiveOrderId = ro.Id
	--WHERE roi.BusinessId = @BusinessId
	--	AND ro.IsVoided <> 1
	--	AND roi.IsDeleted <> 1
	--GROUP BY roi.PurchaseOrderId;

	---- 4. Update tmpPoi with Sum(QuantityReceived) from tmpRoiAmt
	--UPDATE #tmpPoi
	--SET
	--	QuantityReceived = roi.QuantityReceived
	--FROM #tmpRoiAmt roi
	--WHERE #tmpPoi.PurchaseOrderId = roi.PurchaseOrderId

	---- 5. Update tmpPo
	--UPDATE #tmpPo
	--SET 
	--	Quantity = poiAmt.Quantity,
	--	QuantityDue = (poiAmt.Quantity - poiAmt.QuantityReceived)
	--FROM #tmpPoi poiAmt
	--WHERE #tmpPo.Id = poiAmt.PurchaseOrderId

	---- 6. Update Status2
	--UPDATE #tmpPo
	--SET
	--	STATUS2 = 
	--		CASE 
	--			WHEN (IsVoided = 1) THEN 'Void'
	--			WHEN (QuantityDue <= 0) THEN 'Done'
	--			WHEN (QuantityDue >= Quantity) THEN 'New'
	--			ELSE 'Partial'
	--		END;


	--SELECT * FROM #tmpPo;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;