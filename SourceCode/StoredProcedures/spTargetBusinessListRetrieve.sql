USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spTargetBusinessListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spTargetBusinessListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spTargetBusinessListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT
		other.BusinessId AS Value,
		b.BusinessName AS Text
	FROM 
	(
		SELECT
			br.BusinessId,
			br.RootBusinessId
		FROM [BusinessRelation] br
		WHERE br.BusinessId <> @BusinessId -- Ignore self
	) other
	INNER JOIN [Business] b
		ON	other.BusinessId = b.BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;