USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[GetUsername]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnGetItemFullName', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnGetItemFullName];
GO
CREATE FUNCTION [dbo].[fnGetItemFullName](@ItemGroupCode varchar(50), @ItemGroupName nvarchar(100), @ItemName nvarchar(100))
	RETURNS nvarchar(250) 
AS
BEGIN
	declare @fullname nvarchar(250);
		set @fullname = N'<undefined>';
	if (@ItemGroupName = @ItemName) 
		begin
			set @fullname = concat( cast(@ItemGroupCode as nvarchar(50)), N' ', @ItemGroupName );
		end
	else
		begin
			set @fullname = concat( cast(@ItemGroupCode as nvarchar(50)), N' ', @ItemGroupName, N' - ', @ItemName);
		end

	return @fullname;
END
