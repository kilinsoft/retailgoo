USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemAdjustmentAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemAdjustmentAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemAdjustmentAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		ia.*
	FROM [ItemAdjustment] ia
	WHERE	ia.BusinessId = @BusinessId
		AND ia.IsDisabled <> 1
	ORDER BY ia.AdjustmentId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;


GO