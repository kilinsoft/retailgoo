USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spPurchaseOrderManage')
   EXEC('CREATE PROCEDURE [dbo].[spPurchaseOrderManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spPurchaseOrderManage]
	@PurchaseOrderId bigint,
	@BusinessId bigint,
    @IsCashPurchase bit,
    @SupplierId bigint,
    --@SupplierName nvarchar(100),
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @DateDelivery date,
    @AmountTypeId tinyint,
    @PaymentTypeId tinyint,
    @Note nvarchar(MAX),
    @CreditTerm nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
    --@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tPurchaseOrderItem READONLY
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetPoId BIGINT;
		SET @targetPoId = @PurchaseOrderId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	DECLARE @updateItemList tIdTable;

	-- 1. Manage [PurchaseOrder] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextPoName NVARCHAR(50);
				SET @nextPoName = '';
			DECLARE @nextCpName NVARCHAR(50);
				SET @nextCpName = '';
			
			IF @IsCashPurchase = 1
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'CashPurchase', @RetDocName = @nextCpName OUT;
				END
			ELSE
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'PurchaseOrder', @RetDocName = @nextPoName OUT;
				END

			-- Insert Purchase Order
			INSERT INTO [dbo].[PurchaseOrder] 
				([BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextPoName, @IsCashPurchase, @nextCpName, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, @DateDelivery, @AmountTypeId, @PaymentTypeId, @Note, @CreditTerm, @Subtotal, @Tax, @Total, 
				CASE /* Status */
					WHEN @IsCashPurchase = 1 THEN 'Done'
					ELSE @status
				END, 
				0, @dateNow, @dateNow, @username, @username
			SET @targetPoId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @blockingItem = dbo.fnIsPurchaseOrderEditable(@PurchaseOrderId);

			IF @blockingItem <> N''
				BEGIN
					SET @ErrTxt = N'The opearation is blocked by ' + @blockingItem;
					RAISERROR( @ErrTxt, 16, 1 );
				END

			UPDATE [dbo].[PurchaseOrder]
			SET    
			[SupplierId] = @SupplierId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, 
			[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, [Phone] = @Phone, [Date] = @Date, [DateDelivery] = @DateDelivery, 
			[AmountTypeId] = @AmountTypeId, [PaymentTypeId] = @PaymentTypeId, [Note] = @Note, [CreditTerm] = @CreditTerm, [Subtotal] = @Subtotal, 
			[Tax] = @Tax, [Total] = @Total,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[PurchaseOrderId] = @PurchaseOrderId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[PurchaseOrder]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[PurchaseOrderId] = @PurchaseOrderId
		END


	-- 2. Manage SubItems
	---- Insert Purchase Order's items
	INSERT INTO [dbo].[PurchaseOrderItem] ([PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT PurchaseOrderId = @targetPoId, ItemId, Quantity, 0, Price, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Purchase Order's items
	UPDATE poi
	SET poi.ItemId = i.ItemId, poi.Quantity = i.Quantity,
		poi.Price = i.Price, poi.DateModified = @dateNow, poi.UpdatedBy = @username
	FROM [dbo].[PurchaseOrderItem] AS poi
		INNER JOIN @Items AS i
		ON poi.PurchaseOrderItemId = i.PurchaseOrderItemId 
		WHERE i.ItemState = 'M'

	---- Disable Purchase Order's items
	UPDATE poi
	SET poi.IsDisabled = 1, poi.DateModified = @dateNow, poi.UpdatedBy = @username
	FROM [dbo].[PurchaseOrderItem] AS poi
		INNER JOIN @Items AS i
		ON poi.PurchaseOrderItemId = i.PurchaseOrderItemId 
		WHERE i.ItemState = 'D'


	-- 3. Re-calculate PurchaseOrder.Status again in case PurchaseOrderItem.Quantity changes
	IF @ItemState = 'M'
		BEGIN
			INSERT INTO @updateItemList
			SELECT po.PurchaseOrderId FROM [PurchaseOrder] po WHERE po.PurchaseOrderId = @targetPoId;

			EXEC dbo.spPurchaseOrderStatusUpdate @updateItemList;
		END
		

	-- 4. In case of IsCasePurchase=1, we have to create ReceiveOrder and OutgoingPayment items associated to this PurchaseOrder as well
	IF @IsCashPurchase = 1
	BEGIN
		IF @ItemState = 'N'
		BEGIN
		
			-- Create ReceiveOrder item
			--DECLARE @nextItemName NVARCHAR(50);
			DECLARE @newRoId BIGINT;
			DECLARE @newOpId BIGINT;
			--EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'ReceiveOrder', @RetDocName = @nextItemName OUT;

			-- Insert ReceiveOrder
			INSERT INTO [dbo].[ReceiveOrder] 
				([BusinessId], [ReceiveOrderNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextCpName, 1, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, @Note, N'', @Subtotal, @Tax, @Total, 0.00, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @newRoId = SCOPE_IDENTITY();

			INSERT INTO [ReceiveOrderItem]
				([ReceiveOrderId], [PurchaseOrderItemId], [ItemId], [Quantity], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@newRoId, poi.PurchaseOrderItemId, poi.ItemId, poi.Quantity, poi.Price, 0, @dateNow, @dateNow, @username, @username
			FROM [PurchaseOrderItem] poi
			WHERE	poi.PurchaseOrderId = @targetPoId;

			-- Update Item Quantity in the Item table
			UPDATE [Item]
			SET [Item].Quantity = ([Item].Quantity + newItems.Quantity)
			FROM 
				( 
					SELECT roi.Quantity, roi.ItemId
					FROM [ReceiveOrderItem] roi
					INNER JOIN [PurchaseOrderItem] poi
						ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
					INNER JOIN [PurchaseOrder] po
						ON poi.PurchaseOrderId = po.PurchaseOrderId
					WHERE	po.PurchaseOrderId = @targetPoId
				) newItems
			WHERE	[Item].ItemId = newItems.ItemId;

			-- Create OutgoingPayment item
			--EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextItemName OUT;

			INSERT INTO [dbo].[OutgoingPayment] 
				([BusinessId], [OutgoingPaymentNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Detail1], [Detail2], [Detail3], [Phone], [Date], [PaymentTypeId], [Note], [TaxIdInfo], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextCpName, 1, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, 1 /*Cash*/, @Note, N'', @Subtotal, @Tax, @Total, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @newOpId = SCOPE_IDENTITY();

			INSERT INTO [dbo].[OutgoingPaymentItem] 
				([OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [Amount], [Description], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@newOpId, @newRoId, N'', @Total, N'', 0, @dateNow, @dateNow, @username, @username;

		END
	END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO