USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCashSalesVoid')
   EXEC('CREATE PROCEDURE [dbo].[spCashSalesVoid] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCashSalesVoid]
	@SalesInvoiceId BIGINT,
	@UserId BIGINT
AS

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @IncomingPaymentId BIGINT;
		SET @IncomingPaymentId = -1;

	-- Search and void IncomingPayment, first
	SELECT TOP 1 @IncomingPaymentId = IncomingPaymentId
	FROM [IncomingPaymentItem]
	WHERE SalesInvoiceId = @SalesInvoiceId;

	IF @IncomingPaymentId <> -1
		BEGIN
			UPDATE [dbo].[IncomingPayment]
			SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
			WHERE IncomingPaymentId = @IncomingPaymentId;
		END

	-- Void SalesInvoice
	UPDATE [dbo].[SalesInvoice]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE SalesInvoiceId = @SalesInvoiceId;

	-- Update Item.Quantity due to void'ed SalesInvoice
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + sii.Quantity
	FROM [SalesInvoiceItem] sii
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @SalesInvoiceId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;