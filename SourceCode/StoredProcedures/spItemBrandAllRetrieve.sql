USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemBrandAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemBrandAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemBrandAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ib.ItemBrandId, ib.ItemBrandName, 
		CASE
			WHEN sum(i.Quantity) > 0 THEN sum(i.Quantity)
			ELSE 0
		END as ItemQuantity
	from ItemBrand ib
	left join ItemGroup ig
		on ig.ItemBrandId = ib.ItemBrandId
	left join Item i
		on i.ItemGroupId = ig.ItemGroupId 
	where
		ib.BusinessId = @BusinessId
		AND	ib.IsDisabled <> 1
		AND (ig.IsDisabled <> 1 OR ig.IsDisabled IS NULL)
		AND (i.IsDisabled <> 1 OR i.IsDisabled IS NULL)
	group by ib.ItemBrandId, ib.ItemBrandName;

SET @ReturnValue = 0;
RETURN @ReturnValue;