USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spGetItemPath')
   EXEC('CREATE PROCEDURE [dbo].[spGetItemPath] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spGetItemPath]
    @BusinessId bigint,
	@ItemGroupId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT ImagePath
	FROM ItemGroup
	WHERE BusinessId = @BusinessId
		AND ItemGroupId = @ItemGroupId

SET @ReturnValue = 0;
RETURN @ReturnValue;