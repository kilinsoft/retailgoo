USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBusinessStaffManage')
   EXEC('CREATE PROCEDURE [dbo].[spBusinessStaffManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBusinessStaffManage]
	@BusinessId bigint,
	@StaffUserId bigint,
	@StaffId bigint,
	@Email varchar(254),
    @Role varchar(10),
	@Status varchar(10),
	@UserId bigint,
	@ItemState varchar(1)
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
	   EXEC @username = GetUsername @UserId = @UserId;
	
	IF @ItemState = 'N'
		BEGIN
			---- Create a user for this staff but mark this user as pending. This is to wait for the user register with retailgoo system first.
			--DECLARE @newUserId BIGINT;
			--exec @newUserId = dbo.spUserCreate 
			--	@Username = @Email,
			--	@Password = NULL,
			--	@Salt = NULL,
			--	@Email = @Email,
			--	@Guid = '',
			--	@DateGuidExpired = NULL,
			--	@IsDisabled = 0,
			--	@IsFacebookUser = 0;

			---- Mark this newly created user as pending
			--INSERT INTO [dbo].[UserPending]
			--	   ([UserId]
			--	   ,[DateCreated])
			-- VALUES
			--	   (@newUserId, @dateNow);

			-- Then create the staff
			INSERT INTO [dbo].[Staff]
				   ([BusinessId]
				   ,[UserId]
				   ,[Title]
				   ,[Firstname]
				   ,[Lastname]
				   ,[Birthdate]
				   ,[Gender]
				   ,[Identification]
				   ,[Phone]
				   ,[Fax]
				   ,[Email]
				   ,[FacebookId]
				   ,[Address1]
				   ,[Address2]
				   ,[Address3]
				   ,[City]
				   ,[CountryId]
				   ,[ZipCode]
				   ,[Role]
				   ,[Status]
				   ,[IsDisabled]
				   ,[Guid]
				   ,[DateGuidExpired]
				   ,[DateCreated]
				   ,[DateModified]
				   ,[CreatedBy]
				   ,[UpdatedBy])
			 VALUES
				   (@BusinessId, @StaffUserId, NULL, '', '', NULL, NULL, '', '', '', @Email
				   ,'', '', '', '', '', NULL, '', @Role, @Status, 0, '', NULL, @dateNow, @dateNow, @username, @username );

		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET [Status] = @Status
				  ,[Role] = @Role
				  ,[DateModified] = @dateNow
			 WHERE StaffId = @StaffId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET	[Status] = 'inactive',
					[IsDisabled] = 1
			 WHERE StaffId = @StaffId;
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO