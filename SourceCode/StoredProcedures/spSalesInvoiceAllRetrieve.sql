USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesInvoiceAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSalesInvoiceAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesInvoiceAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		si.*, 
		cust.CustomerName,
		CONVERT(DECIMAL(18,4), si.Total - si.AmountPaid) AS AmountDue	
	FROM [SalesInvoice] si
	LEFT JOIN [Customer] cust
		ON si.CustomerId = cust.CustomerId
	WHERE	si.BusinessId = @BusinessId
		AND si.IsDisabled <> 1
		AND si.IsCashSales <> 1
	ORDER BY si.SalesInvoiceId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;


GO