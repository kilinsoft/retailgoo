USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spBusinessInfoRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spBusinessInfoRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spBusinessInfoRetrieve]
    @BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT b.*, btype.BusinessTypeName, c.CountryName, c.CountryShortName
	FROM Business b
	LEFT JOIN BusinessType btype 
		ON b.BusinessTypeId = btype.BusinessTypeId
	LEFT JOIN Country c
		ON b.CountryId = c.CountryId
	WHERE
		b.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;