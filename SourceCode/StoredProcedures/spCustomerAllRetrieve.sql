USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCustomerAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCustomerAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCustomerAllRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.CustomerId, s.CustomerName, s.CustomerNo, s.ContactPerson, s.Phone, sg.CustomerGroupName
	from Customer s
	left join CustomerGroup sg
		on s.CustomerGroupId = sg.CustomerGroupId
	where 
			s.BusinessId = @BusinessId
		and	s.IsDisabled = 0

SET @ReturnValue = 0;
RETURN @ReturnValue;