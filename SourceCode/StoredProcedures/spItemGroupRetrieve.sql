USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemGroupRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemGroupRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemGroupRetrieve]
	@ItemGroupId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.*, iCate.ItemCategoryName, iBrand.ItemBrandName
	from ItemGroup ig
	left join ItemCategory iCate
		on ig.ItemCategoryId = iCate.ItemCategoryId
	left join ItemBrand iBrand
		on ig.ItemBrandId = iBrand.ItemBrandId
	where
			ig.ItemGroupId = @ItemGroupId
		AND	ig.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;