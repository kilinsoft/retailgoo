USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemAdjustmentRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemAdjustmentRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemAdjustmentRetrieve]
	@AdjustmentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		ia.*
	FROM [ItemAdjustment] ia
	WHERE ia.AdjustmentId = @AdjustmentId

SET @ReturnValue = 0;
RETURN @ReturnValue;
