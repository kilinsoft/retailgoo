USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCountryListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCountryListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCountryListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select c.CountryId as Value, c.CountryName as Text
	from Country c
	--select c.CountryShortName as Value, c.CountryName as Text
	--from Country c

SET @ReturnValue = 0;
RETURN @ReturnValue;