USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spStaffInfoManage')
   EXEC('CREATE PROCEDURE [dbo].[spStaffInfoManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spStaffInfoManage]
	@BusinessId bigint,
	@StaffId bigint,
	@StaffUserId bigint,
	@Title varchar(4),
    @Firstname nvarchar(50),
    @Lastname nvarchar(50),
    @Birthdate date,
    @Gender char(1),
	@Identification nvarchar(50),
    @Phone nvarchar(50),
    @Fax nvarchar(50),
	@Email varchar(254),
    @FacebookId nvarchar(50),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @City nvarchar(50),
    @CountryId tinyint,
    @ZipCode nvarchar(50),
    @Role varchar(10),
	@Status varchar(10),
	@ItemState varchar(1),
	@Guid varchar(50),
	@DateGuidExpired DateTime,
	@UserId bigint
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
	   EXEC @username = GetUsername @UserId = @UserId;
	
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Staff]
				   ([BusinessId]
				   ,[UserId]
				   ,[Title]
				   ,[Firstname]
				   ,[Lastname]
				   ,[Birthdate]
				   ,[Gender]
				   ,[Identification]
				   ,[Phone]
				   ,[Fax]
				   ,[Email]
				   ,[FacebookId]
				   ,[Address1]
				   ,[Address2]
				   ,[Address3]
				   ,[City]
				   ,[CountryId]
				   ,[ZipCode]
				   ,[Role]
				   ,[Status]
				   ,[IsDisabled]
				   ,[Guid]
				   ,[DateGuidExpired]
				   ,[DateCreated]
				   ,[DateModified]
				   ,[CreatedBy]
				   ,[UpdatedBy])
			 VALUES
				   (@BusinessId, @StaffUserId, @Title, @Firstname, @Lastname, @Birthdate, @Gender, @Identification, @Phone, @Fax, @Email
				   ,@FacebookId, @Address1, @Address2, @Address3, @City, @CountryId, @ZipCode, @Role, @Status, 0, @Guid, @DateGuidExpired, @dateNow, @dateNow, @username, @username )
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET [Title] = @Title
				  ,[Firstname] = @Firstname
				  ,[Lastname] = @Lastname
				  ,[Birthdate] = @Birthdate
				  ,[Gender] = @Gender
				  ,[Identification] = @Identification
				  ,[Phone] = @Phone
				  ,[Fax] = @Fax
				  ,[FacebookId] = @FacebookId
				  ,[Email] = @Email
				  ,[Address1] = @Address1
				  ,[Address2] = @Address2
				  ,[Address3] = @Address3
				  ,[City] = @City
				  ,[CountryId] = @CountryId
				  ,[ZipCode] = @ZipCode
				  ,[Role] = @Role
				  ,[DateModified] = @dateNow
			 WHERE StaffId = @StaffId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET	[Status] = 'inactive',
					[IsDisabled] = 1
			 WHERE StaffId = @StaffId;
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO