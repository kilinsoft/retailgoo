USE [retailgoo]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemCategoryId]    Script Date: 1/23/2015 1:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID(N'dbo.fnGetItemCategoryId', N'fn') IS NOT NULL
	DROP FUNCTION [dbo].[fnGetItemCategoryId];
GO
CREATE FUNCTION [dbo].[fnGetItemCategoryId](@BusinessId BIGINT, @ItemCategoryName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemCategoryId BIGINT;
		SET @ItemCategoryId = 0;

	SELECT TOP 1 @ItemCategoryId = ic.ItemCategoryId
	FROM ItemCategory ic
	WHERE ic.BusinessId = @BusinessId
		AND ic.ItemCategoryName = @ItemCategoryName

	RETURN @ItemCategoryId;
END
