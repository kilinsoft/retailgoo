USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spCustomerGroupListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spCustomerGroupListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spCustomerGroupListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select sg.CustomerGroupId AS Value, sg.CustomerGroupName AS Text
	from CustomerGroup sg

SET @ReturnValue = 0;
RETURN @ReturnValue;