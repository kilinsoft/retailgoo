USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSalesInvoiceStatusUpdate')
   EXEC('CREATE PROCEDURE [dbo].[spSalesInvoiceStatusUpdate] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSalesInvoiceStatusUpdate]
	@IdTable tIdTable READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	UPDATE [SalesInvoice] 
	SET
		AmountPaid = 
			CASE 
				WHEN res1.AmtPaid IS NULL THEN 0
				ELSE res1.AmtPaid
			END,
		Status = 
			CASE
				WHEN res1.AmtPaid IS NULL OR res1.AmtPaid <= 0 THEN 'New'
				WHEN res1.AmtPaid >= si.Total THEN 'Done'
				ELSE 'Partial'
			END
	FROM [SalesInvoiceItem] sii
	INNER JOIN [SalesInvoice] si
		ON sii.SalesInvoiceId = si.SalesInvoiceId
	INNER JOIN @IdTable it
		ON it.Id = sii.SalesInvoiceId
	LEFT JOIN
		(
			SELECT 
				ipi.SalesInvoiceId,
				SUM(ipi.Amount) AS AmtPaid
			FROM [IncomingPaymentItem] ipi
			INNER JOIN [SalesInvoice] si
				ON ipi.SalesInvoiceId = si.SalesInvoiceId
					AND ipi.IsDisabled <> 1
			INNER JOIN [IncomingPayment] ip
				ON ipi.IncomingPaymentId = ip.IncomingPaymentId
					AND ip.IsDisabled <> 1
					AND ip.Status <> 'Void'
			GROUP BY ipi.SalesInvoiceId
		) res1
		ON sii.SalesInvoiceId = res1.SalesInvoiceId;
	
	--SELECT @status = si.Status
	--FROM [SalesInvoice] si
	--WHERE	si.SalesInvoiceId = @SalesInvoiceId
	--	AND si.IsDisabled <> 1;

	--IF (@status IS NOT NULL AND @status <> 'Void') -- Only Update item that is not disabled and is not VOID'ed
	--	BEGIN
	--		DECLARE @amtPaid DECIMAL (18 ,4);
	--			SET @amtPaid = 0;
	--		DECLARE @amtOwed DECIMAL (18, 4);
	--			SET @amtOwed = 0;

	--		-- Get @amtPaid
	--		SELECT 
	--			@amtOwed = si.Total,
	--			@amtPaid = SUM(ipi.Amount)
	--		FROM [SalesInvoice] si
	--		INNER JOIN [IncomingPaymentItem] ipi
	--			ON si.SalesInvoiceId = ipi.SalesInvoiceId
	--				AND ipi.IsDisabled <> 1
	--		INNER JOIN [IncomingPayment] ip
	--			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
	--				AND ip.IsDisabled <> 1
	--				AND ip.Status <> 'Void'
	--		WHERE	si.SalesInvoiceId = @SalesInvoiceId
	--		GROUP BY si.Total;

	--		IF (@amtPaid IS NULL) -- Set Status=New for item that has not been paid at all
	--			BEGIN
	--				SET @status = 'New';
	--				SET @amtPaid = 0;
	--			END
	--		ELSE IF (@amtPaid > 0 AND @amtPaid < @amtOwed) -- Set Status=Partial for item that has been partially paid
	--			BEGIN
	--				SET @status = 'Partial'
	--			END
	--		ELSE -- Set Status=Paid for item that has been paid
	--			BEGIN
	--				SET @status = 'Paid'
	--			END

	--		-- Update the AmounPaid and the Status of the SalesInvoice item
	--		UPDATE [SalesInvoice]
	--		SET AmountPaid = @amtPaid, Status = @status
	--		WHERE SalesInvoiceId = @SalesInvoiceId;
	--	END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO