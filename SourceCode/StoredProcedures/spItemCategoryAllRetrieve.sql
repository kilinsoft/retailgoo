USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemCategoryAllRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemCategoryAllRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemCategoryAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ic.ItemCategoryId, ic.ItemCategoryName, 
		CASE
			WHEN sum(i.Quantity) > 0 THEN sum(i.Quantity)
			ELSE 0
		END as ItemQuantity
	from ItemCategory ic
	left join ItemGroup ig
		on ig.ItemCategoryId = ic.ItemCategoryId
	left join Item i
		on i.ItemGroupId = ig.ItemGroupId 
	where
		ic.BusinessId = @BusinessId
		AND	ic.IsDisabled <> 1
		AND (ig.IsDisabled <> 1 OR ig.IsDisabled IS NULL)
		AND (i.IsDisabled <> 1 OR i.IsDisabled IS NULL)
	group by ic.ItemCategoryId, ic.ItemCategoryName

SET @ReturnValue = 0;
RETURN @ReturnValue;