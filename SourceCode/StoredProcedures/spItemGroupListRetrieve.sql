USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemGroupListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spItemGroupListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemGroupListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ItemGroupId AS Value, 
		ItemGroupName As Text 
	from 
		ItemGroup
	where 
			BusinessId = @BusinessId
		and IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;