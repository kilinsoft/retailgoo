IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tItemCategoryItem')
   EXEC sp_droptype tItemCategoryItem
GO
CREATE TYPE tItemCategoryItem AS TABLE (
	[ItemGroupId] [bigint] NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
