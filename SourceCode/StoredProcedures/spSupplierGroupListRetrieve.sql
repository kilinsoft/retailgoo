USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spSupplierGroupListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spSupplierGroupListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spSupplierGroupListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select sg.SupplierGroupId AS Value, sg.SupplierGroupName AS Text
	from SupplierGroup sg

SET @ReturnValue = 0;
RETURN @ReturnValue;