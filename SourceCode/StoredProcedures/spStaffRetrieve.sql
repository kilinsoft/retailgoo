USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spStaffRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spStaffRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spStaffRetrieve]
    @UserId bigint,
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	--declare @Role nvarchar(100);

	--SELECT TOP 1 @Role = Staff.Role
	--FROM Staff
	--WHERE Staff.UserId = 1
	--	AND Staff.BusinessId = 1
	--	AND Staff.Status = 'active'

	--declare @sqlStr nvarchar (MAX);
	--set @sqlStr = 'SELECT Staff.*, Business.BusinessName, ScreenPermission.' + CAST(@Role AS NVARCHAR) + ' AS RolePermission';
	--set @sqlStr = @sqlStr + ' FROM Staff';
	--set @sqlStr = @sqlStr + ' INNER JOIN Business';
	--set @sqlStr = @sqlStr + ' ON Staff.BusinessId = Business.BusinessId';
	--set @sqlStr = @sqlStr + ' INNER JOIN ScreenPermission';
	--set @sqlStr = @sqlStr + ' ON Staff.BusinessId = ScreenPermission.BusinessId';
	--set @sqlStr = @sqlStr + ' WHERE Staff.UserId = 1';
	--set @sqlStr = @sqlStr + ' AND Staff.BusinessId = 1';
	--set @sqlStr = @sqlStr + ' AND Staff.Status = ''active''';

	--exec sp_executesql @sqlStr;

	SELECT TOP 1 
		*,
		Role AS RolePermission,
		CASE 
			WHEN stf.Role = 'adm' THEN sp.Admin
			WHEN stf.Role = 'mgr' THEN sp.Admin
			WHEN stf.Role = 'sal' THEN sp.Admin
			WHEN stf.Role = 'clk' THEN sp.Admin
			ELSE sp.Admin
		END AS RolePermissionParameters
	FROM [Staff] stf
	INNER JOIN [Business] b
		ON stf.BusinessId = b.BusinessId
	INNER JOIN [ScreenPermission] sp
		ON stf.BusinessId = sp.BusinessId
	INNER JOIN [System_BusinessRole] sys_role
		ON stf.Role = sys_role.BusinessRoleShortName
	WHERE	stf.UserId = @UserId
		AND stf.BusinessId = @BusinessId
		AND stf.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;