IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spItemCategoryManage')
   EXEC('CREATE PROCEDURE [dbo].[spItemCategoryManage] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spItemCategoryManage]
	@ItemCategoryId bigint,
	@BusinessId bigint,
    @ItemCategoryName nvarchar(100),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tItemCategoryItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetIcId BIGINT;
		SET @targetIcId = @ItemCategoryId;
	declare @username varchar(50);
		exec @Username = GetUsername  @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	--DECLARE @updateItemList tIdTable;

	-- Manage records in [ItemCategory] table
	IF @ItemState = 'N'
		BEGIN
			-- Insert ItemCategory
			INSERT INTO [dbo].[ItemCategory] 
				([BusinessId],[ItemCategoryName],[IsDisabled], [DateCreated], [DateModify], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @ItemCategoryName, 0, @dateNow, @dateNow, @username, @username
			SET @targetIcId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			-- Update ItemCategory
			UPDATE [dbo].[ItemCategory]
			SET    
				[ItemCategoryName] = @ItemCategoryName,
				[DateModify] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[ItemCategoryId] = @ItemCategoryId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			-- Delete ItemCategory
			UPDATE [dbo].[ItemCategory]
			SET    
				[IsDisabled] = 1, [DateModify] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[ItemCategoryId] = @ItemCategoryId
		END

	---- Insert ItemGroup
	--INSERT INTO [dbo].[ItemGroup]
	--    ([BusinessId], [ItemCategoryId], [ItemGroupCode], [ItemGroupName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	--SELECT BusinessId = @BusinessId, ItemCategoryId = @targetIcId, ItemGroupCode, ItemGroupName, 0, @dateNow, @dateNow, @username, @username
	--	FROM @Items
	--	WHERE ItemState = 'N';

	---- Update ItemGroup
	UPDATE igItem
	SET igItem.ItemCategoryId = @targetIcId, 
	    igItem.DateModified = @dateNow, igItem.UpdatedBy = @username
	FROM [dbo].[ItemGroup] AS igItem
		INNER JOIN @Items AS i
		ON igItem.ItemGroupId = i.ItemGroupId 
		WHERE i.ItemState = 'M'

	---- Disable ItemGroup
	UPDATE igItem
	SET igItem.IsDisabled = 1, igItem.DateModified = @dateNow, igItem.UpdatedBy = @username
	FROM [dbo].[ItemGroup] AS igItem
		INNER JOIN @Items AS i
		ON igItem.ItemGroupId = i.ItemGroupId
		WHERE i.ItemState = 'D'


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

GO