USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spReceiveOrderStatusUpdate')
   EXEC('CREATE PROCEDURE [dbo].[spReceiveOrderStatusUpdate] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spReceiveOrderStatusUpdate]
	@IdTable tIdTable READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	UPDATE [ReceiveOrder]
	SET
		AmountPaid =
			CASE 
				WHEN result.AmountPaid IS NULL THEN 0
				ELSE result.AmountPaid
			END,
		Status = 
			CASE 
				WHEN result.AmountPaid IS NULL OR result.AmountPaid = 0 THEN 'New'
				--WHEN result.AmountPaid >= result.Total THEN 'Done'
				WHEN result.AmountPaid >= result.Subtotal THEN 'Done'
				ELSE 'Partial'
			END
	FROM 
	(
		SELECT 
			ro.ReceiveOrderId,
			ro.Total,
			ro.Subtotal,
			SUM(result.Amount) AS AmountPaid
		FROM @IdTable it
		INNER JOIN [ReceiveOrder] ro
			ON it.Id = ro.ReceiveOrderId
		INNER JOIN [ReceiveOrderItem] roi
			ON ro.ReceiveOrderId = roi.ReceiveOrderId
				AND ro.IsDisabled <> 1
				AND ro.Status <> 'Void'
				AND roi.IsDisabled <> 1
		LEFT JOIN (
			SELECT 
				opi.ReceiveOrderId,
				opi.Amount
			FROM [OutgoingPaymentItem] opi
			INNER JOIN [OutgoingPayment] op
				ON opi.OutgoingPaymentId = op.OutgoingPaymentId
					AND op.IsDisabled <> 1
					AND op.Status <> 'Void'
					AND opi.IsDisabled <> 1
		) result
			ON ro.ReceiveOrderId = result.ReceiveOrderId
		GROUP BY ro.ReceiveOrderId, ro.Total, ro.Subtotal
	) result
	WHERE 
		[ReceiveOrder].ReceiveOrderId = result.ReceiveOrderId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

