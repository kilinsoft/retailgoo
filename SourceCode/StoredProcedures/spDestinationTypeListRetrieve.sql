USE [retailgoo]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spDestinationTypeListRetrieve')
   EXEC('CREATE PROCEDURE [dbo].[spDestinationTypeListRetrieve] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[spDestinationTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT
		dt.DestinationTypeId AS VALUE,
		dt.DestinationTypeName AS TEXT
	FROM [DestinationType] dt;

SET @ReturnValue = 0;
RETURN @ReturnValue;