IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = 'tPurchaseOrderItem')
   EXEC sp_droptype tPurchaseOrderItem
GO
CREATE TYPE dbo.tPurchaseOrderItem AS TABLE (
	[PurchaseOrderItemId] [bigint] NOT NULL,
	[PurchaseOrderId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
);
GO
