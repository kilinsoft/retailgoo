﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace RetailGOO
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //Initialize Logger
            Logger.Init();
            Logger.SysInfo("Application started"); 

            //ToDo : ThreadJob initialization
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["init"] = 0; // fix the session id until it expires.
            Logger.SessionInfo(string.Format("Session started"));
            Session[SessionVariable.StartTime] = DateTime.Now;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Logger.SysError(string.Format("sender[ {0} ] EventArgs[ {1} ]", sender.ToString(), e.ToString()));
        }

        protected void Session_End(object sender, EventArgs e)
        {
            
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Logger.SystemLog.Fatal("Application ended. If it was unintentional, please check the system");
        }
    }
}