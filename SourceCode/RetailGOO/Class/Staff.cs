﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RetailGOO.Util.Constant;
using RetailGOO.Util;
using RetailGOO.Util.EnumType;

namespace RetailGOO.Class
{
    public class Staff
    {
        public long StaffId { get; set; }
        public string BusinessName { get; set; }
        public StaffStatus Status { get; set; }
        public StaffRole Role { get; set; }
        public RolePermission Permission { get; set; }

        //public Staff(string businessName, string statusStr, string roleStr)
        //{
        //    this.BusinessName = businessName;
        //    this.Role = GetRole(roleStr);
        //    this.Status = GetStatus(statusStr);
        //}
        public static StaffStatus GetStatusFromString(string statusStr)
        {
            switch (statusStr.ToLower())
            {
                case StaffStatusString.Active:
                    return StaffStatus.Active;
                case StaffStatusString.Waiting:
                    return StaffStatus.Waiting;
                case StaffStatusString.Declined:
                    return StaffStatus.Declined;
                case StaffStatusString.Inactive:
                default:
                    return StaffStatus.Inactive;
            }
        }
        public static StaffRole GetRoleFromString(string roleStr) {
            switch (roleStr.ToLower())
            {
                case StaffRoleString.Admin:
                    return StaffRole.Admin;
                case StaffRoleString.Manager:
                    return StaffRole.Manager;
                case StaffRoleString.Clerk:
                    return StaffRole.Clerk;
                case StaffRoleString.Purchase:
                    return StaffRole.Purchase;
                case StaffRoleString.Sales:
                default:
                    return StaffRole.Sales;
            }
        }

        public static string GetRoleFullname(string roleShortName)
        {
            switch (roleShortName.ToLower())
            {
                case StaffRoleString.Admin:
                    return "Admin";
                case StaffRoleString.Manager:
                    return "Manager";
                case StaffRoleString.Clerk:
                    return "Clerk";
                case StaffRoleString.Purchase:
                    return "Purchase";
                case StaffRoleString.Sales:
                default:
                    return "Sales";
            }
        }

    }
}