﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RetailGOO.Util.Constant;
using RetailGOO.Util;
using RetailGOO.Util.EnumType;

namespace RetailGOO.Class
{
    public class RolePermission
    {
        private Dictionary<BusinessScreen, ScreenPermission> _permissionTable = new Dictionary<BusinessScreen, ScreenPermission>();
        private static Dictionary<string, BusinessScreen> _screenMappingTable = null;
        private static Dictionary<string, ScreenPermission> _screenPermissionMappingTable = null;

        public static string GetScreenString(BusinessScreen screen)
        {
            foreach (KeyValuePair<string, BusinessScreen> pair in _screenMappingTable)
            {
                if (pair.Value == screen)
                    return pair.Key;
            }
            return "";
        }

        public static string GetPermissionString(ScreenPermission type)
        {
            foreach (KeyValuePair<string, ScreenPermission> pair in _screenPermissionMappingTable)
            {
                if (pair.Value == type)
                    return pair.Key;
            }
            return "";
        }

        public BusinessScreen GetBusinessScreen(string screenString)
        {
            if (_screenMappingTable.ContainsKey(screenString))
                return _screenMappingTable[screenString];
            else
                return BusinessScreen.Other;
        }

        public RolePermission()
        {
            Initialize();
        }
        public RolePermission(Dictionary<BusinessScreen, ScreenPermission> permissionTable)
        {
            Initialize();
            this._permissionTable = permissionTable;
        }
        public RolePermission(string permissionParameters)
        {
            Initialize();

            if (string.IsNullOrEmpty(permissionParameters))
                return;
            string[] sp = permissionParameters.Split(',');
            for (int i = 0; i < sp.Length; ++i)
            {
                string[] elements = sp[i].Split(':');
                if (elements.Length != 2)
                    continue;

                string screenName = elements[0];
                string permission = elements[1];
                if (_screenMappingTable.ContainsKey(screenName))
                {
                    BusinessScreen screen = _screenMappingTable[screenName];
                    if (_permissionTable.ContainsKey(screen))
                    {
                        try
                        {
                            _permissionTable[screen] = GetScreenPermission(permission);
                        }
                        catch (Exception ex)
                        {
                            Logger.SysFatal(
                                string.Format(
                                    "Invalid Screen. BusinessId[{0}] Screen[{1}] ScreenName[{2}]. {3}",
                                    SessionHelper.GetCurrentBusinessId(),
                                    screen,
                                    screenName,
                                    ex.ToString()));
                        }
                    }
                }
            }
        }

        private void Initialize()
        {
            if (_screenMappingTable == null)
            {
                _screenMappingTable = new Dictionary<string, BusinessScreen>();

                _screenMappingTable.Add(BusinessScreenString.CashPurchase, BusinessScreen.CashPurchase);
                _screenMappingTable.Add(BusinessScreenString.PurchaseOrder, BusinessScreen.PurchaseOrder);
                _screenMappingTable.Add(BusinessScreenString.ReceiveOrder, BusinessScreen.ReceiveOrder);
                _screenMappingTable.Add(BusinessScreenString.OutgoingPayment, BusinessScreen.OutgoingPayment);
                _screenMappingTable.Add(BusinessScreenString.Expense, BusinessScreen.Expense);
                _screenMappingTable.Add(BusinessScreenString.CashSales, BusinessScreen.CashSales);
                _screenMappingTable.Add(BusinessScreenString.SalesInvoice, BusinessScreen.SalesInvoice);
                _screenMappingTable.Add(BusinessScreenString.IncomingPayment, BusinessScreen.IncomingPayment);
                _screenMappingTable.Add(BusinessScreenString.ItemManagement, BusinessScreen.ItemManagement);
                _screenMappingTable.Add(BusinessScreenString.CategoryManagement, BusinessScreen.CategoryManagement);
                _screenMappingTable.Add(BusinessScreenString.BrandManagement, BusinessScreen.BrandManagement);
                _screenMappingTable.Add(BusinessScreenString.CountSheet, BusinessScreen.CountSheet);
                _screenMappingTable.Add(BusinessScreenString.CurrentStock, BusinessScreen.CurrentStock);
                _screenMappingTable.Add(BusinessScreenString.AdjustStock, BusinessScreen.AdjustStock);
                _screenMappingTable.Add(BusinessScreenString.TransferStock, BusinessScreen.TransferStock);
                _screenMappingTable.Add(BusinessScreenString.ReceiveTransferStock, BusinessScreen.ReceiveTransferStock);
                _screenMappingTable.Add(BusinessScreenString.ReorderStock, BusinessScreen.ReorderStock);
                _screenMappingTable.Add(BusinessScreenString.CustomerInfo, BusinessScreen.CustomerInfo);
                _screenMappingTable.Add(BusinessScreenString.SupplierInfo, BusinessScreen.SupplierInfo);
                _screenMappingTable.Add(BusinessScreenString.CashManagement, BusinessScreen.CashManagement);

                _screenPermissionMappingTable = new Dictionary<string, ScreenPermission>();
                _screenPermissionMappingTable.Add(ScreenPermissionString.All, ScreenPermission.All);
                _screenPermissionMappingTable.Add(ScreenPermissionString.Create, ScreenPermission.Create);
                _screenPermissionMappingTable.Add(ScreenPermissionString.Edit, ScreenPermission.Edit);
                _screenPermissionMappingTable.Add(ScreenPermissionString.Delete, ScreenPermission.Delete);
                _screenPermissionMappingTable.Add(ScreenPermissionString.View, ScreenPermission.View);
            }

            foreach (KeyValuePair<string, BusinessScreen> pair in _screenMappingTable)
            {
                _permissionTable.Add(pair.Value, ScreenPermission.View);
            }
        }

        public ScreenPermission GetScreenPermission(BusinessScreen screen)
        {
            if (_permissionTable.ContainsKey(screen))
                return (ScreenPermission)_permissionTable[screen];
            else
                return ScreenPermission.View;
        }

        public ScreenPermission GetScreenPermission(string permissionString)
        {
            if (_screenPermissionMappingTable.ContainsKey(permissionString))
                return _screenPermissionMappingTable[permissionString];
            else
                return ScreenPermission.View;
        }

        public string GetPermissionParameters()
        {
            var sb = new System.Text.StringBuilder();
            IList<string> permissionPairs = new List<string>();
            foreach (KeyValuePair<BusinessScreen, ScreenPermission> pair in _permissionTable)
            {
                permissionPairs.Add(
                    string.Format("{0}:{1}",
                        GetScreenString(pair.Key),
                        GetPermissionString(pair.Value)));
            }
            string p = string.Join(",", permissionPairs);
            return p;
        }

        public static string GetDefaultPermissionParameters(StaffRole role)
        {
            var sb = new System.Text.StringBuilder();

            if (role == StaffRole.Admin || role == StaffRole.Manager)
            {
                sb.Append(string.Format("{0}:{1}", BusinessScreenString.CashPurchase, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.PurchaseOrder, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveOrder, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.OutgoingPayment, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.Expense, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashSales, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SalesInvoice, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.IncomingPayment, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ItemManagement, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CategoryManagement, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.BrandManagement, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CountSheet, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CurrentStock, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.AdjustStock, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.TransferStock, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveTransferStock, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReorderStock, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CustomerInfo, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SupplierInfo, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashManagement, ScreenPermission.All));
            }
            else if (role == StaffRole.Clerk)
            {
                sb.Append(string.Format("{0}:{1}", BusinessScreenString.CashPurchase, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.PurchaseOrder, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveOrder, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.OutgoingPayment, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.Expense, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashSales, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SalesInvoice, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.IncomingPayment, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ItemManagement, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CategoryManagement, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.BrandManagement, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CountSheet, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CurrentStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.AdjustStock, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.TransferStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveTransferStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReorderStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CustomerInfo, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SupplierInfo, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashManagement, ScreenPermission.View));
            }
            else if (role == StaffRole.Purchase)
            {
                sb.Append(string.Format("{0}:{1}", BusinessScreenString.CashPurchase, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.PurchaseOrder, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveOrder, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.OutgoingPayment, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.Expense, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashSales, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SalesInvoice, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.IncomingPayment, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ItemManagement, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CategoryManagement, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.BrandManagement, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CountSheet, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CurrentStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.AdjustStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.TransferStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveTransferStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReorderStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CustomerInfo, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SupplierInfo, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashManagement, ScreenPermission.View));
            }
            else //if (role == StaffRole.Sales)
            {
                sb.Append(string.Format("{0}:{1}", BusinessScreenString.CashPurchase, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.PurchaseOrder, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveOrder, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.OutgoingPayment, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.Expense, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashSales, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SalesInvoice, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.IncomingPayment, ScreenPermission.All));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ItemManagement, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CategoryManagement, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.BrandManagement, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CountSheet, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CurrentStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.AdjustStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.TransferStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReceiveTransferStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.ReorderStock, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CustomerInfo, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.SupplierInfo, ScreenPermission.View));
                sb.Append(string.Format(",{0}:{1}", BusinessScreenString.CashManagement, ScreenPermission.View));
            }

            return sb.ToString();
        }

        public bool hasScreenPermission(string screenName, string itemState)
        {
            // 1) get a BusinessScreen
            // 2) get a ScreenPermission
            // 3) check the itemStatus with the ScreenPermission
            // if the ScreenPermission is a, then the function returns true
            // if the ScreenPermission is c and if the itemStatus is N, then the fuction returnes true, else the function returns false
            // if the ScreenPermission is e and if the itemStatus is M, then the fuction returnes true, else the function returns false
            // if the ScreenPermission is d and if the itemStatus is D, then the fuction returnes true, else the function returns false
            // if the ScreenPermission is v, then the function returns false

            BusinessScreen bs = GetBusinessScreen(screenName);
            ScreenPermission sp = GetScreenPermission(bs);

            if (sp == ScreenPermission.All)
                return true;
            else if ((sp == ScreenPermission.Create) && (itemState == "N"))
                return true;
            else if ((sp == ScreenPermission.Edit) && (itemState == "M"))
                return true;
            else if ((sp == ScreenPermission.Delete) && (itemState == "D"))
                return true;
            else if (sp == ScreenPermission.View)
                return false;
            return false;
        }

        public string GetFullItemStateString(string itemState)
        {
            if (itemState == ItemState.New)
                return "new";
            else if (itemState == ItemState.Modified)
                return "modify";
            else if (itemState == ItemState.Deleted)
                return "delete";
            return "view";
        }
    }
}