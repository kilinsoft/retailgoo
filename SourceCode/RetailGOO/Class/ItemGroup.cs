﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Util.Constant;

namespace RetailGOO.Class
{
    public class ItemGroup
    {
        public long ItemGroupId { get; set; }
        public long BusinessId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Filename { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }
        public DateTime DateModified { get; set; }
        public string UpdatedBy { get; set; }

        public ItemGroupDto GetDto()
        {
            var itemGroupDto = new ItemGroupDto();
            return itemGroupDto;
        }
    }
}