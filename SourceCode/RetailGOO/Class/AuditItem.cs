﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetailGOO.Class
{
    public class AuditItem
    {
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public AuditItem()
        {
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now;
            CreatedBy = RetailGOO.Util.Constant.DatabaseDefaultValue.SystemUsername;
            UpdatedBy = RetailGOO.Util.Constant.DatabaseDefaultValue.SystemUsername;
        }
    }
}