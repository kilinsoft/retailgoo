﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using RetailGOO.Dto;

namespace RetailGOO.Dto
{
    [DataContract]
    public class GenericResponse<T> : BaseResponse
    {
        private T _responseItem;

        [DataMember]
        public int ResponseItemCount { get; set; }

        [DataMember]
        public T ResponseItem
        {
            get
            {
                return _responseItem;
            }
            set
            {
                if (value is ICollection)
                    ResponseItemCount = (value as ICollection).Count;

                _responseItem = value;
            }
        }

        [DataMember]
        public long VirtualItemCount { get; set; }

        //[DataMember]
        //public int CurrentPageIndex { get; set; }

        //[DataMember]
        //public int RowPerPage { get; set; }

        public GenericResponse()
        {
            //ResponseItem = default(T);
            ResponseItemCount = 0;
            VirtualItemCount = 0;
        }
    }
}