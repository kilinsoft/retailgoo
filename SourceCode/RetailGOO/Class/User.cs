﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace RetailGOO.Class
{
    public class User : AuditItem
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        public string Guid { get; set; }
        public DateTime? DateGuidExpired { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsFacebookUser { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        //public string WebsiteUrl { get; set; }
        public string FacebookId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
    }
}