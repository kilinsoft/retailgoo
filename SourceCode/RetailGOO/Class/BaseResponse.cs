﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class BaseResponse
    {
        [DataMember]
        public bool Success { get; private set; }

        [DataMember]
        public int ResponseCode { get; private set; }

        [DataMember]
        public string StatusText { get; private set; }

        [DataMember]
        public string DebugText { get; private set; }

        // For Server-side information only
        public IList<string> Messages = new List<string>();

        public BaseResponse()
        {
            this.Success = false;
            this.StatusText = "<unassigned>";
        }

        public void AddMessage(string message)
        {
            Messages.Add(message);
        }

        public void Succeed(string txt = null)
        {
            this.Success = true;
            this.ResponseCode = 0;
            if (txt == null)
                this.StatusText = "Success";
            else
            {
                AddMessage(txt);
                this.StatusText = txt;
            }
        }
        public void Fail(string errorText, string debugText = "")
        {
            this.Success = false;
            this.ResponseCode = 1;
            this.StatusText = errorText;
            this.DebugText = debugText;
            Messages.Add(errorText);
            Messages.Add(debugText);
        }
        public void SetDebugText(string debugText)
        {
            this.DebugText = debugText;
        }
        public string GetFullText()
        {
            var sb = new System.Text.StringBuilder();
            for (int i = 0; i < Messages.Count; ++i)
                sb.Append(Messages[i] + ". ");
            return sb.ToString();
        }
    }
}