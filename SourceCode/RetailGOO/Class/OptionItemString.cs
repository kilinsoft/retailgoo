﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Class
{
    [DataContract]
    public class OptionItemString
    {
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string Text { get; set; }

        public OptionItemString()
        {
            this.Value = "";
            this.Text = null;
        }

        public OptionItemString(string val, string text)
        {
            this.Value = val;
            this.Text = text;
        }
    }
}