﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Class
{
    [DataContract]
    public class OptionItem
    {
        [DataMember]
        public long Value { get; set; }
        [DataMember]
        public string Text { get; set; }

        public OptionItem()
        {
            this.Value = -1;
            this.Text = null;
        }

        public OptionItem(long val, string text)
        {
            this.Value = val;
            this.Text = text;
        }
    }
}