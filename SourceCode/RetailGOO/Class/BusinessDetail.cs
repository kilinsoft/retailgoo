﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RetailGOO.Class;
using RetailGOO.Util.EnumType;

using System.Globalization;

namespace RetailGOO.Class
{
    public class BusinessDetail : AuditItem
    {
        public long BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string Currentcy { get; set; }
        public string DefaultPriceType { get; set; }
        public bool UseServiceCharge { get; set; }
        public decimal ServiceChargeAmount { get; set; }
        public string SalesTaxType { get; set; }
        public decimal SalesTaxAmount { get; set; }
        public bool UseStockItemControl { get; set; }
        public string CostingType { get; set; }
    }
}