﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.BusinessLayer;

namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class Settings
    {
        #region DocSetting

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<DocSettingDto> RetrieveDocSetting()
        {
            return DocumentAdapter.RetrieveDocSetting();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageDocSetting(DocSettingDto dto)
        {
            return DocumentAdapter.ManageDocSetting(dto);
        }

        #endregion
    }
}
