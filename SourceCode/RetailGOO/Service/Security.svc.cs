﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Util;
using RetailGOO.Util.Constant;
using RetailGOO.BusinessLayer;
using RetailGOO.Class;

namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class Security
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse Authenticate( CredentialDto cred )
        {
            if (cred == null)
                return new BaseResponse();

            // ToDo: Pevent DoS authentication
            Logger.SessionLog.Info(string.Format("Attempt login. User[ {0} ]", cred.Username));
            Logger.SessionDebug(string.Format("{0}", cred.ToString()));
            BaseResponse resp = new BaseResponse();

            string username = cred.Username.Trim().ToLower();
            string password = cred.Password.Trim();
            string accessToken = cred.AccessToken.Trim();

            bool fValid = false;
            try
            {
                if (cred.IsFacebookUser && cred.Password == "")
                {
                    // Validate by Facebook API
                    fValid = Identity.ValidateFacebookUser(username, accessToken);
                    if (fValid)
                    {
                        // Register this user to RetailGOO if it does not exist
                        var rspUser = Identity.GetUserByUsername(cred.Email);
                        if (!rspUser.Success)
                            fValid = CreateUser(cred).Success;
                    }
                }
                else
                {
                    // Validate by checking against DB
                    fValid = Identity.ValidateUser(username, password);
                }

                if (fValid)
                {
                    Identity.SignIn(username);
                    resp.Succeed();

                    Logger.SessionInfo(string.Format("Valid login. User[ {0} ]", cred.Username));
                    resp.Succeed("Valid login");
                }
                else
                {
                    Logger.SessionWarn(string.Format("Invalid login. User[ {0} ]", cred.Username));
                    resp.Fail("Username or Password is incorrect");
                }
            }
            catch (Exception ex)
            {
                fValid = false;
                Logger.SessionError(ex.ToString());
                resp.Fail("There was an error in the latest action. Please try again or contact administrators.");
            }

            return resp;
        }

        [OperationContract]
        [WebInvoke(
              Method = "POST",
              ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse CreateUser(CredentialDto cred)
        {
            cred.Simplify();
            // ToDo: Pevent DoS authentication
            Logger.SessionInfo(string.Format("Try to Create User. User[ {0} ] Email[ {1} ]", cred.Username, cred.Email));

            BaseResponse resp = new BaseResponse();
            resp = Identity.CreateUser(cred.Username, cred.Password, cred.Email, cred.IsFacebookUser);
            if ( resp.Success )
                Logger.SessionInfo(string.Format("User created successful. User[ {0} ] Email[ {1} ] FacebookAccount[ {2} ]", cred.Username, cred.Email, cred.IsFacebookUser.ToString()));
            else
                Logger.SessionError(string.Format("{0}. User[ {1} ] Email[ {2} ] FacebookAccount[ {3} ]", resp.StatusText, cred.Username, cred.Email, cred.IsFacebookUser.ToString()));
            return resp;
        }
    }
}