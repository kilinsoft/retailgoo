﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Class.DTO;
using RetailGOO.Class.DTO.Response;
using RetailGOO.Class.Util;
using RetailGOO.Class.BusinessLayer;

namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class Business
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse Create(Class.DTO.BusinessDTO busiDto)
        {
            busiDto.Simplify();
            Logger.SessionInfo(string.Format("Try to Create Business. Business[ {0} ] Creator[ {1} ]", busiDto.BusinessName, busiDto.CreatedBy));

            BaseResponse resp = new BaseResponse();
            resp = RetailGOO.Class.BusinessLayer.Business.CreateBusiness(busiDto);
            if (resp.Success)
                Logger.SessionInfo(string.Format("Business created successful. Business[ {0} ] Creator[ {1} ]", busiDto.BusinessName, busiDto.CreatedBy));
            else
                Logger.SessionError(string.Format("{0}. Business[ {1} ] Creator[ {2} ]", resp.StatusText, busiDto.BusinessName, busiDto.CreatedBy));
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<BusinessSelectionDto>> Retrieve()
        {
            var resp = new GenericResponse<IList<BusinessSelectionDto>>();
            resp = RetailGOO.Class.BusinessLayer.Business.RetrieveBusiness(SessionHelper.GetCurrentUsername());
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<CustomerDto>> RetrieveCustomerInfo()
        {
            // Pro : ToDo : Check if the user who is requesting WCF data is a valid user.
            return BusinessData.CustomerInfoRetrieve();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse UpdateCustomerInfo(GenericResponse<IList<CustomerDto>> dataDto)
        {
            var resp = new BaseResponse();
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<SupplierDto>> RetrieveSupplierInfo()
        {
            // Pro : ToDo : Check if the user who is requesting WCF data is a valid user.
            return BusinessData.SupplierInfoRetrieve();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse UpdateSupplierInfo(GenericResponse<IList<SupplierDto>> dataDto)
        {
            var resp = new BaseResponse();
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<string>> RetrieveAutoCompleteData(string type)
        {
            var resp = new GenericResponse<IList<string>>();

            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<string>> RetrieveRoles()
        {
            var resp = new GenericResponse<IList<string>>();

            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<string>> UpdateRoles(string type)
        {
            var resp = new GenericResponse<IList<string>>();

            return resp;
        }
    }
}
