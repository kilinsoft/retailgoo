﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.BusinessLayer;

namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class BusinessService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse CreateBusiness(BusinessDto busiDto)
        {
            string creator = SessionHelper.GetCurrentUsername();
            busiDto.Simplify();
            Logger.SessionInfo(string.Format("Try to Create Business. Business[ {0} ] Creator[ {1} ]", busiDto.BusinessName, creator));

            BaseResponse resp = new BaseResponse();
            resp = Business.CreateBusiness(busiDto);
            if (resp.Success)
                Logger.SessionInfo(string.Format("Business created successful. Business[ {0} ] Creator[ {1} ]", busiDto.BusinessName, creator));
            else
                Logger.SessionError(string.Format("{0}. Business[ {1} ] Creator[ {2} ]", resp.StatusText, busiDto.BusinessName, creator));
            return resp;
        }

        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        //public GenericResponse<IList<BusinessSelectionDto>> RetrieveStaffBusinesses()
        //{
        //    var resp = new GenericResponse<IList<BusinessSelectionDto>>();
        //    resp = Business.RetrieveStaffBusinesses();
        //    return resp;
        //}

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<string>> RetrieveAutoCompleteData(string type)
        {
            var resp = new GenericResponse<IList<string>>();

            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<RolesDto> RetrieveRoles(string rolesname)
        {
            return Business.RetrieveRoles(rolesname);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageRoles(RolesDto dto)
        {
            return Business.ManageRoles(dto);
        }

        #region AddUser

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendInvitation(BusinessStaffDto dto)
        {
            var rsp = StaffAdapter.SendInvitation(dto);
            return rsp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> SearchUserByEmail(string keyword)
        {
            return Identity.SearchUserByEmail(keyword, false);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<UserProfileDto>> RetrieveAllBusinessStaff()
        {
            return Business.RetrieveAllBusinessStaff();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse UpdateStaffRoleAndStatus(IList<UserProfileDto> modifiedStaffList)
        {
            return StaffAdapter.UpdateStaffRoleAndStatus(modifiedStaffList);
        }

        #endregion

        #region Peripheral

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveCountryList(string type)
        {
            return Business.RetrieveCountryList();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveDefaultPriceList()
        {
            return Business.RetrieveDefaultPriceList();
        }

        #endregion

        #region BusinessInfo

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<BusinessInfoDto> RetrieveBusinessInfo()
        {
            return Business.RetrieveBusinessInfo();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageBusinessInfo(BusinessInfoDto dto)
        {
            return Business.ManageBusinessInfo(dto);
        }
        
        #endregion

        #region FinancialSetting

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<FinancialSettingDto> RetrieveFinancialSetting()
        {
            return DocumentAdapter.RetrieveFinancialSetting();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageFinancialSetting(FinancialSettingDto dto)
        {
            return DocumentAdapter.ManageFinancialSetting(dto);
        }

        #endregion

        #region UserScreen

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<UserProfileDto> RetrieveStaffInfo(long staffId)
        {
            return StaffAdapter.RetrieveStaffInfo(staffId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageStaffInfo(UserProfileDto dto)
        {
            return StaffAdapter.ManageStaffInfo(dto);
        }

        #endregion
    }
}
