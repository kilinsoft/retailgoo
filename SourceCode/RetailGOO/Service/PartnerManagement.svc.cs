﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Util;
using RetailGOO.Class;
using RetailGOO.BusinessLayer;


namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class PartnerManagement
    {
        #region Supplier

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<SupplierDto> RetrieveSupplierInfo(long supplierId)
        {
            return SupplierAdapter.RetrieveSupplier(supplierId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<SupplierDto>> RetrieveAllSupplier()
        {
            return SupplierAdapter.RetrieveAllSupplier();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveSupplierList()
        {
            return SupplierAdapter.RetrieveSupplierList();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveSupplierGroupList()
        {
            return SupplierAdapter.RetrieveSupplierGroupList();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<SupplierDto> RetrieveSupplier(long id)
        {
            return SupplierAdapter.RetrieveSupplier(id);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageSupplier(SupplierDto dto)
        {
            return SupplierAdapter.ManageSupplier(dto);
        }

        #endregion

        #region Customer

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<CustomerDto> RetrieveCustomerInfo(long CustomerId)
        {
            return CustomerAdapter.RetrieveCustomer(CustomerId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<CustomerDto>> RetrieveAllCustomer()
        {
            return CustomerAdapter.RetrieveAllCustomer();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveCustomerList()
        {
            return CustomerAdapter.RetrieveCustomerList();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveCustomerGroupList()
        {
            return CustomerAdapter.RetrieveCustomerGroupList();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<CustomerDto> RetrieveCustomer(long id)
        {
            return CustomerAdapter.RetrieveCustomer(id);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageCustomer(CustomerDto dto)
        {
            return CustomerAdapter.ManageCustomer(dto);
        }

        #endregion

        #region SalesPerson

        // HIM: to be implemented
        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        //public GenericResponse<SalesPersonInfoDto> RetrieveSalesPersonInfo(long salespersonId)
        //{
        //    return StaffAdapter.RetrieveSalesPerson(salespersonId);
        //}

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveSalesPersonList()
        {
            return SalesAdapter.RetrieveSalesPersonList();
        }

        #endregion

    }

}
