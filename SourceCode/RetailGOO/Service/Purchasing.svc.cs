﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Util;
using RetailGOO.Class;
using RetailGOO.BusinessLayer;

namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class Purchasing
    {
        #region CashPurchase

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<PurchaseOrderDto>> RetrieveAllCashPurchase()
        {
            return PurchaseAdapter.RetrieveAllCashPurchase();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageCashPurchase(PurchaseOrderDto dto)
        {
            return PurchaseAdapter.ManageCashPurchase(dto);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidCashPurchase(long id)
        {
            return PurchaseAdapter.VoidCashPurchase(id);
        }

        #endregion

        #region PurchaseOrder

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<PurchaseOrderDto>> RetrieveAllPurchaseOrder(SearchParameterDto searchDto)
        {
            var resp = new GenericResponse<IList<PurchaseOrderDto>>();
            resp = PurchaseAdapter.RetrieveAllPurchaseOrder(searchDto);
            return resp;
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<PurchaseOrderDto> RetrievePurchaseOrder(long purchaseOrderId)
        {
            return PurchaseAdapter.RetrievePurchaseOrder(purchaseOrderId);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<PurchaseOrderItemDto>> RetrieveAllPurchaseOrderItem(long purchaseOrderId)
        {
            var resp = new GenericResponse<IList<PurchaseOrderItemDto>>();
            resp = PurchaseAdapter.RetrieveAllPurchaseOrderItem(purchaseOrderId);
            return resp;
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManagePurchaseOrder(PurchaseOrderDto dto)
        {
            var resp = PurchaseAdapter.ManagePurchaseOrder(dto);
            return resp;
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidPurchaseOrder (long id)
        {
            var resp = PurchaseAdapter.VoidPurchaseOrder(id);
            return resp;
        }

        #endregion

        #region ReceiveOrder

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ReceiveOrderDto>> RetrieveAllReceiveOrder()
        {
            return PurchaseAdapter.RetrieveAllReceiveOrder();
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ReceiveOrderDto> RetrieveReceiveOrder(long id)
        {
            return PurchaseAdapter.RetrieveReceiveOrder(id);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ReceiveOrderItemDto>> RetrieveAllReceiveOrderItem(long ReceiveOrderId)
        {
            return PurchaseAdapter.RetrieveAllReceiveOrderItem(ReceiveOrderId);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageReceiveOrder(ReceiveOrderDto dto)
        {
            return PurchaseAdapter.ManageReceiveOrder(dto);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidReceiveOrder(long id)
        {
            return PurchaseAdapter.VoidReceiveOrder(id);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrievePurchaseOrderList(long supplierId)
        {
            return PurchaseAdapter.RetrievePurchaseOrderList(supplierId);
        }

        #endregion

        #region OutgoingPayment

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OutgoingPaymentDto>> RetrieveAllOutgoingPayment()
        {
            return PurchaseAdapter.RetrieveAllOutgoingPayment();
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<OutgoingPaymentDto> RetrieveOutgoingPayment(long id)
        {
            return PurchaseAdapter.RetrieveOutgoingPayment(id);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OutgoingPaymentItemDto>> RetrieveAllOutgoingPaymentItem(long OutgoingPaymentId)
        {
            return PurchaseAdapter.RetrieveAllOutgoingPaymentItem(OutgoingPaymentId);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageOutgoingPayment(OutgoingPaymentDto dto)
        {
            return PurchaseAdapter.ManageOutgoingPayment(dto);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidOutgoingPayment(long id)
        {
            return PurchaseAdapter.VoidOutgoingPayment(id);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveReceiveOrderList()
        {
            return PurchaseAdapter.RetrieveReceiveOrderList();
        }

        #endregion

        #region Expense

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ExpenseDto>> RetrieveAllExpense()
        {
            return PurchaseAdapter.RetrieveAllExpense();
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ExpenseDto> RetrieveExpense(long id)
        {
            return PurchaseAdapter.RetrieveExpense(id);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageExpense(ExpenseDto dto)
        {
            return PurchaseAdapter.ManageExpense(dto);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidExpense(long id)
        {
            return PurchaseAdapter.VoidExpense(id);
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveExpenseCategoryList()
        {
            return PurchaseAdapter.RetrieveExpenseCategoryList();
        }

        #endregion
    }
}
