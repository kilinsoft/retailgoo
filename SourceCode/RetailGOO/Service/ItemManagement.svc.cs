﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.BusinessLayer;

namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class ItemManagement
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemGroupDto>> RetrieveAllItemGroup()
        {
            var resp = new GenericResponse<IList<ItemGroupDto>>();
            resp = ItemAdapter.RetrieveAllItemGroup();
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemGroupDto> RetrieveItemGroup(long itemGroupId)
        {
            return ItemAdapter.RetrieveItemGroup(itemGroupId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemDto>> RetrieveAllItem(long itemGroupId)
        {
            return ItemAdapter.RetrieveAllItem(itemGroupId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemDto> RetrieveItem(long itemId)
        {
            return ItemAdapter.RetrieveItem(itemId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageItemGroup(ItemGroupDto dto)
        {
            return ItemAdapter.ManageItemGroup(dto);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveItemList()
        {
            return ItemAdapter.RetrieveItemList();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemDto>> RetrieveAllSalesItem()
        {
            return ItemAdapter.RetrieveAllSalesItem();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse AddNewItemGroupInline(string itemGroupName)
        {
            return ItemAdapter.AddNewItemGroupInline(itemGroupName);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveItemGroupList()
        {
            return ItemAdapter.RetrieveItemGroupList();
        }

        #region ItemAdjustment

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemAdjustmentDto>> RetrieveAllItemAdjustment()
        {
            return ItemAdapter.RetrieveAllItemAdjustment();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemAdjustmentItemDto>> RetrieveAllItemAdjustmentItem(long AdjustmentId)
        {
            return ItemAdapter.RetrieveAllItemAdjustmentItem(AdjustmentId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemAdjustmentDto> RetrieveItemAdjustment(long AdjustmentId)
        {
            return ItemAdapter.RetrieveItemAdjustment(AdjustmentId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageItemAdjustment(ItemAdjustmentDto dto)
        {
            return ItemAdapter.ManageItemAdjustment(dto);;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidItemAdjustment(long AdjustmentId)
        {
            return ItemAdapter.VoidItemAdjustment(AdjustmentId);
        }

        #endregion

        #region ItemTransfer

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemTransferDto>> RetrieveAllItemTransfer()
        {
            return ItemAdapter.RetrieveAllItemTransfer();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemTransferItemDto>> RetrieveAllItemTransferItem(long TransferStockId)
        {
            return ItemAdapter.RetrieveAllItemTransferItem(TransferStockId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemTransferDto> RetrieveItemTransfer(long TransferId)
        {
            return ItemAdapter.RetrieveItemTransfer(TransferId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageItemTransfer(ItemTransferDto dto)
        {
            return ItemAdapter.ManageItemTransfer(dto); ;
        }

        #endregion

        #region ItemReceiveTransfer

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemReceiveTransferDto>> RetrieveAllItemReceiveTransfer()
        {
            return ItemAdapter.RetrieveAllItemReceiveTransfer();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemReceiveTransferItemDto>> RetrieveAllItemReceiveTransferItem(long ReceiveId)
        {
            return ItemAdapter.RetrieveAllItemReceiveTransferItem(ReceiveId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemReceiveTransferDto> RetrieveItemReceiveTransfer(long ReceiveId)
        {
            return ItemAdapter.RetrieveItemReceiveTransfer(ReceiveId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageItemReceiveTransfer(ItemReceiveTransferDto dto)
        {
            return ItemAdapter.ManageItemReceiveTransfer(dto); ;
        }

        #endregion

        #region CurrentStock

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemCurrentStockDto>> RetrieveCurrentStock(ItemCurrentStockSelectionDto selection)
        {
            return ItemAdapter.RetrieveCurrentStock(selection);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemGroupImageDto> LoadItemImage(long ItemId)
        {
            return ItemAdapter.LoadItemImage(ItemId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SaveItemImage(ItemGroupImageDto dto)
        {
            return ItemAdapter.SaveItemImage(dto);
        }

        #endregion

        #region ItemCreation

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> AddNewItem(ItemDto dto)
        {
            return ItemAdapter.AddNewItem(dto);
        }

        #endregion

        #region ItemCategory
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemCategoryDto>> RetrieveAllItemCategory()
        {
            var resp = new GenericResponse<IList<ItemCategoryDto>>();
            resp = ItemAdapter.RetrieveAllItemCategory();
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemCategoryItemDto>> RetrieveAllItemCategoryItem(long ItemCategoryId)
        {
            var resp = new GenericResponse<IList<ItemCategoryItemDto>>();
            resp = ItemAdapter.RetrieveAllItemCategoryItem(ItemCategoryId);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemCategoryDto> RetrieveItemCategory(long ItemCategoryId)
        {
            return ItemAdapter.RetrieveItemCategory(ItemCategoryId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageItemCategory(ItemCategoryDto dto)
        {
            return ItemAdapter.ManageItemCategory(dto);
        }
        #endregion

        #region ItemBrand
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemBrandDto>> RetrieveAllItemBrand()
        {
            var resp = new GenericResponse<IList<ItemBrandDto>>();
            resp = ItemAdapter.RetrieveAllItemBrand();
            return resp;
        }
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<ItemBrandItemDto>> RetrieveAllItemBrandItem(long ItemBrandId)
        {
            var resp = new GenericResponse<IList<ItemBrandItemDto>>();
            resp = ItemAdapter.RetrieveAllItemBrandItem(ItemBrandId);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemBrandDto> RetrieveItemBrand(long ItemBrandId)
        {
            return ItemAdapter.RetrieveItemBrand(ItemBrandId);
        }
        #endregion

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<ItemGroupInfoDto> RetrieveItemGroupInfo(long ItemGroupId)
        {
            return ItemAdapter.RetrieveItemGroupInfo(ItemGroupId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageItemBrand(ItemBrandDto dto)
        {
            return ItemAdapter.ManageItemBrand(dto);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<string> RetrieveItemView(long itemId)
        {
            return ItemAdapter.RetrieveItemView(itemId);
        }
    }
}
