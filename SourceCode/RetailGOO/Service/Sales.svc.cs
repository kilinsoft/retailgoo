﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web;

using RetailGOO.Dto;
using RetailGOO.Util;
using RetailGOO.Class;
using RetailGOO.BusinessLayer;

namespace RetailGOO.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class Sales
    {
        #region CashSales

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<SalesInvoiceDto>> RetrieveAllCashSales()
        {
            return SalesAdapter.RetrieveAllCashSales();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<SalesInvoiceItemDto>> RetrieveAllCashSalesItem(long SalesInvoiceId)
        {
            return SalesAdapter.RetrieveAllCashSalesItem(SalesInvoiceId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<SalesInvoiceDto> RetrieveCashSales(long SalesInvoiceId)
        {
            return SalesAdapter.RetrieveCashSales(SalesInvoiceId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageCashSales(SalesInvoiceDto dto)
        {
            var resp = SalesAdapter.ManageCashSales(dto);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidCashSales(long SalesInvoiceId)
        {
            return SalesAdapter.VoidCashSales(SalesInvoiceId);
        }

        #endregion

        #region SalesInvoice

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<SalesInvoiceDto>> RetrieveAllSalesInvoice()
        {
            return SalesAdapter.RetrieveAllSalesInvoice();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<SalesInvoiceItemDto>> RetrieveAllSalesInvoiceItem(long SalesInvoiceId)
        {
            //SalesInvoiceId = 9;
            return SalesAdapter.RetrieveAllSalesInvoiceItem(SalesInvoiceId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<SalesInvoiceDto> RetrieveSalesInvoice(long SalesInvoiceId)
        {
            return SalesAdapter.RetrieveSalesInvoice(SalesInvoiceId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<OptionItem>> RetrieveSalesInvoiceList()
        {
            return SalesAdapter.RetrieveSalesInvoiceList();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageSalesInvoice(SalesInvoiceDto dto)
        {
            var resp = SalesAdapter.ManageSalesInvoice(dto);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidSalesInvoice(long SalesInvoiceId)
        {
            return SalesAdapter.VoidSalesInvoice(SalesInvoiceId);
        }

        #endregion

        #region IncomingPayment

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<IncomingPaymentDto>> RetrieveAllIncomingPayment()
        {
            return SalesAdapter.RetrieveAllIncomingPayment();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<IncomingPaymentItemDto>> RetrieveAllIncomingPaymentItem(long IncomingPaymentId)
        {
            return SalesAdapter.RetrieveAllIncomingPaymentItem(IncomingPaymentId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IncomingPaymentDto> RetrieveIncomingPayment(long IncomingPaymentId)
        {
            return SalesAdapter.RetrieveIncomingPayment(IncomingPaymentId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse ManageIncomingPayment(IncomingPaymentDto dto)
        {
            var resp = SalesAdapter.ManageIncomingPayment(dto);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse VoidIncomingPayment(long IncomingPaymentId)
        {
            return SalesAdapter.VoidIncomingPayment(IncomingPaymentId);
        }

        #endregion

        #region POS

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<BillDto>> RetrieveAllPendingBill()
        {
            return SalesAdapter.RetrieveAllPendingBill();
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<BillDto> RetrieveBill(long billId)
        {
            return SalesAdapter.RetrieveBill(billId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<IList<BillItemDto>> RetrieveAllBillItem(long billId)
        {
            return SalesAdapter.RetrieveAllBillItem(billId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<long> SubmitHoldSale(BillDto dto)
        {
            var resp = SalesAdapter.SubmitHoldSale(dto);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<long> SubmitPaidBill(BillDto dto)
        {
            var resp = SalesAdapter.SubmitPaidBill(dto);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<long> CancelHoldSale(BillDto dto)
        {
            var resp = SalesAdapter.CancelHoldSale(dto);
            return resp;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public GenericResponse<long> ManageBill(BillDto dto)
        {
            var resp = SalesAdapter.ManageBill(dto);
            return resp;
        }

        #endregion
    }
}
