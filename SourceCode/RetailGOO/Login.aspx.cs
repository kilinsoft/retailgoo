﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "Retail GOO - Login";

            if (AuthenticationHelper.IsAuthenticated())
            {
                // Redirect to business screen if the user has not selected a business to work with
                if (HttpContext.Current.Session[SessionVariable.BusinessId] == null ||
                    HttpContext.Current.Session[SessionVariable.BusinessId].ToString() == "")
                {
                    Response.Redirect(RetailGooPageUrl.BusinessSelection);
                }
                else
                {
                    Response.Redirect(RetailGooPageUrl.Dashboard);
                }
            }
        }
    }
}