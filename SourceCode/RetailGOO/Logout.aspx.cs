﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO
{
    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string username = SessionHelper.GetCurrentUsername();
                Identity.SignOut(username);
            }
            catch (Exception ex)
            {
            }
            //Response.Redirect(RetailGooPageUrl.LogInPage);
        }
    }
}