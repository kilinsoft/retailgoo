﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NoAuth.Master" AutoEventWireup="true" CodeBehind="ProcessLink.aspx.cs" Inherits="RetailGOO.ProcessLink" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4" style="margin: 50px auto 50px auto;">
                <asp:Label ID="lblText" runat="server"></asp:Label> <br />
                <asp:HyperLink ID="hlTargetPage" runat="server"></asp:HyperLink>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</asp:Content>
