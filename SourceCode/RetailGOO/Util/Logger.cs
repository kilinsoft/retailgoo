﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.Util
{
    public static class Logger
    {
        public static readonly log4net.ILog SystemLog = log4net.LogManager.GetLogger("system");
        public static readonly log4net.ILog SessionLog = log4net.LogManager.GetLogger("session");
        public static void Init()
        {
            if (SystemLog != null)
            {
                var appenders = SystemLog.Logger.Repository.GetAppenders();
                foreach (log4net.Appender.RollingFileAppender appender in appenders)
                {
                    appender.MaxSizeRollBackups = 31; // 1 Month
                }
            }
            if (SessionLog != null)
            {
                var appenders = SystemLog.Logger.Repository.GetAppenders();
                foreach (log4net.Appender.RollingFileAppender appender in appenders)
                {
                    appender.MaxSizeRollBackups = 31; // 1 Month
                }
            }
        }
        public static void SessionDebug(string txt)
        {
            SessionLog.Debug(string.Format("ID[ {0} ] {1}:{2} {3}", Trace.GetSessionID(), Trace.GetClassName(), Trace.GetCurrentMethod(2), txt));
        }
        public static void SessionInfo(string txt)
        {
            SessionLog.Info(string.Format("ID[ {0} ] {1}", Trace.GetSessionID(), txt));
        }
        public static void SessionWarn(string txt)
        {
            SessionLog.Warn(string.Format("ID[ {0} ] {1}", Trace.GetSessionID(), txt));
        }
        public static void SessionError(string txt)
        {
            SessionLog.Error(string.Format("ID[ {0} ] {1}:{2} {3}", Trace.GetSessionID(), Trace.GetClassName(), Trace.GetCurrentMethod(2), txt));
        }
        public static void SessionFatal(string txt)
        {
            SessionLog.Fatal(string.Format("ID[ {0} ] {1}:{2} {3}", Trace.GetSessionID(), Trace.GetClassName(), Trace.GetCurrentMethod(2), txt));
        }

        public static void AuthSessionDebug(string txt)
        {
            SessionLog.Debug(string.Format("ID[ {0} ] User[ {1} ] {2}:{3} {4}", Trace.GetSessionID(), HttpContext.Current.Session[SessionVariable.Username], Trace.GetClassName(), Trace.GetCurrentMethod(2), txt));
        }
        public static void AuthSessionInfo(string txt)
        {
            SessionLog.Info(string.Format("ID[ {0} ] User[ {1} ] {2}", Trace.GetSessionID(), HttpContext.Current.Session[SessionVariable.Username], txt));
        }
        public static void AuthSessionWarn(string txt)
        {
            SessionLog.Info(string.Format("ID[ {0} ] User[ {1} ] {2}", Trace.GetSessionID(), HttpContext.Current.Session[SessionVariable.Username], txt));
        }
        public static void AuthSessionError(string txt)
        {
            SessionLog.Error(string.Format("ID[ {0} ] User[ {1} ] {2}:{3} {4}", Trace.GetSessionID(), HttpContext.Current.Session[SessionVariable.Username], Trace.GetClassName(), Trace.GetCurrentMethod(2), txt));
        }
        public static void AuthSessionFatal(string txt)
        {
            SessionLog.Fatal(string.Format("ID[ {0} ] User[ {1} ] {2}:{3} {4}", Trace.GetSessionID(), HttpContext.Current.Session[SessionVariable.Username], Trace.GetClassName(), Trace.GetCurrentMethod(2), txt));
        }

        public static void SysDebug(string txt)
        {
            SystemLog.Debug(string.Format("{0}:{1} {2}", Trace.GetClassName(2), Trace.GetCurrentMethod(2), txt));
        }
        public static void SysInfo(string txt)
        {
            SystemLog.Info(string.Format("{0}", txt));
        }
        public static void SysWarn(string txt)
        {
            SystemLog.Warn(string.Format("{0}", txt));
        }
        public static void SysError(string txt)
        {
            SystemLog.Error(string.Format("{0}:{1} {2}", Trace.GetClassName(2), Trace.GetCurrentMethod(2), txt));
        }
        public static void SysFatal(string txt)
        {
            SystemLog.Fatal(string.Format("{0}:{1} {2}", Trace.GetClassName(2), Trace.GetCurrentMethod(2), txt));
        }
        
    }
}