﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.UI;

using RetailGOO.Util.Constant;
using RetailGOO.Class;

namespace RetailGOO.Util
{
    public class HtmlSelectHelper
    {
        public static string ConvertToListString(IList<OptionItem> itemList)
        {
            IList<string> strList = new List<string>();

            foreach (var item in itemList)
            {
                strList.Add(string.Format("{{Value:{0},Text:'{1}'}}", item.Value, ToEscapeString(item.Text)));
            }
            string retString = string.Format("[{0}]", string.Join(",", strList));
            return retString;
        }
        public static string ConvertToListString(IList<OptionItemString> itemList)
        {
            IList<string> strList = new List<string>();

            foreach (var item in itemList)
            {
                strList.Add(string.Format("{{Value:'{0}',Text:'{1}'}}", item.Value, ToEscapeString(item.Text)));
            }
            string retString = string.Format("[{0}]", string.Join(",", strList));
            return retString;
        }

        public static string ConvertToListString(IList<KeyValuePair<string, string>> pairList, bool fAddDefaultItem = false, string defaultItemName = "")
        {
            IList<string> strList = new List<string>();
            if (fAddDefaultItem)
            {
                if (defaultItemName == "")
                {
                    strList.Add(string.Format("{{value:{0},name:'{1}'}}",
                        DefaultNullableValue.BigInt,
                        DefaultEnumString.Undefined));
                }
                else
                {
                    strList.Add(string.Format("{{value:{0},name:'{1}'}}",
                        "''",
                        defaultItemName));
                }
            }
            foreach (var pair in pairList)
            {
                strList.Add(string.Format("{{value:{0},name:'{1}'}}", pair.Key, pair.Value));
            }
            string ret = string.Format("[{0}]", string.Join(",", strList));
            return ret;
        }

        public static string ToEscapeString(string text)
        {
            var result = new StringBuilder(text.Length + 2);
            foreach (var c in text)
            {
                switch (c)
                {
                    case '\'': result.Append(@"\'"); break;
                    default: result.Append(c);  break;
                }
            }
            return result.ToString();
        }
    }
}