﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Facebook;

namespace RetailGOO.Util
{
    public class FacebookHelper
    {
        public static bool IsValidUser(string username, string accessToken)
        {
            bool fRet = false;
            try
            {
                var client = new FacebookClient(accessToken);
                dynamic result = client.Get("me", new { fields = "id" });
                string id = result.id;
                if (id.Equals(username))
                    fRet = true;
            }
            catch (Exception ex)
            {
                Logger.SysFatal(string.Format("Could not validate FacebookUser. User[ {0} ] AccessToken[ {1} ]. {2}", username, accessToken, ex.ToString()));
            }
            return fRet;
        }
    }
}