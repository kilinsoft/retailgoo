﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace RetailGOO.Util
{
    public static class Encryption
    {
        public static byte[] GenerateSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[16];
            rng.GetBytes(buff);
            //string strSalt = Convert.ToBase64String(buff);
            //byte[] byteSalt = Convert.FromBase64String(strSalt);
            //return strSalt;
            return buff;
        }

        public static byte[] GenerateHashedPassword(string text, byte[] salt)
        {
            byte[] plainText = Encoding.UTF8.GetBytes(text);
            HashAlgorithm algorithm = new SHA256Managed();
            byte[] strTextAndSalt = new byte[plainText.Length + salt.Length];
            for (int i = 0; i < plainText.Length; ++i)
                strTextAndSalt[i] = plainText[i];
            for (int i = 0; i < salt.Length; ++i)
                strTextAndSalt[plainText.Length + i] = salt[i];
            return algorithm.ComputeHash(strTextAndSalt);
        }

        public static bool ByteArraysMatch(byte[] b1, byte[] b2)
        {
            if (b1.Length != b2.Length)
                return false;
            for (int i = 0; i < b1.Length; ++i)
                if (b1[i] != b2[i])
                    return false;
            return true;
        }

        public static string SimpleEncrypt(string txt)
        {
            byte[] passBytes = System.Text.Encoding.Unicode.GetBytes(txt);
            return Convert.ToBase64String(passBytes);
        }

        public static string SimpleDecrypt(string encryptedTxt)
        {
            try
            {
                byte[] byteData = Convert.FromBase64String(encryptedTxt);
                return System.Text.Encoding.Unicode.GetString(byteData);
            }
            catch (Exception ex)
            {
                Logger.SysError(string.Format("Could not decrypted string[ {0} ]", encryptedTxt));
            }
            return "";
        }
    }
}