﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// SQL conns
using System.Data;
using System.Data.SqlClient;
using System.Configuration; // ConnectionStringSettings
using System.Web.Configuration; //ConnectionStringSettings
using RetailGOO.Util;


namespace RetailGOO.Util
{
    public static class DatabaseConnection
    {
        private static ConnectionStringSettings _connString = null;
        public static SqlConnection GetConnection()
        {
            if (_connString == null)
            {
                try
                {
                    _connString = ConfigurationManager.ConnectionStrings["LocalSqlServer"];
                }
                catch (Exception ex)
                {
                    Logger.SystemLog.Fatal(string.Format("Database connection initialization failed. {0}", ex.ToString()));
                    return null;
                }
            }
            return new SqlConnection(_connString.ToString());
        }

    }
}