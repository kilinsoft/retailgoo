﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace RetailGOO.Util
{
    public static class Trace
    {
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod(int callerStackFrame = 1)
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(callerStackFrame);

            return sf.GetMethod().Name;
        }

        public static string GetClassName(int callerStackFrame = 1)
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(callerStackFrame);

            return sf.GetMethod().ReflectedType.Name;
        }

        public static string GetSessionID()
        {
            return HttpContext.Current.Session.SessionID;
        }
    }
}