﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetailGOO.Util
{
    namespace Constant {
        public class SessionVariable
        {
            // Session
            public const string StartTime = "TimeStart";
            public const string Username = "Username";
            public const string UserProfile = "UserProfile";
            public const string UserId = "UserId";
            public const string User = "User";
            public const string BusinessId = "BusinessId";
            public const string BusinessName = "BusinessName";
            public const string Staff = "Staff";
        }

        public class RetailGooPageUrl
        {
            public const string LogInPage = "~/Login.aspx";
            public const string LogOutPage = "~/Logout.aspx";
            public const string BusinessSelection = "~/Page/BusinessSelection.aspx";
            public const string CreateBusiness = "~/Page/CreateBusiness.aspx";
            public const string Dashboard = "~/Page/Dashboard.aspx";
            public const string ProhibitedPage = "~/Page/ProhibitedPage.aspx";
            public const string ProcessLink = "www.gooretail.com/ProcessLink.aspx";
        }

        public struct DatabaseTable
        {
            public const string User = "User";
            public const string Business = "Business";
            public const string Staff = "Staff";
            public const string Customer = "Customer";
            public const string Supplier = "Supplier";
            public const string ItemGroup = "ItemGroup";
            public const string Item = "Item";
            public const string Branch = "Branch";
            public const string Brand = "Brand";
            public const string Category = "Category";
            public const string PurchaseOrder = "PurchaseOrder";
            public const string PurchaseOrderItem = "PurchaseOrderItem";
        }

        public struct DatabaseDefaultValue
        {
            public const string SystemUsername = "system";
        }

        public struct DefaultNullableValue
        {
            public const long BigInt = -1;
            public const string VarChar = "";
            public const string String = "";
        }

        public struct DefaultNullableOptionItem
        {
            public const int Value = -1;
            public const string Text = "<undefined>";
            public const string BusinessPartnerText = "<anonymous>";
            public const string HoldSaleCustomerName = "Customer";
        }

        public struct DefaultEnumString
        {
            public const string Undefined = "Undefined";
        }

        public struct TimeDefinition
        {
            public const int UserCreationValidityPeriod = 14; // Days;
            public const int BusinessCreationValidityPeriod = 60; // Days;
        }

        public struct BillStatus
        {
            public const string Open = "Open";
            public const string Done = "Done";
        }

        public class RegEx
        {
            public class UsernameRegEx
            {
                public const string Expression = "^[a-z][.a-z0-9_-]{4,31}$";
                public const string Description = "Username must be 5-32 character long and may consist of at least one letter and follow by: <a-z> <0-9> <.> <_> <->"; 
            }
            public class EmailRegEx
            {
                public const string Expression = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$";
                public const string Description = "Email must be in the following formats: <email@domain.com> <name.surname@domain.com>";
            }

            public class PersonNameRegEx
            {
                public const string Expression = "^[a-zA-Z ]{1,50}$";
                public const string Description = "Value must be 1-50 character long";
            }

            public class PasswordRegEx
            {
                public const string AlphanumericCharExpression = "^[a-zA-Z0-9]$";
                public const string ValidSymbols = "`~!@#$%^&*()-_=+[]{}|;:'\",<.>/\\? ";
                public const string Description = "Password must be 5-32 character long and may consist of letters, numbers and/or symbols";
            }
        }

        public class StaffRoleString
        {
            public const string Admin = "adm";
            public const string Manager = "mgr";
            public const string Purchase = "pur";
            public const string Clerk = "clk";
            public const string Sales = "sal";
        }

        public class StaffStatusString
        {
            public const string Active = "active";
            public const string Inactive = "inactive";
            public const string Waiting = "waiting";
            public const string Declined = "declined";
        }

        public class BusinessScreenString
        {
            public const string CashPurchase = "cp";
            public const string PurchaseOrder = "po";
            public const string ReceiveOrder = "ro";
            public const string OutgoingPayment = "op";
            public const string Expense = "ex";

            public const string CashSales = "cs";
            public const string SalesInvoice = "siv";
            public const string IncomingPayment = "ip";

            public const string ItemManagement = "im";
            public const string CategoryManagement = "cgm";
            public const string BrandManagement = "bm";
            public const string CountSheet = "csh";
            public const string CurrentStock = "cst";
            public const string AdjustStock = "as";
            public const string TransferStock = "ts";
            public const string ReceiveTransferStock = "rts";
            public const string ReorderStock = "rs";

            public const string CustomerInfo = "ci";
            public const string SupplierInfo = "si";

            public const string CashManagement = "cam";
        }

        public class ScreenPermissionString
        {
            public const string View = "v";
            public const string Delete = "d";
            public const string Edit = "e";
            public const string Create = "c";
            public const string All = "a";
        }

        public class ItemState
        {
            public const string New = "N";
            public const string Modified = "M";
            public const string Deleted = "D";
            public const string Original = "O";
        }

        public class Gender
        {
            public const string Male = "M";
            public const string Female = "F";
            public const string NotAvailable = "N/A";
        }

        public class Title
        {
            public const string Mr = "Mr";
            public const string Ms = "Ms";
            public const string Mrs = "Mrs";
            public const string NotAvailable = "N/A";
        }

        public class LinkResponseType
        {
            public const string StaffAccept = "staffaccept";
            public const string StaffReject = "staffreject";
            public const string UserConfirm = "userconfirm";
            public const string UserCancel = "usercancel";
        }

        public class DataOrder
        {
            public const string Ascending = "ASC";
            public const string Descending = "DESC";
        }

        public class SortByType
        {
            public const string NewlyAdded = "DateCreated";
            public const string RecentUpdated = "DateModified";
        }

        public class SystemSetting
        {
            public const string ApplicationName = "RetailGOO";
            public const string ActivationTypeCode = "Activate";
            public const string DeclinationTypeCode = "Decline";
            public const int ExpirationDay = 14;
        }
    }
}