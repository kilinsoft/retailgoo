﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;

using RetailGOO.Util;
using RetailGOO.Dto;


namespace RetailGOO.Util
{
    public static class Validator
    {
        public static bool ValidateString(string text, string regex)
        {
            bool fRet = true;
            try
            {
                var match = Regex.Match(text, regex, RegexOptions.IgnoreCase);
                fRet = match.Success;
            }
            catch (Exception ex)
            {
                fRet = false;
            }
            return fRet;
        }
        public static bool ValidateCountryCode(string code)
        {
            bool fRet = true;
            try
            {
                RegionInfo info = new RegionInfo(code);
            }
            catch (ArgumentException ex)
            {
                fRet = false;
            }
            return fRet;
        }
        public static bool ValidatePassword(string pwd)
        {
            bool fRet = true;
            var validSymbolCharArray = Constant.RegEx.PasswordRegEx.ValidSymbols.ToCharArray();
            var pwdCharArray = pwd.ToCharArray();
            for ( int i = 0; i < pwdCharArray.Length; ++i )
            {
                bool fCharValid = false;
                if (Regex.Match(pwdCharArray[i].ToString(), Constant.RegEx.PasswordRegEx.AlphanumericCharExpression).Success)
                    continue;
                else
                {
                    for (int j = 0; j < validSymbolCharArray.Length; ++j)
                    {
                        if (pwdCharArray[i].Equals(validSymbolCharArray[j]))
                        {
                            fCharValid = true;
                            break;
                        }
                    }
                }
                if (!fCharValid)
                {
                    fRet = false;
                    break;
                }
            }
            return fRet;
        }
    }
}