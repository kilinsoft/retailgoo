﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace RetailGOO.Util
{
    public class DateTimeHelper
    {
        //public static string ConvertToDateString(DateTime dt)
        //{
        //    return dt.ToString("dd/MM/yyyy");
        //}

        //public static string ConvertToDateTimeString(DateTime dt)
        //{
        //    return dt.ToString("dd/MM/yyyy hh:mm:ss");
        //}

        //public static string ConvertToTimeString(DateTime dt)
        //{
        //    return dt.ToString("hh:mm:ss");
        //}

        //public static DateTime ConvertToDateTime(string dtStr)
        //{
        //    if (dtStr == null || dtStr == "")
        //        return DateTime.Now;

        //    DateTime parsedDate = new DateTime();
        //    CultureInfo ci = new CultureInfo("en-US", false);
        //    if ( dtStr.Contains(" ") ) // Contains Date + Time
        //        parsedDate = DateTime.ParseExact(dtStr, "dd/MM/yyyy hh:mm:ss", ci);
        //    else
        //        parsedDate = DateTime.ParseExact(dtStr, "dd/MM/yyyy", ci);
        //    return parsedDate;
        //}

        public static string ConvertToDateString(DateTime dt)
        {
            return dt.ToString("yyyy'-'MM'-'dd");
        }

        public static string ConvertToDateTimeString(DateTime dt)
        {
            return dt.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
        }

        public static DateTime ConvertToDateTime(string dtStr)
        {
            if (dtStr == null || dtStr == "")
                return DateTime.Now.ToUniversalTime();

            return DateTime.Parse(dtStr, null, System.Globalization.DateTimeStyles.RoundtripKind);
        }
    }
}