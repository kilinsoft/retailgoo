﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RetailGOO.Util;
using RetailGOO.Util.Constant;
using RetailGOO.Dto;
using RetailGOO.Class;

namespace RetailGOO.Util
{
    public class EmailHelper
    {
        public static string GenerateInvitationEmailString(string businessName, string role, string guid)
        {
            string generatedActivationUrl = string.Format("{0}?type={1}&value={2}", RetailGooPageUrl.ProcessLink, SystemSetting.ActivationTypeCode, guid);
            string generatedDeclinationUrl = string.Format("{0}?type={1}&value={2}", RetailGooPageUrl.ProcessLink, SystemSetting.DeclinationTypeCode, guid);

            string activationLink = string.Format("<a href=\"{0}\">{1}</a>", generatedActivationUrl, generatedActivationUrl);
            string declinationLink = string.Format("<a href=\"{0}\">{1}</a>", generatedDeclinationUrl, "DECLINE");
            //return string.Format("Recipient email: {0}, Guid: {1}, Guid Expired: {2}, ActivateLink: {3}",
            //    email,
            //    guid,
            //    dateGuidExpired.ToString(),
            //    string.Format("{0}?guid={1}",
            //        WebsiteHelper.ConvertToAbsoluteUrl(RetailGooPageUrl.ProcessLink),
            //        guid));

            string content = 
                string.Format(
                    "<p>You are invited to join {0} as a {1}</p><p>Please click the link below to activate your account<br>{2}</p><p>or click {3} to decline this invitation.</p>",
                    SystemSetting.ApplicationName,
                    role,
                    activationLink,
                    declinationLink
                );
            return content;
        }
        public static BaseResponse SendEmail(string email, string content)
        {
            var rsp = new BaseResponse();
            {
                // Pro : ToDo : Send Email. For testing purpose, just return succeed().
                rsp.Succeed();
            }
            return rsp;
        }
    }
}