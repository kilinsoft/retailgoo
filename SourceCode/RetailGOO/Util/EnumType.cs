﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetailGOO.Util
{
    namespace EnumType
    {
        public enum ScreenPermission
        {
            View = 0,
            Delete = 1,
            Edit = 2,
            Create = 3,
            All = 4
        }

        public enum BusinessScreen
        {
            CashPurchase = 0,
            PurchaseOrder,
            ReceiveOrder,
            OutgoingPayment,
            Expense,

            CashSales,
            SalesInvoice,
            IncomingPayment,

            ItemManagement,
            CategoryManagement,
            BrandManagement,
            CountSheet,
            CurrentStock,
            AdjustStock,
            TransferStock,
            ReceiveTransferStock,
            ReorderStock,

            CustomerInfo,
            SupplierInfo,

            CashManagement,

            Other
        }

        public enum StaffRole { Admin = 0, Manager, Clerk, Purchase, Sales }
        public enum StaffStatus { Active = 0, Inactive, Waiting, Declined }
    }
}