﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetailGOO.Util
{
    public class DateTimeString
    {
        public string Data { get; set; }

        public DateTimeString(DateTime dt) {
            Data = DateTimeHelper.ConvertToDateTimeString(dt);
        }

        public override string ToString()
        {
            return Data;
        }

        public static implicit operator DateTimeString(DateTime dt)
        {
            return new DateTimeString(dt);
        }
    }
}