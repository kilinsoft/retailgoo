﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RetailGOO.Util
{
    public class ImageHelper
    {
        public string LoadImage(string fileName)
        {
            byte[] imageBytes;
            string base64String;
            Image img;

            using (MemoryStream streamBitmap = new MemoryStream())
            {
                img = Image.FromFile(fileName);
                img.Save(streamBitmap, ImageFormat.Jpeg);
                // Convert Image to byte[]
                imageBytes = streamBitmap.ToArray();

                // Convert byte[] to Base64 String
                base64String = Convert.ToBase64String(imageBytes);

                img.Dispose();
            }
            return String.Format(@"data:image/jpeg;base64,{0}", base64String);
        }

        public bool SaveImage(string fileName, string imageBase64String)
        {
            var imageBase64Data = Regex.Match(imageBase64String, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            var imageBinaryData = Convert.FromBase64String(imageBase64Data);
            Image resized;

            using (var streamBitmap = new MemoryStream(imageBinaryData))
            {
                using (var img = Image.FromStream(streamBitmap))
                {
                    resized = (Image)(new Bitmap(img, new Size(150, 150)));

                    resized.Save(fileName, ImageFormat.Jpeg);
                }
            }
            return true;
        }
    }
}