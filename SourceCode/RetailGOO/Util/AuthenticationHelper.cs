﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.Util
{
    public class AuthenticationHelper
    {
        public static bool IsAuthenticated()
        {
            if (HttpContext.Current.Session[SessionVariable.UserId] != null &&
                HttpContext.Current.Session[SessionVariable.User] != null)
                return true;

            // Pro : ToDo : Check if this is the correct method to validate cookie
            bool fAuthenticated = false;
            try
            {

                HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie == null)
                    return fAuthenticated;
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket.Name != "" && !authTicket.Expired)
                {
                    fAuthenticated = true;
                    HttpContext.Current.Session[SessionVariable.Username] = authTicket.Name;
                    // Find user information
                    RetailGOO.Dto.GenericResponse<Class.User> rspUser = null;
                    rspUser = BusinessLayer.Identity.GetUserByUsername(authTicket.Name);
                    if (rspUser.ResponseItem != null)
                    {
                        HttpContext.Current.Session[SessionVariable.UserId] = rspUser.ResponseItem.UserId;
                        HttpContext.Current.Session[SessionVariable.User] = rspUser.ResponseItem;

                        var rspUserProfile = BusinessLayer.Identity.GetUserProfile(rspUser.ResponseItem.UserId);
                        HttpContext.Current.Session[SessionVariable.UserProfile] = rspUserProfile.ResponseItem;

                        fAuthenticated = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SysFatal(string.Format("Could not check Authentication. {0}", ex.ToString()));
            }
            return fAuthenticated;
        }

        public static void CreateAuthenticationTicket(string username)
        {
            try
            {
                FormsAuthenticationTicket ticket =
                    new FormsAuthenticationTicket(
                        1,
                        username,
                        DateTime.Now,
                        // Pro : Edit Here : Extend Cookie time in Dev phase
                            //DateTime.Now.AddMinutes(30),
                        DateTime.Now.AddDays(1),
                        true, // Is Persistent 
                        DateTime.Now.ToString());

                string encryptedTicket = FormsAuthentication.Encrypt(ticket);

                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                cookie.Expires = DateTime.Now.AddMinutes(30);

                HttpContext.Current.Response.Cookies.Add(cookie);                
            }
            catch (Exception ex)
            {
                Logger.SysFatal(string.Format("Could not create AuthenticationTicket. {0}", ex.ToString()));
            }
        }

        public static void CancelAuthenticationTicket(string username)
        {
            try
            {
                FormsAuthenticationTicket ticket =
                        new FormsAuthenticationTicket(
                            1,
                            username,
                            DateTime.Now,
                            DateTime.Now.AddDays(-1),
                            false, // Is Persistent 
                            DateTime.Now.ToString());

                string encryptedTicket = FormsAuthentication.Encrypt(ticket);

                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception ex)
            {
                Logger.SysFatal(string.Format("Could not expire AuthenticationTicket. {0}", ex.ToString()));
            }
        }
    }
}