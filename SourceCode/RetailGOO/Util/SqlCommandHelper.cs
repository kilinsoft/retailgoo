﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using RetailGOO.Dto;
using RetailGOO.Class;

namespace RetailGOO.Util
{
    public class SqlCommandHelper
    {
        public string CommandText = "";
        public string StoredProcedureName = "";
        public long ReturnId = 0;
        public long RecordCount = 0;
        public IList<SqlParameter> ParamList = new List<SqlParameter>();
        public SqlCommand SqlCommand = null;
        public DataTable DataResult = new DataTable();
        public IList<SqlParameter> NonDataParamList = new List<SqlParameter>();
        private IList<string> OutParamList = new List<string>();
        public Dictionary<string, object> OutParams = new Dictionary<string, object>();
        public SqlTransaction SqlTransaction = null;

        public SqlCommandHelper()
        {
        }

        public BaseResponse RunSpNonQuery(string spName)
        {
            return this.ExecuteSp(spName, false);
        }

        public BaseResponse RunSpQuery(string spName)
        {           
            return this.ExecuteSp(spName, true);
        }

        private BaseResponse ExecuteSp(string spName, bool fQuery)
        {
            BaseResponse rsp = new BaseResponse();
            try
            {
                using (var conn = DatabaseConnection.GetConnection())
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        for (int i = 0; i < this.ParamList.Count; ++i)
                            cmd.Parameters.Add((SqlParameter)this.ParamList[i]);

                        SqlParameter returnParameter = cmd.Parameters.Add("@RetVal", SqlDbType.BigInt);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        if (fQuery)
                        {
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(this.DataResult);

                            RecordCount = long.Parse(returnParameter.Value.ToString());
                        }
                        else
                        {
                            cmd.ExecuteNonQuery();

                            foreach (var spParam in OutParamList)
                            {
                                OutParams.Add(spParam, cmd.Parameters[spParam].Value);
                            }
                        }

                        SqlCommand = cmd;
                        rsp.Succeed();
                    }
                }
            }
            catch (Exception ex)
            {
                rsp.Fail(string.Format("Error calling StoredProcedure [ {0} ]. {1}", spName, ex.ToString()));
            }
            return rsp;
        }

        public void AddDefaultInsertParams()
        {
            string username = SessionHelper.GetCurrentUsername();
            this.AddParam("@DateCreated", SqlDbType.DateTime, DateTime.Now);
            this.AddParam("@DateModified", SqlDbType.DateTime, DateTime.Now);
            this.AddParam("@CreatedBy", SqlDbType.VarChar, username);
            this.AddParam("@UpdatedBy", SqlDbType.VarChar, username);
        }

        public void AddDefaultParams()
        {
            DateTime dt = DateTime.Now;
            string username = SessionHelper.GetCurrentUsername();
            this.AddParam("@DateCreated", SqlDbType.DateTime, dt);
            this.AddParam("@DateModified", SqlDbType.DateTime, dt);
            this.AddParam("@CreatedBy", SqlDbType.VarChar, username);
            this.AddParam("@UpdatedBy", SqlDbType.VarChar, username);
        }

        public void AddParam(string spParamName, SqlDbType dbParamType, object value, ParameterDirection paramDirection = ParameterDirection.Input)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = spParamName;
            param.SqlDbType = dbParamType;
            if (dbParamType == SqlDbType.NVarChar || dbParamType == SqlDbType.VarChar ||
                dbParamType == SqlDbType.Char || dbParamType == SqlDbType.NChar)
                param.Value = value ?? "";
            else
                param.Value = value;
            param.Direction = paramDirection;
            this.ParamList.Add(param);
        }

        public void AddOutputParam(string spParamName, SqlDbType dbParamType)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = spParamName;
            param.SqlDbType = dbParamType;
            param.Direction = ParameterDirection.Output;
            this.ParamList.Add(param);

            this.OutParamList.Add(spParamName);
        }

        public void AddNonDataParam(string spParamName, SqlDbType dbParamType, object value)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = spParamName;
            param.SqlDbType = dbParamType;
            param.Value = value;
            this.NonDataParamList.Add(param);
        }

        public String GetParam(string spParamName)
        {
            if (SqlCommand != null)
                return SqlCommand.Parameters[spParamName].Value.ToString();
            else
                return "";
        }

        public BaseResponse ExecuteQuery(string queryCmd)
        {
            BaseResponse rsp = new BaseResponse();
            try
            {
                using (var conn = DatabaseConnection.GetConnection())
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        if (this.SqlTransaction != null)
                            cmd.Transaction = this.SqlTransaction;
                        for (int i = 0; i < this.ParamList.Count; ++i)
                            cmd.Parameters.Add((SqlParameter)this.ParamList[i]);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(this.DataResult);
                        SqlCommand = cmd;
                        rsp.Succeed();
                        CommandText = GetSqlCommandText(cmd);
                    }
                }
            }
            catch (Exception ex)
            {
                rsp.Fail(string.Format("Command[ {0} ]. {1}", CommandText, ex.ToString()));
            }
            return rsp;
        }

        public BaseResponse ExecuteInsert(string tableName)
        {
            BaseResponse rsp = new BaseResponse();
            string insertCmd = GenerateInsertCommand(tableName, this.ParamList);
            try
            {
                using (var conn = DatabaseConnection.GetConnection())
                {
                    conn.Open();
                    //using (var ts = new TransactionScope()) // Automatic rollback on error
                    //{
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        if (this.SqlTransaction != null)
                            cmd.Transaction = this.SqlTransaction;
                        for (int i = 0; i < this.ParamList.Count; ++i)
                            cmd.Parameters.Add((SqlParameter)this.ParamList[i]);
                        for (int i = 0; i < this.NonDataParamList.Count; ++i)
                            cmd.Parameters.Add((SqlParameter)this.NonDataParamList[i]);
                        //cmd.ExecuteNonQuery();
                        object obj = cmd.ExecuteScalar();
                        ReturnId = Convert.ToInt64(obj);
                        SqlCommand = cmd;
                        CommandText = GetSqlCommandText(cmd);
                    }
                    //    ts.Complete();
                    //}
                }
                rsp.Succeed();
            }
            catch (Exception ex)
            {
                rsp.Fail(string.Format("Command[ {0} ]. {1}", CommandText, ex.ToString()));
            }
            return rsp;
        }

        public BaseResponse ExecuteUpdate(string tableName, string whereClauseCondition)
        {
            BaseResponse rsp = new BaseResponse();
            string updateCmd = GenerateUpdateCommand(tableName, this.ParamList, whereClauseCondition);
            try
            {
                using (var conn = DatabaseConnection.GetConnection())
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        if (this.SqlTransaction != null)
                            cmd.Transaction = this.SqlTransaction;
                        for (int i = 0; i < this.ParamList.Count; ++i)
                            cmd.Parameters.Add((SqlParameter)this.ParamList[i]);
                        for (int i = 0; i < this.NonDataParamList.Count; ++i)
                            cmd.Parameters.Add((SqlParameter)this.NonDataParamList[i]);
                        cmd.ExecuteNonQuery();
                        SqlCommand = cmd;
                        CommandText = GetSqlCommandText(cmd);
                    }
                }
                rsp.Succeed();
            }
            catch (Exception ex)
            {
                rsp.Fail(string.Format("Command[ {0} ]. {1}", CommandText, ex.ToString()));
            }
            return rsp;
        }

        public BaseResponse ExecuteDelete(string tableName, string whereClauseCondition)
        {
            BaseResponse rsp = new BaseResponse();
            string deleteCmd = GenerateDeleteCommand(tableName, whereClauseCondition);
            try
            {
                using (var conn = DatabaseConnection.GetConnection())
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                    {
                        if (this.SqlTransaction != null)
                            cmd.Transaction = this.SqlTransaction;
                        for (int i = 0; i < this.ParamList.Count; ++i)
                            cmd.Parameters.Add((SqlParameter)this.ParamList[i]);
                        cmd.ExecuteNonQuery();
                        SqlCommand = cmd;
                        CommandText = GetSqlCommandText(cmd);
                    }
                }
                rsp.Succeed();
            }
            catch (Exception ex)
            {
                rsp.Fail(string.Format("Command[ {0} ]. {1}", CommandText, ex.ToString()));
            }
            return rsp;
        }

        public string GetSqlCommandText(SqlCommand cmd)
        {
            string query = cmd.CommandText;
            foreach (SqlParameter param in cmd.Parameters)
            {
                query = query.Replace(param.ParameterName, param.Value.ToString());
            }
            return query;
        }

        private string GenerateInsertCommand(string tableName, IList<SqlParameter> paramList)
        {
            string cmd = "INSERT INTO [" + tableName + "] (";
            for (int i = 0; i < paramList.Count; ++i)
            {
                SqlParameter param = (SqlParameter) paramList[i];
                if ( i != 0 ) // append "," before every param except the first one
                    cmd += ",";
                cmd += param.ParameterName.Substring(1, param.ParameterName.Length - 1);
            }
            cmd += ") VALUES (";
            for (int i = 0; i < paramList.Count; ++i)
            {
                SqlParameter param = (SqlParameter)paramList[i];
                if (i != 0) // append "," before every param except the first one
                    cmd += ",";
                cmd += param.ParameterName;
            }
            cmd += ");";

            cmd += "SELECT SCOPE_IDENTITY();";
            
            return cmd;
        }

        private string GenerateUpdateCommand(string tableName, IList<SqlParameter> paramList, string whereClauseCondition)
        {
            string cmd = "UPDATE " + tableName + " SET ";
            for (int i = 0; i < paramList.Count; ++i)
            {
                SqlParameter param = (SqlParameter)paramList[i];
                if (i != 0) // append "," before every param except the first one
                    cmd += ",";
                cmd += string.Format("{0}={1}",
                    param.ParameterName.Substring(1, param.ParameterName.Length - 1),
                    param.ParameterName);
            }
            cmd += " WHERE ";
            cmd += whereClauseCondition;
            cmd += ";";
            return cmd;
        }

        private string GenerateDeleteCommand(string tableName, string whereClauseCondition)
        {
            return string.Format("DELETE FROM {0} WHERE {1};", tableName, whereClauseCondition);
        }
    }
}