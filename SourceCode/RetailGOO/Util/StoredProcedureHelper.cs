﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using RetailGOO.Dto;
using RetailGOO.Class;

namespace RetailGOO.Util
{
    public class StoredProcedureHelper
    {
        private string _spName = "";
        private Dictionary<string, object> _params = new Dictionary<string, object>();
        public SqlCommand SqlCommand = null;
        public DataSet DataResult = new DataSet();
        public StoredProcedureHelper(string storedProcedureName)
        {
            this._spName = storedProcedureName;
        }
        public void AddParam(string spParamName, SqlDbType dbParamType, object value, ParameterDirection paramDirection = ParameterDirection.Input)
        {
            var param = new SqlParameter(spParamName, value);
            param.Direction = paramDirection;
            _params.Add(spParamName, param);
        }
        public void RemoveParam(string spParamName)
        {
            _params.Remove(spParamName);
        }
        public BaseResponse Execute()
        {
            var resp = new BaseResponse();
            try
            {
                using (var conn = DatabaseConnection.GetConnection())
                {
                    using( var cmd = new SqlCommand(_spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (KeyValuePair<string, object> pair in _params)
                            cmd.Parameters.Add((SqlParameter)pair.Value);
                        cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                        conn.Open();
                        //cmd.ExecuteNonQuery();
                        //SqlDataReader reader = cmd.ExecuteReader();
                        using (var da = new SqlDataAdapter(cmd))
                            da.Fill(this.DataResult);
                        int ret = (int) cmd.Parameters["@ReturnValue"].Value;
                        if (ret != 0)
                        {
                            resp.Fail(string.Format( "Database operation failed[ {0} ]", _spName ));
                            Logger.SessionWarn(resp.StatusText);
                        }
                        else
                            resp.Succeed();
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                resp.Fail(string.Format("Error in operation[ {0} ]. Reason[ {1} ] ErrorNumber[ {2} ] ErrorState[ {3} ]", _spName, sqlEx.Message, sqlEx.Number, sqlEx.State));
                Logger.SessionError(resp.StatusText);
            }
            catch (Exception ex)
            {
                resp.Fail(string.Format("Error in operation[ {0} ]. {1}. {2}", _spName, ex.Message, ex.ToString()));
                Logger.SessionError(resp.StatusText);
            }
            return resp;
        }
    }
}