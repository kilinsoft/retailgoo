﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RetailGOO.Util.Constant;
using RetailGOO.Class;

namespace RetailGOO.Util
{
    public class SessionHelper
    {
        public class BusinessInfo
        {
            private static double? _tax = null;
            public static double Tax {
                get
                {
                    if (_tax.HasValue)
                        return _tax.Value;
                    // Pro : ToDo : get Tax from DB
                    _tax = 0.07;
                    return _tax.Value;
                }
            }

            public static Dictionary<string, int> _docSetting = null;
            public static Dictionary<string,int> DocSetting
            {
                get
                {
                    if (_docSetting != null)
                        return _docSetting;

                    _docSetting = new Dictionary<string, int>();
                    // Pro : ToDo : get Document Template setting from DB, use default template=0 for now
                    _docSetting.Add(Constant.BusinessScreenString.CashPurchase, 0);
                    _docSetting.Add(Constant.BusinessScreenString.PurchaseOrder, 0);
                    _docSetting.Add(Constant.BusinessScreenString.ReceiveOrder, 0);
                    _docSetting.Add(Constant.BusinessScreenString.OutgoingPayment, 0);
                    _docSetting.Add(Constant.BusinessScreenString.CashSales, 0);
                    _docSetting.Add(Constant.BusinessScreenString.SalesInvoice, 0);
                    _docSetting.Add(Constant.BusinessScreenString.IncomingPayment, 0);
                    _docSetting.Add(Constant.BusinessScreenString.AdjustStock, 0);
                    _docSetting.Add(Constant.BusinessScreenString.CurrentStock, 0);
                    return _docSetting;
                }
            }
        }

        public static User GetCurrentUser()
        {
            if (HttpContext.Current.Session[SessionVariable.User] != null)
                return (User) HttpContext.Current.Session[SessionVariable.User];
            else
                return null;
        }

        public static string GetCurrentUsername()
        {
            if (HttpContext.Current.Session[SessionVariable.Username] != null)
                return HttpContext.Current.Session[SessionVariable.Username].ToString();
            else
                return "";
        }

        public static long GetCurrentUserId()
        {
            if (HttpContext.Current.Session[SessionVariable.UserId] != null)
                return long.Parse(HttpContext.Current.Session[SessionVariable.UserId].ToString());
            else
                return -1;
        }

        public static long GetCurrentBusinessId()
        {
            if (HttpContext.Current.Session[SessionVariable.BusinessId] == null)
                return 0;
            string businessIdStr = HttpContext.Current.Session[SessionVariable.BusinessId].ToString();
            try
            {
                return long.Parse(businessIdStr);
            }
            catch (Exception ex)
            {
                Logger.SysError(string.Format("Could not convert businessId[ {0} ]. {1}", businessIdStr, ex.ToString()));
            }
            return 0;
        }
        public static string GetCurrentBusinessName()
        {
            if (HttpContext.Current.Session[SessionVariable.BusinessName] == null)
                return "";
            return HttpContext.Current.Session[SessionVariable.BusinessName].ToString();
        }

        public static Staff GetCurrentStaff()
        {
            if (HttpContext.Current.Session[SessionVariable.Staff] == null)
            {
                Logger.SysError(
                    string.Format(
                        "Session.Staff is NULL. BusinessId[{0}] UserId[{1}]",
                        SessionHelper.GetCurrentBusinessId(),
                        SessionHelper.GetCurrentUserId()));
                return null;
            }
            else
                return (Staff)HttpContext.Current.Session[SessionVariable.Staff];
        }
    }
}