﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetailGOO.Util
{
    public class WebsiteHelper
    {
        public static string ConvertToAbsoluteUrl(string relativeUrl)
        {
            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }
        public static string GetWebsiteUrl()
        {
            return HttpContext.Current.Request.Url.Scheme + "://" + 
            HttpContext.Current.Request.Url.Authority +
            HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
        }
    }
}