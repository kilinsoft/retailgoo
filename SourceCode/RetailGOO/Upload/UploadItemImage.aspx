﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Printable.Master" AutoEventWireup="true" CodeBehind="UploadItemImage.aspx.cs" Inherits="RetailGOO.Upload.UploadItemImage" %>
<asp:Content ID="Content3" ContentPlaceHolderID="pageHead" runat="server">
    <link rel="stylesheet" href="./jquery.Jcrop.css" type="text/css" />
    <script src="../js/jquery.Jcrop.js"></script>
    <script type="text/javascript">
        var nMaxWaitAttempts = 3;
        var jcrop_api;

        function pageLoad() {
            initJcrop();
            waitForData();
        }
        function waitForData() {
            var dto = window.dto; // receive this dto data from parent window;
            if (dto == null) {
                if (nMaxWaitAttempts > 0) {
                    --nMaxWaitAttempts;
                    setTimeout(waitForData, 1000);
                }
            }
            else {
                renderImageData(dto);
            }
        }
        function renderImageData(srcitem) {
            $("#cropbox").attr("src", srcitem);
            jcrop_api.setImage(srcitem);
        }
        function initJcrop()
        {
            // Hide any interface elements that require Jcrop
            // (This is for the local user interface portion.)
            $('.requiresjcrop').hide();

            // Invoke Jcrop in typical fashion
            $('#cropbox').Jcrop({
                onChange: updatePreview,
                onSelect: updatePreview,
                aspectRatio: 1, //To keep aspect ratio
                boxWidth: 650, //To handle large images
                boxHeight: 400 //To handle large images
            }, function () {
                jcrop_api = this;
            });
        };
        function releaseCheck() {
            jcrop_api.setOptions({ allowSelect: true });
        };
        function updatePreview(c) {
            if(parseInt(c.w) > 0) {
                // Show image preview
                var imageObj = $("#cropbox")[0];
                var canvas = $("#preview")[0];
                // force width and height to 150px
                canvas.width = canvas.height = 150;
                var context = canvas.getContext("2d");
                if (imageObj != null && c.x != 0 && c.y != 0 && c.w != 0 && c.h != 0) {
                    context.drawImage(imageObj, c.x, c.y, c.w, c.h, 0, 0, canvas.width, canvas.height);
                }
            }
        };
        function onclickCurrent() {
            $("#cropbox").attr("src", choosefile);
            jcrop_api.setImage(choosefile);
        };
        function onclickOK() {
            // convert the image to the base64 string
            var jpegUrl = document.getElementById("preview").toDataURL("image/jpeg");
            //var jpegUrl = parseInt(jQuery('#w').val());
            // set the base64 string to the parent img tag
            window.opener.document.getElementById("imgItem").src = jpegUrl;
            // set the image name to the parent img name
            var imgeName = $('#btnChooseImageName').val();
            if (imgeName != "")
                window.opener.document.getElementById("imgName").value = $('#btnChooseImageName').val();
            // close this page
            window.close();
        };
        function onclickCancel() {
            window.close();
        };
        function onchangeChooseImage() {
            var filesSelected = document.getElementById("btnChooseImageName").files;
            if (filesSelected.length > 0) {
                var fileToLoad = filesSelected[0];

                var fileReader = new FileReader();
                fileReader.onload = function (fileLoadedEvent) {
                    var srcData = fileLoadedEvent.target.result; // <--- data: base64
                    // put image to jcrop_api
                    $("#cropbox").attr("src", srcData);
                    jcrop_api.setImage(srcData);
                }
                fileReader.readAsDataURL(fileToLoad);
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageBody" runat="server">
        <div>
            Upload Item Image
        </div>
        <div class="content-form section">
            <img src="" id="cropbox" style="width:650px;height:400px;" />
            <canvas id="preview" style="width:150px;height:150px;" />
        </div>
        <div>
            <input type="file" id="btnChooseImageName" value="Choose File" accept="image/*" onchange="onchangeChooseImage();" />
            <input type="button" id="btnOK" value="OK" onclick="onclickOK();" />
            <input type="button" id="btnCancel" value="Cancel" onclick="onclickCancel();" />
        </div>
    <div class="clearfix"></div>
</asp:Content>
