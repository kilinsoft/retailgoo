﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel;
using RetailGOO.Util.Constant;

namespace RetailGOO.Dto
{
    [DataContract]
    public class SearchParameterDto
    {
        [DataMember]
        public string FilterColumn { get; set; }
        [DataMember]
        public string FilterKeyword { get; set; }
        [DataMember]
        public string SortingColumn { get; set; }
        [DataMember]
        public string SortBy { get; set; }
        [DataMember]
        public string SortingDirection { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int RecordPerPage { get; set; }

        public SearchParameterDto()
        {
            FilterColumn = "";
            FilterKeyword = "";
            SortingColumn = "";
            SortingDirection = "DESC";
            PageIndex = 0;
            SortBy = SortByType.NewlyAdded;
            RecordPerPage = 25;
        }
    }
}