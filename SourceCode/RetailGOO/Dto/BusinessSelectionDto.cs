﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class BusinessSelectionDto
    {
        [DataMember]
        public long BusinessId { get; set; }

        [DataMember]
        public string BusinessName { get; set; }

        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public DateTime DateExpired { get; set; }
    }
}