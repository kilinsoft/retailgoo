﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class UserProfileDto : BaseDto
    {
        [DataMember]
        public long UserId { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public long StaffId { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Firstname { get; set; }

        [DataMember]
        public string Lastname { get; set; }

        [DataMember]
        public string Birthdate { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string Identification { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string FacebookId { get; set; }

        [DataMember]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string Address3 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public long CountryId { get; set; }

        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public string RoleShortName { get; set; }

        [DataMember]
        public string RoleName { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Guid { get; set; }

        [DataMember]
        public string DateGuidExpired { get; set; }

        //[DataMember]
        //public DateTime DateCreated { get; set; }

        //[DataMember]
        //public DateTime DateModified { get; set; }
    }
}