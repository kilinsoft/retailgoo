﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    public class FinancialSettingDto : BaseDto
    {
        [DataMember]
        public long CurrencyId { get; set; }
        [DataMember]
        public long DefaultPriceTypeId { get; set; }
        [DataMember]
        public bool UseServiceCharge { get; set; }
        [DataMember]
        public decimal ServiceChargeAmount { get; set; }
        [DataMember]
        public long SalesTaxTypeId { get; set; }
        [DataMember]
        public decimal SalesTaxAmount { get; set; }
        [DataMember]
        public bool UseStockItemControl { get; set; }
        [DataMember]
        public long CostingTypeId { get; set; }
    }
}