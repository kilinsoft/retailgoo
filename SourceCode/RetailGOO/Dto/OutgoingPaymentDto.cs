﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class OutgoingPaymentItemDto : BaseDto
    {
        [DataMember]
        public long OutgoingPaymentItemId { get; set; }
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public long OutgoingPaymentId { get; set; }
        [DataMember]
        public string SupplierInvoice { get; set; }
        [DataMember]
        public long ReceiveOrderId { get; set; }
        [DataMember]
        public string ReceiveOrderNo { get; set; }
        //[DataMember]
        //public long PurchaseOrderId { get; set; }
        //[DataMember]
        //public string PurchaseOrderNo { get; set; }
        //[DataMember]
        //public string PurchaseOrderNo { get; set; }
        //[DataMember]
        //public long ItemId { get; set; }
        //[DataMember]
        //public string OptionName { get; set; }
        //[DataMember]
        //public decimal QuantityOrdered { get; set; }
        //[DataMember]
        //public decimal QuantityReceived { get; set; }
        //[DataMember]
        //public decimal PoQuantity { get; set; }
        [DataMember]
        public decimal AmountPaid { get; set; } // Amount Paid
        [DataMember]
        public decimal AmountDue { get; set; }
        [DataMember]
        public string Description { get; set; }
        //[DataMember]
        //public decimal Price { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
        //[DataMember]
        //public string UnitOfMeasure { get; set; }

        public OutgoingPaymentItemDto()
        {
            this.OutgoingPaymentId = -1;
            this.OutgoingPaymentItemId = -1;
            this.ReceiveOrderId = -1;
            this.IsDisabled = false;
        }
    }

    [DataContract]
    public class OutgoingPaymentDto : BaseDto
    {
        [DataMember]
        public long OutgoingPaymentId { get; set; }

        //[DataMember]
        //public long BusinessId { get; set; }

        [DataMember]
        public string OutgoingPaymentNo { get; set; }

        [DataMember]
        public long SupplierId { get; set; }

        [DataMember]
        public string SupplierName { get; set; }

        [DataMember]
        public string Reference { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string Detail1 { get; set; }

        [DataMember]
        public string Detail2 { get; set; }

        [DataMember]
        public string Detail3 { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Date { get; set; }

        //[DataMember]
        //public string AmountType { get; set; }

        //[DataMember]
        //public string PaymentMethod { get; set; }

        [DataMember]
        public string TaxIdInfo { get; set; }

        [DataMember]
        public int AmountTypeId { get; set; }

        [DataMember]
        public string AmountTypeName { get; set; }

        [DataMember]
        public int PaymentTypeId { get; set; }

        [DataMember]
        public string PaymentTypeName { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public decimal Subtotal { get; set; }

        [DataMember]
        public decimal Tax { get; set; }

        [DataMember]
        public decimal Total { get; set; }

        [DataMember]
        public string Status { get; set; }

        //[DataMember]
        //public bool IsVoided { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        //[DataMember]
        //public DateTime DateCreated { get; set; }

        //[DataMember]
        //public DateTime DateModified { get; set; }

        //[DataMember]
        //public string CreatedBy { get; set; }

        [DataMember]
        public IList<OutgoingPaymentItemDto> Items { get; set; }

        public OutgoingPaymentDto()
        {
            this.OutgoingPaymentId = -1;
            this.SupplierId = -1;
            this.IsDisabled = false;
        }
    }

}