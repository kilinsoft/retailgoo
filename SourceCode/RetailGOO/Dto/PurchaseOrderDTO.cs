﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class PurchaseOrderItemDto : BaseDto
    {
        [DataMember]
        public long PurchaseOrderItemId { get; set; }
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public long ItemId { get; set; }
        [DataMember]
        public string ItemFullName { get; set; }
        [DataMember]
        public long PurchaseOrderId { get; set; }
        [DataMember]
        public string PurchaseOrderNo { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public decimal Quantity { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
        [DataMember]
        public string UnitOfMeasure { get; set; }

        public PurchaseOrderItemDto()
        {
            this.PurchaseOrderItemId = -1;
            this.BusinessId = -1;
            this.ItemId = -1;
            this.IsDisabled = false;
        }
    }

    [DataContract]
    public class PurchaseOrderDto : BaseDto
    {
        [DataMember]
        public long PurchaseOrderId { get; set; }

        //[DataMember]
        //public long BusinessId { get; set; }

        [DataMember]
        public string PurchaseOrderNo { get; set; }

        [DataMember]
        public bool IsCashPurchase { get; set; }

        [DataMember]
        public string CashPurchaseNo { get; set; }

        [DataMember]
        public long SupplierId { get; set; }

        [DataMember]
        public string SupplierName { get; set; }

        [DataMember]
        public string Reference { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string Address3 { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Date { get; set; }

        [DataMember]
        public string DateDelivery { get; set; }

        [DataMember]
        public int AmountTypeId { get; set; }

        [DataMember]
        public string AmountTypeName { get; set; }

        [DataMember]
        public int PaymentTypeId { get; set; }

        [DataMember]
        public string PaymentTypeName { get; set; }
        
        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public string CreditTerm { get; set; }

        [DataMember]
        public decimal Subtotal { get; set; }

        [DataMember]
        public decimal Tax { get; set; }

        [DataMember]
        public decimal Total { get; set; }

        [DataMember]
        public string Status { get; set; }

        //[DataMember]
        //public bool IsVoided { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        //[DataMember]
        //public DateTime DateCreated { get; set; }

        //[DataMember]
        //public DateTime DateModified { get; set; }

        //[DataMember]
        //public string CreatedBy { get; set; }

        [DataMember]
        public IList<PurchaseOrderItemDto> Items { get; set; }

        public PurchaseOrderDto()
        {
            this.PurchaseOrderId = -1;
            this.SupplierId = -1;
        }
    }

}