﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemGroupInfoDto : BaseDto
    {
        [DataMember]
        public long ItemGroupId { get; set; }
        [DataMember]
        public string ItemGroupCode { get; set; }
        [DataMember]
        public string ItemGroupName { get; set; }
        [DataMember]
        public string ItemBrandName { get; set; }
        [DataMember]
        public decimal ItemQuantity { get; set; }
    }

    [DataContract]
    public class ItemGroupDto : BaseDto
    {
        [DataMember]
        public long ItemGroupId { get; set; }
        [DataMember]
        public string ItemGroupCode { get; set; }
        [DataMember]
        public string ItemGroupName { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
        [DataMember]
        public long ItemCategoryId { get; set; }
        [DataMember]
        public string ItemCategoryName { get; set; }
        [DataMember]
        public long ItemBrandId { get; set; }
        [DataMember]
        public string ItemBrandName { get; set; }
        [DataMember]
        public long ItemBranchId { get; set; }
        [DataMember]
        public string ItemBranchName { get; set; }
        [DataMember]
        public decimal Cost { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string UnitOfMeasure { get; set; }
        [DataMember]
        public bool IsForSales { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
        [DataMember]
        public string Quantity { get; set; }
        [DataMember]
        public IList<ItemDto> Items { get; set; }
    }
}