﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class BusinessDto
    {
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public string BusinessName { get; set; }
        [DataMember]
        public short BusinessTypeId { get; set; }
        [DataMember]
        public string BusinessTypeName { get; set; }
        [DataMember]
        public bool UseTemplateBusiness { get; set; }
        [DataMember]
        public long ImmediateBusinessId { get; set; }
        [DataMember]
        public long RootBusinessId { get; set; }

        [DataMember]
        public string ImagePath { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public short CountryId { get; set; }
        [DataMember]
        public string CountryName { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]
        public string FacebookUrl { get; set; }
        [DataMember]
        public string TaxIdInfo { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public DateTime DateExpired { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public DateTime DateModified { get; set; }

        public BusinessDto()
        {
            BusinessId = -1;
            BusinessName = "";
            CreatedBy = "";
            BusinessTypeId = -1;
            BusinessTypeName = "";
            ImagePath = "";
            Address1 = "";
            Address2 = "";
            Address3 = "";
            City = "";
            CountryId = -1;
            CountryName = "";
            ZipCode = "";
            Phone = "";
            Fax = "";
            Email = "";
            Website = "";
            FacebookUrl = "";
            TaxIdInfo = "";
            UpdatedBy = "";
            DateExpired = DateTime.Now;
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now;
        }

        public void Simplify()
        {
            BusinessName = BusinessName.Trim();
            ImagePath = ImagePath.Trim();
            Address1 = Address1.Trim();
            Address2 = Address2.Trim();
            Address3 = Address3.Trim();
            City = City.Trim();
            ZipCode = ZipCode.Trim();
            Phone = Phone.Trim();
            Fax = Fax.Trim();
            Email = Email.Trim();
            Website = Website.Trim();
            FacebookUrl = FacebookUrl.Trim();
            TaxIdInfo = TaxIdInfo.Trim();
        }
    }
}