﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class IncomingPaymentItemDto : BaseDto
    {
        // IP_003
        [DataMember]
        public long IncomingPaymentItemId { get; set; }
        [DataMember]
        public long SalesInvoiceId { get; set; }
        [DataMember]
        public string SalesInvoiceNo { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public decimal Due { get; set; }
        [DataMember]
        public decimal Pay { get; set; }

        public IncomingPaymentItemDto()
        {
            this.IncomingPaymentItemId = -1;
            this.SalesInvoiceId = -1;
        }
    }

    [DataContract]
    public class IncomingPaymentDto : BaseDto
    {
        [DataMember]
        public long IncomingPaymentId { get; set; }
        [DataMember]
        public string IncomingPaymentNo { get; set; }
        [DataMember]
        public long CustomerId { get; set; }
        [DataMember]
        public string Reference { get; set; }
        [DataMember]
        public long PaymentTypeId { get; set; }
        [DataMember]
        public long BankId { get; set; }
        [DataMember]
        public string BankAccNo { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public bool Vat { get; set; }
        [DataMember]
        public bool Wht { get; set; }
        [DataMember]
        public decimal WhtPercent { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public decimal Subtotal { get; set; }
        [DataMember]
        public decimal Tax { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public IList<IncomingPaymentItemDto> Items { get; set; }

        public IncomingPaymentDto()
        {
            this.IncomingPaymentId = -1;
            this.CustomerId = -1;
            this.IsDisabled = false;
        }
    }

}