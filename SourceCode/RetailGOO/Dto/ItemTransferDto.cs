﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemTransferItemDto : BaseDto
    {
        [DataMember]
        public long TransferItemId { get; set; }
        [DataMember]
        public long TransferId { get; set; }
        [DataMember]
        public long ItemId { get; set; }
        [DataMember]
        public decimal Quantity { get; set; }

        [DataMember]
        public string ItemFullName { get; set; }

        public ItemTransferItemDto()
        {
            this.TransferItemId = -1;
            this.TransferId = -1;
            this.ItemId = -1;
        }
    }

    [DataContract]
    public class ItemTransferDto : BaseDto
    {
        [DataMember]
        public long TransferId { get; set; }
        [DataMember]
        public string TransferName { get; set; }
        [DataMember]
        public long SourceBusinessId { get; set; }
        [DataMember]
        public string SourceBusinessName { get; set; }
        [DataMember]
        public long TargetBusinessId { get; set; }
        [DataMember]
        public string TargetBusinessName { get; set; }
        [DataMember]
        public string Reference { get; set; }
        [DataMember]
        public string DateSent { get; set; }
        [DataMember]
        public string DateReceived { get; set; }
        [DataMember]
        public long TransferStatusId { get; set; }
        [DataMember]
        public string TransferStatusName { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public IList<ItemTransferItemDto> Items { get; set; }

        public ItemTransferDto()
        {
            this.TransferId = -1;
            this.TransferStatusId = -1;
        }
    }

}