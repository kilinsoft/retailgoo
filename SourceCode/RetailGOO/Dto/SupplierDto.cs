﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class SupplierDto : BaseDto
    {
        [DataMember]
        public long SupplierId { get; set; }
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public string SupplierName { get; set; }
        [DataMember]
        public string SupplierNo { get; set; }
        [DataMember]
        public long SupplierGroupId { get; set; }
        [DataMember]
        public string SupplierGroupName { get; set; }
        [DataMember]
        public string TaxNo { get; set; }
        [DataMember]
        public string ContactPerson { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string CreditTerm { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public long CountryId { get; set; }
        [DataMember]
        public string CountryName { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string Remark { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
    }
}