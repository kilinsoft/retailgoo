﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemAdjustmentItemDto : BaseDto
    {
        [DataMember]
        public long AdjustmentItemId { get; set; }
        [DataMember]
        public long AdjustmentId { get; set; }
        [DataMember]
        public long ItemId { get; set; }
        [DataMember]
        public decimal QuantityBefore { get; set; }
        [DataMember]
        public decimal QuantityAfter { get; set; }

        [DataMember]
        public string ItemFullName { get; set; }
        [DataMember]
        public decimal Difference { get; set; }

        public ItemAdjustmentItemDto()
        {
            this.AdjustmentItemId = -1;
            this.AdjustmentId = -1;
            this.ItemId = -1;
        }
    }

    [DataContract]
    public class ItemAdjustmentDto : BaseDto
    {
        [DataMember]
        public long AdjustmentId { get; set; }
        [DataMember]
        public string AdjustmentName { get; set; }
        [DataMember]
        public long AdjustmentReasonId { get; set; }
        [DataMember]
        public string AdjustmentNo { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public IList<ItemAdjustmentItemDto> Items { get; set; }

        public ItemAdjustmentDto()
        {
            this.AdjustmentId = -1;
            this.AdjustmentReasonId = -1;
        }
    }
}