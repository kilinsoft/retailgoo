﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemCurrentStockDto : BaseDto
    {
        [DataMember]
        public string ItemGroupCode { get; set; }

        [DataMember]
        public string ItemCategoryName { get; set; }

        [DataMember]
        public string ItemBrandName { get; set; }

        [DataMember]
        public decimal Quantity { get; set; }
    }

    [DataContract]
    public class ItemCurrentStockSelectionDto : BaseDto
    {
        [DataMember]
        public long ItemGroupId { get; set; }

        [DataMember]
        public long ItemCategoryId { get; set; }

        [DataMember]
        public long ItemBrandId { get; set; }
    }
}