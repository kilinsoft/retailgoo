﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class SalesInvoiceItemDto : BaseDto
    {
        [DataMember]
        public long SalesInvoiceItemId { get; set; }
        [DataMember]
        public long SalesInvoiceId { get; set; }
        [DataMember]
        public long ItemId { get; set; }
        [DataMember]
        public decimal Quantity { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        //[DataMember]
        //public string DateCreated { get; set; }
        //[DataMember]
        //public string DateModified { get; set; }
        //[DataMember]
        //public string CreatedBy { get; set; }
        //[DataMember]
        //public string UpdatedBy { get; set; }
        [DataMember]
        public string ItemFullName { get; set; }
        [DataMember]
        public decimal Amount { get; set; }

        public SalesInvoiceItemDto()
        {
            this.SalesInvoiceItemId = -1;
            this.ItemId = -1;
        }
    }

    [DataContract]
    public class SalesInvoiceDto : BaseDto
    {
        [DataMember]
        public long SalesInvoiceId { get; set; }
        [DataMember]
        public string SalesInvoiceNo { get; set; }
        [DataMember]
        public bool IsCashSales { get; set; }
        [DataMember]
        public string CashSalesNo { get; set; }
        [DataMember]
        public long CustomerId { get; set; }
        [DataMember]
        public string Reference { get; set; }
        [DataMember]
        public string ContactPerson { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string DateDelivery { get; set; }
        [DataMember]
        public int DestinationTypeId { get; set; }
        [DataMember]
        public int PriceTypeId { get; set; }
        [DataMember]
        public int AmountTypeId { get; set; }
        [DataMember]
        public int PaymentTypeId { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public long SalesPersonId { get; set; }
        [DataMember]
        public decimal Subtotal { get; set; }
        [DataMember]
        public decimal Discount { get; set; }
        [DataMember]
        public decimal Charge { get; set; }
        [DataMember]
        public decimal Tax { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public decimal AmountPaid { get; set; }
        [DataMember]
        public decimal AmountDue { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
        //[DataMember]
        //public string DateCreated { get; set; }
        //[DataMember]
        //public string DateModified { get; set; }
        //[DataMember]
        //public string CreatedBy { get; set; }
        //[DataMember]
        //public string UpdatedBy { get; set; }
        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public IList<SalesInvoiceItemDto> Items { get; set; }

        public SalesInvoiceDto()
        {
            this.SalesInvoiceId = -1;
            this.CustomerId = -1;
            this.SalesPersonId = -1;
        }
    }

}