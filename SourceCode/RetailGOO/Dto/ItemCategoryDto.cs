﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemCategoryItemDto : BaseDto
    {
        [DataMember]
        public long ItemGroupId { get; set; }

        [DataMember]
        public string ItemGroupCode { get; set; }

        [DataMember]
        public string ItemGroupName { get; set; }

        [DataMember]
        public string ItemBrandName { get; set; }

        [DataMember]
        public decimal ItemQuantity { get; set; }

        public ItemCategoryItemDto()
        {
            this.ItemGroupId = -1;
        }
    }

    [DataContract]
    public class ItemCategoryDto : BaseDto
    {
        [DataMember]
        public long ItemCategoryId { get; set; }

        [DataMember]
        public string ItemCategoryName { get; set; }

        [DataMember]
        public decimal ItemQuantity { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public IList<ItemCategoryItemDto> Items { get; set; }

        public ItemCategoryDto()
        {
            this.ItemCategoryId = -1;
        }
    }
}