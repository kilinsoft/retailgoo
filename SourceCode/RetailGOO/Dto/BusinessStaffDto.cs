﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class BusinessStaffDto
    {
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public long StaffId { get; set; }
        [DataMember]
        public string Target { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string ItemState { get; set; }
    }
}