﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    public class BillDto : BaseDto
    {
        [DataMember]
        public long BillId { get; set; }

        [DataMember]
        public long BusinessId { get; set; }

        [DataMember]
        public long CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public decimal DiscountAmount { get; set; }

        [DataMember]
        public decimal DiscountPercent { get; set; }

        [DataMember]
        public bool IsDiscountPercent { get; set; }

        [DataMember]
        public decimal AmountCash { get; set; }

        [DataMember]
        public decimal AmountCheque { get; set; }

        [DataMember]
        public decimal AmountCredit { get; set; }

        [DataMember]
        public decimal Total { get; set; }

        [DataMember]
        public decimal Subtotal { get; set; }

        [DataMember]
        public decimal Tax { get; set; }

        [DataMember]
        public decimal AmountReceived { get; set; }

        [DataMember]
        public decimal AmountChange { get; set; }

        [DataMember]
        public decimal Quantity { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public IList<BillItemDto> BillItems { get; set; }

        [DataMember]
        public IList<BillPaymentItemDto> BillPaymentItems { get; set; }


        public BillDto()
        {
            BillId = -1;
            BusinessId = -1;
            CustomerId = -1;
            Status = Util.Constant.BillStatus.Open;
            AmountCash = AmountChange = AmountCheque = AmountCredit = DiscountAmount = DiscountPercent = AmountReceived = 0;
            Quantity = 0;
            Subtotal = Total = Tax = 0;
        }
    }

    public class BillItemDto : BaseDto
    {
        [DataMember]
        public long ItemId { get; set; }

        [DataMember]
        public string ItemFullName { get; set; }

        [DataMember]
        public long BillItemId { get; set; }

	    [DataMember]
        public long BillId { get; set; }

        [DataMember]
        public decimal Discount { get; set; }

        [DataMember]
        public decimal Quantity { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        public BillItemDto()
        {
            BillItemId = -1;
            BillId = -1;
            ItemId = -1;
            Discount = 0;
            Quantity = 0;
            Price = 0;
        }
    }

    public class BillPaymentItemDto : BaseDto
    {
        [DataMember]
        public long BillPaymentItemId { get; set; }

	    [DataMember]
        public long BillId { get; set; }

	    [DataMember]
        public int PaymentTypeId { get; set; }

	    [DataMember]
        public decimal Amount { get; set; }

	    [DataMember]
        public bool IsDisabled { get; set; }

        public BillPaymentItemDto()
        {
            BillPaymentItemId = -1;
            BillId = -1;
            BillPaymentItemId = -1;
            Amount = 0;
        }
    }
}