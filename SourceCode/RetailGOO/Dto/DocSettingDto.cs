﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class DocSettingDto : BaseDto
    {
        [DataMember]
        public long DocSettingId { get; set; }

        [DataMember]
        public string SalesOrderPrefix { get; set; }
        [DataMember]
        public bool SalesOrderYear { get; set; }
        [DataMember]
        public bool SalesOrderMonth { get; set; }
        [DataMember]
        public int SalesOrderNextNo { get; set; }
        [DataMember]
        public string SalesOrderSuffix { get; set; }
        //[DataMember]
        //public string SalesOrderPreview { get; set; }
        [DataMember]
        public int SalesOrderTemplate { get; set; }

        [DataMember]
        public string InvoicePrefix { get; set; }
        [DataMember]
        public bool InvoiceYear { get; set; }
        [DataMember]
        public bool InvoiceMonth { get; set; }
        [DataMember]
        public int InvoiceNextNo { get; set; }
        [DataMember]
        public string InvoiceSuffix { get; set; }
        [DataMember]
        public int InvoiceTemplate { get; set; }

        [DataMember]
        public string ReceiptPrefix { get; set; }
        [DataMember]
        public bool ReceiptYear { get; set; }
        [DataMember]
        public bool ReceiptMonth { get; set; }
        [DataMember]
        public int ReceiptNextNo { get; set; }
        [DataMember]
        public string ReceiptSuffix { get; set; }
        [DataMember]
        public int ReceiptTemplate { get; set; }

        [DataMember]
        public string StockAdjPrefix { get; set; }
        [DataMember]
        public bool StockAdjYear { get; set; }
        [DataMember]
        public bool StockAdjMonth { get; set; }
        [DataMember]
        public int StockAdjNextNo { get; set; }
        [DataMember]
        public string StockAdjSuffix { get; set; }
        [DataMember]
        public int StockAdjTemplate { get; set; }

        [DataMember]
        public string ReceiveOrderPrefix { get; set; }
        [DataMember]
        public bool ReceiveOrderYear { get; set; }
        [DataMember]
        public bool ReceiveOrderMonth { get; set; }
        [DataMember]
        public int ReceiveOrderNextNo { get; set; }
        [DataMember]
        public string ReceiveOrderSuffix { get; set; }
        [DataMember]
        public int ReceiveOrderTemplate { get; set; }

        [DataMember]
        public string CashPurchasePrefix { get; set; }
        [DataMember]
        public bool CashPurchaseYear { get; set; }
        [DataMember]
        public bool CashPurchaseMonth { get; set; }
        [DataMember]
        public int CashPurchaseNextNo { get; set; }
        [DataMember]
        public string CashPurchaseSuffix { get; set; }
        [DataMember]
        public int CashPurchaseTemplate { get; set; }

        [DataMember]
        public string CashSalesPrefix { get; set; }
        [DataMember]
        public bool CashSalesYear { get; set; }
        [DataMember]
        public bool CashSalesMonth { get; set; }
        [DataMember]
        public int CashSalesNextNo { get; set; }
        [DataMember]
        public string CashSalesSuffix { get; set; }
        [DataMember]
        public int CashSalesTemplate { get; set; }

        [DataMember]
        public string PurchaseOrderPrefix { get; set; }
        [DataMember]
        public bool PurchaseOrderYear { get; set; }
        [DataMember]
        public bool PurchaseOrderMonth { get; set; }
        [DataMember]
        public int PurchaseOrderNextNo { get; set; }
        [DataMember]
        public string PurchaseOrderSuffix { get; set; }
        [DataMember]
        public int PurchaseOrderTemplate { get; set; }

        [DataMember]
        public string PaymentPrefix { get; set; }
        [DataMember]
        public bool PaymentYear { get; set; }
        [DataMember]
        public bool PaymentMonth { get; set; }
        [DataMember]
        public int PaymentNextNo { get; set; }
        [DataMember]
        public string PaymentSuffix { get; set; }
        [DataMember]
        public int PaymentTemplate { get; set; }

        [DataMember]
        public string ExpensePrefix { get; set; }
        [DataMember]
        public bool ExpenseYear { get; set; }
        [DataMember]
        public bool ExpenseMonth { get; set; }
        [DataMember]
        public int ExpenseNextNo { get; set; }
        [DataMember]
        public string ExpenseSuffix { get; set; }
        [DataMember]
        public int ExpenseTemplate { get; set; }

        public DocSettingDto()
        {
            this.DocSettingId = -1;
        }
    }
}