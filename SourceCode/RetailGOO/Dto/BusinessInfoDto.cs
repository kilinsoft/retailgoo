﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class BusinessInfoDto : BaseDto
    {
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public string BusinessName { get; set; }
        [DataMember]
        public long BusinessTypeId { get; set; }
        [DataMember]
        public string BusinessTypeName { get; set; }
        [DataMember]
        public bool UseTemplateBusiness { get; set; }
        [DataMember]
        public long ImmediateBusinessId { get; set; }
        [DataMember]
        public long RootBusinessId { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public long CountryId { get; set; }
        [DataMember]
        public string CountryName { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]
        public string FacebookUrl { get; set; }
        [DataMember]
        public string TaxIdInfo { get; set; }
        [DataMember]
        public string DateExpired { get; set; }
    }
}