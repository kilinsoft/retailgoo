﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace RetailGOO.Dto
{
    [DataContract]
    public class FilterDto
    {
        [DataMember]
        public string OrderBy { get; set; }
        [DataMember]
        public string OrderType { get; set; }
        [DataMember]
        public DateTime? TimeStart { get; set; }
        [DataMember] 
        public DateTime? TimeEnd { get; set; }
    }
}