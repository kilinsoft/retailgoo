﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ExpenseDto : BaseDto
    {
        [DataMember]
        public long ExpenseId { get; set; }

        //[DataMember]
        //public long BusinessId { get; set; }

        [DataMember]
        public string ExpenseNo { get; set; }

        [DataMember]
        public long VendorId { get; set; }

        [DataMember]
        public string VendorName { get; set; }

        [DataMember]
        public long SupplierId { get; set; }

        [DataMember]
        public string SupplierName { get; set; }

        [DataMember]
        public long ExpenseCategoryId { get; set; }

        [DataMember]
        public string ExpenseCategoryName { get; set; }

        [DataMember]
        public bool UseWht { get; set; }

        [DataMember]
        public decimal WhtPercent { get; set; }

        [DataMember]
        public decimal WhtAmount { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public bool IsVoided { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public string Date { get; set; }

        //[DataMember]
        //public DateTime DateCreated { get; set; }

        //[DataMember]
        //public DateTime DateModified { get; set; }

        //[DataMember]
        //public string CreatedBy { get; set; }

        //[DataMember]
        //public IList<OutgoingPaymentItemDto> Items { get; set; }

        public ExpenseDto()
        {
            this.ExpenseId = -1;
            this.VendorId = -1;
            this.IsVoided = false;
            this.IsDeleted = false;
        }
    }

}