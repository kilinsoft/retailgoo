﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class CustomerDto : BaseDto
    {
        [DataMember]
        public long CustomerId { get; set; }
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string CustomerNo { get; set; }
        [DataMember]
        public long CustomerGroupId { get; set; }
        [DataMember]
        public string CustomerGroupName { get; set; }
        [DataMember]
        public string TaxNo { get; set; }
        [DataMember]
        public int DefaultPriceTypeId { get; set; }
        [DataMember]
        public string ContactPerson { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string CreditTerm { get; set; }
        [DataMember]
        public string CreditLimit { get; set; }
        [DataMember]
        public string AddressBilling1 { get; set; }
        [DataMember]
        public string AddressBilling2 { get; set; }
        [DataMember]
        public string AddressBilling3 { get; set; }
        [DataMember]
        public string CityBilling { get; set; }
        [DataMember]
        public int CountryBillingId { get; set; }
        [DataMember]
        public string CountryBillingName { get; set; }
        [DataMember]
        public string ZipCodeBilling { get; set; }
        [DataMember]
        public string RemarkBilling { get; set; }
        [DataMember]
        public string AddressShipping1 { get; set; }
        [DataMember]
        public string AddressShipping2 { get; set; }
        [DataMember]
        public string AddressShipping3 { get; set; }
        [DataMember]
        public string CityShipping { get; set; }
        [DataMember]
        public int CountryShippingId { get; set; }
        [DataMember]
        public string CountryShippingName { get; set; }
        [DataMember]
        public string ZipCodeShipping { get; set; }
        [DataMember]
        public string RemarkShipping { get; set; }
    }
}