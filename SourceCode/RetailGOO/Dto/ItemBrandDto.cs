﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemBrandItemDto : BaseDto
    {
        [DataMember]
        public long ItemGroupId { get; set; }

        [DataMember]
        public string ItemGroupCode { get; set; }

        [DataMember]
        public string ItemGroupName { get; set; }

        [DataMember]
        public string ItemBrandName { get; set; }

        [DataMember]
        public string ItemCategoryName { get; set; }

        [DataMember]
        public decimal ItemQuantity { get; set; }

        public ItemBrandItemDto()
        {
            this.ItemGroupId = -1;
        }
    }

    [DataContract]
    public class ItemBrandDto : BaseDto
    {
        [DataMember]
        public long ItemBrandId { get; set; }

        [DataMember]
        public string ItemBrandName { get; set; }

        [DataMember]
        public decimal ItemQuantity { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public IList<ItemBrandItemDto> Items { get; set; }

        public ItemBrandDto()
        {
            this.ItemBrandId = -1;
        }

    }
}