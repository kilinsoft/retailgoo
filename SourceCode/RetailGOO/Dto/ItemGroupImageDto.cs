﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemGroupImageDto : BaseDto
    {
        [DataMember]
        public long ItemGroupId { get; set; }

        [DataMember]
        public string ItemFileName { get; set; }

        [DataMember]
        public string UrlData { get; set; }

        public ItemGroupImageDto()
        {
            this.ItemGroupId = -1;
        }
    }
}