﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel;

using RetailGOO.Util.Constant;
using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class BaseDto
    {
        [DataMember]
        public string ItemState { get; set; }
        [DataMember]
        public string Version { get; set; }

        public BaseDto()
        {
            this.ItemState = Util.Constant.ItemState.Original;
            this.Version = DateTimeHelper.ConvertToDateTimeString(DateTime.Now);
        }
    }
}