﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using RetailGOO.Util;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ReceiveOrderItemDto : BaseDto
    {
        [DataMember]
        public long ReceiveOrderItemId { get; set; }
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public long ReceiveOrderId { get; set; }
        [DataMember]
        public long PurchaseOrderItemId { get; set; }
        [DataMember]
        public string PurchaseOrderNo { get; set; }
        //[DataMember]
        //public string PurchaseOrderNo { get; set; }
        [DataMember]
        public long ItemId { get; set; }
        [DataMember]
        public string ItemFullName { get; set; }
        //[DataMember]
        //public decimal QuantityOrdered { get; set; }
        //[DataMember]
        //public decimal QuantityReceived { get; set; }
        [DataMember]
        public decimal QuantityOrdered { get; set; }
        [DataMember]
        public decimal Quantity { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public decimal AmountPay { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public string UnitOfMeasure { get; set; }

        public ReceiveOrderItemDto()
        {
            this.ReceiveOrderItemId = -1;
            this.BusinessId = -1;
            this.ItemId = -1;
            this.IsDeleted = false;
        }
    }

    [DataContract]
    public class ReceiveOrderDto : BaseDto
    {
        [DataMember]
        public long ReceiveOrderId { get; set; }

        //[DataMember]
        //public long BusinessId { get; set; }

        [DataMember]
        public string ReceiveOrderNo { get; set; }

        [DataMember]
        public long SupplierId { get; set; }

        [DataMember]
        public string SupplierName { get; set; }

        [DataMember]
        public string Reference { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string Address3 { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Date { get; set; }

        //[DataMember]
        //public string AmountType { get; set; }

        //[DataMember]
        //public string PaymentMethod { get; set; }

        [DataMember]
        public string PaymentTerm { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public decimal Subtotal { get; set; }

        [DataMember]
        public decimal Tax { get; set; }

        [DataMember]
        public decimal Total { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public decimal AmountDue { get; set; }

        [DataMember]
        public decimal AmountPaid { get; set; }

        [DataMember]
        public IList<ReceiveOrderItemDto> Items { get; set; }

        public ReceiveOrderDto()
        {
            this.ReceiveOrderId = -1;
            this.SupplierId = -1;
        }
    }

}