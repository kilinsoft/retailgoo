﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class ItemDto : BaseDto
    {
        [DataMember]
        public long ItemId { get; set; }

        [DataMember]
        public string ItemName { get; set; }

        [DataMember]
        public string ItemFullName { get; set; }

        [DataMember]
        public long ItemGroupId { get; set; }

        [DataMember]
        public string ItemGroupCode { get; set; }

        [DataMember]
        public string ItemGroupName { get; set; }

        [DataMember]
        public long ItemBrandId { get; set; }

        [DataMember]
        public string ItemBrandName { get; set; }

        [DataMember]
        public long ItemCategoryId { get; set; }

        [DataMember]
        public string ItemCategoryName { get; set; }

        [DataMember]
        public long ItemBranchId { get; set; }

        [DataMember]
        public string ItemBranchName { get; set; }

        [DataMember]
        public string ImagePath { get; set; }

        [DataMember]
        public string OptionName { get; set; }

        [DataMember]
        public string Barcode { get; set; }

        [DataMember]
        public decimal Quantity { get; set; }

        [DataMember]
        public decimal Cost { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public decimal Price02 { get; set; }

        [DataMember]
        public decimal Price03 { get; set; }

        [DataMember]
        public decimal Price04 { get; set; }

        [DataMember]
        public string UnitOfMeasure { get; set; }

        [DataMember]
        public bool IsForSales { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        public string getOptionName()
        {
            return getOptionName(this.ItemGroupName, this.ItemGroupCode, this.ItemName);
        }

        public static string getOptionName(string itemGroupName, string itemGroupCode, string itemName)
        {
            if (itemGroupName == itemName)
                return string.Format("{0} {1}", itemGroupCode, itemGroupName);
            else
                return string.Format("{0} {1} - {2}", itemGroupCode, itemGroupName, itemName);
        }
    }
}