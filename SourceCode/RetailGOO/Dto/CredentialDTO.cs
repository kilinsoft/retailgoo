﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace RetailGOO.Dto
{
    [DataContract]
    public class CredentialDto
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public bool IsFacebookUser { get; set; }

        [DataMember]
        public string AccessToken { get; set; }

        public CredentialDto()
        {
            Username = "";
            Password = "";
            Email = "";
            IsFacebookUser = false;
            AccessToken = "";
        }

        public void Simplify()
        {
            Username = Username.Trim().ToLower();
            Password = Password.Trim();
            Email = Email.Trim().ToLower();
            AccessToken = AccessToken.Trim();
        }
    }
}