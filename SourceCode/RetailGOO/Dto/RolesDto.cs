﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RetailGOO.Dto
{
    [DataContract]
    public class RolesItemDto
    {
        [DataMember]
        public string PageName { get; set; }
        [DataMember]
        public string RoleTypeValue { get; set; }

        public RolesItemDto()
        {
        }
    }

    [DataContract]
    public class RolesDto : BaseDto
    {
        [DataMember]
        public long BusinessId { get; set; }
        [DataMember]
        public string RoleCategory { get; set; }
        [DataMember]
        public string PageCategory { get; set; }

        [DataMember]
        public IList<RolesItemDto> Items { get; set; }

        public RolesDto()
        {
            BusinessId = -1;
        }
    }
}