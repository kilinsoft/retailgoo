﻿var CSS_TOOLTIP_INVALID = "tooltip-invalid";
var CSS_TOOLTIP_INVALID_HEADER = "tooltip-invalid-header";
var CSS_TOOLTIP_INVALID_BODY = "tooltip-invalid-body";

// Regular expression for each CSS auto-validation class
var REGEX_USERNAME = /^[a-z][\.a-z0-9_-]{4,31}$/;
var REGEX_NOTEMPTY = /^.$/;
var REGEX_NUMBER = /^\d+$/;
var REGEX_EMAIL = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
var REGEX_PASSWORD = /^[\.\-\+\=\_\\/\[\]\{\}\|\;\:\'\"\?\,\<\.\>\!@#$%^&\*\(\)a-zA-Z0-9]{5,32}$/;
var REGEX_NAME = /^[a-zA-Z ]{1,50}$/;
var REGEX_LIMIT_LENGTH200 = /^(.|[\r\n]){0,200}$/;

// CSS classes to cause validation defined here
var CSS_AUTO_VALIDATE_PREFIX = 'auto-validate-';
var CSS_AUTO_VALIDATE_USERNAME = CSS_AUTO_VALIDATE_PREFIX + 'username';
var CSS_AUTO_VALIDATE_NOTEMPTY = CSS_AUTO_VALIDATE_PREFIX + 'notempty';
var CSS_AUTO_VALIDATE_NUMBER = CSS_AUTO_VALIDATE_PREFIX + 'number';
var CSS_AUTO_VALIDATE_LIMIT_LENGTH200 = CSS_AUTO_VALIDATE_PREFIX + 'limit-length200';
var CSS_AUTO_VALIDATE_EMAIL = CSS_AUTO_VALIDATE_PREFIX + 'email';
var CSS_AUTO_VALIDATE_PASSWORD = CSS_AUTO_VALIDATE_PREFIX + 'password';
var CSS_AUTO_VALIDATE_NAME = CSS_AUTO_VALIDATE_PREFIX + 'name';

// Function to adjust the value for CSS auto-validation class before they are validated
var ADJUSTFUNC_TRIM = function ($input) { $input.val($.trim($input.val())); };
var ADJUSTFUNC_TRIM_TOLOWER = function ($input) { $input.val($.trim($input.val()).toLowerCase()); };
var ADJUSTFUNC_TRIM_TOUPPER = function ($input) { $input.val($.trim($input.val()).toUpperCase()); };

// All behaviors of each auto-validation class are mapped here
// the auto-validation classes cannot be used together. Only one class can be applied to a control.
var CSS_AUTO_VALIDATE_CLASS = new Array(
    { cssClass: CSS_AUTO_VALIDATE_USERNAME, regex: REGEX_USERNAME, errorText: "Username must be 5-32 character long and may consist of at least one letter and follow by: <a-z> <0-9> <.> <_> <->", title: "Invalid Username", adjustFunc: ADJUSTFUNC_TRIM_TOLOWER },
    { cssClass: CSS_AUTO_VALIDATE_NOTEMPTY, regex: REGEX_NOTEMPTY, errorText: "The value must not be blank.", title: "Invalid value", adjustFunc: ADJUSTFUNC_TRIM },
    { cssClass: CSS_AUTO_VALIDATE_NUMBER, regex: REGEX_NUMBER, errorText: "The value must be a number", title: "Invalid number", adjustFunc: ADJUSTFUNC_TRIM },
    { cssClass: CSS_AUTO_VALIDATE_LIMIT_LENGTH200, regex: REGEX_LIMIT_LENGTH200, errorText: "The value must not be grater than 200 characters", title: "Invalid length", adjustFunc: null },
    { cssClass: CSS_AUTO_VALIDATE_EMAIL, regex: REGEX_EMAIL, errorText: "Email may contain letters, numbers and symbols <.> <_> <-> <@>", title: "Invalid Email", adjustFunc: ADJUSTFUNC_TRIM },
    { cssClass: CSS_AUTO_VALIDATE_PASSWORD, regex: REGEX_PASSWORD, errorText: "The value must be 5-32 character long and may consist of letters, numbers and/or symbols", title: "Invalid Password", adjustFunc: ADJUSTFUNC_TRIM },
    { cssClass: CSS_AUTO_VALIDATE_NAME, regex: REGEX_NAME, errorText: "The value must be 1-50 character long", title: "Invalid Name", adjustFunc: ADJUSTFUNC_TRIM });
// CSS clases to alter the data in a control. These classes may hook up both
// onblur and onkeyup events.
// The following CSS classes can be used together
var CSS_AUTO_TRIM = 'auto-trim';
var CSS_AUTO_TOLOWERCASE = 'auto-tolowercase';
var CSS_AUTO_TOUPPERCASE = 'auto-touppercase';

// Handlers for showing tooltip
var MOUSEENTER_HANDLER = function () {
    if ($(this).data('valid') == false) {
        // Disable all other invalid tooltips if active
        $('[class*=' + CSS_AUTO_VALIDATE_PREFIX + ']').each(function () { hideInvalidTooltip($(this)); });
        // Display invalid tooltip for this element
        for (var i = 0; i < CSS_AUTO_VALIDATE_CLASS.length; ++i) {
            var cssClasses = $(this).attr('class');
            if (cssClasses.indexOf(CSS_AUTO_VALIDATE_CLASS[i].cssClass) >= 0) {
                displayInvalidTooltip($(this), CSS_AUTO_VALIDATE_CLASS[i].errorText, CSS_AUTO_VALIDATE_CLASS[i].title);
                break;
            }
        }
    }
};
var MOUSELEAVE_HANDLER = function () {
    hideInvalidTooltip($(this));
};

// Main section to bind events to handlers
$(document).ready(function () {
    $('[class*=' + CSS_AUTO_VALIDATE_PREFIX + ']').each(function () { $(this).keyup(function () { if ($(this).data('valid') == false) validateElement($(this)); }); });
    $('.' + CSS_AUTO_TRIM).each(function () { $(this).blur(function () { makeTrim($(this)); }); });
    $('.' + CSS_AUTO_TOLOWERCASE).each(function () { $(this).keyup(function () { $(this).val($(this).val().toLowerCase()); }); });
    $('.' + CSS_AUTO_TOUPPERCASE).each(function () { $(this).keyup(function () { $(this).val($(this).val().toUpperCase()); }); });
});

// validateElement
//  Set the state of the element to true/false as a result from testing its value against the provided regex
//  or the regexes in CSS_AUTO_VALIDATE_CLASS. The state of the element is stored in $element.data('valid')
// Parameter:
//  $element    : JQuery-reference element to be validated
//  regex       : [Optional] a string containing the regular expression to be tested against.
//                If the parameter is not provided. It will search for an appropriate regex in
//                CSS_AUTO_VALIDATE_CLASS and bind tooltip to the element as well.
function validateElement($element, /*Optional*/regex) {
    if (!hasValue($element))
        return false;
    var regularEx = null;
    var mouseenterHandler;
    var mouseleaveHandler = function () {
        hideInvalidTooltip($element);
    };
    if (hasValue(regex))
        regularEx = regex;
    else {
        for (var i = 0; i < CSS_AUTO_VALIDATE_CLASS.length; ++i) {
            // Check if this element is a validator-bound element
            var cssClasses = $element.attr('class');
            if (cssClasses.indexOf(CSS_AUTO_VALIDATE_CLASS[i].cssClass) >= 0) {
                regularEx = CSS_AUTO_VALIDATE_CLASS[i].regex;
                if (typeof (CSS_AUTO_VALIDATE_CLASS[i].adjustFunc) == 'function')
                    CSS_AUTO_VALIDATE_CLASS[i].adjustFunc($element);
                break;
            }
        }
    }
    if (regularEx == null) {
        return false;
    } else if (!regularEx.test($element.val())) {
        // Check if the input is editable or else the invalid notification would be unnecessary.
        if ($element.isEnabled() && !$element.isReadOnly()) {
            $element.addClass('invalid');
            $element.data('valid', false);
            if (!hasValue(regex)) { // bind tooltip only if the element has CSS_AUTO_VALIDATE_CLASS
                $element.unbind('mouseenter', MOUSEENTER_HANDLER);
                $element.unbind('mouseleave', MOUSELEAVE_HANDLER);
                $element.bind('mouseenter', MOUSEENTER_HANDLER);
                $element.bind('mouseleave', MOUSELEAVE_HANDLER);
            }
        }
        return false;
    }
    else {
        $element.data('valid', true);
        $element.removeClass('invalid');
        $element.unbind('mouseenter', MOUSEENTER_HANDLER);
        $element.unbind('mouseleave', MOUSELEAVE_HANDLER);
        hideInvalidTooltip($element);
        return true;
    }
}

function makeTrim($element) {
    if (!hasValue($element))
        return false;
    $element.val($.trim($element.val()));
    return true;
}


// displayInvalidTooltip
//  : This function inserts a div tag right after the target element and display it as a tooltip
// Parameter
//  $element    : JQuery-reference element as a target element.
//  txt         : text to be displayed in the tooltip body.
//  title       : text to be displayed as a tooltip header.
function displayInvalidTooltip($element, txt, title) {
    if (!hasValue($element))
        return false;
    var FADEIN_DELAY = 50;
    // Check if there is an invalid tooltip of this element being displayed already.
    if ($('body').next() != null && !$('body').next().hasClass(CSS_TOOLTIP_INVALID) && $element.height() > 0) {
        var $tooltip = $('<div class=' + CSS_TOOLTIP_INVALID + '><div class=' + CSS_TOOLTIP_INVALID_HEADER + '>' + title + '</div><div class=' + CSS_TOOLTIP_INVALID_BODY + '>' + txt + '</div></td></tr></table></div>');
        $tooltip.cache($element.attr('id')); // Set the name of this tooltip with id of its parent element. The name will be used in hideInvalidTooltip function
        //$element.after($tooltip);
        $tooltip.hide();
        $('body').append($tooltip); // We have to insert it at the end of the body or else it will not be displayed correctly if it is used with JQuery dialog.
        var posX = $element.offset().left;
        var posY = $element.offset().top + $element.outerHeight();
        $tooltip.css('top', posY + "px");
        $tooltip.css('left', posX + "px");
        //$tooltip.fadeIn(FADEIN_DELAY);
        $tooltip.show(FADEIN_DELAY);
    }
    return true;
}

// displayInvalidTooltipForGroup
//  : This function inserts one div tag right after the first element of a specific group and display it as a tooltip
// Parameter
//  group   : [Optional] string which is a group name of one of CSS_AUTO_VALIDATE_CLASS. 
//            i.e. "group1" from class="auto-validate-csln[group1]"
//            This function treats all validation-bound elements as a single group if the parameter is not provided.
function displayInvalidTooltipForGroup(/*Optional*/group) {
    for (var i = 0; i < CSS_AUTO_VALIDATE_CLASS.length; ++i) {
        var jQuerySelector;
        if (hasValue(group))
            jQuerySelector = '.' + CSS_AUTO_VALIDATE_CLASS[i].cssClass + '\\[' + group + '\\]';
        else
            jQuerySelector = '.' + CSS_AUTO_VALIDATE_CLASS[i].cssClass;
        var $elementGroup = $(jQuerySelector);
        var $element = $elementGroup.first();
        if ($element != null) {
            if ($element.data('valid') == false) {
                displayInvalidTooltip($element, CSS_AUTO_VALIDATE_CLASS[i].errorText, CSS_AUTO_VALIDATE_CLASS[i].title);
                $element.focus();
                break;
            }
        }
    }
    return true;
}

// hideInvalidTooltip
//  : This function removes the tooltip which is a div tag being next to the target element
// Parameter
//  $element    : JQuery-reference element as a target element.
function hideInvalidTooltip($element) {
    var FADEOUT_DELAY = 0;
    if (!hasValue($element))
        return false;
    var $tooltips = $('div.' + CSS_TOOLTIP_INVALID);
    if ($tooltips != null) {
        $tooltips.each(function () {
            if ($(this).cache() == $element.attr('id')) {
                $(this).hide(FADEOUT_DELAY, function () { $(this).remove(); });
            }
        });
    }
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Global functions/variables to check state of the page
// Used for validation, detecting changes, etc.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

// formValid
//  Get the validity of the form as stated by global parameter "_FORM_VALID_"
//  or the state of specific group stated by the parameter
// Usage :
//  formValid() : Return the state
//  formValid( string groupName ) : Return the state of the specified group
function formValid(group) {
    // Validate the state of all validator-bound elements     
    var fValid = true;
    for (var i = 0; i < CSS_AUTO_VALIDATE_CLASS.length; ++i) {
        var jQuerySelector;
        if (hasValue(group))
            jQuerySelector = '.' + CSS_AUTO_VALIDATE_CLASS[i].cssClass + '\\[' + group + '\\]';
        else
            jQuerySelector = '.' + CSS_AUTO_VALIDATE_CLASS[i].cssClass;
        $(jQuerySelector).each(function () {
            // Reset all outstanding invalid tooltip and valid/invalid state
            clearInvalid($(this));
            validateElement($(this));
            if ($(this).data('valid') == false)
                fValid = false;
        });
    }
    // Set the tooltip to the first element that is in invalid state
    if (!fValid) // Display invalid tooltip
    {
        if (hasValue(group))
            displayInvalidTooltipForGroup(group);
        else
            displayInvalidTooltipForGroup();
    }
    return fValid;
}

// formValid
//  Clear all invalid notification 
// Usage :
//  clearInvalid() : Clear invalid status on all elements
//  clearInvalid( $element ) : Clear invalid status on element
function clearInvalid( /*Optional*/$element) {
    var clearFunc = function ($e) {
        hideInvalidTooltip($e);
        $e.removeClass('invalid');
        $e.data('valid', null);
    };
    if (hasValue($element))
        clearFunc($element);
    else
        $('[class*=' + CSS_AUTO_VALIDATE_PREFIX + ']').each(function () {
            clearFunc($(this));
        });
}

function isValidEmail(email) {
    return (new RegExp(REGEX_EMAIL)).test(email);
}
function isValidPassword(pwd) {
    return (new RegExp(REGEX_PASSWORD)).test(pwd);
}
