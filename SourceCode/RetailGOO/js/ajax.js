﻿function blockPage() {
    $.blockUI({ message: '<img src="../Images/ajax-loader.gif" />' });
}

function unblockPage() {
    $.unblockUI();
}


function sendAjax(url, data, successHandlerFunc, failureHandlerFunc) {
    if (typeof (successHandlerFunc) !== "function")
        return;
    if (typeof (failureHandlerFunc) !== "function")
        return;
    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(data),
        //timeout: 30000,
        dataType: 'JSON',
        contentType: 'application/json; charset=utf-8',
        processData: true,
        success: successHandlerFunc,
        error: failureHandlerFunc
    });
}

jQuery.fn.sendAjax = function (url, data, successHandlerFunc, failureHandlerFunc) {
    if (typeof (successHandlerFunc) !== "function")
        return;
    if (typeof (failureHandlerFunc) !== "function")
        return;
    var $obj = $(this);
    $obj.attr('disabled', 'disabled');
    sendAjax(
        url, 
        data, 
        function(resp) { 
            $obj.removeAttr('disabled');
            successHandlerFunc(resp); 
        }, 
        function(type, error, errorThrown) {
            $obj.removeAttr('disabled');
            failureHandlerFunc(type.responseText, error, errorThrown); 
        }
    );
}