﻿function doNewItemDialog(itemName) {
    $('body').append("\
        <div id='NewItemDialog' style='display: none;' title='Adds new item dialog'>\
          <p>Adds New Item.</p>\
          <table>\
              <tr>\
                  <td>Item Name</td>\
                  <td>\
                      <input type='text' id='txtIN' />\
                  </td>\
              </tr>\
              <tr>\
                  <td>Item Code</td>\
                  <td>\
                      <input type='text' id='txtIC' />\
                  </td>\
              </tr>\
          </table>\
      </div>\
    ");

    $('#txtIN').val(itemName);

    $("#NewItemDialog").dialog({
        autoOpen: true,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            Ok: function () {
                // add new item by ajax, the block type
                // 1) validates a value in 3 textbox.
                // 2) sends an item info ( item group name, catagory name, brand name ) to ajax function and block it
                // 3) gets a return, if error this function does nothing, then return item info to the caller
                if ($("#txtIN").val() == "") {
                    displayError("Please specify the item name.");
                    return false;
                }
                if ($("#txtIC").val() == "") {
                    displayError("Please specify the item code.");
                    return false;
                }

                var item = {};
                item["ItemGroupName"] = $("#txtIN").val();
                item["ItemGroupCode"] = $("#txtIC").val();
                item["ItemName"] = $("#txtIN").val();

                sendAjaxUpdate("ItemManagement.svc", "AddNewItem", item,
                    function (rsp) {
                        var item = rsp.ResponseItem;
                        if (g_cell != null) {
                            g_cell.getGrid().cacheSelection[String(g_cell.colDef.name)].push(item[0]);
                            g_cell.setValue(parseInt(item[0]["Value"]));
                        }
                        $("#NewItemDialog").dialog("close");
                    },
                    function (rsp) {
                        displayError(rsp.StatusText);
                    });
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            removeNewItemDialog();
        }
    });
}

function removeNewItemDialog() {
    $("#NewItemDialog").remove();
}