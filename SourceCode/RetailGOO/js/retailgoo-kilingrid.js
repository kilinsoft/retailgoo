﻿var __css_radio_select = "rad-select";

var ePageState = {
    mainView: "mainView",
    mainNew: "mainNew",
    mainEdit: "mainEdit",
    mainDelete: "mainDelete"
}

var eUndefinedValue = -1;
var eUndefinedText = "<undefined>";
var eUndefinedItem = { Value: eUndefinedValue, Text: eUndefinedText };

// Variables

var g_pageState = ePageState.mainNew;
var g_mainGrid = null;
var g_subGrid = null;

var g_mainItems = [];
var g_activeMainItem = null;
var g_subItems = [];
//var g_activeSubItem = null;

// Common Page Functions
function retrieveAjaxDataAsync(serviceName, contractName, queryItem, funcOnSuccess, funcOnFailure) {
    retrieveAjaxData(serviceName, contractName, queryItem, funcOnSuccess, funcOnFailure, false);
}
function retrieveAjaxDataSync(serviceName, contractName, queryItem, funcOnSuccess, funcOnFailure) {
    retrieveAjaxData(serviceName, contractName, queryItem, funcOnSuccess, funcOnFailure, true);
}
function retrieveAjaxData(serviceName, contractName, queryItem, funcOnSuccess, funcOnError, fBlockPage) {
    if (fBlockPage)
        blockPage();
    sendAjax(
        "../Service/" + serviceName + "/" + contractName,
        queryItem,
        function (resp) {
            if (resp.Success) {
                if (funcOnSuccess != null && funcOnSuccess != undefined)
                    funcOnSuccess(resp.ResponseItem, resp.VirtualItemCount);
            }
            else {
                if (funcOnError != null && funcOnError != undefined)
                    funcOnError(resp);
                else
                    alert(resp.StatusText);
            }
            if (fBlockPage)
                unblockPage();
        },
        function (type, err, errThrown) {
            if (funcOnError != null && funcOnError != undefined)
                funcOnError(err);
            else
                alert(err + ": " + errThrown);
            if (fBlockPage)
                unblockPage();
        });
}
function sendAjaxUpdate(serviceName, contractName, queryItem, funcOnSuccess, funcOnError) {
    blockPage();
    sendAjax(
        "../Service/" + serviceName + "/" + contractName,
        queryItem,
        function (resp) {
            unblockPage();
            if (resp.Success) {
                if (funcOnSuccess != null && funcOnSuccess != undefined)
                    funcOnSuccess(resp);
            }
            else {
                if (funcOnError != null && funcOnError != undefined)
                    funcOnError(resp);
                else
                    alert(resp.StatusText);
            }
        },
        function (type, err, errThrown) {
            var e = eval("(" + type.responseText + ")");
            alert(e.Message);
        });
}

//
// Grid Functions
//

function setGridData(grid, data) {
    setGridDataWithPage(grid, data, null, null);
}
function setGridDataWithPage(grid, data, nVirtualItems, pageIndex)
{
    var $selObj = null;
    if (grid === g_mainGrid) {
        g_mainItems = data;
        $selObj = $("#selMainSelectedBy option:first-child");
    }
    else if (grid === g_subGrid) {
        g_subItems = data;
        $selObj = $("#selSubSelectedBy option:first-child");
    }
    if ($selObj != null && $selObj.length)
        $selObj.prop("selected", true);
    grid.setData(data, nVirtualItems, pageIndex);
}

function activateRadioOnSelected(pRow) {
    pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
}
function activateRadioOnDeselected(pRow) {
    pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
}


// 
// Page General Functions
//

function alertClearFirst() {
    displayModal("<div class='info'>RETAIL GOO</div>", "Please save or discard your changes first.");
}
function displayError(txt) {
    displayModal("<div class='error'>ERROR!</div>", txt);
}
function displayInfo(txt) {
    displayModal("<div class='info'>RETAIL GOO</div>", txt);
}
function displayModal(dialogTitle, txt, funcOk, funcCancel) {
    var dt = new Date();
    var timeStr = dt.getHours() + dt.getMinutes() + dt.getSeconds();
    var modalName = "__bootstrapModalDialog";
    $("#" + modalName).remove(); // Remove any previous modal dialog
    $modalDiv = $("<div class=\"modal fade\" id=\"" + modalName + "\"> \
                  <div class=\"modal-dialog\"> \
                    <div class=\"modal-content\"> \
                      <div class=\"modal-header\"> \
                        <div class=\"modal-title\"></div> \
                      </div> \
                      <div class=\"modal-body\"> \
                        <p></p> \
                      </div> \
                      <div class=\"modal-footer\"></div> \
                    </div> \
                  </div> \
                </div>");
    $("body").append($modalDiv);
    $modalDiv.find(".modal-title").append(dialogTitle);

    $modalDiv.on('shown.bs.modal', function (e) {
        $(".btn.btn-primary").focus();
    })

    // display only 1 button if funcCancel=null
    if (funcCancel != undefined && funcCancel != null) {
        $btnCancel = $("<input type=\"button\" class=\"btn btn-default\" value=\"Cancel\" />");
        $btnCancel.on('click', function() {
            $modalDiv.modal('hide');
            $('.modal-backdrop').remove(); // ensure backdrop is disabled
            funcCancel();
        });
        $btnOk = $("<input type=\"button\" class=\"btn btn-primary\" value=\"OK\" />");
        $btnOk.on('click', function() {
            $modalDiv.modal('hide');
            $('.modal-backdrop').remove(); // ensure backdrop is disabled
            funcOk();
        });
        $modalDiv.find('.modal-footer').append($btnCancel).append($btnOk);
    }
    else {
        $btnOk = $("<input type=\"button\" class=\"btn btn-primary\" value=\"OK\" />");
        $btnOk.on('click', function () {
            $modalDiv.modal('hide');
            $('.modal-backdrop').remove(); // ensure backdrop is disabled
            if (funcOk != undefined && funcOk != null)
                funcOk();
        });
        $modalDiv.find('.modal-footer').append($btnOk);
    }
    $modalDiv.find('p').append(txt);
    $modalDiv.modal('show');
    $(":focus").blur();
}

function lockSubGridControl() {
    $("#btnSubEdit").disable();
    $("#btnSubDelete").disable();
    $("#selSubSelectedBy").disable();
}

function unlockSubGridControl() {
    $("#btnSubEdit").enable();
    $("#btnSubDelete").enable();
    $("#selSubSelectedBy").enable();
}

function lockMainGridControl() {
    $("#btnMainEdit").disable();
    $("#btnMainDelete").disable();
    $("#selSubSelectedBy").disable();
}

function unlockMainGridControl() {
    $("#btnMainEdit").enable();
    $("#btnMainDelete").enable();
    $("#selMainSelectedBy").enable();
}

function enableSaveClear() {
    $("#btnSave").removeClass("disable");
    $("#btnClear").removeClass("disable");
}

function disableSaveClear() {
    $("#btnSave").addClass("disable");
    $("#btnClear").addClass("disable");
}

// Grid Event Handlers

function createRadioSelectColumn(kGridRow, $col, gridName, iRow, iCol, value) {
    var radName = "rad_" + gridName;
    var $input = $("<input type=\"radio\" name=\"" + radName + "\" value=\"" + iRow + "\" class=\"" + __css_radio_select + "\" />");
    $input.attr("id", "rad_" + gridName + "_" + iRow);
    $col.append($input);
    return true;
};

function onDefaultRowSelected(kGridRow, prevSelectedRow) {
    kGridRow.$element.find("input[type=radio]").prop("checked", true);
    return true;
}

function onDefaultRowDeselected(kGridRow) {
    kGridRow.$element.find("input[type=radio]").prop("checked", false);
    return true;
}

function onDefaultRowCreated(kGridRow) {
    return true;
}

function onDefaultRowAdded(kGridRow) {
    return true;
}

function onDefaultRowModified(kGridRow) {
    return true;
}

function onDefaultRowDeleted(kGridRow) {
    return true;
}

// Dynamic tooltip
var g_dynamicTooltipTimer;
function delayDisplayTooltip(func, value) {
    g_dynamicTooltipTimer = setTimeout(function () {
        func(value);
    }, 1000);
}
function cancelDisplayTooltip() {
    clearTimeout(g_dynamicTooltipTimer);
}

// Page Common Controls

//var g_useMainSearch_ = false;
//var g_mainSearchText_ = "";
//var g_mainSearchCriteria_ = "";
//var g_funcDefaultLoad_ = null;
//var g_funcSearchSelectionCallback_ = null;
//var g_criteriaList_ = null;
//function registerMainSearch(funcDefault, funcCallback, criteriaList) {
//    var $txt = $("#txtMainSearch").removeProp('disabled');
//    var $btn = $("#btnMainSearch").removeProp('disabled');
//    $txt.keyup(function (e) {
//        if (e.keyCode == 13) {
//            $btn.trigger('click');
//        }
//    });
//    $btn.on('click', function () {
//        g_mainSearchText_ = $("#txtMainSearch").val();
//        if (String($.trim($txt.val())) == "") {
//            if (g_funcDefaultLoad_ != undefined || g_funcDefaultLoad_ != null)
//                g_funcDefaultLoad_();
//        }
//        else {
//            var $divOptions = $("<div></div>");
//            for (var i = 0; i < g_criteriaList_.length; ++i) {
//                var $option = $("<div></div>").addClass("search-option");
//                var criteria = g_criteriaList_[i];
//                $option.append("search for <span>" + g_mainSearchText_ + "</span> in: <span>" + criteria["Caption"] + "</span>");
//                $option.on('click', function () {
//                    $(".search-option").removeClass("active");
//                    $(this).addClass("active");

//                    var val = criteria["Value"];
//                    g_mainSearchCriteria_ = val;
//                });
//                $divOptions.append($option);
//            }
//            displayModal(
//                $divOptions,
//                function () {
//                    if (g_funcSearchSelectionCallback_ != undefined && g_funcSearchSelectionCallback_ != null)
//                        g_funcSearchSelectionCallback_(g_mainSearchCriteria_, g_mainSearchText_);
//                },
//                function () {
//                    g_mainSearchCriteria_ = "";
//                    g_mainSearchText_ = "";
//                    $("#txtMainSearch").val("");
//                }
//            );
//        }
//    });
//    g_useMainSearch_ = true;
//    g_funcDefaultLoad_ = funcDefault;
//    g_funcSearchSelectionCallback_ = funcCallback;
//    g_criteriaList_ = criteriaList;
//}
//function getSearchText() {
//    if (!g_useMainSearch_)
//        return "";
//    else
//        return g_mainSearchText_;
//}
//function getSearchField() {
//    if (!g_useMainSearch_)
//        return "";
//    else
//        return g_mainSearchCriteria_;
//}

function doDefaultCancel() {
    //var fClear = false;
    //if (isPageDirty()) {
    //    if (window.confirm("You are about to cancel your current action and discard all changes.\n\nDo you want to continue?")) {
    //        fClear = true;
    //    }
    //} else
    //    fClear = true;
    //if (fClear) {
    //    setPageDirty(false);
    //    location.reload();
    //}
    displayModal(
        "Confirm canceling",
        "You are about to cancel your current action and discard all changes.<br><br>Do you want to continue?",
        function () {
            setPageDirty(false);
            location.reload();
        },
        function () { });
}
function doDefaultClear() {
    displayModal(
        "Confirm clearing data",
        "Your unsaved data will be lost.<br><br>Do you want to continue?",
        function () {
            setPageDirty(false);
            switch (g_pageState) {
                case ePageState.mainNew:
                    changeState(ePageState.mainNew);
                    break;
                case ePageState.mainView:
                    break;
                case ePageState.mainEdit:
                    if (g_mainGrid.data.length > 0)
                        setFormData(g_activeMainItem);
                    // For screen with subGrid
                    if (g_subGrid != null) {
                        setGridData(g_subGrid, g_subItems);
                        g_subGrid.unlock();
                    }
                    changeState(ePageState.mainEdit);
                    break;
                default:
                    break;
            }
        },
        function () { });
}
function doDefaultVoid(serviceName, methodName, idColumnName) {
    var row = g_mainGrid.selectedRow;
    if (row == null) {
        displayError("Please select an item first");
        return;
    }
    if (row.data["Status"].toLowerCase() == "void") {
        displayError("The item has already been marked as VOID");
        return;
    }
    var fContinue = window.confirm("Voiding the item will affect all of its related items as well.\n\nDo you want to continue?");
    if (fContinue) {
        var idVoid = parseInt(row.data[idColumnName], 10);
        //sendAjaxUpdate("Purchasing.svc", "VoidPurchaseOrder", idVoid,
        sendAjaxUpdate(serviceName, methodName, idVoid,
            function (rsp) {
                setPageDirty(false);
                changeState(ePageState.mainView);
                var iRow = g_mainGrid.selectedRow.rowIndex;
                retrieveMainGridData(function () {
                    g_mainGrid.selectRow(iRow);
                });
            },
            function (rsp) {
                displayError(rsp.StatusText);
            });
    }
}
function doDefaultPrint() {
    window.print();
}

function initGridSorter(grid, selId, options) {
    var $sel = $("" + selId);
    for (var i = 0; i < options.length; ++i)
        $sel.append($("<option></option>").attr("value", options[i]["name"]).text(options[i]["title"]));
    $sel.on("change.gridSort", function (e) {
        var val = $sel.val();
        var fValueCompare = getAssocValue(options, "name", val, "valueCompare");
        for (var i = grid.data.length - 1; i >= 0; --i)
            if (grid.data[i].itemState == KGrid.ItemState.blankItem)
                grid.data.splice(i, 1);
        grid.data.sort(sortByKey(val, fValueCompare));
        grid.setData(grid.data);
        if (grid === g_mainGrid)
            changeState(ePageState.mainNew);
    });
}
function initGridSorterAjax(selId, options, funcCallback) {
    var $sel = $("" + selId);
    for (var i = 0; i < options.length; ++i)
        $sel.append($("<option></option>").attr("value", options[i]["name"]).text(options[i]["title"]));
    $sel.on("change.gridSort", function (e) {
        var val = $sel.val();
        funcCallback(val);
    });
}

function displayItemView(itemId) {
    sendAjax("../Service/ItemManagement.svc/RetrieveItemView", value,
        function (rsp) {
            var item = rsp.ResponseItem;
            var tooltipContent;
            tooltipContent = 'Date: ' + item['Date'] + '</br>';
            tooltipContent += 'Delivery Date: ' + item['DateDelivery'] + '</br>';
            //tooltipContent += 'Amount Type: ' + g_selAmountType.getOption(item['AmountTypeId'])[0].innerText + '</br>';
            tooltipContent += 'Payment Method: ' + g_selPaymentType.getOption(item['PaymentTypeId'])[0].innerText;
            $('#divDynamicTooltip').html(tooltipContent);
            $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
        },
        function (type, err, errThrown) {
        });
}

// Helper functions
function getAssocValue(aoItems, colFind, valFind, colTarget) {
    for (var i = 0; i < aoItems.length; ++i) {
        var obj = aoItems[i];
        if (obj[colFind] == valFind)
            return obj[colTarget];
    }
    return null;
}

function addUndefinedOption(a) {
    var fFoundUndefined = false;
    for (var i = 0; i < a.length; ++i) {
        if (parseInt(String(a[i]["Value"]), 10) == -1) {
            fFoundUndefined = true;
            break;
        }
    }
    if (!fFoundUndefined) {
        a.push(eUndefinedItem);
    }
}

function setComboBoxData(ddObject, list, selectedValue) { // For selectize
    ddObject.clear();
    ddObject.clearOptions();
    ddObject.addOption(list);
    if (selectedValue != null && selectedValue != undefined)
        ddObject.setValue(selectedValue);
}

// Class: MainSearch
//  $txtSearch:  jQuery element for search textbox
//  $btnSearch:   jQuery element for search button
//  funcSearchCallback:   function to be called when the user click search button or hit ENTER while the searh textbox is focused
//                        funcSearchCallback = function( queryText ); where queryText might be empty string ( "" );
function MainSearch($txtSearch, $btnSearch, funcSearchCallback) {

    this.searchText = "";
    this.funcCallback = funcSearchCallback;
    this.$txtSearch = $txtSearch.removeProp('disabled');
    this.$btnSearch = $btnSearch.removeProp('disabled');

    var self = this;

    this.$txtSearch.keyup(function (e) {
        if (e.keyCode == 13) {
            self.$btnSearch.trigger('click');
        }
    });
    this.$btnSearch.on('click', function () {
        self.searchText = String($.trim(self.$txtSearch.val()));
        self.funcCallback(self.searchText);
    });
}