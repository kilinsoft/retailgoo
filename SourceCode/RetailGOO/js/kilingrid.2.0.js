﻿(function ($) {
    $.extend(true, window, { // Register namespace
        KGrid: {
            ItemState: {
                blankItem: "B", // Blank row, added automatically to fill rowPerPage
                originalItem: "O", // Row with data and it has not been altered yet.
                newItem: "N", // Row that has been supplied with data on client-side, the former state is 'blank'
                modifiedItem: "M", // Data row that has been modified client-side, the former state is 'original'
                deletedItem: "D"
            },
            Formatter: {
                Amount: KAmountFormatter,
                Radio: KRadioFormatter,
                Text: KTextFormatter,
                Selection: KComboFormatter
            },
            Editor: {
                Selection: ComboEditor,
                Text: KTextEditor,
                Amount: KAmountEditor
            },
            Validator: {
                Require: KRequireValidator
            },
            Grid: KilinSoftGrid,
            CssClass: {
                //SelectorRadio: "k-sel-rad",
                //InputText: "k-input-text",
                supportOldIe: "k-grid-support-old-ie",
                gridContainer: "k-grid-container",

                grid: "k-grid",
                row: "k-row",
                cell: "k-cell",
                cellReadOnly: "k-cell-readonly",
                cellLast: "last",

                rowHeader: "k-row-header",
                cellHeader: "k-cell-header",

                gridBody: "k-grid-body",
                gridBodyDiv: "k-grid-body-div",

                gridHeader: "k-grid-header",
                gridHeaderDiv: "k-grid-header-div",
                gridPageArea: "k-grid-page",
                gridPageAreaDiv: "k-grid-page-div",
                gridPageItem: "k-grid-page-item",
                gridPageItemDisabled: "page-item-disabled",

                rowSelected: "k-row-selected",
                rowDeleted: "k-row-deleted",
                rowNew: "k-row-new",
                rowModified: "k-row-modified",
                rowOdd: "odd",
                rowEven: "even",
                rowDummy: "k-row-dummy",
                rowReadOnly: "k-row-readonly",

                rowLocked: "k-row-locked",
                cellLocked: "k-cell-locked",
                gridLocked: "k-grid-locked",

                inputTextContainer: "k-input-text-container",
                inputSelection: "k-input-selection",
                inputAmountContainer: "k-input-amt-container",

                //editorAmountInvalid: "k-editor-amt-inv",

                radioContainer: "k-radio-container",

                formatText: "k-format-text",
                formatAmount: "k-format-amount"
            },
            Function: {
                cloneObject: function (obj) {
                    return $.extend(true, {}, obj);
                },
                convertToAmount: function (val) {
                    var parts = val.toString().split(".");
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    return parts.join(".");
                },
                getAssocValue: function (aoItems, colFind, valFind, colTarget) {
                    for (var i = 0; i < aoItems.length; ++i) {
                        var obj = aoItems[i];
                        if (obj[colFind] == valFind)
                            return obj[colTarget];
                    }
                    return null;
                }
            }
        }
    });

    ////////////////////
    // Formatters
    ////////////////////
    function KRadioFormatter(pGridCell, pVal, pColDef, pData) {
        var id = pGridCell.parentRow.parentGrid.gridId;
        id = id.substring(1, id.length);
        pGridCell.$element.addClass(KGrid.CssClass.formatText);
        var html = "<div style='text-align: center'><input type='radio' name='kSelRad-" + id + "' class='kgrid-radio' /></div>";
        pGridCell.$element.append(html);
        return html;
    }
    function KAmountFormatter(precision) {
        if (precision == null || precision == undefined)
            precision = 0;
        return function (pGridCell, pValue, pGridColDef, pData) {
            if (String(pValue) == "")
                return "";
            var amt = parseFloat(pValue);
            if (!isNaN(amt)) {
                var parts = String(pValue).split(".");
                if (precision <= 0)
                    amt = KGrid.Function.convertToAmount(Math.round(pValue));
                else
                    amt = KGrid.Function.convertToAmount(amt.toFixed(precision));
            }
            pGridCell.$element.addClass(KGrid.CssClass.formatAmount);
            var $input = $("<input type='text' readonly></input>");
            $input.css({ 'text-align': 'right', 'padding-right': '10px', 'border': '0px', 'background': 'transparent' });
            $input.val(amt);
            //var $div = $("<div>" + amt + "</div>").css({ 'text-align': 'right', 'padding-right': '10px' });
            var $div = $("<div></div>").append($input);
            var width = pGridCell.$element.css("min-width").replace('px', '');
            if (width <= 0)
                width = pGridCell.$element.css("width").replace('px', '');
            if (width <= 0)
                width = pGridCell.$element.css("max-width").replace('px', '');
            $div.css({ 'width': width - 1 });
            $input.css({ 'width': $div.width() - 1 });
            //var $div = $("<div>" + amt + </div>");
            pGridCell.$element.append($div);
            return pGridCell.$element.html();
        }
    }
    function KTextFormatter(pGridCell, pValue, pGridColDef, pData) {
        var val = (pValue == null ? "" : pValue);
        var $input = $("<input type='text' readonly></input>");
        $input.css({ 'padding-left': '4px', 'padding-right': '4px', 'border': '0px', 'background': 'transparent' });
        var align = 'left';
        if (pGridColDef.align == 'center') {
            align = 'center';
        } else if (pGridColDef.align == 'right') {
            align = 'right';
        }
        $input.css({ 'text-align': align });
        $input.val(val);
        var $div = $("<div></div>").append($input);
        var width = pGridCell.$element.css("min-width").replace('px', '');
        if (width <= 0)
            width = pGridCell.$element.css("width").replace('px', '');
        if (width <= 0)
            width = pGridCell.$element.css("max-width").replace('px', '');
        $div.css({ 'width': width - 1 });
        $input.css({ 'width': width - 1 });
        //var $div = $("<div>" + val + "</div>").css({ 'padding-left': '4px', 'padding-right': '4px' });
        pGridCell.$element.append($div);
        return pGridCell.$element.html();
    }
    function KComboFormatter(options) {
        var self = this;
        this.config = {
            valueField: "Value",
            textField: "Text",
            onTextRequested: null
        };
        $.each(options, function (keyIn, valueIn) {
            $.each(self.config, function (key, value) {
                if (keyIn.toLowerCase() == key.toLowerCase()) {
                    self.config[key] = valueIn;
                    return false;
                }
            });
        });

        return function (pGridCell, pValue, pGridColDef, pData) {
            var displayText = "";
            if (String(pValue) != "") {
                // Search Text from Cell's cache first, if not found, then invoke handler function
                var cache = null;
                var gridCache = pGridCell.getGrid().cacheSelection;
                var cacheName = String(pGridCell.colDef.name);
                if (gridCache != null && gridCache != undefined) {
                    var selectionCache = gridCache[cacheName];
                    if (selectionCache != null && selectionCache != undefined)
                        cache = selectionCache;
                }
                if (cache != null)
                    displayText = KGrid.Function.getAssocValue(cache, self.config.valueField, pValue, self.config.textField);
                else {
                    if (self.config.onTextRequested != null)
                        displayText = self.config.onTextRequested(pGridCell, pValue);
                }
            }
            var $div = $("<div>" + displayText + "</div>").css({ 'padding-left': '4px', 'padding-right': '4px' });
            pGridCell.$element.append($div);
            return pGridCell.$element.html();
        }
    }

    ////////////////////
    // Editors
    ////////////////////
    function KTextEditor(pGridCell, pValue, pData) {
        this.$element = null;
        this.$input = null;
        this.oldValue = pValue;
        var self = this;

        this.setValue = function (val) {
            self.$input.val(val);
        }
        this.getValue = function () {
            return self.$input.val();
        }
        this.reset = function () {
            self.$input.val(self.oldValue);
        }
        this.destroy = function () {
            self.$element.remove();
        }
        this.focus = function () {
            self.$input.focus();
            var len = self.$input.val().length;
            self.$input[0].setSelectionRange(len, len);
        }
        this.isChanged = function () {
            return (String(self.oldValue) != String(self.getValue()));
        }
        this.invalid = function () {
            self.focus();
            self.$input.animate({ "background-color": "#ff8080" }, 200);
        }
        this.init = function () {
            self.$element = $("<div><input type='text' id='kInputText'></div>").addClass(KGrid.CssClass.inputTextContainer);
            self.$input = self.$element.find("input").val(pValue);

            var blurHandler = function (e) {
                //var args = { "element": self.$input, "value": self.$input.val() };
                pGridCell.callbackCommit(e, self);
            };
            var keypressHandler = function (e) {
                if (e.which == 13 || e.which == 9) { // ENTER or TAB
                    e.preventDefault();
                    self.$input.trigger("blur");
                    pGridCell.parentRow.triggerNextEditableCell(pGridCell);
                }
            };
            self.$input.on("blur.kBlurHandler", blurHandler);
            self.$input.on("keydown.kKeyPressHandler", keypressHandler);
            //self.$input.on("blur.kBlurHandler", blurHandler).on("click.kClickHandler", clickHandler);
            pGridCell.$element.append(self.$element);
            self.$input.focus();
        }
    }
    //function ComboEditor(optionItems, changeHandler) {
    function ComboEditor(options) {
        return function (pGridCell, pValue, pData) {
            var self = this;
            this.config = {
                create: false,
                loadOnDemand: false,
                options: [],
                valueField: "Value",
                textField: "Text",
                onLoad: null, // function (pGridCell, pFuncCallback)
                onTextRequested: null, // function (pGridCell, valGet)
                //onGetInitOptions: null,     // functoin (pGridCell, pEditor, pCallback)
                onChanged: null         // function (pGridCell, option, rowData);
            }
            // Load options
            $.each(options, function (keyIn, valueIn) {
                $.each(self.config, function (key, value) {
                    if (keyIn.toLowerCase() == key.toLowerCase()) {
                        self.config[key] = valueIn;
                        return false;
                    }
                });
            });

            this.$element = null;
            this.selObj = null;
            //this.oldValue = pValue == "" ? "" : parseInt(String(pValue), 10);
            this.oldValue = pValue;
            this.oldText = "";

            this.getTextByValue = function (val) {
                if (self.config.onTextRequested != null && self.oldValue !== "") {
                    return self.config.onTextRequested(pGridCell, val);
                }
                return "";
            }
            this.getCache = function () {
                var gridCache = pGridCell.getGrid().cacheSelection;
                var cacheName = String(pGridCell.colDef.name);
                if (gridCache != null && gridCache != undefined) {
                    var selectionCache = gridCache[cacheName];
                    if (selectionCache != null && selectionCache != undefined)
                        return $.extend(true, [], selectionCache); // return cloned array
                }
                return null;
            }
            this.setCache = function (options) {
                var gridCache = pGridCell.getGrid().cacheSelection;
                var cacheName = String(pGridCell.colDef.name);
                if (gridCache == undefined || gridCache == null)
                    pGridCell.getGrid().cacheSelection = {};
                gridCache = pGridCell.getGrid().cacheSelection;
                gridCache[cacheName] = $.extend(true, [], options); // copy array
            }

            this.setValue = function (val) {
                // Create an option item if it does not exist
                var cache = self.getCache();
                var options = [];

                self.selObj.clear();
                self.selObj.clearOptions();

                if (val !== "") {
                    var o = {};
                    if (!isNaN(parseInt(val, 10)))
                        val = parseInt(val, 10);
                    //val = parseInt(val, 10);
                    o[self.config.valueField] = val;
                    if (self.config.onGetInitTextByValue != null)
                        o[self.config.textField] = self.config.onGetInitTextByValue(pGridCell, val);
                    else
                        o[self.config.textField] = "<null>";
                    self.selObj.addOption(o);
                }
                var fExists = false;
                if (options != null) {
                    options = $.extend(true, [], cache); // Clone cache
                    for (var i = 0; i < cache.length; ++i) {
                        if (String(cache[i][self.config.valueField]) == String(val)) {
                            cache.slice(i, 1); // Remove the existing from its location.
                            fExists = true;
                            break;
                        }
                    }
                }
                for (var i = 0; i < options.length; ++i)
                    self.selObj.addOption(options[i]);

                if (String(val) != "") // select the initial option
                    self.selObj.setValue(val);
            }
            this.getValue = function () {
                var val = self.selObj.getValue();
                // If val is string, it means that the val is newly created. So set the val to -1;
                if (parseInt(val, 10) == NaN)
                    val = -1;
                    // If this is a string number, convert it to number
                else if (String(parseInt(val, 10)) == val)
                    return parseInt(val, 10);
                return val;
            }
            this.getText = function () {
                var selectedVal = self.selObj.getValue();
                if (selectedVal == "")
                    return "<unassiged>";
                var option = self.selObj.getOption(selectedVal);
                return option[0].innerText;
            }
            this.destroy = function () {
                // Due to selectize internal events, we cannot remove it right away
                // or the selectize will throw errors.
                self.$element.hide();
                setTimeout(function () {
                    self.selObj.destroy();
                    self.$element.remove();
                }, 200);
            }
            this.focus = function () {
                self.selObj.focus();
            }
            this.isChanged = function () {
                var val = self.getValue();
                return (String(self.oldValue) != String(val));
            }
            this.invalid = function () {
                self.selObj.focus();
                self.$element.animate({ "background-color": "#ff8080" }, 200);
            }
            this.funcOnChange = function (val) {
                var oldVal = self.oldValue;
                if (val != self.oldValue) {
                    if (self.config.onChanged != null) {
                        var ret = {};
                        ret[self.config.valueField] = self.getValue();
                        ret[self.config.textField] = self.getText();
                        self.config.onChanged(pGridCell, ret, pData);
                    }
                }
                //if (self.getValue() !== "")
                //    pGridCell.callbackCommit(null, self);
            }
            this.funcOnDropdownClose = function (e) {
                // Delay the call to callbackCommit until all events from selectize have been fired completely.
                // Otherwise there will be error thrown from selectize.
                //setTimeout(function () { pGridCell.callbackCommit(null, self); }, 100);


                //if (self.selObj.getValue() === "")
                //    self.selObj.setValue(self.oldValue);
                //pGridCell.callbackCommit(null, self);

                //var val = self.selObj.getValue();
                //if (String(val) == String(self.oldValue) && pGridCell.parentRow.rowState != KGrid.ItemState.blankItem)
                //    pGridCell.callbackCancel(self);
                setTimeout(function () {
                    pGridCell.callbackCommit(null, self);
                }, 200);
            }
            this.funcOnBlur = function (e) {
                pGridCell.callbackCommit(e, self);
            }
            this.funcOnLoadFinished = function (data) {
                if (String(self.oldValue) != "") {
                    self.selObj.addOption(self.oldOption);
                    self.selObj.setValue(self.oldValue);
                }
            }
            this.render = function (items) {
                var cache = items;
                if (self.getCache() == null) { // This means the function is called after loadOnDemand
                    pGridCell.$element.empty();
                    pGridCell.$element.append(self.$element);
                    if (cache.length > 0)
                        self.setCache(cache);
                }
                $select.selectize({
                    create: self.config.create,
                    valueField: self.config.valueField,
                    labelField: self.config.textField,
                    searchField: [self.config.textField],
                    sortField: self.config.valueField,
                    openOnFocus: true,
                    dropdownParent: "body",
                    options: cache,
                    onChange: self.funcOnChange,
                    onDropdownClose: self.funcOnDropdownClose,
                    onOptionAdd: function (val, data) {
                        var cache = self.getCache();
                        if (cache == null) {
                            cache = [];
                            self.setCache([]);
                            cache = self.getCache();
                        }
                        //var item = {};
                        //item[self.config.valueField] = -1;
                        //item[self.config.textField] = val;
                        //cache.push(item);
                        cache.push(data);
                        self.setCache(cache);
                        //self.selObj.setValue(val);
                        //return data;
                    }
                });
                self.selObj = $select[0].selectize;
                if (self.oldValue !== "") {
                    self.selObj.addOption(self.oldOption);
                    self.selObj.setValue(self.oldValue);
                }
                self.selObj.focus();
            }
            this.init = function () {
                $divSelect = $("<div style='height: 100%; width: 100%;'><select placeholder='-- Select --' /></div>");
                $divSelect.addClass(KGrid.CssClass.inputSelection);
                self.$element = $divSelect;
                $select = self.$element.find("select");
                pGridCell.$element.append(self.$element);
                self.oldText = self.getTextByValue(self.oldValue);
                self.oldOption = {};
                self.oldOption[self.config.valueField] = self.oldValue;
                self.oldOption[self.config.textField] = self.oldText;

                var cache = self.getCache();
                if (cache == null) {
                    if (self.config.loadOnDemand && self.config.onLoad != null) {
                        var $tmpElement = self.$element;
                        pGridCell.$element.empty();
                        pGridCell.$element.append("<div>Loading ...</div>");
                        self.config.onLoad(pGridCell, self.render); // Defer loading to the callback, the target func must call render(data).
                    } else if (self.config.options.length > 0) {
                        self.render(self.config.options);
                    }
                    else {
                        self.render([]);
                    }
                }
                else {
                    self.render(cache);
                }
            }
        }
    }
    function KAmountEditor(precision) {
        return function (pGridCell, pValue, pData) {
            this.$element = $("<div><input type='text' id='kInputAmount'></div>").addClass(KGrid.CssClass.inputAmountContainer);
            this.$input = this.$element.find("input").val(pValue);
            this.precision = ((precision != null && precision != undefined) ? precision : 0);
            this.valid = true;
            //this.value = (this.$element.val() == "" ? 0 : this.$element.val());
            this.value = pValue;
            this.oldValue = pValue;
            this.displayText = this.data;
            var self = this;

            this.$input.on("blur.amountInputAutoFormat", function () {
                self.setValue($(this).val());
            });
            this.$input.on("focus.amountInputFocus", function () {
                $(this).one('mouseup', function () {
                    $(this).select();
                    return false;
                }).select();
            });
            this.$input.on("keydown.amountInputKeydown", function (e) {
                if (e.which == 13 || e.which == 9) { // ENTER or TAB
                    e.preventDefault();
                    self.$input.trigger("blur");
                    pGridCell.parentRow.triggerNextEditableCell(pGridCell);
                    return;
                }

                //// Allow: backspace, delete, tab, escape, enter and .
                //if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                //    // Allow: Ctrl+A
                //    (e.keyCode == 65 && e.ctrlKey === true) ||
                //    // Allow: home, end, left, right
                //    (e.keyCode >= 35 && e.keyCode <= 39)) {
                //    // let it happen, don't do anything
                //    return;
                //}
                //// Ensure that it is a number and stop the keypress
                //if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                //    e.preventDefault();
                //}
            });
            this.$input.keypress(function (event) {
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            this.formatOnZero = function () {
                if (self.value == 0) {
                    var val = "0";
                    if (self.precision > 0) {
                        val += ".";
                        for (var i = 0; i < self.precision; ++i)
                            val += "0";
                    }
                    self.$input.val(val);
                }
            };
            this.validate = function (val) {
                if (val == null || val == undefined)
                    return false;
                // with commas:     ^-?(([1-9][0-9]{0,2}(,[0-9]{3})*)|0)(\.[0-9]{1,4})?$
                // without commas:  ^-?(([1-9][0-9]*)|0)(\.[0-9]{1,4})?$
                // ** upto 8 decimal digits
                var regEx = /^-?((([1-9][0-9]{0,2}(,[0-9]{3})*)|([1-9][0-9]*))|0)(\.[0-9]{1,8})?$/;
                return regEx.test(val);
            }
            this.setValue = function (v) {
                if (String(v) == "" && self.oldValue == "") {
                    self.valid = true;
                    self.value = 0;
                    self.displayText = self.value;
                    return true;
                }
                var val = parseFloat(v);
                if (isNaN(val) || !self.validate(val)) {
                    self.valid = false;
                    self.displayText = val;
                    return false;
                }
                else {
                    self.valid = true;
                    self.value = parseFloat(String(val).replace(',', ''));
                    // Check if we have to round the precision up to 2 digits.
                    var parts = String(self.value).split(".");
                    if (self.precision <= 0)
                        self.value = Math.round(self.value);
                    else {
                        if (parts.length > 1 && parts[1].length > self.precision)
                            self.value = self.value.toFixed(self.precision);
                    }
                    self.displayText = KGrid.Function.convertToAmount(self.value);
                    self.$input.val(self.displayText);

                    self.formatOnZero();
                    return true;
                }
            };
            this.getValue = function () {
                return self.value;
            };
            this.destroy = function () {
                self.$element.remove();
            };
            this.focus = function () {
                self.$input.focus();
            };
            this.isChanged = function () {
                return (String(self.oldValue) != String(self.value));
            };
            this.invalid = function () {
                self.$input.focus();
                self.$input.animate({ "background-color": "#ff8080" }, 200);
            };
            this.init = function () {
                var blurHandler = function (e) {
                    pGridCell.callbackCommit(e, self);
                }
                var keyDownHandler = function (e) {
                    // backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && e.ctrlKey === true) || // Ctrl + A
                        // home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                }
                self.$input.on("blur.kBlurHandler", blurHandler);
                //self.$input.on("keydown.kKeyDownHandler", keyDownHandler);

                pGridCell.$element.append(self.$element);
                self.$input.focus();
            }
        }
    }

    ////////////////////
    // Validators
    ////////////////////
    function KRequireValidator(val) {
        return !(String(val) == "" || val == undefined || val == null);
    }

    ////////////////////
    // KilinSoft Grid
    ////////////////////
    var g_k_nClick = 0, g_k_clickTimer = null;
    var KGridColumnDef = {
        name: "",
        title: "",
        editable: true,
        locked: false,
        formatter: null,
        editor: null,
        validator: null,
        required: false,
        align: 'left',
        width: 0,
        height: 0,
        actualWidth: -1,
        actualHeight: -1,
        css: '',
        isStretchCol: false,
        onCreated: null,
        onChanged: null,
        onFocused: null,
        onMouseEnter: null,
        onMouseLeave: null
    }
    var KGridDef = {
        editable: false,
        enableAddBlankRow: false,
        ignoreAutoEditorClose: false,
        ignoreAutoCloseEditorTriggerList: [],
        displayHeader: true,
        displayPagination: false,
        numPageDisplayed: 10,
        rowPerPage: 20,
        paginationHeight: 25,

        rowHeight: 25,
        headerHeight: 25,
        height: 0,
        width: 0,
        widthExcludingScrollbar: 0,
        onRowCreated: null,
        onRowDeleted: null,
        onRowSelected: null,
        onRowDeselected: null,
        onRowChanged: null,
        onRowMouseEnter: null,
        onRowMouseLeave: null,
        onRowDblClick: null,
        onBlankRowAdded: null,
        onDataRowAdded: null,

        onPageSelected: null
    }
    function KGridCell(pRowIndex, pColIndex, pValue, pColDef, pParentRow) {
        this.colIndex = pColIndex;
        this.rowIndex = pRowIndex;
        this.value = pValue;
        this.prevValue = pValue;
        this.originalValue = pValue;
        this.valid = true;
        this.$element = $("<td></td>").addClass(KGrid.CssClass.cell);
        this.displayHtml = "";
        this.locked = false;
        this.editable = pColDef.editable;
        this.colDef = pColDef;
        this.parentRow = pParentRow;
        this.editor = null;

        var self = this;

        this.notifyChange = function () {
            if (self.colDef.onChanged != null) // Cell Change
                self.colDef.onChanged(self, self.value);
            self.parentRow.callbackCellChanged(self);
        };
        this.notifyCreate = function () {
            if (self.colDef.onCreated != null)
                self.colDef.onCreated(self);
        };
        this.notifyFocus = function (pEditor) {
            if (self.colDef.onFocused != null)
                self.colDef.onFocused(self, pEditor);
        };
        this.notifyMouseEnter = function () {
            if (self.colDef.onMouseEnter != null)
                self.colDef.onMouseEnter(self)
        }
        this.notifyMouseLeave = function () {
            if (self.colDef.onMouseLeave != null)
                self.colDef.onMouseLeave(self)
        }
        this.lock = function () { // cell
            if (self.isEditing())
                return false;
            self.locked = true;
            self.$element.addClass(KGrid.CssClass.cellLocked);
            return true;
        };
        this.unlock = function () {
            self.locked = false;
            self.$element.removeClass(KGrid.CssClass.cellLocked);
        }
        this.setReadOnly = function (fReadOnly) {
            if (!fReadOnly && self.colDef.editable)
                self.editable = true;
            else
                self.editable = false;
            //if (!self.editable)
            //    self.$element.addClass(KGrid.CssClass.cellReadOnly);
            //else
            //    self.$element.removeClass(KGrid.CssClass.cellReadOnly);
        }
        this.isEditing = function () { // Cell
            return (self.editor != null);
        }
        this.registerEditor = function (pEditor) {
            self.editor = pEditor;
            self.parentRow.editingCell = self;
            self.getGrid().lastEditableCell = this;
        };
        this.unregisterEditor = function () {
            self.editor = null;
            self.parentRow.editingCell = null;
        };
        this.openEditor = function () {
            if (self.editable && !self.locked &&
                (self.parentRow.rowState != KGrid.ItemState.deletedItem)) {

                self.displayHtml = self.$element.html();
                self.$element.empty();
                self.$element.off("click.KGridCellClickHandler");
                var editor = new self.colDef.editor(self, self.value, self.parentRow.data);
                editor.init();
                self.notifyFocus(editor);

                // Store activeRow and activeEditor
                self.getGrid().rowInProgress = self.parentRow;
                self.registerEditor(editor);
            }
        };
        this.render = function () {
            self.$element.empty();
            //this.$element.append(self.displayHtml);
            self.$element.on("click.KGridCellClickHandler", self.funcOnClick);
            self.$element.on("dblclick.KGridCellDblClickHandler", self.funcOnDblClick);
            self.$element.on("mouseenter.KGridCellEnterHandler", self.notifyMouseEnter);
            self.$element.on("mouseleave.KGridCellLeaveHandler", self.notifyMouseLeave);
            self.colDef.formatter(self, self.value, self.colDef, self.parentRow.data);
        };
        this.isChanged = function () {
            return (String(self.originalValue) != String(self.value));
        }
        this.validate = function (val) {
            if (self.colDef.required && !KGrid.Validator.Require(val))
                return false;
            if ((String(val) != "") && (val != undefined) && (val != null) &&
                (self.colDef.validator != null))
                return self.colDef.validator(val);
            return true;
        };
        this.setValue = function (pVal) {
            self.valid = self.validate(pVal);
            self.prevValue = self.value;
            self.value = pVal;
            self.parentRow.data[self.colDef.name] = self.value;

            if (!self.parentRow.isChanged()) { // Check if all cells' values of the row have been changed back to their original values.
                self.parentRow.setState(self.parentRow.prevRowState);
                //self.parentRow.setState(self.parentRow.originalRowState);
            }
            else if (self.parentRow.rowState == KGrid.ItemState.blankItem) {
                self.parentRow.setState(KGrid.ItemState.newItem);
                // Add new blank row if needed
                if (self.getGrid().gridDef.enableAddBlankRow) {
                    // Only add a blank row if the current row is exactly the last one
                    if (self.parentRow.rowIndex + 1 >= self.getGrid().rows.length) {
                        var blankRow = self.getGrid().addRow(null);
                        var objDiv = blankRow.$element[0];
                        objDiv.scrollTop = objDiv.scrollHeight;
                    }
                }
            }
            else if (self.parentRow.rowState == KGrid.ItemState.originalItem)
                self.parentRow.setState(KGrid.ItemState.modifiedItem);

            self.render(); // re-render every time
        };
        this.getGrid = function () {
            return self.parentRow.parentGrid;
        }
        this.select = function () {
            self.parentRow.callbackCellSelect(self);
        },
        this.funcOnClick = function (e) {
            self.select();
            //g_k_nClick++;
            //if (g_k_nClick === 1) {
            //    g_k_clickTimer = setTimeout(function () {
            //        g_k_nClick = 0;
            //        self.select();
            //    }, 250);
            //} else {
            //    clearTimeout(g_k_clickTimer);
            //    g_k_nClick = 0;
            //    self.parentRow.notifyDblClick();
            //}
        };
        this.funcOnDblClick = function (e) {
            //e.preventDefault(); // Use DblClick handler from the special handler in the single click event handler instead
            self.parentRow.notifyDblClick();
        };
        this.callbackCommit = function (e, editor) {
            // 1. If ignoreAutoEditorClose is active, then do not call commitChange() yet.
            var grid = self.getGrid();
            var gridDef = grid.gridDef;
            var fIgnoreAutoEditorClose = gridDef.ignoreAutoEditorClose;
            if (fIgnoreAutoEditorClose) {
                //if (grid.isPreventEditorClose) {
                    if (grid.closeEditorTimer != null)
                        clearTimeout(grid.closeEditorTimer);
                    grid.closeEditorTimer = setTimeout(
                        function () {
                            if (grid.isPreventEditorClose === true) {
                                grid.isPreventEditorClose = false;
                            } else {
                                self.commitChange();
                            }
                        }, 300);
                    return; // prevent the calling of commitchange section below
                //}
            }

            if (!self.commitChange()) {
                if (e == null)
                    return;
                e.preventDefault;
                e.stopPropagation();
            }
        };
        this.callbackCancel = function (editor) {
            self.render();
            self.unregisterEditor();
            self.getGrid().rowInProgress = null;
            editor.destroy();
        };
        this.commitChange = function () {
            if (self.locked)
                return false;

            var editor = self.editor;
            if (editor == null)
                return false;

            if (!editor.isChanged()) {
                self.render();
                self.unregisterEditor();
                self.getGrid().rowInProgress = null;
                editor.destroy();
                return true;
            }

            var val = editor.getValue();
            if (self.validate(val) || self.originalValue == val) {
                self.getGrid().rowInProgress = null;
                self.unregisterEditor();
                editor.destroy();
                self.setValue(val);

                // check if this cell belong to a blank row and the row's required cells are not completed yet.
                if (self.parentRow.itemState == KGrid.ItemState.newItem) {
                    for (var i = 0; i < self.parentRow.cells.length; ++i) {
                        var cell = self.parentRow.cells[i];
                        if (!cell.validate(cell.value)) {
                            self.parentRow.valid = false;
                            break;
                        }
                    }
                }
                if (self.prevValue != val)
                    self.notifyChange();
            }
            else {
                editor.invalid();
                return false;
            }
            return true;
        };

        if (pColIndex == self.getGrid().colDefs.length - 1) { // check if this is the last cell of the row
            self.$element.addClass(KGrid.CssClass.cellLast);

            // add additional user-defined classes
            if (self.colDef.css != '') {
                var cssClasses = self.colDef.css.split(",");
                for (var i = 0; i < cssClasses.length; ++i) {
                    self.$element.addClass(cssClasses[i]);
                }
            }
        }
        if (self.colDef.isStretchCol)
            self.$element.css({ "height": self.colDef.actualHeight, "width": self.colDef.actualWidth });
        else
            self.$element.css({ "height": self.colDef.actualHeight, "min-width": self.colDef.actualWidth });

        self.setReadOnly(!self.editable);

        //self.setValue(pValue, false); // set value but do not notify any changes yet
        self.render();
        self.notifyCreate();
    }

    function KGridRow(pRowIndex, pData, pParentGrid) {
        this.rowIndex = pRowIndex;
        this.$element = $("<tr></tr>").addClass(KGrid.CssClass.row);
        this.parentGrid = pParentGrid;
        this.data = pData;
        this.locked = false;
        this.readOnly = false;
        this.rowState = KGrid.ItemState.blankItem;
        this.prevRowState = KGrid.ItemState.blankItem;
        this.originalRowState = KGrid.ItemState.blankItem;

        this.cells = [];

        var self = this;

        this.notifyRowCreated = function () {
            var gridDef = self.parentGrid.gridDef;
            if (gridDef.onRowCreated != null)
                gridDef.onRowCreated(self);
        }
        this.notifyRowSelected = function () {
            var gridDef = self.parentGrid.gridDef;
            if (gridDef.onRowSelected != null)
                gridDef.onRowSelected(self);
        };
        this.notifyRowDeselected = function () {
            var gridDef = self.parentGrid.gridDef;
            if (gridDef.onRowDeselected != null)
                gridDef.onRowDeselected(self);
        };
        this.notifyRowChanged = function (pCell) {
            var gridDef = self.parentGrid.gridDef;
            if (gridDef.onRowChanged != null)
                gridDef.onRowChanged(self, pCell);
        }
        this.notifyRowMouseEnter = function () {
            // Do Not Notify if the Row is Blank
            if (self.rowState == KGrid.ItemState.blankItem)
                return;
            var gridDef = self.parentGrid.gridDef;
            if (gridDef.onRowMouseEnter != null)
                gridDef.onRowMouseEnter(self);
        }
        this.notifyRowMouseLeave = function () {
            var gridDef = self.parentGrid.gridDef;
            if (gridDef.onRowMouseLeave != null)
                gridDef.onRowMouseLeave(self);
        }
        this.notifyDblClick = function () {
            var gridDef = self.parentGrid.gridDef;
            if (gridDef.onRowDblClick != null)
                gridDef.onRowDblClick(self);
        }
        this.callbackCellChanged = function (pCell) {
            self.notifyRowChanged(pCell);
        }
        this.isChanged = function () {
            for (var i = 0; i < self.cells.length; ++i)
                if (self.cells[i].isChanged())
                    return true;
            return false;
        };
        this.validate = function () {
            var fValid = false;
            for (var i = 0; i < self.cells.length; ++i)
                if (!self.cells[i].validate(self.cells[i].value))
                    return false;
            return true;
        };
        this.lock = function () { // Row
            if (self.isEditing())
                return false;
            for (var i = 0; i < self.cells.length; ++i)
                self.cells[i].lock();
            self.locked = true;
            return true;
        };
        this.unlock = function () {
            for (var i = 0; i < self.cells.length; ++i)
                self.cells[i].unlock();
            self.locked = false;
        };
        this.setReadOnly = function (fReadOnly) {
            for (var i = 0; i < self.cells.length; ++i)
                self.cells[i].setReadOnly(fReadOnly);
            self.readOnly = fReadOnly
            if (fReadOnly)
                self.$element.addClass(KGrid.CssClass.rowReadOnly);
            else
                self.$element.removeClass(KGrid.CssClass.rowReadOnly);
        };
        this.isEditing = function () {
            return (self.parentGrid.rowInProgress == self);
        };
        this.setData = function (pData, itemState) {
            self.data = pData;
            if (itemState == undefined || itemState == null) {
                if (self.rowState != KGrid.ItemState.originalItem)
                    self.setState(KGrid.ItemState.newItem);
                else
                    self.setState(KGrid.ItemState.modifiedItem);
            } else {
                self.setState(itemState);
            }

            var colDefs = self.parentGrid.colDefs;
            for (var i = 0; i < colDefs.length; ++i) {
                var colDef = colDefs[i];
                var val;
                val = (self.data == null) ? "" : self.data[colDef["name"]];
                if (val == undefined)
                    val = "";
                var cell = self.getCellByColumnName(colDef["name"]);
                if (cell != null && cell != undefined)
                    cell.setValue(val);
            }
            // Check if we need to add a blank row
            var grid = self.parentGrid;
            nRows = grid.rows.length;
            if (nRows > 0 && (grid.rows[nRows - 1].rowState != KGrid.ItemState.blankItem) && grid.gridDef.enableAddBlankRow)
                grid.addRow(null);
        }
        this.setState = function (pState) {
            self.prevRowState = self.rowState;
            self.rowState = pState;
            if (self.data != null)
                self.data.itemState = pState;

            // Apply CSS style for each rowState
            self.$element.removeClass(KGrid.CssClass.rowNew);
            self.$element.removeClass(KGrid.CssClass.rowModified);
            self.$element.removeClass(KGrid.CssClass.rowDeleted);
            var css;
            switch (self.rowState) {
                case KGrid.ItemState.newItem:
                    css = KGrid.CssClass.rowNew;
                    break;
                case KGrid.ItemState.modifiedItem:
                    css = KGrid.CssClass.rowModified;
                    break;
                case KGrid.ItemState.deletedItem:
                    css = KGrid.CssClass.rowDeleted;
                    break;
                default:
                    break;
            }
            self.$element.addClass(css);
        };
        this.getCellByColumnName = function (colName) {
            for (var i = 0; i < self.cells.length; ++i) {
                var col = self.cells[i];
                if (col.colDef.name == colName)
                    return col;
            }
            return null;
        };
        this.delete = function () {
            self.parentGrid.deleteRowByIndex(self.rowIndex);
        }
        this.undoDelete = function () {
            self.parentGrid.undoDeleteRowByIndex(self.rowIndex);
        }
        this.revertState = function () {
            self.setState(self.prevRowState);
            self.prevRowState = self.rowState;
        };
        this.select = function (pTriggeredCell, pNotify) {
            if (pTriggeredCell == undefined || pTriggeredCell == null)
                pTriggeredCell = null;
            if (pNotify == undefined || pNotify == null)
                pNotify = true;

            self.$element.addClass(KGrid.CssClass.rowSelected);

            if (pTriggeredCell != null)
                pTriggeredCell.openEditor();
            else
                self.triggerNextRequiredCell(false);

            // deselect previous row
            var selectedRow = self.parentGrid.selectedRow;
            if (selectedRow != null && selectedRow != self) {
                selectedRow.deselect();
            }

            self.parentGrid.selectedRow = self;

            if (pNotify)
                self.notifyRowSelected();
        };
        this.deselect = function (pNotify) {
            if (pNotify == undefined || pNotify == null)
                pNotify = true;
            self.$element.removeClass(KGrid.CssClass.rowSelected);
            if (self.parentGrid.selectedRow == self)
                self.parentGrid.selectedRow = null;
            self.notifyRowDeselected(self);
        };
        this.triggerNextRequiredCell = function () {
            if (self.locked)
                return;
            var cellToTrigger = null;
            for (var i = 0; i < self.cells.length; ++i) {
                var cell = self.cells[i];
                var colDef = cell.colDef;
                if (cell.editable && !cell.locked) {
                    if (cellToTrigger == null)
                        cellToTrigger = cell; // will trigger this cell first if there is no required cell.
                    if (colDef.required) {
                        var fValid = true;
                        if (colDef.validator != null)
                            fValid = colDef.validator(cell.value);
                        else
                            fValid = KGrid.Validator.Require(cell.value) // use default validator
                        if (!fValid) {
                            cellToTrigger = cell;
                            break;
                        }
                    }
                }
            }
            if (cellToTrigger != null)
                cellToTrigger.openEditor();
        }
        this.triggerNextEditableCell = function (pCell) {
            if (self.locked)
                return;
            var cellToTrigger = null;
            for (var i = pCell.colIndex + 1; i < self.cells.length; ++i) {
                var cell = self.cells[i];
                var colDef = cell.colDef;
                if (cell.editable && !cell.locked) {
                    cellToTrigger = cell; // will trigger this cell
                    break;
                }
            }
            if (cellToTrigger != null)
                cellToTrigger.openEditor();
        }
        this.callbackCellSelect = function (pCell) {
            if (self.locked)
                return;
            // return if selecting a blank row and enableAddBlankRow=false
            if (!self.parentGrid.gridDef.enableAddBlankRow) {
                if (self.rowState == KGrid.ItemState.blankItem)
                    return;
            }
            var fCanSelect = true;
            // Commit any editor in progress and check if its result is valid.
            var activeRow = self.parentGrid.rowInProgress;
            if (activeRow != null) {
                if (!activeRow.editingCell.commitChange())
                    fCanSelect = false;
                else if (!activeRow.validate()) {
                    fCanSelect = false;
                    activeRow.triggerNextRequiredCell();
                }
            }
            // Check if the selectedRow has been supplied with all required fields
            var selectedRow = pCell.parentRow.parentGrid.selectedRow;
            if (selectedRow != null && selectedRow.rowState != KGrid.ItemState.blankItem) {
                if (!selectedRow.validate()) {
                    // Allow selecting different cell on the same row
                    if (selectedRow != pCell.parentRow) {
                        fCanSelect = false;
                        selectedRow.triggerNextRequiredCell();
                    }
                }
            }
            // Allow to select only the first blank row before selecting other blank rows
            if (pCell.parentRow.rowState == KGrid.ItemState.blankItem) {
                var rows = self.parentGrid.rows;
                var blankRow = null;
                for (var i = 0; i < rows.length; ++i) {
                    if (rows[i].rowState == KGrid.ItemState.blankItem) {
                        if (rows[i].rowIndex != self.rowIndex) {
                            fCanSelect = false;
                            rows[i].triggerNextRequiredCell(true);
                        }
                        break;
                    }
                }
            }
            //// If the row has been selected already, do not select it again but open cell's editor instead.
            //if (self == selectedRow) {
            //    pCell.openEditor();
            //    fCanSelect = false;
            //}

            if (fCanSelect)
                self.select(pCell, (self != selectedRow)); // Notify if select a different row
        };

        if (self.rowIndex % 2 == 0)
            self.$element.addClass(KGrid.CssClass.rowOdd);

        var rowStateInit = (self.data == null) ? KGrid.ItemState.blankItem : self.data.itemState;
        self.setState(rowStateInit);
        self.prevRowState = rowStateInit;
        self.originalRowState = rowStateInit;

        var colDefs = self.parentGrid.colDefs;
        for (var i = 0; i < colDefs.length; ++i) {
            var colDef = colDefs[i];
            var val;
            val = (self.data == null) ? "" : self.data[colDef["name"]];
            if (val == undefined)
                val = "";
            var cell = new KGridCell(self.rowIndex, i, val, colDef, self);
            self.cells.push(cell);
            self.$element.append(cell.$element);
        }

        self.$element.on("mouseenter.KGridRowEnterHandler", self.notifyRowMouseEnter);
        self.$element.on("mouseleave.KGridRowLeaveHandler", self.notifyRowMouseLeave);

        self.notifyRowCreated();
    }

    function KilinSoftGrid(pGridId, pGridDef, pColDefs, pData) {
        var self = this;
        this.colDefs = [];
        this.gridDef = KGrid.Function.cloneObject(KGridDef);
        this.data = [{}];
        this.rows = [];
        this.nVirtualRows = 0;
        this.pageIndex = 0;
        this.selectedRow = null;
        this.rowInProgress = null;
        this.locked = false;
        this.lastEditableCell = null;

        this.isPreventEditorClose = false;
        this.closeEditorTimer = null;

        this.gridId = pGridId;
        // Load options
        $.each(pGridDef, function (keyIn, valueIn) {
            $.each(self.gridDef, function (key, value) {
                if (keyIn.toLowerCase() == key.toLowerCase()) {
                    self.gridDef[key] = valueIn;
                    return false;
                }
            });
        });
        // Load column definition
        for (var i = 0; i < pColDefs.length; ++i) {
            var colDef = KGrid.Function.cloneObject(KGridColumnDef);
            colDef.editable = this.gridDef.editable; // Load default editable value from GridDef before override it with colDef
            $.each(pColDefs[i], function (keyIn, valueIn) {
                $.each(colDef, function (key, value) {
                    if (keyIn.toLowerCase() == key.toLowerCase()) {
                        colDef[key] = valueIn;
                        return false;
                    }
                })
            });
            // Set Default formattor and editor if there are none.
            if (colDef.formatter == null)
                colDef.formatter = KGrid.Formatter.Text;
            if (colDef.editor == null)
                colDef.editor = KGrid.Editor.Text;
            self.colDefs.push(colDef);
        }

        this.$element = $("<div></div>").addClass(KGrid.CssClass.grid);
        $(this.gridId).append(this.$element);

        this.notifyBlankRowAdded = function (pGridRow) {
            if (self.gridDef.onBlankRowAdded != null)
                self.gridDef.onBlankRowAdded(pGridRow);
        };
        this.notifyDataRowAdded = function (pGridRow) {
            if (self.gridDef.onDataRowAdded != null)
                self.gridDef.onDataRowAdded(pGridRow);
        };
        this.notifyRowSelected = function (pGridRow) {
            if (self.gridDef.onRowSelected != null)
                self.gridDef.onRowSelected(pGridRow);
        };
        this.notifyRowDeselected = function (pGridRow) {
            if (self.gridDef.onRowDeselected != null)
                self.gridDef.onRowDeselected(pGridRow);
        };
        this.notifyRowCreated = function (pGridRow) {
            if (self.gridDef.onRowCreated != null)
                self.gridDef.onRowCreated(pGridRow);
        };
        this.lock = function () {
            if (self.isEditing())
                return false;
            for (var i = 0; i < self.rows.length; ++i)
                self.rows[i].lock();
            self.locked = true;
            self.$element.addClass(KGrid.CssClass.gridLocked);
            return true;
        }
        this.unlock = function () {
            for (var i = 0; i < self.rows.length; ++i)
                self.rows[i].unlock();
            self.locked = false;
            self.$element.removeClass(KGrid.CssClass.gridLocked);
        }
        this.clearSelection = function () {
            self.rowInProgress = null;
            if (self.selectedRow != null)
                self.selectedRow.deselect();
        }
        this.renderHeader = function () {
            var $headerDiv = $("<div></div>");
            var $header = $("<table><thead><tr></tr></thead></table>").addClass(KGrid.CssClass.gridHeader);
            var $rowHeader = $header.find("tr");
            $rowHeader.addClass(KGrid.CssClass.rowHeader);
            //self.$element.append($header);
            var colDefs = self.colDefs;
            for (var i = 0; i < colDefs.length; ++i) {
                var colDef = colDefs[i];
                var colTitle = "&nbsp;"
                if (colDef.title != "")
                    colTitle = colDef.title;
                var $cellHeader = $("<th></th>").append(colTitle).addClass(KGrid.CssClass.cellHeader);
                if (i == colDefs.length - 1)
                    $cellHeader.addClass(KGrid.CssClass.cellLast);
                $cellHeader.css({ "height": colDef.headerActualHeight, "min-width": colDef.actualWidth });
                $rowHeader.append($cellHeader);
            }
            $headerDiv.addClass(KGrid.CssClass.gridHeaderDiv).append($header).css({ "height": colDef.headerActualHeight });
            self.$element.append($headerDiv);
        };
        this.renderPagination = function () {
            var $pageDiv = $("<div></div>").addClass(KGrid.CssClass.gridPageArea);
            var $pageItemPrev = $("<div></div>").html("<<").addClass(KGrid.CssClass.gridPageItem);
            var $pageItemNext = $("<div></div>").html(">>").addClass(KGrid.CssClass.gridPageItem);
            $pageDiv.css({ "height": self.gridDef.paginationHeight, "line-height": self.gridDef.paginationHeight + "px", "width": self.gridDef.widthExcludingScrollbar });

            nAvailablePages = Math.ceil(self.nVirtualRows / self.gridDef.rowPerPage);
            //iPageStart = self.gridDef.rowPerPage * Math.floor(self.pageIndex / (self.gridDef.rowPerPage == 0 ? 1 : self.gridDef.rowPerPage));
            iPageStart = Math.floor(self.pageIndex / self.gridDef.numPageDisplayed) * self.gridDef.numPageDisplayed;

            // Check if we need to display page-jump anchors
            var fDisplayJumpBack = false;
            var fDisplayJumpNext = false;
            if (Math.floor(self.pageIndex / self.gridDef.numPageDisplayed) != 0)
                fDisplayJumpBack = true;
            if ((iPageStart / self.gridDef.numPageDisplayed) < (Math.ceil(nAvailablePages / self.gridDef.numPageDisplayed) - 1))
                fDisplayJumpNext = true;

            //if (self.pageIndex >= self.gridDef.numPageDisplayed) {
            //    $pageItemPrev.on('click', function () {
            //        var iNextPage = iPageStart - 1;
            //        self.pageIndex = iNextPage;
            //        if (self.gridDef.onPageSelected != null)
            //            self.gridDef.onPageSelected(self.pageIndex);
            //    });
            //    $pageDiv.append($pageItemPrev);
            //    $pageDiv.append($sep);
            //}
            if (fDisplayJumpBack) {
                $pageItemPrev.data("page-index", iPageStart - 1);
                $pageItemPrev.on('click', function () {
                    //var iNextPage = iPageStart - 1;
                    //self.pageIndex = iNextPage;
                    self.pageIndex = $pageItemPrev.data("page-index");
                    if (self.gridDef.onPageSelected != null)
                        self.gridDef.onPageSelected(self.pageIndex);
                });
                $pageDiv.append($pageItemPrev);
                //var $sep = $("<span>/</span>");
                //$pageDiv.append($sep);
            }
            for (var i = 0; i + iPageStart < nAvailablePages && i < self.gridDef.numPageDisplayed; ++i) {
                var $pageItem = $("<div></div>").html(iPageStart + i + 1).addClass(KGrid.CssClass.gridPageItem);
                if (self.pageIndex == iPageStart + i)
                    $pageItem.addClass(KGrid.CssClass.gridPageItemDisabled);
                else {
                    $pageItem.data("page-index", iPageStart + i);
                    $pageItem.on('click', function () {
                        //var iNextPage = iPageStart + i;
                        //self.pageIndex = iNextPage;
                        self.pageIndex = $(this).data("page-index");
                        if (self.gridDef.onPageSelected != null)
                            self.gridDef.onPageSelected(self.pageIndex);
                    });
                }
                $pageDiv.append($pageItem);
                if (i + iPageStart != nAvailablePages - 1 && i != self.gridDef.numPageDisplayed - 1) {
                    var $sep = $("<span>/</span>");
                    $pageDiv.append($sep);
                }
            }
            if (fDisplayJumpNext) { // display the next button to go display the link to next batch of pages
                $pageItemNext.data("page-index", iPageStart + self.gridDef.numPageDisplayed);
                $pageItemNext.on('click', function () {
                    //var iNextPage = iPageStart + self.gridDef.numPageDisplayed;
                    //self.pageIndex = iNextPage;
                    self.pageIndex = $pageItemNext.data("page-index");
                    if (self.gridDef.onPageSelected != null)
                        self.gridDef.onPageSelected(self.pageIndex);
                });
                //var $sep = $("<span>/</span>");
                //$pageDiv.append($sep);
                $pageDiv.append($pageItemNext);
            }
            self.$element.append($pageDiv);
        };
        this.renderGrid = function () {
            self.rows = [];
            self.clearSelection();

            self.updateDimension();

            var gridBodyWidth = self.gridDef.widthExcludingScrollbar;
            var gridBodyHeight = self.gridDef.height;

            var gridBodyDivWidth = self.gridDef.width;
            var gridBodyDivHeight = self.gridDef.height;

            this.$element.empty(); // clear all child elements

            var $bodyDiv = $("<div></div>");
            var $body = $("<table><tbody></tbody></table>").addClass(KGrid.CssClass.gridBody);
            if (self.gridDef.displayHeader) {
                self.renderHeader();
                gridBodyHeight -= self.gridDef.headerHeight + 1 /*1px for bottom-border*/;
                $body.css({ "margin-top": self.colDefs[0].headerActualHeight });
            }
            $bodyDiv.addClass(KGrid.CssClass.gridBodyDiv).append($body);
            $bodyDiv.css({ "width": gridBodyDivWidth - 1 /*To shift the scrollbar left 1px so it does not block the view of the grid's right border*/, "height": gridBodyDivHeight });
            self.$element.append($bodyDiv);
            if (self.gridDef.displayPagination) {
                self.renderPagination();
                gridBodyHeight -= self.gridDef.paginationHeight + 1 /*1px for bottom-border*/;
                $body.css({ "margin-bottom": self.colDefs[0].paginationHeight + 1 });
            }
            $body.css({ "width": gridBodyWidth });


            // Calculate how many dummy row we have to add to fill the grid's height
            var nVisualDisplayRow = Math.floor(gridBodyHeight / (self.gridDef.rowHeight + 1));
            var nActualDisplayRow = (nVisualDisplayRow > self.data.length) ? nVisualDisplayRow : self.data.length; // See if we have to fill the grid with empty rows.
            if (self.gridDef.displayPagination) {
                if (self.gridDef.rowPerPage < nActualDisplayRow)
                    nActualDisplayRow = self.gridDef.rowPerPage;
            }

            for (var i = 0; i < nActualDisplayRow; ++i) {
                var gridRow;
                if (i < self.data.length)
                    gridRow = new KGridRow(i, self.data[i], self);
                else
                    gridRow = new KGridRow(i, self.createBlankDataItem(), self);

                gridRow.setReadOnly(!self.gridDef.editable);
                // Should lock the new row if the grid is locked
                if (self.locked)
                    gridRow.lock();
                self.rows.push(gridRow);
                $body.append(gridRow.$element);
            }

            if (self.gridDef.enableAddBlankRow) {
                if (self.data.length > 0 && (self.data[self.data.length - 1].itemState != KGrid.ItemState.blankItem)) {
                    self.addRow(null);
                }
            }

        };
        this.setData = function (items, nVirtualItems, iPageIndex) {
            self.data = $.extend(true, [], items);
            for (var i = 0; i < self.data.length; ++i) { // Apply itemState attribute to each obj in the data array
                var tmpData = self.data[i];
                if (tmpData.itemState == undefined || tmpData.itemState == null)
                    self.data[i].itemState = KGrid.ItemState.originalItem;
            }

            if (nVirtualItems != undefined && nVirtualItems != null)
                self.nVirtualRows = nVirtualItems;
            else
                self.nVirtualRows = (items == null) ? 0 : items.length;
            if (iPageIndex != undefined && iPageIndex != null)
                self.pageIndex = iPageIndex;

            self.renderGrid();
        };
        this.updateDimension = function () {
            var containerWidth = parseInt(self.$element.parent().width(), 10);
            var containerHeight = parseInt(self.$element.parent().height(), 10);
            self.gridDef.width = containerWidth - 2;
            self.gridDef.widthExcludingScrollbar = containerWidth - 18 - 2 /* scrollbar's width is 17px across all browsers */;
            self.gridDef.height = containerHeight - 2;
            // set width & height to the container so that the container can use overflow.
            self.$element.css({ "height": containerHeight, "width": containerWidth });

            // Calculate cell height
            var gridDef = self.gridDef;
            var cellActualHeight = gridDef.rowHeight + 1 /* the extra 1px comes from the bottom border's height */;
            var cellHeaderActualHeight = gridDef.headerHeight + 1;

            // find if there is a column that should stretch to fit the remaining space
            var stretchCols = [];
            //var remainingSpace = containerWidth;
            var remainingSpace = gridDef.widthExcludingScrollbar;
            for (var i = 0; i < self.colDefs.length; ++i) {
                cellColDef = self.colDefs[i];
                if (cellColDef.width == 0) {
                    cellColDef.isStretchCol = true;
                    stretchCols.push(i);
                }
                else {
                    cellColDef.actualWidth = cellColDef.width + 1;
                    remainingSpace -= cellColDef.actualWidth;
                }
                cellColDef.height = gridDef.rowHeight;
                cellColDef.actualHeight = cellActualHeight;
                cellColDef.headerHeight = gridDef.headerHeight;
                cellColDef.headerActualHeight = cellHeaderActualHeight;
            }
            if (stretchCols.length > 0) { // Assign proper width to all stretch cols
                var stretchWidth = Math.floor(remainingSpace / stretchCols.length);
                var remaining = remainingSpace % stretchCols.length;
                // Diversify the remaining space to all stretched columns
                for (var i = 0; i < stretchCols.length; ++i) {
                    var colDef = self.colDefs[stretchCols[i]];
                    colDef.actualWidth = stretchWidth;
                    colDef.width = colDef.actualWidth - 1;
                }
                for (var i = 0; i < stretchCols.length; ++i) {
                    var colDef = self.colDefs[stretchCols[i]];
                    if (remaining > 0) {
                        colDef.width += 1;
                        colDef.actualWidth += 1;
                        --remaining;
                    }
                }
            }
        };
        //this.injectInternetExplorerSupport = function () {
        //    var $parentDiv = self.$element.parent();
        //    var html = $parentDiv.html();
        //    $parentDiv.empty();
        //    var $divWrapper = $("<div class='" + KGrid.CssClass.gridContainer + "'></div>");
        //    $divWrapper.css({ "width": self.gridDef.width });
        //    $divWrapper.append("<!--[if lte IE 9]><div class='" + KGrid.CssClass.supportOldIe + "'><!--<![endif]-->");
        //    $divWrapper.append(html);
        //    $divWrapper.append("<!--[if lte IE 9]></div><!--<![endif]-->");
        //    $parentDiv.append($divWrapper);
        //};
        this.preventActiveEditorClose = function () {
            // Only set the flag if the timer exists.
            if (self.closeEditorTimer != null)
                self.isPreventEditorClose = true;
        }
        this.closeActiveEditor = function () {
            if (self.lastEditableCell != null) {
                self.isPreventEditorClose = false;
                self.lastEditableCell.commitChange();
            }
        }
        this.selectRow = function (row) { // Accept rowIndex or kGridRow
            if (!isNaN(row) && row < self.rows.length)
                self.rows[row].select();
            else
                row.select(false);
        }
        this.createBlankDataItem = function () {
            var retData = {};
            for (var iCol = 0; iCol < self.colDefs.length; ++iCol) {
                var colDef = self.colDefs[iCol];
                if (colDef.name != "")
                    retData[colDef.name] = null;
            }
            retData.itemState = KGrid.ItemState.blankItem;
            return retData;
        }
        this.deleteRowByIndex = function (iRow) {
            var row = self.rows[iRow];
            if (row == null)
                return false;
            if (row.rowState == KGrid.ItemState.blankItem ||
                row.rowState == KGrid.ItemState.deletedItem)
                return false;

            if (row.editingCell != null) {
                //row.editingCell.commitChange();
                row.editor.destroy();
                row.editingCell.unregisterEditor();
                self.rowInProgress = null;
            }

            if (row.rowState != KGrid.ItemState.newItem) {
                // only mark the row as deleted but do not remove it.
                row.setState(KGrid.ItemState.deletedItem);
                return true;
            }

            if (self.gridDef.onRowDeleted != null)
                self.gridDef.onRowDeleted(row);

            self.data.splice(row.dataIndex, 1);
            // Update index of all rows
            for (var i = 0; i < self.rows.length; ++i)
                self.rows[i].rowIndex = i;

            row.$element.remove();
            self.selectedRow = null;

            return true;
        }
        this.undoDeleteRowByIndex = function (iRow) {
            var row = self.rows[iRow];
            if (row == null || row.rowState != KGrid.ItemState.deletedItem)
                return;
            row.setState(row.prevRowState);
        }
        this.isEditing = function () {
            return (self.rowInProgress != null);
        }
        this.commitChange = function () {
            if (self.rowInProgress == null)
                return true;
            return self.rowInProgress.editingCell.commitChange();
        }
        this.addRow = function (pData, itemState) {
            if (!self.gridDef.enableAddBlankRow && pData == null)
                return null;

            var row = null;
            var item = null;
            if (pData != null)
                item = pData;
            else
                item = self.createBlankDataItem();

            // Check if we should: 
            // 1. Only assign the data to the first blank row we find, or
            // 2. Add the new row with data because there is no blank row left.
            for (var i = 0; i < self.rows.length; ++i) {
                if (self.rows[i].rowState == KGrid.ItemState.blankItem) {
                    row = self.rows[i];
                    break;
                }
            }
            if (row == null) { // add a new row with data 
                row = new KGridRow(self.rows.length, item, self);
                row.setReadOnly(false);
                if (self.locked)
                    row.lock();
                self.$element.find("tbody").append(row.$element);
            }
            else { // use the first blank row and supply it with the data
                row.setData(item);
            }

            if (itemState == undefined || itemState == null) {
                if (pData == null)
                    row.setState(KGrid.ItemState.blankItem);
                else
                    row.setState(KGrid.ItemState.originalItem);
            } else {
                row.setState(itemState);
            }

            if (pData == null)
                self.notifyBlankRowAdded(row);
            else
                self.notifyDataRowAdded(row);

            return row;
        }

        // Draw the grid & update the grid data
        self.setData(pData);
    }

})(jQuery);
