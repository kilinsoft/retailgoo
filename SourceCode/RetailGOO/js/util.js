﻿function cloneObject(obj) {
    return $.extend(true, {}, obj);
}

function cloneArray(arr) {
    return arr.slice();
}

function hasValue(param) {
    return ((param != undefined && param != null) == true);
}

function changeUrl(url, param) {
    if (hasValue(param))
        window.location.href = url + param;
    else
        window.location.href = url;
}

function scrollToElement(id) {
    if ($('#' + id).length == 0)
        return;
    $('html,body').animate({ scrollTop: $("#" + id).offset().top }, 'slow');
}

function GoToUrl(url, param) {
    if (hasValue(param))
        window.location.href = url + param;
    else
        window.location.href = url;
}

function escapeHtml(str) {
    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };

    return String(str).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}

function padSpace(str, nPad) {
    str = str.toString();
    if (str.length < nPad) {
        var strTmp = pad("0" + "A", nPad);
        strTmp = strTmp.replace("0", "&nbsp;");
        strTmp = strTmp.replace("A", str)
    }
    return strTmp;
}

function padZero(str, nPad) {
    var str = str.toString();
    return str.length < nPad ? pad("0" + str, nPad) : str;
}

function isFunction(func) {
    var tmpType = {};
    return func && tmpType.toString.call(func) === '[object Function]';
}

// Check if n is integer number
function isIntNumber(n) {
    return !isNaN(parseInt(n)) && isFinite(n);
}

// Return the integer number. The null value will be return if 'n' is not a number.
function formIntNumber(n) {
    if (IsIntNumber(n)) return parseInt(n, 10); //To remove a zero prefix. '01' -> '1'
    else return null;
}

//Get name of the function which this code is running on.
function getFunctionName() {
    var funcCode = GetFunctionName.caller.toString();
    funcCode = funcCode.slice(funcCode.indexOf(" ") + 1, funcCode.indexOf("{"));
    var funcName = funcCode.trim();
    return funcName;
}

function scrollUpToTarget($element, delay) {
    if (!hasValue(delay))
        delay = 400;
    $('html, body').animate({
        scrollTop: $element.offset().top - 10
    }, delay);
}

$.fn.enable = function () {
    this.prop("disabled", false);
};
$.fn.disable = function () {
    this.prop("disabled", true);
};
$.fn.isEnabled = function () {
    return $(this).is(":disabled");
};

// Manage check/uncheck state of an input control of the type "checkbox"
$.fn.check = function () {
    if (this.is(':checkbox') || this.is(':radio'))
        this.prop('checked', true);
};
$.fn.uncheck = function () {
    if (this.is(':checkbox') || this.is(':radio'))
        this.prop('checked', false);
};
$.fn.isChecked = function () {
    return (this.attr('checked') == 'checked');
};

// Manage readonly state of an input of the following types -- text, checkbox, radio, password, file -- , and textarea.
$.fn.readOnly = function (val) {
    if (this.is(':text') || this.is(':checkbox') || this.is(':radio') || this.is(':password') || this.is(':file') || this.is(':textarea'))
        if (val == true) {
            //this.attr("readonly", "readonly");
            this.addClass("readonly");
        } else {
            //this.removeAttr("readonly");
            this.removeClass("readonly");
        }
    this.prop("readonly", (val == true));
};
$.fn.isReadOnly = function () {
    return (this.attr('readonly') == 'readonly');
};

/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Check Form Dirty
//
/////////////////////////////////////////////////////////////////////////////////////////////////
var __kilinutil_css_cause_dirty = "kutil-cause-dirty";
var __kilinutil_data_isdirty = "isDirty";
$.fn.isDirty = function () {
    var isDirty = $(this).data(__kilinutil_data_isdirty);
    if (hasValue(isDirty))
        return isDirty;
}
$.fn.setDirty = function (flag) {
    $(this).data(__kilinutil_data_isdirty, (flag == true ? true : false))
}
$(function () {
    $("." + __kilinutil_css_cause_dirty).on("change", function () {
        $(this).setDirty(true);
    }).setDirty(false);
});


/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Cache
//
/////////////////////////////////////////////////////////////////////////////////////////////////
var __kilinutil_data_cache = "cacheValue";
var __kilinutil_data_state = "oldState";
$.fn.cache = function (val) {
    if (hasValue(val))
        $(this).data(__kilinutil_data_cache, val);
    else
        return $(this).data(__kilinutil_data_cache);
}
$.fn.saveState = function () {
    var $tmp = $(this).clone(true);
    $(this).data(__kilinutil_data_state, $tmp);
}
$.fn.revertState = function () {
    if (hasValue($(this).data(__kilinutil_data_state))) {
        var $tmp = $(this).data(__kilinutil_data_state);
        $(this).replaceWith($tmp);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Prevent from navigating away if there are unsaved changes
//
/////////////////////////////////////////////////////////////////////////////////////////////////

var CSS_NOT_CAUSE_DIRTY = "not-cause-dirty";
var CSS_CLEAR_DIRTY = "clear-dirty";
var __IS_DIRTY__ = false;
var __IS_PREVENT_NAVIGATION_ON_UNSAVED__ = false;
var __IS_INIT_PREVENT_NAVIGATION_ON_UNSAVED__ = false;

// preventNavigationOnUnsaved
//  Indicate that the screen is registered to be monitors for any changes made to the screen.
//  If the user tries to navigate away, close the screen, click on a link, there will be 
//  a notification dialog to warn the user whether they want to save any unsaved changes first.
//  
//  For the controls which do not cause dirty state, add the following css class to the control
//      : not-cause-dirty
//  For the controls that clears the dirty state, add the following css class to the control
//      : clear-dirty
//
// Parameter 
//  return  : Boolean whether the screen uses this functionality
//  flag    : Boolean whether the screen will be using this functionality. Calling the function
//            with a flag provided will register the screen to this functionality.
function preventNavigationOnUnsaved(/*Optional*/ flag) {
    if (hasValue(flag)) {
        var fUse = (flag == true);
        __IS_PREVENT_NAVIGATION_ON_UNSAVED__ = fUse;
        if (fUse) {
            // Enclose the __doPostBack with try/catch or else the function would fire an error
            // if we chose "Stay on the page" after we've triggered to navigate away from the page.
            if (__IS_INIT_PREVENT_NAVIGATION_ON_UNSAVED__ != true) {
                __IS_INIT_PREVENT_NAVIGATION_ON_UNSAVED__ = true;
                if (window.__doPostBack) {
                    var originalPostBackFunc = window.__doPostBack;
                    window.__doPostBack = function (eventTarget, eventArgument) {
                        try {
                            originalPostBackFunc(eventTarget, eventArgument);
                        }
                        catch (ex) {
                            if (ex.message.indexOf("Unspecified") == -1)
                                throw ex;
                        }
                    }
                }
                var checkIsDirtyFunc = function () {
                    if (__IS_PREVENT_NAVIGATION_ON_UNSAVED__ && isPageDirty())
                        return "You are about to navigate away from this screen.\nAny unsaved changes will be discarded.";
                }
                window.onbeforeunload = checkIsDirtyFunc;
                window.onsubmit = checkIsDirtyFunc;
            }
            $('input:not([type=button]):not([type=submit]):not(.' + CSS_NOT_CAUSE_DIRTY + '),textarea:not(.' + CSS_NOT_CAUSE_DIRTY + '),select:not(.' + CSS_NOT_CAUSE_DIRTY + ')').on("input.causeDirty", function () {
                // The CSS for clearing dirty state may apply to a parent control of which the input resides in.
                // For example, asp.net and telerik apply classes from the tag "CssClass" to a div not to an input control.
                // Then before marking the state to dirty we have to make sure that this control is not reside in the 
                // parent being said.
                if ($(this).closest('.' + CSS_NOT_CAUSE_DIRTY).length == 0)
                    setPageDirty(true);
            });
            $('.' + CSS_CLEAR_DIRTY).click(function () {
                setPageDirty(false);
            });
        }
    }
    return __IS_PREVENT_NAVIGATION_ON_UNSAVED__;
}

// isPageDirty
//  Get the page state to dirty which means if there are any changes done to the page
// Parameter 
//  return  : the dirty state
function isPageDirty() {
    return __IS_DIRTY__;
}

function setPageDirty(flag) {
    if (hasValue(flag))
        __IS_DIRTY__ = (flag == true);
}

// Conversion
function convertToJsonDate(strDate) {
    var dt = new Date(strDate);
    var newDate = new Date(Date.UTC(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), dt.getSeconds(), dt.getMilliseconds()));
    return '/Date(' + newDate.getTime() + ')/';
}
function convertToUTCDateTime(date) {
    // check if this is a Date obj or date string
    if (typeof date.getMonth === 'function') { // date obj
        return new Date(date.getTime() - (date.getTimezoneOffset() * 60000));
    }
    else { // string
        return new Date(date).toISOString();
    }
}
//function convertToLocalDateTime(date) {
//    if (typeof date.getMonth === 'function') { // date obj
//        return new Date(date.getTime() + (date.getTimezoneOffset() * 60000));
//    } else { // string
//        if (date.indexOf("T") > 0) {
//            var dateLocal = new Date(date);
//        } else {

//        }
//    }
//}
//function convertFromUTCDateString(strDateUTC) {
//    // Expect format 2013-03-10T02:00:00Z", and will convert it to local javascript date object.
//    var dateUtc = new Date(strDateUTC);
//    var dateLocal = new Date(dateUtc.getTime() + (dateUtc.getTimezoneOffset * 60000));
//    return dateLocalTime.getFullYear() + "-" + (dateLocalTime.getMonth() + 1) + "-" + dateLocalTime.getDate();
//}

function convertToAmount(val, precision) {
    if (precision != null && precision != undefined) {
        //if (val % 1 !== 0) // float
        if (String(val) == val)
            val = parseFloat(val);
        val = val.toFixed(precision);
    }
    var parts = val.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

// Validation functions
function validateEmail(txt) {
    var regEx = /^(([^<>()[\]\\.:;,\s@\"]+(\.[^<>()[\]\\.:;,\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regEx.test(txt);
}

function validateUsername(txt) {
    var regEx = /^[a-z][a-z0-9]*[\.[a-z][a-z0-9]*]{0,1}$/;
    return (regEx.test(txt) && (txt.length <= 50));
}

function validateAmount(val) {
    if (!hasValue(val) || val.length > 22) //1,000,000,000,000.0000
        return false;
    // with commas:     ^-?(([1-9][0-9]{0,2}(,[0-9]{3})*)|0)(\.[0-9]{1,4})?$
    // without commas:  ^-?(([1-9][0-9]*)|0)(\.[0-9]{1,4})?$
    var regEx = /^-?((([1-9][0-9]{0,2}(,[0-9]{3})*)|([1-9][0-9]*))|0)(\.[0-9]{1,4})?$/;
    return regEx.test(val);
}

function validateFloat(val) {
    return !isNaN(parseFloat(val)) && isFinite(val);
}

function validateInteger(val) {
    return !isNaN(value) && (parseInt(Number(val)) == value) && !isNaN(parseInt(value, 10));
}

// Sorting
function sortByKey(keyName, isDecimal) {
    return function (v1, v2) {
        var valA, valB;
        if (isDecimal) {
            if (v1 == null || v1 == undefined)
                valA = Infinity;
            else
                valA = parseFloat(v1[keyName]);
            if (v2 == null || v2 == undefined)
                valB = Infinity;
            else
                valB = parseFloat(v2[keyName]);
        }
        else {
            valA = String(v1[keyName]).toLowerCase();
            valB = String(v2[keyName]).toLowerCase();
        }
        return ((valA < valB) ? -1 : ((valA > valB) ? 1 : 0));
    };
}

(function ($) {
    $.extend(true, window, {
        Util: {
            Input: {
                Amount: AmountInput
            }
        }
    });

    // Amount input
    function AmountInput(domInput, precision, funcOnChange, funcOnFocus) {
        var aInput = this;
        this.$element = $("" + domInput);
        var i = this.$element.length;
        this.data = (this.$element.val() == "" ? 0 : this.$element.val());
        this.displayText = this.data;
        this.formatOnZero();
        this.precision = (hasValue(precision) ? precision : 0);
        this.valid = true;
        this.funcOnBlur = function () {
            aInput.setValue(aInput.$element.val());
        }
        this.funcOnChange = funcOnChange;
        this.funcOnFocus = funcOnFocus;
        var $input = this.$element;
        //$input.on("focus.amountInputAutoFormat", function () {
        //    if (this.valid)
        //        $input.val(this.data);
        //});
        $input.on("blur.amountInputAutoFormat", function () {
            aInput.setValue($input.val());
        });
        $input.on("focus.amountInputFocus", function () {
            $(this).one('mouseup', function () {
                $(this).select();
                return false;
            }).select();
            if (aInput.funcOnFocus != null && aInput.funcOnFocus != undefined)
                aInput.funcOnFocus(aInput);
        });
        $input.keypress(function (event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

    }

    AmountInput.prototype.cssError = "util-amountinput-invalid";
    AmountInput.prototype.cssDisabled = "util-amountinput-disabled";
    AmountInput.prototype.isValid = function () {
        return this.valid;
    }
    AmountInput.prototype.setValue = function (val) {
        val = $.trim(val);
        if (val == "" && this.data == "") {
            this.displayText = "";
            this.$element.removeClass(this.cssError);
            this.$element.val(this.displayText);

            return true;
        } else if (val == "" && this.data != "") {
            this.displayText = convertToAmount(this.data);
            this.$element.removeClass(this.cssError);
            this.$element.val(this.displayText);

            this.formatOnZero();
            return true;
        }
        else {
            val = parseFloat(String(val).replace(',', ''));
            if (!isNaN(val))
                val = val.toFixed(this.precision);
            if (!validateAmount(val)) {
                this.valid = false;
                this.displayText = val;
                this.$element.addClass(this.cssError);
                return false;
            }
            else {
                this.valid = true;
                this.data = parseFloat(val);
                // Check if we have to round the precision up to 2 digits.
                var parts = String(this.data).split(".");
                if (this.precision <= 0)
                    this.data = Math.round(this.data);
                else {
                    if (parts.lenght > 1 && parts[1].length > this.precision)
                        this.data = this.data.toFixed(this.precision);
                }
                this.displayText = convertToAmount(this.data);
                this.$element.removeClass(this.cssError);
                this.$element.val(this.displayText);

                this.formatOnZero();

                if (this.funcOnChange != null && this.funcOnChange != undefined) {
                    this.funcOnChange(this.$element, this.data);
                }

                return true;
            }
        }
    };
    AmountInput.prototype.getValue = function () {
        return this.data;
    };
    AmountInput.prototype.formatOnZero = function () {
        if (this.data == 0) {
            var val = "0";
            if (this.precision > 0) {
                val += ".";
                for (var i = 0; i < this.precision; ++i)
                    val += "0";
            }
            this.$element.val(val);
        }
    };
    AmountInput.prototype.lock = function () {
        this.$element.prop("readonly", true);
        this.$element.off("blur.amountInputAutoFormat");
    }
    AmountInput.prototype.unlock = function () {
        this.$element.prop("readonly", false);
        this.$element.on("blur.amountInputAutoFormat", this.funcOnBlur);
    }
    AmountInput.prototype.disable = function () {
        this.lock();
        this.$element.addClass(this.cssDisabled);
    }
    AmountInput.prototype.enable = function () {
        this.unlock();
        this.$element.removeClass(this.cssDisabled);
    }

}(jQuery));


// For getting mouse position
var g_mousePos = { x: -1, y: -1 };
(function ($) {
    $(document).mousemove(function (event) {
        g_mousePos.x = event.pageX;
        g_mousePos.y = event.pageY;
    });
}(jQuery));