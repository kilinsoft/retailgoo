﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public static class Business
    {
        public static BaseResponse CreateBusiness(BusinessDto bDto)
        {
            BaseResponse rsp = new BaseResponse();

            DateTime dateExpired = DateTime.Now.AddDays(Util.Constant.TimeDefinition.BusinessCreationValidityPeriod);

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@UserId", SqlDbType.BigInt, SessionHelper.GetCurrentUserId());
            cmd.AddParam("@BusinessName", SqlDbType.NVarChar, bDto.BusinessName);
            cmd.AddParam("@BusinessTypeId", SqlDbType.TinyInt, bDto.BusinessTypeId != -1 ? (byte)bDto.BusinessTypeId : (object)DBNull.Value);
            //cmd.AddParam("@UseTemplateBusiness", SqlDbType.Bit, bDto.UseTemplateBusiness);
            //cmd.AddParam("@ImmediateBusinessId", SqlDbType.BigInt, bDto.ImmediateBusinessId);
            //cmd.AddParam("@RootBusinessId", SqlDbType.BigInt, bDto.RootBusinessId);
            cmd.AddParam("@ImagePath", SqlDbType.VarChar, bDto.ImagePath);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, bDto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, bDto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, bDto.Address3);
            cmd.AddParam("@City", SqlDbType.NVarChar, bDto.City);
            cmd.AddParam("@CountryId", SqlDbType.TinyInt, (bDto.CountryId != -1 ? bDto.CountryId : (object)DBNull.Value));
            cmd.AddParam("@ZipCode", SqlDbType.NVarChar, bDto.ZipCode);
            cmd.AddParam("@Phone", SqlDbType.NVarChar, bDto.Phone);
            cmd.AddParam("@Fax", SqlDbType.NVarChar, bDto.Fax);
            cmd.AddParam("@Email", SqlDbType.VarChar, bDto.Email);
            cmd.AddParam("@Website", SqlDbType.NVarChar, bDto.Website);
            cmd.AddParam("@FacebookUrl", SqlDbType.NVarChar, bDto.FacebookUrl);
            cmd.AddParam("@TaxIdInfo", SqlDbType.NVarChar, bDto.TaxIdInfo);
            cmd.AddParam("@DateExpired", SqlDbType.DateTime, dateExpired);
            //cmd.AddOutputParam("@BusinessId", SqlDbType.BigInt);
            rsp = cmd.RunSpNonQuery("spBusinessCreate");

            if (!rsp.Success)
            {
                rsp.Fail(string.Format("Could not create the business[ {0} ]", bDto.BusinessName));
                rsp.SetDebugText(rsp.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", rsp.StatusText, rsp.StatusText));
            }
            return rsp;  
        }

        public static GenericResponse<IList<BusinessSelectionDto>> RetrieveAvailableBusinessesForUser()
        {
            var rsp = new GenericResponse<IList<BusinessSelectionDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@UserId", SqlDbType.BigInt, SessionHelper.GetCurrentUserId());
            BaseResponse ret = cmd.RunSpQuery("spStaffBusinessAllRetrieve");
            if (ret.Success)
            {
                var result = new List<BusinessSelectionDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    BusinessSelectionDto bsDto = new BusinessSelectionDto();
                    bsDto.BusinessId = long.Parse(row["BusinessId"].ToString());
                    bsDto.BusinessName = row["BusinessName"].ToString();
                    bsDto.DateExpired = (DateTime)row["DateExpired"];
                    bsDto.Role = row["Role"].ToString();
                    result.Add(bsDto);
                }
                rsp.Succeed();
                rsp.ResponseItem = result;
            }
            else
            {
                rsp.Fail(string.Format("Could not retrieve StaffBusiness for UserId[{0}]", SessionHelper.GetCurrentUserId()));
                rsp.SetDebugText(rsp.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", rsp.StatusText, ret.StatusText));
            }
            return rsp;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveStaffList()
        {
            var rsp = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spStaffListRetrieve");
            if (ret.Success)
            {
                var result = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = long.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    result.Add(item);
                }
                rsp.Succeed();
                rsp.ResponseItem = result;
            }
            else
            {
                rsp.Fail(string.Format("Could not retrieve StaffList for BusinessId[{0}]", SessionHelper.GetCurrentBusinessId()));
                rsp.SetDebugText(rsp.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", rsp.StatusText, ret.StatusText));
            }
            return rsp;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveCountryList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spCountryListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                // Add undefined item by default
                //tmpResult.Add(new OptionItemString() { Value = "", Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem()
                    {
                        Value = long.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CountryList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveDefaultPriceList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spDefaultPriceTypeListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve DefaultPriceList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveTargetBusinessList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spTargetBusinessListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                // Add undefined item by default
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve TargetBusinessList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }
        public static GenericResponse<IList<OptionItem>> RetrieveSourceBusinessList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spSourceBusinessListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                // Add undefined item by default
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SourceBusinessList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveBusinessTypeList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spBusinessTypeListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve BusinessTypeList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveCurrencyList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spCurrencyListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CurrencyList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveDefaultPriceTypeList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spDefaultPriceTypeListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve DefaultPriceTypeList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveSalesTaxTypeList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spSalesTaxTypeListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SalesTaxTypeList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveCostingTypeList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spCostingTypeListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CostingTypeList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #region BusinessInfo

        public static GenericResponse<BusinessInfoDto> RetrieveBusinessInfo()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<BusinessInfoDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spBusinessInfoRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    BusinessInfoDto dto = new BusinessInfoDto();

                    dto.BusinessId = long.Parse(row["BusinessId"].ToString());
                    dto.BusinessName = row["BusinessName"].ToString();
                    dto.BusinessTypeId = row["BusinessTypeId"] != DBNull.Value ? 
                        long.Parse(row["BusinessTypeId"].ToString()) : -1;
                    dto.BusinessTypeName = row["BusinessTypeName"] != DBNull.Value ?
                        row["BusinessTypeName"].ToString() : "";
                    //dto.UseTemplateBusiness = (bool)row["UseTemplateBusiness"];
                    //dto.ImmediateBusinessId = row["ImmediateBusinessId"] != DBNull.Value ? 
                    //    long.Parse(row["ImmediateBusinessId"].ToString()) : -1;
                    //dto.RootBusinessId = row["RootBusinessId"] != DBNull.Value ? 
                    //    long.Parse(row["RootBusinessId"].ToString()) : -1;
                    dto.ImagePath = row["ImagePath"].ToString();
                    dto.Address1 = row["Address1"].ToString();
                    dto.Address2 = row["Address2"].ToString();
                    dto.Address3 = row["Address3"].ToString();
                    dto.City = row["City"].ToString();
                    dto.CountryId = row["CountryId"] != DBNull.Value?
                        long.Parse(row["CountryId"].ToString()) : -1;
                    dto.CountryName = row["CountryName"] != DBNull.Value?
                        row["CountryName"].ToString() : "";
                    dto.ZipCode = row["ZipCode"].ToString();
                    dto.Phone = row["Phone"].ToString();
                    dto.Fax = row["Fax"].ToString();
                    dto.Email = row["Email"].ToString();
                    dto.Website = row["Website"].ToString();
                    dto.FacebookUrl = row["FacebookUrl"].ToString();
                    dto.TaxIdInfo = row["TaxIdInfo"].ToString();
                    dto.DateExpired = DateTimeHelper.ConvertToDateString((DateTime)row["DateExpired"]);

                    //dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("DocSetting[{0}] does not exist", businessId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve BusinessInfo for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageBusinessInfo(BusinessInfoDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@BusinessName", SqlDbType.NVarChar, dto.BusinessName);
            cmd.AddParam("@BusinessTypeId", SqlDbType.TinyInt, dto.BusinessTypeId);
            //cmd.AddParam("@UseTemplateBusiness", SqlDbType.Bit, dto.UseTemplateBusiness);
            //cmd.AddParam("@ImmediateBusinessId", SqlDbType.BigInt, dto.ImmediateBusinessId);
            //cmd.AddParam("@RootBusinessId", SqlDbType.BigInt, dto.RootBusinessId);
            cmd.AddParam("@ImagePath", SqlDbType.VarChar, dto.ImagePath);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, dto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, dto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, dto.Address3);
            cmd.AddParam("@City", SqlDbType.NVarChar, dto.City);
            cmd.AddParam("@CountryId", SqlDbType.TinyInt, dto.CountryId != -1 ? dto.CountryId : (object)DBNull.Value);
            cmd.AddParam("@ZipCode", SqlDbType.NVarChar, dto.ZipCode);
            cmd.AddParam("@Phone", SqlDbType.NVarChar, dto.Phone);
            cmd.AddParam("@Fax", SqlDbType.NVarChar, dto.Fax);
            cmd.AddParam("@Email", SqlDbType.NVarChar, dto.Email);
            cmd.AddParam("@Website", SqlDbType.NVarChar, dto.Website);
            cmd.AddParam("@FacebookUrl", SqlDbType.NVarChar, dto.FacebookUrl);
            cmd.AddParam("@TaxIdInfo", SqlDbType.NVarChar, dto.TaxIdInfo);
            cmd.AddParam("@DateExpired", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.DateExpired));

            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            //cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@ItemState", SqlDbType.Char, 'M');    // HIM: Docsetting is always changed. 

            BaseResponse ret = cmd.RunSpNonQuery("spBusinessInfoManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process BusinessInfo for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region Roles
        public static void ConvertRolesParaToDto(IList<RolesItemDto> items, string rolesformat)
        {
            string[] rawPara = rolesformat.Split(',');

            foreach (string s in rawPara)
            {
                string[] r = s.Split(':');

                var item = new RolesItemDto();

                item.PageName = r[0].ToString();
                item.RoleTypeValue = r[1].ToString();

                items.Add(item);
            }

            return;
        }
        public static GenericResponse<RolesDto> RetrieveRoles(string roleShortName)
        //public static GenericResponse<RolesDto> RetrieveRoles()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<RolesDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spRolesRetrieve");
            if (ret.Success)
            {
                RolesDto dto = new RolesDto();
                var items = new List<RolesItemDto>();
                dto.Items = items;

                dto.RoleCategory = Staff.GetRoleFullname(roleShortName);

                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    // convert a rolesparameter to a rolesdto
                   ConvertRolesParaToDto(dto.Items, row[dto.RoleCategory].ToString());
                }

                result.Succeed();
                result.ResponseItem = dto;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve Roles for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageRoles(RolesDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            // convert a rolesdto to a rolesparameter
            string rolespara = "";
            RolesItemDto last = dto.Items.Last();
            foreach (RolesItemDto row in dto.Items)
            {
                rolespara += row.PageName;
                rolespara += ":";
                rolespara += row.RoleTypeValue;
                if (row != last)
                    rolespara += ",";
            }

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@RolesColumnName", SqlDbType.NVarChar, dto.RoleCategory);
            cmd.AddParam("@RolesParameter", SqlDbType.NVarChar, rolespara.ToString());
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@ItemState", SqlDbType.Char, 'M');    // HIM: Roles is always changed. 

            BaseResponse ret = cmd.RunSpNonQuery("spRolesManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process Roles for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItemString>> RetrieveRoleList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItemString>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spRoleListRetrieve");
            if (ret.Success)
            {
                IList<OptionItemString> items = new List<OptionItemString>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItemString();
                    item.Value = row["Value"].ToString();
                    item.Text = row["Text"].ToString();
                    items.Add(item);
                }

                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve RoleList"));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItemString>> RetrieveStaffStatusList()
        {
            var items = new List<OptionItemString>();

            items.Add(new OptionItemString() { Value = StaffStatusString.Active, Text = StaffStatusString.Active });
            items.Add(new OptionItemString() { Value = StaffStatusString.Inactive, Text = StaffStatusString.Inactive });
            items.Add(new OptionItemString() { Value = StaffStatusString.Waiting, Text = StaffStatusString.Waiting });
            items.Add(new OptionItemString() { Value = StaffStatusString.Declined, Text = StaffStatusString.Declined });

            var result = new GenericResponse<IList<OptionItemString>>();
            result.Succeed();
            result.ResponseItem = items;
            return result;
        }

        #endregion

        public static GenericResponse<IList<UserProfileDto>> RetrieveAllBusinessStaff()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<UserProfileDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spBusinessStaffAllRetrieve");
            if (ret.Success)
            {
                var items = new List<UserProfileDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new UserProfileDto();
                    item.StaffId = long.Parse(row["StaffId"].ToString());
                    item.Username = row["Username"].ToString();
                    item.Email = row["Email"].ToString();

                    if (item.Username == string.Empty)
                    {
                        item.Username = item.Email;
                    }

                    item.Status = row["Status"].ToString();
                    item.RoleShortName = row["BusinessRoleShortName"].ToString();
                    item.RoleName = row["BusinessRoleName"].ToString();
                    items.Add(item);
                }

                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve All BusinessStaff for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }
    }
}