﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
//using System.Windows.Forms;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class ItemAdapter
    {
        #region ItemManagement

        public static GenericResponse<IList<ItemGroupDto>> RetrieveAllItemGroup()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemGroupDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemGroupAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemGroupDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemGroupDto igDto = new ItemGroupDto()
                    {
                        ItemGroupId = long.Parse(row["ItemGroupId"].ToString()),
                        ItemGroupCode = row["ItemGroupCode"].ToString(),
                        ItemGroupName = row["ItemGroupName"].ToString(),
                        ItemCategoryName = row["ItemCategoryName"].ToString(),
                        ItemBrandName = row["ItemBrandName"].ToString(),
                        Quantity = row["Quantity"].ToString()
                    };

                    tmpResult.Add(igDto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve All ItemGroup for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ItemGroupDto> RetrieveItemGroup(long itemGroupId)
        {
            var result = new GenericResponse<ItemGroupDto>();
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@ItemGroupId", SqlDbType.BigInt, itemGroupId);
            BaseResponse ret = cmd.RunSpQuery("spItemGroupRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    var dto = new ItemGroupDto()
                    {
                        ItemGroupId = (long)row["ItemGroupId"],
                        ItemGroupName = row["ItemGroupName"].ToString(),
                        ItemGroupCode = row["ItemGroupCode"].ToString(),
                        ImagePath = row["ImagePath"].ToString(),
                        ItemCategoryId =
                            row["ItemCategoryId"] != DBNull.Value ?
                                (long)row["ItemCategoryId"] : -1,
                        ItemBrandId =
                            row["ItemBrandId"] != DBNull.Value ?
                                (long)row["ItemBrandId"] : -1,
                        Cost = (decimal)row["Cost"],
                        Price = (decimal)row["Price"],
                        UnitOfMeasure = row["Uom"].ToString(),
                        IsForSales = (bool)row["IsForSales"],
                        Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"])
                    };

                    var retSubItems = RetrieveAllItem(itemGroupId);
                    if (retSubItems.Success)
                    {
                        dto.Items = retSubItems.ResponseItem;
                        result.Succeed();
                        result.ResponseItem = dto;
                    }
                    else
                    {
                        result.Fail(retSubItems.StatusText);
                        Logger.SessionError(result.StatusText);
                    }
                }
                else
                {
                    result.Fail(string.Format("ItemGroupId[{0}] does not exist", itemGroupId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemGroupId[ {0} ]", itemGroupId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemDto>> RetrieveAllItem(long itemGroupId)
        {
            var result = new GenericResponse<IList<ItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@ItemGroupId", SqlDbType.BigInt, itemGroupId);

            BaseResponse ret = cmd.RunSpQuery("spItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var dto = new ItemDto();

                    dto.ItemId = (long)row["ItemId"];
                    dto.ItemName = row["ItemName"].ToString();
                    dto.ItemGroupId = (long)row["ItemGroupId"];
                    dto.Barcode = row["Barcode"].ToString();
                    dto.Quantity = (decimal)row["Quantity"];
                    dto.IsForSales = (bool)row["IsForSales"];
                    dto.Price = (decimal)row["Price"];
                    dto.Cost = (decimal)row["Cost"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve Items for ItemGroupId[{0}]", itemGroupId));
                Logger.SessionError(result.StatusText);
            }
            return result;
        }

        public static GenericResponse<ItemDto> RetrieveItem(long itemId)
        {
            var result = new GenericResponse<ItemDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@ItemId", SqlDbType.BigInt, itemId);

            BaseResponse ret = cmd.RunSpQuery("spItemRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    var dto = new ItemDto();

                    dto.ItemId = (long)row["ItemId"];
                    dto.ItemName = row["ItemName"].ToString();
                    dto.ItemGroupId = (long)row["ItemGroupId"];
                    dto.Barcode = row["Barcode"].ToString();
                    dto.Quantity = (decimal)row["Quantity"];
                    //dto.IsForSales = (bool)row["IsForSales"]; // There are IsForSales in Item and ItemGroup tables,
                    // refer to IsForSales from ItemGroup table for now
                    dto.IsForSales = (bool)row["IsItemGroupForSales"];
                    dto.ItemGroupName = row["ItemGroupName"].ToString();
                    dto.ItemBrandName = row["ItemBrandName"].ToString();
                    dto.ItemCategoryName = row["ItemCategoryName"].ToString();
                    dto.UnitOfMeasure = row["Uom"].ToString();
                    dto.Price = (decimal)row["Price"];
                    dto.Cost = (decimal)row["Cost"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("ItemId[{0}] does not exist", itemId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemId[{0}]", itemId));
                Logger.SessionError(result.StatusText);
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveItemList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                // Add undefined item by default
                //items.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    long itemId = (long)row["ItemId"];
                    string itemName = row["ItemName"].ToString();
                    string itemGroupName = row["ItemGroupName"].ToString();
                    string itemGroupCode = row["ItemGroupCode"].ToString();

                    item.Value = itemId;
                    item.Text = ItemDto.getOptionName(itemGroupName, itemGroupCode, itemName);
                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ItemList for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemDto>> RetrieveAllSalesItem()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spSalesItemAllRetrieve");
            if (ret.Success)
            {
                var items = new List<ItemDto>();
                // Add undefined item by default
                //items.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var dto = new ItemDto();

                    dto.ItemId = (long)row["ItemId"];
                    dto.ItemName = row["ItemName"].ToString();
                    dto.ItemGroupId = (long)row["ItemGroupId"];
                    dto.Barcode = row["Barcode"].ToString();
                    dto.Price = (decimal)row["Price"];
                    dto.ItemGroupName = row["ItemGroupName"].ToString();
                    dto.ItemGroupCode = row["ItemGroupCode"].ToString();
                    dto.ItemBrandId = int.Parse(row["ItemBrandId"].ToString());
                    dto.ItemBrandName = row["ItemBrandName"].ToString();
                    dto.ItemCategoryId = int.Parse(row["ItemCategoryId"].ToString());
                    dto.ItemCategoryName = row["ItemCategoryName"].ToString();
                    dto.UnitOfMeasure = row["Uom"].ToString();

                    items.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve AllSalesItem for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageItemGroup(ItemGroupDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemGroupId", SqlDbType.BigInt, dto.ItemGroupId);
            cmd.AddParam("@ItemGroupCode", SqlDbType.NVarChar, dto.ItemGroupCode);
            cmd.AddParam("@ItemGroupName", SqlDbType.NVarChar, dto.ItemGroupName);
            cmd.AddParam("@ImagePath", SqlDbType.VarChar, dto.ImagePath);
            cmd.AddParam("@ItemCategoryId", SqlDbType.BigInt,
                dto.ItemCategoryId != -1 ? dto.ItemCategoryId : (object)DBNull.Value);
            cmd.AddParam("@ItemBrandId", SqlDbType.BigInt,
                dto.ItemBrandId != -1 ? dto.ItemBrandId : (object)DBNull.Value);
            cmd.AddParam("@Cost", SqlDbType.Decimal, dto.Cost);
            cmd.AddParam("@Price", SqlDbType.Decimal, dto.Price);
            cmd.AddParam("@Uom", SqlDbType.NVarChar, dto.UnitOfMeasure);
            cmd.AddParam("@IsForSales", SqlDbType.Bit, dto.IsForSales);
            //cmd.AddParam("@IsDisabled", SqlDbType.Bit, dto.IsDisabled);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));

            // Prepare Items
            DataTable dt = ConstructItemTable();

            // Check if we have to add default item
            if (dto.ItemState == ItemState.New && dto.Items.Count == 0)
            {
                dt.Rows.Add(
                    -1, // ItemId
                    -1, // ItemGroupId
                    dto.ItemGroupName, // ItemName
                    "", // Barcode
                    0, // Quantity
                    dto.Cost, // Cost
                    dto.Price, // Price
                    false, // IsForSales
                    "N" // ItemState
                    );
            }
            else
            {
                if (dto.Items != null)
                {
                    for (int i = 0; i < dto.Items.Count; ++i)
                    {
                        // Create item with default values
                        var item = new ItemDto();
                        item.Barcode = "";
                        item.Quantity = 0;
                        item.Cost = 0;
                        item.Price = 0;

                        item = dto.Items[i];
                        dt.Rows.Add(
                            item.ItemId,
                            item.ItemGroupId,
                            item.ItemName,
                            item.Barcode,
                            item.Quantity,
                            item.Cost,
                            item.Price,
                            item.IsForSales,
                            item.ItemState);
                    }
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);

            BaseResponse ret = cmd.RunSpNonQuery("spItemGroupManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process ItemGroup for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse AddNewItemGroupInline(string itemGroupName)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemGroupName", SqlDbType.NVarChar, itemGroupName);

            BaseResponse ret = cmd.RunSpNonQuery("spItemGroupCreateInline");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not complete Inline ItemGroupCreation. BusinessId[{0}] ItemGroup[{1}]", businessId, itemGroupName));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructItemTable()
        {
            DataTable dt = new DataTable("Item");
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("ItemGroupId", typeof(long));
            dt.Columns.Add("ItemName", typeof(string));
            dt.Columns.Add("Barcode", typeof(string));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("Cost", typeof(decimal));
            dt.Columns.Add("Price", typeof(decimal));
            dt.Columns.Add("IsForSales", typeof(bool));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region ItemCategory

        public static GenericResponse<IList<OptionItem>> RetrieveItemCategoryList(bool fAddUndefinedItem = true)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemCategoryListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                /*if (fAddUndefinedItem)
                    items.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });*/
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = (long)row["Value"];
                    item.Text = row["Text"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ItemCategoryList for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemCategoryDto>> RetrieveAllItemCategory()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemCategoryDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemCategoryAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemCategoryDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemCategoryDto icDto = new ItemCategoryDto()
                    {
                        ItemCategoryId = long.Parse(row["ItemCategoryId"].ToString()),
                        ItemCategoryName = row["ItemCategoryName"].ToString(),
                        ItemQuantity = decimal.Parse(row["ItemQuantity"].ToString())
                    };

                    tmpResult.Add(icDto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve All ItemCategory for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemCategoryItemDto>> RetrieveAllItemCategoryItem(long ItemCategoryId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemCategoryItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemCategoryId", SqlDbType.BigInt, ItemCategoryId);
            BaseResponse ret = cmd.RunSpQuery("spItemCategoryItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemCategoryItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemCategoryItemDto dto = new ItemCategoryItemDto();

                    dto.ItemGroupId = long.Parse(row["ItemGroupId"].ToString());
                    dto.ItemGroupCode = row["ItemGroupCode"].ToString();
                    dto.ItemBrandName = row["ItemBrandName"].ToString();
                    dto.ItemGroupName = row["ItemGroupName"].ToString();
                    dto.ItemQuantity = (decimal)row["ItemQuantity"];

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemCategoryItem for ItemCategoryId[{0}]", ItemCategoryId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ItemCategoryDto> RetrieveItemCategory(long ItemCategoryId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<ItemCategoryDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemCategoryId", SqlDbType.BigInt, ItemCategoryId);
            BaseResponse ret = cmd.RunSpQuery("spItemCategoryRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    ItemCategoryDto dto = new ItemCategoryDto();

                    dto.ItemCategoryId = long.Parse(row["ItemCategoryId"].ToString());
                    dto.ItemCategoryName = row["ItemCategoryName"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModify"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("ItemCategory[{0}] does not exist", ItemCategoryId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemCategory for CategoryId[{0}]", ItemCategoryId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageItemCategory(ItemCategoryDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemCategoryId", SqlDbType.BigInt, dto.ItemCategoryId);
            cmd.AddParam("@ItemCategoryName", SqlDbType.NVarChar, dto.ItemCategoryName);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));

            // Prepare Items
            DataTable dt = ConstructItemCategoryTable();

            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.ItemGroupId,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);

            BaseResponse ret = cmd.RunSpNonQuery("spItemCategoryManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process ItemGroup for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructItemCategoryTable()
        {
            DataTable dt = new DataTable("ItemCategoryItem");
            dt.Columns.Add("ItemGroupId", typeof(long));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region ItemBrand

        public static GenericResponse<IList<OptionItem>> RetrieveItemBrandList(bool fAddUndefinedItem = true)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemBrandListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                /*if (fAddUndefinedItem)
                    items.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });*/
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = (long)row["Value"];
                    item.Text = row["Text"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ItemBrandList for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemBrandDto>> RetrieveAllItemBrand()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemBrandDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemBrandAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemBrandDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemBrandDto ibDto = new ItemBrandDto()
                    {
                        ItemBrandId = long.Parse(row["ItemBrandId"].ToString()),
                        ItemBrandName = row["ItemBrandName"].ToString(),
                        ItemQuantity = (decimal)row["ItemQuantity"]
                    };

                    tmpResult.Add(ibDto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve All ItemBrand for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemBrandItemDto>> RetrieveAllItemBrandItem(long ItemBrandId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemBrandItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemBrandId", SqlDbType.BigInt, ItemBrandId);
            BaseResponse ret = cmd.RunSpQuery("spItemBrandItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemBrandItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemBrandItemDto dto = new ItemBrandItemDto();

                    dto.ItemGroupId = long.Parse(row["ItemGroupId"].ToString());
                    dto.ItemGroupCode = row["ItemGroupCode"].ToString();
                    dto.ItemGroupName = row["ItemGroupName"].ToString();
                    dto.ItemBrandName = row["ItemBrandName"].ToString();
                    dto.ItemCategoryName = row["ItemCategoryName"].ToString();
                    dto.ItemQuantity = (decimal)row["ItemQuantity"];

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemBrandItem for ItemBrandId[{0}]", ItemBrandId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ItemBrandDto> RetrieveItemBrand(long ItemBrandId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<ItemBrandDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemBrandId", SqlDbType.BigInt, ItemBrandId);
            BaseResponse ret = cmd.RunSpQuery("spItemBrandRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    ItemBrandDto dto = new ItemBrandDto();

                    dto.ItemBrandId = long.Parse(row["ItemBrandId"].ToString());
                    dto.ItemBrandName = row["ItemBrandName"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("ItemBrand[{0}] does not exist", ItemBrandId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemBrand for BrandId[{0}]", ItemBrandId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageItemBrand(ItemBrandDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemBrandId", SqlDbType.BigInt, dto.ItemBrandId);
            cmd.AddParam("@ItemBrandName", SqlDbType.NVarChar, dto.ItemBrandName);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));

            // Prepare Items
            DataTable dt = ConstructItemBrandTable();

            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.ItemGroupId,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);

            BaseResponse ret = cmd.RunSpNonQuery("spItemBrandManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process ItemGroup for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructItemBrandTable()
        {
            DataTable dt = new DataTable("ItemBrandItem");
            dt.Columns.Add("ItemGroupId", typeof(long));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region ItemAdjustment

        public static GenericResponse<IList<OptionItem>> RetrieveItemAdjustmentReasonList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemAdjustmentReasonListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ItemAdjustmentReasonList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemAdjustmentDto>> RetrieveAllItemAdjustment()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemAdjustmentDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemAdjustmentAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemAdjustmentDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemAdjustmentDto dto = new ItemAdjustmentDto()
                    {
                        AdjustmentId = long.Parse(row["AdjustmentId"].ToString()),
                        AdjustmentName = row["AdjustmentName"].ToString(),
                        //AdjustmentReasonId = long.Parse(row["AdjustmentReasonId"].ToString()),
                        AdjustmentNo = row["AdjustmentNo"].ToString(),
                        //Note = row["Note"].ToString(),
                        Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]),
                        Status = row["Status"].ToString(),
                        Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"])
                    };

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve All ItemAdjustment for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemAdjustmentItemDto>> RetrieveAllItemAdjustmentItem(long AdjustmentId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemAdjustmentItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@AdjustmentId", SqlDbType.BigInt, AdjustmentId);
            BaseResponse ret = cmd.RunSpQuery("spItemAdjustmentItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemAdjustmentItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemAdjustmentItemDto dto = new ItemAdjustmentItemDto();

                    dto.AdjustmentItemId = long.Parse(row["AdjustmentItemId"].ToString());
                    dto.AdjustmentId = long.Parse(row["AdjustmentId"].ToString());
                    dto.ItemId = long.Parse(row["ItemId"].ToString());
                    dto.QuantityBefore = (decimal)row["QuantityBefore"];
                    dto.QuantityAfter = (decimal)row["QuantityAfter"];
                    dto.Difference = (decimal)row["Difference"];

                    dto.ItemFullName = row["ItemFullName"].ToString();

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemAdjustmentItem for AdjustmentId[{0}]", AdjustmentId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ItemAdjustmentDto> RetrieveItemAdjustment(long AdjustmentId)
        {
            var result = new GenericResponse<ItemAdjustmentDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@AdjustmentId", SqlDbType.BigInt, AdjustmentId);
            BaseResponse ret = cmd.RunSpQuery("spItemAdjustmentRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    ItemAdjustmentDto dto = new ItemAdjustmentDto();

                    dto.AdjustmentId = long.Parse(row["AdjustmentId"].ToString());
                    dto.AdjustmentName = row["AdjustmentName"].ToString();
                    dto.AdjustmentReasonId = row["AdjustmentReasonId"] == (object)DBNull.Value ? -1 : long.Parse(row["AdjustmentReasonId"].ToString());
                    dto.AdjustmentNo = row["AdjustmentNo"].ToString();
                    dto.Note = row["Note"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.Status = row["Status"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("ItemAdjustment[{0}] does not exist", AdjustmentId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemAdjustment for AdjustmentId[{0}]", AdjustmentId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageItemAdjustment(ItemAdjustmentDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("as", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} Adjustment[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.AdjustmentId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@AdjustmentId", SqlDbType.BigInt, dto.AdjustmentId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@AdjustmentName", SqlDbType.NVarChar, dto.AdjustmentName);
            cmd.AddParam("@AdjustmentReasonId", SqlDbType.BigInt,
                dto.AdjustmentReasonId != -1 ? dto.AdjustmentReasonId : (object)DBNull.Value);
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            cmd.AddParam("@Date", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Date));
            //cmd.AddParam("@Status", SqlDbType.VarChar, dto.Status);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.VarChar, userId);

            // Prepare Items
            DataTable dt = ConstructItemAdjustmentItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.AdjustmentItemId,
                        dto.AdjustmentId,
                        item.ItemId,
                        item.QuantityBefore,
                        item.QuantityAfter,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spItemAdjustmentManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process ItemAdjustment for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructItemAdjustmentItemTable()
        {
            DataTable dt = new DataTable("ItemAdjustmentItem");
            dt.Columns.Add("ItemAdjustmentItemId", typeof(long));
            dt.Columns.Add("ItemAdjustmentId", typeof(long));
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("QuantityBefore", typeof(decimal));
            dt.Columns.Add("QuantityAfter", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        public static BaseResponse VoidItemAdjustment(long AdjustmentId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@AdjustmentId", SqlDbType.BigInt, AdjustmentId);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            BaseResponse ret = cmd.RunSpNonQuery("spItemAdjustmentVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void ItemAdjustment. BusinessId[{0}] AdjustmentId[{1}]", businessId, AdjustmentId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region ItemTransfer

        public static GenericResponse<IList<ItemTransferDto>> RetrieveAllItemTransfer()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemTransferDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemTransferAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemTransferDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemTransferDto dto = new ItemTransferDto()
                    {
                        TransferId = long.Parse(row["TransferId"].ToString()),
                        TransferName = row["TransferName"].ToString(),
                        DateSent = DateTimeHelper.ConvertToDateString((DateTime)row["DateSent"]),
                        TransferStatusName = row["TransferStatusTypeName"].ToString(),
                        //Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"])
                    };

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve All ItemTransfer for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemTransferItemDto>> RetrieveAllItemTransferItem(long TransferId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemTransferItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@TransferId", SqlDbType.BigInt, TransferId);
            BaseResponse ret = cmd.RunSpQuery("spItemTransferItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemTransferItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemTransferItemDto dto = new ItemTransferItemDto();

                    dto.ItemId = long.Parse(row["ItemId"].ToString());
                    dto.TransferItemId = long.Parse(row["TransferItemId"].ToString());
                    dto.TransferId = long.Parse(row["TransferId"].ToString());
                    //dto.ItemId = long.Parse(row["ItemId"].ToString());
                    dto.Quantity = (decimal)row["Quantity"];

                    dto.ItemFullName = row["ItemFullName"].ToString();

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemTransferItem for TransferId[{0}]", TransferId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ItemTransferDto> RetrieveItemTransfer(long TransferId)
        {
            var result = new GenericResponse<ItemTransferDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@TransferId", SqlDbType.BigInt, TransferId);
            BaseResponse ret = cmd.RunSpQuery("spItemTransferRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    ItemTransferDto dto = new ItemTransferDto();

                    dto.TransferId = long.Parse(row["TransferId"].ToString());
                    dto.TransferName = row["TransferName"].ToString();
                    dto.Reference = row["Reference"].ToString();
                    dto.DateSent = DateTimeHelper.ConvertToDateString((DateTime)row["DateSent"]);
                    dto.SourceBusinessId = long.Parse(row["SourceBusinessId"].ToString());
                    dto.SourceBusinessName = row["SourceBusinessName"].ToString();
                    dto.TargetBusinessId = long.Parse(row["TargetBusinessId"].ToString());
                    dto.TargetBusinessName = row["TargetBusinessName"].ToString();
                    dto.TransferStatusId = row["TransferStatusTypeId"] == (object)DBNull.Value ? -1 : long.Parse(row["TransferStatusTypeId"].ToString());
                    dto.TransferStatusName = row["TransferStatusTypeName"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("ItemTransfer[{0}] does not exist", TransferId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemTransfer for TransferId[{0}]", TransferId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageItemTransfer(ItemTransferDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("ts", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} Transfer[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.TransferId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@TransferId", SqlDbType.BigInt, dto.TransferId);
            cmd.AddParam("@TransferName", SqlDbType.NVarChar, dto.TransferName);
            cmd.AddParam("@SourceBusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@TargetBusinessId", SqlDbType.BigInt, dto.TargetBusinessId);
            cmd.AddParam("@Reference", SqlDbType.NVarChar, dto.Reference);
            cmd.AddParam("@DateSent", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.DateSent));
            cmd.AddParam("@DateReceived", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.DateReceived));
            cmd.AddParam("@TransferStatusTypeId", SqlDbType.BigInt,
                dto.TransferStatusId != -1 ? dto.TransferStatusId : (object)DBNull.Value);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.VarChar, userId);

            // Prepare Items
            DataTable dt = ConstructItemTransferItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.TransferItemId,
                        dto.TransferId,
                        item.ItemId,
                        item.Quantity,
                        item.ItemState);
                }
            }
            cmd.AddParam("@SubItems", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spItemTransferManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process ItemTransfer for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructItemTransferItemTable()
        {
            DataTable dt = new DataTable("ItemTransferItem");
            dt.Columns.Add("ItemTransferItemId", typeof(long));
            dt.Columns.Add("ItemTransferId", typeof(long));
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region ItemReceiveTransfer

        public static GenericResponse<IList<ItemReceiveTransferDto>> RetrieveAllItemReceiveTransfer()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemReceiveTransferDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemReceiveTransferAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemReceiveTransferDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemReceiveTransferDto dto = new ItemReceiveTransferDto()
                    {
                        TransferId = long.Parse(row["TransferId"].ToString()),
                        TransferName = row["TransferName"].ToString(),
                        DateSent = DateTimeHelper.ConvertToDateString((DateTime)row["DateSent"]),
                        TransferStatusName = row["TransferStatusTypeName"].ToString(),
                    };

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve All ItemReceiveTransfer for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemReceiveTransferItemDto>> RetrieveAllItemReceiveTransferItem(long ReceiveId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemReceiveTransferItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@TransferId", SqlDbType.BigInt, ReceiveId);
            BaseResponse ret = cmd.RunSpQuery("spItemReceiveTransferItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemReceiveTransferItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemReceiveTransferItemDto dto = new ItemReceiveTransferItemDto();

                    dto.TransferItemId = long.Parse(row["TransferItemId"].ToString());
                    dto.TransferId = long.Parse(row["TransferId"].ToString());
                    //dto.ItemId = long.Parse(row["ItemId"].ToString());
                    dto.Quantity = (decimal)row["Quantity"];

                    dto.ItemFullName = row["ItemFullName"].ToString();

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemReceiveTransferItem for TransferId[{0}]", ReceiveId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ItemReceiveTransferDto> RetrieveItemReceiveTransfer(long ReceiveId)
        {
            var result = new GenericResponse<ItemReceiveTransferDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@TransferId", SqlDbType.BigInt, ReceiveId);
            BaseResponse ret = cmd.RunSpQuery("spItemReceiveTransferRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    ItemReceiveTransferDto dto = new ItemReceiveTransferDto();

                    dto.TransferId = long.Parse(row["TransferId"].ToString());
                    dto.TransferName = row["TransferName"].ToString();
                    dto.Reference = row["Reference"].ToString();
                    dto.DateSent = DateTimeHelper.ConvertToDateString((DateTime)row["DateSent"]);
                    dto.SourceBusinessId = long.Parse(row["SourceBusinessId"].ToString());
                    dto.SourceBusinessName = row["SourceBusinessName"].ToString();
                    dto.TargetBusinessId = long.Parse(row["TargetBusinessId"].ToString());
                    dto.TargetBusinessName = row["TargetBusinessName"].ToString();
                    dto.TransferStatusId = row["TransferStatusTypeId"] == (object)DBNull.Value ? -1 : long.Parse(row["TransferStatusTypeId"].ToString());
                    dto.TransferStatusName = row["TransferStatusTypeName"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("ItemReceiveTransfer[{0}] does not exist", ReceiveId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemReceiveTransfer for TransferId[{0}]", ReceiveId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageItemReceiveTransfer(ItemReceiveTransferDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("rts", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} ReceiveTransfer[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.TransferId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@TransferId", SqlDbType.BigInt, dto.TransferId);
            cmd.AddParam("@TransferName", SqlDbType.NVarChar, dto.TransferName);
            cmd.AddParam("@SourceBusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@TargetBusinessId", SqlDbType.BigInt, dto.TargetBusinessId);
            cmd.AddParam("@Reference", SqlDbType.NVarChar, dto.Reference);
            cmd.AddParam("@DateSent", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.DateSent));
            cmd.AddParam("@DateReceived", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.DateReceived));
            cmd.AddParam("@TransferStatusTypeId", SqlDbType.BigInt,
                dto.TransferStatusId != -1 ? dto.TransferStatusId : (object)DBNull.Value);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.VarChar, userId);

            // Prepare Items
            DataTable dt = ConstructItemReceiveTransferItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.TransferItemId,
                        dto.TransferId,
                        item.ItemId,
                        item.Quantity,
                        item.ItemState);
                }
            }
            cmd.AddParam("@SubItems", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spItemReceiveTransferManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process ItemReceiveTransfer for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructItemReceiveTransferItemTable()
        {
            DataTable dt = new DataTable("ItemReceiveItem");
            dt.Columns.Add("ItemTransferItemId", typeof(long));
            dt.Columns.Add("ItemTransferId", typeof(long));
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region ItemTransferStatus

        public static GenericResponse<IList<OptionItem>> RetrieveItemTransferStatusList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemTransferStatusListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ItemTransferStatusList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveItemTransferAllStatusList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemTransferStatusListAllRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ItemTransferStatusListAll"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveReceiveItemTransferStatusList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spReceiveItemTransferStatusListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ReceiveItemTransferStatusList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }
        #endregion

        #region ItemImage
        public static GenericResponse<ItemGroupImageDto> LoadItemImage(long ItemGroupId)
        {
            // A item image loading step
            // 1) gets an ImagePath from spGetImagePath
            // 2) loads an image file from the ImagePath
            // 3) converts the image file to a Base64 string
            // 4) returns the Base64 string

            var result = new GenericResponse<ItemGroupImageDto>();
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            string fileName = "";
            var dto = new ItemGroupImageDto();

            // 1)
            fileName = GetImagePath(ItemGroupId);
            dto.ItemFileName = fileName;

            string path = HttpContext.Current.Server.MapPath("~/Upload/Image");
            string fullfilename = Path.Combine(path, fileName);

            // 2)
            // 3)
            ImageHelper ih = new ImageHelper();
            if (File.Exists(fullfilename))
                dto.UrlData = ih.LoadImage(fullfilename);

            // 4)
            result.Succeed();
            result.ResponseItem = dto;

            return result;
        }

        public static BaseResponse SaveItemImage(ItemGroupImageDto dto)
        {
            // A item image saving step
            // 1) gets an ImagePath from spGetImagePath.
            // 2) create new image file name from the ImagePath.
            //      if an ItemImageDto.ItemFileName == the ImagePath, then ImagePath will separate to get a origin file name and creating the image file name
            //      else the image file name will create from a image path format, <business id>-<YYYYMMDD>-<HHmmSS>-<ItemImageDto.ItemFileName>.jpg
            // 3) delete a image file of the ImagePath
            // 4) converts a Base63 string to new image file
            // 5) save the new image file to disk
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            string newFileName = "";
            string oldFileName = "";
            var result = new BaseResponse();

            // 1)
            string path = HttpContext.Current.Server.MapPath("~/Upload/Image");
            DirectoryInfo di = Directory.CreateDirectory(path);

            // 2)
            // create the image file name with the format image file name <business id>_<YYYYMMDD>_<itemFileName>.jpg
            string itemFileName = Regex.Match(dto.ItemFileName, @"(?<businessId>.+?)_(?<itemDate>.+?)_(?<itemTime>.+?)_(?<itemFileName>.+)\.").Groups["itemFileName"].Value;
            if (itemFileName == "")
                itemFileName = Path.GetFileNameWithoutExtension(dto.ItemFileName);
            DateTime now = DateTime.Now;
            string itemdatetime = now.ToString("yyyyMMdd_HHmmss");
            newFileName = String.Format("{0}_{1}_{2}.jpg",
                                 businessId, itemdatetime, itemFileName);
            string newFullFileName = Path.Combine(path, newFileName);

            // 3)
            oldFileName = GetImagePath(dto.ItemGroupId);
            string oldFullFileName = Path.Combine(path, oldFileName);
            try
            {
                if (File.Exists(oldFullFileName))
                    File.Delete(oldFullFileName);
            }
            catch (IOException e)
            {
            }

            // 4)
            ImageHelper ih = new ImageHelper();
            ih.SaveImage(newFullFileName, dto.UrlData);

            // 5)
            SetImagePath(dto.ItemGroupId, newFileName);

            result.Succeed();
            //result.ResponseItem = items;

            return result;
        }

        public static string GetImagePath(long ItemGroupId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            string fileName = "";

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemGroupId", SqlDbType.BigInt, ItemGroupId);

            BaseResponse ret = cmd.RunSpQuery("spGetItemPath");
            if (ret.Success)
            {
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    fileName = row["ImagePath"].ToString();
                }
            }
            return fileName;
        }

        public static bool SetImagePath(long ItemGroupId, string fileName)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemGroupId", SqlDbType.BigInt, ItemGroupId);
            cmd.AddParam("@ImagePath", SqlDbType.NVarChar, fileName);
            cmd.AddParam("@ItemState", SqlDbType.Char, 'M');
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            BaseResponse ret = cmd.RunSpQuery("spSetImagePath");
            if (ret.Success)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region CurrentStock

        public static GenericResponse<IList<OptionItem>> RetrieveItemGroupList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spItemGroupListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();

                // Add undefined item by default
                //items.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.SelectingText });

                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = (long)row["Value"];
                    item.Text = row["Text"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve ItemGroupList for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ItemCurrentStockDto>> RetrieveCurrentStock(ItemCurrentStockSelectionDto selection)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ItemCurrentStockDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemGroupId", SqlDbType.BigInt, selection.ItemGroupId == -1 ? (object)DBNull.Value : selection.ItemGroupId);
            cmd.AddParam("@ItemBrandId", SqlDbType.BigInt, selection.ItemBrandId == -1 ? (object)DBNull.Value : selection.ItemBrandId);
            cmd.AddParam("@ItemCategoryId", SqlDbType.BigInt, selection.ItemCategoryId == -1 ? (object)DBNull.Value : selection.ItemCategoryId);
            BaseResponse ret = cmd.RunSpQuery("spCurrentStockRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemCurrentStockDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemCurrentStockDto dto = new ItemCurrentStockDto();

                    dto.ItemGroupCode = row["ItemGroupCode"].ToString();
                    dto.ItemCategoryName = row["ItemCategoryName"].ToString();
                    dto.ItemBrandName = row["ItemBrandName"].ToString();
                    dto.Quantity = (decimal)row["Quantity"];


                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemCurrentStock for BusinessId[{0}]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region ItemCreation

        public static GenericResponse<IList<OptionItem>> AddNewItem(ItemDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            //var result = new GenericResponse<OptionItem>();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemGroupName", SqlDbType.NVarChar, dto.ItemGroupName);
            cmd.AddParam("@ItemGroupCode", SqlDbType.NVarChar, dto.ItemGroupCode);
            cmd.AddParam("@ItemName", SqlDbType.NVarChar, dto.ItemName);
            cmd.AddParam("@ItemState", SqlDbType.Char, 'N');
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@ItemId", SqlDbType.BigInt, -1, ParameterDirection.Output);

            BaseResponse ret = cmd.RunSpNonQuery("spItemGroupCreation");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                var item = new OptionItem();

                item.Value = long.Parse(cmd.GetParam("@ItemId"));
                item.Text = ItemDto.getOptionName(dto.ItemGroupName, dto.ItemGroupCode, dto.ItemName);

                tmpResult.Add(item);

                result.Succeed();
                //result.ResponseItem = item;
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not complete ItemCreation. BusinessId[{0}] ItemCategory[{1}] ItemBrand[{2}] ItemGroup[{3}]",
                    businessId, dto.ItemCategoryName, dto.ItemBrandName, dto.ItemGroupName));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        public static GenericResponse<ItemGroupInfoDto> RetrieveItemGroupInfo(long ItemGroupId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<ItemGroupInfoDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemGroupId", SqlDbType.BigInt, ItemGroupId);

            BaseResponse ret = cmd.RunSpQuery("spItemGroupInfoRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    var dto = new ItemGroupInfoDto();

                    dto.ItemGroupId = (long)row["ItemGroupId"];
                    dto.ItemGroupCode = row["ItemGroupCode"].ToString();
                    dto.ItemGroupName = row["ItemGroupName"].ToString();
                    dto.ItemBrandName = row["ItemBrandName"].ToString();
                    dto.ItemQuantity = (decimal)row["ItemQuantity"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("ItemGroupId[{0}] does not exist", ItemGroupId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemGroupId[{0}]", ItemGroupId));
                Logger.SessionError(result.StatusText);
            }
            return result;
        }

        public static GenericResponse<string> RetrieveItemView(long itemId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<string>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ItemId", SqlDbType.BigInt, itemId);

            BaseResponse ret = cmd.RunSpQuery("spItemViewRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    string data = ""; // data = <# remaining item in stock>|<last sale price>

                    data += (decimal)row["NumRemaining"];
                    data += "|";
                    data += (decimal)row["PriceLastSale"];

                    result.Succeed();
                    result.ResponseItem = data;
                }
                else
                {
                    result.Fail(string.Format("ItemId[{0}] does not exist", itemId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ItemView for ItemId[{0}]", itemId));
                Logger.SessionError(result.StatusText);
            }
            return result;
        }
    }
}