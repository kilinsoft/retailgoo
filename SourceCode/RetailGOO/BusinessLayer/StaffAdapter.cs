﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.EnumType;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class StaffAdapter
    {
        public static BaseResponse SendInvitation(BusinessStaffDto dto)
        {
            //var resp = Business.ValidateBusinessStaff();
            //if (!resp.Success)
            //    return resp;
            var resp = new BaseResponse();

            // Validate if the doer is manager or admin role
            var role = SessionHelper.GetCurrentStaff().Role;
            if (role != StaffRole.Manager && role != StaffRole.Admin)
            {
                resp.Fail("Manager level or above is required");
                return resp;
            }

            long businessId = SessionHelper.GetCurrentBusinessId();

            // Check if the username or the staff email already exist for this business
            bool fStaffExist = false;
            SqlCommandHelper cmdCheck = new SqlCommandHelper();
            cmdCheck.AddParam("@StaffId", SqlDbType.BigInt, DBNull.Value);
            cmdCheck.AddParam("@Username", SqlDbType.VarChar, dto.Target);
            cmdCheck.AddParam("@UserEmail", SqlDbType.VarChar, DBNull.Value);

            BaseResponse retCheck = cmdCheck.RunSpQuery("spStaffInfoRetrieve");
            if (retCheck.Success)
            {
                if (cmdCheck.DataResult.Rows.Count > 0) // User already exist in the staff list
                    fStaffExist = true;
            }
            else
            {
                resp.Fail(string.Format("Could not CheckStaffExist for BusinessId[ {0} ]", businessId));
                resp.SetDebugText(retCheck.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", resp.StatusText, retCheck.StatusText));
                return resp;
            }

            var targetEmail = dto.Target;
            string guid = Guid.NewGuid().ToString();
            DateTime dateNow = DateTime.UtcNow;
            TimeSpan expirationTime = new TimeSpan(SystemSetting.ExpirationDay, 0, 0, 0);
            DateTime dateGuidExpired = dateNow.Add(expirationTime);

            string targetStatus = StaffStatusString.Waiting;
            if (dto.StaffId != -1) // Exist retailgoo user, set the status = "active"
            {
                targetStatus = StaffStatusString.Active;
            }

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@StaffId", SqlDbType.BigInt, dto.StaffId);

            if (dto.UserId == -1)
            {
                cmd.AddParam("@StaffUserId", SqlDbType.BigInt, DBNull.Value);
                cmd.AddParam("@Email", SqlDbType.VarChar, targetEmail);
            }
            else {
                cmd.AddParam("@StaffUserId", SqlDbType.BigInt, dto.UserId);

                string email = "";
                var userRsp = RetailGOO.BusinessLayer.Identity.GetUserProfile(dto.UserId);
                if (userRsp.Success && userRsp.ResponseItem != null)
                    email = userRsp.ResponseItem.Email;

                cmd.AddParam("@Email", SqlDbType.VarChar, email);
            }


            if (fStaffExist)
            {
                cmd.AddParam("@ItemState", SqlDbType.VarChar, ItemState.Modified);
            }
            else
            {
                cmd.AddParam("@ItemState", SqlDbType.VarChar, ItemState.New);
            }

            cmd.AddParam("@Title", SqlDbType.VarChar, DBNull.Value);
            cmd.AddParam("@Firstname", SqlDbType.NVarChar, "");
            cmd.AddParam("@Lastname", SqlDbType.NVarChar, "");
            cmd.AddParam("@Birthdate", SqlDbType.Date, DBNull.Value);
            cmd.AddParam("@Gender", SqlDbType.Char, DBNull.Value);
            cmd.AddParam("@Identification", SqlDbType.NVarChar, "");
            cmd.AddParam("@Phone", SqlDbType.NVarChar, "");
            cmd.AddParam("@Fax", SqlDbType.NVarChar, "");
            cmd.AddParam("@FacebookId", SqlDbType.NVarChar, "");
            cmd.AddParam("@Address1", SqlDbType.NVarChar, "");
            cmd.AddParam("@Address2", SqlDbType.NVarChar, "");
            cmd.AddParam("@Address3", SqlDbType.NVarChar, "");
            cmd.AddParam("@City", SqlDbType.NVarChar, "");
            cmd.AddParam("@CountryId", SqlDbType.TinyInt, DBNull.Value);
            cmd.AddParam("@ZipCode", SqlDbType.NVarChar, "");
            cmd.AddParam("@Role", SqlDbType.VarChar, dto.Role);
            cmd.AddParam("@Status", SqlDbType.VarChar, targetStatus);
            cmd.AddParam("@Guid", SqlDbType.VarChar, guid);
            cmd.AddParam("@DateGuidExpired", SqlDbType.DateTime, dateGuidExpired);

            cmd.AddParam("@UserId", SqlDbType.BigInt, SessionHelper.GetCurrentUserId());

            BaseResponse ret = cmd.RunSpNonQuery("spStaffInfoManage");
            if (ret.Success)
            {
                resp.Succeed();
            }
            else
            {
                resp.Fail(string.Format("Could not SendInvitation for BusinessId[ {0} ]", businessId));
                resp.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", resp.StatusText, ret.StatusText));
            }

            //else
            //{
            //    resp.Fail(string.Format("The staff already exists"));
            //    Logger.SessionError(string.Format("{0}.", resp.StatusText));
            //}

            // Send Invitation Email
            if (resp.Success)
            {
                string emailContent = EmailHelper.GenerateInvitationEmailString(SessionHelper.GetCurrentBusinessName(), dto.Role, guid);
                resp = EmailHelper.SendEmail(targetEmail, emailContent);
            }

            return resp;
        }

        public static GenericResponse<Staff> RetrieveCurrentStaffInfo()
        {
            var rsp = new GenericResponse<Staff>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@UserId", SqlDbType.VarChar, SessionHelper.GetCurrentUserId());
            var ret = cmd.RunSpQuery("spStaffRetrieve");
            if (ret.Success && cmd.DataResult.Rows.Count > 0)
            {
                DataRow row = cmd.DataResult.Rows[0];
                Staff staff = new Staff();
                staff.StaffId = long.Parse(row["StaffId"].ToString());
                staff.BusinessName = row["BusinessName"].ToString();
                staff.Role = Staff.GetRoleFromString(row["Role"].ToString());
                staff.Status = Staff.GetStatusFromString(row["Status"].ToString());
                staff.Permission = new RolePermission(row["RolePermissionParameters"].ToString());
                rsp.ResponseItem = staff;
                rsp.Succeed();
            }
            else
            {
                rsp.Fail(
                    string.Format(
                        "Invalid Staff. BusinessId[{0}] UserId[{1}]",
                        SessionHelper.GetCurrentBusinessId(),
                        SessionHelper.GetCurrentUserId()));
                Logger.SessionError(rsp.GetFullText());
            }
            return rsp;
        }
        public static GenericResponse<UserProfileDto> RetrieveStaffInfo(long staffId)
        {
            var rsp = new GenericResponse<UserProfileDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@StaffId", SqlDbType.VarChar, staffId);
            cmd.AddParam("@Username", SqlDbType.VarChar, DBNull.Value);
            cmd.AddParam("@UserEmail", SqlDbType.VarChar, DBNull.Value);
            var ret = cmd.RunSpQuery("spStaffInfoRetrieve");
            if (ret.Success && cmd.DataResult.Rows.Count > 0)
            {
                DataRow row = cmd.DataResult.Rows[0];
                var user = new UserProfileDto();

                user.StaffId = staffId;
                user.UserId = long.Parse(row["UserId"].ToString());
                user.Title = row["Title"] != DBNull.Value ? row["Title"].ToString() : "";
                user.Firstname = row["Firstname"].ToString();
                user.Lastname = row["Lastname"].ToString();
                user.Birthdate = row["Birthdate"] != DBNull.Value ?
                    DateTimeHelper.ConvertToDateString((DateTime)row["Birthdate"]) : null;
                user.Gender = row["Gender"] != DBNull.Value ? row["Gender"].ToString() : "";
                user.Phone = row["Phone"].ToString();
                user.Fax = row["Fax"].ToString();
                user.Email = row["Email"].ToString();
                user.Identification = row["Identification"].ToString();
                user.Guid = row["Guid"].ToString();
                user.DateGuidExpired = row["DateGuidExpired"] != DBNull.Value ?
                    DateTimeHelper.ConvertToDateString((DateTime)row["DateGuidExpired"]) : "";
                user.FacebookId = row["FacebookId"].ToString();
                user.Address1 = row["Address1"].ToString();
                user.Address2 = row["Address2"].ToString();
                user.Address3 = row["Address3"].ToString();
                user.City = row["City"].ToString();
                user.CountryId = row["CountryId"] != DBNull.Value ? long.Parse(row["CountryId"].ToString()) : -1;
                user.CountryName = row["CountryName"] != DBNull.Value ? row["CountryName"].ToString() : "";
                user.ZipCode = row["ZipCode"].ToString();
                user.Role = row["Role"].ToString();
                user.Status = row["Status"].ToString();

                rsp.ResponseItem = user;
                rsp.Succeed();
            }
            else
            {
                rsp.Fail(
                    string.Format(
                        "Invalid Staff. BusinessId[{0}] StaffId[{1}]",
                        SessionHelper.GetCurrentBusinessId(),
                        staffId));
                Logger.SessionError(rsp.GetFullText());
            }
            return rsp;
        }
        public static BaseResponse ManageStaffInfo(UserProfileDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            // Prevent changing of self role
            if (dto.StaffId == SessionHelper.GetCurrentStaff().StaffId && 
                Staff.GetRoleFullname(dto.Role) != SessionHelper.GetCurrentStaff().Role.ToString())
            {
                result.Fail("Self role changing is prohibited");
                return result;
            }
            // Prevent manager users to change role of admin users
            if (SessionHelper.GetCurrentStaff().Role == Staff.GetRoleFromString(StaffRoleString.Manager))
            {
                var targetStaffInfo = RetrieveStaffInfo(dto.StaffId);
                if (targetStaffInfo.ResponseItem != null)
                {
                    if (targetStaffInfo.ResponseItem.Role != dto.Role && targetStaffInfo.ResponseItem.Role == StaffRoleString.Admin)
                    {
                        result.Fail("Changing the role of admin users is prohibited");
                        return result;
                    }
                }
            }

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@StaffId", SqlDbType.BigInt, dto.StaffId);
            cmd.AddParam("@StaffUserId", SqlDbType.BigInt, dto.UserId);
            cmd.AddParam("@Title", SqlDbType.VarChar, dto.Title);
            cmd.AddParam("@Firstname", SqlDbType.NVarChar, dto.Firstname);
            cmd.AddParam("@Lastname", SqlDbType.NVarChar, dto.Lastname);
            cmd.AddParam("@Birthdate", SqlDbType.NVarChar, dto.Birthdate != "" ?
                DateTimeHelper.ConvertToDateTime(dto.Birthdate) : (object)DBNull.Value);
            cmd.AddParam("@Gender", SqlDbType.VarChar, dto.Gender != "" ?
                dto.Gender : (object)DBNull.Value);
            cmd.AddParam("@Identification", SqlDbType.NVarChar, dto.Identification);
            cmd.AddParam("@Phone", SqlDbType.NVarChar, dto.Phone);
            cmd.AddParam("@Fax", SqlDbType.NVarChar, dto.Fax);
            cmd.AddParam("@Email", SqlDbType.NVarChar, dto.Email);
            cmd.AddParam("@FacebookId", SqlDbType.NVarChar, dto.FacebookId);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, dto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, dto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, dto.Address3);
            cmd.AddParam("@City", SqlDbType.NVarChar, dto.City);
            cmd.AddParam("@CountryId", SqlDbType.TinyInt, dto.CountryId != -1 ? dto.CountryId : (object)DBNull.Value);
            cmd.AddParam("@ZipCode", SqlDbType.NVarChar, dto.ZipCode);
            cmd.AddParam("@Role", SqlDbType.VarChar, dto.Role);
            cmd.AddParam("@Status", SqlDbType.VarChar, dto.Status);
            cmd.AddParam("@Guid", SqlDbType.VarChar, "");
            cmd.AddParam("@DateGuidExpired", SqlDbType.DateTime, DBNull.Value);
            cmd.AddParam("@ItemState", SqlDbType.VarChar, dto.ItemState);

            BaseResponse ret = cmd.RunSpNonQuery("spStaffInfoManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process StaffInfo for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse UpdateStaffRoleAndStatus(IList<UserProfileDto> modifiedStaffList)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            // Validate if the doer is manager or admin role
            var role = SessionHelper.GetCurrentStaff().Role;
            if (role != StaffRole.Manager && role != StaffRole.Admin)
            {
                result.Fail("Manager level or above is required");
                return result;
            }

            if (modifiedStaffList.Count <= 0)
            {
                result.Fail("There is not any changes in Staff Role or Status");
                return result;
            }

            // Prevent the modification of self role and status
            foreach (var item in modifiedStaffList)
            {
                if (item.StaffId == SessionHelper.GetCurrentStaff().StaffId)
                {
                    result.Fail("Changing self role or status is prohibited.");
                    return result;
                }
            }

            DataTable dt = new DataTable("StaffRoleAndStatusItem");
            dt.Columns.Add("StaffId", typeof(long));
            dt.Columns.Add("Role", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("ItemState", typeof(string));
            for (int i = 0; i < modifiedStaffList.Count; ++i)
            {
                var item = modifiedStaffList[i];
                dt.Rows.Add(
                    item.StaffId,
                    item.Role,
                    item.Status,
                    item.ItemState);
            }

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@Items", SqlDbType.Structured, dt);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            BaseResponse ret = cmd.RunSpNonQuery("spStaffRoleAndStatusUpdate");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not update StaffRoleAndStatus for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }
    }
}