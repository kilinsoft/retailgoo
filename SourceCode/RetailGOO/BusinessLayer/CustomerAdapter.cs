﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class CustomerAdapter
    {
        public static GenericResponse<IList<CustomerDto>> RetrieveAllCustomer()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<CustomerDto>>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spCustomerAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<CustomerDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    CustomerDto custDto = new CustomerDto()
                    {
                        CustomerId = long.Parse(row["CustomerId"].ToString()),
                        CustomerName = row["CustomerName"].ToString(),
                        CustomerNo = row["CustomerNo"].ToString(),
                        CustomerGroupName = row["CustomerGroupName"].ToString(),
                        //TaxNo = row["TaxNo"].ToString(),
                        //DefaultPriceTypeId = int.Parse(row["DefaultPrice"].ToString()),
                        ContactPerson = row["ContactPerson"].ToString(),
                        //Email = row["Email"].ToString(),
                        Phone = row["Phone"].ToString(),
                        //Fax = row["Fax"].ToString(),
                        //CreditTerm = row["CreditTerm"].ToString(),
                        //CreditLimit = row["CreditLimit"].ToString(),
                        //AddressBilling1 = row["AddressBilling1"].ToString(),
                        //AddressBilling2 = row["AddressBilling2"].ToString(),
                        //AddressBilling3 = row["AddressBilling3"].ToString(),
                        //CityBilling = row["CityBilling"].ToString(),
                        //CountryBillingId = 
                        //    row["CountryBillingId"] != DBNull.Value ? 
                        //        int.Parse(row["CountryBillingId"].ToString()) : -1,
                        //ZipCodeBilling = row["ZipCodeBilling"].ToString(),
                        //RemarkBilling = row["RemarkBilling"].ToString(),
                        //AddressShipping1 = row["AddressShipping1"].ToString(),
                        //AddressShipping2 = row["AddressShipping2"].ToString(),
                        //AddressShipping3 = row["AddressShipping3"].ToString(),
                        //CityShipping = row["CityShipping"].ToString(),
                        //CountryShippingId = 
                        //    row["CountryShippingId"] != DBNull.Value ? 
                        //        int.Parse(row["CountryShippingId"].ToString()) : -1,
                        //ZipCodeShipping = row["ZipCodeShipping"].ToString(),
                        //RemarkShipping = row["RemarkShipping"].ToString(),
                        //Version = DateTimeHelper.ConvertToDateTimeString((DateTime) row["DateModified"])
                    };
                    tmpResult.Add(custDto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CustomerInfo for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }

            return result;
        }

        public static GenericResponse<CustomerDto> RetrieveCustomer(long custId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<CustomerDto>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@CustomerId", SqlDbType.BigInt, custId);
            BaseResponse ret = cmd.RunSpQuery("spCustomerRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<CustomerDto>();
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    CustomerDto custDto = new CustomerDto()
                    {
                        CustomerId = long.Parse(row["CustomerId"].ToString()),
                        CustomerName = row["CustomerName"].ToString(),
                        CustomerNo = row["CustomerNo"].ToString(),
                        CustomerGroupId =
                            row["CustomerGroupId"] != DBNull.Value ?
                                int.Parse(row["CustomerGroupId"].ToString()) : -1,
                        CustomerGroupName = row["CustomerGroupName"].ToString(),
                        TaxNo = row["TaxNo"].ToString(),
                        DefaultPriceTypeId =
                            row["DefaultPriceTypeId"] != DBNull.Value ?
                                int.Parse(row["DefaultPriceTypeId"].ToString()) : -1,
                        ContactPerson = row["ContactPerson"].ToString(),
                        Email = row["Email"].ToString(),
                        Phone = row["Phone"].ToString(),
                        Fax = row["Fax"].ToString(),
                        CreditTerm = row["CreditTerm"].ToString(),
                        CreditLimit = row["CreditLimit"].ToString(),
                        AddressBilling1 = row["AddressBilling1"].ToString(),
                        AddressBilling2 = row["AddressBilling2"].ToString(),
                        AddressBilling3 = row["AddressBilling3"].ToString(),
                        CityBilling = row["CityBilling"].ToString(),
                        CountryBillingId =
                            row["CountryBillingId"] != DBNull.Value ?
                                int.Parse(row["CountryBillingId"].ToString()) : -1,
                        ZipCodeBilling = row["ZipCodeBilling"].ToString(),
                        RemarkBilling = row["RemarkBilling"].ToString(),
                        AddressShipping1 = row["AddressShipping1"].ToString(),
                        AddressShipping2 = row["AddressShipping2"].ToString(),
                        AddressShipping3 = row["AddressShipping3"].ToString(),
                        CityShipping = row["CityShipping"].ToString(),
                        CountryShippingId =
                            row["CountryShippingId"] != DBNull.Value ?
                                int.Parse(row["CountryShippingId"].ToString()) : -1,
                        ZipCodeShipping = row["ZipCodeShipping"].ToString(),
                        RemarkShipping = row["RemarkShipping"].ToString(),
                        Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"])
                    };
                    result.Succeed();
                    result.ResponseItem = custDto;
                }
                else
                {
                    result.Fail(string.Format("CustomerId[{0}] does not exist", custId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve Customer. BusinessId[{0}] CustomerId[{1}]", businessId, custId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }

            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveCustomerGroupList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spCustomerGroupListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CustomerGroupList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveCustomerList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();
            var tmpResult = new List<OptionItem>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spCustomerListRetrieve");
            if (ret.Success)
            {
                tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.BusinessPartnerText });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var pair = new OptionItem()
                    {
                        Value = long.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(pair);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CustomerOptionItems for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageCustomer(CustomerDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("ci", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} CustomerInfo[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.CustomerId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            var result = new BaseResponse();
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();

            SqlCommandHelper cmd = new SqlCommandHelper();
            var ret = new BaseResponse();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@CustomerId", SqlDbType.BigInt, dto.CustomerId);
            cmd.AddParam("@CustomerName", SqlDbType.NVarChar, dto.CustomerName);
            cmd.AddParam("@CustomerNo", SqlDbType.NVarChar, dto.CustomerNo);
            cmd.AddParam("@CustomerGroupId", SqlDbType.TinyInt,
                dto.CustomerGroupId != -1 ? dto.CustomerGroupId : (object)DBNull.Value);
            cmd.AddParam("@TaxNo", SqlDbType.NVarChar, dto.TaxNo);
            cmd.AddParam("@DefaultPriceTypeId", SqlDbType.TinyInt, dto.DefaultPriceTypeId);
            cmd.AddParam("@ContactPerson", SqlDbType.NVarChar, dto.ContactPerson);
            cmd.AddParam("@Email", SqlDbType.VarChar, dto.Email);
            cmd.AddParam("@Phone", SqlDbType.NVarChar, dto.Phone);
            cmd.AddParam("@Fax", SqlDbType.NVarChar, dto.Fax);
            cmd.AddParam("@CreditTerm", SqlDbType.NVarChar, dto.CreditTerm);
            cmd.AddParam("@CreditLimit", SqlDbType.NVarChar, dto.CreditLimit);
            cmd.AddParam("@AddressBilling1", SqlDbType.NVarChar, dto.AddressBilling1);
            cmd.AddParam("@AddressBilling2", SqlDbType.NVarChar, dto.AddressBilling2);
            cmd.AddParam("@AddressBilling3", SqlDbType.NVarChar, dto.AddressBilling3);
            cmd.AddParam("@CityBilling", SqlDbType.NVarChar, dto.CityBilling);
            cmd.AddParam("@CountryBillingId", SqlDbType.TinyInt,
                dto.CountryBillingId != -1 ? dto.CountryBillingId : (object)DBNull.Value);
            cmd.AddParam("@ZipCodeBilling", SqlDbType.NVarChar, dto.ZipCodeBilling);
            cmd.AddParam("@RemarkBilling", SqlDbType.NVarChar, dto.RemarkBilling);
            cmd.AddParam("@AddressShipping1", SqlDbType.NVarChar, dto.AddressShipping1);
            cmd.AddParam("@AddressShipping2", SqlDbType.NVarChar, dto.AddressShipping2);
            cmd.AddParam("@AddressShipping3", SqlDbType.NVarChar, dto.AddressShipping3);
            cmd.AddParam("@CityShipping", SqlDbType.NVarChar, dto.CityShipping);
            cmd.AddParam("@CountryShippingId", SqlDbType.TinyInt,
                dto.CountryShippingId != -1 ? dto.CountryShippingId : (object)DBNull.Value);
            cmd.AddParam("@ZipCodeShipping", SqlDbType.NVarChar, dto.ZipCodeShipping);
            cmd.AddParam("@RemarkShipping", SqlDbType.NVarChar, dto.RemarkShipping);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime,
                dto.Version != string.Empty ? DateTimeHelper.ConvertToDateTime(dto.Version) : DateTime.Now);

            ret = cmd.RunSpNonQuery("spCustomerManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("An Error occurred. {0}", ret.StatusText));
                Logger.SessionError(string.Format("Error ManageCustomer. BusinessId[{0}] CustomerId[{1}] CustomerName[{2}] ItemState [{3}]",
                    businessId,
                    dto.CustomerId,
                    dto.CustomerName,
                    dto.ItemState.ToString()));
            }

            return result;
        }
    }
}