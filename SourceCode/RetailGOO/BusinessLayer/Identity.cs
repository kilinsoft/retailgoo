﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Transactions;

using System.IO;
using System.Net;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Util;
using RetailGOO.Util.EnumType;
using RetailGOO.Util.Constant;
using RetailGOO.Class;


namespace RetailGOO.BusinessLayer
{
    
    public static class Identity
    {
        public static BaseResponse CreateUser( 
            string username,
            string password,
            string email,
            bool fFbUser
            )
        {
            BaseResponse resp = new BaseResponse();
            bool fValid = true;

            if (fValid && !Validator.ValidateString(email, RegEx.EmailRegEx.Expression))
            {
                fValid = false;
                resp.Fail(RegEx.UsernameRegEx.Description);
            }
            // Validate password if not a facebook user
            if (!fFbUser)
            {
                if (fValid && !Validator.ValidatePassword(password))
                {
                    fValid = false;
                    resp.Fail(RegEx.PasswordRegEx.Description);
                }
            }
            // Return failure on validation errors
            if (!fValid)
            {
                Logger.SessionDebug(resp.StatusText);
            }
            else
            {
                try
                {
                    SqlCommandHelper cmd = new SqlCommandHelper();
                    cmd.AddParam("@Username", SqlDbType.VarChar, email);
                    if (fFbUser)
                    {
                        cmd.AddParam("@Password", SqlDbType.VarBinary, DBNull.Value);
                        cmd.AddParam("@Salt", SqlDbType.VarBinary, DBNull.Value);
                        cmd.AddParam("@Email", SqlDbType.VarChar, email.ToLower());
                        cmd.AddParam("@Guid", SqlDbType.VarChar, "");
                        cmd.AddParam("@DateGuidExpired", SqlDbType.DateTime, DBNull.Value);
                    }
                    else
                    {
                        byte[] salt = null;
                        byte[] hashedPwd = null;
                        try
                        {
                            if (password != null)
                            {
                                salt = Encryption.GenerateSalt();
                                hashedPwd = Encryption.GenerateHashedPassword(password, salt);
                            }
                        }
                        catch (Exception ex)
                        {
                            resp.Fail("Security module error", ex.ToString());
                            Logger.SysFatal(resp.GetFullText());
                            return resp;
                        }

                        string guid = Guid.NewGuid().ToString();

                        cmd.AddParam("@Password", SqlDbType.VarBinary, hashedPwd);
                        cmd.AddParam("@Salt", SqlDbType.VarBinary, salt);
                        cmd.AddParam("@Email", SqlDbType.VarChar, email);
                        cmd.AddParam("@Guid", SqlDbType.VarChar, guid);
                        cmd.AddParam("@DateGuidExpired", SqlDbType.DateTime, DateTime.Now.AddDays(TimeDefinition.UserCreationValidityPeriod));
                    }
                    cmd.AddParam("@IsDisabled", SqlDbType.Bit, false);
                    cmd.AddParam("@IsFacebookUser", SqlDbType.Bit, fFbUser);
                    BaseResponse ret = cmd.RunSpNonQuery("spUserCreate");
                    if (ret.Success)
                    {
                        resp.Succeed();
                    }
                    else
                    {
                        resp.Fail(string.Format("Could not create user, Email[ {0} ] IsFacebookUser[ {1} ]", email, fFbUser));
                        resp.SetDebugText(ret.StatusText);
                        Logger.SessionError(string.Format("{0}. {1}", resp.StatusText, ret.StatusText));
                    }

                    SignIn(email.ToLower());

                    //SqlCommandHelper cmd = new SqlCommandHelper();
                    //cmd.AddParam("@Username", SqlDbType.VarChar, email); // Use email as username
                    //cmd.AddParam("@Password", SqlDbType.VarBinary, hashedPwd);
                    //cmd.AddParam("@Salt", SqlDbType.VarBinary, salt);
                    //cmd.AddParam("@Email", SqlDbType.VarChar, email);
                    //cmd.AddParam("@Guid", SqlDbType.VarChar, guid);
                    //cmd.AddParam("@DateGuidExpired", SqlDbType.DateTime, DateTime.Now.AddDays(TimeDefinition.UserCreationValidityPeriod));
                    //cmd.AddParam("@IsActive", SqlDbType.Bit, fActive);
                    //cmd.AddParam("@IsFacebookUser", SqlDbType.Bit, fFbUser);
                    //cmd.AddParam("@Initial", SqlDbType.Char, "");
                    //cmd.AddParam("@Firstname", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Lastname", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Birthday", SqlDbType.Date, DBNull.Value);
                    //cmd.AddParam("@Gender", SqlDbType.Char, "");
                    //cmd.AddParam("@Phone", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Fax", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Website", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@FacebookUrl", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Address1", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Address2", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Address3", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@City", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@Country", SqlDbType.NVarChar, DBNull.Value);
                    //cmd.AddParam("@ZipCode", SqlDbType.NVarChar, "");
                    //cmd.AddParam("@DateCreated", SqlDbType.DateTime, DateTime.Now);
                    //cmd.AddParam("@DateModified", SqlDbType.DateTime, DateTime.Now);
                    //cmd.AddParam("@UpdatedBy", SqlDbType.VarChar, "<system>");

                    //resp = cmd.ExecuteInsert(DatabaseTable.User);

                    // Pro: ToDo : Send a confirmation email to the user.

                    return resp;  
                }
                catch (Exception ex)
                {
                    resp.Fail(string.Format("Could not Create User[ {0} ]", email), ex.ToString());
                    Logger.SysError(resp.GetFullText());
                    return resp;
                }
            }
            return resp;
        }
        public static bool ValidateUser(string username, string password)
        {
            bool ret = false;
            var rspUser = GetUserByUsername(username);
            if (rspUser.Success && rspUser.ResponseItem != null)
            {
                byte[] pwd = Encryption.GenerateHashedPassword(password, rspUser.ResponseItem.Salt);
                ret = pwd.SequenceEqual(rspUser.ResponseItem.Password);
            }
            return ret;
        }

        public static bool ValidateFacebookUser(string username, string accessToken)
        {
            return FacebookHelper.IsValidUser(username, accessToken);
        }

        public static bool IsValidUser(string username)
        {
            bool fRet = false;
            return fRet;
            //bool fValid = false;
            //if (Validator.ValidateString(username, RegEx.UsernameRegEx.Expression))
            //    fValid = true;
            //if (fValid)
            //{
            //    string sqlCmd = string.Format("select 1 from [user] where username='{0}';", username.Trim().ToLower());
            //    try
            //    {
            //        using (var conn = DatabaseConnection.GetConnection())
            //        {
            //            conn.Open();
            //            //using (var ts = new TransactionScope()) // Automatic rollback on error
            //            //{
            //            using (SqlCommand cmd = new SqlCommand( sqlCmd, conn))
            //            {
            //                int iRet = (int) cmd.ExecuteScalar();
            //                if (iRet > 0)
            //                    fValid = true;
            //            }
            //                //ts.Complete();
            //            //}
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.SysFatal(string.Format("Could not validate user. SqlCommand[ {0} ]. {1}", sqlCmd, ex.ToString()));
            //        fRet = false;
            //    }
            //}
            //fRet = fValid;
            //return fRet;
        }

        public static void SignIn(string username)
        {
            AuthenticationHelper.CreateAuthenticationTicket(username);
            HttpContext.Current.Session[SessionVariable.Username] = username;
        }

        public static void SignOut(string username)
        {
            AuthenticationHelper.CancelAuthenticationTicket(username);
            HttpContext.Current.Session.Abandon();
        }

        public static GenericResponse<User> GetUserByUsername(string username)
        {
            var resp = new GenericResponse<User>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@UserId", SqlDbType.BigInt, DBNull.Value);
            cmd.AddParam("@Username", SqlDbType.VarChar, username);
            cmd.AddParam("@Email", SqlDbType.VarChar, DBNull.Value);
            BaseResponse ret = cmd.RunSpQuery("spUserRetrieve");
            if (ret.Success)
            {
                if ( cmd.DataResult.Rows.Count > 0 )
                {
                    DataRow row = cmd.DataResult.Rows[0];
                    User user = new User()
                    {
                        UserId = long.Parse(row["UserId"].ToString()),
                        Username = row["Username"].ToString(),
                        Password = row["Password"] == DBNull.Value ? null : (byte[])row["Password"],
                        Salt = row["Salt"] == DBNull.Value ? null : (byte[])row["Salt"],
                        Email = row["Email"].ToString(),
                        Guid = row["Guid"].ToString(),
                        DateGuidExpired = row["DateGuidExpired"] != DBNull.Value ? (DateTime?)row["DateGuidExpired"] : null,
                        IsDisabled = (bool)row["IsDisabled"],
                        IsFacebookUser = (bool)row["IsFacebookUser"],
                        //Title = row["Title"].ToString(),
                        //Firstname = row["Firstname"].ToString(),
                        //Lastname = row["Lastname"].ToString(),
                        //Birthday = row["Birthday"] != DBNull.Value ? (DateTime?)row["Birthday"] : null,
                        //Gender = row["Gender"].ToString(),
                        //Phone = row["Phone"].ToString(),
                        //Fax = row["Fax"].ToString(),
                        ////WebsiteUrl = row["WebsiteUrl"].ToString(),
                        //FacebookId = row["FacebookId"].ToString(),
                        //Address1 = row["Address1"].ToString(),
                        //Address2 = row["Address2"].ToString(),
                        //Address3 = row["Address3"].ToString(),
                        //City = row["City"].ToString(),
                        //Country = row["Country"].ToString(),
                        //ZipCode = row["ZipCode"].ToString(),
                        //DateCreated = (DateTime) row["DateCreated"],
                        //DateModified = (DateTime) row["DateModified"],
                        //UpdatedBy = row["UpdatedBy"].ToString(),
                        //CreatedBy = row["CreatedBy"].ToString()
                    };
                    resp.ResponseItem = user;
                    resp.Succeed();
                }
            }
            else
            {
                resp.Fail(string.Format("Could not retrieve User by Username[ {0} ]", username));
                Logger.SessionError(resp.GetFullText());
                return resp;
            }
            return resp;
        }

        public static GenericResponse<UserProfileDto> GetUserProfile(long userId)
        {
            var resp = new GenericResponse<UserProfileDto>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            BaseResponse ret = cmd.RunSpQuery("spUserProfileRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    DataRow row = cmd.DataResult.Rows[0];
                    var user = new UserProfileDto();

                    user.UserId = long.Parse(row["UserId"].ToString());
                    user.Title = row["Title"] != DBNull.Value ? row["Title"].ToString() : "";
                    user.Firstname = row["Firstname"].ToString();
                    user.Lastname = row["Lastname"].ToString();
                    user.Birthdate = row["Birthdate"] != DBNull.Value ?
                        DateTimeHelper.ConvertToDateString((DateTime)row["Birthdate"]) : null;
                    user.Gender = row["Gender"] != DBNull.Value ? row["Gender"].ToString() : "";
                    user.Phone = row["Phone"].ToString();
                    user.Fax = row["Fax"].ToString();
                    user.FacebookId = row["FacebookId"].ToString();
                    user.Address1 = row["Address1"].ToString();
                    user.Address2 = row["Address2"].ToString();
                    user.Address3 = row["Address3"].ToString();
                    user.City = row["City"].ToString();
                    user.CountryId = row["CountryId"] != DBNull.Value ? long.Parse(row["CountryId"].ToString()) : -1;
                    user.CountryName = row["CountryName"] != DBNull.Value ? row["CountryName"].ToString() : "";
                    user.ZipCode = row["ZipCode"].ToString();

                    user.Email = row["Email"].ToString();

                    resp.ResponseItem = user;
                    resp.Succeed();
                }
            }
            else
            {
                resp.Fail(string.Format("Could not retrieve UserProfile by UserId[ {0} ]", userId));
                Logger.SessionError(resp.GetFullText());
                return resp;
            }
            return resp;
        }

        public static GenericResponse<IList<OptionItem>> SearchUserByEmail(string keyword, bool isIncludeSelfUser)
        {
            var resp = new GenericResponse<IList<OptionItem>>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@FilterKeyword", SqlDbType.VarChar, keyword);
            BaseResponse ret = cmd.RunSpQuery("spSearchUserByEmail");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var items = new List<OptionItem>();
                    foreach (DataRow row in cmd.DataResult.Rows)
                    {
                        var item = new OptionItem();
                        item.Value = long.Parse(row["UserId"].ToString());
                        item.Text = row["Email"].ToString();
                        if (!isIncludeSelfUser)
                        {
                            if (SessionHelper.GetCurrentUserId() == item.Value)
                                continue; // Do not add this user to the result list
                        }
                        items.Add(item);
                    }
                    resp.ResponseItem = items;
                    resp.Succeed();
                }
            }
            else
            {
                resp.Fail(string.Format("Could not SearchUserByEmail. Keyword[ {0} ]", keyword));
                Logger.SessionError(resp.GetFullText());
                return resp;
            }
            return resp;
        }

        public static BaseResponse ProcessGeneratedLink(string type, string value)
        {
            var rsp = new BaseResponse();

            bool fValid = true;
            string targetTable = "";
            bool fSetActive = false;
            string staffStatus = StaffStatusString.Inactive;
            switch (type)
            {
                case LinkResponseType.StaffAccept:
                    targetTable = DatabaseTable.Staff;
                    staffStatus = StaffStatusString.Active;
                    break;
                case LinkResponseType.StaffReject:
                    targetTable = DatabaseTable.Staff;
                    staffStatus = StaffStatusString.Declined;
                    break;
                case LinkResponseType.UserConfirm:
                    targetTable = DatabaseTable.User;
                    fSetActive = true;
                    break;
                case LinkResponseType.UserCancel:
                    targetTable = DatabaseTable.User;
                    fSetActive = false;
                    break;
                default:
                    fValid = false;
                    break;
            }
            if (!fValid)
            {
                rsp.Fail(string.Format("Invalid query string. Type[ {0} ] Value[ {1} ]", type, value));
                Logger.SessionError(rsp.GetFullText());
                return rsp;
            }

            bool fGuidExist = false;
            DateTime? dateGuidExpired = (DateTime?)null;
            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@Guid", SqlDbType.VarChar, value);
            BaseResponse ret = cmd.ExecuteQuery(string.Format(
                "SELECT TOP 1 * " +
                "FROM {0} " +
                "WHERE Guid = @Guid;",
                targetTable
                ));
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    fGuidExist = true;
                    DataRow row = cmd.DataResult.Rows[0];
                    dateGuidExpired = row["DateGuidExpired"] != DBNull.Value ? (DateTime?)row["DateGuidExpired"] : null;
                }
            }
            else
            {
                rsp.Fail(string.Format("Could not retrieve Guid. Table[ {0} ] Type[ {1} ] Guid[ {2} ]", targetTable, type, value));
                Logger.SessionError(rsp.GetFullText());
                return rsp;
            }
            if (!fGuidExist)
            {
                rsp.Fail("The link is invalid or has already expired");
                return rsp;
            }

            if (!dateGuidExpired.HasValue || DateTime.Now > dateGuidExpired.Value)
            {
                rsp.Fail("The link has already expired");
                return rsp;
            }

            BaseResponse retAction = null;
            SqlCommandHelper cmdAction = new SqlCommandHelper();
            if ( type == LinkResponseType.UserCancel ) // If the user cancels the registration, then delete the record in User table
            {
                retAction = cmdAction.ExecuteDelete(targetTable, "Guid = @Guid");
            }
            else // Update record 
            {
                cmdAction.AddParam("@Guid", SqlDbType.VarChar, "");
                cmdAction.AddParam("@DateGuidExpired", SqlDbType.DateTime, DBNull.Value);
                cmdAction.AddParam("@DateModified", SqlDbType.DateTime, DateTime.Now);
                cmdAction.AddParam("@UpdatedBy", SqlDbType.VarChar, "system");
                
                if (targetTable == DatabaseTable.User)
                {
                    cmdAction.AddParam("@IsDisabled", SqlDbType.Bit, !fSetActive);
                    retAction = cmdAction.ExecuteUpdate(targetTable, "Guid = @Guid");
                }
                else // targetTable == DatabaseTable.Staff
                {
                    cmdAction.AddParam("@Status", SqlDbType.VarChar, staffStatus);
                    retAction = cmdAction.ExecuteUpdate(targetTable, "Guid = @Guid");
                }
            }
            rsp = retAction;
            return rsp;
        }
    }
}