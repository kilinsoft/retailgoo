﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class DocumentAdapter
    {
        #region DocSetting

        public class DocSettingPara
        {
            public string Prefix { get; set; }
            public bool Year { get; set; }
            public bool Month { get; set; }
            public string Suffix { get; set; }

            public DocSettingPara()
            {
                // initial a value
                this.Prefix = "";
                this.Year = false;
                this.Month = false;
                this.Suffix = "";
            }
            public DocSettingPara(string prefix, bool year, bool month, string suffix)
            {
                // initial a value
                this.Prefix = prefix;
                this.Year = year;
                this.Month = month;
                this.Suffix = suffix;
            }
        }

        public static DocSettingPara convertDocSettingFormatToParameter(string formatStr)
        {
            // HIM: convert Docsetting table data to DocSettingDto
            string[] rawPara = formatStr.Split('-');

            DocSettingPara dto = new DocSettingPara();

            bool doneDigit = false;

            for (int i = 0; i < rawPara.Length; i++)
            {
                if (rawPara[i].StartsWith("{")) // this is for a digit.
                {
                    doneDigit = true;
                    continue;
                }

                if ((i == 0) && (doneDigit == false))// this is for a prefix.
                {
                    dto.Prefix = rawPara[0];
                    continue;
                }

                if ((i != 0) && (doneDigit == false)) // this is for a year and month.
                {
                    if (rawPara[i] == "YY")
                        dto.Year = true;

                    if (rawPara[i] == "MM")
                        dto.Month = true;
                }

                if ((i != 0) && (doneDigit == true)) // this is for a suffix.
                {
                    dto.Suffix = rawPara[i];
                }
            }
            return dto;
        }

        public static string convertDocSettingParameterToFormat(DocSettingPara dto)
        {
            string formatStr = "";
            
            if (dto.Prefix != "")
                formatStr += dto.Prefix;

            if (dto.Year == true)
            {
                if (formatStr != "")
                    formatStr += "-";
                formatStr += "YY";
            }

            if (dto.Month == true)
            {
                if (formatStr != "")
                    formatStr += "-";
                formatStr += "MM";
            }

            if (formatStr != "")
                formatStr += "-";

            formatStr += "{XXXXXXX}";

            if (dto.Suffix != "")
            {
                formatStr += "-";
                formatStr += dto.Suffix;
            }

            return formatStr;
        }

        public static GenericResponse<DocSettingDto> RetrieveDocSetting()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<DocSettingDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spDocSettingRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    DocSettingDto dto = new DocSettingDto();

                    dto.DocSettingId = long.Parse(row["DocSettingId"].ToString());

                    DocSettingPara SalesOrderPara = convertDocSettingFormatToParameter(row["SalesOrderFormat"].ToString());
                    dto.SalesOrderPrefix = SalesOrderPara.Prefix;
                    dto.SalesOrderYear = SalesOrderPara.Year;
                    dto.SalesOrderMonth = SalesOrderPara.Month;
                    dto.SalesOrderSuffix = SalesOrderPara.Suffix;
                    dto.SalesOrderNextNo = int.Parse(row["SalesOrderNo"].ToString()) + 1;
                    dto.SalesOrderTemplate = int.Parse(row["SalesOrderTemplate"].ToString());

                    DocSettingPara InvoicePara = convertDocSettingFormatToParameter(row["InvoiceFormat"].ToString());
                    dto.InvoicePrefix = InvoicePara.Prefix;
                    dto.InvoiceYear = InvoicePara.Year;
                    dto.InvoiceMonth = InvoicePara.Month;
                    dto.InvoiceSuffix = InvoicePara.Suffix;
                    dto.InvoiceNextNo = int.Parse(row["InvoiceNo"].ToString()) + 1;
                    dto.InvoiceTemplate = int.Parse(row["InvoiceTemplate"].ToString());

                    DocSettingPara ReceiptPara = convertDocSettingFormatToParameter(row["ReceiptFormat"].ToString());
                    dto.ReceiptPrefix = ReceiptPara.Prefix;
                    dto.ReceiptYear = ReceiptPara.Year;
                    dto.ReceiptMonth = ReceiptPara.Month;
                    dto.ReceiptSuffix = ReceiptPara.Suffix;
                    dto.ReceiptNextNo = int.Parse(row["ReceiptNo"].ToString()) + 1;
                    dto.ReceiptTemplate = int.Parse(row["ReceiptTemplate"].ToString());

                    DocSettingPara StockAdjPara = convertDocSettingFormatToParameter(row["StockAdjFormat"].ToString());
                    dto.StockAdjPrefix = StockAdjPara.Prefix;
                    dto.StockAdjYear = StockAdjPara.Year;
                    dto.StockAdjMonth = StockAdjPara.Month;
                    dto.StockAdjSuffix = StockAdjPara.Suffix;
                    dto.StockAdjNextNo = int.Parse(row["StockAdjNo"].ToString()) + 1;
                    dto.StockAdjTemplate = int.Parse(row["StockAdjTemplate"].ToString());

                    DocSettingPara ReceiveOrderPara = convertDocSettingFormatToParameter(row["ReceiveOrderFormat"].ToString());
                    dto.ReceiveOrderPrefix = ReceiveOrderPara.Prefix;
                    dto.ReceiveOrderYear = ReceiveOrderPara.Year;
                    dto.ReceiveOrderMonth = ReceiveOrderPara.Month;
                    dto.ReceiveOrderSuffix = ReceiveOrderPara.Suffix;
                    dto.ReceiveOrderNextNo = int.Parse(row["ReceiveOrderNo"].ToString()) + 1;
                    dto.ReceiveOrderTemplate = int.Parse(row["ReceiveOrderTemplate"].ToString());

                    DocSettingPara CashPurchasePara = convertDocSettingFormatToParameter(row["CashPurchaseFormat"].ToString());
                    dto.CashPurchasePrefix = CashPurchasePara.Prefix;
                    dto.CashPurchaseYear = CashPurchasePara.Year;
                    dto.CashPurchaseMonth = CashPurchasePara.Month;
                    dto.CashPurchaseSuffix = CashPurchasePara.Suffix;
                    dto.CashPurchaseNextNo = int.Parse(row["CashPurchaseNo"].ToString()) + 1;
                    dto.CashPurchaseTemplate = int.Parse(row["CashPurchaseTemplate"].ToString());

                    DocSettingPara CashSalesPara = convertDocSettingFormatToParameter(row["CashSalesFormat"].ToString());
                    dto.CashSalesPrefix = CashSalesPara.Prefix;
                    dto.CashSalesYear = CashSalesPara.Year;
                    dto.CashSalesMonth = CashSalesPara.Month;
                    dto.CashSalesSuffix = CashSalesPara.Suffix;
                    dto.CashSalesNextNo = int.Parse(row["CashSalesNo"].ToString()) + 1;
                    dto.CashSalesTemplate = int.Parse(row["CashSalesTemplate"].ToString());

                    DocSettingPara PurchaseOrderPara = convertDocSettingFormatToParameter(row["PurchaseOrderFormat"].ToString());
                    dto.PurchaseOrderPrefix = PurchaseOrderPara.Prefix;
                    dto.PurchaseOrderYear = PurchaseOrderPara.Year;
                    dto.PurchaseOrderMonth = PurchaseOrderPara.Month;
                    dto.PurchaseOrderSuffix = PurchaseOrderPara.Suffix;
                    dto.PurchaseOrderNextNo = int.Parse(row["PurchaseOrderNo"].ToString()) + 1;
                    dto.PurchaseOrderTemplate = int.Parse(row["PurchaseOrderTemplate"].ToString());

                    DocSettingPara PaymentPara = convertDocSettingFormatToParameter(row["PaymentFormat"].ToString());
                    dto.PaymentPrefix = PaymentPara.Prefix;
                    dto.PaymentYear = PaymentPara.Year;
                    dto.PaymentMonth = PaymentPara.Month;
                    dto.PaymentSuffix = PaymentPara.Suffix;
                    dto.PaymentNextNo = int.Parse(row["PaymentNo"].ToString()) + 1;
                    dto.PaymentTemplate = int.Parse(row["PaymentTemplate"].ToString());

                    DocSettingPara ExpensePara = convertDocSettingFormatToParameter(row["ExpenseFormat"].ToString());
                    dto.ExpensePrefix = ExpensePara.Prefix;
                    dto.ExpenseYear = ExpensePara.Year;
                    dto.ExpenseMonth = ExpensePara.Month;
                    dto.ExpenseSuffix = ExpensePara.Suffix;
                    dto.ExpenseNextNo = int.Parse(row["ExpenseNo"].ToString()) + 1;
                    dto.ExpenseTemplate = int.Parse(row["ExpenseTemplate"].ToString());

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("DocSetting[{0}] does not exist", businessId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve DocSetting for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }

            return result;
        }

        public static BaseResponse ManageDocSetting(DocSettingDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@DocSettingId", SqlDbType.BigInt, dto.DocSettingId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);

            // SalesOrder parameter
            string SalesOrderFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.SalesOrderPrefix,
                    dto.SalesOrderYear,
                    dto.SalesOrderMonth,
                    dto.SalesOrderSuffix));
            cmd.AddParam("@SalesOrderFormat", SqlDbType.NVarChar, SalesOrderFormat);
            cmd.AddParam("@SalesOrderNo", SqlDbType.NVarChar, dto.SalesOrderNextNo);
            cmd.AddParam("@SalesOrderTemplate", SqlDbType.TinyInt, 0);

            // Invoice parameter
            string InvoiceFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.InvoicePrefix,
                    dto.InvoiceYear,
                    dto.InvoiceMonth,
                    dto.InvoiceSuffix));
            cmd.AddParam("@InvoiceFormat", SqlDbType.NVarChar, InvoiceFormat);
            cmd.AddParam("@InvoiceNo", SqlDbType.NVarChar, dto.InvoiceNextNo);
            cmd.AddParam("@InvoiceTemplate", SqlDbType.TinyInt, 0);

            // Receipt parameter
            string ReceiptFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.ReceiptPrefix,
                    dto.ReceiptYear,
                    dto.ReceiptMonth,
                    dto.ReceiptSuffix));
            cmd.AddParam("@ReceiptFormat", SqlDbType.NVarChar, ReceiptFormat);
            cmd.AddParam("@ReceiptNo", SqlDbType.NVarChar, dto.ReceiptNextNo);
            cmd.AddParam("@ReceiptTemplate", SqlDbType.TinyInt, 0);

            // StockAdj parameter
            string StockAdjFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.StockAdjPrefix,
                    dto.StockAdjYear,
                    dto.StockAdjMonth,
                    dto.StockAdjSuffix));
            cmd.AddParam("@StockAdjFormat", SqlDbType.NVarChar, StockAdjFormat);
            cmd.AddParam("@StockAdjNo", SqlDbType.NVarChar, dto.StockAdjNextNo);
            cmd.AddParam("@StockAdjTemplate", SqlDbType.TinyInt, 0);

            // ReceiveOrder parameter
            string ReceiveOrderFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.ReceiveOrderPrefix,
                    dto.ReceiveOrderYear,
                    dto.ReceiveOrderMonth,
                    dto.ReceiveOrderSuffix));
            cmd.AddParam("@ReceiveOrderFormat", SqlDbType.NVarChar, ReceiveOrderFormat);
            cmd.AddParam("@ReceiveOrderNo", SqlDbType.NVarChar, dto.ReceiveOrderNextNo);
            cmd.AddParam("@ReceiveOrderTemplate", SqlDbType.TinyInt, 0);

            // CashPurchase parameter
            string CashPurchaseFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.CashPurchasePrefix,
                    dto.CashPurchaseYear,
                    dto.CashPurchaseMonth,
                    dto.CashPurchaseSuffix));
            cmd.AddParam("@CashPurchaseFormat", SqlDbType.NVarChar, CashPurchaseFormat);
            cmd.AddParam("@CashPurchaseNo", SqlDbType.NVarChar, dto.CashPurchaseNextNo);
            cmd.AddParam("@CashPurchaseTemplate", SqlDbType.TinyInt, 0);

            // CashSales parameter
            string CashSalesFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.CashSalesPrefix,
                    dto.CashSalesYear,
                    dto.CashSalesMonth,
                    dto.CashSalesSuffix));
            cmd.AddParam("@CashSalesFormat", SqlDbType.NVarChar, CashSalesFormat);
            cmd.AddParam("@CashSalesNo", SqlDbType.NVarChar, dto.CashSalesNextNo);
            cmd.AddParam("@CashSalesTemplate", SqlDbType.TinyInt, 0);

            // PurchaseOrder parameter
            string PurchaseOrderFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.PurchaseOrderPrefix,
                    dto.PurchaseOrderYear,
                    dto.PurchaseOrderMonth,
                    dto.PurchaseOrderSuffix));
            cmd.AddParam("@PurchaseOrderFormat", SqlDbType.NVarChar, PurchaseOrderFormat);
            cmd.AddParam("@PurchaseOrderNo", SqlDbType.NVarChar, dto.PurchaseOrderNextNo);
            cmd.AddParam("@PurchaseOrderTemplate", SqlDbType.TinyInt, 0);

            // Payment parameter
            string PaymentFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.PurchaseOrderPrefix,
                    dto.PaymentYear,
                    dto.PaymentMonth,
                    dto.PaymentSuffix));
            cmd.AddParam("@PaymentFormat", SqlDbType.NVarChar, PaymentFormat);
            cmd.AddParam("@PaymentNo", SqlDbType.NVarChar, dto.PaymentNextNo);
            cmd.AddParam("@PaymentTemplate", SqlDbType.TinyInt, 0);

            // Expense parameter
            string ExpenseFormat = convertDocSettingParameterToFormat(
                new DocSettingPara(dto.ExpensePrefix,
                    dto.ExpenseYear,
                    dto.ExpenseMonth,
                    dto.ExpenseSuffix));
            cmd.AddParam("@ExpenseFormat", SqlDbType.NVarChar, ExpenseFormat);
            cmd.AddParam("@ExpenseNo", SqlDbType.NVarChar, dto.ExpenseNextNo);
            cmd.AddParam("@ExpenseTemplate", SqlDbType.TinyInt, 0);

            cmd.AddParam("@UserId", SqlDbType.VarChar, userId);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@ItemState", SqlDbType.Char, 'M');    // HIM: Docsetting is always changed. 

            BaseResponse ret = cmd.RunSpNonQuery("spDocSettingManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process DocSetting for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region FinancialSetting

        public static GenericResponse<FinancialSettingDto> RetrieveFinancialSetting()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<FinancialSettingDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spFinancialSettingRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    FinancialSettingDto dto = new FinancialSettingDto();

                    dto.CurrencyId = long.Parse(row["CurrencyId"].ToString());
                    dto.DefaultPriceTypeId = long.Parse(row["DefaultPriceTypeId"].ToString());
                    dto.UseServiceCharge = (bool)row["UseServiceCharge"];
                    dto.ServiceChargeAmount = (decimal)row["ServiceChargeAmount"];
                    dto.SalesTaxTypeId = long.Parse(row["SalesTaxTypeId"].ToString());
                    dto.SalesTaxAmount = (decimal)row["SalesTaxAmount"];
                    dto.UseStockItemControl = (bool)row["UseStockItemControl"];
                    dto.CostingTypeId = long.Parse(row["CostingTypeId"].ToString());

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("FinancialSetting[{0}] does not exist", businessId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve FinancialSetting for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageFinancialSetting(FinancialSettingDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@CurrencyId", SqlDbType.TinyInt, dto.CurrencyId);
            cmd.AddParam("@DefaultPriceTypeId", SqlDbType.TinyInt, dto.DefaultPriceTypeId);
            cmd.AddParam("@UseServiceCharge", SqlDbType.Bit, dto.UseServiceCharge);
            cmd.AddParam("@ServiceChargeAmount", SqlDbType.Decimal, dto.ServiceChargeAmount);
            cmd.AddParam("@SalesTaxTypeId", SqlDbType.TinyInt, dto.SalesTaxTypeId);
            cmd.AddParam("@SalesTaxAmount", SqlDbType.Decimal, dto.SalesTaxAmount);
            cmd.AddParam("@UseStockItemControl", SqlDbType.Bit, dto.UseStockItemControl);
            cmd.AddParam("@CostingTypeId", SqlDbType.TinyInt, dto.CostingTypeId);

            cmd.AddParam("@UserId", SqlDbType.VarChar, userId);
            cmd.AddParam("@ItemState", SqlDbType.Char, 'M');    // HIM: Docsetting is always changed. 

            BaseResponse ret = cmd.RunSpNonQuery("spFinancialSettingManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process FinancialSetting for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion
    }
}