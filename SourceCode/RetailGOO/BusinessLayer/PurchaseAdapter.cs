﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class PurchaseAdapter
    {

        #region CashPurchase

        public static GenericResponse<IList<PurchaseOrderDto>> RetrieveAllCashPurchase()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<PurchaseOrderDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spCashPurchaseAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<PurchaseOrderDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    PurchaseOrderDto item = new PurchaseOrderDto();

                    item.PurchaseOrderId = long.Parse(row["PurchaseOrderId"].ToString());
                    item.CashPurchaseNo = row["CashPurchaseNo"].ToString();
                    item.SupplierName = row["SupplierName"] != DBNull.Value ?
                        row["SupplierName"].ToString() : DefaultNullableOptionItem.BusinessPartnerText;
                    item.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    item.Total = (decimal)row["Total"];
                    item.Status = row["Status"].ToString(); 
                    
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllCashPurchase for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageCashPurchase(PurchaseOrderDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("cp",dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} CashPurchase[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.PurchaseOrderId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            return ManagePurchaseOrderOriginal(dto);
        }

        public static BaseResponse VoidCashPurchase(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@PurchaseOrderId", SqlDbType.BigInt, id);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            BaseResponse ret = cmd.RunSpNonQuery("spCashPurchaseVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void CashPurchase. BusinessId[{0}] PurchaseOrderId[{1}]", businessId, id));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region PurchaseOrder

        public static GenericResponse<IList<PurchaseOrderDto>> RetrieveAllPurchaseOrder(SearchParameterDto searchDto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<PurchaseOrderDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            //cmd.AddParam("@FilterColumn", SqlDbType.VarChar, searchDto.FilterColumn);
            cmd.AddParam("@FilterKeyword", SqlDbType.NVarChar, searchDto.FilterKeyword);
            cmd.AddParam("@SortingColumn", SqlDbType.VarChar, searchDto.SortingColumn);
            //cmd.AddParam("@SortingDirection", SqlDbType.VarChar, searchDto.SortingDirection);
            cmd.AddParam("@IndexStart", SqlDbType.BigInt, (searchDto.PageIndex * searchDto.RecordPerPage));
            cmd.AddParam("@MaxCount", SqlDbType.Int, searchDto.RecordPerPage);
            BaseResponse ret = cmd.RunSpQuery("spPurchaseOrderAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<PurchaseOrderDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    PurchaseOrderDto item = new PurchaseOrderDto();
                    
                    item.PurchaseOrderId = long.Parse(row["PurchaseOrderId"].ToString());
                    item.PurchaseOrderNo = row["PurchaseOrderNo"].ToString();
                    item.SupplierName = row["SupplierName"] != DBNull.Value ?
                        row["SupplierName"].ToString() : DefaultNullableOptionItem.BusinessPartnerText;
                    item.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    item.Total = (decimal) row["Total"];
                    item.Status = row["Status"].ToString(); 
                    
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
                result.VirtualItemCount = cmd.RecordCount;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllPurchaseOrder for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrievePurchaseOrderList(long supplierId = -1)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            if (supplierId != -1)
                cmd.AddParam("@SupplierId", SqlDbType.BigInt, supplierId );
            BaseResponse ret = cmd.RunSpQuery("spPurchaseOrderListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem();

                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve PurchaseOrderList for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<PurchaseOrderDto> RetrievePurchaseOrder(long poId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<PurchaseOrderDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@PurchaseOrderId", SqlDbType.BigInt, poId);
            BaseResponse ret = cmd.RunSpQuery("spPurchaseOrderRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    PurchaseOrderDto dto = new PurchaseOrderDto();

                    dto.PurchaseOrderId = (long)row["PurchaseOrderId"];
                    dto.PurchaseOrderNo = row["PurchaseOrderNo"].ToString();
                    dto.IsCashPurchase = (bool)row["IsCashPurchase"];
                    dto.CashPurchaseNo = row["CashPurchaseNo"].ToString();
                    dto.SupplierId = row["SupplierId"] != DBNull.Value ?
                        (long)row["SupplierId"] : -1;
                    dto.Reference = row["Reference"].ToString();
                    dto.ContactPerson = row["ContactPerson"].ToString();
                    dto.Address1 = row["Address1"].ToString();
                    dto.Address2 = row["Address2"].ToString();
                    dto.Address3 = row["Address3"].ToString();
                    dto.Phone = row["Phone"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.DateDelivery = DateTimeHelper.ConvertToDateString((DateTime)row["DateDelivery"]);
                    dto.AmountTypeId = row["AmountTypeId"] != DBNull.Value ?
                        int.Parse(row["AmountTypeId"].ToString()) : -1;
                    dto.PaymentTypeId = int.Parse(row["PaymentTypeId"].ToString());
                    dto.AmountTypeName = row["AmountTypeName"] != DBNull.Value ?
                        row["AmountTypeName"].ToString() : "";
                    dto.PaymentTypeName = row["PaymentTypeName"].ToString();
                    dto.Note = row["Note"].ToString();
                    dto.CreditTerm = row["CreditTerm"].ToString();
                    dto.Subtotal = (decimal)row["subtotal"];
                    dto.Tax = (decimal)row["Tax"];
                    dto.Total = (decimal)row["Total"];
                    dto.Status = row["Status"].ToString();
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("PurchaseOrder[{0}] does not exist", poId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve PurchaseOrder[ {0} ]", poId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<PurchaseOrderItemDto>> RetrieveAllPurchaseOrderItem(long purchaseOrderId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<PurchaseOrderItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@PurchaseOrderId", SqlDbType.BigInt, purchaseOrderId);
            BaseResponse ret = cmd.RunSpQuery("spPurchaseOrderItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<PurchaseOrderItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    PurchaseOrderItemDto item = new PurchaseOrderItemDto();
                    item.PurchaseOrderItemId = long.Parse(row["PurchaseOrderItemId"].ToString());
                    item.ItemId = (long) row["ItemId"];
                    item.ItemFullName = row["ItemFullName"].ToString();
                    item.Quantity = (decimal)row["Quantity"];
                    item.Price = (decimal)row["Price"];
                    item.UnitOfMeasure = row["Uom"].ToString();
                    item.Amount = (decimal)row["Amount"];
                    item.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);
                    
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllPurchaseOrderItem for BusinessId[{0}] PurchaseOrderId[{1}]", businessId, purchaseOrderId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManagePurchaseOrder(PurchaseOrderDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("po", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} PurchaseOrder[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.PurchaseOrderId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            return ManagePurchaseOrderOriginal(dto);
        }

        public static BaseResponse VoidPurchaseOrder(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@PurchaseOrderId", SqlDbType.BigInt, id);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            BaseResponse ret = cmd.RunSpNonQuery("spPurchaseOrderVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void PurchaseOrder[{0}] for BusinessId[{1}]", id, businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructPurchaseOrderItemTable()
        {
            DataTable dt = new DataTable("PurchaseOrderItem");
            dt.Columns.Add("PurchaseOrderItemId", typeof(long));
            dt.Columns.Add("PurchaseOrderId", typeof(long));
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("Price", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region ReceiveOrder

        public static GenericResponse<IList<ReceiveOrderDto>> RetrieveAllReceiveOrder()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ReceiveOrderDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spReceiveOrderAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ReceiveOrderDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new ReceiveOrderDto();

                    item.ReceiveOrderId = long.Parse(row["ReceiveOrderId"].ToString());
                    item.ReceiveOrderNo = row["ReceiveOrderNo"].ToString();
                    item.SupplierName = row["SupplierName"] != DBNull.Value ?
                        row["SupplierName"].ToString() : DefaultNullableOptionItem.BusinessPartnerText;
                    item.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    item.Total = (decimal)row["Total"];
                    item.Status = row["Status"].ToString(); 

                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllReceiveOrder for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ReceiveOrderDto> RetrieveReceiveOrder(long roId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<ReceiveOrderDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@ReceiveOrderId", SqlDbType.BigInt, roId);
            BaseResponse ret = cmd.RunSpQuery("spReceiveOrderRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    var item = new ReceiveOrderDto();

                    item.ReceiveOrderId = long.Parse(row["ReceiveOrderId"].ToString());
                    item.ReceiveOrderNo = row["ReceiveOrderNo"].ToString();
                    item.SupplierId = row["SupplierId"] != DBNull.Value ?
                        long.Parse(row["SupplierId"].ToString()) : -1;
                    item.SupplierName = row["SupplierName"] != DBNull.Value ?
                        row["SupplierName"].ToString() : DefaultNullableOptionItem.BusinessPartnerText;
                    item.Reference = row["Reference"].ToString();
                    item.ContactPerson = row["ContactPerson"].ToString();
                    item.Address1 = row["Address1"].ToString();
                    item.Address2 = row["Address2"].ToString();
                    item.Address3 = row["Address3"].ToString();
                    item.Phone = row["Phone"].ToString();
                    item.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    item.PaymentTerm = row["PaymentTerm"].ToString();
                    item.Note = row["Note"].ToString();
                    item.Subtotal = (decimal)row["subtotal"];
                    item.Tax = (decimal)row["Tax"];
                    item.Total = (decimal)row["Total"];
                    item.Status = row["Status"].ToString(); 
                    item.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    // For use in SubGrid of OutgoingPayment screen
                    item.Amount = item.Subtotal; // Use Subtotal instead of Total because the Subtotal will be taxed again after the
                                                 // item has been added to the subGrid in OutgoingPayment screen
                    item.AmountPaid = (decimal)row["AmountPaid"];
                    item.AmountDue = Math.Abs(item.Amount - item.AmountPaid);

                    result.Succeed();
                    result.ResponseItem = item;
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ReceiveOrder[ {0} ]", roId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<ReceiveOrderItemDto>> RetrieveAllReceiveOrderItem(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ReceiveOrderItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@ReceiveOrderId", SqlDbType.BigInt, id);
            BaseResponse ret = cmd.RunSpQuery("spReceiveOrderItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ReceiveOrderItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new ReceiveOrderItemDto();
                    item.ReceiveOrderItemId = long.Parse(row["ReceiveOrderId"].ToString());
                    item.ReceiveOrderId = long.Parse(row["ReceiveOrderId"].ToString());
                    item.PurchaseOrderItemId = long.Parse(row["PurchaseOrderItemId"].ToString());
                    item.PurchaseOrderNo = row["PurchaseOrderNo"].ToString();
                    item.ItemId = long.Parse(row["ItemId"].ToString());
                    item.ItemFullName = row["ItemFullName"].ToString();
                    item.UnitOfMeasure = row["Uom"].ToString();
                    item.QuantityOrdered = (decimal)row["QuantityOrdered"];
                    item.Quantity = (decimal)row["Quantity"];
                    item.Price = (decimal)row["Price"];
                    item.Amount = (decimal)row["Amount"];
                    item.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ReceiveOrderItem for BusinessId[ {0} ] ReceiveOrderId[ {1} ]", businessId, id));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageReceiveOrder(ReceiveOrderDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("ro", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} ReceiveOrder[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.ReceiveOrderId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@ReceiveOrderId", SqlDbType.BigInt, dto.ReceiveOrderId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@SupplierId", SqlDbType.BigInt,
                dto.SupplierId != -1 ? dto.SupplierId : (object)DBNull.Value);
            //cmd.AddParam("@SupplierName", SqlDbType.NVarChar, dto.SupplierName);
            cmd.AddParam("@Reference", SqlDbType.NVarChar, dto.Reference);
            cmd.AddParam("@ContactPerson", SqlDbType.NVarChar, dto.ContactPerson);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, dto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, dto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, dto.Address3);
            cmd.AddParam("@Phone", SqlDbType.VarChar, dto.Phone);
            cmd.AddParam("@Date", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Date));
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            cmd.AddParam("@PaymentTerm", SqlDbType.NVarChar, dto.PaymentTerm);
            cmd.AddParam("@Subtotal", SqlDbType.Decimal, dto.Subtotal);
            cmd.AddParam("@Tax", SqlDbType.Decimal, dto.Tax);
            cmd.AddParam("@Total", SqlDbType.Decimal, dto.Total);
            //cmd.AddParam("@Status", SqlDbType.VarChar, dto.Status);
            //cmd.AddParam("@IsVoided", SqlDbType.Bit, dto.IsVoided);
            //cmd.AddParam("@IsDeleted", SqlDbType.Bit, dto.IsDeleted);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            // Prepare Items
            DataTable dt = ConstructReceiveOrderItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.ReceiveOrderItemId,
                        dto.ReceiveOrderId,
                        item.PurchaseOrderItemId,
                        item.ItemId,
                        item.Price,
                        item.Quantity,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spReceiveOrderManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process ReceiveOrder[ {0} ]", dto.ReceiveOrderId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse VoidReceiveOrder(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@ReceiveOrderId", SqlDbType.BigInt, id);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            BaseResponse ret = cmd.RunSpNonQuery("spReceiveOrderVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void ReceiveOrder[{0}] for BusinessId[{1}]", id, businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructReceiveOrderItemTable()
        {
            DataTable dt = new DataTable("ReceiveOrderItem");
            dt.Columns.Add("ReceiveOrderItemId", typeof(long));
            dt.Columns.Add("ReceiveOrderId", typeof(long));
            dt.Columns.Add("PurchaseOrderItemId", typeof(long));
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("Price", typeof(decimal));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region OutgoingPayment

        public static GenericResponse<IList<OutgoingPaymentDto>> RetrieveAllOutgoingPayment()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OutgoingPaymentDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spOutgoingPaymentAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OutgoingPaymentDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OutgoingPaymentDto();

                    item.OutgoingPaymentId = long.Parse(row["OutgoingPaymentId"].ToString());
                    item.OutgoingPaymentNo = row["OutgoingPaymentNo"].ToString();
                    item.SupplierName = row["SupplierName"] != DBNull.Value ?
                        row["SupplierName"].ToString() : DefaultNullableOptionItem.BusinessPartnerText;
                    item.Reference = row["Reference"].ToString();
                    item.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    item.Total = (decimal)row["Total"];
                    item.Status = row["Status"].ToString();
                    item.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllOutgoingPayment for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<OutgoingPaymentDto> RetrieveOutgoingPayment(long opId)
        {
            var result = new GenericResponse<OutgoingPaymentDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@OutgoingPaymentId", SqlDbType.BigInt, opId);
            BaseResponse ret = cmd.RunSpQuery("spOutgoingPaymentRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    var item = new OutgoingPaymentDto();
                    
                    item.OutgoingPaymentId = long.Parse(row["OutgoingPaymentId"].ToString());
                    item.OutgoingPaymentNo = row["OutgoingPaymentNo"].ToString();
                    item.SupplierId = row["SupplierId"] != DBNull.Value ?
                        (long)row["SupplierId"] : -1;
                    item.SupplierName = row["SupplierName"] != DBNull.Value ?
                        row["SupplierName"].ToString() : DefaultNullableOptionItem.BusinessPartnerText;
                    item.Reference = row["Reference"].ToString();
                    item.ContactPerson = row["ContactPerson"].ToString();
                    item.Detail1 = row["Detail1"].ToString();
                    item.Detail2 = row["Detail2"].ToString();
                    item.Detail3 = row["Detail3"].ToString();
                    item.Phone = row["Phone"].ToString();
                    item.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    item.PaymentTypeId = int.Parse(row["PaymentTypeId"].ToString());
                    item.PaymentTypeName = row["PaymentTypeName"].ToString();
                    item.TaxIdInfo = row["TaxIdInfo"].ToString();
                    item.Note = row["Note"].ToString();
                    item.Subtotal = (decimal)row["subtotal"];
                    item.Tax = (decimal)row["Tax"];
                    item.Total = (decimal)row["Total"];
                    item.Status = row["Status"].ToString();
                    item.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = item;
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve OutgoingPayment[ {0} ]", opId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OutgoingPaymentItemDto>> RetrieveAllOutgoingPaymentItem(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OutgoingPaymentItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@OutgoingPaymentId", SqlDbType.BigInt, id);
            BaseResponse ret = cmd.RunSpQuery("spOutgoingPaymentItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OutgoingPaymentItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OutgoingPaymentItemDto()
                    {
                        OutgoingPaymentItemId = long.Parse(row["OutgoingPaymentItemId"].ToString()),
                        OutgoingPaymentId = long.Parse(row["OutgoingPaymentId"].ToString()),
                        ReceiveOrderId = long.Parse(row["ReceiveOrderId"].ToString()),
                        ReceiveOrderNo = row["ReceiveOrderNo"].ToString(),
                        SupplierInvoice = row["SupplierInvoice"].ToString(),
                        Description = row["Description"].ToString(),
                        Amount = (decimal)row["Amount"],
                        AmountDue = (decimal)row["AmountDue"],
                        AmountPaid = (decimal)row["AmountPaid"],
                        Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"])
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllOutgoingPaymentItem for BusinessId[ {0} ] OutgoingPaymentId[ {1} ]", businessId, id));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageOutgoingPayment(OutgoingPaymentDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("op", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} OutgoingPayment[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.OutgoingPaymentId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@OutgoingPaymentId", SqlDbType.BigInt, dto.OutgoingPaymentId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@OutgoingPaymentNo", SqlDbType.NVarChar, dto.OutgoingPaymentNo);
            cmd.AddParam("@SupplierId", SqlDbType.BigInt,
                dto.SupplierId != -1 ? dto.SupplierId : (object)DBNull.Value);
            cmd.AddParam("@Reference", SqlDbType.NVarChar, dto.Reference);
            cmd.AddParam("@ContactPerson", SqlDbType.NVarChar, dto.ContactPerson);
            cmd.AddParam("@Detail1", SqlDbType.NVarChar, dto.Detail1);
            cmd.AddParam("@Detail2", SqlDbType.NVarChar, dto.Detail2);
            cmd.AddParam("@Detail3", SqlDbType.NVarChar, dto.Detail3);
            cmd.AddParam("@Phone", SqlDbType.VarChar, dto.Phone);
            cmd.AddParam("@Date", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Date));
            cmd.AddParam("@PaymentTypeId", SqlDbType.VarChar, dto.PaymentTypeId);
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            cmd.AddParam("@TaxIdInfo", SqlDbType.NVarChar, dto.TaxIdInfo);
            cmd.AddParam("@Subtotal", SqlDbType.Decimal, dto.Subtotal);
            cmd.AddParam("@Tax", SqlDbType.Decimal, dto.Tax);
            cmd.AddParam("@Total", SqlDbType.Decimal, dto.Total);
            //cmd.AddParam("@Status", SqlDbType.VarChar, dto.Status);
            //cmd.AddParam("@IsVoided", SqlDbType.Bit, dto.IsVoided);
            //cmd.AddParam("@IsDeleted", SqlDbType.Bit, dto.IsDeleted);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
     
            // Prepare Items
            DataTable dt = ConstructOutgoingPaymentItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.OutgoingPaymentItemId,
                        item.ReceiveOrderId,
                        item.SupplierInvoice,
                        item.Amount,
                        item.Description,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spOutgoingPaymentManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process OutgoingPayment[ {0} ]", dto.OutgoingPaymentId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse VoidOutgoingPayment(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@OutgoingPaymentId", SqlDbType.BigInt, id);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            BaseResponse ret = cmd.RunSpNonQuery("spOutgoingPaymentVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void OutgoingPayment[{0}] for BusinessId[{1}]", id, businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveReceiveOrderList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spReceiveOrderListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem();

                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ReceiveOrderList for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructOutgoingPaymentItemTable()
        {
            DataTable dt = new DataTable("OutgoingPaymentItem");
            dt.Columns.Add("OutgoingPaymentItemId", typeof(long));
            dt.Columns.Add("ReceiveOrderId", typeof(long));
            dt.Columns.Add("SupplierInvoice", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion

        #region Expense

        public static GenericResponse<IList<ExpenseDto>> RetrieveAllExpense()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<ExpenseDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spExpenseAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ExpenseDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new ExpenseDto();
                    
                    item.ExpenseId = long.Parse(row["ExpenseId"].ToString());
                    item.ExpenseNo = row["ExpenseNo"].ToString();
                    item.SupplierName = row["SupplierName"].ToString();
                    item.ExpenseCategoryName = row["ExpenseCategoryName"].ToString();
                    item.Date = DateTimeHelper.ConvertToDateTimeString((DateTime)row["Date"]);
                    item.Amount = (decimal)row["Amount"];
                    
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllExpenses for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<ExpenseDto> RetrieveExpense(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<ExpenseDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@ExpenseId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spExpenseRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    var item = new ExpenseDto();

                    item.ExpenseId = long.Parse(row["ExpenseId"].ToString());
                    item.ExpenseNo = row["ExpenseNo"].ToString();
                    item.SupplierId = row["SupplierId"] != DBNull.Value ?
                        (long)row["SupplierId"] : -1;
                    item.ExpenseCategoryId = long.Parse(row["ExpenseCategoryId"].ToString());
                    item.ExpenseCategoryName = row["ExpenseCategoryName"].ToString();
                    item.Date = DateTimeHelper.ConvertToDateTimeString((DateTime)row["Date"]);
                    item.Note = row["Note"].ToString();
                    item.Amount = (decimal)row["Amount"];
                    item.UseWht = (bool)row["UseWht"];
                    item.WhtPercent = (decimal)row["WhtPercent"];
                    item.WhtAmount = (decimal)row["WhtAmount"];
                    //item.IsVoided = (bool)row["IsVoided"];
                    item.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = item;
                }
                
            }
            else
            {
                result.Fail(string.Format("Could not retrieve Expenses[ {0} ]", id));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageExpense(ExpenseDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@ExpenseId", SqlDbType.BigInt, dto.ExpenseId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ExpenseNo", SqlDbType.NVarChar, dto.ExpenseNo);
            cmd.AddParam("@SupplierId", SqlDbType.BigInt, dto.SupplierId);
            //cmd.AddParam("@VendorName", SqlDbType.NVarChar, dto.VendorName);
            cmd.AddParam("@ExpenseCategoryId", SqlDbType.BigInt, dto.ExpenseCategoryId);
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            cmd.AddParam("@Date", SqlDbType.Date, DateTimeHelper.ConvertToDateTime(dto.Date));
            //cmd.AddParam("@ExpenseCategoryName", SqlDbType.NVarChar, dto.ExpenseCategoryName);
            cmd.AddParam("@Amount", SqlDbType.Decimal, dto.Amount);
            cmd.AddParam("@UseWht", SqlDbType.Bit, dto.UseWht);
            cmd.AddParam("@WhtPercent", SqlDbType.Decimal, dto.WhtPercent);
            cmd.AddParam("@WhtAmount", SqlDbType.Decimal, dto.WhtAmount);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            BaseResponse ret = cmd.RunSpNonQuery("spExpenseManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not ManageExpense. BusinessId[{0}] ExpenseId[{1}]", businessId, dto.ExpenseId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse VoidExpense(long id)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@ExpenseId", SqlDbType.BigInt, id);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            BaseResponse ret = cmd.RunSpNonQuery("spExpenseVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void ExpenseId[{0}] for BusinessId[{1}]", id, businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveExpenseCategoryList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spExpenseCategoryListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();
                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();
                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not RetrieveExpenseCategoryList for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region peripheral

        public static GenericResponse<IList<OptionItem>> RetrieveAmountTypeList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            BaseResponse ret = cmd.RunSpQuery("spAmountTypeListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();
                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();
                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve AmountTypeList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }
        public static GenericResponse<IList<OptionItem>> RetrievePaymentTypeList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            BaseResponse ret = cmd.RunSpQuery("spPaymentTypeListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();
                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();
                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve PaymentTypeList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region Original

        public static BaseResponse ManagePurchaseOrderOriginal(PurchaseOrderDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@PurchaseOrderId", SqlDbType.BigInt, dto.PurchaseOrderId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@IsCashPurchase", SqlDbType.Bit, dto.IsCashPurchase);
            cmd.AddParam("@SupplierId", SqlDbType.BigInt,
                dto.SupplierId != -1 ? dto.SupplierId : (object)DBNull.Value);
            cmd.AddParam("@Reference", SqlDbType.NVarChar, dto.Reference);
            cmd.AddParam("@ContactPerson", SqlDbType.NVarChar, dto.ContactPerson);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, dto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, dto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, dto.Address3);
            cmd.AddParam("@Phone", SqlDbType.VarChar, dto.Phone);
            cmd.AddParam("@Date", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Date));
            cmd.AddParam("@DateDelivery", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.DateDelivery));
            cmd.AddParam("@AmountTypeId", SqlDbType.TinyInt, (byte)dto.AmountTypeId);
            cmd.AddParam("@PaymentTypeId", SqlDbType.TinyInt, (byte)dto.PaymentTypeId);
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            cmd.AddParam("@CreditTerm", SqlDbType.NVarChar, dto.CreditTerm);
            cmd.AddParam("@Subtotal", SqlDbType.Decimal, dto.Subtotal);
            cmd.AddParam("@Tax", SqlDbType.Decimal, dto.Tax);
            cmd.AddParam("@Total", SqlDbType.Decimal, dto.Total);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            // Prepare Items
            DataTable dt = ConstructPurchaseOrderItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.PurchaseOrderItemId,
                        dto.PurchaseOrderId,
                        item.ItemId,
                        item.Quantity,
                        item.Price,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spPurchaseOrderManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process PurchaseOrder[ {0} ]", dto.PurchaseOrderId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion
    }
}