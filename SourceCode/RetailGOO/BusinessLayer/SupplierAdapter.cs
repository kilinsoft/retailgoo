﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class SupplierAdapter
    {
        public static GenericResponse<IList<SupplierDto>> RetrieveAllSupplier()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<SupplierDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spSupplierAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<SupplierDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    SupplierDto supplierDto = new SupplierDto()
                    {
                        //BusinessId = businessId,
                        SupplierId = long.Parse(row["SupplierId"].ToString()),
                        SupplierName = row["SupplierName"].ToString(),
                        SupplierNo = row["SupplierNo"].ToString(),
                        //SupplierGroupId = 
                        //    row["SupplierGroupId"] != DBNull.Value ?
                        //        int.Parse(row["SupplierGroupId"].ToString()) :
                        //        -1,
                        SupplierGroupName = 
                            row["SupplierGroupName"] != DBNull.Value ?
                                row["SupplierGroupName"].ToString() :
                                DefaultNullableOptionItem.Text,
                        //TaxNo = row["TaxNo"].ToString(),
                        ContactPerson = row["ContactPerson"].ToString(),
                        //Email = row["Email"].ToString(),
                        Phone = row["Phone"].ToString()
                        //Fax = row["Fax"].ToString(),
                        //CreditTerm = row["CreditTerm"].ToString(),
                        //Address1 = row["Address1"].ToString(),
                        //Address2 = row["Address2"].ToString(),
                        //Address3 = row["Address3"].ToString(),
                        //City = row["City"].ToString(),
                        //CountryId = int.Parse(row["CountryId"].ToString()),
                        //CountryName = row["CountryName"].ToString(),
                        //ZipCode = row["ZipCode"].ToString(),
                        //Remark = row["Remark"].ToString(),
                        //Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"])
                    };
                    tmpResult.Add(supplierDto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SupplierInfo for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<SupplierDto> RetrieveSupplier(long supplierId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<SupplierDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@SupplierId", SqlDbType.BigInt, supplierId);
            BaseResponse ret = cmd.RunSpQuery("spSupplierRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    DataRow row = cmd.DataResult.Rows[0];
                    var item = new SupplierDto()
                    {
                        BusinessId = businessId,
                        SupplierId = long.Parse(row["SupplierId"].ToString()),
                        SupplierName = row["SupplierName"].ToString(),
                        SupplierNo = row["SupplierNo"].ToString(),
                        SupplierGroupId =
                            row["SupplierGroupId"] != DBNull.Value ?
                                int.Parse(row["SupplierGroupId"].ToString()) :
                                DefaultNullableOptionItem.Value,
                        SupplierGroupName =
                            row["SupplierGroupName"] != DBNull.Value ?
                                row["SupplierGroupName"].ToString() :
                                DefaultNullableOptionItem.Text,
                        TaxNo = row["TaxNo"].ToString(),
                        ContactPerson = row["ContactPerson"].ToString(),
                        Email = row["Email"].ToString(),
                        Phone = row["Phone"].ToString(),
                        Fax = row["Fax"].ToString(),
                        CreditTerm = row["CreditTerm"].ToString(),
                        Address1 = row["Address1"].ToString(),
                        Address2 = row["Address2"].ToString(),
                        Address3 = row["Address3"].ToString(),
                        City = row["City"].ToString(),
                        //Country =
                        //    row["Country"] != DBNull.Value ?
                        //        row["Country"].ToString() : "",
                        CountryId =
                            row["CountryId"] != DBNull.Value ?
                                int.Parse(row["CountryId"].ToString()) :
                                DefaultNullableOptionItem.Value,
                        CountryName = row["CountryName"] != DBNull.Value ?
                                row["CountryName"].ToString() :
                                DefaultNullableOptionItem.Text,
                        ZipCode = row["ZipCode"].ToString(),
                        Remark = row["Remark"].ToString(),
                        Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"])
                    };
                    result.ResponseItem = item;
                    result.Succeed();
                }
                else
                {
                    result.Fail(string.Format("Supplier[{0}] does not exist", supplierId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SupplierId[{0}] for BusinessId[{1}]", supplierId, businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveSupplierGroupList()
        {
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            BaseResponse ret = cmd.RunSpQuery("spSupplierGroupListRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<OptionItem>();
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.Text });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem item = new OptionItem()
                    {
                        Value = int.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SupplierGroupList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveSupplierList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();
            var tmpResult = new List<OptionItem>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spSupplierListRetrieve");
            if (ret.Success)
            {
                tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.BusinessPartnerText });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var pair = new OptionItem()
                    {
                        Value = long.Parse(row["Value"].ToString()),
                        Text = row["Text"].ToString()
                    };
                    tmpResult.Add(pair);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SupplierOptionItems for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageSupplier(SupplierDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("si", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} Supplier[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.SupplierId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            var result = new BaseResponse();
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();

            SqlCommandHelper cmd = new SqlCommandHelper();
            var ret = new BaseResponse();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@SupplierId", SqlDbType.BigInt, dto.SupplierId);
            cmd.AddParam("@SupplierName", SqlDbType.NVarChar, dto.SupplierName);
            cmd.AddParam("@SupplierNo", SqlDbType.NVarChar, dto.SupplierNo);
            cmd.AddParam("@SupplierGroupId", SqlDbType.TinyInt, 
                dto.SupplierGroupId != -1 ? dto.SupplierGroupId : (object) DBNull.Value);
            cmd.AddParam("@TaxNo", SqlDbType.NVarChar, dto.TaxNo);
            cmd.AddParam("@ContactPerson", SqlDbType.NVarChar, dto.ContactPerson);
            cmd.AddParam("@Email", SqlDbType.VarChar, dto.Email);
            cmd.AddParam("@Phone", SqlDbType.NVarChar, dto.Phone);
            cmd.AddParam("@Fax", SqlDbType.NVarChar, dto.Fax);
            cmd.AddParam("@CreditTerm", SqlDbType.NVarChar, dto.CreditTerm);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, dto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, dto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, dto.Address3);
            cmd.AddParam("@City", SqlDbType.NVarChar, dto.City);
            cmd.AddParam("@CountryId", SqlDbType.TinyInt, 
                dto.CountryId != -1 ? dto.CountryId : (object) DBNull.Value);
            cmd.AddParam("@ZipCode", SqlDbType.NVarChar, dto.ZipCode);
            cmd.AddParam("@Remark", SqlDbType.NVarChar, dto.Remark);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime,
                dto.Version != string.Empty ? DateTimeHelper.ConvertToDateTime(dto.Version) : DateTime.Now);

            ret = cmd.RunSpNonQuery("spSupplierManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("An Error occurred. {0}", ret.StatusText));
                Logger.SessionError(string.Format("Error ManageSupplier. BusinessId[{0}] SupplierId[{1}] SupplierName[{2}] ItemState [{3}]",
                    businessId,
                    dto.SupplierId,
                    dto.SupplierName,
                    dto.ItemState.ToString()));
            }

            return result;
        }
    }
}