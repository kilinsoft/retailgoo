﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class ReportAdapter
    {
        public static GenericResponse<string> RetrieveOutstandingItemCount()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<string>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spReportOutstandingRetrieve");
            if (ret.Success)
            {
                string retVal = "0,0,0,0,0,0"; // Pending SI Count; Overdue SI Count; Reorder Item Count; Pending PO Count; Overdue PO Count; Pending RO Count
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];
                    retVal = row["PendingPaymentSalesInvoiceCount"].ToString();
                    retVal += ",";
                    retVal += row["OverdueSalesInvoiceCount"].ToString();
                    retVal += ",";
                    retVal += row["ItemToReorderCount"].ToString();
                    retVal += ",";
                    retVal += row["PendingReceivingPurchaseOrderCount"].ToString();
                    retVal += ",";
                    retVal += row["OverduePurchaseOrderCount"].ToString();
                    retVal += ",";
                    retVal += row["PendingPaymentReceiveOrderCount"].ToString();
                }
                result.Succeed();
                result.ResponseItem = retVal;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve ReportOutstandingItems for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<List<ItemDto>> RetrieveTop5Items()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<List<ItemDto>>();

            int numItem = 5;// Top 5
            int numHistoryRange = 30; //Examine the cumulative quantity from the bill in the last 30 days

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@NumItem", SqlDbType.Int, numItem);
            cmd.AddParam("@NumHistoryRange", SqlDbType.Int, numHistoryRange);
            BaseResponse ret = cmd.RunSpQuery("spReportTopItemsRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<ItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    ItemDto item = new ItemDto();
                    item.ItemId = long.Parse(row["ItemId"].ToString());
                    item.ItemFullName = row["ItemFullName"].ToString();
                    item.Quantity = decimal.Parse(row["Amount"].ToString()); // Actually we should use "Amount" field but there is no field name "Amount" in ItemDto
                    tmpResult.Add(item);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve Top5Items for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }
    }
}