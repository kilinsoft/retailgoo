using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;

using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.BusinessLayer
{
    public class SalesAdapter
    {
        #region CashSales

        public static GenericResponse<IList<SalesInvoiceDto>> RetrieveAllCashSales()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<SalesInvoiceDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spCashSalesAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<SalesInvoiceDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    SalesInvoiceDto dto = new SalesInvoiceDto();

                    dto.SalesInvoiceId = long.Parse(row["SalesInvoiceId"].ToString());
                    dto.CashSalesNo = row["CashSalesNo"].ToString();
                    dto.CustomerId = row["CustomerId"] == (object)DBNull.Value ? -1 : long.Parse(row["CustomerId"].ToString());
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.Total = (decimal)row["Total"];
                    dto.Status = row["Status"].ToString();
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    dto.CustomerName = row["CustomerName"] == (object)DBNull.Value ? DefaultNullableOptionItem.BusinessPartnerText : row["CustomerName"].ToString();

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CashSales for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<SalesInvoiceItemDto>> RetrieveAllCashSalesItem(long SalesInvoiceId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<SalesInvoiceItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@SalesInvoiceId", SqlDbType.BigInt, SalesInvoiceId);
            BaseResponse ret = cmd.RunSpQuery("spCashSalesItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<SalesInvoiceItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    SalesInvoiceItemDto dto = new SalesInvoiceItemDto();

                    dto.SalesInvoiceItemId = long.Parse(row["SalesInvoiceItemId"].ToString());
                    dto.SalesInvoiceId = long.Parse(row["SalesInvoiceId"].ToString());
                    dto.ItemId = long.Parse(row["ItemId"].ToString());
                    dto.Quantity = (decimal)row["Quantity"];
                    dto.Price = (decimal)row["Price"];
                    dto.ItemFullName = row["ItemFullName"].ToString();
                    dto.Amount = (decimal)row["Quantity"] * (decimal)(decimal)row["Price"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CashSalesItem for SalesInvoiceId[{0}]", SalesInvoiceId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<SalesInvoiceDto> RetrieveCashSales(long SaleInvoiceId)
        {
            var result = new GenericResponse<SalesInvoiceDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@SalesInvoiceId", SqlDbType.BigInt, SaleInvoiceId);
            BaseResponse ret = cmd.RunSpQuery("spCashSalesRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    SalesInvoiceDto dto = new SalesInvoiceDto();

                    dto.SalesInvoiceId = long.Parse(row["SalesInvoiceId"].ToString());
                    dto.CashSalesNo = row["CashSalesNo"].ToString();
                    dto.CustomerId = row["CustomerId"] == (object)DBNull.Value ? -1 : long.Parse(row["CustomerId"].ToString());
                    dto.Reference = row["Reference"].ToString();
                    dto.ContactPerson = row["ContactPerson"].ToString();
                    dto.Address1 = row["Address1"].ToString();
                    dto.Address2 = row["Address2"].ToString();
                    dto.Address3 = row["Address3"].ToString();
                    dto.Phone = row["Phone"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.DateDelivery = DateTimeHelper.ConvertToDateString((DateTime)row["DateDelivery"]);
                    dto.DestinationTypeId = row["DestinationTypeId"] == (object)DBNull.Value ? -1 : int.Parse(row["DestinationTypeId"].ToString());
                    dto.PriceTypeId = row["PriceTypeId"] == (object)DBNull.Value ? -1 : int.Parse(row["PriceTypeId"].ToString());
                    dto.AmountTypeId = row["AmountTypeId"] == (object)DBNull.Value ? -1 : int.Parse(row["AmountTypeId"].ToString());
                    dto.PaymentTypeId = row["PaymentTypeId"] == (object)DBNull.Value? -1 : int.Parse(row["PaymentTypeId"].ToString());
                    dto.Note = row["Note"].ToString();
                    dto.SalesPersonId = long.Parse(row["SalesPersonId"].ToString());
                    dto.Subtotal = (decimal)row["Subtotal"];
                    dto.Discount = (decimal)row["Discount"];
                    dto.Charge = (decimal)row["Charge"];
                    dto.Tax = (decimal)row["Tax"];
                    dto.Total = (decimal)row["Total"];
                    dto.Status = row["Status"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    dto.CustomerName = row["CustomerName"] == (object)DBNull.Value ? DefaultNullableOptionItem.BusinessPartnerText : row["CustomerName"].ToString();

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("CashSales[{0}] does not exist", SaleInvoiceId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve CashSale for SalesInvoiceId[{0}]", SaleInvoiceId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageCashSales(SalesInvoiceDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("cs", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} CashSales[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.SalesInvoiceId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            return ManageSalesInvoiceOriginal(dto);
        }

        public static BaseResponse VoidCashSales(long SalesInvoiceId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@SalesInvoiceId", SqlDbType.BigInt, SalesInvoiceId);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            BaseResponse ret = cmd.RunSpNonQuery("spCashSalesVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void CashSales. BusinessId[{0}] SalesInvoiceId[{1}]", businessId, SalesInvoiceId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region SalesInvoice

        public static GenericResponse<IList<SalesInvoiceDto>> RetrieveAllSalesInvoice()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<SalesInvoiceDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spSalesInvoiceAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<SalesInvoiceDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    SalesInvoiceDto dto = new SalesInvoiceDto();

                    dto.SalesInvoiceId = long.Parse(row["SalesInvoiceId"].ToString());
                    dto.SalesInvoiceNo = row["SalesInvoiceNo"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.CustomerName = row["CustomerName"] == (object)DBNull.Value ? DefaultNullableOptionItem.BusinessPartnerText : row["CustomerName"].ToString();
                    dto.Total = (decimal)row["Total"];
                    dto.Status = row["Status"].ToString();
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SalesInvoice for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<SalesInvoiceItemDto>> RetrieveAllSalesInvoiceItem(long SalesInvoiceId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<SalesInvoiceItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@SalesInvoiceId", SqlDbType.BigInt, SalesInvoiceId);
            BaseResponse ret = cmd.RunSpQuery("spSalesInvoiceItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<SalesInvoiceItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    SalesInvoiceItemDto dto = new SalesInvoiceItemDto();

                    dto.SalesInvoiceItemId = long.Parse(row["SalesInvoiceItemId"].ToString());
                    dto.SalesInvoiceId = long.Parse(row["SalesInvoiceId"].ToString());
                    dto.ItemId = long.Parse(row["ItemId"].ToString());
                    dto.Quantity = (decimal)row["Quantity"];
                    dto.Price = (decimal)row["Price"];
                    dto.ItemFullName = row["ItemFullName"].ToString();
                    dto.Amount = (decimal)row["Quantity"] * (decimal)row["Price"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SalesInvoiceItem for SalesInvoiceId[{0}]", SalesInvoiceId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<SalesInvoiceDto> RetrieveSalesInvoice(long SalesInvoiceId)
        {
            var result = new GenericResponse<SalesInvoiceDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@SalesInvoiceId", SqlDbType.BigInt, SalesInvoiceId);
            BaseResponse ret = cmd.RunSpQuery("spSalesInvoiceRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    SalesInvoiceDto dto = new SalesInvoiceDto();

                    dto.SalesInvoiceId = long.Parse(row["SalesInvoiceId"].ToString());
                    dto.SalesInvoiceNo = row["SalesInvoiceNo"].ToString();
                    dto.IsCashSales = (bool)row["IsCashSales"];
                    dto.CashSalesNo = row["CashSalesNo"].ToString();
                    dto.CustomerId = row["CustomerId"] == (object)DBNull.Value ? -1 : long.Parse(row["CustomerId"].ToString());
                    dto.Reference = row["Reference"].ToString();
                    dto.ContactPerson = row["ContactPerson"].ToString();
                    dto.Address1 = row["Address1"].ToString();
                    dto.Address2 = row["Address2"].ToString();
                    dto.Address3 = row["Address3"].ToString();
                    dto.Phone = row["Phone"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.DateDelivery = DateTimeHelper.ConvertToDateString((DateTime)row["DateDelivery"]);
                    dto.DestinationTypeId = row["DestinationTypeId"] == (object)DBNull.Value ? -1 : int.Parse(row["DestinationTypeId"].ToString());
                    dto.PriceTypeId = row["PriceTypeId"] == (object)DBNull.Value ? -1 : int.Parse(row["PriceTypeId"].ToString());
                    dto.AmountTypeId = row["AmountTypeId"] == (object)DBNull.Value ? -1 : int.Parse(row["AmountTypeId"].ToString());
                    dto.PaymentTypeId = row["PaymentTypeId"] == (object)DBNull.Value ? -1 : int.Parse(row["PaymentTypeId"].ToString());
                    dto.Note = row["Note"].ToString();
                    dto.SalesPersonId = long.Parse(row["SalesPersonId"].ToString());
                    dto.Subtotal = (decimal)row["Subtotal"];
                    dto.Discount = (decimal)row["Discount"];
                    dto.Charge = (decimal)row["Charge"];
                    dto.Tax = (decimal)row["Tax"];
                    dto.Total = (decimal)row["Total"];
                    dto.Status = row["Status"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.AmountPaid = (decimal)row["AmountPaid"];

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("Invoice[{0}] does not exist", SalesInvoiceId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve SalesInvoice for SalesInvoiceId[{0}]", SalesInvoiceId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<OptionItem>> RetrieveSalesInvoiceList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            BaseResponse ret = cmd.RunSpQuery("spSalesInvoiceAllRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();

                    item.Value = (long)row["SalesInvoiceId"];
                    item.Text = row["SalesInvoiceNo"].ToString();

                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve SalesInvoiceList for BusinessId[ {0} ]", businessId));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageSalesInvoice(SalesInvoiceDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("siv", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} SalesInvoice[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.SalesInvoiceId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            return ManageSalesInvoiceOriginal(dto);
        }

        public static DataTable ConstructSalesInvoiceItemTable()
        {
            DataTable dt = new DataTable("SalesInvoiceItem");
            dt.Columns.Add("SalesInvoiceItemId", typeof(long));
            dt.Columns.Add("SalesInvoiceId", typeof(long));
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("Price", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        public static BaseResponse VoidSalesInvoice(long SalesInvoiceId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@SalesInvoiceId", SqlDbType.BigInt, SalesInvoiceId);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            BaseResponse ret = cmd.RunSpNonQuery("spSalesInvoiceVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void SalesInvoice. BusinessId[{0}] SalesInvoiceId[{1}]", businessId, SalesInvoiceId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #region SalesPerson

        public static GenericResponse<IList<OptionItem>> RetrieveSalesPersonList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();
            var tmpResult = new List<OptionItem>();
            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spSalesPersonListRetrieve");
            if (ret.Success)
            {
                //tmpResult.Add(new OptionItem() { Value = DefaultNullableOptionItem.Value, Text = DefaultNullableOptionItem.BusinessPartnerText });
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    OptionItem dto = new OptionItem();

                    dto.Value = long.Parse(row["Value"].ToString());
                    dto.Text = row["Text"].ToString();

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve RetrieveSalesPersonList for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #endregion

        #region IncomingPayment

        public static GenericResponse<IList<IncomingPaymentDto>> RetrieveAllIncomingPayment()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<IncomingPaymentDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spIncomingPaymentAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<IncomingPaymentDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    IncomingPaymentDto dto = new IncomingPaymentDto();

                    dto.IncomingPaymentId = long.Parse(row["IncomingPaymentId"].ToString());
                    dto.IncomingPaymentNo = row["IncomingPaymentNo"].ToString();
                    dto.Reference = row["Reference"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.Status = row["Status"].ToString();
                    dto.CustomerId = row["CustomerId"] == (object)DBNull.Value ? -1 : long.Parse(row["CustomerId"].ToString());
                    dto.Total = (decimal)row["Total"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    dto.CustomerName = row["CustomerName"] == (object)DBNull.Value ? DefaultNullableOptionItem .BusinessPartnerText : row["CustomerName"].ToString();

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve IncomingPayment for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<IncomingPaymentItemDto>> RetrieveAllIncomingPaymentItem(long IncomingPaymentId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<IncomingPaymentItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@IncomingPaymentId", SqlDbType.BigInt, IncomingPaymentId);
            BaseResponse ret = cmd.RunSpQuery("spIncomingPaymentItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<IncomingPaymentItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    IncomingPaymentItemDto dto = new IncomingPaymentItemDto();

                    //dto.IncomingPaymentItemId = long.Parse(row["IncomingPaymentItemId"].ToString());
                    dto.SalesInvoiceId = long.Parse(row["SalesInvoiceId"].ToString());
                    dto.SalesInvoiceNo = row["SalesInvoiceNo"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.Amount = (decimal)row["Total"];
                    dto.Due = (decimal)row["AmountDue"];
                    dto.Pay = (decimal)row["AmountPaid"];
                    //dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve IncomingPaymentItem for IncomingPaymentId[{0}]", IncomingPaymentId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IncomingPaymentDto> RetrieveIncomingPayment(long IncomingPaymentId)
        {
            var result = new GenericResponse<IncomingPaymentDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@IncomingPaymentId", SqlDbType.BigInt, IncomingPaymentId);
            BaseResponse ret = cmd.RunSpQuery("spIncomingPaymentRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    IncomingPaymentDto dto = new IncomingPaymentDto();

                    dto.IncomingPaymentId = long.Parse(row["IncomingPaymentId"].ToString());
                    dto.IncomingPaymentNo = row["IncomingPaymentNo"].ToString();
                    dto.CustomerId = row["CustomerId"] == (object)DBNull.Value ? -1 : long.Parse(row["CustomerId"].ToString());
                    dto.Reference = row["Reference"].ToString();
                    dto.PaymentTypeId = row["PaymentTypeId"] == (object)DBNull.Value ? -1 : long.Parse(row["PaymentTypeId"].ToString());
                    dto.BankId = row["BankId"] == (object)DBNull.Value ? -1 : long.Parse(row["BankId"].ToString());
                    dto.BankAccNo = row["BankAccNo"].ToString();
                    dto.Address1 = row["Address1"].ToString();
                    dto.Address2 = row["Address2"].ToString();
                    dto.Address3 = row["Address3"].ToString();
                    dto.Phone = row["Phone"].ToString();
                    dto.Date = DateTimeHelper.ConvertToDateString((DateTime)row["Date"]);
                    dto.Note = row["Note"].ToString();
                    dto.Subtotal = (decimal)row["Subtotal"];
                    dto.Tax = (decimal)row["Tax"];
                    dto.Total = (decimal)row["Total"];
                    dto.Status = row["Status"].ToString();
                    dto.IsDisabled = (bool)row["IsDisabled"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    //dto.CustomerName = row["CustomerName"].ToString();

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("IncomingPayment[{0}] does not exist", IncomingPaymentId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve IncomingPayment for IncomingPaymentId[{0}]", IncomingPaymentId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static BaseResponse ManageIncomingPayment(IncomingPaymentDto dto)
        {
            // check the screen permission
            Staff staff = SessionHelper.GetCurrentStaff();
            if (!staff.Permission.hasScreenPermission("ip", dto.ItemState))
            {
                var result_permission = new BaseResponse();

                result_permission.Fail(string.Format("You do not have permission to {0} IncomingPayment[ {1} ]",
                    staff.Permission.GetFullItemStateString(dto.ItemState),
                    dto.IncomingPaymentId));
                Logger.SessionError(string.Format("{0}", result_permission.StatusText));

                return result_permission;
            }

            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@IncomingPaymentId", SqlDbType.BigInt, dto.IncomingPaymentId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@CustomerId", SqlDbType.BigInt,
                dto.CustomerId != -1 ? dto.CustomerId : (object)DBNull.Value);
            cmd.AddParam("@Reference", SqlDbType.NVarChar, dto.Reference);
            cmd.AddParam("@PaymentTypeId", SqlDbType.BigInt,
                dto.PaymentTypeId != -1 ? dto.PaymentTypeId : (object)DBNull.Value);
            cmd.AddParam("@BankId", SqlDbType.BigInt,
                dto.BankId != -1 ? dto.BankId : (object)DBNull.Value);
            cmd.AddParam("@BankAccNo", SqlDbType.NVarChar, dto.BankAccNo);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, dto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, dto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, dto.Address3);
            cmd.AddParam("@Phone", SqlDbType.VarChar, dto.Phone);
            cmd.AddParam("@Date", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Date));
            cmd.AddParam("@Vat", SqlDbType.Bit, dto.Vat);
            cmd.AddParam("@Wht", SqlDbType.Bit, dto.Wht);
            cmd.AddParam("@WhtPercent", SqlDbType.Decimal, dto.WhtPercent);
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            cmd.AddParam("@Subtotal", SqlDbType.Decimal, dto.Subtotal);
            cmd.AddParam("@Tax", SqlDbType.Decimal, dto.Tax);
            cmd.AddParam("@Total", SqlDbType.Decimal, dto.Total);
            //cmd.AddParam("@Status", SqlDbType.VarChar, dto.Status);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            // Prepare Items
            DataTable dt = ConstructIncomingPaymentItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.IncomingPaymentItemId,
                        dto.IncomingPaymentId,
                        item.SalesInvoiceId,
                        item.Pay,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spIncomingPaymentManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process IncomingPayment for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructIncomingPaymentItemTable()
        {
            DataTable dt = new DataTable("IncomingPaymentItem");
            dt.Columns.Add("IncomingPaymentItemId", typeof(long));
            dt.Columns.Add("IncomingPaymentId", typeof(long));
            dt.Columns.Add("SalesInvoiceId", typeof(long));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        public static BaseResponse VoidIncomingPayment(long IncomingPaymentId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            //cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            cmd.AddParam("@IncomingPaymentId", SqlDbType.BigInt, IncomingPaymentId);
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            BaseResponse ret = cmd.RunSpNonQuery("spIncomingPaymentVoid");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not void IncomingPayment. BusinessId[{0}] IncomingPaymentId[{1}]", businessId, IncomingPaymentId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region POS

        public static GenericResponse<IList<BillDto>> RetrieveAllPendingBill()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<BillDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BusinessId", SqlDbType.BigInt, SessionHelper.GetCurrentBusinessId());
            BaseResponse ret = cmd.RunSpQuery("spPendingBillAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<BillDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    BillDto dto = new BillDto();

                    dto.BillId = long.Parse(row["BillId"].ToString());
                    dto.CustomerId = row["CustomerId"] == (object)DBNull.Value ? -1 : long.Parse(row["CustomerId"].ToString());
                    dto.CustomerName = row["CustomerName"] == (object)DBNull.Value ? DefaultNullableOptionItem.HoldSaleCustomerName : row["CustomerName"].ToString();
                    dto.Quantity = (decimal)row["Quantity"];
                    dto.Total = (decimal)row["Total"];
                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve AllHoldSale for BusinessId[{0}]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<BillDto> RetrieveBill(long billId)
        {
            var result = new GenericResponse<BillDto>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BillId", SqlDbType.BigInt, billId);
            BaseResponse ret = cmd.RunSpQuery("spBillRetrieve");
            if (ret.Success)
            {
                if (cmd.DataResult.Rows.Count > 0)
                {
                    var row = cmd.DataResult.Rows[0];

                    BillDto dto = new BillDto();

                    dto.BillId = long.Parse(row["BillId"].ToString());
                    dto.CustomerId = row["CustomerId"] == (object)DBNull.Value ? -1 : long.Parse(row["CustomerId"].ToString());
                    dto.CustomerName = row["CustomerName"] == (object)DBNull.Value ? DefaultNullableOptionItem.HoldSaleCustomerName : row["CustomerName"].ToString();
                    dto.DiscountAmount = (decimal)row["DiscountAmount"];
                    dto.DiscountPercent = (decimal)row["DiscountPercent"];
                    dto.IsDiscountPercent = (bool)row["IsDiscountPercent"];
                    dto.Subtotal = (decimal)row["Subtotal"];
                    dto.Total = (decimal)row["Total"];
                    dto.Tax = (decimal)row["Tax"];
                    //dto.AmountReceived = (decimal)row["AmountReceived"];
                    //dto.AmountChanged = (decimal)row["AmountChanged"];
                    dto.Status = row["Status"].ToString();
                    dto.Note = row["Note"].ToString();

                    dto.Version = DateTimeHelper.ConvertToDateTimeString((DateTime)row["DateModified"]);

                    // Retrieve BillItems
                    var billItems = RetrieveAllBillItem(billId);
                    if (billItems.Success && billItems.ResponseItem.Count > 0)
                        dto.BillItems = billItems.ResponseItem;
                    else
                        dto.BillItems = new List<BillItemDto>();
                    // Retrieve BillPaymentItems
                    var billPaymentItems = RetrieveAllBillPaymentItem(billId);
                    if (billPaymentItems.Success)
                    {
                        var items = billPaymentItems.ResponseItem;
                        dto.BillPaymentItems = items;
                        if (items.Count > 0)
                        {
                            for (int i = 0; i < items.Count; ++i)
                                dto.AmountReceived = items[i].Amount;
                        }
                    }
                    else
                        dto.BillPaymentItems = new List<BillPaymentItemDto>();

                    result.Succeed();
                    result.ResponseItem = dto;
                }
                else
                {
                    result.Fail(string.Format("BillId[{0}] does not exist", billId));
                    Logger.SessionError(result.StatusText);
                }
            }
            else
            {
                result.Fail(string.Format("Could not retrieve Bill for BillId[{0}]", billId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<BillItemDto>> RetrieveAllBillItem(long BillId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<BillItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BillId", SqlDbType.BigInt, BillId);
            BaseResponse ret = cmd.RunSpQuery("spBillItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<BillItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    BillItemDto dto = new BillItemDto();

                    dto.BillItemId = long.Parse(row["BillItemId"].ToString());
                    dto.BillId = long.Parse(row["BillId"].ToString());
                    dto.ItemId = long.Parse(row["ItemId"].ToString());
                    dto.ItemFullName = row["ItemFullName"].ToString();
                    dto.Quantity = (decimal)row["Quantity"];
                    dto.Price = (decimal)row["Price"];
                    dto.Discount = (decimal)row["Discount"];

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve BillItem for BillId[{0}]", BillId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<IList<BillPaymentItemDto>> RetrieveAllBillPaymentItem(long BillId)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<BillPaymentItemDto>>();

            SqlCommandHelper cmd = new SqlCommandHelper();

            cmd.AddParam("@BillId", SqlDbType.BigInt, BillId);
            BaseResponse ret = cmd.RunSpQuery("spBillPaymentItemAllRetrieve");
            if (ret.Success)
            {
                var tmpResult = new List<BillPaymentItemDto>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var dto = new BillPaymentItemDto();

                    dto.BillPaymentItemId = long.Parse(row["BillPaymentItemId"].ToString());
                    dto.BillId = long.Parse(row["BillId"].ToString());
                    dto.PaymentTypeId = int.Parse(row["PaymentTypeId"].ToString());
                    dto.Amount = (decimal)row["Amount"];

                    tmpResult.Add(dto);
                }
                result.Succeed();
                result.ResponseItem = tmpResult;
            }
            else
            {
                result.Fail(string.Format("Could not retrieve BillPaymentItem for BillId[{0}]", BillId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static GenericResponse<long> SubmitHoldSale(BillDto dto)
        {
            var ret = new GenericResponse<long>();
            if (dto.BillItems.Count == 0)
            {
                ret.Fail("No item selected");
                return ret;
            }
            dto.Status = BillStatus.Open;
            return ManageBill(dto);
        }

        public static GenericResponse<long> CancelHoldSale(BillDto dto)
        {
            dto.ItemState = ItemState.Deleted;
            for (int i = 0; i < dto.BillItems.Count; ++i)
                dto.BillItems[i].ItemState = ItemState.Deleted;
            for (int i = 0; i < dto.BillPaymentItems.Count; ++i)
                dto.BillPaymentItems[i].ItemState = ItemState.Deleted;

            return ManageBill(dto);
        }

        public static GenericResponse<long> SubmitPaidBill(BillDto dto)
        {
            var ret = new GenericResponse<long>();
            decimal sum = 0;
            for (int i = 0; i < dto.BillPaymentItems.Count; ++i) {
                var pItem = dto.BillPaymentItems[i];
                if (!pItem.IsDisabled)
                    sum += pItem.Amount;
            }
            if (sum < dto.Total)
            {
                ret.Fail("Insufficient Payment");
                return ret;
            }

            dto.Status = BillStatus.Done;
            return ManageBill(dto);
        }

        public static GenericResponse<long> ManageBill(BillDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new GenericResponse<long>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@CustomerId", SqlDbType.BigInt,
                dto.CustomerId != -1 ? dto.CustomerId : (object)DBNull.Value);
            cmd.AddParam("@BillId", SqlDbType.BigInt, dto.BillId);
            cmd.AddParam("@DiscountAmount", SqlDbType.Decimal, dto.DiscountAmount);
            cmd.AddParam("@DiscountPercent", SqlDbType.Decimal, dto.AmountReceived);
            cmd.AddParam("@IsDiscountPercent", SqlDbType.Decimal, dto.IsDiscountPercent);
            cmd.AddParam("@Subtotal", SqlDbType.Decimal, dto.Subtotal);
            cmd.AddParam("@Total", SqlDbType.Decimal, dto.Total);
            cmd.AddParam("@Tax", SqlDbType.Decimal, dto.Tax);
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            //cmd.AddParam("@AmountReceived", SqlDbType.Decimal, dto.AmountReceived);
            //cmd.AddParam("@AmountChanged", SqlDbType.Decimal, dto.AmountChanged);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);
            cmd.AddParam("@Status", SqlDbType.VarChar, dto.Status);
            cmd.AddOutputParam("@NewBillId", SqlDbType.BigInt);

            // Prepare BillItems
            DataTable dt = ConstructBillItemTable();
            if (dto.BillItems != null)
            {
                for (int i = 0; i < dto.BillItems.Count; ++i)
                {
                    var item = dto.BillItems[i];
                    dt.Rows.Add(
                        item.BillItemId,
                        dto.BillId,
                        item.ItemId,
                        item.Quantity,
                        item.Discount,
                        item.Price,
                        item.ItemState);
                }
            }
            cmd.AddParam("@BillItems", SqlDbType.Structured, dt);

            // Prepare BillPaymentItems
            dt = ConstructBillPaymentItemTable();
            if (dto.BillPaymentItems != null)
            {
                for (int i = 0; i < dto.BillPaymentItems.Count; ++i)
                {
                    var item = dto.BillPaymentItems[i];
                    dt.Rows.Add(
                        item.BillPaymentItemId,
                        dto.BillId,
                        item.PaymentTypeId,
                        item.Amount,
                        item.ItemState);
                }
            }
            cmd.AddParam("@BillPaymentItems", SqlDbType.Structured, dt);

            BaseResponse ret = cmd.RunSpNonQuery("spBillManage");
            if (ret.Success)
            {
                // Return the newly created ID
                if (dto.ItemState == ItemState.New)
                {
                    if (cmd.OutParams["@NewBillId"] != DBNull.Value)
                    {
                        long newId = long.Parse(cmd.OutParams["@NewBillId"].ToString());
                        result.ResponseItem = newId;
                    }
                    else
                    {
                        result.Fail("Could not store this item in database");
                    }
                }
                else if (dto.ItemState == ItemState.Modified)
                {
                    result.ResponseItem = dto.BillId;
                } 

                result.Succeed();

            }
            else
            {
                result.Fail(string.Format("Could not process Bill for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        public static DataTable ConstructBillItemTable()
        {
            DataTable dt = new DataTable("BillItem");
            dt.Columns.Add("BillItemId", typeof(long));
            dt.Columns.Add("BillId", typeof(long));
            dt.Columns.Add("ItemId", typeof(long));
            dt.Columns.Add("Quantity", typeof(decimal));
            dt.Columns.Add("Discount", typeof(decimal));
            dt.Columns.Add("Price", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        public static DataTable ConstructBillPaymentItemTable()
        {
            DataTable dt = new DataTable("BillPaymentItem");
            dt.Columns.Add("BillPaymentItemId", typeof(long));
            dt.Columns.Add("BillId", typeof(long));
            dt.Columns.Add("PaymentTypeId", typeof(long));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("ItemState", typeof(string));
            return dt;
        }

        #endregion POS

        #region Other

        public static GenericResponse<IList<OptionItem>> RetrieveDestinationTypeList()
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            var result = new GenericResponse<IList<OptionItem>>();

            SqlCommandHelper cmd = new SqlCommandHelper();
            BaseResponse ret = cmd.RunSpQuery("spDestinationTypeListRetrieve");
            if (ret.Success)
            {
                var items = new List<OptionItem>();
                foreach (DataRow row in cmd.DataResult.Rows)
                {
                    var item = new OptionItem();
                    item.Value = long.Parse(row["Value"].ToString());
                    item.Text = row["Text"].ToString();
                    items.Add(item);
                }
                result.Succeed();
                result.ResponseItem = items;
            }
            else
            {
                result.Fail(string.Format("Could not Retrieve DestinationTypeList"));
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion

        #region Original

        public static BaseResponse ManageSalesInvoiceOriginal(SalesInvoiceDto dto)
        {
            long businessId = SessionHelper.GetCurrentBusinessId();
            long userId = SessionHelper.GetCurrentUserId();
            var result = new BaseResponse();

            SqlCommandHelper cmd = new SqlCommandHelper();
            cmd.AddParam("@SalesInvoiceId", SqlDbType.BigInt, dto.SalesInvoiceId);
            cmd.AddParam("@BusinessId", SqlDbType.BigInt, businessId);
            cmd.AddParam("@IsCashSales", SqlDbType.Bit, dto.IsCashSales);
            cmd.AddParam("@CustomerId", SqlDbType.BigInt,
                dto.CustomerId != -1 ? dto.CustomerId : (object)DBNull.Value);
            cmd.AddParam("@Reference", SqlDbType.NVarChar, dto.Reference);
            cmd.AddParam("@ContactPerson", SqlDbType.NVarChar, dto.ContactPerson);
            cmd.AddParam("@Address1", SqlDbType.NVarChar, dto.Address1);
            cmd.AddParam("@Address2", SqlDbType.NVarChar, dto.Address2);
            cmd.AddParam("@Address3", SqlDbType.NVarChar, dto.Address3);
            cmd.AddParam("@Phone", SqlDbType.VarChar, dto.Phone);
            cmd.AddParam("@Date", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Date));
            cmd.AddParam("@DateDelivery", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.DateDelivery));
            cmd.AddParam("@DestinationTypeId", SqlDbType.TinyInt,
                dto.DestinationTypeId != -1 ? dto.DestinationTypeId : (object)DBNull.Value);
            cmd.AddParam("@PriceTypeId", SqlDbType.TinyInt,
                dto.PriceTypeId != -1 ? dto.PriceTypeId : (object)DBNull.Value);
            cmd.AddParam("@AmountTypeId", SqlDbType.TinyInt,
                dto.AmountTypeId != -1 ? dto.AmountTypeId : (object)DBNull.Value);
            cmd.AddParam("@PaymentTypeId", SqlDbType.TinyInt,
                dto.PaymentTypeId != -1 ? dto.PaymentTypeId : (object)DBNull.Value);
            cmd.AddParam("@Note", SqlDbType.NVarChar, dto.Note);
            cmd.AddParam("@SalesPersonId", SqlDbType.BigInt, dto.SalesPersonId);
            cmd.AddParam("@Subtotal", SqlDbType.Decimal, dto.Subtotal);
            cmd.AddParam("@Discount", SqlDbType.Decimal, dto.Discount);
            cmd.AddParam("@Charge", SqlDbType.Decimal, dto.Charge);
            cmd.AddParam("@Tax", SqlDbType.Decimal, dto.Tax);
            cmd.AddParam("@Total", SqlDbType.Decimal, dto.Total);
            cmd.AddParam("@AmountPaid", SqlDbType.Decimal, dto.AmountPaid);
            //cmd.AddParam("@Status", SqlDbType.VarChar, dto.Status);
            cmd.AddParam("@ItemState", SqlDbType.Char, dto.ItemState);
            cmd.AddParam("@Version", SqlDbType.DateTime, DateTimeHelper.ConvertToDateTime(dto.Version));
            cmd.AddParam("@UserId", SqlDbType.BigInt, userId);

            // Prepare Items
            DataTable dt = ConstructSalesInvoiceItemTable();
            if (dto.Items != null)
            {
                for (int i = 0; i < dto.Items.Count; ++i)
                {
                    var item = dto.Items[i];
                    dt.Rows.Add(
                        item.SalesInvoiceItemId,
                        dto.SalesInvoiceId,
                        item.ItemId,
                        item.Quantity,
                        item.Price,
                        item.ItemState);
                }
            }
            cmd.AddParam("@Items", SqlDbType.Structured, dt);
            BaseResponse ret = cmd.RunSpNonQuery("spSalesInvoiceManage");
            if (ret.Success)
            {
                result.Succeed();
            }
            else
            {
                result.Fail(string.Format("Could not process SalesInvoice for BusinessId[ {0} ]", businessId));
                result.SetDebugText(ret.StatusText);
                Logger.SessionError(string.Format("{0}. {1}", result.StatusText, ret.StatusText));
            }
            return result;
        }

        #endregion
    }
}