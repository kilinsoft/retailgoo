﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RetailGOO.BusinessLayer;
using RetailGOO.Dto;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO
{
    public partial class ProcessLink : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = "";
            string value = "";
            if (Request.QueryString["type"] != null)
                type = Request.QueryString["type"];
            if (Request.QueryString["value"] != null)
                value = Request.QueryString["value"];

            //lblText.Text = string.Format("Type = [{0}], Value = [{1}]", type, value);
            BaseResponse rsp = Identity.ProcessGeneratedLink(type, value);
            if (rsp.Success)
            {
                lblText.Text = "Your action has been successfully processed. Please browse to RetailGOO";
            }
            else
            {
                lblText.Text = rsp.GetFullText();
            }
            string targetUrl = WebsiteHelper.ConvertToAbsoluteUrl(RetailGooPageUrl.LogInPage);
            hlTargetPage.Target = targetUrl;
            hlTargetPage.Text = "Retail GOO website";
        }
    }
}