﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="RetailGOO.LogOut" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <script type="text/javascript">
        function onLoad() {
            setTimeout(function () { window.location = "Login.aspx"; }, 2000);
        }
    </script>
    <title></title>
</head>
<body onload="onLoad()">
    <form id="form1" runat="server">
    <div>
    You are being redirected to the Login page
    </div>
    </form>
</body>
</html>
