﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Printable.Master" AutoEventWireup="true" CodeBehind="PurchaseOrderA.aspx.cs" Inherits="RetailGOO.Printable.PurchaseOrderA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style type="text/css">

    </style>
    <script type="text/javascript">

        var nMaxWaitAttempts = 3;
        var item = null;

        function pageLoad() {
            waitForData();
        }
        function waitForData() {
            var dto = window.dto; // receive this dto data from parent window;
            if (dto == null) {
                if (nMaxWaitAttempts > 0) {
                    --nMaxWaitAttempts;
                    setTimeout(waitForData, 1000);
                }
            }
            else {
                renderPrintableData(dto);
                doDefaultPrint();
            }
        }
        function renderPrintableData(item) {
            $("#txtName").val(item["SupplierName"])
            $("#txtRef").val(item["Reference"]);
            $("#txtContactPerson").val(item["ContactPerson"]);
            $("#txtPoNo").val(item["PurchaseOrderNo"]);
            $("#txtDate").val(item["Date"]);
            $("#txtDateDelivery").val(item["DateDelivery"]);
            $("#txtAddr1").val(item["Address1"]);
            $("#txtAddr2").val(item["Address2"]);
            $("#txtAddr3").val(item["Address3"]);
            $("#txtStatus").val(item["Status"]);
            $("#txtPhone").val(item["Phone"]);
            $("#txtCreditTerm").val(item["CreditTerm"]);
            $("#txtPaymentMethod").val(item["PaymentTypeName"]);
            $("#txtNote").val(item["Note"]);

            $("#txtSubtotal").val(convertToAmount(item["Subtotal"]));
            $("#txtTax").val(convertToAmount(item["Tax"]));
            $("#txtTotal").val(convertToAmount(item["Total"]));

            var subItems = item["Items"];

            for (var i = 0; i < subItems.length; ++i) {
                var subItem = subItems[i];
                var $row = $("<tr></tr>");
                $row.append("<td>" + subItem["ItemFullName"] + "</td>");
                $row.append("<td>" + convertToAmount(subItem["Quantity"], 0) + "</td>");
                $row.append("<td>" + subItem["UnitOfMeasure"] + "</td>");
                $row.append("<td>" + subItem["Price"] + "</td>");
                $row.append("<td>" + subItem["Amount"] + "</td>");
                $("#grid tbody").append($row);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="printable theme-black">
        <div class="caption section">
            Purchase Order
        </div>
        <div class="content-form section">
            <table id="tblForm" class="form double">
                <tbody>
                    <tr>
                        <td>Name:</td>
                        <td>
                            <input type="text" id="txtName" />
                        </td>
                        <td>Reference:</td>
                        <td>
                            <input type="text" id="txtRef" />
                        </td>
                    </tr>
                    <tr>
                        <td>Contact:</td>
                        <td>
                            <input type="text" id="txtContactPerson" />
                        </td>
                        <td>PO Number:</td>
                        <td>
                            <input type="text" id="txtPoNo" />
                        </td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td>
                            <input type="text" id="txtAddr1" />
                        </td>
                        <td>Date:</td>
                        <td>
                            <input type="text" id="txtDate" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="text" id="txtAddr2" />
                            <input type="text" id="txtAddr3" hidden />
                        </td>
                        <td>Status:</td>
                        <td>
                            <input type="text" id="txtStatus" />
                        </td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td>
                            <input type="text" id="txtPhone" />
                        </td>
                        <td>Payment Method:</td>
                        <td>
                            <input type="text" id="txtPaymentMethod" />
                        </td>
                    </tr>
                    <tr>
                        <td>Delivery Date:</td>
                        <td>
                            <input type="text" id="txtDateDelivery" />
                        </td>
                        <td>Credit Term:</td>
                        <td>
                            <input type="text" id="txtCreditTerm" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="content-grid section">
            <table id="grid" class="grid-printable">
                <thead>
                    <tr>
                        <th style="width: 400px">Item</th>
                        <th style="width: 100px">Quantity</th>
                        <th style="width: 100px">UOM</th>
                        <th style="width: 100px">Price</th>
                        <th style="width: 100px">Amount</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="summary section">
            <table class="form double">
                <tr>
                    <td>Note:</td>
                    <td>
                        <input type="text" id="txtNote" />
                    </td>
                    <td class="summary-caption">Sub total:</td>
                    <td>
                        <input type="text" class="amount" id="txtSubtotal" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="summary-caption">Tax:</td>
                    <td>
                        <input type="text" class="amount" id="txtTax" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="summary-caption">Total:</td>
                    <td>
                        <input type="text" class="amount" id="txtTotal" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
