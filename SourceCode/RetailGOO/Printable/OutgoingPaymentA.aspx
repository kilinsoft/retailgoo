﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Printable.Master" AutoEventWireup="true" CodeBehind="OutgoingPaymentA.aspx.cs" Inherits="RetailGOO.Printable.OutgoingPaymentA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style type="text/css">
        textarea {
            border: 0;
        }
    </style>
    <script type="text/javascript">

        var nMaxWaitAttempts = 3;
        var item = null;

        function pageLoad() {
            waitForData();
        }
        function waitForData() {
            var dto = window.dto; // receive this dto data from parent window;
            if (dto == null) {
                if (nMaxWaitAttempts > 0) {
                    --nMaxWaitAttempts;
                    setTimeout(waitForData, 1000);
                }
            }
            else {
                renderPrintableData(dto);
                doDefaultPrint();
            }
        }
        function renderPrintableData(item) {
            $("#txtName").val(item["SupplierName"])
            $("#txtRef").val(item["Reference"]);
            //$("#txtContactPerson").val(item["ContactPerson"]);
            $("#txtOpNo").val(item["OutgoingPaymentNo"]);
            $("#txtDate").val(item["Date"]);
            $("#txtDetail1").val(item["Detail1"]);
            $("#txtDetail2").val(item["Detail2"]);
            $("#txtDetail3").val(item["Detail3"]);
            $("#txtStatus").val(item["Status"]);
            $("#txtPhone").val(item["Phone"]);
            $("#txtPaymentMethod").val(item["PaymentTypeName"]);
            $("#txtTaxIdInfo").val(item["TaxIdInfo"]);
            $("#txtNote").val(item["Note"]);

            $("#txtSubtotal").val(convertToAmount(item["Subtotal"]));
            $("#txtTax").val(convertToAmount(item["Tax"]));
            $("#txtTotal").val(convertToAmount(item["Total"]));

            var subItems = item["Items"];

            for (var i = 0; i < subItems.length; ++i) {
                var subItem = subItems[i];
                var $row = $("<tr></tr>");
                $row.append("<td>" + subItem["SupplierInvoice"] + "</td>");
                $row.append("<td>" + subItem["ReceiveOrderNo"] + "</td>");
                $row.append("<td>" + subItem["Description"] + "</td>");
                $row.append("<td>" + subItem["Amount"] + "</td>");
                $row.append("<td>" + subItem["AmountDue"] + "</td>");
                $row.append("<td>" + subItem["AmountPaid"] + "</td>");
                $("#grid tbody").append($row);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="printable theme-black">
        <div class="caption section">
            Outgoing Payment
        </div>
        <div class="content-form section">
            <table id="tblForm" class="form double">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <input type="text" id="txtName" />
                                </td>
                                <td>Reference:</td>
                                <td>
                                    <input type="text" id="txtRef" />
                                </td>
                            </tr>
                            <tr>
                                <td>Details:</td>
                                <td>
                                    <input type="text" id="txtDetail1" />
                                </td>
                                <td>Payment Number:</td>
                                <td>
                                    <input type="text" id="txtOpNo" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtDetail2" />
                                </td>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtDetail3" />
                                </td>
                                <td>Status:</td>
                                <td>
                                    <input type="text" id="txtStatus" />
                                </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <input type="text" id="txtPhone" />
                                </td>
                                <td>Payment Method:</td>
                                <td>
                                    <input type="text" id="txtPaymentMethod" />
                                </td>
<%--                                <td>WHT:</td>
                                <td style="vertical-align: middle;">
                                    <input type="checkbox" id="chkWht" />
                                    <input type="text" id="txtWht" style="width: 50px;" />
                                    %
                                    <select id="selWht" style="width: 144px;">
                                        <option value="1">5. Wage</option>
                                    </select>
                                </td>--%>
                            </tr>
                            <tr>
                                <td>Tax ID:</td>
                                <td>
                                    <textarea id="txtTaxIdInfo" style="height: 60px; resize: none;"></textarea>
                                </td>
                                <td></td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
        <div class="content-grid section">
            <table id="grid" class="grid-printable">
                <thead>
                    <tr>
                        <th style="width: 120px">SupplierInvoice</th>
                        <th style="width: 120px">RO Number</th>
                        <th style="width: 260px">Description</th>
                        <th style="width: 100px">Amount</th>
                        <th style="width: 100px">Due</th>
                        <th style="width: 100px">Pay</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="summary section">
            <table class="form double">
                <tr>
                    <td>Note:</td>
                    <td>
                        <input type="text" id="txtNote" />
                    </td>
                    <td class="summary-caption">Sub total:</td>
                    <td>
                        <input type="text" class="amount" id="txtSubtotal" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="summary-caption">Tax:</td>
                    <td>
                        <input type="text" class="amount" id="txtTax" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="summary-caption">Total:</td>
                    <td>
                        <input type="text" class="amount" id="txtTotal" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>


