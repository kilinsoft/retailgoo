﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Printable.Master" AutoEventWireup="true" CodeBehind="IncomingPaymentA.aspx.cs" Inherits="RetailGOO.Printable.IncomingPaymentA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style type="text/css">

        #txtBankAccNo {
            margin-bottom: 0px;
        }

    </style>
    <script type="text/javascript">

        var nMaxWaitAttempts = 3;
        var item = null;

        function pageLoad() {
            waitForData();
        }
        function waitForData() {
            var dto = window.dto; // receive this dto data from parent window;
            if (dto == null) {
                if (nMaxWaitAttempts > 0) {
                    --nMaxWaitAttempts;
                    setTimeout(waitForData, 1000);
                }
            }
            else {
                renderPrintableData(dto);
                doDefaultPrint();
            }
        }
        function renderPrintableData(item) {
            $("#txtName").val(item["CustomerName"])
            $("#txtRef").val(item["Reference"]);
            $("#txtRcNo").val(item["IncomingPaymentNo"]);
            $("#txtDate").val(item["Date"]);
            $("#txtAddr1").val(item["Address1"]);
            $("#txtAddr2").val(item["Address2"]);
            $("#txtAddr3").val(item["Address3"]);
            $("#txtStatus").val(item["Status"]);
            $("#txtPhone").val(item["Phone"]);
            $("#txtPaymentMethod").val(item["PaymentTypeName"]);
            $("#txtBank").val(item["BankId"]);
            $("#txtBankAccNo").val(item["BankAccNo"]);
            $("#txtNote").val(item["Note"]);

            $("#txtSubtotal").val(convertToAmount(item["Subtotal"]));
            $("#txtTax").val(convertToAmount(item["Tax"]));
            $("#txtTotal").val(convertToAmount(item["Total"]));

            var subItems = item["Items"];

            for (var i = 0; i < subItems.length; ++i) {
                var subItem = subItems[i];
                var $row = $("<tr></tr>");
                $row.append("<td>" + subItem["SalesInvoiceNo"] + "</td>");
                $row.append("<td>" + subItem["Date"] + "</td>");
                $row.append("<td>" + convertToAmount(subItem["Amount"], 0) + "</td>");
                $row.append("<td>" + subItem["Due"] + "</td>");
                $row.append("<td>" + subItem["Pay"] + "</td>");
                $("#grid tbody").append($row);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="printable theme-black">
        <div class="caption section">
            Incoming Payment
        </div>
        <div class="content-form section">
            <table id="tblForm" class="form double">
                <tbody>
                    <tr>
                        <td>Name:</td>
                        <td>
                            <input type="text" id="txtName" />
                        </td>
                        <td>Reference:</td>
                        <td>
                            <input type="text" id="txtRef" />
                        </td>
                    </tr>
                    <tr>
                        <td>Detail:</td>
                        <td>
                            <input type="text" id="txtAddr1" />
                        </td>
                        <td>Receipt No:</td>
                        <td>
                            <input type="text" id="txtRcNo" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" id="txtAddr2" />
                        </td>
                        <td>Date:</td>
                        <td>
                            <input type="text" id="txtDate" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="text" id="txtAddr3" />
                        </td>
                        <td>Status:</td>
                        <td>
                            <input type="text" id="txtStatus" />
                        </td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td>
                            <input type="text" id="txtPhone" />
                        </td>
                        <td>Payment Method:</td>
                        <td>
                            <input type="text" id="txtPaymentMethod" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Bank No:</td>
                        <td>
                            <input type="text" id="txtBankAccNo" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="content-grid section">
            <table id="grid" class="grid-printable">
                <thead>
                    <tr>
                        <th style="width: 400px">Customer Invoice</th>
                        <th style="width: 100px">Date</th>
                        <th style="width: 100px">Amount</th>
                        <th style="width: 100px">Due</th>
                        <th style="width: 100px">Pay</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="summary section">
            <table class="form double">
                <tr>
                    <td>Note:</td>
                    <td>
                        <input type="text" id="txtNote" />
                    </td>
                    <td class="summary-caption">Sub total:</td>
                    <td>
                        <input type="text" class="amount" id="txtSubtotal" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="summary-caption">Tax:</td>
                    <td>
                        <input type="text" class="amount" id="txtTax" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="summary-caption">Total:</td>
                    <td>
                        <input type="text" class="amount" id="txtTotal" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
