﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Printable.Master" AutoEventWireup="true" CodeBehind="CurrentStockA.aspx.cs" Inherits="RetailGOO.Printable.CurrentStockA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style type="text/css">
    </style>
    <script type="text/javascript">

        var nMaxWaitAttempts = 3;
        var item = null;

        function pageLoad() {
            waitForData();
        }
        function waitForData() {
            var dto = window.dto; // receive this dto data from parent window;
            if (dto == null) {
                if (nMaxWaitAttempts > 0) {
                    --nMaxWaitAttempts;
                    setTimeout(waitForData, 1000);
                }
            }
            else {
                renderPrintableData(dto);
                doDefaultPrint();
            }
        }
        function renderPrintableData(item) {
            $("#txtIgn").val(item["ItemGroupName"])
            $("#txtIcn").val(item["ItemCategoryName"])
            $("#txtIbn").val(item["ItemBrandName"])

            var subItems = item["Items"];

            for (var i = 0; i < subItems.length; ++i) {
                var subItem = subItems[i];
                var $row = $("<tr></tr>");
                $row.append("<td>" + subItem["ItemGroupName"] + "</td>");
                $row.append("<td>" + subItem["ItemCategoryName"] + "</td>");
                $row.append("<td>" + subItem["ItemBrandName"] + "</td>");
                $row.append("<td>" + subItem["Quantity"] + "</td>");
                $("#grid tbody").append($row);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="printable theme-black">
        <div class="caption section">
            Current Stock
        </div>
        <div class="content-form section">
            <table class="form single">
                <tr>
                    <td>Item Name:</td>
                    <td>
                        <input type="text" id="txtIgn" />
                    </td>
                </tr>
                <tr>
                    <td>Category Name:</td>
                    <td>
                        <input type="text" id="txtIcn" />
                    </td>
                </tr>
                <tr>
                    <td>Brand Name:</td>
                    <td>
                        <input type="text" id="txtIbn" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="content-grid section">
            <table id="grid" class="grid-printable">
                <thead>
                    <tr>
                        <th style="width: 250px">Item Code</th>
                        <th style="width: 200px">Item Category</th>
                        <th style="width: 200px">Item Brand</th>
                        <th style="width: 200px">Quantity</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
