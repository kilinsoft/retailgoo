﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Printable.Master" AutoEventWireup="true" CodeBehind="ExpenseA.aspx.cs" Inherits="RetailGOO.Printable.ExpenseA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style type="text/css">

    </style>
    <script type="text/javascript">

        var nMaxWaitAttempts = 3;
        var item = null;

        function pageLoad() {
            waitForData();
        }
        function waitForData() {
            var dto = window.dto; // receive this dto data from parent window;
            if (dto == null) {
                if (nMaxWaitAttempts > 0) {
                    --nMaxWaitAttempts;
                    setTimeout(waitForData, 1000);
                }
            }
            else {
                renderPrintableData(dto);
                doDefaultPrint();
            }
        }
        function renderPrintableData(item) {

            $("#txtName").val(item["SupplierName"])
            $("#txtCategory").val(item["ExpenseCategoryName"]);
            $("#txtNote").val(item["Note"]);
            $("#txtDate").val(item["Date"]);
            $("#txtAmount").val(item["Amount"]);
            $("#chkWht").prop('checked', item["UseWht"]);
            $("#lblWhtPercent").text(item["WhtPercent"]);
            $("#txtWhtAmount").val(item["WhtAmount"]);

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="printable theme-black">
        <div class="caption section">
            Expense
        </div>
        <div class="content-form section">
            <table id="tblForm" class="form single">
                        <tbody>
                            <tr>
                                <td>Amount:</td>
                                <td>
                                    <input type="text" id="txtAmount" />
                                </td>
                            </tr>
                            <tr>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Vendor:</td>
                                <td>
                                    <input type="text" id="txtName" />
                                </td>
                            </tr>
                            <tr>
                                <td>Category:</td>
                                <td>
                                    <input type="text" id="txtCategory" />
                                </td>
                            </tr>
                            <tr>
                                <td>Notes:</td>
                                <td>
                                    <input type="text" id="txtNote" />
                                </td>
                            </tr>
                            <tr>
                                <td>Withholding Tax:</td>
                                <td style="vertical-align: middle;">
                                    <input type="checkbox" id="chkWht" />
                                    <span id="lblWhtPercent"></span>%
                                    <input type="text" id="txtWhtAmount" style="width: 170px;" />
                                </td>
                            </tr>
<%--                            <tr>
                                <td></td>
                                <td style="text-align: right;">
                                     <input type="button" id="btnAddExpense" class="btn-action-big btn-green" value="Add Expense" onclick="addExpenseToGrid();" />
                                </td>
                            </tr>--%>
                        </tbody>
                    </table>
        </div>
      
    </div>
</asp:Content>
