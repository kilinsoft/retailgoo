﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Master
{
    public partial class Auth : System.Web.UI.MasterPage
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
        }

        protected override void OnInit(EventArgs e)
        {
            if (AuthenticationHelper.IsAuthenticated())
            {
                // Redirect to business screen if the user has not selected a business to work with
                if (HttpContext.Current.Session[SessionVariable.BusinessId] == null ||
                    HttpContext.Current.Session[SessionVariable.BusinessId].ToString() == "")
                {
                    // Do not redirect if the screen is in this list
                    string[] ignoreList = new string[] { 
                        RetailGooPageUrl.BusinessSelection,
                        RetailGooPageUrl.CreateBusiness
                    };
                    bool fRedirect = true;
                    for (int i = 0; i < ignoreList.Length; ++i)
                    {
                        string pageUrl = ignoreList[i].Substring(1);
                        if (Request.Url.AbsolutePath.Equals(pageUrl, StringComparison.OrdinalIgnoreCase))
                        {
                            fRedirect = false;
                            break;
                        }
                    }
                    if (fRedirect)        
                        Response.Redirect(RetailGooPageUrl.BusinessSelection);
                }

                // Pro : ToDo : Check if BusinessId is valid
                // Check if the BusinessID is valid. If it is not valid, this might be a security risk because
                // the user is trying to forge a businessId. Redirect the user to the Login screen
                bool fValid = false;
                fValid = true;
                if (!fValid)
                {
                    Identity.SignOut(SessionHelper.GetCurrentUsername());
                    Response.Redirect(RetailGooPageUrl.LogInPage +
                        "?Reason=" +
                        Encryption.SimpleEncrypt("You were redirected to the login page because the incorrect BusinessId was used"));
                }

                if (HttpContext.Current.Session[SessionVariable.BusinessName] != null)
                    lblBusiName.Text = HttpContext.Current.Session[SessionVariable.BusinessName].ToString();

                if (HttpContext.Current.Session[SessionVariable.User] != null && 
                    HttpContext.Current.Session[SessionVariable.UserProfile] != null &&
                    HttpContext.Current.Session[SessionVariable.Staff] != null)
                {
                    UserProfileDto userProfile = (UserProfileDto)HttpContext.Current.Session[SessionVariable.UserProfile];
                    User user = (User)HttpContext.Current.Session[SessionVariable.User];
                    Staff staff = (Staff)HttpContext.Current.Session[SessionVariable.Staff];
                    Username = userProfile.Firstname + " " + userProfile.Lastname;
                    Email = user.Email;
                    RoleName = staff.Role.ToString(); 
                }

            }
            else
            {
                Response.Redirect(RetailGooPageUrl.LogInPage +
                    "?ReturnPage=" +
                    Encryption.SimpleEncrypt(Server.UrlEncode(Request.Url.ToString())));
            }
        }
    }
}