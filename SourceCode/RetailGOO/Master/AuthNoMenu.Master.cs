﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.Master
{
    public partial class AuthNoMenu : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
        }

        protected override void OnInit(EventArgs e) 
        {
            if (AuthenticationHelper.IsAuthenticated())
            {
                // Redirect to business screen if the user has not selected a business to work with
                if (HttpContext.Current.Session[SessionVariable.BusinessId] == null ||
                    HttpContext.Current.Session[SessionVariable.BusinessId].ToString() == "")
                {
                    // Do not redirect if this is actually the business selection screen
                    if (!Request.Url.AbsolutePath.Equals(RetailGooPageUrl.BusinessSelection.Substring(1), StringComparison.OrdinalIgnoreCase))
                        Response.Redirect(RetailGooPageUrl.BusinessSelection);
                }

                // Pro : ToDo : Check if BusinessId is valid
                // Check if the BusinessID is valid. If it is not valid, this might be a security risk because
                // the user is trying to forge a businessId. Redirect the user to the Login screen
                bool fValid = false;
                fValid = true;
                if (!fValid)
                {
                    Identity.SignOut(SessionHelper.GetCurrentUsername());
                    Response.Redirect(RetailGooPageUrl.LogInPage +
                        "?Reason=" +
                        Encryption.SimpleEncrypt("You were redirected to the login page because the incorrect BusinessId was used"));
                }

            }
            else
            {
                Response.Redirect(RetailGooPageUrl.LogInPage +
                    "?ReturnPage=" +
                    Encryption.SimpleEncrypt(Server.UrlEncode(Request.Url.ToString())));
            }
        }
    }
}