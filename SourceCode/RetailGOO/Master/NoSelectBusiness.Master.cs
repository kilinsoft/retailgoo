﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Util.Constant;

namespace RetailGOO.Master
{
    public partial class NoSelectBusiness : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
        }

        protected override void OnInit(EventArgs e) 
        {
            if (AuthenticationHelper.IsAuthenticated())
            {

            }
            else
            {
                Response.Redirect(RetailGooPageUrl.LogInPage +
                    "?ReturnPage=" +
                    Encryption.SimpleEncrypt(Server.UrlEncode(Request.Url.ToString())));
            }
        }
    }
}