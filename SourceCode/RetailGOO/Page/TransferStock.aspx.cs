﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class TransferStock : System.Web.UI.Page
    {
        public string TransferStatusListString { get; set; }
        public string TransferAllStatusListString { get; set; }
        public string SourceBusinessNameString { get; set; }
        public string TargetBusinessListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspTransferStatus = ItemAdapter.RetrieveItemTransferStatusList();
            if (rspTransferStatus.Success && rspTransferStatus.ResponseItem != null && rspTransferStatus.ResponseItem.Count > 0)
                TransferStatusListString = HtmlSelectHelper.ConvertToListString(rspTransferStatus.ResponseItem);
            else
                TransferStatusListString = "[]";

            var rspTransferAllStatus = ItemAdapter.RetrieveItemTransferAllStatusList();
            if (rspTransferAllStatus.Success && rspTransferAllStatus.ResponseItem != null && rspTransferAllStatus.ResponseItem.Count > 0)
                TransferAllStatusListString = HtmlSelectHelper.ConvertToListString(rspTransferAllStatus.ResponseItem);
            else
                TransferAllStatusListString = "[]";

            var rspSourceBusiness = Business.RetrieveSourceBusinessList();
            if (rspSourceBusiness.Success && rspSourceBusiness.ResponseItem != null && rspSourceBusiness.ResponseItem.Count > 0)
                SourceBusinessNameString = string.Format("['{0}']", rspSourceBusiness.ResponseItem[0].Text);
            else
                SourceBusinessNameString = "[]";

            var rspTargetBusiness = Business.RetrieveTargetBusinessList();
            if (rspTargetBusiness.Success && rspTargetBusiness.ResponseItem != null && rspTargetBusiness.ResponseItem.Count > 0)
                TargetBusinessListString = HtmlSelectHelper.ConvertToListString(rspTargetBusiness.ResponseItem);
            else
                TargetBusinessListString = "[]";
        }
    }
}