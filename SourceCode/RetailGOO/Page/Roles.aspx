﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="Roles.aspx.cs" 
    Inherits="RetailGOO.Page.Roles" ClientIDMode="Static"%>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .permit-column {
            width: 120px;
        }
        .td-caption {
            color: #6d4c77;
            width: 150px !important;
            padding-left: 10px;
        }
    </style>
    <script type="text/javascript">
        // Variables declaration
        var g_roleList = <%=RoleListString%>;

        var g_selRole;

        function pageLoad() {

            preventNavigationOnUnsaved(true);
            
            g_selRole = $("#selRole").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                readOnly: true,
                options: g_roleList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        retrieveMainData(val);
                    }
                },
            })[0].selectize;

            // Initialize behaviors
            $("#divSales").hide();
            $("#divInventory").hide();
            $("#divBusinessPartner").hide();

            $("#btnVoid").addClass('disable').disable();
            $("#btnPrint").addClass('disable').disable();
            $("#btnClear").addClass('disable').disable();

            // Ajax calls to initialize mandatory variables
            //retrieveMainData($("#selRole option:selected").text());
        }
        function retrieveMainData(RolesName, funcOnSuccess) {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveRoles", RolesName,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function displayMainItemData(item) {
            setFormData(item);
        }
        function setFormData(item) {
            var subItems = item["Items"];
            for (var i = 0; i < subItems.length; i++)
            {
                var ctl = "input[type=radio][name='" + subItems[i]["PageName"] + "'][value='" +
                    subItems[i]["RoleTypeValue"] + "']";
                $(ctl).prop('checked',true)
            }
        }
        function constructDtoItem() {
            var item = {};
            var row = g_activeMainItem;

            //item["DocSettingId"] = row == null ? -1 : row.DocSettingId;
            item["RoleCategory"] = $("#selRole option:selected").text();
            item["Items"] = [];

            var cpItem = {};
            cpItem["PageName"] = "cp";
            cpItem["RoleTypeValue"] = $("input[name=cp]:checked").val();
            item["Items"].push(cpItem);

            var poItem = {};
            poItem["PageName"] = "po";
            poItem["RoleTypeValue"] = $("input[name=po]:checked").val();
            item["Items"].push(poItem);

            var roItem = {};
            roItem["PageName"] = "ro";
            roItem["RoleTypeValue"] = $("input[name=ro]:checked").val();
            item["Items"].push(roItem);

            var opItem = {};
            opItem["PageName"] = "op";
            opItem["RoleTypeValue"] = $("input[name=op]:checked").val();
            item["Items"].push(opItem);

            var csItem = {};
            csItem["PageName"] = "cs";
            csItem["RoleTypeValue"] = $("input[name=cs]:checked").val();
            item["Items"].push(csItem);

            var sivItem = {};
            sivItem["PageName"] = "siv";
            sivItem["RoleTypeValue"] = $("input[name=siv]:checked").val();
            item["Items"].push(sivItem);

            var ipItem = {};
            ipItem["PageName"] = "ip";
            ipItem["RoleTypeValue"] = $("input[name=ip]:checked").val();
            item["Items"].push(ipItem);

            var asItem = {};
            asItem["PageName"] = "as";
            asItem["RoleTypeValue"] = $("input[name=as]:checked").val();
            item["Items"].push(asItem);

            var tsItem = {};
            tsItem["PageName"] = "ts";
            tsItem["RoleTypeValue"] = $("input[name=ts]:checked").val();
            item["Items"].push(tsItem);

            var rtsItem = {};
            rtsItem["PageName"] = "rts";
            rtsItem["RoleTypeValue"] = $("input[name=rts]:checked").val();
            item["Items"].push(rtsItem);

            var rsItem = {};
            rsItem["PageName"] = "rs";
            rsItem["RoleTypeValue"] = $("input[name=rs]:checked").val();
            item["Items"].push(rsItem);

            var ciItem = {};
            ciItem["PageName"] = "ci";
            ciItem["RoleTypeValue"] = $("input[name=ci]:checked").val();
            item["Items"].push(ciItem);

            var siItem = {};
            siItem["PageName"] = "si";
            siItem["RoleTypeValue"] = $("input[name=si]:checked").val();
            item["Items"].push(siItem);

            return item;
        }
        function onPageGroupChanged() {
            var selectedVal = parseInt($("#selPage").val(), 10);
            var groups = [$("#divPurchasing"), $("#divSales"), $("#divInventory"), $("#divBusinessPartner")];
            for (var i = 0; i < groups.length; ++i) {
                if (i == selectedVal)
                    groups[i].show();
                else
                    groups[i].hide();
            }
        }
        function doSave() {
            var item = constructDtoItem();

            sendAjaxUpdate("BusinessService.svc", "ManageRoles", item,
                function (rsp) {
                },
                function (rsp) {
                   displayError(rsp.StatusText);
                });
        }
        function doVoid() {
        }
        function doClear() {
        }
        function doPrint() {
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('HardwareSetting.aspx');
        }
        function goNext() {
            changeUrl('User.aspx');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-purple">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('BusinessInfo.aspx');">Business Info</a></li>
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('FinancialSetting.aspx');">Financial Setting</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs no-border-bottom">
            <%--<li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('HardwareSetting.aspx');">Hardware Setting</a></li>--%>
            <li class="purple active width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('Roles.aspx');">Roles</a></li>
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('User.aspx')">User</a></li>
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('DocSetting.aspx')">Doc Setting</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table class="form single">
                        <tr>
                            <td colspan="2" class="cell-header">
                                Roles Management:
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption">Roles:</td>
                            <td>
                                <select id="selRole" class="width-200 not-cause-dirty" placeHolder="Role">
<%--                                    <option value="0">Manager</option>
                                    <option value="1">Purchase</option>
                                    <option value="2">Stocking</option>
                                    <option value="3">Sales</option>--%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption">Page:</td>
                            <td>
                                <select id="selPage" class="width-200 not-cause-dirty" onchange="onPageGroupChanged();">
                                    <option value="0">Purchasing</option>
                                    <option value="1">Sales</option>
                                    <option value="2">Inventory</option>
                                    <option value="3">Business Partner</option>
                                </select>
                            </td>
                        </tr>
			        </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div id="divPurchasing" class="section">
                    <table>
                        <tr>
                            <td colspan="6" class="cell-header">
                                Purchasing:
                            </td>
                        </tr>
                        <tr><!--cash purchase-->
                            <td class="td-caption" style="width: 150px;">Cash Purchase:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllCp" name="cp" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateCp" name="cp" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditCp" name="cp" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteCp" name="cp" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewCp" name="cp" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr><!--purchase order-->
                            <td class="td-caption" style="width: 150px;">Purchase Order:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllPo" name="po" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreatePo" name="po" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditPo" name="po" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeletePo" name="po" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewPo" name="po" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr><!--receive order-->
                            <td class="td-caption" style="width: 150px;">Receive Order:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllRo" name="ro" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateRo" name="ro" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditRo" name="ro" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteRo" name="ro" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewRo" name="ro" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr><!--outgoing payment-->
                            <td class="td-caption" style="width: 150px;">Outgoing Payment:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllOp" name="op" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateOp" name="op" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditOp" name="op" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteOp" name="op" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewOp" name="op" value="v">
                                <span>View</span>
                            </td>
                        </tr>
			        </table>
                </div>
                <div id="divSales" class="section">
                    <table>
                        <tr>
                            <td colspan="6" class="cell-header">
                                Sales:
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Cash Sales:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllCs" name="cs" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateCs" name="cs" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditCs" name="cs" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteCs" name="cs" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewCs" name="cs" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Sales Invoice:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllSi" name="siv" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateSi" name="siv" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditSi" name="siv" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteSi" name="siv" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewSi" name="siv" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Incoming Payment:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllIp" name="ip" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateIp" name="ip" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditIp" name="ip" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteIp" name="ip" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewIp" name="ip" value="v">
                                <span>View</span>
                            </td>
                        </tr>
		            </table>
                </div>
                <div id="divInventory" class="section">
                    <table>
                        <tr>
                            <td colspan="6" class="cell-header">
                                Inventory:
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Adjust Stock:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllAs" name="as" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateAs" name="as" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditAs" name="as" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteAs" name="as" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewAs" name="as" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Transfer Stock:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllTs" name="ts" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateTs" name="ts" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditTs" name="ts" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteTs" name="ts" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewTs" name="ts" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Receive Transfer Stock:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radAllRts" name="rts" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radCreateRts" name="rts" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radEditRts" name="rts" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="radDeleteRts" name="rts" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="radViewRts" name="rts" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Reorder Stock:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio1" name="rs" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio2" name="rs" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio3" name="rs" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio4" name="rs" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="Radio5" name="rs" value="v">
                                <span>View</span>
                            </td>
                        </tr>
		            </table>
                </div><!--div inventory-->
                <div id="divBusinessPartner" class="section">
                    <table>
                        <tr>
                            <td colspan="6" class="cell-header">
                                Business Partner:
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Customer Info:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio6" name="ci" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio7" name="ci" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio8" name="ci" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio9" name="ci" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="Radio10" name="ci" value="v">
                                <span>View</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-caption" style="width: 150px;">Supplier Info:</td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio11" name="si" value="a">
                                <span>All</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio12" name="si" value="c">
                                <span>Create</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio13" name="si" value="e">
                                <span>Edit</span>
                            </td>
                            <td class="cell-control left permit-column">
                                <input type="radio" id="Radio14" name="si" value="d">
                                <span>Delete</span>
                            </td>
                            <td>
                                <input type="radio" id="Radio15" name="si" value="v">
                                <span>View</span>
                            </td>
                        </tr>
		            </table>
                </div><!--div business partner-->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div class="btn-nav" id="btnBackward" onclick="goBack();">&lt;</div>
                        <div class="btn-save" id="btnSave" onclick="doSave();">Save</div>
                        <div class="btn-void" id="btnVoid" onclick="doVoid();">Void</div>
                        <div class="btn-clear" id="btnClear" onclick="doClear();">Clear</div>
                        <div class="btn-print" id="btnPrint" onclick="doPrint();">Print</div>
                        <div class="btn-cancel" id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div class="btn-nav" id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
            </div>
        </div><!--tab-content-->
    </div><!--subpage-->
</asp:Content>
