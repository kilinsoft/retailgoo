﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="CurrentStock.aspx.cs" Inherits="RetailGOO.Page.CurrentStock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>

    </style>
    <script type="text/javascript">
        // Variables declaration

        // Control variables
        var g_selItemGroup, g_selItemCategory, g_selItemBrand;
        var g_docTemplate = <%=DocumentTemplate%>;

        // Data variables
        var g_itemgroupList = <%=ItemGroupListString%>;
        var g_itemcategoryList = <%=ItemCategoryListString%>;
        var g_itembrandList = <%=ItemBrandListString%>;

        function pageLoad() {

            preventNavigationOnUnsaved(true);

            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Item Code", name: "ItemGroupCode", required: false, editable: false },
            { title: "Item Category", name: "ItemCategoryName", required: false, editable: false },
            { title: "Item Brand", name: "ItemBrandName", required: false, editable: false },
            { title: "Item Qty", name: "Quantity", width: 150, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false
                //onRowSelected: mgOnRowSelected,
                //onRowDeselected: mgOnRowDeselected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "Item Code", name: "ItemGroupId", valueCompare: false },
                { title: "Item Category", name: "ItemCategoryName", valueCompare: false },
                { title: "Item Brand", name: "ItemBrandName", valueCompare: false },
                { title: "Item Qty", name: "Quantity", valueCompare: true }]);

            g_selItemGroup = $("#selItemGroup").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_itemgroupList,
                onChange: function (val) {
                    if (val != -1 && val != "") {
                        g_selItemCategory.disable();
                        g_selItemBrand.disable();
                    } else 
                    {
                        g_selItemCategory.enable();
                        g_selItemBrand.enable();
                    }
                },
            })[0].selectize;
            //g_selItemGroup.addOption({ Value: "", Text: "--select--" });

            g_selItemCategory = $("#selItemCategory").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_itemcategoryList,
                onChange: function (val) {
                    if (val != -1 && val != "") {
                        g_selItemGroup.disable();
                        g_selItemBrand.disable();
                    } else 
                    {
                        g_selItemGroup.enable();
                        g_selItemBrand.enable();
                    }
                },
            })[0].selectize;
            //g_selItemCategory.addOption({ Value: "", Text: "--select--" });
            //g_selItemCategory.removeOption(-1);

            g_selItemBrand = $("#selItemBrand").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_itembrandList,
                onChange: function (val) {
                    if (val != -1 && val != "") {
                        g_selItemCategory.disable();
                        g_selItemGroup.disable();
                    } else 
                    {
                        g_selItemCategory.enable();
                        g_selItemGroup.enable();
                    }
                },
            })[0].selectize;
            //g_selItemBrand.addOption({ Value: "", Text: "--select--" });
            //g_selItemBrand.removeOption(-1);

            // Initialize behaviors
            g_selItemGroup.setValue("");
            g_selItemCategory.setValue("");
            g_selItemBrand.setValue("");

            $("#btnSave").addClass('disable').disable();
            $("#btnVoid").addClass('disable').disable();
            //$("#btnClear").addClass('disable').disable();
            $("#btnCancel").addClass('disable').disable();

            // Ajax calls to initialize mandatory variables
        }

        function retrieveMainGridData(item, funcOnSuccess) {
            sendAjaxUpdate("ItemManagement.svc", "RetrieveCurrentStock", item,
                function (rsp) {
                    var items = rsp.ResponseItem;
                    for (var i = 0; i < items.length; ++i) {
                        var item = items[i];
                        if ( item["ItemBrandName"] == "" )
                            item["ItemBrandName"] = "-"
                        if ( item["ItemCategoryName"] == "" )
                            item["ItemCategoryName"] = "-"
                    }
                    setGridData(g_mainGrid, rsp.ResponseItem);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        //function retrieveMainItemData(SalesInvoiceId) {
        //    retrieveAjaxDataSync("Sales.svc", "RetrieveSalesInvoice", SalesInvoiceId,
        //        function (rsp) {
        //            g_activeMainItem = rsp.ResponseItem;
        //            displayMainItemData(g_activeMainItem);
        //            changeState(ePageState.mainView);
        //            updateCommandButtons("main", g_mainGrid.selectedRow);
        //        });
        //}

        // Handlers
        //function mgOnRowSelected(pRow) {
            //pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            //if (!isPageDirty()) {
            //    if (pRow.rowState == eItemState.originalItem) {
            //        retrieveMainItemData(g_mainGrid.selectedRow.data["SalesInvoiceId"]);
            //    }
            //} else {
            //    alertClearFirst();
            //    if (g_mainGrid.selectedRow != null)
            //        g_mainGrid.selectedRow.deselect();
            //}
        //}
        //function mgOnRowDeselected(pRow) {
            //pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        //}
        function constructSearchingItem() {
            var item = {};

            item["ItemGroupId"] = g_selItemGroup.getValue() == ""? -1 : g_selItemGroup.getValue();
            item["ItemCategoryId"] = g_selItemCategory.getValue() == ""? -1 : g_selItemCategory.getValue();
            item["ItemBrandId"] = g_selItemBrand.getValue() == ""? -1 : g_selItemBrand.getValue();

            return item;
        }
        function btnSearchOnClick() {
            var item = constructSearchingItem();

            retrieveMainGridData(item);
        }
        function constructDtoItem() {
            var item = {};

            item["ItemGroupName"] = g_selItemGroup.getOption(g_selItemGroup.getValue())[0].innerText;
            item["ItemCategoryName"] = g_selItemCategory.getOption(g_selItemCategory.getValue())[0].innerText;
            item["ItemBrandName"] = g_selItemBrand.getOption(g_selItemBrand.getValue())[0].innerText;
            item["Items"] = [];

            for (var i = 0; i < g_mainGrid.rows.length; ++i) {
                var row = g_mainGrid.rows[i];
                var subItem = {};
                subItem["ItemGroupName"] = row.data["ItemGroupCode"];
                subItem["ItemCategoryName"] = row.data["ItemCategoryName"];
                subItem["ItemBrandName"] = row.data["ItemBrandName"];
                subItem["Quantity"] = row.data["Quantity"];
                item["Items"].push(subItem);
            }

            return item;
        }
        function doSave() {
        }
        function doVoid() {
        }
        function doClear() {
            g_selItemGroup.enable();
            g_selItemGroup.setValue("");
            g_selItemCategory.enable();
            g_selItemCategory.setValue("");
            g_selItemBrand.enable();
            g_selItemBrand.setValue("");
            setGridData(g_mainGrid, []);
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/CurrentStockA.aspx";
            var docTemplateBUrl = "../Printable/CurrentStockB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Current Stock", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
        }
        function goBack() {
        }
        function goNext() {
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-purple">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('AdjustStock.aspx');">Adjust Stock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('TransferStock.aspx');">Transfer Stock</a></li>
            <li class="inactive" style="width: 304px;"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveTransferStock.aspx')">Receive TransferStock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReorderStock.aspx');">Reorder Stock</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ItemManagement.aspx');">ItemManagement</a></li>
            <li class="inactive" style="width: 184px;"><a href="#" data-toggle="tab" onclick="changeUrl('CategoryManagement.aspx');">Category Management</a></li>
            <li class="inactive" style="width: 180px;"><a href="#" data-toggle="tab" onclick="changeUrl('BrandManagement.aspx')">Brand Management</a></li>
            <%--<li class="inactive" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CountSheet.aspx');">Count Sheet</a></li>--%>
            <li class="active" style="width: 280px;"><a href="#" data-toggle="tab" onclick="changeUrl('CurrentStock.aspx');">Current Stock</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table class="form single">
                        <tr>
                            <td>Item Name:</td>
                            <td>
                                <select id="selItemGroup" placeHolder="select">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Category Name:</td>
                            <td>
                                <select id="selItemCategory" placeHolder="select">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Brand Name:</td>
                            <td>
                                <select id="selItemBrand" placeHolder="select">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="custom" style="padding-left: 300px;">
                                <input type="button" id="btnSearch" value="Search" class="btn-action-big btn-purple" onclick="btnSearchOnClick();" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="grid-command search">
                        <div>
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>

            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
</asp:Content>
