﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NoSelectBusiness.Master" AutoEventWireup="true" CodeBehind="BusinessSelection.aspx.cs" Inherits="RetailGOO.Page.BusinessSelection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <script type="text/javascript">

        var g_dataArray = new Array();
        var g_selectedBusinessId = -1;

        function pageLoad() {
            //retrieveBusiness();
        }

        function retrieveBusiness() {
            sendAjax(
                "../Service/BusinessService.svc/RetrieveAssignedBusiness",
                {},
                function (resp) {
                    if (resp.Success) {
                        g_dataArray = resp.ResponseItem;
                        displayData(g_dataArray);
                    }
                    else {
                        alert("Could not retrieve the list of businesses.");
                    }
                },
                function (type, err, errThrown) {
                    alert('Error\n type: ' + type + '\nerr: ' + err + '\nerrThrown: ' + errThrown);
                }
                );
        }

        function displayData(dataArray) {
            $('#selBusiness').html("");
            for (var i = 0; i < dataArray.length; ++i) {
                $('#selBusiness')
                    .append($("<option></option>")
                    .attr("value", dataArray[i]["BusinessId"])
                    .html(dataArray[i]["BusinessName"] + " (" + dataArray[i]["Role"] + ")"));
            }
        }

        function selectBusiness() {
            //g_selectedBusinessId = $("#selBusiness").val();
        }

        function btnSelectOnClick()
        {
            var selectedId = $("#selBusiness").val();
            if ( selectedId == "")
                alert("Please select a business");
            else {
                GoToUrl("./Dashboard.aspx?BusinessId=" + $("#selBusiness").val())
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="subpage">
        <div class="section section-last" style="padding: 50px 0px;">
            <table class="form blue">
                <tr>
                    <td rowspan="6" class="cell-name right" style="padding-right: 50px;">
                        <img src="../images/logo00.png" />
                    </td>
                    <td class="cell-header">Select the business to work with:</td>
                </tr>
                <tr>
                    <td class="cell-control left">
                        <%--
                        <select id="selBusiness" runat="server" class="width-300">
                            <option value="1">-- Select Business --</option>
                        </select>         
                        --%>
                        <asp:DropDownList ID="selBusiness" runat="server" CssClass="width-300"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="cell-control left">
                        <asp:Button runat="server" ID="BtnSelect" cssClass="btn-action-big btn-blue" Text="Select" OnClick="BtnSelectOnClick" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-control left" style="height: 50px; vertical-align: bottom;">
                        <a href=CreateBusiness.aspx>Create a new business</a>
                    </td>
                </tr>
                <tr>
                    <td class="cell-control left">
                        <a href="../logout.aspx">Log out</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
