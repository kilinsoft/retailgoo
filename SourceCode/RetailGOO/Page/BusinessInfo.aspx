﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="BusinessInfo.aspx.cs"
    Inherits="RetailGOO.Page.BusinessInfo" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <script type="text/javascript">
        // Variables declaration

        // Control variables
        var g_selBusinessType;
        var g_selCountry;

        // Data variables
        var g_businesstypeList = <%=BusinessTypeListString%>;
        var g_countryList = <%=CountryListString%>;

        function pageLoad() {

            preventNavigationOnUnsaved(true);

            g_selBusinessType = $("#selType").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                readOnly: true,
                options: g_businesstypeList,
                onChange: function (val) {
                    //if (val != "" && val != -1) {
                    //    if (g_pageState == ePageState.mainNew)
                    //        retrieveCustomerInfo(val);
                    //}
                },
            })[0].selectize;

            g_selCountry = $("#selCountry").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                placeholder: 'Country',
                options: g_countryList,
                onChange: function (val) {
                    //if (val != "" && val != -1) {
                    //    if (g_pageState == ePageState.mainNew)
                    //        retrieveCustomerInfo(val);
                    //}
                },
            })[0].selectize;
            g_selCountry.removeOption(-1);

            // Initialize behaviors
            $("#btnVoid").addClass('disable').disable();
            $("#btnPrint").addClass('disable').disable();
            $("#btnClear").addClass('disable').disable();

            // Ajax calls to initialize mandatory variables
            retrieveMainData();
        }
        function retrieveMainData(funcOnSuccess) {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveBusinessInfo", {},
                function (items) {
                    g_activeMainItem = items;
                    displayMainItemData(g_activeMainItem);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function displayMainItemData(item) {
            setFormData(item);
        }
        function setFormData(item) {
            g_selBusinessType.setValue(item.BusinessTypeId);
            $("#txtName").val(item.BusinessName);
            $("#txtAddr1").val(item.Address1);
            $("#txtAddr2").val(item.Address2);
            $("#txtAddr3").val(item.Address3);
            $("#txtCity").val(item.City);
            g_selCountry.setValue(item["CountryId"]);
            $("#txtZip").val(item.ZipCode);
            $("#txtPhone").val(item.Phone);
            $("#txtFax").val(item.Fax);
            $("#txtEmail").val(item.Email);
            $("#txtWebsite").val(item.Website);
            $("#txtFbUrl").val(item.FacebookUrl);
            $("#txtTaxId").val(item.TaxIdInfo);
        }
        function constructDtoItem() {
            var item = {};
            var row = g_activeMainItem;

            item["BusinessName"] = $("#txtName").val();
            item["BusinessTypeId"] = g_selBusinessType.getValue();
            item["ImagePath"] = row == null ? "" : row.RootBusinessId;
            item["Address1"] = $("#txtAddr1").val();
            item["Address2"] = $("#txtAddr2").val();
            item["Address3"] = $("#txtAddr3").val();
            item["City"] = $("#txtCity").val();
            item["CountryId"] = g_selCountry.getValue() != "" ? g_selCountry.getValue() : -1;
            item["ZipCode"] = $("#txtZip").val();
            item["Phone"] = $("#txtPhone").val();
            item["Fax"] = $("#txtFax").val();
            item["Email"] = $("#txtEmail").val();
            item["Website"] = $("#txtWebsite").val();
            item["FacebookUrl"] = $("#txtFbUrl").val();
            item["TaxIdInfo"] = $("#txtTaxId").val();
            item["DateExpired"] = row == null ? "" : row.DateExpired;

            return item;
        }
        function validateForm() {
            return true;
        }
        function doSave() {
            if (!validateForm())
                return;
            var item = constructDtoItem();

            sendAjaxUpdate("BusinessService.svc", "ManageBusinessInfo", item,
                function (rsp) {
                    setPageDirty(false);
                    //if (g_pageState == ePageState.mainEdit) {
                    //    changeState(ePageState.mainView);
                    //    var selectedRow = g_mainGrid.selectedRow;
                    //    g_mainGrid.clearSelection();
                    //    if (selectedRow != null)
                    //       selectedRow.select(null, false);
                    //}
                    //else {
                    //changeState(ePageState.mainNew);
                    //retrieveMainGridData();
                    //}
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
        }
        function doClear() {
            var fClear = window.confirm("Your unsaved data will be lost.\nDo you want to continue?");
            if (fClear) {
                setPageDirty(false);
                retrieveMainData();
            }
        }
        function doPrint() {
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('User.aspx');
        }
        function goNext() {
            changeUrl('FinancialSetting.aspx');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-blue">
        <ul id="tab0" class="nav nav-tabs first-row">
            <%--<li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('HardwareSetting.aspx');">Hardware Setting</a></li>--%>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('Roles.aspx');">Roles</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('User.aspx')">User</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="active blue"><a href="#" data-toggle="tab" onclick="changeUrl('BusinessInfo.aspx');">Business Info</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('FinancialSetting.aspx');">Financial Setting</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('DocSetting.aspx')">Doc Setting</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table class="form single">
                        <tr>
                            <td>Logo:</td>
                            <td>
                                <input type="button" id="btnChooseFile" class="btn-action width-150" value="Choose File" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div class="image-holder">
                                    <img src="../images/logo00.png" id="imgLogo" style="" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Business Type:</td>
                            <td>
                                <select id="selType" class="width-200">
                                </select>
                            </td>
                        </tr>
<%--                        <tr>
                            <td>Synchronize Master Data:</td>
                            <td>
                                <input type="text" id="Text1" class="width-250" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td>Business Name:</td>
                            <td>
                                <input type="text" id="txtName" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td>
                                <input type="text" id="txtAddr1" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtAddr2" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtAddr3" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>
                                <input type="text" id="txtCity" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>
                                <select id="selCountry" >
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Zip Code:</td>
                            <td>
                                <input type="text" id="txtZip" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td>
                                <input type="text" id="txtPhone" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Fax:</td>
                            <td>
                                <input type="text" id="txtFax" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>
                                <input type="text" id="txtEmail" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Website:</td>
                            <td>
                                <input type="text" id="txtWebsite" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Facebook Page:</td>
                            <td>
                                <input type="text" id="txtFbUrl" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Tax ID:</td>
                            <td>
                                <input type="text" id="txtTaxId" class="width-250" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!--
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                -->
                <%--                <div id="divCommonControls" style="text-align: center; margin-top: 10px;">
                    <div style="display: inline-block;">--%>
                <div class="section common-control">
                    <div>
                        <div class="btn-nav" id="btnBackward" onclick="goBack();">&lt;</div>
                        <div class="btn-save" id="btnSave" onclick="doSave();">Save</div>
                        <div class="btn-void" id="btnVoid" onclick="doVoid();">Void</div>
                        <div class="btn-clear" id="btnClear" onclick="doClear();">Clear</div>
                        <div class="btn-print" id="btnPrint" onclick="doPrint();">Print</div>
                        <div class="btn-cancel" id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div class="btn-nav" id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
    </div>
    </div><!--tab-content-->
    </div><!--subpage-->
</asp:Content>
