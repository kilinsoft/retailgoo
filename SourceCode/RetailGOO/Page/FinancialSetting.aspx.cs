﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class FinancialSetting : System.Web.UI.Page
    {
        public string CurrencyListString { get; set; }
        public string DefaultPriceTypeListString { get; set; }
        public string SalesTaxTypeListString { get; set; }
        public string CostingTypeListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // HIM: mock a listing
            //CurrencyListString = "[{Value:1,Text:'US Dollar ($)'}]";
            //DefaultPriceTypeListString = "[{Value:1,Text:'Normal Price'}]";
            //SalesTaxTypeListString = "[{Value:1,Text:'Include Tax'}]";
            //CostingTypeListString = "[{Value:1,Text:'Moving Avg'}]";

            var rspCurrency = Business.RetrieveCurrencyList();
            if (rspCurrency.Success && rspCurrency.ResponseItem != null && rspCurrency.ResponseItem.Count > 0)
                CurrencyListString = HtmlSelectHelper.ConvertToListString(rspCurrency.ResponseItem);
            //else
            //    CurrencyListString = "[{Value:-1,Text:'<anonymous>'}]";

            var rspDefaultPrice = Business.RetrieveDefaultPriceTypeList();
            if (rspDefaultPrice.Success && rspDefaultPrice.ResponseItem != null && rspDefaultPrice.ResponseItem.Count > 0)
                DefaultPriceTypeListString = HtmlSelectHelper.ConvertToListString(rspDefaultPrice.ResponseItem);
            //else
            //    DefaultPriceTypeListString = "[{Value:-1,Text:'<anonymous>'}]";

            var rspSalesTax = Business.RetrieveSalesTaxTypeList();
            if (rspSalesTax.Success && rspSalesTax.ResponseItem != null && rspSalesTax.ResponseItem.Count > 0)
                SalesTaxTypeListString = HtmlSelectHelper.ConvertToListString(rspSalesTax.ResponseItem);
            //else
            //    SalesTaxTypeListString = "[{Value:-1,Text:'<anonymous>'}]";

            var rspCosting = Business.RetrieveCostingTypeList();
            if (rspCosting.Success && rspCosting.ResponseItem != null && rspCosting.ResponseItem.Count > 0)
                CostingTypeListString = HtmlSelectHelper.ConvertToListString(rspCosting.ResponseItem);
            //else
            //    CostingTypeListString = "[{Value:-1,Text:'<anonymous>'}]";

        }
    }
}