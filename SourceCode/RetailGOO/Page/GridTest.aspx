﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NoAuth.Master" AutoEventWireup="true" CodeBehind="GridTest.aspx.cs" Inherits="RetailGOO.Page.GridTest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        div.outer-div {
            height: 410px;
            width: 710px;
            background-color: lightgray;
        }

        div.middle-div {
            /*width: 700px;
            height: 400px;*/  
            position: relative;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            background-color: lightyellow;
            display: table;
            margin: 0 auto;
        }
        .grid-container {
            width: 517px; /* 500 + 17px (scrollbar width) */
            height: 98px; /* 24px header + 100px body + 24px header */
        }
        .test-k-grid {
            width: 517px;
            height: 98px;

            overflow-y: scroll;
            overflow-x: hidden;
        }
        .test-k-grid-header {
            position: absolute;
            top: 0;
            z-index: 20;
        }
        .test-k-grid-header th {
            width: 90px;
            height: 24px;
            background-color: pink;
            border-bottom: 1px solid gray;
            border-right: 1px solid gray;

            text-align: center;
        }
        .test-k-grid-header th:last-child {
            border-right: none;
        }

        .test-k-grid-body {
            margin-top: 25px;
            margin-bottom: 25px;
        }
        .test-k-grid-body td {
            width: 90px;
            height: 24px;
            background-color: orange;
            border-bottom: 1px dotted gray;
            border-right: 1px dotted gray;

            text-align: center;
        }
        .test-k-grid-body td:last-child {
            border-right: none;
        }
        .k-grid-pagination {
            width: 400px;
            height: 24px;
            background-color: aqua;

            position: absolute;
            bottom: 0;
        }
    </style>
    <script>
        function pageLoad() {
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Item Name", name: "ItemFullName", required: false, editable: false },
            {
                title: "Qty", name: "Quantity", width: 100, required: true, editable: true, formatter: KGrid.Formatter.Amount(2)
            },
            {
                title: "Discount", name: "Discount", width: 100, required: true, editable: true, formatter: KGrid.Formatter.Amount(2)
            },
            { title: "Price", name: "Price", width: 100, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: true,
                enableAddBlankRow: false,
                displayPagination: true,
                rowPerPage: 10,
                numPageDisplayed: 2,
                onPageSelected: mgPageSelect
            }
            //g_grid = new KGrid.Grid("#theGrid", mainGridDef, mainGridColsDef, []);

            testData = [
                { ItemFullName: "1" },
                { ItemFullName: "2" },
                { ItemFullName: "3" },
                { ItemFullName: "4" },
                { ItemFullName: "5" },
                { ItemFullName: "6" },
                { ItemFullName: "7" },
                { ItemFullName: "11" },
                { ItemFullName: "21" },
                { ItemFullName: "31" },
                { ItemFullName: "41" },
                { ItemFullName: "51" },
                { ItemFullName: "61" },
                { ItemFullName: "71" },
                { ItemFullName: "1" },
                { ItemFullName: "2" },
                { ItemFullName: "3" },
                { ItemFullName: "4" },
                { ItemFullName: "5" },
                { ItemFullName: "6" },
                { ItemFullName: "7" },
                { ItemFullName: "11" },
                { ItemFullName: "21" },
                { ItemFullName: "31" },
                { ItemFullName: "41" },
                { ItemFullName: "51" },
                { ItemFullName: "61" },
                { ItemFullName: "1" },
                { ItemFullName: "2" },
                { ItemFullName: "3" },
                { ItemFullName: "4" },
                { ItemFullName: "5" },
                { ItemFullName: "6" },
                { ItemFullName: "7" },
                { ItemFullName: "11" },
                { ItemFullName: "21" },
                { ItemFullName: "31" },
                { ItemFullName: "41" },
                { ItemFullName: "51" },
                { ItemFullName: "61" },
                { ItemFullName: "1" },
                { ItemFullName: "2" },
                { ItemFullName: "3" },
                { ItemFullName: "4" },
                { ItemFullName: "5" },
                { ItemFullName: "6" },
                { ItemFullName: "7" },
                { ItemFullName: "11" },
                { ItemFullName: "21" },
                { ItemFullName: "31" },
                { ItemFullName: "41" },
                { ItemFullName: "51" },
                { ItemFullName: "61" },
                { ItemFullName: "1" },
                { ItemFullName: "2" },
                { ItemFullName: "3" },
                { ItemFullName: "4" },
                { ItemFullName: "5" },
                { ItemFullName: "6" },
                { ItemFullName: "7" },
                { ItemFullName: "11" },
                { ItemFullName: "21" },
                { ItemFullName: "31" },
                { ItemFullName: "41" },
                { ItemFullName: "51" },
                { ItemFullName: "61" }
            ];

            g_grid = new KGrid.Grid("#theGrid", mainGridDef, mainGridColsDef, testData);
            //g_grid.addRow({ ItemFullName: "22" }, KGrid.ItemState.newItem);
            //g_grid.addRow({ ItemFullName: "23" });
            //g_grid.addRow({ ItemFullName: "24" });
            //g_grid.addRow({ ItemFullName: "25" });
        }
        function mgPageSelect(pageIndex) {
            alert("pageIndex: " + pageIndex);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="outer-div">
        <div class="middle-div">
            <div class="grid-container">
                <div class="test-k-grid">
                    <table class="test-k-grid-header">
                        <thead>
                            <tr>
                                <th>Column 1</th>
                                <th>Column 2</th>
                                <th>Column 3</th>
                                <th>Column 4</th>
                                <th>Column 5</th>
                            </tr>
                        </thead>
                    </table>
                    <table class="test-k-grid-body">
                        <tbody>
                            <tr><td>test Row 1</td><td>test 2</td><td>test 3</td><td>test 4</td><td>test 5</td></tr>
                            <tr><td>test Row 2</td><td>test 2</td><td>test 3</td><td>test 4</td><td>test 5</td></tr>
                            <tr><td>test Row 3</td><td>test 2</td><td>test 3</td><td>test 4</td><td>test 5</td></tr>
                            <tr><td>test Row 4</td><td>test 2</td><td>test 3</td><td>test 4</td><td>test 5</td></tr>
                            <tr><td>test Row 5</td><td>test 2</td><td>test 3</td><td>test 4</td><td>test 5</td></tr>
                        </tbody>
                    </table>
                    <div class="k-grid-pagination">1 2 3 4 5</div>
                </div>
            </div>
        </div>
    </div>
    <div id="theGrid" style="width: 700px; height: 200px; background-color: pink;" ></div>
    <div class="aaa"></div>
</asp:Content>
