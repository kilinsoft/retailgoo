﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" 
    Inherits="RetailGOO.Page.User" ClientIDMode="Static"%>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <script type="text/javascript">
        
        // Data variables
        var g_staffList = <%=StaffListString%>;
        var g_countryList = <%=CountryListString%>;
        var g_roleList = <%=RoleListString%>;
        var g_currentStaffId = <%=CurrentStaffId%>;
        var g_genderList = [{Value: 'M', Text: 'Male'}, {Value: 'F', Text: 'Female'}];
        var g_titleList = [{Value: 'MR', Text: 'Mr.'}, {Value: 'MISS', Text: 'Miss'}, {Value: 'MRS', Text: 'Mrs'}];

        // Control variables
        var g_selStaff, g_selTitle, g_selGender, g_selCountry, g_selRole;

        function pageLoad() {

            preventNavigationOnUnsaved(true);

            g_selStaff = $("#selStaff").selectize({
                create: false, valueField: 'Value', labelField: 'Text',
                readOnly: true,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        retrieveStaffInfo(val);
                    }
                }
            })[0].selectize;

            g_selTitle = $("#selTitle").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            g_selGender = $("#selGender").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            g_selCountry = $("#selCountry").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            g_selRole = $("#selRole").selectize({
                create: false, valueField: 'Value', labelField: 'Text',
                readOnly: true
            })[0].selectize;

            $("#txtBirthdate").datepicker({autoclose: true, format: "yyyy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            // Initialize behaviors
            $("#btnVoid").addClass('disable').disable();
            $("#btnPrint").addClass('disable').disable();


            setComboBoxData(g_selStaff, g_staffList, g_currentStaffId);
            //setComboBoxData(g_selStaff, g_staffList, null);
            setComboBoxData(g_selTitle, g_titleList, null);
            setComboBoxData(g_selGender, g_genderList, null);
            setComboBoxData(g_selCountry, g_countryList, null);
            setComboBoxData(g_selRole, g_roleList, null);
        }

        function retrieveStaffInfo(staffId) {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveStaffInfo", staffId,
                function (items) {
                    g_activeMainItem = items;
                    setFormData(g_activeMainItem);
                });
        }

        function setFormData(item) {
            g_selTitle.setValue(item["Title"]);
            $("#txtFirstname").val(item["Firstname"]);
            $("#txtLastname").val(item["Lastname"]);
            $("#txtBirthdate").datepicker("setDate", new Date(item["Birthdate"]));
            g_selGender.setValue(item["Gender"]);
            $("#txtIdentification").val(item["Identification"]);
            $("#txtPhone").val(item["Phone"]);
            $("#txtFax").val(item["Fax"]);
            $("#txtEmail").val(item["Email"]);
            $("#txtFacebookId").val(item["FacebookId"]);
            $("#txtAddr1").val(item["Address1"]);
            $("#txtAddr2").val(item["Address2"]);
            $("#txtAddr3").val(item["Address3"]);
            $("#txtCity").val(item["City"]);
            g_selCountry.setValue(item["CountryId"] != -1? item["CountryId"] : "");
            $("#txtZipCode").val(item["ZipCode"]);
            g_selRole.setValue(item["Role"]);
        }
        function clearFormData() {
            g_selStaff.setValue("");
            g_selTitle.setValue("");
            $("#txtFirstname").val("");
            $("#txtLastname").val("");
            $("#txtBirthdate").val("");
            g_selGender.setValue("");
            $("#txtIdentification").val("");
            $("#txtPhone").val("");
            $("#txtFax").val("");
            $("#txtEmail").val("");
            $("#txtFacebookId").val("");
            $("#txtAddr1").val("");
            $("#txtAddr2").val("");
            $("#txtAddr3").val("");
            $("#txtCity").val("");
            g_selCountry.setValue("");
            $("#txtZipCode").val("");
            g_selRole.setValue("");
        }

        function constructDtoItem() {
            var item = {};
            item["StaffId"] = g_selStaff.getValue();
            item["Title"] = g_selTitle.getValue();
            item["Firstname"] = $("#txtFirstname").val();
            item["Lastname"] = $("#txtLastname").val();
            item["Birthdate"] = $("#txtBirthdate").val();
            item["Gender"] = g_selGender.getValue();
            item["Identification"] = $("#txtIdentification").val();
            item["Phone"] = $("#txtPhone").val();
            item["Fax"] = $("#txtFax").val();
            item["Email"] = $("#txtEmail").val();
            item["FacebookId"] = $("#txtFacebookId").val();
            item["Address1"] = $("#txtAddr1").val();
            item["Address2"] = $("#txtAddr2").val();
            item["Address3"] = $("#txtAddr3").val();
            item["ZipCode"] = $("#txtZipCode").val();
            item["City"] = $("#txtCity").val();
            item["CountryId"] = g_selCountry.getValue() != "" ? g_selCountry.getValue() : -1;
            item["Role"] = g_selRole.getValue();
            item["ItemState"] = KGrid.ItemState.modifiedItem;

            return item;
        }

        function validateForm() {
            if (g_selStaff.getValue() == "")
            {
                displayError("Please select User ID");
                return false;
            }
            return true;
        }

        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            //if (!validateSubItems())
            //    return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "BusinessService.svc", "ManageStaffInfo", item,
                function(rsp) {
                    setPageDirty(false);
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
        }
        function doClear() {
            displayModal(
                "Confirm Clear", 
                "Your unsaved data will be lost.<br>Do you want to continue?",
                function() {
                    setPageDirty(false);
                    clearFormData();
                },
                function() {
                });
        }
        function doPrint() {

        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('Roles.aspx');
        }
        function goNext() {
            changeUrl('BusinessInfo.aspx');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-red" style="margin-bottom: 35px;">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('BusinessInfo.aspx');">Business Info</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('FinancialSetting.aspx');">Financial Setting</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <%--<li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('HardwareSetting.aspx');">Hardware Setting</a></li>--%>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('Roles.aspx');">Roles</a></li>
            <li class="active"><a href="#" data-toggle="tab" onclick="GoToUrl('User.aspx')">User</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('DocSetting.aspx')">Doc Setting</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table class="form single">
                        <tr>
                            <td colspan="2" class="cell-header">
                                User Management
                            </td>
                        </tr>
				        <tr>
					        <td>User ID:</td>
                            <td>
                                <select id="selStaff"></select>
                            </td>
				        </tr>
                        <tr>
					        <td>Initial:</td>
                            <td>
                                <select id="selTitle" placeHolder="-- initial --"></select>
                            </td>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td>
                                <input type="text" id="txtFirstname" class="width-250" />
                            </td>
				        </tr>
                        <tr>
                            <td>Surname:</td>
                            <td>
                                <input type="text" id="txtLastname" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Birthday:</td>
                            <td>
<%--                                <select id="selDay" style="width:70px;">
                                    <option value="1">07</option>
                                </select>
                                <select id="selMonth" style="width:100px; padding-left: 5px;">
                                    <option value="1">August</option>
                                </select>
                                <select id="selYear" style="width:70px; padding-left: 5px;">
                                    <option value="1">1977</option>
                                </select>--%>
                                <input type="text" id="txtBirthdate" placeHolder="-- birthday --" style="width: 230px;" />
                                <img src="../images/icon_calendar.png">
                            </td>
                        </tr>
                        <tr>
                            <td>Sex:</td>
                            <td>
                                <select id="selGender" placeHolder="-- sex --"></select>
                            </td>
                        </tr>
                        <tr>
                            <td>ID:</td>
                            <td>
                                <input type="text" id="txtIdentification" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td>
                                <input type="text" id="txtPhone" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Fax:</td>
                            <td>
                                <input type="text" id="txtFax" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>
                                <input type="text" id="txtEmail" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Facebook ID:</td>
                            <td>
                                <input type="text" id="txtFacebookId" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td>
                                <input type="text" id="txtAddr1" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtAddr2" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtAddr3" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>
                                <input type="text" id="txtCity" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>
                                <select id="selCountry" placeHolder="-- country --"></select>
                            </td>
                        </tr>
                        <tr>
                            <td>Zip Code:</td>
                            <td>
                                <input type="text" id="txtZipCode" class="width-250" />
                            </td>
                        </tr>
                        <tr>
                            <td>Business Role:</td>
                            <td>
                                <select id="selRole"></select>
                            </td>
                        </tr>
			        </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div class="btn-nav" id="btnBackward" onclick="goBack();">&lt;</div>
                        <div class="btn-save" id="btnSave" onclick="doSave();">Save</div>
                        <div class="btn-void" id="btnVoid" onclick="doVoid();">Void</div>
                        <div class="btn-clear" id="btnClear" onclick="doClear();">Clear</div>
                        <div class="btn-print" id="btnPrint" onclick="doPrint();">Print</div>
                        <div class="btn-cancel" id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div class="btn-nav" id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
            </div>
        </div><!--tab-content-->
    </div><!--subpage-->
</asp:Content>