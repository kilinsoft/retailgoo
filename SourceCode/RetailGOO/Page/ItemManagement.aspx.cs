﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;

namespace RetailGOO.Page
{
    public partial class ItemManagement : System.Web.UI.Page
    {
        public string CategoryListString { get; set; }
        public string BrandListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rsp = ItemAdapter.RetrieveItemCategoryList();
            if (rsp.Success && rsp.ResponseItem != null && rsp.ResponseItem.Count > 0)
                CategoryListString = HtmlSelectHelper.ConvertToListString(rsp.ResponseItem);
            else
                CategoryListString = "[{Value:-1,Text:'<undefined>'}]";

            var rsp2 = ItemAdapter.RetrieveItemBrandList();
            if (rsp2.Success && rsp2.ResponseItem != null && rsp2.ResponseItem.Count > 0)
                BrandListString = HtmlSelectHelper.ConvertToListString(rsp2.ResponseItem);
            else
                BrandListString = "[{Value:-1,Text:'<undefined>'}]";
        }
    }
}