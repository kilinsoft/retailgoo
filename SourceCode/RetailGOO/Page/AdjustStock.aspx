﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="AdjustStock.aspx.cs" Inherits="RetailGOO.Page.AdjustStock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .grid-note table {
            width: 100%;
        }
        .grid-note td:first-child {
            width: 70px;
        }
        .grid-note td:first-child + td {
            width: auto;
        }
        .grid-note input[type=text] {
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        // Control variables
        var g_selReason;

        // Data variables
        var g_reasonList = <%=ReasonListString%>;
        
        function pageLoad() {

            preventNavigationOnUnsaved();

            // Initialize variables
            // Category List
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "ID", name: "AdjustmentNo", required: false, editable: false },
            { title: "Name", name: "AdjustmentName", required: false, editable: false },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Status", name: "Status", required: false, editable: false }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);
            var selEditorOptions = {
                create: true,
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                onLoad: onLoadHandler,
                options: []
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            // Adjust Stock
            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            {
                title: "Item", name: "ItemId", width: 300, required: true, editable: true,
                editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions),
                onChanged: onItemChangedHandler
            },
            {
                title: "Current Qty", name: "QuantityBefore", required: true, editable: false,
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0),
                onChanged: recalculateQtyAfter
            },
            {
                title: "New Qty", name: "QuantityAfter", required: true, editable: false,
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0),
                onChanged: recalculateQtyAfter
            },
            {
                title: "Difference", name: "Difference", required: true, editable: true,
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0),
                onChanged: recalculateQtyAfter
            }
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            $("#txtDate").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            g_selReason = $("#selReason").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_reasonList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveReasonInfo(val);
                    }
                },
            })[0].selectize;

            initGridSorter(g_subGrid, "#selSubSelectedBy",
            [{ title: "Item", name: "ItemFullName", numberComparison: false },
            { title: "Current Qty", name: "QuantityBefore", valueCompare: true },
            { title: "New Qty", name: "QuantityAfter", valueCompare: true },
            { title: "Difference", name: "Difference", valueCompare: true }]);

            initGridSorter(g_mainGrid, "#selMainSelectedBy",
            [{ title: "ID", name: "No", valueCompare: false },
            { title: "Name", name: "Name", valueCompare: false },
            { title: "Date", name: "Date", valueCompare: false },
            { title: "Status", name: "Status", valueCompare: false }]
            );

            // Initialize behaviors
            $("#btnSubEdit").disable();

            // Init state
            changeState(ePageState.mainNew);

            // Ajax calls to initialize mandatory variables
            retrieveMainGridData();
        }
        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#txtAdjName").readOnly(true);
            $("#txtNote").readOnly(true);
            g_selReason.disable();
            $("#txtAdjNo").readOnly(true);
            $("#txtNote").readOnly(true);
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtAdjName").readOnly(false);
            $("#txtNote").readOnly(false);
            g_selReason.enable();
            $("#txtAdjNo").readOnly(false);
            $("#txtNote").readOnly(false);
        }
        function setFormData(item) {
            $("#txtAdjName").val(item["AdjustmentName"]);
            $("#txtDate").datepicker("setDate", new Date(item["Date"]));
            g_selReason.setValue(item["AdjustmentReasonId"]);
            $("#txtAdjNo").val(item["AdjustmentNo"]);
            $("#txtNote").val(item["Note"]);

            //showVoid(item["Status"].toLowerCase() == "void");
        }
        function clearFormData() {
            $("#tblForm input[type=text]").val("");
            $("#txtAdjName").val("");
            $("#txtDate").datepicker("setDate", new Date());
            if (g_reasonList.length > 0)
                g_selReason.setValue(1);
            $("#txtAdjNo").val("");
            $("#txtNote").val("");
        }
        function retrieveSubGridData(adjustmentId, funcOnSuccess) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveAllItemAdjustmentItem", adjustmentId,
                function (items) {
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveAllItemAdjustment", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(adjustmentId) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveItemAdjustment", adjustmentId,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveReasonInfo(reasonId)
        {
        }
        function setGridData(grid, data) {
            if (grid === g_mainGrid) {
                g_mainItems = data;
                $("#selMainSelectedBy option:first-child").prop("selected", true);
            }
            else if (grid === g_subGrid) {
                g_subItems = data;
                $("#selSubSelectedBy option:first-child").prop("selected", true);
            }
            grid.setData(data);
        }
        function displayMainItemData(item) {
            setFormData(item);
            retrieveSubGridData(item["AdjustmentId"]);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            return true;
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    //showVoid(false);
                    enableSaveClear();

                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formLock();
                    subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }
        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable(); //already active
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable(); //already active
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if (g_selReason.getValue() == "") {
                displayError("Please specify a reason");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on row [" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            item["AdjustmentId"] = row == null ? -1 : row.data["AdjustmentId"];
            item["AdjustmentName"] = $("#txtAdjName").val();
            item["AdjustmentReasonId"] = g_selReason.getValue();
            item["AdjustmentNo"] = $("#txtAdjNo").val();
            item["Note"] = $("#txtNote").val();
            item["Status"] = "New";
            item["ItemState"] = (g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem : KGrid.ItemState.modifiedItem);
            item["Version"] = row == null ? "" : row.data["Version"];
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                //if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                if (row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    subItem["AdjustmentItemId"] = row.data["AdjustmentItemId"] == null ? -1 : parseInt(row.data["AdjustmentItemId"], 10);
                    subItem["AdjustmentId"] = row.data["AdjustmentId"] == null ? -1 : parseInt(row.data["AdjustmentId"], 10);
                    subItem["ItemId"] = !isNaN(parseInt(row.data["ItemId"], 10)) ? parseInt(row.data["ItemId"], 10) : -1;
                    subItem["ItemFullName"] = row.data["ItemFullName"];
                    subItem["QuantityBefore"] = row.data["QuantityBefore"];
                    subItem["QuantityAfter"] = row.data["QuantityAfter"];
                    subItem["ItemState"] = row.rowState;
                    item["Items"].push(subItem);
                }
            }
            return item;
        }
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "ItemManagement.svc", "ManageItemAdjustment", item,
                function(rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        if (selectedRow != null)
                            selectedRow.select(null, false);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            var row = g_mainGrid.selectedRow;
            if (row == null) {
                displayError("Please select an item first");
                return;
            }
            if (row.data["IsVoided"]) {
                displayError("The item has already been marked as VOID");
                return; 
            }
            var fContinue = window.confirm("Voiding the item will affect all of its related items as well.\n\nDo you want to continue?");
            if (fContinue) {
                var idVoid = parseInt(row.data["AdjustmentId"], 10);
                sendAjaxUpdate( "ItemManagement.svc", "VoidItemAdjustment", idVoid,
                    function(rsp) {
                        setPageDirty(false);
                        changeState(ePageState.mainView);
                        var iRow = g_mainGrid.selectedRow.rowIndex;
                        retrieveMainGridData(function() {
                            g_mainGrid.selectRow(iRow);
                        });
                    },
                    function(rsp) {
                        displayError(rsp.StatusText);
                    });
            }
        }
        function doClear() {
            var fClear = window.confirm("Your unsaved data will be lost.\nDo you want to continue?");
            if (fClear) {
                setPageDirty(false);
                switch(g_pageState) {
                    case ePageState.mainNew:
                        changeState(ePageState.mainNew);
                        break;
                    case ePageState.mainView:
                        break;
                    case ePageState.mainEdit:
                        if (g_mainGrid.data.length > 0)
                            setFormData(g_mainGrid.data[g_mainGrid.selectedRow.rowIndex]);
                        setGridData(g_subGrid, g_subItems);
                        g_subGrid.unlock();
                        changeState(ePageState.mainEdit);
                        //changeState(ePageState.mainNew);
                        break;
                    default:
                        break;
                }
            }
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/AdjustStockA.aspx";
            var docTemplateBUrl = "../Printable/AdjustStockB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Adjust Stock", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('CurrentStock.aspx');
        }
        function goNext() {
            changeUrl('TransferStock.aspx');
        }
        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["AdjustmentId"]);
                }
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        function sgOnItemChanged(pCell, val) {
        }
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_subItems, "ItemId", pVal, "ItemFullName");
        }
        function onLoadHandler(pCell, pCallback) {
            retrieveAjaxDataAsync(
                "ItemManagement.svc", "RetrieveItemList", {},
                function (items) {
                    g_itemList = items;
                    pCallback(g_itemList);
                },
                function (txt) {
                    displayError(txt);
                    pCallback();
                });
        }
        function onItemChangedHandler(pCell, pVal) {
            var row = pCell.parentRow;
            var qtybeforeCell = row.getCellByColumnName("QuantityBefore");
            var qtyafterCell = row.getCellByColumnName("QuantityAfter");
            var diffCell = row.getCellByColumnName("Difference");

            if (pVal === "") {
                if (row.rowState == KGrid.ItemState.newItem) {
                    qtybeforeCell.setValue("");
                    qtyafterCell.setValue("");
                    diffCell.setValue("");
                }
                else
                    return;
            } else if (isNaN(parseInt(pVal, 10))) { // user specify non-existent item
                qtybeforeCell.setValue(0);
                qtyafterCell.setValue(0);
                diffCell.setValue(0);

                recalculateQtyAfter(pCell, pVal);
            } else {
                retrieveAjaxDataAsync(
                    "ItemManagement.svc", "RetrieveItem", pVal,
                    function (item) {
                        qtybeforeCell.setValue(item.Quantity);
                        qtyafterCell.setValue(item.Quantity);
                        diffCell.setValue(0);

                        //recalculateQtyAfter(pCell, pVal);
                    },
                    function (txt) {
                        displayError(txt);
                    });
            }
        }
        function recalculateQtyAfter(pCell, pVal) {
            var row = pCell.parentRow;
            var qtybeforeCell = row.getCellByColumnName("QuantityBefore");
            var qtyafterCell = row.getCellByColumnName("QuantityAfter");
            var diffCell = row.getCellByColumnName("Difference");

            var qtybefore = qtybeforeCell.value;
            var diff = diffCell.value;

            qtyafterCell.setValue(parseFloat(qtybefore) + diff);
        }
        function showVoid(fShow) {
            if (fShow)
                $("#tdVoid").empty().append("<div>VOID</div>");
            else
                $("#tdVoid").empty();
        }
        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                    $("#btnMail").disable();
                    $("#btnReceive").disable();
                }
                else {
                    var rowData = pRow.data;
                    var fVoid = rowData["IsVoided"];
                    if (fVoid) {
                        $("#btnMainEdit").disable();
                        $("#btnMainDelete").enable();
                        $("#btnMail").disable();
                        $("#btnReceive").disable();
                    } else {
                        $("#btnMainEdit").enable();
                        $("#btnMainDelete").disable();
                        $("#btnMail").enable();
                        $("#btnReceive").enable();
                    }
                }
            }
            else { // subGrid
                if (pRow == null) {
                    $("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    if (pRow.readOnly)
                        $("#btnSubEdit").enable();
                    else
                        $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }
        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            //$("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();
            
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-orange">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ItemManagement.aspx');">ItemManagement</a></li>
            <li class="inactive" style="width: 184px;"><a href="#" data-toggle="tab" onclick="changeUrl('CategoryManagement.aspx');">Category Management</a></li>
            <li class="inactive" style="width: 180px;"><a href="#" data-toggle="tab" onclick="changeUrl('BrandManagement.aspx')">Brand Management</a></li>
            <%--<li class="inactive" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CountSheet.aspx');">Count Sheet</a></li>--%>
            <li class="inactive" style="width: 280px;"><a href="#" data-toggle="tab" onclick="changeUrl('CurrentStock.aspx');">Current Stock</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="active"><a href="#" data-toggle="tab" onclick="changeUrl('AdjustStock.aspx');">Adjust Stock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('TransferStock.aspx');">Transfer Stock</a></li>
            <li class="inactive" style="width: 304px;"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveTransferStock.aspx')">Receive TransferStock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReorderStock.aspx');">Reorder Stock</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section"><!--form-->
                    <table id="tblForm" class="form double">
                        <tr>
                            <td>Adjust Name:</td>
                            <td>
                                <input type="text" id="txtAdjName" />
                            </td>
                            <td>Date:</td>
                            <td>
                                <input type="text" id="txtDate" />
                            </td>
                        </tr>
                        <tr>
                            <td>Reason:</td>
                            <td>
                                <select id="selReason" placeholder="-- search --" tabindex="1">
                                </select>
                            </td>
                            <td>Number:</td>
                            <td>
                                <input type="text" id="txtAdjNo" />
                            </td>
                        </tr>
			        </table>
                </div><!--form/end-->
                <div class="grid-command no-border">
                    <div>
                        <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="onSubEdit();" />
                        <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="onSubDelete();" />
                        <input type="button" id="btnSubUndoDelete" class="btn-action" value="Undo" onclick="onSubUndoDelete();" />
                    </div>
                    <div>
                        <span>Selected by:</span>
                        <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                        </select>
                    </div>
                </div>
                <div class="section">
                    <!-- AdjustStock grid -->
                    <div id="subGrid" class="search" style="height: 180px;">
                    </div>
                </div>
                <div class="section grid-note">
                    <table>
                        <tr>
                            <td class="caption-normal">Notes:</td>
                            <td>
                                <input type="text" id="txtNote" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div>
                    <table class="form single">
                        <tr>
                            <td class="cell-header">Category List:</td>
                        </tr>
			        </table>
                </div>
                <div class="grid-command no-border">
                    <div>
                        <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                        <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="OnMainDelete();" />
                    </div>
                    <div>
                        <span>Selected by:</span>
                        <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                        </select>
                    </div>
                </div>
                <div>
                    <!-- Category List grid -->
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>

            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
</asp:Content>
