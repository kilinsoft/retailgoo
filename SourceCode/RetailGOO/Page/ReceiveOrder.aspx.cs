﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class ReceiveOrder : System.Web.UI.Page
    {
        public string TaxFactor { get; set; }
        public int DocumentTemplate { get; set; }
        public string SupplierListString { get; set; }
        public string ItemListString { get; set; }
        //public string PurchaseOrderListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspSupplier = SupplierAdapter.RetrieveSupplierList();
            if (rspSupplier.Success && rspSupplier.ResponseItem != null && rspSupplier.ResponseItem.Count > 0)
                SupplierListString = HtmlSelectHelper.ConvertToListString(rspSupplier.ResponseItem);
            else
                SupplierListString = "[{Value:-1,Text:'<anonymous>'}]";

            var rspItem = ItemAdapter.RetrieveItemList();
            if (rspItem.Success && rspItem.ResponseItem != null && rspItem.ResponseItem.Count > 0)
                ItemListString = HtmlSelectHelper.ConvertToListString(rspItem.ResponseItem);
            else
                ItemListString = "[]";
            // Change to retrieve PurchaseOrderList according to selected Supplier instead
                //var rspPoList = PurchaseAdapter.RetrievePurchaseOrderList();
                //if (rspPoList.Success && rspPoList.ResponseItem != null && rspPoList.ResponseItem.Count > 0)
                //    PurchaseOrderListString = HtmlSelectHelper.ConvertToListString(rspPoList.ResponseItem);
                //else
                //    PurchaseOrderListString = "[]";

            TaxFactor = SessionHelper.BusinessInfo.Tax.ToString();
            DocumentTemplate = SessionHelper.BusinessInfo.DocSetting[BusinessScreenString.PurchaseOrder];
        }
    }
}