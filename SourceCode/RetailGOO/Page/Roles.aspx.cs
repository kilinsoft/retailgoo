﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.Util;
using RetailGOO.Class;
using RetailGOO.BusinessLayer;

namespace RetailGOO.Page
{
    public partial class Roles : System.Web.UI.Page
    {
        public string RoleListString { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            var rspRoleList = Business.RetrieveRoleList();
            if (rspRoleList.Success && rspRoleList.ResponseItem != null && rspRoleList.ResponseItem.Count > 0)
                RoleListString = HtmlSelectHelper.ConvertToListString(rspRoleList.ResponseItem);
            else
                RoleListString = "[]";
        }
    }
}