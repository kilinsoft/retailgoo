﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="OutgoingPayment.aspx.cs" Inherits="RetailGOO.Page.OutgoingPayment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .nav-tabs > li {
            width: 160px !important;
        }
    </style>
    <script type="text/javascript">

        // Control variables
        var g_subtotal, g_tax, g_total;
        var g_selSupplier;
        var g_selPaymentType;
        var g_docTemplate = <%=DocumentTemplate%>;

        // Data variables
        var g_paymentTypeList = <%=PaymentTypeListString%>;
        var g_supplierList = <%=SupplierListString%>;
        var g_taxFactor = <%=TaxFactor%>;
        var g_itemList = null;

        function pageLoad() {
               
            preventNavigationOnUnsaved(true);

            // Initialize variables

            var selEditorOptions = {
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                onLoad: onLoadHandler,
                onChanged: retrieveReceiveOrderInfo,
                options: []
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Supplier Invoice", name: "SupplierInvoice", required: false, editable: true },
            { title: "RO Number", name: "ReceiveOrderId", required: true, editable: true, editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions) },
            { title: "Description", name: "Description", required: false, editable: true },
            { title: "Amount", name: "Amount", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) },
            { title: "Due", name: "AmountDue", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) },
            { title: "Pay", name: "AmountPaid", required: true, editable: true, editor: KGrid.Editor.Amount(2), formatter: KGrid.Formatter.Amount(2) },
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected
                //onRowChanged: amountChangeHandler
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Payment No", name: "OutgoingPaymentNo", required: false, editable: false },
            { title: "Ref", name: "Reference", required: false, editable: false },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Name", name: "SupplierName", required: false, editable: false },
            { title: "Total", name: "Total", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                onRowMouseEnter: mgOnRowMouseEnter,
                onRowMouseLeave: mgOnRowMouseLeave
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            $("#txtDate").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            g_selSupplier = $("#selSupplier").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_supplierList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveSupplierInfo(val);
                    }
                }
            })[0].selectize;

            g_selPaymentType = $("#selPaymentType").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_paymentTypeList
            })[0].selectize;

            // Summary calculation
            var funcOnTaxChanged = function($e, val) {
                var tax = parseFloat(val);
                var subtotal = parseFloat(g_subtotal.getValue());
                var total = tax + subtotal;
                g_total.setValue(total);
            };
            var funcOnSubtotalChanged = function($e, val) { 
                var subtotal = parseFloat(val);
                var taxedAmount = parseFloat(g_taxFactor) * subtotal;
                g_tax.setValue(taxedAmount);
                funcOnTaxChanged(g_tax.$element, taxedAmount); // Call onTaxChanged next
            };
            g_subtotal = new Util.Input.Amount("#txtSubtotal", 2, funcOnSubtotalChanged);
            g_tax = new Util.Input.Amount("#txtTax", 2, funcOnTaxChanged);
            g_total = new Util.Input.Amount("#txtTotal", 2);

            initGridSorter(g_subGrid, "#selSubSelectedBy",
                [{ title: "Supplier Invoice", name: "SupplierInvoice", valueCompare: false }, 
                { title: "RO Number", name: "ReceiveOrderNo", valueCompare: false },
                { title: "Description", name: "Description", valueCompare: false },
                { title: "Amount", name: "Amount", valueCompare: true },
                { title: "AmountDue", name: "AmountDue", valueCompare: true },
                { title: "AmountPay", name: "AmountPaid", valueCompare: true }]);
            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "Payment No", name: "OutgoingPaymentNo", numberComparison: false }, 
                { title: "Ref", name: "Reference", valueCompare: false },
                { title: "Date", name: "Date", valueCompare: false },
                { title: "Name", name: "SupplierName", valueCompare: false },
                { title: "Total", name: "Total", valueCompare: true }]);

            // Init elements' behaviors
            $("#txtStatus").readOnly(true);
            $("#btnSubEdit").disable();

            // Init element's values

            // Init state
            changeState(ePageState.mainNew);

            // Behaviors
            g_subtotal.disable();
            //g_tax.disable();
            g_total.disable();

            retrieveMainGridData();
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#txtNote").readOnly(true);
            g_selSupplier.disable();
            g_selPaymentType.disable();
            $("#txtDate").prop('disabled', true);
            $("#txtTaxIdInfo").prop('disabled', true);
            g_subtotal.disable();
            g_tax.disable();
            g_total.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtStatus").readOnly(true);
            $("#txtOpNo").readOnly(true);
            $("#txtDate").prop('disabled', false);
            $("#txtNote").readOnly(false);
            $("#txtTaxIdInfo").prop('disabled', false);
            g_selSupplier.enable();
            g_selPaymentType.enable();
            //g_subtotal.enable();
            g_tax.enable();
            //g_total.enable();
        }

        function setFormData(item) {
            g_selSupplier.setValue(item["SupplierId"]);
            $("#txtRef").val(item["Reference"]);
            $("#txtOpNo").val(item["OutgoingPaymentNo"]);
            $("#txtDate").datepicker("setDate", new Date(item["Date"]));
            $("#txtDetail1").val(item["Detail1"]);
            $("#txtDetail2").val(item["Detail2"]);
            $("#txtDetail3").val(item["Detail3"]);
            $("#txtStatus").val(item["Status"]);
            $("#txtPhone").val(item["Phone"]);
            $("#txtTaxIdInfo").val(item["TaxIdInfo"]);

            g_selPaymentType.setValue(item["PaymentTypeId"]);
            $("#txtNote").val(item["Note"]);
            showVoid(item["Status"].toLowerCase() == "void");

            g_subtotal.setValue(item["Subtotal"]);
            g_tax.setValue(item["Tax"]);
            g_total.setValue(item["Total"]);
        }
        function clearFormData() {
            if (g_supplierList.length > 0)
                g_selSupplier.setValue(-1);
            $("#tblForm input[type=text]").val("");
            $("#txtStatus").val("Paid");
            $("#txtNote").val("");
            $("#txtDate").datepicker("setDate", new Date());
            if (g_paymentTypeList.length > 0)
                g_selPaymentType.setValue(g_paymentTypeList[0]["Value"]);

            g_subtotal.setValue(0);
            g_tax.setValue(0);
            g_total.setValue(0);
        }

        function retrieveSubGridData(id, funcOnSuccess) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllOutgoingPaymentItem", id,
                function (items) {
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllOutgoingPayment", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(id) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveOutgoingPayment", id,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveSupplierInfo(supplierId) {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveSupplierInfo", supplierId,
                function (item) {
                    //$("#txtContact").val(item.ContactPerson);
                    $("#txtDetail1").val(item["Address1"]);
                    $("#txtDetail2").val(item["Address2"]);
                    $("#txtDetail3").val(item["Address3"]);
                    $("#txtPhone").val(item["Phone"]);
                });
        }
        function retrieveReceiveOrderInfo(pCell, pItem) {
            var receiveOrderId = parseInt(pItem.Value);
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveReceiveOrder", receiveOrderId,
                function (item) {
                    var row = pCell.parentRow;
                    var amt = item["Amount"];
                    var amtDue = item["AmountDue"];
                    var amtPay = amtDue;
                    var roNo = item["ReceiveOrderNo"];
                    var amtCell = row.getCellByColumnName("Amount");
                    var amtDueCell = row.getCellByColumnName("AmountDue");
                    var amtPayCell = row.getCellByColumnName("AmountPaid");
                    amtCell.setValue(amt);
                    amtDueCell.setValue(amtDue);
                    amtPayCell.setValue(amtPay);
                    pCell.parentRow.data["ReceiveOrderNo"] = roNo;

                    pCell.parentRow.triggerNextEditableCell(pCell);

                    recalculateSummary();
                });
        }

        function displayMainItemData(item) {
            setFormData(item);
            retrieveSubGridData(item["OutgoingPaymentId"]);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            return true;
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    showVoid(false);
                    enableSaveClear();
                    
                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formLock();
                    subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }

        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable(); //already active
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable(); //already active
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if (g_selSupplier.getValue() == "") {
                displayError("Please specify a payee");
                return false;
            }
            if ($("#txtDate").val() == "") {
                displayError("Please specify the Date");
                return false;
            }
            if (!validateFloat(g_subtotal.getValue()) || !g_subtotal.isValid()) {
                displayError("Invalid Subtotal");
                return false;
            }
            if (!validateFloat(g_tax.getValue()) || !g_tax.isValid()) {
                displayError("Invalid Tax");
                return false;
            }
            if (!validateFloat(g_total.getValue()) || !g_total.isValid()) {
                displayError("Invalid Total");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on row [" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            item["OutgoingPaymentId"] = row == null ? -1 : row.data["OutgointPaymentId"];
            item["BusinessId"] = -1;
            item["OutgoingPaymentNo"] = row == null ? -1 : row.data["OutgoingPaymentNo"];
            item["SupplierId"] = g_selSupplier.getValue();
            item["SupplierName"] = g_selSupplier.getOption(g_selSupplier.getValue())[0].innerText;
            item["Reference"] = $("#txtRef").val();
            //item["Contact"] = $("#txtContact").val();
            item["Address1"] = $("#txtDetail1").val();
            item["Address2"] = $("#txtDetail2").val();
            item["Address3"] = $("#txtDetail3").val();
            item["Date"] = convertToUTCDateTime($("#txtDate").val());
            item["Status"] = "Paid";
            item["Phone"] = $("#txtPhone").val();
            item["TaxIdInfo"] = $("#txtTaxIdInfo").val();
            item["AmountTypeId"] = -1;
            item["AmountTypeName"] = "";
            item["PaymentTypeId"] = g_selPaymentType.getValue();
            item["PaymentTypeName"] = g_selPaymentType.getOption(g_selPaymentType.getValue())[0].innerText;
            item["Note"] = $("#txtNote").val();
            item["Subtotal"] = g_subtotal.getValue();
            item["Tax"] = g_tax.getValue();
            item["Total"] = g_total.getValue();
            item["ItemState"] = (g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem : KGrid.ItemState.modifiedItem);
            item["Version"] = row == null ? "" : row.data["Version"];
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                //if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                if (row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    subItem["OutgoingPaymentItemId"] = row.data["OutgoingPaymentItemId"] == null ? -1 : parseInt(row.data["OutgoingPaymentItemId"], 10);
                    subItem["SupplierInvoice"] = row.data["SupplierInvoice"] == null ? "" : row.data["SupplierInvoice"];
                    subItem["ReceiveOrderId"] = parseInt(row.data["ReceiveOrderId"], 10);
                    subItem["ReceiveOrderNo"] = row.data["ReceiveOrderNo"];
                    subItem["Description"] = row.data["Description"] == null ? "" : row.data["Description"];
                    subItem["Amount"] = parseFloat(row.data["Amount"]);
                    subItem["AmountDue"] = parseFloat(row.data["AmountDue"]);
                    subItem["AmountPaid"] = parseFloat(row.data["AmountPaid"]);

                    subItem["ItemState"] = row.rowState;
                    item["Items"].push(subItem);
                }
            }

            return item;
        }
        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "Purchasing.svc", "ManageOutgoingPayment", item,
                function(rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        selectedRow.select(null, true);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            doDefaultVoid("Purchasing.svc", "VoidOutgoingPayment", "OutgoingPaymentId");
        }
        function doClear() {
            if (preventNavigationOnUnsaved() && isPageDirty())
                doDefaultClear();
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/OutgoingPaymentA.aspx";
            var docTemplateBUrl = "../Printable/OutgoingPaymentB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Outgoing Payment", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('ReceiveOrder.aspx');
        }
        function goNext() {
            changeUrl('CashPurchase.aspx');
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["OutgoingPaymentId"]);
                }  
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function mgOnRowMouseEnter(pRow) {
            delayDisplayTooltip(displayTableView, pRow.data["OutgoingPaymentId"]);
        }
        function mgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function displayTableView(value) {
            //$("#divDynamicTooltip").appendTo("body"); // So the div does not display relatively to its parent but the body instead
            //retrieveAjaxDataSync("Purchasing.svc", "RetrieveOutgoingPayment", value,
            sendAjax("../Service/Purchasing.svc/RetrieveOutgoingPayment", value,
                function (rsp) {
                    var item = rsp.ResponseItem;
                    var tooltipContent;
                    tooltipContent = 'Date: ' + item['Date'] + '</br>';
                    tooltipContent += 'Status: ' + item['Status'] + '</br>';
                    tooltipContent += 'Payment Method: ' + g_selPaymentType.getOption(item['PaymentTypeId'])[0].innerText;
                    $('#divDynamicTooltip').html(tooltipContent);
                    $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
                },
                function (type, err, errThrown) {
                });
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        function sgOnItemChanged(pCell, val) {
        }
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_subItems, "ReceiveOrderId", pVal, "ReceiveOrderNo");
        }
        function onLoadHandler(pCell, pCallback) {
            retrieveAjaxDataAsync( 
                "Purchasing.svc", "RetrieveReceiveOrderList", {},
                function(items) {
                    g_itemList = items;
                    pCallback(g_itemList);
                },
                function (txt) {
                    displayError(txt);
                    pCallback();
                });
        }
        //function amountChangeHandler(pRow, pCell) {
        //    //recalculateAmount(pCell);
        //    setTimeout(recalculateSummary(), 1000);
        //}
        //function recalculateAmount(pCell) {
        //    var amtCell = pCell.parentRow.cells[4];
        //    var amtDueCell = pCell.parentRow.cells[5];
        //    var amtPayCell = pCell.parentRow.cells[6];
        //    var amt = amtCell.value == null ? 0 : amtCell.value;
        //    var amtDue = amtDueCell.value == null ? 0 : amtDueCell.value;
        //    var amtPay = amtPayCell.value == null ? 0 : amtPayCell.value;
        //    amtCell.setValue( parseFloat(qty) * parseFloat(price) );
        //}
        function recalculateSummary() {
            var d = g_subGrid.data;
            var subtotal = 0;
            for (var i = 0; i < d.length; ++i) {
                var val = parseFloat(d[i]["AmountPaid"]);
                if (!isNaN(val))
                    subtotal += val;
            }
            g_subtotal.setValue(subtotal);
            var taxedAmount = parseFloat(g_taxFactor) * parseFloat(subtotal);
            g_tax.setValue(taxedAmount);
            g_total.setValue(taxedAmount + subtotal);
        }
        function showVoid(fShow) {
            if (fShow)
                $("#tdVoid").empty().append("<div>VOID</div>");
            else
                $("#tdVoid").empty();
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                }
                else {
                    var rowData = pRow.data;
                    var fVoid = (rowData["Status"].toLowerCase() == "void");
                    if (fVoid) {
                        $("#btnMainEdit").disable();
                        $("#btnMainDelete").enable();
                    } else {
                        $("#btnMainEdit").enable();
                        $("#btnMainDelete").disable();
                    }
                }
            }
            else { // subGrid
                if (pRow == null) {
                    //$("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    //if (pRow.readOnly)
                    //    $("#btnSubEdit").enable();
                    //else
                    //    $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }

        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            //$("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();
            
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            if (!canEditMainItem(g_activeMainItem)) {
                displayInfo("Cannot edit this type of item");
                return;
            }
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
        }
        function onMainDelete() {
            if (!canDeleteMainItem(g_mainGrid.selectedRow.rowIndex)) {
                displayInfo("Cannot delete this type of item");
                return;
            }
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();

            if (selectedRow.data["Status"].toLowerCase() == "void") {
                if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?"))
                {
                    var data = selectedRow.data;
                    var item = {};
                    item["OutgoingPaymentId"] = data["OutgoingPaymentId"];
                    item["Version"] = data["Version"];
                    item["ItemState"] = KGrid.ItemState.deletedItem;
                    //item["Items"] = [];

                    sendAjaxUpdate( "Purchasing.svc", "ManageOutgoingPayment", item,
                        function(rsp) {
                            setPageDirty(false);
                            retrieveMainGridData();
                            changeState(ePageState.mainNew);
                        },
                        function(rsp) {
                            displayError(rsp.StatusText);
                        });
                }
            }
            else {
                displayError("Please VOID the item before deleting it");
            }
        }
        function canEditMainItem(item) {
            // Pro : ToDo
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-green">
        <ul id="yellow" class="nav nav-tabs">
            <li class="inactive"><a href="#tab1" data-toggle="tab" onclick="changeUrl('CashPurchase.aspx');">Cash Purchase</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('PurchaseOrder.aspx');">Purchase Order</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveOrder.aspx')">Receive Order</a></li>
            <li class="active"><a href="#" data-toggle="tab">Outgoing Payment</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('Expense.aspx');">Expense</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tabAddUser">
                <!-- Form Pane -->
                <div id="divForm" class="section">
                    <table id="tblForm" class="form double">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <select id="selSupplier" placeholder="-- search --"></select>
                                </td>
                                <td>Reference:</td>
                                <td>
                                    <input type="text" id="txtRef" />
                                </td>
                            </tr>
                            <tr>
                                <td>Details:</td>
                                <td>
                                    <input type="text" id="txtDetail1" />
                                </td>
                                <td>Payment Number:</td>
                                <td>
                                    <input type="text" id="txtOpNo" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtDetail2" />
                                </td>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate" style="width: 195px;" />
                                    <img src="../images/icon_calendar.png" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtDetail3" />
                                </td>
                                <td>Status:</td>
                                <td>
                                    <input type="text" id="txtStatus" />
                                </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <input type="text" id="txtPhone" />
                                </td>
                                <td>Payment Method:</td>
                                <td>
                                    <select id="selPaymentType">
                                    </select>
                                </td>
<%--                                <td>WHT:</td>
                                <td style="vertical-align: middle;">
                                    <input type="checkbox" id="chkWht" />
                                    <input type="text" id="txtWht" style="width: 50px;" />
                                    %
                                    <select id="selWht" style="width: 144px;">
                                        <option value="1">5. Wage</option>
                                    </select>
                                </td>--%>
                            </tr>
                            <tr>
                                <td>Tax ID:</td>
                                <td>
                                    <textarea id="txtTaxIdInfo" style="height: 60px; resize: none;"></textarea>
                                </td>
                                <td></td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- Form Pane -->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div id="subGridPane" class="section">
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="onSubEdit();" />
                            <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="onSubDelete();" />
                            <input type="button" id="btnSubUndoDelete" class="btn-action" value="Undo" onclick="onSubUndoDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="subGrid" style="height: 180px;">
                    </div>
                    <div class="amount-summary">
                        <table id="tblSummary" class="form summary">
                            <tr>
                                <td class="caption">Note:</td>
                                <td class="control">
                                    <input type="text" id="txtNote" />
                                </td>
                                <td class="caption-amount">Sub total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtSubtotal" />
                                </td>
                            </tr>
                            <tr>
                                <td id="tdVoid" class="pane-void" colspan="2" rowspan="2"></td>
                                <td class="caption-amount">Tax:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTax" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption-amount">Total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTotal" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="header-search">Search</div>
                    <div class="grid-command search">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                            <input type="button" id="btnPay" class="btn-action text-teal" value="Pay" onclick="onPay();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>

            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>
