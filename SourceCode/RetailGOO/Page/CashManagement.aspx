﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="CashManagement.aspx.cs" Inherits="RetailGOO.Page.CashManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
    </style>
    <script type="text/javascript">
        // Variables declaration
        function pageLoad() {

            preventNavigationOnUnsaved();

            // Initialize behaviors
            $("#grid1").kilinGrid({
                onRowSelected: onRowSelected
            });
            $("#grid2").kilinGrid({
                onRowSelected: onRowSelected
            });

            // Ajax calls to initialize mandatory variables
        }

        function onItemSelectGrid1() {
        }

        function btnChooseFileOnClick() {
        }

        function Save() {
        }
        function Void() {
        }
        function Clear() {
        }
        function Print() {
        }
        function Cancel() {
        }
        function GoBack() {
        }
        function GoNext() {
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-blue">
        <ul id="tab0" class="nav nav-tabs">
            <li class="active"><a href="#" data-toggle="tab">Cash Management</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section"><!--form-->
                    <table class="form single">
                        <tr>
                            <td>Source:</td>
                            <td>
                                <select id="selSrc">
                                    <option value="0">Terminal 1 / 100.00</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Destination:</td>
                            <td>
                                <select id="selDest">
                                    <option value="0">Bank 01</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Amount:</td>
                            <td>
                                <input type="text" id="txtAmount" />
                            </td>
                        </tr>
			        </table>
                </div><!--form/end-->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>

            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
</asp:Content>
