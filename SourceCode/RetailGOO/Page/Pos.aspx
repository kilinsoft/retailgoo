﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/AuthNoMenu.Master" AutoEventWireup="true" CodeBehind="Pos.aspx.cs" Inherits="RetailGOO.Page.Pos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <link rel="stylesheet" href="<%# ResolveClientUrl("~/css/pos.css") %>" type="text/css" />
    <script type="text/javascript">

        // Constants
        var MENU1_SLOT = 8;
        var MENU2_SLOT = 6;
        var MENU3_SLOT = 49;
        var TAB_SLOT = 3;

        var MODE_CATALOG = 0;
        var MODE_HOLDSALE = 1;
        var MODE_CASHIER = 2;

        var PAYMENT_ALL = 0;
        var PAYMENT_CASH = 1;
        var PAYMENT_CREDIT = 2;
        var PAYMENT_CHEQUE = 3;

        var g_useProductSearch = false;

        // Control variables
        var g_subtotal, g_tax, g_total, g_change;
        var g_totalDiscountOnItem, g_discountOnTopAmount, g_totalDiscount;
        var g_lblSubtotal, g_lblTax, g_lblTotal, g_lblDiscount, g_lblChange;
        var g_amtDiscount, g_amtPayment, g_amtTendered, g_amtChange;
        var g_cashierCustomer;

        var g_mode = MODE_CATALOG;

        var g_selHoldSaleCustomer;
        
        var g_categoryId = -1;
        var g_brandId = -1;
        var g_menu3TabIndex = 0;

        var g_menu1Page = g_menu2Page = 0;

        var g_bill = null;
        var g_paymentItems = [];
        var g_useDiscountPercent = false;

        var g_paymentMethod = PAYMENT_CASH;

        //var g_lastInput = { 
        //    isAmountInput: false,
        //    isCellInput: false,
        //    rowIndex: 0,
        //    colIndex: 0,
        //    $element: null
        //};
        var InputType = { textBox: "txtbox", amountBox: "amtbox", cell: "gridCell" };
        var g_lastInput = { 
            type: "txtbox", // "txtbox": normal text-box, "amtbox": amount input box, "cell": Editable grid cell
            $element: null,
            object: null
        };

        // Data variables
        var g_username = "<%=Username%>";

        var g_brandList = <%=BrandListString%>;
        var g_categoryList = <%=CategoryListString%>;
        var g_originalItemGroups = <%=SalesItemsString%>;
        var g_itemGroups = cloneArray(g_originalItemGroups);
        var g_taxFactor = <%=TaxFactor%>;
        var g_customerList = <%=CustomerListString%>;

        var defaultSalesItem = {
            ItemId : -1,
            ItemName : '',
            ItemFullName : '',
            ItemGroupName : '',
            ItemGroupCode : '',
            Quantity : 0.0,
            Discount : 0.0,
            Price : 0.0
            //PricePerItem : 0.0,
        }

        function pageLoad() {
            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Item Name", name: "ItemFullName", required: false, editable: false },
            { title: "Qty", name: "Quantity", width: 100, required: true, editable: true, formatter: KGrid.Formatter.Amount(2), 
                editor: KGrid.Editor.Amount(2), onChanged: onCartGridCellValueChanged, onFocused: onCartGridInputCellFocused },
            { title: "Discount", name: "Discount", width: 100, required: true, editable: true, formatter: KGrid.Formatter.Amount(2), 
                editor: KGrid.Editor.Amount(2), onChanged: onCartGridCellValueChanged, onFocused: onCartGridInputCellFocused },
            { title: "Price", name: "Amount", width: 100, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: true,
                enableAddBlankRow: false,
                ignoreAutoEditorClose: true,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            var holdSaleGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Customer", name: "CustomerName", required: false, editable: false },
            { title: "Last Modified", name: "Version", width: 150, required: false, editable: false, align: 'center' },
            { title: "Items", name: "Quantity", width: 100, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) },
            { title: "Total", name: "Total", width: 100, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var holdSaleGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: holdSaleGridOnRowSelected,
                onRowDeselected: holdSaleGridOnRowDeselected,
                onRowDblClick: holSaleGridOnRowDblClick
            }
            g_holdSaleGrid = new KGrid.Grid("#holdSaleGrid", holdSaleGridDef, holdSaleGridColsDef, []);

            g_lblTotal = $('#lbl-amount-big');
            g_lblDiscount = $('#lbl-discount');
            g_lblSubtotal = $('#lbl-subtotal');
            g_lblChange = $('#lbl-change');
            g_lblTax = $('#lbl-tax');

            g_amtDiscount = new Util.Input.Amount("#txtDiscount", 2, updateBalance, onAmountInputFocused);
            g_amtPayment = new Util.Input.Amount("#txtAmt1", 2, null, onAmountInputFocused);
            g_amtTendered = new Util.Input.Amount("#txtAmt2", 2, null, onAmountInputFocused);
            g_amtChange = new Util.Input.Amount("#txtAmt3", 2, null, onAmountInputFocused);

            g_selHoldSaleCustomer = $("#selHoldSaleCustomer").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_customerList
            })[0].selectize;

            g_cashierCustomer = $("#selCashierCustomer").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_customerList
            })[0].selectize;

            $("#lblUsernameTop").html(g_username);
            $("#lblUsername").html(g_username.replace(" ", "<br>"));

            // Init elements' behaviors
            setInterval(updateDateTime, 1000);


            //var $dragging = null;
            //$(document.body).on("mousemove", function(e) {
            //    if ($dragging) {
            //        $dragging.offset({
            //            left: e.pageX
            //        });

            //    }
            //});
            //$(document.body).on("mousedown", "div.switch-bar", function (e) {
            //    $dragging = $(e.target);
            //});
            //$(document.body).on("mouseup", function (e) {
            //    $dragging = null;
            //});


            $('*').on('focus',
                function() {
                    if ($(this).is('.input-text')) {
                        g_lastInput.$element = $(this);
                        g_lastInput.type = InputType.textBox;
                    }
                    else
                        g_lastInput.$element = null;
                });
            $('.keypad > div').on('focus', 
                function(){
                    return false;
                }
            );

            $("#txtProductSearch").keypress(function(e) {
                if (e.which == 13) {
                    onBtnProductSearchClick();
                }
            });

            $('#divCustomerNameDlg').on('shown.bs.modal', function() { // Run when the modal is shown completely
                $("#txtCustomerName").focus().select(); 
            })
            $('#divCustomerNameDlg').on("keypress", function(e) {
                if (e.keyCode == 13) {
                    $("#btnCustomerNameOk").click();
                }
            });

            // Make the Payment Amount textbox automatically add payment amount when losing focus
            $('#txtAmt1').on('blur',
                function() {
                    if (g_amtPayment.getValue() == 0) // Do not submit if the amount is 0, to clear the paid amount, use "Cancel" button instead
                        return;
                    submitPaymentAmount(g_paymentMethod, g_amtPayment.getValue()); // txtAmt1 is the same object as g_amtPayment
                    g_amtPayment.setValue(0);
                });

            // Init element's values

            // Init state
            g_amtTendered.lock();
            g_amtChange.lock();

            // Behaviors
            updateDateTime();
            clearPos();
            updateMenu1(g_brandList);
            updateMenu2(g_categoryList);
            updateMenu3(g_itemGroups, g_itemGroups.length, g_menu3TabIndex);

            $('#divHoldSales').hide();
            $('#divCashier').hide();

            // Test
            //$('#divCatalog').hide();

        }
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function holdSaleGridOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);

            retrieveHoldSale(pRow.data["BillId"], null);
        }
        function holdSaleGridOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function holSaleGridOnRowDblClick(pRow) {
            retrieveHoldSale(pRow.data["BillId"], onBtnHoldSaleApply);
        }
        function onBtnCustomerNameOk() {
            $('#divCustomerNameDlg').modal('hide');
            saveToHoldSale();
        }
        function onCartGridCellValueChanged(pCell, pVal) {
            recalculateRowAmount(pCell, pVal);
            updateBalance();
        }
        function onCartGridInputCellFocused(pCell) {
            g_lastInput.type = InputType.cell;
            g_lastInput.object = pCell;
        }
        
        ///////////////////////////////////////////////////////////
        //
        // Data retrieval functions
        //
        
        // Disable this ajax loading func because we already pre-load all items from CodeBehind.
        //function retrieveCatalog(filterObj) {
        //    if (filterObj == undefined)
        //        filterObj = null;
        //    retrieveAjaxDataSync("ItemManagement.svc", "RetrieveAllSalesItem", filterObj,
        //        function (rsp) {
        //            var items = rsp.ResponseItem;
        //            g_menu3TabIndex = 0;
        //            updateMenu3(items, items.length, g_menu3TabIndex);
        //        });
        //}
        function retrieveAllPendingBill(callback) {
            retrieveAjaxDataSync("Sales.svc", "retrieveAllPendingBill", null,
                function (items) {
                    if (items.length > 0)
                        setGridData(g_holdSaleGrid, items);
                    else
                        setGridData(g_holdSaleGrid, []);
                });
        }
        function retrieveHoldSale(id, pNextFunc) {
            retrieveAjaxDataSync("Sales.svc", "retrieveBill", id,
                function (item) {
                    g_bill = item;
                    displayCurrentBill(g_bill);
                    if (pNextFunc != null && pNextFunc != undefined)
                        pNextFunc();
                });
        }

        ///////////////////////////////////////////////////////////
        //
        // Balance Board
        //
        function clearHoldSale() {
            setGridData(g_mainGrid, []);
            g_bill = null;
            g_paymentItems = [];
        }
        function getPayment(typeId) { // 0 = all, 1 = cash, 2 = credit, 3 = cheque
            var cash, credit, cheque;
            cash = credit = cheque = 0;

            if (g_paymentItems != null) {
                for (var i = 0; i < g_paymentItems.length; ++i) {
                    var item = g_paymentItems[i];
                    if (item["ItemState"] == KGrid.ItemState.deletedItem)
                        continue;
                    var amt = parseFloat(item["Amount"]);
                    var paymentTypeId = item["PaymentTypeId"];
                    if ( paymentTypeId == PAYMENT_CASH) {
                        cash += amt;
                    } 
                    else if (paymentTypeId == PAYMENT_CREDIT) {
                        credit += amt;
                    }
                    else if (paymentTypeId == PAYMENT_CHEQUE) {
                        cheque += amt;
                    }
                }
            }
            if ( typeId == PAYMENT_CASH) {
                return cash;
            } 
            else if (typeId == PAYMENT_CREDIT) {
                return credit;
            }
            else if (typeId == PAYMENT_CHEQUE) {
                return cheque;
            }
            return cash + credit + cheque; // case 0
        }
        
        function recalculateRowAmount(pCell, pVal) {
            var row = pCell.parentRow;
            var qtyCell = row.getCellByColumnName("Quantity");
            var price = row.data["Price"];
            var amtCell = row.getCellByColumnName("Amount");
            var discountCell = row.getCellByColumnName("Discount");
            var qty = qtyCell.value;
            //if (qty === "")
            //    qty = 0;
            //if (price === "")
            //    price = 0;
            //qtyCell.setValue(qty);
            //priceCell.setValue(price);
            amtCell.setValue( (parseFloat(qty) * parseFloat(price)) - parseFloat(discountCell.value) );
        }

        function updateBalance() {
            // Calculate g_subtotal and g_totalDiscountOnItem from MainGrid
            var sumDiscount = 0;
            var sumAmount = 0;
            for (var i = 0; i < g_mainGrid.data.length; ++i) {
                var record = g_mainGrid.data[i];
                sumDiscount += parseFloat(record["Discount"]);
                sumAmount += (parseFloat(record["Quantity"]) * parseFloat(record["Price"]));
            }
            g_subtotal = sumAmount;

            g_totalDiscountOnItem = sumDiscount;
            if (g_useDiscountPercent) {
                var tmpTotal = g_subtotal + g_totalDiscountOnItem;
                g_discountOnTopAmount = g_amtDiscount.getValue() * tmpTotal / 100;
            } else {
                g_discountOnTopAmount = g_amtDiscount.getValue();
            }
            g_totalDiscount = g_totalDiscountOnItem + g_discountOnTopAmount;

            g_tax = (g_subtotal - (g_totalDiscountOnItem + g_discountOnTopAmount)) * g_taxFactor;
            if (g_tax < 0)
                g_tax = 0;
            g_total = g_subtotal + g_tax - g_totalDiscount;
            // Total must not be below 0
            if (g_total < 0)
                g_total = 0;

            var sumPayment = getPayment(PAYMENT_CASH) + getPayment(PAYMENT_CREDIT) + getPayment(PAYMENT_CHEQUE); // Cash + Credit + Cheque
            g_change = sumPayment - g_total;
            if (g_change < 0)
                g_change = 0;

            g_lblTotal.html(convertToAmount(g_total, 2));
            g_lblSubtotal.html(convertToAmount(g_subtotal, 2));
            g_lblChange.html(convertToAmount(g_change, 2));
            g_lblDiscount.html(convertToAmount(g_totalDiscount, 2));
            g_lblTax.html(convertToAmount(g_tax, 2));

            g_amtTendered.setValue(sumPayment);
            g_amtChange.setValue(g_change);

            updateDiscountOnTopButtons();

            if (getPayment(PAYMENT_ALL) >= g_total)
                enablePaidBillSubmission(true);
            else
                enablePaidBillSubmission(false);
        }


        ///////////////////////////////////////////////////////////
        //
        // Catalog
        //
        function updateMenu1(items) {
            // Clear all handler and data from all Menu1 items
            for (var i  =0; i < MENU1_SLOT; ++i)
                $("#divMenu1-"+i).text("").unbind("click.Menu1Filter");
            for (var i = 0, j = g_menu1Page * MENU1_SLOT; j < ((g_menu1Page * MENU1_SLOT) + MENU1_SLOT) && j < items.length; ++i, ++j) {
                $("#divMenu1-" + i).text(items[j]["Text"]).data("item", items[j]).on("click.Menu1Filter", function() {
                    onFilterClick("ItemBrandId", $(this).data("item"), $(this).attr('id'));
                });
            }
        }
        function updateMenu2(items) {
            for (var i  =0; i < MENU2_SLOT; ++i)
                $("#divMenu2-"+i).text("").unbind("click.Menu2Filter");
            for (var i = 0, j = g_menu1Page * MENU2_SLOT; j < ((g_menu1Page * MENU2_SLOT) + MENU2_SLOT) && j < items.length; ++i, ++j) {
                $("#divMenu2-" + i).text(items[j]["Text"]).data("item", items[j]).on("click.Menu2Filter", function() {
                    onFilterClick("ItemCategoryId", $(this).data("item"), $(this).attr('id'));
                });
            }
        }
        function updateMenu3(items, nVirtualItems, iPage) {
            // Update Menu3's tabs
            $("#menu3Tab").empty();
            var nItems = items.length;
            if (nVirtualItems != null && nVirtualItems != undefined)
                nItems = nVirtualItems;
            if (iPage == null || iPage == undefined)
                iPage = 0;
            var nTabs = Math.ceil(nItems / MENU3_SLOT);
            if (iPage >= TAB_SLOT) {
                var tabPrevIndex = (Math.floor(iPage / TAB_SLOT) * TAB_SLOT) - 1;
                $tabPrev = $("<li><a>&lt;&lt;</a></li>").addClass("nav-page").on('click',function(){
                    onMenu3TabClick(tabPrevIndex);
                });
                $("#menu3Tab").append($tabPrev);
            }
            var iTabStart = TAB_SLOT * Math.floor(iPage / TAB_SLOT);
            for (var i = 0; i < TAB_SLOT && (iTabStart + i < nTabs); ++i) {
                $a = $("<a></a>").html("Page " + (iTabStart + i + 1));
                $tab = $("<li></li>").append($a);
                if (iTabStart + i == iPage) 
                    $tab.addClass("active");
                else {
                    var tabIndex = iTabStart + i;
                    $tab.on('click', function() {
                        onMenu3TabClick(tabIndex);
                    });
                }
                $("#menu3Tab").append($tab);
            }
            if (iPage < nTabs - 1) {
                var tabNextIndex = iTabStart + TAB_SLOT;
                $tabNext = $("<li><a>&gt;&gt;</a></li>").addClass("nav-page").on('click',function(){
                    onMenu3TabClick(tabNextIndex);
                });
                $("#menu3Tab").append($tabNext);
            }

            // Update Menu3's items
            var $menu3 = $("#divMenu3");
            $menu3.html("");
            if (items.length > 0) {
                for (var i = 0; i < items.length; ++i) {
                    var itemGroup = items[i];
                    var item = itemGroup[0]; // Display only the first item first. We will handle ItemGroup with subItems below
                    var $divItem = $("<div></div>");
                    $divItem.addClass("menu3-item");
                    $divItem.html(escapeHtml(item["ItemGroupName"]));
                    if (itemGroup.length > 1) { // This is actually a group of Items that should be represented by one ItemGroup
                        $divItem.data('item', itemGroup);
                        $divItem.on('click',
                            function () {
                                onDisplaySubItems($(this).data('item'));
                            });
                    }
                    else { // This is an ItemGroup with only one Item. Just handle it as a single item.
                        $divItem.data('item', item);
                        $divItem.on('click',
                            function () {
                                onMenu3Click($(this).data('item'));
                            });
                    }
                    $menu3.append($divItem);
                }
            }
            else {
                $div = $("<div>No Item</div>");
                $div.css({"text-align": "center", "vertical-align" : "middle", "height" : "200px", "line-height" : "200px"});
                $menu3.append($div);
            }
        }
        function getItemFullName(item) {
            if (item["ItemName"].toLowerCase() == item["ItemGroupName"].toLowerCase())
                return item["ItemGroupName"];
            else
                return item["ItemGroupName"] + " " + item["ItemName"];
        }
        function onBtnProductSearchClick() {
            var txt = $("#txtProductSearch").val().trim();
            if (txt != "") {
                g_useProductSearch = true;
                g_itemGroups = cloneArray(g_originalItemGroups);
                $(".menu1-item").removeClass("active");
                $(".menu2-item").removeClass("active");
                var filteredItems = [];
                for (var i = 0; i < g_itemGroups.length; ++i) {
                    var item = g_itemGroups[i];
                    if (item[0]["ItemGroupName"].toLowerCase().indexOf(txt.toLowerCase()) >= 0)
                        filteredItems.push(item);
                }
                g_itemGroups = filteredItems;
            } else {
                g_useProductSearch = false;
                g_itemGroups = g_originalItemGroups;
            }
            $("#txtProductSearch").val("");
            updateMenu3(g_itemGroups, g_itemGroups.length, g_menu3TabIndex);
        }
        function onBtnMenu1Click(direction) {
            nPage = Math.ceil(g_brandList.length / MENU1_SLOT);
            if (direction == 'left') {
                if (g_menu1Page == 0)
                    return;
                --g_menu1Page;
            }else { // right
                if (g_menu1Page == nPage-1)
                    return;
                ++g_menu1Page
            }
            updateMenu1(g_brandList);
        }
        
        function onBtnMenu2Click(direction) {
            nPage = Math.ceil(g_categoryList.length / MENU2_SLOT);
            if (direction == 'up') {
                if (g_menu2Page == 0)
                    return;
                --g_menu2Page;
            }else { // down
                if (g_menu2Page == nPage-1)
                    return;
                ++g_menu2Page
            }
            updateMenu2(g_brandList);
        }
        function onMenu3Click(item) {
            // 0. Close the dialog in case it is opened.
            $('#divDialog').modal('hide');

            // 1. Check if the salesOrder already contains this item
            var existingItem = null;
            var salesOrder = (g_mainGrid.data == null ? [] : g_mainGrid.data);
            for (var i = 0; i < salesOrder.length; ++i) {
                if (salesOrder[i]["ItemId"] == item["ItemId"]) {
                    existingItem = salesOrder[i];
                    break;
                }
            }

            // 2. If the item already exists, add 1 to its order quantity.
            //    If the item does not exist, add it to salesOrder.
            if (existingItem != null) {
                var qty = parseFloat(existingItem["Quantity"]);
                qty += 1;
                existingItem["Quantity"] = qty;
                existingItem["Discount"] = 0;
                existingItem["Amount"] = qty * parseFloat(existingItem["Price"]);
                //existingItem["Price"] += existingItem["PricePerItem"];
            } else {
                var newItem = cloneObject(defaultSalesItem);
                newItem["ItemId"] = item["ItemId"];
                newItem["ItemName"] = item["ItemName"];
                newItem["ItemFullName"] = getItemFullName(item);
                newItem["ItemGroupName"] = item["ItemGroupName"];
                newItem["ItemGroupCode"] = item["ItemGroupCode"];
                newItem["Quantity"] = 1;
                //newItem["PricePerItem"] = item["Price"];
                newItem["Price"] = item["Price"];
                newItem["Amount"] = item["Price"]; // Default amount = 1 x ItemPricePerUnit
                newItem["itemState"] = KGrid.ItemState.newItem;
                salesOrder.push(newItem); // add selectedItem to the salesOrder
            }
            setGridData(g_mainGrid, salesOrder);
            updateBalance();
        }
        function onFilterClick(filterParam, filterItem, buttonId) { // Category menu
            var filterValue = filterItem.Value;

            // Toggle status of the pressed filter button
            if ($("#" + buttonId).hasClass("active")) {
                if (filterParam == "ItemBrandId") {
                    $(".menu1-item").removeClass("active");
                    g_brandId = -1;
                } else {
                    $(".menu2-item").removeClass("active");
                    g_categoryId = -1;
                }
            } else {
                if (filterParam == "ItemBrandId") {
                    $(".menu1-item").removeClass("active");
                    g_brandId = filterValue;
                } else {
                    $(".menu2-item").removeClass("active");
                    g_categoryId = filterValue;
                }
                $("#" + buttonId).addClass("active");
            }

            displayCatalog(g_menu3TabIndex);
        }
        function displayCatalog(tabIndex) {
            var arrFilteredItem = [];

            for (var i = 0; i < g_itemGroups.length; ++i) {
                var item = g_itemGroups[i][0]; // Can check from the first item in the list
                var matchBrand = (item["ItemBrandId"] == g_brandId);
                var matchCategory = (item["ItemCategoryId"] == g_categoryId);
                if (g_brandId != -1) {
                    if (g_categoryId != -1) {
                        if (matchBrand && matchCategory)
                            arrFilteredItem.push(g_itemGroups[i]);
                    } else {
                        if (matchBrand)
                            arrFilteredItem.push(g_itemGroups[i]);
                    }
                }
                else if (g_categoryId != -1) {
                    if (matchCategory)
                        arrFilteredItem.push(g_itemGroups[i]);
                }
                else {
                    arrFilteredItem.push(g_itemGroups[i]);
                }
            }
            updateMenu3(arrFilteredItem, arrFilteredItem.length, tabIndex); // Refresh the catalog pane
        }
        function onMenu3TabClick(tabIndex) {
            displayCatalog(tabIndex);
        }
        function onDeleteOrder() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            var itemToDelete = g_mainGrid.selectedRow.data;
            if (itemToDelete == null)
                return;
            for (var i = 0; i < g_mainGrid.data.length; ++i) {
                if (itemToDelete["ItemId"] == g_mainGrid.data[i]["ItemId"]) {
                    g_mainGrid.data.splice(i, 1);
                    break;
                }
            }
            setGridData(g_mainGrid, g_mainGrid.data);
            updateBalance();
        }
        function processCurrentBill() {
            if (g_mode == MODE_HOLDSALE)
                return;
            // If current bill have not been add to the HoldSale list, then pop-up a textbox to ask
            // for the name of this customer. After that, add this bill to the HoldSale list
            if (g_mainGrid.data.length <= 0) {
                switchToHoldSale();
            } else if (g_bill == null || g_bill["BillId"] == -1) { // This bill has not been added to the HoldSale list
                $('#txtCustomerName').val("Customer"); // Default customer name
                $('#divCustomerNameDlg').modal('show');
            } else {
                saveToHoldSale();
            }
        }

        ///////////////////////////////////////////////////////////
        //
        // HoldSale
        //
        function displayCurrentBill(item) {
            // Set Amount value to each BillItem
            var billItems = item.BillItems;
            if (billItems != null && billItems.length > 0) {
                for (var i = 0; i < billItems.length; ++i) {
                    billItems[i]["Amount"] = (parseFloat(billItems[i]["Quantity"]) * parseFloat(billItems[i]["Price"])) - parseFloat(billItems[i]["Discount"]);
                }
                item.BillItems = billItems;
            }
            setGridData(g_mainGrid, item.BillItems);

            g_bill = item;
            g_paymentItems = cloneArray(g_bill["BillPaymentItems"]);
            g_useDiscountPercent = g_bill["IsDiscountPecent"];
            g_amtDiscount.setValue(g_useDiscountPercent ? (parseFloat(g_bill["DiscountPercent"]) * 100) : g_bill["DiscountAmount"]);
            $("#txtNote").val(item["Note"]);

            updateBalance();
        }
        function onDisplaySubItems(itemList) {
            $divDialogBody = $('#divDialogBody');
            $divDialogBody.html("");
            var itemGroupCode = itemList[0]["ItemGroupCode"];
            var itemGroupName = itemList[0]["ItemGroupName"];
            $('#lblDialogTitle').html(escapeHtml(itemGroupCode + " : " + itemGroupName));
            
            for (var i = 0; i < itemList.length; ++i) {
                var item = itemList[i];
                $divItem = $('<div class="menu3-item"></div>');
                $divItem.append(escapeHtml(item["ItemName"]));
                $divItem.data('item', item);
                $divItem.on('click',
                    function() {
                        onMenu3Click($(this).data('item'));
                    });
                $divDialogBody.append($divItem);
            }

            $('#divDialog').modal('show');
        } 
        function onBtnHoldSaleApply() {
            if (g_holdSaleGrid.selectedRow != null)
                g_holdSaleGrid.selectedRow.deselect();
            switchToCatalog();
        }
        function onBtnHoldSaleCancel() {
            clearHoldSale();
            clearCashier();
            switchToCatalog();
        }
        function discardSelectedHoldSale() {
            if (g_holdSaleGrid.selectedRow != null) {
                var itemToDiscard = g_bill;
                itemToDiscard["ItemState"] = KGrid.ItemState.deletedItem;
                billItems = itemToDiscard["BillItems"];
                billPaymentItems = itemToDiscard["BillPaymentItems"];
                if (billItems != null) {
                    for (var i = 0; i < billItems.length; ++i)
                        billItems[i]["ItemState"] = KGrid.ItemState.deletedItem;
                }
                if (billPaymentItems != null) {
                    for (var i = 0; i < billPaymentItems.length; ++i)
                        billPaymentItems[i]["ItemState"] = KGrid.ItemState.deletedItem;
                }

                sendAjaxUpdate( "Sales.svc", "ManageBill", itemToDiscard,
                    function(rsp) {
                        retrieveAllPendingBill();
                        clearHoldSale();
                        //switchToHoldSale();
                    },
                    function(rsp) {
                        displayError(rsp.StatusText);
                    });
            }
        }
        
        ///////////////////////////////////////////////////////////
        //
        // Cashier
        //
        function onAmountInputFocused(obj) {
            g_lastInput.type = InputType.amountBox;
            g_lastInput.object = obj;
        }
        function updateDiscountOnTopButtons() {
            $("#btnDiscountAmount").removeClass("btn-cashier-inactive");
            $("#btnDiscountPercent").removeClass("btn-cashier-inactive");
            if (g_useDiscountPercent) {
                $("#btnDiscountAmount").addClass("btn-cashier-inactive");
            } else {
                $("#btnDiscountPercent").addClass("btn-cashier-inactive");
            }
        }
        function clearPaymentItems() {
            // Clear payment items
            var tmpPaymentItems = []
            for (var i = 0; i < g_paymentItems.length; ++i) {
                var item = g_paymentItems[i];
                if (item["ItemState"] == KGrid.ItemState.originalItem) {
                    item["ItemState"] = KGrid.ItemState.deletedItem; // to delete previous payment and flag the items to be disabled in the database
                    tmpPaymentItems.push(item);
                } else if (item["ItemState"] == KGrid.ItemState.deletedItem) {
                    tmpPaymentItems.push(item);
                } else {
                    ; // 
                }
            }
            g_paymentItems = tmpPaymentItems;
        }
        function clearCashier() {
            g_useDiscountPercent = false;
            g_amtDiscount.setValue(0);
            g_amtChange.setValue(0);
            g_amtTendered.setValue(0);
            enablePaidBillSubmission(false);
            updateDiscountOnTopButtons();

            g_paymentItems = [];
        }
        //function displayCustomer() {
        //    $("#txtCustomer").val(g_bill["CustomerId"]);
        //}
        function triggerKey(c) {
            //if (g_lastInput.isCellInput) {
            if (g_lastInput.type == InputType.cell || g_lastInput.type == InputType.amountBox) {
                //var lastInput = g_mainGrid.lastEditableCell;
                var lastInput = g_lastInput.object;
                var cellVal = null;
                var cellValStr = "";
                var valParts = [];
                var useDecimalSep = false;
                if (lastInput != null) {
                    //lastInput.openEditor();
                    if (g_lastInput.type == InputType.cell)
                        cellVal = lastInput.value;
                    else
                        cellVal = lastInput.data // AmountBox holds value in the member named "data"
                    cellValStr = String(cellVal);
                    valParts = cellValStr.split(".");
                    useDecimalSep = (lastInput.useDecimalSep == true);
                    
                    //lastInput.editor.focus();
                    //var $lastInput = lastInput.editor.$input;
                } 
                else
                    return;

                lastInput.useDecimalSep = false;
                var displayVal = cellValStr;

                if (c == "backspace") { // Backspace
                    if (useDecimalSep == true)
                        displayVal = cellValStr;
                    else if (cellValStr.length == 1) {
                        lastInput.setValue(0);
                        displayVal = "";
                        //} else if (valParts.length == 2 && valParts[1].length == 1) { // delete the fractional part and "."
                        //    lastInput.setValue(parseInt(valParts[0]));
                        //    displayVal = valParts[0];
                    } else {
                        var newValStr = cellValStr.substring(0, cellValStr.length - 1); // delete the last fractional part or last digit
                        lastInput.setValue(parseFloat(newValStr));

                        if (cellValStr.indexOf(".") > -1 && valParts[1].length == 1) // if the next trigger is digit, add "." first
                            lastInput.useDecimalSep = true;
                        displayVal = newValStr;
                    }
                } else if (c == "clear") {
                    lastInput.setValue(0);
                    displayVal = "";
                } else if (c == "%") {
                    var selectedRow = g_mainGrid.selectedRow;
                    if (selectedRow != null) {
                        var cell = g_lastInput.object;
                        var percentDiscountAmt = cell.value;
                        if (percentDiscountAmt <= 100 && percentDiscountAmt >= 0) {
                            var price = selectedRow.data["Price"];
                            var qtyCell = selectedRow.getCellByColumnName("Quantity");
                            var discountAmt = parseFloat(qtyCell.value) * parseFloat(percentDiscountAmt) * parseFloat(price) / 100;
                            //cell.setValue(discountAmt);
                            displayVal = discountAmt;
                        }
                    }
                } else if (c == ".") {
                    if (cellValStr.indexOf(".") == -1) {
                        lastInput.useDecimalSep = true;
                        displayVal = cellValStr + ".";
                    }
                } else {
                    displayVal = "";
                    if (cellValStr.indexOf(".") > -1) {
                        if (valParts.length > 1 && valParts[1].length >= 2) // Prohibited input for more than 2 fractional digits
                            displayVal = cellValStr;
                        else
                            displayVal = cellValStr + String(c);
                    } else {
                        if (useDecimalSep) {
                            displayVal = cellValStr + ("." + String(c));
                        } else {
                            if (cellValStr == "0")
                                displayVal = String(c);
                            else
                                displayVal = cellValStr + String(c);
                        }
                    }
                    lastInput.setValue(displayVal);
                }

                // update cell textbox
                if (g_lastInput.type == InputType.cell) {
                    lastInput.openEditor();
                    $input = lastInput.editor.$input;
                }
                else {
                    lastInput.$element.focus();
                    lastInput.$element.select();
                    $input = lastInput.$element;
                }
                if (displayVal == "0")
                    $input.val("");
                else
                    $input.val(displayVal);

                // Trigger onChanged if lastInput is CELL
                if (g_lastInput.type == InputType.cell) {
                    onCartGridCellValueChanged(g_lastInput.object, displayVal);
                }
            } else { // directly modify the normal text input
                $lastInput = g_lastInput.$element;
                if ($lastInput == null || $lastInput == undefined)
                    return;

                $lastInput.focus();

                var val = $lastInput.val();
                var valStr = String($lastInput.val());

                if (c == "backspace") { // Backspace
                    if (val != "")
                        $lastInput.val(valStr.substring(0, valStr.length - 1));
                } else if (c == "clear") {
                    $lastInput.val("");
                } else if (c == "%") {
                    return; // Do not support yet
                } else if (c == ".") {
                    if (valStr.indexOf(".") == -1)
                        $lastInput.val(valStr + ".");
                } else {
                    $lastInput.val(valStr + String(c));
                }
            }

        }
        function triggerKey2(c) {
            
            //if (g_lastInput.isCellInput) {
            if (g_lastInput.type == InputType.cell || g_lastInput.type == InputType.amountBox) {
                //var lastInput = g_mainGrid.lastEditableCell;
                var lastInput = g_lastInput.object;
                var cellVal = null;
                var cellValStr = "";
                var valParts = [];
                var useDecimalSep = false;
                if (lastInput != null) {
                    //lastInput.openEditor();
                    if (g_lastInput.type == InputType.cell) {
                        //cellVal = lastInput.value;
                        // Hack the way we commit the value to support the behavior that the user change the value in the cell field and hit ENTER. 
                        // If setValue() is not called, the active value will return to original because the isChanged state of the cell is not TRUE.
                        cellVal = lastInput.editor.$element.find("input[type=text]").val();
                        lastInput.setValue(cellVal);
                    }
                    else
                        cellVal = lastInput.data // AmountBox holds value in the member named "data"
                    cellValStr = String(cellVal);
                    valParts = cellValStr.split(".");
                    useDecimalSep = (lastInput.useDecimalSep == true);
                    
                    //lastInput.editor.focus();
                    //var $lastInput = lastInput.editor.$input;
                } 
                else
                    return;

                lastInput.useDecimalSep = false;
                var displayVal = cellValStr;

                if (c == "enter") {
                    ;
                } else if (c == "backspace") { // Backspace
                    if (useDecimalSep == true)
                        displayVal = cellValStr;
                    else if (cellValStr.length == 1) {
                        lastInput.setValue(0);
                        displayVal = "";
                        //} else if (valParts.length == 2 && valParts[1].length == 1) { // delete the fractional part and "."
                        //    lastInput.setValue(parseInt(valParts[0]));
                        //    displayVal = valParts[0];
                    } else {
                        var newValStr = cellValStr.substring(0, cellValStr.length - 1); // delete the last fractional part or last digit
                        lastInput.setValue(parseFloat(newValStr));

                        if (cellValStr.indexOf(".") > -1 && valParts[1].length == 1) // if the next trigger is digit, add "." first
                            lastInput.useDecimalSep = true;
                        displayVal = newValStr;
                    }
                } else if (c == "clear") {
                    lastInput.setValue(0);
                    displayVal = "";
                } else if (c == "%") {
                    var selectedRow = g_mainGrid.selectedRow;
                    if (selectedRow != null) {
                        var cell = g_lastInput.object;
                        var percentDiscountAmt = cell.value;
                        if (percentDiscountAmt <= 100 && percentDiscountAmt >= 0) {
                            var price = selectedRow.data["Price"];
                            var qtyCell = selectedRow.getCellByColumnName("Quantity");
                            var discountAmt = parseFloat(qtyCell.value) * parseFloat(percentDiscountAmt) * parseFloat(price) / 100;
                            //cell.setValue(discountAmt);
                            displayVal = discountAmt;
                        }
                    }
                } else if (c == ".") {
                    if (cellValStr.indexOf(".") == -1) {
                        lastInput.useDecimalSep = true;
                        displayVal = cellValStr + ".";
                    }
                } else {
                    displayVal = "";
                    if (cellValStr.indexOf(".") > -1) {
                        if (valParts.length > 1 && valParts[1].length >= 2) // Prohibited input for more than 2 fractional digits
                            displayVal = cellValStr;
                        else
                            displayVal = cellValStr + String(c);
                    } else {
                        if (useDecimalSep) {
                            displayVal = cellValStr + ("." + String(c));
                        } else {
                            if (cellValStr == "0")
                                displayVal = String(c);
                            else
                                displayVal = cellValStr + String(c);
                        }
                    }
                    lastInput.setValue(displayVal);
                }

                // update cell textbox
                if (g_lastInput.type == InputType.cell) {
                    lastInput.openEditor();
                    $input = lastInput.editor.$input;
                }
                else {
                    lastInput.$element.focus();
                    lastInput.$element.select();
                    $input = lastInput.$element;
                }
                if (displayVal == "0") {
                    $input.val("");
                    lastInput.setValue(0);
                }
                else {
                    $input.val(displayVal);
                    lastInput.setValue(displayVal);
                }

                // Trigger onChanged if lastInput is CELL
                if (g_lastInput.type == InputType.cell) {
                    onCartGridCellValueChanged(g_lastInput.object, displayVal);
                }
            } else { // directly modify the normal text input
                $lastInput = g_lastInput.$element;
                if ($lastInput == null || $lastInput == undefined)
                    return;

                $lastInput.focus();

                var val = $lastInput.val();
                var valStr = String($lastInput.val());

                if (c == "backspace") { // Backspace
                    if (val != "")
                        $lastInput.val(valStr.substring(0, valStr.length - 1));
                } else if (c == "clear") {
                    $lastInput.val("");
                } else if (c == "%") {
                    return; // Do not support yet
                } else if (c == ".") {
                    if (valStr.indexOf(".") == -1)
                        $lastInput.val(valStr + ".");
                } else {
                    $lastInput.val(valStr + String(c));
                }
            }

            // Check if we have to close the active editor.
            if (c == 'enter') {
                g_mainGrid.closeActiveEditor();
            } 
            else if (c == '%') {
                g_mainGrid.closeActiveEditor();
            }
            else {

                // Check if the key will replace the selected text or just append the key value to the end of the selected text.
                //var fPreventEditorClose = g_mainGrid.isPreventEditorClose;
                //if (!fPreventEditorClose)

                    g_mainGrid.preventActiveEditorClose();

                //var $input = null;
                //if (g_lastInput.type == InputType.cell || g_lastInput.type == InputType.amountBox) {
                //    var lastInput = g_lastInput.object;
                //    //$input = lastInput.$element;
                //    //$input.select();
                //    //simulateCharacterPress(c);
                //    g_mainGrid.preventActiveEditorClose();
                //} else {

                //}
            }
        }
        function simulateCharacterPress(charStr) {
            jQuery.event.trigger({ type : 'keypress', which : charStr.charCodeAt(0) });
        }
        function enablePaidBillSubmission(fEnable) {
            if (fEnable) {
                $("#btnCashierSave").removeClass("btn-disable");
                $("#btnCashierPrint").removeClass("btn-disable");
            }
            else {
                $("#btnCashierSave").addClass("btn-disable");
                $("#btnCashierPrint").addClass("btn-disable");
            }
        }
        function submitPaymentAmount(paymentType, amt) {
            //alert("type: " + paymentType + " , Amount:" + amt);

            // find payment item of this payment type and override the payment value
            var fFound = false;
            for (var i = 0; i < g_paymentItems.length; ++i) {
                var paymentItem = g_paymentItems[i];
                if (paymentItem["PaymentTypeId"] == paymentType)
                {
                    fFound = true;
                    paymentItem["Amount"] = amt;
                    paymentItem["ItemState"] = KGrid.ItemState.modifiedItem;
                    break;
                }
            }
            if (!fFound) {
                var paymentItem = {};
                paymentItem["Amount"] = amt;
                paymentItem["PaymentTypeId"] = paymentType;
                paymentItem["ItemState"] = KGrid.ItemState.newItem;
                g_paymentItems.push(paymentItem);
            }
            updateBalance();
        }
        function validatePayment() {
            return (getPayment(PAYMENT_ALL) >= g_total); // getPayment(0) is sum payment from all payment types.
        }
        function submitPaidBill() {
            var item = constructDtoItem();

            if (item["BillId"] == -1)
                item["ItemState"] = "N";
            else
                item["ItemState"] = "M";
 
            sendAjaxUpdate( "Sales.svc", "SubmitPaidBill", item,
                function(rsp) {
                    if (rsp.Success) {
                        clearPos();
                    }
                    else
                        displayError(rsp.StatusText);
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function displayPrintBill() {
            doDefaultPrint();
        }
        function onDiscountAmountClick() {
            g_useDiscountPercent = false;
            updateDiscountOnTopButtons();
            updateBalance();
        }
        function onDiscountPercentClick() {
            g_useDiscountPercent = true;
            updateDiscountOnTopButtons();
            updateBalance();
        }
        function onBtnPaymentMethodClicked(typeNum) {
            switch (typeNum) {
                case PAYMENT_CHEQUE:
                    g_paymentMethod = PAYMENT_CHEQUE;
                    break;
                case PAYMENT_CREDIT:
                    g_paymentMethod = PAYMENT_CREDIT;
                    break;
                default:
                case PAYMENT_CASH:
                    g_paymentMethod = PAYMENT_CASH;
                    break;
            }
        }
        function onBtnCashierCancelClicked() {
            g_paymentItems = [];
            updateBalance();
        }
        function onBtnCashierApplyClicked() {
            // Do nothing as the OnBlur function of the g_amtPayment will automatically handle the submit of paid amount
        }
        function onBtnCashierSaveClicked() {
            if (!validatePayment()) {
                return false;
            }
            submitPaidBill();
        }
        function onBtnCashierPrintClicked() {
            if (!validatePayment()) {
                return false;
            }
            displayPrintBill();
            submitPaidBill();
        }

        ///////////////////////////////////////////////////////////
        //
        // General
        //
        function updateDateTime() {
            var now = new Date();
            $("#lblDate").html(now.getDate() + "/" + (now.getMonth() + 1) + "/" + now.getFullYear());
            var hour = now.getHours();
            var minute = now.getMinutes();
            var second = now.getSeconds();
            if (hour < 10)
                hour = "0" + hour;
            if (minute < 10)
                minute = "0" + minute;
            if (second < 10)
                second = "0" + second;
            $("#lblTime").html(hour + ":" + minute + ":" + second);
        }
        function clearPos() {
            $("#txtNote").val("");
            clearHoldSale();
            clearCashier(); 
            updateBalance();
            switchToCatalog();
        }
        function switchToCashier() {
            if (g_mode == MODE_CASHIER)
                return;

            enablePaidBillSubmission(false);

            if (g_mainGrid.data.length <= 0) {
                displayInfo("There is no selected item.");
                return;
            }
            $("#divCashier").show();
            $("#divHoldSales").hide();
            $("#divCatalog").hide();
            $("#txtNote").attr("readonly", "readonly");
            g_mainGrid.lock();
            g_mode = MODE_CASHIER;
            g_paymentMethod = PAYMENT_CASH; // reset PaymentMethod to cash
        }
        function switchToHoldSale() {
            retrieveAllPendingBill();

            $("#divCashier").hide();
            $("#divHoldSales").show();
            $("#divCatalog").hide();
            $("#txtNote").attr("readonly", "readonly");
            g_mainGrid.lock();
            g_mode = MODE_HOLDSALE;
        }
        function switchToCatalog() {
            if (g_mode == MODE_CATALOG)
                return;
            $("#divCashier").hide();
            $("#divHoldSales").hide();
            $("#divCatalog").show();
            $("#txtNote").removeAttr("readonly", "readonly");
            g_mainGrid.unlock();
            g_mode = MODE_CATALOG;
        }
        function onClear() {
            if (g_mode == MODE_CATALOG) {
                displayModal(
                    "Confirm clear",
                    "Do you want to clear and discard unsaved data?",
                    function() {
                        clearPos();
                    },
                    function (){}
                    );
            } else if (g_mode == MODE_HOLDSALE) {
                displayModal(
                    "Confirm discard",
                    "Do you want to discard this item?",
                    function() {
                        discardSelectedHoldSale();
                    },
                    function (){}
                    );
            }
        }
        function onSwitchBarClick(barName) {
            $("#divHoldSales").animate({
                opacity: 0
            },
                500,
                function() {
                    $("#divHoldSales").css("opacity", 1);
                });
            $("#divCashier").animate({
                opacity: 0
            },
                500,
                function() {
                    $("#divCashier").css("opacity", 1);
                });
            $("#" + barName).animate({
                right: "+=100"
            },
                500,
                function() {
                    // Animation complete.
                    $("#" + barName).css("right", 0);
                    clearPos();
                });
        }
        function getNewBill() {
            var billItem = {};
            billItem['Tax'] = 0.0;
            billItem['Total'] = 0.0;
            billItem['TotalDiscount'] = 0.0;
            billItem['Subtotal'] = 0.0;
            billItem['DiscountAmount'] = 0.0;
            billItem['DiscountPercent'] = 0.0;
            billItem['IsPercentDiscount'] = false;
            billItem['AmountReceived'] = 0.0;
            billItem['AmountChange'] = 0.0;
            billItem['BillId'] = -1;
            billItem['CustomerId'] = -1;
            billItem['CustomerName'] = "Customer";
            billItem['Quantity'] = 0.0;
            billItem['Note'] = "";
            billItem['BillItems'] = [];
            billItem['BillPaymentItems'] = [];
            return billItem;
        }
        function getNewBillItem() {
            var billItem = {};
            billItem['BillItemId'] = -1;
            billItem['ItemId'] = -1;
            billItem['ItemFullName'] = "";
            billItem['Quantity'] = 0.0;
            billItem['Discount'] = 0.0;
            billItem['Price'] = 0.0;
            return billItem;
        }
        function getNewBillPaymentItem() {
            var billPaymentItem = {};
            billPaymentItem['BillPaymentItemId'] = -1;
            billPaymentItem['PaymentTypeId'] = -1;
            billPaymentItem['Amount'] = "";
            billPaymentItem['ItemState'] = "N";
            return billPaymentItem;
        }
        function constructDtoItem() {
            var item = {};
            if (g_bill == null) {
                item["BillId"] = -1;
                
                if (g_selHoldSaleCustomer.getValue() != "") 
                    item["CustomerId"] = g_selHoldSaleCustomer.getValue();
                else
                    item["CustomerId"] = -1;

                item["ItemState"] = KGrid.ItemState.newItem;
            } else {
                item["BillId"] = g_bill["BillId"];
                item["CustomerId"] = g_bill["CustomerId"];
                item["Version"] = g_bill["Version"];
                item["ItemState"] = KGrid.ItemState.modifiedItem;
            }
            
            if (g_useDiscountPercent) {
                item["DiscountAmount"] = 0;
                item["DiscountPercent"] = g_amtDiscount.getValue();
                item["IsDiscountPercent"] = true;
            }
            else {
                item["DiscountAmount"] = g_amtDiscount.getValue();
                item["DiscountPercent"] = 0;
                item["IsDiscountPercent"] = false;
            }

            item["AmountCash"] = 0;
            item["AmountCredit"] = 0;
            item["AmountCheque"] = 0;
            item["AmountReceived"] = 0;
            item["AmountChange"] = 0;
            item["Total"] = g_total;
            item["Subtotal"] = g_subtotal;
            item["Tax"] = g_tax;
            item["Quantity"] = 0;
            item["Note"] = $("#txtNote").val();

            item["BillItems"] = [];
            item["BillPaymentItems"] = [];
            for( var i = 0; i < g_mainGrid.rows.length; ++i) {
                row = g_mainGrid.rows[i];

                if (row.rowState == KGrid.ItemState.blankItem)
                    break;

                subItem = {};
                subItem["BillItemId"] = row.data["BillItemId"] == null ? -1 : parseInt(row.data["BillItemId"], 10);
                subItem["ItemId"] = row.data["ItemId"] == null ? -1 : parseInt(row.data["ItemId"], 10);
                subItem["Quantity"] = parseFloat(row.data["Quantity"]);
                subItem["Discount"] = parseFloat(row.data["Discount"]);
                subItem["Price"] = parseFloat(row.data["Price"]);
                subItem["ItemState"] = (row.rowState == KGrid.ItemState.originalItem ? KGrid.ItemState.modifiedItem : row.rowState); // Change itemState in case of 'original' to 'modified'.

                item["BillItems"].push(subItem);
            }
            //for (var i = 0; i < g_paymentItems.lenght; ++i) {
            //    var pItem = g_paymentItems[i];
            //    var subItem = {};
            //    subItem["BillPaymentItemId"] = pItem["BillPaymentItemId"];
            //    subItem["BillId"] = pItem["BillId"];
            //    subItem
            //}
            item["BillPaymentItems"] = g_paymentItems;
            return item;
        }
        function saveToHoldSale() {
            $("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });

            var item = constructDtoItem();
            sendAjaxUpdate( "Sales.svc", "SubmitHoldSale", item,
                function(rsp) {
                    retrieveHoldSale(rsp.ResponseItem, null);  
                    switchToHoldSale();
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="pos-top">
        <div class="pos-left">
            <div class="bar-top">
                <div class="login-info">
                    Logged in as <span id="lblUsernameTop" style="max-width: 100px; overflow: hidden;"></span>| Date: <span id="lblDate"></span>| Time <span id="lblTime"></span>
                </div>
                <div class="product-search">
                    <div class="div-search-input">
                        <input type="text" id="txtProductSearch" class="input-text" placeholder="Find Product or Service" />
                    </div>
                    <div class="spyglass-button">
                        <img src="../Images/pos_spyglass_80x80.png" height="40" width="40" onclick="onBtnProductSearchClick();" />
                    </div>
                </div>
            </div>
            <div class="bar-sum">
                <div class="amount-big">
                    <span id="lbl-amount-big">0.00</span>
                </div>
                <div class="amount-small">
                    <div>
                        disc:<span id="lbl-discount">0.00</span><br />
                        sub:<span id="lbl-subtotal">0.00</span><br />
                        change:<span id="lbl-change">0.00</span><br />
                        tax:<span id="lbl-tax">0.00</span><br />
                    </div>
                </div>
            </div>
            <div class="switchable-pane">
                <div id="divCatalog" class="catalog">
                    <div class="menu1">
                        <div class="nav-arrow left" onclick="onBtnMenu1Click('left');">
                            <img src="../Images/menu1_nav_left.png" />
                        </div>
                        <div id="divMenu1" class="item-bar">
                            <div id="divMenu1-0" class="menu1-item"></div>
                            <div id="divMenu1-1" class="menu1-item"></div>
                            <div id="divMenu1-2" class="menu1-item"></div>
                            <div id="divMenu1-3" class="menu1-item"></div>
                            <div id="divMenu1-4" class="menu1-item"></div>
                            <div id="divMenu1-5" class="menu1-item"></div>
                            <div id="divMenu1-6" class="menu1-item"></div>
                            <div id="divMenu1-7" class="menu1-item"></div>
                        </div>
                        <div class="nav-arrow right" onclick="onBtnMenu1Click('right');">
                            <img src="../Images/menu1_nav_right.png" />
                        </div>
                    </div>
                    <div id="divMenu2" class="menu2">
                        <div class="menu2-arrow" onclick="onBtnMenu2Click('up');">
                            <img src="../Images/menu2_uparrow.png" />
                        </div>
                        <div class="menu2-items">
                            <div id="divMenu2-0" class="menu2-item"></div>
                            <div id="divMenu2-1" class="menu2-item"></div>
                            <div id="divMenu2-2" class="menu2-item"></div>
                            <div id="divMenu2-3" class="menu2-item"></div>
                            <div id="divMenu2-4" class="menu2-item"></div>
                            <div id="divMenu2-5" class="menu2-item"></div>
                        </div>
                        <div class="menu2-arrow" onclick="onBtnMenu2Click('down');">
                            <img src="../Images/menu2_downarrow.png" />
                        </div>
                    </div>
                    <div class="menu3">
                        <ul id="menu3Tab" class="nav nav-tabs">
                            <%--<li class="active"><a href="#">Page 1</a></li>--%>
                            <%--                            
                            <li><a href="#">Page 2</a></li>
                            <li><a href="#">Page 3</a></li>
                            <li><a href="#">Page 4</a></li>
                            <li><a href="#">Page 5</a></li>
                            <li><a href="#">Page 6</a></li>
                            --%>
                        </ul>
                        <div id="divMenu3" class="menu3-tab-view">
                        </div>

                        <!-- Modal dialog to show subItems in the catalog pane -->
                        <div id="divDialog" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><strong>X</strong></span></button>
                                        <h3 class="modal-title" id="lblDialogTitle"></h3>
                                    </div>
                                    <div id="divDialogBody" class="modal-body">
                                    </div>
                                    <!--
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                    -->
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                    </div>
                </div>
                <div id="divHoldSales" class="holdsales" style="display: none">
                    <div class="holdsales-container">
                        <div class="switch-bar" id="barHoldSale" onclick="onSwitchBarClick('barHoldSale');">
                            <img src="../Images/pos_switch_bar.png" />
                        </div>
                        <div class="holdsales-caption">
                            HOLD SALE
                        </div>
                        <div class="holdsales-grid">
                            <div id="holdSaleGrid" style="width: 790px; height: 235px;"></div>
                        </div>
                        <div class="holdsales-command">
                            <div id="btnHoldSalesApply" class="button btn-holdsales-apply" onclick="onBtnHoldSaleApply();">
                                APPLY
                            </div>
                            <div id="btnHoldSalesCancel" class="button btn-holdsales-cancel" onclick="onBtnHoldSaleCancel();">
                                CANCEL
                            </div>
                        </div>

                    </div>

                </div>
                <div id="divCashier" class="cashier" style="display: none">
                    <div class="cashier-container">
                        <div class="switch-bar" id="barCashier" onclick="onSwitchBarClick('barCashier');">
                            <img src="../Images/pos_switch_bar.png" />
                        </div>
                        <div class="cashier-form">
                            <div class="form-row">
                                <div class="caption">Customer :</div>
                                <div class="txt-customer-container">
                                    <%--<input type="text" id="txtCashierCustomer" class="txt-cashier input-text" placeholder="Find Customer" />--%>
                                    <select id="selCashierCustomer" class="" placeholder="-- customer --"></select>
                                </div>
                                <%--<div id="btnSearch" class="button btn-cashier-search">Search</div>--%>
                            </div>
                            <div class="form-row">
                                <div class="caption">Discount :</div>
                                <div class="txt-discount-container">
                                    <input type="text" id="txtDiscount" class="txt-cashier input-amount" />
                                </div>
                                <div id="btnDiscountAmount" class="button btn-cashier-dollar" onclick="onDiscountAmountClick();">$</div>
                                <div id="btnDiscountPercent" class="button btn-cashier-percent" onclick="onDiscountPercentClick();">%</div>
                            </div>
                            <div class="form-row">
                                <div class="caption">Pricing Level :</div>
                                <div class="txt-pricing-container">
                                    <input type="text" id="txtPricingLevel" placeholder="Normal" readonly="readonly" class="txt-cashier" />
                                </div>
                            </div>
                            <div class="form-last-row">
                                <div class="col1">
                                    <div class="caption">Payment Method :</div>
                                    <div id="btnCash" class="button btn-cash" onclick="onBtnPaymentMethodClicked(PAYMENT_CASH);">Cash</div>
                                    <div id="btnCredit" class="button btn-credit" onclick="onBtnPaymentMethodClicked(PAYMENT_CREDIT);">Credit</div>
                                    <div id="btnCheque" class="button btn-cheque" onclick="onBtnPaymentMethodClicked(PAYMENT_CHEQUE);">Cheque</div>
                                    <div class="txt-paymentamount-container">
                                        <input type="text" id="txtAmt1" class="txt-cashier input-amount" />
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="caption">Amount<br />
                                        Tendered :</div>
                                    <div class="txt-amt-tendered-container">
                                        <input type="text" id="txtAmt2" class="txt-cashier" />
                                    </div>
                                </div>
                                <div class="col3">
                                    <div class="caption">Change<br />
                                        Amount :</div>
                                    <div class="txt-amt-changed-container">
                                        <input type="text" id="txtAmt3" class="txt-cashier" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cashier-command">
                            <div id="btnCashierCancel" class="button btn-cashier-command" onclick="onBtnCashierCancelClicked();">
                                Cancel
                            </div>
                            <div id="btnCashierApply" class="button btn-cashier-command" onclick="onBtnCashierApplyClicked();">
                                Apply
                            </div>
                            <div id="btnCashierSave" class="button btn-cashier-command" onclick="onBtnCashierSaveClicked();">
                                Save
                            </div>
                            <div id="btnCashierPrint" class="button btn-cashier-command" onclick="onBtnCashierPrintClicked();">
                                Print
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal dialog to show subItems in the catalog pane -->
                <div id="divCustomerNameDlg" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><strong>X</strong></span></button>
                                <h3 class="modal-title">Specify Customer Name</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-customername">
                                    <select id="selHoldSaleCustomer" placeholder="Customer" class="sel-holdsale-customername not-cause-dirty"></select>
                                    <div id="btnCustomerNameOk" class="btn-customername-ok" onclick="onBtnCustomerNameOk();">OK</div>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
        </div>
        <div class="pos-right">
            <div class="pos-logo">
                <a href="Dashboard.aspx">
                    <img src="../Images/logo00.png" />
                </a>
            </div>
            <div class="pos-user">

                <div class="" style="padding: 10px 10px 0 10px;">
                    <div id="divMainAvatar" class="height-50 width-50" style="background-color: lightblue; text-align: center; line-height: 50px; display: inline-block; vertical-align: middle;">
                        Image
                    </div>
                    <div class="" style="height: 50px; max-width: 100px; text-align: left; overflow: hidden; margin-left: 5px; display: inline-block; vertical-align: middle;">
                        <span style="font-size: 8pt;" id="lblUsername"></span>
                        <br />
                        <%--                        <span style="font-size: 7pt; color:gray;">mitsumete</span><br />
                        <span style="font-size: 8pt; color:blue;" >[Admin]</span>--%>
                    </div>
                </div>

            </div>
            <div class="mainmenu">
            </div>
        </div>
    </div>
    <div class="pos-bottom">
        <div class="itemlist">
            <div class="pos-grid">
                <div id="mainGrid" class="main" style="height: 190px"></div>
            </div>
            <div class="grid-command">
                <div class="button btn-print-invoice">Print Invoice</div>
                <div class="button btn-delete" onclick="onDeleteOrder();">Delete</div>
            </div>
            <div class="notes">
                <div>
                    <span>Notes:</span>
                    <input type="text" id="txtNote" class="input-text" />
                    <div id="btnNote" class="button btn-enter">ENTER</div>
                </div>
            </div>
        </div>
        <div class="keypad">
            <div class="num" onclick="triggerKey2('1');">1</div>
            <div class="num" onclick="triggerKey2('2');">2</div>
            <div class="num" onclick="triggerKey2('3');">3</div>
            <div class="num" onclick="triggerKey2('4');">4</div>
            <div class="num" onclick="triggerKey2('5');">5</div>
            <div class="num" onclick="triggerKey2('6');">6</div>
            <div class="num" onclick="triggerKey2('7');">7</div>
            <div class="num" onclick="triggerKey2('8');">8</div>
            <div class="num" onclick="triggerKey2('9');">9</div>
            <div class="num bottommost" onclick="triggerKey2('0');">0</div>
            <div class="num bottommost" onclick="triggerKey2('.');">.</div>
            <div class="num bottommost" onclick="triggerKey2('%');">%</div>
        </div>
        <div class="calc-pad">
            <div class="calc-group1">
                <div class="btn-calc-del" onclick="triggerKey2('backspace');">
                    <img src="../Images/calc_leftarrow_62x62.png" /></div>
                <div class="btn-calcfield-clear" onclick="triggerKey2('clear');">C</div>
            </div>
            <div class="btn-calcpad-clear" onclick="onClear();">CLEAR</div>
            <div class="btn-hold" onclick="processCurrentBill();">HOLD</div>
            <div class="btn-calc-enter" onclick="triggerKey2('enter');">
                <img src="../Images/calc_enter_62x62.png" /></div>
            <div class="btn-getbill" onclick="switchToCashier();">$</div>
        </div>

    </div>
</asp:Content>
