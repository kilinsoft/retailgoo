﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NoSelectBusiness.Master" AutoEventWireup="true" CodeBehind="CreateBusiness.aspx.cs" Inherits="RetailGOO.Page.CreateBusiness" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <script type="text/javascript">
        function pageLoad() {

        }
        function createBusiness() {
            if (!validateForm()) {
                return;
            }
            var item = {};
            item["BusinessTypeId"] = -1;
            item["BusinessName"] = $("#txtBusinessName").val();
            item["ImagePath"] = "";
            item["Address1"] = "";
            item["Address2"] = "";
            item["Address3"] = "";
            item["CountryId"] = -1;
            item["City"] = "";
            item["ZipCode"] = "";
            item["Phone"] = "";
            item["Fax"] = "";
            item["Email"] = "";
            item["Website"] = "";
            item["FacebookUrl"] = "";
            item["TaxIdInfo"] = "";

            sendAjaxUpdate("BusinessService.svc", "CreateBusiness", item,
                function (rsp) {
                    GoToUrl('BusinessSelection.aspx');
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function validateForm() {
            $("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });

            if ($("#txtBusinessName").val() == "") {
                displayError("Please enter the Business Name");
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <!-- Region: Business Info -->
    <div style="padding: 15px;">
        <table>
            <tbody>
    <%--            <tr>
                    <td class="cell-name">Logo:</td>
                    <td class="cell-control">
                        <input type="button" id="btnChooseFile" value="Choose File" class="btn-action-big" />
                    </td>
                </tr>--%>
                <tr>
                    <td></td>
                    <td class="cell-control">
                        <img id="imgLogo" src="../images/logo00.png"/>
                    </td>
                </tr>
<%--                <tr>
                    <td class="cell-name">Business Type:</td>
                    <td class="cell-control">
                        <select id="selType" placeholder="-- Busines Type --" class="width-150">
                            <option value="1">Book Store</option>
                            <option value="2">Coffee Shop</option>
                            <option value="3">Grocery</option>
                        </select>
                    </td>
                </tr>--%>
<%--                <tr>
                    <td class="cell-name">Synchronize Master Data:</td>
                    <td class="cell-control">
                        <input type="checkbox" id="chkSync" />
                    </td>
                </tr>--%>
                <tr>
                    <td class="cell-name">Business Name:</td>
                    <td class="cell-control">
                        <input type="text" id="txtBusinessName" class="width-200" />
                    </td>
                </tr>
<%--                <tr>
                    <td class="cell-name">Address:</td>
                    <td class="cell-control">
                        <input type="text" id="txtAddr1" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name"></td>
                    <td class="cell-control">
                        <input type="text" id="txtAddr2" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name"></td>
                    <td class="cell-control">
                        <input type="text" id="txtAddr3" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">City:</td>
                    <td class="cell-control">
                        <input type="text" id="txtCity" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Country:</td>
                    <td class="cell-control">
                        <select id="selCountry" class="width-150">
                            <option value="TH">TH</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Zip Code:</td>
                    <td class="cell-control">
                        <input type="text" id="txtPostal" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Phone:</td>
                    <td class="cell-control">
                        <input type="text" id="txtPhone" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Fax:</td>
                    <td class="cell-control">
                        <input type="text" id="txtFax" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Email:</td>
                    <td class="cell-control">
                        <input type="text" id="txtEmail" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Website:</td>
                    <td class="cell-control">
                        <input type="text" id="txtWebsite" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Facebook Page:</td>
                    <td class="cell-control">
                        <input type="text" id="txtFbUrl" class="width-200" />
                    </td>
                </tr>
                <tr>
                    <td class="cell-name">Tax ID:</td>
                    <td class="cell-control">
                        <input type="text" id="txtTaxIdInfo" class="width-200" />
                    </td>
                </tr>--%>
                <tr>
                    <td></td>
                    <td class="cell-control">
                        <input type="button" id="btnCreate" class="btn-action-big" onclick="createBusiness();" value="Create Business" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
