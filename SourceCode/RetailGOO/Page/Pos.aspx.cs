﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class Pos : System.Web.UI.Page
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }

        public string BrandListString { get; set; }
        public string CategoryListString { get; set; }
        public string SalesItemsString { get; set; }
        public string CustomerListString { get; set; }
        public string TaxFactor { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();

            TaxFactor = SessionHelper.BusinessInfo.Tax.ToString();

            if (AuthenticationHelper.IsAuthenticated())
            {
                // Redirect to business screen if the user has not selected a business to work with
                if (HttpContext.Current.Session[SessionVariable.BusinessId] == null ||
                    HttpContext.Current.Session[SessionVariable.BusinessId].ToString() == "")
                {
                    // Do not redirect if the screen is in this list
                    string[] ignoreList = new string[] { 
                        RetailGooPageUrl.BusinessSelection,
                        RetailGooPageUrl.CreateBusiness
                    };
                    bool fRedirect = true;
                    for (int i = 0; i < ignoreList.Length; ++i)
                    {
                        string pageUrl = ignoreList[i].Substring(1);
                        if (Request.Url.AbsolutePath.Equals(pageUrl, StringComparison.OrdinalIgnoreCase))
                        {
                            fRedirect = false;
                            break;
                        }
                    }
                    if (fRedirect)
                        Response.Redirect(RetailGooPageUrl.BusinessSelection);
                }

                if (HttpContext.Current.Session[SessionVariable.User] != null &&
                    HttpContext.Current.Session[SessionVariable.UserProfile] != null &&
                    HttpContext.Current.Session[SessionVariable.Staff] != null)
                {
                    UserProfileDto userProfile = (UserProfileDto)HttpContext.Current.Session[SessionVariable.UserProfile];
                    RetailGOO.Class.User user = (RetailGOO.Class.User)HttpContext.Current.Session[SessionVariable.User];
                    Staff staff = (Staff)HttpContext.Current.Session[SessionVariable.Staff];
                    Username = userProfile.Firstname + " " + userProfile.Lastname;
                    Email = user.Email;
                    RoleName = staff.Role.ToString();
                }

            }
            else
            {
                Response.Redirect(RetailGooPageUrl.LogInPage +
                    "?ReturnPage=" +
                    Encryption.SimpleEncrypt(Server.UrlEncode(Request.Url.ToString())));
            }

            var rsp = ItemAdapter.RetrieveItemBrandList(false); // Do not automatically add "<undefined>" item
            if (rsp.Success && rsp.ResponseItem != null && rsp.ResponseItem.Count > 0)
                BrandListString = HtmlSelectHelper.ConvertToListString(rsp.ResponseItem);
            else
                //BrandListString = "[{Value:-1,Text:'<undefined>'}]";
                BrandListString = "[]";

            var rspCategory = ItemAdapter.RetrieveItemCategoryList(false); // Do not automatically add "<undefined>" item
            if (rspCategory.Success && rspCategory.ResponseItem != null && rspCategory.ResponseItem.Count > 0)
                CategoryListString = HtmlSelectHelper.ConvertToListString(rspCategory.ResponseItem);
            else
                //CategoryListString = "[{Value:-1,Text:'<undefined>'}]";
                CategoryListString = "[]";

            var rspCust = CustomerAdapter.RetrieveCustomerList(); // Do not automatically add "<undefined>" item
            if (rspCust.Success && rspCust.ResponseItem != null && rspCust.ResponseItem.Count > 0)
                CustomerListString = HtmlSelectHelper.ConvertToListString(rspCust.ResponseItem);
            else
                //CategoryListString = "[{Value:-1,Text:'<undefined>'}]";
                CustomerListString = "[]";

            // Prepare merchandise
            var rspItems = ItemAdapter.RetrieveAllSalesItem();
            if (rspItems.Success && rspItems.ResponseItem != null && rspItems.ResponseItem.Count > 0)
            {
                // Check each item, if there are series of items with the same ItemGroupId, it means 
                // the item contains subItems and the item should be represented as Item-Level-1 using ItemGroupName.
                // The underlying items (subItems) then be represented as Item-Level-2 using the ItemName.

                var itemGroupDict = new Dictionary<long, List<ItemDto>>();
                for (int i = 0; i < rspItems.ResponseItem.Count; ++i)
                {
                    var item = rspItems.ResponseItem[i];
                    List<ItemDto> subItemList = null;
                    if (itemGroupDict.TryGetValue(item.ItemGroupId, out subItemList))
                    {
                        subItemList.Add(item);
                    }
                    else
                    {
                        subItemList = new List<ItemDto>();
                        subItemList.Add(item);
                        itemGroupDict.Add(item.ItemGroupId, subItemList);
                    }
                        
                }

                StringBuilder sb = new StringBuilder();
                sb.Append("["); // Outer-most array start character
                {   // Process each itemGroup
                    var jsonItemGroupList = new List<string>();
                    foreach (var itemGroupList in itemGroupDict)
                    {
                        var itemList = itemGroupList.Value;
                        StringBuilder sbItemGroup = new StringBuilder();
                        sbItemGroup.Append("[");
                        {
                            var jsonParamList = new List<string>();
                            for (int i = 0; i < itemList.Count; ++i)
                            {
                                StringBuilder itemSb = new StringBuilder();
                                itemSb.Append("{");
                                {
                                    var item = itemList[i];
                                    var paramList = new List<string>();
                                    paramList.Add(string.Format("{0}:{1}", "ItemId", item.ItemId));
                                    paramList.Add(string.Format("{0}:{1}", "ItemGroupId", item.ItemGroupId));
                                    paramList.Add(string.Format("{0}:\"{1}\"", "ItemGroupName", item.ItemGroupName));
                                    paramList.Add(string.Format("{0}:\"{1}\"", "ItemGroupCode", item.ItemGroupCode));
                                    paramList.Add(string.Format("{0}:\"{1}\"", "ItemName", item.ItemName));
                                    paramList.Add(string.Format("{0}:{1}", "Price", item.Price));
                                    paramList.Add(string.Format("{0}:\"{1}\"", "UnitOfMeasure", item.UnitOfMeasure));
                                    paramList.Add(string.Format("{0}:{1}", "ItemBrandId", item.ItemBrandId));
                                    paramList.Add(string.Format("{0}:{1}", "ItemCategoryId", item.ItemCategoryId));

                                    string paramListString = String.Join(",", paramList);
                                    itemSb.Append(paramListString);
                                }
                                itemSb.Append("}");
                                jsonParamList.Add(itemSb.ToString());
                            }
                            string itemListString = String.Join(",", jsonParamList);
                            sbItemGroup.Append(itemListString);
                        }
                        sbItemGroup.Append("]");
                        jsonItemGroupList.Add(sbItemGroup.ToString());
                    }
                    string itemGroupListString = String.Join(",", jsonItemGroupList);
                    sb.Append(itemGroupListString);
                }
                sb.Append("]");
                SalesItemsString = sb.ToString();
            }
            else
                SalesItemsString = "[]";
            
        }
    }
}