﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections;

namespace RetailGOO.Page
{
    public partial class CustomerInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //PopulateCountry(selBtCountry);
            //PopulateCountry(selStCountry);
        }

        private void PopulateCountry(System.Web.UI.HtmlControls.HtmlSelect selObj)
        {
            Hashtable h = new Hashtable();
            Dictionary<string, string> objDic = new Dictionary<string, string>();
            foreach (CultureInfo ObjCultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo objRegionInfo = new RegionInfo(ObjCultureInfo.Name);
                if (!objDic.ContainsKey(objRegionInfo.EnglishName))
                {
                    objDic.Add(objRegionInfo.EnglishName, objRegionInfo.TwoLetterISORegionName.ToUpper());
                }
            }
            SortedList<string, string> sortedList = new SortedList<string, string>(objDic);
            foreach (KeyValuePair<string, string> val in sortedList)
            {
                selObj.Items.Add(new ListItem(val.Key, val.Value));
            }
        }
    }
}