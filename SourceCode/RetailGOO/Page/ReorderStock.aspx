﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="ReorderStock.aspx.cs" Inherits="RetailGOO.Page.ReorderStock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .date-caption {
            color: #e75a73;
            padding-left: 100px !important;
        }
        .date-control input[type=text] {
            width: 100%;
        }
        .grid-remark table {
            width: 100%;
        }
        .grid-remark td:first-child {
            width: 70px;
        }
        .grid-remark td:first-child + td {
            width: auto;
            padding-right: 10px;
        }
        .grid-remark td:first-child + td + td {
            width: 200px;
        }
        .grid-remark input[type=text] {
            width: 100%;
        }
        .grid-remark input[type=button] {
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        // Variables declaration
        function pageLoad() {

            preventNavigationOnUnsaved(true);

            // Initialize behaviors
            var selEditorOptions = {
                //create: true,
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                onLoad: onLoadHandler,
                options: []
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            {
                title: "Item", name: "ItemId", width: 300, required: true, editable: true,
                editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions),
                onChanged: onItemChangedHandler
            },
            {
                title: "Qty Available", name: "Quantity", required: false, editable: false,
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0)
                //,onChanged: recalculateAmount
            },
            { title: "On Order", name: "OnOrder", required: false, editable: true },
            {
                title: "Reorder Qty", name: "ReOrderQuantity", required: false, editable: true,
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0)
                //,onChanged: recalculateAmount
            },
            {
                title: "Reorder Point", name: "ReorderPoint", required: false, editable: true,
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0)
                //,onChanged: recalculateAmount
            },
            {
                title: "Last Price", name: "Price", required: false, editable: false,
                formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2)
                //,onChanged: recalculateAmount
            },
            { title: "Last Supplier", name: "LastSupplier", required: false, editable: false }
            ];
            var mainGridDef = {
                editable: true,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                onRowMouseEnter: mgOnRowMouseEnter,
                onRowMouseLeave: mgOnRowMouseLeave
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            initGridSorter(g_mainGrid, "#selMainSelectedBy", [
                { title: "Item No.", name: "ItemId", valueCompare: false },
                { title: "Qty Available", name: "QtyAvailable", valueCompare: false },
                { title: "On Order", name: "OnOrder", valueCompare: false },
                { title: "Reorder Qty", name: "ReorderQty", valueCompare: false },
                { title: "Reorder Point", name: "ReorderPoint", valueCompare: false },
                { title: "Last Price", name: "LastPrice", valueCompare: false },
                { title: "Last Supplier", name: "LastSupplier", valueCompare: false }
                ]);

            $("#txtDate").datepicker({ autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            // Init elements' behaviors
            $("#btnVoid").addClass('disable').disable();
            $("#btnPrint").addClass('disable').disable();

            // Init state
            changeState(ePageState.mainNew);

            // Ajax calls to initialize mandatory variables
            retrieveMainGridData();
        }
        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#txtDate").prop('disabled', true);
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtDate").prop('disabled', false);
        }
        function setFormData(item) {
            $("#txtDate").datepicker("setDate", new Date(item["Date"]));
        }
        function clearFormData() {
            $("#tblForm input[type=text]").val("");
            $("#txtDate").datepicker("setDate", new Date());
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveAllSalesInvoice", {},
                function (rsp) {
                    setGridData(g_mainGrid, rsp.ResponseItem);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(SalesInvoiceId) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveSalesInvoice", SalesInvoiceId,
                function (rsp) {
                    g_activeMainItem = rsp.ResponseItem;
                    displayMainItemData(g_activeMainItem);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function setGridData(grid, data) {
            if (grid === g_mainGrid) {
                g_mainItems = data;
                $("#selMainSelectedBy option:first-child").prop("selected", true);
            }
            grid.setData(data);
        }
        function displayMainItemData(item) {
            setFormData(item);
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    mainSectionUnlock();
                    enableSaveClear();

                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }
        // Grid controls
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if ($("#txtDate").val() == "") {
                displayError("Please specify the Date");
                return false;
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            //item["SalesInvoiceId"] = row == null ? -1 : row.data["SalesInvoiceId"];
            item["Date"] = convertToUTCDateTime($("#txtDate").val());

            return item;
        }
        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate("Sales.svc", "ManageSalesInvoice", item,
                function (rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        if (selectedRow != null)
                            selectedRow.select(null, false);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
        }
        function doClear() {
            var fClear = window.confirm("Your unsaved data will be lost.\nDo you want to continue?");
            if (fClear) {
                setPageDirty(false);
                switch (g_pageState) {
                    case ePageState.mainNew:
                        changeState(ePageState.mainNew);
                        break;
                    case ePageState.mainView:
                        break;
                    case ePageState.mainEdit:
                        if (g_mainGrid.data.length > 0)
                            setFormData(g_mainGrid.data[g_mainGrid.selectedRow.rowIndex]);
                        //setGridData(g_subGrid, g_subItems);
                        //g_subGrid.unlock();
                        changeState(ePageState.mainEdit);
                        //changeState(ePageState.mainNew);
                        break;
                    default:
                        break;
                }
            }
        }
        function doPrint() {
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('ReceiveTransferStock.aspx');
        }
        function goNext() {
            changeUrl('ItemManagement.aspx');
        }
        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == eItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["SalesInvoiceId"]);
                }
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function mgOnRowMouseEnter(pRow) {
            delayDisplayTooltip(displayTableView, pRow.data["SalesInvoiceId"]);
        }
        function mgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function displayTableView(value) {
            /*
            sendAjax("../Service/Sales.svc/RetrieveSalesInvoice", value,
                function (rsp) {
                    var item = rsp.ResponseItem;
                    var tooltipContent;
                    tooltipContent = 'Delivery To: ' + g_selDeliveryTo.getOption(item['DestinationTypeId'])[0].innerText + '</br>';
                    tooltipContent += 'Delivery Date: ' + item["DateDelivery"] + '</br>';
                    tooltipContent += 'Sales Person: ' + g_selSalesPerson.getOption(item['SalesPersonId'])[0].innerText;
                    $('#divDynamicTooltip').html(tooltipContent);
                    $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
                },
                function (type, err, errThrown) {
                });
                */
        }
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_mainItems, "ItemId", pVal, "ItemFullName");
        }
        function onLoadHandler(pCell, pCallback) {
            g_cell = pCell;
            retrieveAjaxDataAsync(
                "ItemManagement.svc", "RetrieveItemList", {},
                function (items) {
                    g_itemList = items;
                    pCallback(g_itemList);
                },
                function (txt) {
                    displayError(txt);
                    pCallback();
                });
        }
        function onItemChangedHandler(pCell, pVal) {
            /*
            var row = pCell.parentRow;
            var qtyCell = row.getCellByColumnName("Quantity");
            var priceCell = row.getCellByColumnName("Price");
            var amtCell = row.getCellByColumnName("Amount");

            if (pVal === "") {
                if (row.rowState == KGrid.ItemState.newItem) {
                    qtyCell.setValue("");
                    priceCell.setValue("");
                    amtCell.setValue("");
                }
                else
                    return;
            } else if (isNaN(parseInt(pVal, 10))) { // user specify non-existent item
                qtyCell.setValue(0);
                priceCell.setValue(0);
                amtCell.setValue(0);
                //row.data["OptionName"] = pVal;

                recalculateAmount(pCell, pVal);
            } else {
                retrieveAjaxDataAsync(
                    "ItemManagement.svc", "RetrieveItem", pVal,
                    function (item) {
                        qtyCell.setValue(0);
                        priceCell.setValue(item.Price);
                        //row.data["OptionName"] = item.OptionName;

                        recalculateAmount(pCell, pVal);
                    },
                    function (txt) {
                        displayError(txt);
                    });
            }
            */
        }
        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                }
                else {
                    $("#btnMainEdit").enable();
                    $("#btnMainDelete").disable();
                }
            }
        }
        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();

            if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?")) {
                var data = selectedRow.data;
                var item = {};
                item["Id"] = data["Id"];
                item["Version"] = data["Version"];
                item["ItemState"] = KGrid.ItemState.deletedItem;
                //item["Items"] = [];

                sendAjaxUpdate("Sales.svc", "ManageSalesInvoice", item,
                    function (rsp) {
                        setPageDirty(false);
                        retrieveMainGridData();
                        changeState(ePageState.mainNew);
                    },
                    function (rsp) {
                        displayError(rsp.StatusText);
                    });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-pink">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ItemManagement.aspx');">ItemManagement</a></li>
            <li class="inactive" style="width: 184px;"><a href="#" data-toggle="tab" onclick="changeUrl('CategoryManagement.aspx');">Category Management</a></li>
            <li class="inactive" style="width: 180px;"><a href="#" data-toggle="tab" onclick="changeUrl('BrandManagement.aspx')">Brand Management</a></li>
            <%--<li class="inactive" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CountSheet.aspx');">Count Sheet</a></li>--%>
            <li class="inactive" style="width: 280px;"><a href="#" data-toggle="tab" onclick="changeUrl('CurrentStock.aspx');">Current Stock</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('AdjustStock.aspx');">Adjust Stock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('TransferStock.aspx');">Transfer Stock</a></li>
            <li class="inactive" style="width: 304px;"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveTransferStock.aspx')">Receive TransferStock</a></li>
            <li class="active"><a href="#" data-toggle="tab" onclick="changeUrl('ReorderStock.aspx');">Reorder Stock</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <!-- Form Pane -->
                <div id="divForm" class="section">
                    <table id="tblForm" class="form double">
                        <tbody>
                        <tr>
                            <td></td>
                            <td>
                            </td>
                            <td class="date-caption">Date:</td>
                            <td class="date-control">
                                <input type="text" id="txtDate" style="width: 195px;" />
                                <img src="../images/icon_calendar.png" />
                            </td>
                        </tr>
                        </tbody>
			        </table>
                </div> <!-- Form Pane -->
                <div class="grid-command no-border">
                    <div>
                        <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                        <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                    </div>
                    <div>
                        <span>Selected by:</span>
                        <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                        </select>
                    </div>
                </div>
                <div class="section">
                <div id="mainGrid" class="search" style="height: 180px;">
                </div>
                </div>
                <div class="section grid-remark">
                    <table>
                        <tr>
                            <td class="caption-normal">Remark:</td>
                            <td>
                                <input type="text" id="txtNote" />
                            </td>
                            <td>
                                <input type="button" id="txtCreatePo" value="Create PO" class="btn-action-big btn-pink" onclick="btnCreatePoOnClick();" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>

