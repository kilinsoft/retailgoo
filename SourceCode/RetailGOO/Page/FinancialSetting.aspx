﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="FinancialSetting.aspx.cs" 
    Inherits="RetailGOO.Page.FinancialSetting" ClientIDMode="Static"%>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .sel-tax {
            width: 125px !important;
        }
        .txt-tax {
            width: 125px !important;
        }
        .txt-tax + span {
            padding-left: 5px;
        }
    </style>
    <script type="text/javascript">
        // Variables declaration

        // Control variables
        var g_selCurrency;
        var g_selDefaultPriceType;
        var g_selSalesTaxType;
        var g_selCostingType;

        // Data variables
        var g_currencyList = <%=CurrencyListString%>;
        var g_defaultpricetypeList = <%=DefaultPriceTypeListString%>;
        var g_salestaxtypeList = <%=SalesTaxTypeListString%>;
        var g_costingtypeList = <%=CostingTypeListString%>;

        function pageLoad() {

            preventNavigationOnUnsaved(true);

            g_selCurrency = $("#selCurrency").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                readOnly: true,
                options: g_currencyList,
                onChange: function (val) {
                    //if (val != "" && val != -1) {
                    //    if (g_pageState == ePageState.mainNew)
                    //        retrieveCustomerInfo(val);
                    //}
                },
            })[0].selectize;

            g_selDefaultPriceType = $("#selPricing").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                readOnly: true,
                options: g_defaultpricetypeList,
                onChange: function (val) {
                    //if (val != "" && val != -1) {
                    //    if (g_pageState == ePageState.mainNew)
                    //        retrieveCustomerInfo(val);
                    //}
                },
            })[0].selectize;

            g_selSalesTaxType = $("#selTaxType").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                readOnly: true,
                options: g_salestaxtypeList,
                onChange: function (val) {
                    //if (val != "" && val != -1) {
                    //    if (g_pageState == ePageState.mainNew)
                    //        retrieveCustomerInfo(val);
                    //}
                },
            })[0].selectize;

            g_selCostingType = $("#selCosting").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                readOnly: true,
                options: g_costingtypeList,
                onChange: function (val) {
                    //if (val != "" && val != -1) {
                    //    if (g_pageState == ePageState.mainNew)
                    //        retrieveCustomerInfo(val);
                    //}
                },
            })[0].selectize;

            // Initialize behaviors
            $("#btnVoid").addClass('disable').disable();
            $("#btnPrint").addClass('disable').disable();
            $("#btnClear").addClass('disable').disable();

            // Ajax calls to initialize mandatory variables
            retrieveMainData();
        }
        function retrieveMainData(funcOnSuccess) {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveFinancialSetting", {},
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function displayMainItemData(item) {
            setFormData(item);
        }
        function setFormData(item) {
            g_selCurrency.setValue(item.CurrencyId);
            g_selDefaultPriceType.setValue(item.DefaultPriceTypeId);
            g_selSalesTaxType.setValue(item.SalesTaxTypeId);
            $("#txtTaxAmount").val(item.SalesTaxAmount);
            $("#chkItemControl").prop('checked', item.UseStockItemControl);
            g_selCostingType.setValue(item.CostingTypeId);
        }
        function constructDtoItem() {
            var item = {};
            var row = g_activeMainItem;

            item["CurrencyId"] = g_selCurrency.getValue();
            item["DefaultPriceTypeId"] = g_selDefaultPriceType.getValue();
            item["UseServiceCharge"] = row["UseServiceCharge"];
            item["ServiceChargeAmount"] = row["ServiceChargeAmount"];
            item["SalesTaxTypeId"] = g_selSalesTaxType.getValue();
            item["SalesTaxAmount"] = parseFloat($("#txtTaxAmount").val());
            item["UseStockItemControl"] = $("#chkItemControl").prop('checked');
            item["CostingTypeId"] = g_selCostingType.getValue();

            return item;
        }
        function validateForm() {
            if (g_selCurrency.getValue() == "")
            {
                displayError("Please specify Currency");
                return false;
            }
            if (g_selDefaultPriceType.getValue() == "")
            {
                displayError("Please specify Default Pricing");
                return false;
            }
            if (g_selSalesTaxType.getValue() == "")
            {
                displayError("Please specify Sales Tax");
                return false;
            }
            if (g_selCostingType.getValue() == "")
            {
                displayError("Please specify Costing Method");
                return false;
            }
            return true;
        }
        function doSave() {
            if (!validateForm())
                return;
            var item = constructDtoItem();

            sendAjaxUpdate("BusinessService.svc", "ManageFinancialSetting", item,
                function (rsp) {
                    setPageDirty(false);
                    //if (g_pageState == ePageState.mainEdit) {
                    //    changeState(ePageState.mainView);
                    //    var selectedRow = g_mainGrid.selectedRow;
                    //    g_mainGrid.clearSelection();
                    //    if (selectedRow != null)
                    //       selectedRow.select(null, false);
                    //}
                    //else {
                    //changeState(ePageState.mainNew);
                    //retrieveMainGridData();
                    //}
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
        }
        function doClear() {
        }
        function doPrint() {
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('BusinessInfo.aspx');
        }
        function goNext() {
            changeUrl('DocSetting.aspx');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-yellow">
        <ul id="tab0" class="nav nav-tabs first-row">
            <%--<li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('HardwareSetting.aspx');">Hardware Setting</a></li>--%>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('Roles.aspx');">Roles</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('User.aspx')">User</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('BusinessInfo.aspx');">Business Info</a></li>
            <li class="active yellow"><a href="#" data-toggle="tab" onclick="GoToUrl('FinancialSetting.aspx');">Financial Setting</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('DocSetting.aspx')">Doc Setting</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tabAddUser">
                <div class="section"><!-- Financial Setting section -->
                    <table class="form single">
                        <tr>
                            <td class="cell-header" colspan="2">
                                Financial Setting:
                            </td>
                        </tr>
				        <tr>
					        <td>Currency:</td>
                            <td>
                                <select id="selCurrency" class="width-200">
                                </select>
                            </td>
				        </tr>
                        <tr>
					        <td>Default Pricing:</td>
                            <td>
                                <select id="selPricing" class="width-200">
                                </select>
                            </td>
                        </tr>
			        </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section"><!--Tax Setting-->
                    <table class="form single">
                        <tr>
                            <td class="cell-header" colspan="2">
                                Tax Setting:
                            </td>
                        </tr>
                        <tr>
					        <td>Sales Tax:</td>
                            <td>
                                <select id="selTaxType" class="sel-tax">
                                </select>
                                <input type="text" id="txtTaxAmount" class="txt-tax" />
                                <span>%</span>
                            </td>
                        </tr>
			        </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section"><!--Item Setting-->
                    <table class="form single">
                        <tr>
                            <td class="cell-header" colspan="2">
                                Item Setting:
                            </td>
                        </tr>
                        <tr>
					        <td>Stock Item Control:</td>
                            <td>
                                <input type="checkbox" id="chkItemControl" />
                            </td>
                        </tr>
                        <tr>
					        <td>Costing Method:</td>
                            <td>
                                <select id="selCosting" class="width-200">
                                </select>
                            </td>
                        </tr>
			        </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div class="btn-nav" id="btnBackward" onclick="goBack();">&lt;</div>
                        <div class="btn-save" id="btnSave" onclick="doSave();">Save</div>
                        <div class="btn-void" id="btnVoid" onclick="doVoid();">Void</div>
                        <div class="btn-clear" id="btnClear" onclick="doClear();">Clear</div>
                        <div class="btn-print" id="btnPrint" onclick="doPrint();">Print</div>
                        <div class="btn-cancel" id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div class="btn-nav" id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!--subpage-->
</asp:Content>
