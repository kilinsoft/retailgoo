﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class BusinessInfo : System.Web.UI.Page
    {
        public string BusinessTypeListString { get; set; }
        public string CountryListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspBusinessType = Business.RetrieveBusinessTypeList();
            if (rspBusinessType.Success && rspBusinessType.ResponseItem != null && rspBusinessType.ResponseItem.Count > 0)
                BusinessTypeListString = HtmlSelectHelper.ConvertToListString(rspBusinessType.ResponseItem);
            //else
            //    BusinessTypeListString = "[{Value:-1,Text:'<anonymous>'}]";

            var rspCountry = Business.RetrieveCountryList();
            if (rspCountry.Success && rspCountry.ResponseItem != null && rspCountry.ResponseItem.Count > 0)
                CountryListString = HtmlSelectHelper.ConvertToListString(rspCountry.ResponseItem);
            //else
            //    CountryListString = "[{Value:-1,Text:'<anonymous>'}]";
        }
    }
}