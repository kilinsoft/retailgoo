﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RetailGOO.Util.Constant;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.BusinessLayer;

namespace RetailGOO.Page
{
    public partial class BusinessSelection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!AuthenticationHelper.IsAuthenticated())
            //{
            //    Response.Redirect(RetailGooPageUrl.LogInPage);
            //}

            if (!IsPostBack)
            {
                var resp = new GenericResponse<IList<BusinessSelectionDto>>();
                resp = Business.RetrieveAvailableBusinessesForUser();
                if (resp.Success && resp.ResponseItemCount != 0)
                {
                    selBusiness.Items.Clear();
                    foreach (var item in resp.ResponseItem)
                    {
                        string businessListString = String.Format("[{0}] {1} ({2})", item.BusinessId, item.BusinessName, Staff.GetRoleFullname(item.Role));
                        selBusiness.Items.Add(new ListItem(businessListString, item.BusinessId.ToString()));
                    }
                }
                else if (resp.Success && resp.ResponseItemCount == 0)
                {
                    selBusiness.Enabled = false;
                }
            }
        }

        protected void BtnSelectOnClick(object sender, EventArgs e)
        {
            if (selBusiness.SelectedItem == null)
                return;
            // Set BusinessId.
            HttpContext.Current.Session[SessionVariable.BusinessId] = long.Parse(selBusiness.SelectedItem.Value);
            HttpContext.Current.Session[SessionVariable.BusinessName] = selBusiness.SelectedItem.Text;

            var rspStaff = StaffAdapter.RetrieveCurrentStaffInfo();
            if (rspStaff.Success)
            {
                HttpContext.Current.Session[SessionVariable.Staff] = rspStaff.ResponseItem;
            }
            else
            {
                Identity.SignOut(SessionHelper.GetCurrentUsername());
                Response.Redirect(
                    string.Format("{0}?Value={1}",
                        RetailGooPageUrl.LogOutPage,
                        HttpUtility.UrlEncode(
                            string.Format("Security Violation. The user[ {0} ] does not belong to the business[ {1} ]",
                                SessionHelper.GetCurrentUsername(),
                                SessionHelper.GetCurrentBusinessId())
                        )
                    )
                );
            }
            Response.Redirect(RetailGooPageUrl.Dashboard);
        }
    }
}