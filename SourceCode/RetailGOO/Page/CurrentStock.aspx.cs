﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class CurrentStock : System.Web.UI.Page
    {
        public string ItemGroupListString { get; set; }
        public string ItemCategoryListString { get; set; }
        public string ItemBrandListString { get; set; }
        public int DocumentTemplate { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspItemGroup = ItemAdapter.RetrieveItemGroupList();
            if (rspItemGroup.Success && rspItemGroup.ResponseItem != null && rspItemGroup.ResponseItem.Count > 0)
                ItemGroupListString = HtmlSelectHelper.ConvertToListString(rspItemGroup.ResponseItem);
            //else
            //    ItemGroupListString = "[{Value:-1,Text:'--select--'}]";
            
            var rspItemCategory = ItemAdapter.RetrieveItemCategoryList();
            if (rspItemCategory.Success && rspItemCategory.ResponseItem != null && rspItemCategory.ResponseItem.Count > 0)
                ItemCategoryListString = HtmlSelectHelper.ConvertToListString(rspItemCategory.ResponseItem);
            //else
            //    ItemCategoryListString = "[{Value:-1,Text:'--select--'}]";

            var rspItemBrand = ItemAdapter.RetrieveItemBrandList();
            if (rspItemBrand.Success && rspItemBrand.ResponseItem != null && rspItemBrand.ResponseItem.Count > 0)
                ItemBrandListString = HtmlSelectHelper.ConvertToListString(rspItemBrand.ResponseItem);
            //else
            //    ItemBrandListString = "[{Value:-1,Text:'--select--'}]";

            DocumentTemplate = SessionHelper.BusinessInfo.DocSetting[BusinessScreenString.CurrentStock];
        }
    }
}