﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class ReceiveTransferStock : System.Web.UI.Page
    {
        public string ReceiveTransferStatusListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspReceiveTransferStatus = ItemAdapter.RetrieveReceiveItemTransferStatusList();
            if (rspReceiveTransferStatus.Success && rspReceiveTransferStatus.ResponseItem != null && rspReceiveTransferStatus.ResponseItem.Count > 0)
                ReceiveTransferStatusListString = HtmlSelectHelper.ConvertToListString(rspReceiveTransferStatus.ResponseItem);
            else
                ReceiveTransferStatusListString = "[]";
        }
    }
}