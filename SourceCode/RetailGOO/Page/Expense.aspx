﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" 
    CodeBehind="Expense.aspx.cs" Inherits="RetailGOO.Page.Expense" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .nav-tabs > li {
            width: 160px !important;
        }
    </style>
    <script type="text/javascript">

        // Control variables
        var g_amount, g_whtPercent, g_whtAmount;
        var g_selSupplier, g_selCategory;
        var g_docTemplate = <%=DocumentTemplate%>;

        // Data variables
        var g_supplierList = <%=SupplierListString%>;
        var g_categoryList = <%=ExpenseCategoryListString%>;
        var g_itemList = null;

        function pageLoad() {
               
            preventNavigationOnUnsaved(true);

            // Initialize variables

            //var selEditorOptions = {
            //    loadOnDemand: true,
            //    valueField: "ItemId",
            //    textField: "OptionName",
            //    getValueFromTextField: false,
            //    onTextRequested: onTextRequestedHandler,
            //    onLoad: onLoadHandler,
            //    //onChanged: onItemChangedHandler,
            //    options: []
            //};
            //var selFormattorOptions = {
            //    valueField: "ItemId",
            //    textField: "OptionName",
            //    onTextRequested: onTextRequestedHandler
            //};
            //var subGridColsDef = [
            //{ title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            //{ title: "PO", name: "PurchaseOrderNo", required: false, editable: false },
            //{ title: "Item", name: "OptionName", required: false, editable: false },
            //{ title: "Description", name: "Description", required: false, editable: false },
            //{ title: "PO Qty", name: "PoQuantity", required: false, editable: false, formatter: KGrid.Formatter.Amount(0) },
            //{ title: "Qty", name: "Quantity", required: true, editable: true, formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0) },
            //{ title: "UOM", name: "UnitOfMeasure", required: false, editable: false },
            //{ title: "Price", name: "Price", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) },     
            //{ title: "Amount", name: "Amount", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ////{ title: "Price", name: "Price", required: true, editable: true, formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) },     
            ////{ title: "Amount", name: "Amount", required: true, editable: true, formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) }
            //];
            //var subGridDef = {
            //    editable: false,
            //    enableAddBlankRow: false,
            //    onRowSelected: sgOnRowSelected,
            //    onRowChanged: amountChangeHandler
            //}
            //g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Category", name: "ExpenseCategoryName", required: false, editable: false },
            { title: "Notes", name: "Note", required: false, editable: false },
            { title: "Amount", name: "Amount", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                onRowMouseEnter: mgOnRowMouseEnter,
                onRowMouseLeave: mgOnRowMouseLeave
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            $("#txtDate").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            g_selSupplier = $("#selSupplier").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_supplierList
            })[0].selectize;

            g_selCategory = $("#selCategory").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                load: function(query, callback) {
                    retrieveAjaxDataAsync("Purchasing.svc", "RetrieveExpenseCategoryList", {},
                        function(items) {
                            callback(items);
                        },
                        function(txt) {
                            callback();
                            displayError(txt);
                        });
                }
            })[0].selectize;

            //initGridSorter(g_subGrid, "#selMainSelectedBy",
            //    [{ title: "PO", name: "PurchaseOrderNo", valueCompare: false }, 
            //    { title: "Item", name: "Date", valueCompare: false },
            //    { title: "PO Qty", name: "PoQuantity", valueCompare: true },
            //    { title: "Qty", name: "Quantity", valueCompare: true },
            //    { title: "Amount", name: "Total", valueCompare: true }]);
            initGridSorter(g_mainGrid, "#selSubSelectedBy",
                [{ title: "Date", name: "Date", numberComparison: false }, 
                { title: "Category", name: "CategoryName", valueCompare: false },
                { title: "Amount", name: "Amount", valueCompare: true }]);


            var funcOnAmountChanged = function ($e, val) {
                var amt = parseFloat(val);
                if ($("#chkWht").prop('checked')) {
                    var whtPercent = g_whtPercent.getValue();
                    if (whtPercent === "")
                        whtPercent = 0;

                    g_whtAmount.setValue(amt * whtPercent / 100);
                }
            };
            var funcOnWhtPercentChanged = function ($e, val) {
                var amt = g_amount.getValue();
                if (amt === "")
                    amt = 0;

                g_whtAmount.setValue(amt * val / 100);
            };
            g_amount = new Util.Input.Amount("#txtAmount", 2, funcOnAmountChanged);
            g_whtPercent = new Util.Input.Amount("#txtWhtPercent", 2, funcOnWhtPercentChanged);
            g_whtAmount = new Util.Input.Amount("#txtWhtAmount", 2);

            $("#chkWht").on('change', function () {
                if (this.checked) {
                    g_whtPercent.setValue(0);
                    g_whtAmount.setValue(0);
                    g_whtPercent.enable();
                }
                else {
                    g_whtPercent.setValue(0);
                    g_whtAmount.setValue(0);
                    g_whtPercent.disable();
                }
            });

            // Init elements' behaviors
            g_whtAmount.disable();
            g_whtPercent.disable();

            // Init element's values
            g_whtPercent.setValue(0);
            g_whtAmount.setValue(0);

            // Init state
            changeState(ePageState.mainNew);

            retrieveMainGridData();
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            //$("#txtNote").readOnly(true);
            g_selSupplier.disable();
            g_selCategory.disable();
            //$("#btnAddExpense").disable();
            $("#txtDate").prop('disabled', true);
            g_amount.disable();
            g_whtAmount.disable();
            g_whtPercent.disable();
            $("#chkWht").prop("disabled", true);
            //g_subtotal.disable();
            //g_tax.disable();
            //g_total.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            //$("#txtStatus").readOnly(true);
            //$("#txtRoNo").readOnly(true);
            $("#txtDate").prop('disabled', false);
            //$("#txtNote").readOnly(false);
            g_selSupplier.enable();
            g_selCategory.enable();
            g_amount.enable();
            $("#chkWht").prop("disabled", false);
            //$("#btnAddExpense").enable();
            //g_subtotal.enable();
            //g_tax.enable();
            //g_total.enable();
        }

        function setFormData(item) {
            g_selSupplier.clear();
            g_selSupplier.clearOptions();
            g_selSupplier.addOption({ Value: item["SupplierId"], Text: item["SupplierName"] });
            g_selSupplier.addOption(g_supplierList);
            g_selSupplier.setValue(item["SupplierId"]);
            g_selCategory.clear();
            g_selCategory.clearOptions();
            g_selCategory.addOption({ Value: item["ExpenseCategoryId"], Text: item["ExpenseCategoryName"] });
            g_selCategory.addOption(g_supplierList);
            g_selCategory.setValue(item["ExpenseCategoryId"]);
            $("#txtDate").val(item["Date"]);
            $("#txtNote").val(item["Note"]);
            g_amount.setValue(item["Amount"]);
            g_whtPercent.setValue(item["WhtPercent"]);
            g_whtAmount.setValue(item["WhtAmount"]);
            $("#chkWht").prop('checked', item["UseWht"]);
            showVoid(item["IsVoided"]);
        }
        function clearFormData() {
            g_selSupplier.clear();
            g_selSupplier.clearOptions();
            g_selSupplier.addOption(g_supplierList);
            g_selCategory.clear();
            g_selCategory.clearOptions();
            g_selCategory.addOption(g_categoryList);
            $("#tblForm input[type=text]").val("");
            $("#txtDate").datepicker("setDate", new Date());
            $("#chkWht").prop('checked', false);
        }

        //function retrieveSubGridData(id, funcOnSuccess) {
        //    retrieveAjaxDataSync("Purchasing.svc", "RetrieveReceiveOrderItem", id,
        //        function (rsp) {
        //            setGridData(g_subGrid, rsp.ResponseItem);
        //            if (funcOnSuccess != null)
        //                funcOnSuccess();
        //        });
        //}
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllExpense", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }

        function setGridData(grid, data) {
            if (grid === g_mainGrid) {
                g_mainItems = data;
                $("#selMainSelectedBy option:first-child").prop("selected", true);
            }
            else if (grid === g_subGrid) {
                g_subItems = data;
                $("#selSubSelectedBy option:first-child").prop("selected", true);
            }
            grid.setData(data);
        }

        function displayMainItemData(item) {
            setFormData(item);
            //retrieveSubGridData(item.Id);
        }
        //function setSubGridData(aoData) {
        //    g_subItems = aoData;
        //    setGridData(g_subGrid, aoData);
        //}
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        //function clearSubGrid() {
        //    setGridData(g_subGrid, []);
        //}
        function canDeleteMainItem(iRowIndex) {
            return true;
        }

        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            //updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    //clearSubGrid();
                    //subSectionUnlock();
                    mainSectionUnlock();
                    showVoid(false);
                    enableSaveClear();
                    break;
                case ePageState.mainView:
                    formLock();
                    //subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    //subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }

        // Grid controls
        //function subSectionLock() {
        //    g_subGrid.lock();
        //    //$("#btnSubEdit").disable(); //already active
        //    $("#btnSubDelete").disable();
        //    $("#selSubSelectedBy").disable();
        //}
        //function subSectionUnlock() {
        //    g_subGrid.unlock();
        //    //$("#btnSubEdit").enable(); //already active
        //    //$("#btnSubDelete").enable();
        //    $("#selSubSelectedBy").enable();
        //}
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function initGridSorter(grid, selId, options) {
            var $sel = $(""+selId);
            for (var i = 0; i < options.length; ++i)
                $sel.append($("<option></option>").attr("value", options[i]["name"]).text(options[i]["title"]));
            $sel.on("change.gridSort", function (e) {
                var val = $sel.val();
                var fValueCompare = getAssocValue(options, "name", val, "valueCompare"); 
                for (var i = grid.data.length - 1; i >= 0; --i)
                    if (grid.data[i].itemState == KGrid.ItemState.blankItem)
                        grid.data.splice(i, 1);
                grid.data.sort(sortByKey(val, fValueCompare));
                grid.invalidate();
                if (grid === g_mainGrid)
                    changeState(ePageState.mainNew);
            });
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if ($("#txtAmount").val() == "") {
                displayError("Please specify the amount");
                return false;
            }
            if (g_selSupplier.getValue() == "") {
                displayError("Please specify a vendor");
                return false;
            }
            if (g_selCategory.getValue() == "") {
                displayError("Please specify a category");
                return false;
            }
            if ($("#txtDate").val() == "") {
                displayError("Please specify the Date");
                return false;
            }
            if (!validateFloat(g_amount.getValue())) {
                displayError("Invalid Amount");
                return false;
            }
            if ($("#chkWht").prop('checked')) {
                if (!validateFloat(g_whtPercent.getValue())) {
                    displayError("Invalid Withholding Tax Percentage");
                    return false;
                }
                if (!validateFloat(g_whtAmount.getValue())) {
                    displayError("Invalid Withholding Tax Amount");
                    return false;
                }
            }
            return true;
        }
        //function validateSubItems() {
        //    for (var i = 0; i < g_subGrid.rows.length; ++i) {
        //        var row = g_subGrid.rows[i];
        //        if (row.rowState != KGrid.ItemState.blankItem &&
        //            row.rowState != KGrid.ItemState.originalItem) {
        //            if (!row.validate()) {
        //                displayError("A data on row [" + (i+1) + "] is missing or invalid");
        //                return false;
        //            }
        //        }
        //    }
        //    return true;
        //}
       
        function constructDtoItem() {
            var item = {};
            item["SupplierId"] = g_selSupplier.getValue();
            item["SupplierName"] = g_selSupplier.getOption(g_selSupplier.getValue())[0].innerText;
            item["ExpenseCategoryId"] = g_selCategory.getValue();
            item["ExpenseCategoryName"] = g_selCategory.getOption(g_selCategory.getValue())[0].innerText;
            item["Note"] = $("#txtNote").val();
            item["Date"] = convertToUTCDateTime($("#txtDate").val());
            item["Amount"] = parseFloat($("#txtAmount").val());
            item["UseWht"] = $("#chkWht").prop('checked');
            item["WhtPercent"] = isNaN(g_whtPercent.getValue()) ? 0 : g_whtPercent.getValue();
            item["WhtAmount"] = isNaN(g_whtAmount.getValue()) ? 0 : g_whtAmount.getValue();
            item["ItemState"] = KGrid.ItemState.newItem;
            item["Version"] = "";
            return item;
        }

        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            //if (!validateSubItems())
            //    return false;

            //var row = g_mainGrid.selectedRow;
            var item = constructDtoItem();

            sendAjaxUpdate( "Purchasing.svc", "ManageExpense", item,
                function(rsp) {
                    setPageDirty(false);
                    retrieveMainGridData();
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            ;
        }
        function doClear() {
            if (preventNavigationOnUnsaved() && isPageDirty())
                doDefaultClear();
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/ExpenseA.aspx";
            var docTemplateBUrl = "../Printable/ExpenseB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Expense", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('OutgoingPayment.aspx');
        }
        function goNext() {
            changeUrl('CashPurchase.aspx');
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem)
                    displayMainItemData(g_mainGrid.data[pRow.rowIndex]);
                changeState(ePageState.mainView);
                updateCommandButtons("main", pRow);
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function mgOnRowMouseEnter(pRow) {
            delayDisplayTooltip(displayTableView, pRow.data);
        }
        function mgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function displayTableView(value) {
            //$("#divDynamicTooltip").appendTo("body"); // So the div does not display relatively to its parent but the body instead
            var item = value;
            var tooltipContent;
            tooltipContent = 'Vender: ' + item['SupplierName'] + '</br>';
            tooltipContent += 'Note: ' + item['Note'];
            $('#divDynamicTooltip').html(tooltipContent);
            $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
        }
        //function sgOnRowSelected(pRow) {
        //    pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
        //    updateCommandButtons("sub", g_subGrid.selectedRow);
        //}
        //function sgOnItemChanged(pCell, val) {
        //}
        //function onTextRequestedHandler(pCell, pVal) {
        //    return KGrid.Function.getAssocValue(g_subItems, "ItemId", pVal, "OptionName");
        //}
        //function onLoadHandler(pCell, pCallback) {
        //    sendAjax(
        //        '../Service/ItemManagement.svc/RetrieveItem',
        //        {},
        //        function (res) {
        //            g_itemList = res.ResponseItem;
        //            pCallback(g_itemList);
        //        },
        //        function (res) {
        //            pCallback();
        //        });
        //}
        //function onItemChangedHandler(pCell, pVal) {
        //    var uom = KGrid.Function.getAssocValue(g_subItems, "ItemId", pVal, "UnitOfMeasure");
        //    var price = "";
        //    var optionName = "";
        //    var array = g_subItems; // find item in g_subItems before g_itemList
        //    if (uom == null)
        //        array = g_itemList;
        //    uom = KGrid.Function.getAssocValue(array, "ItemId", pVal, "UnitOfMeasure");
        //    optionName = KGrid.Function.getAssocValue(array,"ItemId", pVal, "OptionName");
        //    price = KGrid.Function.getAssocValue(array,"ItemId", pVal, "Price");
        //    if (uom == null) // Something has gone wrong, uom should be found.
        //        uom = "";
        //    var uomCell = pCell.parentRow.cells[4];
        //    var priceCell = pCell.parentRow.cells[5];
        //    uomCell.setValue(uom);
        //    priceCell.setValue(price == null ? "" : price);
        //    pCell.parentRow.data["OptionName"] = optionName;

        //    pCell.parentRow.triggerNextEditableCell(pCell);
        //}
        //function amountChangeHandler(pRow, pCell) {
        //    recalculateAmount(pCell);
        //    recalculateSummary();
        //}
        //function recalculateAmount(pCell) {
        //    var qtyCell = pCell.parentRow.cells[5];
        //    var qty = qtyCell.value;
        //    var priceCell = pCell.parentRow.cells[7];
        //    var price = priceCell.value;
        //    var amtCell = pCell.parentRow.cells[8];
        //    if (qty === "" || price === "")
        //        return;
        //    amtCell.setValue( parseFloat(qty) * parseFloat(price) );
        //}
        //function recalculateSummary() {
        //    var d = g_subGrid.data;
        //    var subtotal = 0;
        //    for (var i = 0; i < d.length; ++i) {
        //        var val = parseFloat(d[i]["Amount"]);
        //        if (!isNaN(val))
        //            subtotal += val;
        //    }
        //    g_subtotal.setValue(subtotal);
        //    var taxedAmount = parseFloat(g_taxFactor) * parseFloat(subtotal);
        //    g_tax.setValue(taxedAmount);
        //    g_total.setValue(taxedAmount + subtotal);
        //}
        function showVoid(fShow) {
            return true;
            //if (fShow)
            //    $("#tdVoid").empty().append("<div>VOIDED</div>");
            //else
            //    $("#tdVoid").empty();
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            $("#btnMainUndoDelete").hide();

            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                    //$("#btnPay").disable();
                    //$("#btnMail").disable();
                    //$("#btnReceive").disable();
                }
                else {
                    //var rowData = pRow.data;
                    //var fVoid = rowData["IsVoided"];
                    //if (fVoid) {
                    //    $("#btnMainEdit").disable();
                    //    $("#btnMainDelete").enable();
                    //    $("#btnPay").disable();
                    //    //$("#btnMail").disable();
                    //    //$("#btnReceive").disable();
                    //} else {
                        $("#btnMainEdit").disable();
                        $("#btnMainDelete").enable();
                        //$("#btnPay").enable();
                        //$("#btnMail").enable();
                        //$("#btnReceive").enable();
                    //}
                }
            }
            //else { // subGrid
            //    if (pRow == null) {
            //        $("#btnSubEdit").disable();
            //        $("#btnSubDelete").show();
            //        $("#btnSubDelete").disable();
            //        $("#btnSubUndoDelete").hide();
            //    } else {
            //        if (pRow.readOnly)
            //            $("#btnSubEdit").enable();
            //        else
            //            $("#btnSubEdit").disable();
            //        if (pRow.rowState == KGrid.ItemState.deletedItem) {
            //            $("#btnSubDelete").hide();
            //            $("#btnSubUndoDelete").show();
            //        }
            //        else if (pRow.rowState == KGrid.ItemState.blankItem) {
            //            $("#btnSubDelete").show();
            //            $("#btnSubDelete").disable();
            //            $("#btnSubUndoDelete").hide();
            //        }
            //        else {
            //            $("#btnSubDelete").show();
            //            $("#btnSubDelete").enable();
            //            $("#btnSubUndoDelete").hide();
            //        }
            //    }
            //}
        }

        //function onSubEdit() {
        //    var selectedRow = g_subGrid.selectedRow;
        //    if (selectedRow == null)
        //        return;
        //    selectedRow.setReadOnly(false);
        //    selectedRow.triggerNextRequiredCell();
        //    $("#btnSubEdit").disable();
        //}
        //function onSubDelete() {
        //    var selectedRow = g_subGrid.selectedRow;
        //    if (selectedRow == null)
        //        return;
        //    if (!canDeleteSubItem(selectedRow.rowIndex))
        //        return;
        //    if (selectedRow.rowState == KGrid.ItemState.newItem) {
        //        g_subGrid.clearSelection();
        //    }
        //    else
        //        $("#btnSubUndoDelete").show();
            
        //    $("#btnSubEdit").disable();
        //    $("#btnSubDelete").hide();
        //    selectedRow.delete();
        //}
        function onMainUndoDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnMainDelete").show();
            $("#btnMainUndoDelete").hide();
        }
        function onMainEdit() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            $("#btnMainEdit").disable();
        }
        function onMainDelete() {
            //var selectedRow = g_mainGrid.selectedRow;
            //if (selectedRow == null)
            //    return;
            //if (!canDeleteMainItem(selectedRow.rowIndex))
            //    return;
            //if (selectedRow.rowState == KGrid.ItemState.newItem) {
            //    g_mainGrid.clearSelection();
            //}
            //else
            //    $("#btnMainUndoDelete").show();

            //$("#btnMainEdit").disable();
            //$("#btnMainDelete").hide();
            //selectedRow.delete();
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();
            if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?")) {
                var data = selectedRow.data;
                var item = {};
                item["ExpenseId"] = data["ExpenseId"];
                item["Version"] = data["Version"];
                item["ItemState"] = KGrid.ItemState.deletedItem;
                //item["Items"] = [];

                sendAjaxUpdate("Purchasing.svc", "ManageExpense", item,
                    function (rsp) {
                        setPageDirty(false);
                        retrieveMainGridData();
                        changeState(ePageState.mainNew);
                    },
                    function (rsp) {
                        displayError(rsp.StatusText);
                    });
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-green">
        <ul class="nav nav-tabs">
            <li class="inactive"><a href="#tab1" data-toggle="tab" onclick="changeUrl('CashPurchase.aspx');">Cash Purchase</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('PurchaseOrder.aspx');">Purchase Order</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveOrder.aspx');">Receive Order</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('OutgoingPayment.aspx')">Outgoing Payment</a></li>
            <li class="active"><a href="#" data-toggle="tab">Expense</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <!-- Form Pane -->
                <div id="divForm" class="section">
                    <table id="tblForm" class="form single">
                        <tbody>
                            <tr>
                                <td>Amount:</td>
                                <td>
                                    <input type="text" id="txtAmount" />
                                </td>
                            </tr>
                            <tr>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate" style="width: 230px;" />
                                    <img src="../images/icon_calendar.png" />
                                </td>
                            </tr>
                            <tr>
                                <td>Vendor:</td>
                                <td>
                                    <select id="selSupplier" placeholder="-- search --"></select>
                                </td>
                            </tr>
                            <tr>
                                <td>Category:</td>
                                <td>
                                    <select id="selCategory" placeholder="-- search --"></select>
                                </td>
                            </tr>
                            <tr>
                                <td>Notes:</td>
                                <td>
                                    <input type="text" id="txtNote" />
                                </td>
                            </tr>
                            <tr>
                                <td>Withholding Tax:</td>
                                <td style="vertical-align: middle;">
                                    <input type="checkbox" id="chkWht" />
                                    <input type="text" id="txtWhtPercent" style="width: 50px;" />%
                                    <input type="text" id="txtWhtAmount" style="width: 170px;" />
                                </td>
                            </tr>
<%--                            <tr>
                                <td></td>
                                <td style="text-align: right;">
                                     <input type="button" id="btnAddExpense" class="btn-action-big btn-green" value="Add Expense" onclick="addExpenseToGrid();" />
                                </td>
                            </tr>--%>
                        </tbody>
                    </table>
                </div> <!-- Form Pane -->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="header-search">Search</div>
                    <div class="grid-command search">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                            <input type="button" id="btnMainUndoDelete" class="btn-action" value="Undo" onclick="onMainUndoDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>
