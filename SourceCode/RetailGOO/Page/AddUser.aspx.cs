﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class AddUser : System.Web.UI.Page
    {
        public string RoleListString { get; set; }
        public string StatusListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspRoleList = Business.RetrieveRoleList();
            if (rspRoleList.Success && rspRoleList.ResponseItem != null && rspRoleList.ResponseItem.Count > 0)
                RoleListString = HtmlSelectHelper.ConvertToListString(rspRoleList.ResponseItem);
            else
                RoleListString = "[{Value:-1,Text:'<blank>'}]";

            var rspStatusList = Business.RetrieveStaffStatusList();
            if (rspStatusList.Success && rspStatusList.ResponseItem != null && rspStatusList.ResponseItem.Count > 0)
                StatusListString = HtmlSelectHelper.ConvertToListString(rspStatusList.ResponseItem);
            else
                StatusListString = "[{Value:-1,Text:'<blank>'}]";
        }
    }
}