﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="CountSheet.aspx.cs" Inherits="RetailGOO.Page.CountSheet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>

    </style>
    <script type="text/javascript">
        // Variables declaration
        function pageLoad() {

            preventNavigationOnUnsaved();

            // Initialize behaviors
            $("#grid1").kilinGrid({
                onRowSelected: onRowSelected
            });
            $("#grid2").kilinGrid({
                onRowSelected: onRowSelected
            });

            // Ajax calls to initialize mandatory variables
        }

        function onItemSelectGrid1() {
        }

        function btnChooseFileOnClick() {
        }

        function Save() {
        }
        function Void() {
        }
        function Clear() {
        }
        function Print() {
        }
        function Cancel() {
        }
        function GoBack() {
        }
        function GoNext() {
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-red">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('AdjustStock.aspx');">Adjust Stock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('TransferStock.aspx');">Transfer Stock</a></li>
            <li class="inactive" style="width: 304px;"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveTransferStock.aspx')">Receive TransferStock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReorderStock.aspx');">Reorder Stock</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ItemManagement.aspx');">ItemManagement</a></li>
            <li class="inactive" style="width: 184px;"><a href="#" data-toggle="tab" onclick="changeUrl('CategoryManagement.aspx');">Category Management</a></li>
            <li class="inactive" style="width: 180px;"><a href="#" data-toggle="tab" onclick="changeUrl('BrandManagement.aspx')">Brand Management</a></li>
            <li class="active" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CountSheet.aspx');">Count Sheet</a></li>
            <li class="inactive" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CurrentStock.aspx');">Current Stock</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table class="form single">
                        <tr>
                            <td>Date:</td>
                            <td>
                                <input type="text" id="txtDate" />
                            </td>
                        </tr>
                        <tr>
                            <td>Category Name:</td>
                            <td>
                                <select id="selCategory">
                                    <option value="0">Select Category</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Brand Name:</td>
                            <td>
                                <select id="selBrand">
                                    <option value="0">Select Brand</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Item Name:</td>
                            <td>
                                <select id="selItem">
                                    <option value="0">Select Item</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>

            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
</asp:Content>


