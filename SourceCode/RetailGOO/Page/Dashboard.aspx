﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="RetailGOO.Page.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <link rel="stylesheet" href="<%# ResolveClientUrl("~/css/dashboard.css") %>" type="text/css" />
    <script type="text/javascript">
        function pageLoad() {

            retrieveOutstandingItemCount();
            retrieveTop5Items();
        }
        function retrieveOutstandingItemCount() {
            retrieveAjaxDataSync("ReportService.svc", "RetrieveOutstandingItemCount", null,
                function (item) {
                    if (item == null)
                        return;
                    var itemCountNums = item.split(","); // Must be 6 numbers concatenated as A,B,C,D,E,F
                    $('#lblPendingSI').html(itemCountNums[0]);
                    $('#lblNumSIOverdue').html(itemCountNums[1]);
                    $('#lblNumROItem').html(itemCountNums[2]);
                    $('#lblNumPendingPO').html(itemCountNums[3]);
                    $('#lblNumPOOverdue').html(itemCountNums[4]);
                    $('#lblPendingRO').html(itemCountNums[5]);

                    // Update the outstanding pane

                });
        }
        function retrieveTop5Items() {
            retrieveAjaxDataSync("ReportService.svc", "RetrieveTop5Items", null,
                function (items) {
                    if (items == null)
                        return;
                    $("#divTopProducts").empty();

                    for (var i = 0; i < items.length; ++i) {
                        var item = items[i];
                        var $divProduct = $("<div></div>");
                        $divProduct.addClass("product-item");
                        var $divProductName = $("<div></div>");
                        $divProductName.addClass("item-caption");
                        var $divAmtSum = $("<div></div>");
                        $divAmtSum.addClass("item-amount-sum");

                        $divAmtSum.html(convertToAmount(item["Quantity"], 2));
                        $divProductName.html(item["ItemFullName"]);
                        $divProduct.append($divProductName);
                        $divProduct.append($divAmtSum);
                        $("#divTopProducts").append($divProduct);
                    }

                });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
<%--    <div class="subpage">
        <img src="../images/temp_dashboard.png" style="margin: 0px auto;"></img>
    </div>--%>
        <div class="report-main">
        <div class="report-main-inner">
            <!-- Chart pane -->
            <div class="pane-chart section">
                Your browser does not support the chart feature at this moment.
            </div>
            <div class="section">
                <div class="line"></div>
            </div>
            <!-- Summary pane -->
            <div class="pane-summary section">
                <div class="summary-outstanding">
                    <div class="caption-outstanding">Outstanding</div>
                    <div class="pane-outstanding">
                        <div class="cell-outstanding">
                            <div id="lblPendingSI" style="background-color: #a5d9e6">0</div>
                            <div>Sales Orders</div>
                        </div>
                        <div class="cell-outstanding">
                            <div id="lblNumSIOverdue" style="background-color: #fdda76">0</div>
                            <div>Customer<br />Inv. Overdue</div>
                        </div>
                        <div class="cell-outstanding row-end">
                            <div id="lblNumROItem" style="background-color: #b0d3c0">0</div>
                            <div>Re-order Product</div>
                        </div>
                        <div class="cell-outstanding">
                            <div id="lblNumPendingPO" style="background-color: #f6b7ae">0</div>
                            <div>Purchase Orders</div>
                        </div>
                        <div class="cell-outstanding">
                            <div id="lblNumPOOverdue" style="background-color: #d0c3d7">0</div>
                            <div>Purchase Orders<br />Overdue</div>
                        </div>
                        <div class="cell-outstanding">
                            <div id="lblPendingRO" style="background-color: #7aa1c0">0</div>
                            <div>Unpaid<br />Supplier Inv.</div>
                        </div>
                    </div>
                </div>
                <div class="summary-verticalbar"></div>
                <div class="summary-topproduct">
                    <div class="caption-topproduct">TOP 5 PRODUCTS</div>
                    <div id="divTopProducts" class="pane-topproduct-list">
    <%--                    <div class="product-item">
                            <div class="item-caption">01. Item A</div>
                            <div class="item-amount-sum">19,000.00</div>
                        </div>
                        <div class="product-item">
                            <div class="item-caption">01. Item A</div>
                            <div class="item-amount-sum">19,000.00</div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
