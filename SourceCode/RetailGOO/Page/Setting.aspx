﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="Setting.aspx.cs" Inherits="RetailGOO.Page.Setting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <script type="text/javascript">
        function pageLoad()
        {
            document.title = "Retail GOO - Setting";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <ul id="tab" class="nav nav-tabs">
        <li class="blue active"><a href="#tabBusinessInfo" data-toggle="tab">Busines Info</a></li>
        <li class="yellow"><a href="#tabFinancialSetting" data-toggle="tab">Financial Setting</a></li>
        <li class="red"><a href="#tabDocSetting" data-toggle="tab">Doc Setting</a></li>
        <li class="green"><a href="#tabHardwareSetting" data-toggle="tab">Hardware Setting</a></li>
        <li class="purple"><a href="#tabRoles" data-toggle="tab">Roles</a></li>
        <li class="red"><a href="#tabUsers" data-toggle="tab">Users</a></li>
    </ul>
    <div class="blue tab-content">
        <div class="tab-pane fade in active" id="tabAddUser">
        </div>
    </div>
</asp:Content>
