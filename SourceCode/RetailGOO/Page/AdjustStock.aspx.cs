﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class AdjustStock : System.Web.UI.Page
    {
        public string ReasonListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspReason = ItemAdapter.RetrieveItemAdjustmentReasonList();
            if (rspReason.Success && rspReason.ResponseItem != null && rspReason.ResponseItem.Count > 0)
                ReasonListString = HtmlSelectHelper.ConvertToListString(rspReason.ResponseItem);
            else
                ReasonListString = "[{Value:-1,Text:'<anonymous>'}]";

        }
    }
}