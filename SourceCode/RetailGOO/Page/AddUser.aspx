﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="RetailGOO.Page.AddUser" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .table-stafflist {
            width: 100%;
        }
    </style>
    <script type="text/javascript">

        var g_roleList = <%=RoleListString%>;
        var g_statusList = <%=StatusListString%>;
        var g_statusListForWaiting = [{ Value: 'waiting', Text: 'waiting' }];
        var g_statusListForDeclined = [{ Value: 'declined', Text: 'declined' }];
        var g_statusListForNormal = [{ Value: 'active', Text: 'active' }, { Value: 'inactive', Text: 'inactive' }];
        var g_selStatusEditorOption = null;

        var g_mainGrid = null;
        var g_selRole, g_selUserSearch;

        function pageLoad() {

            var selRoleEditorOptions = {
                //create: true,
                loadOnDemand: false,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onRoleTextRequestedHandler,
                options: g_roleList
            };
            var selRoleFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onRoleTextRequestedHandler
            };
            g_selStatusEditorOption = {
                //create: true,
                loadOnDemand: false,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onStatusTextRequestedHandler,
                options: g_statusList
            };
            var selStatusFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onStatusTextRequestedHandler
            };
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "User", name: "Username", required: false, editable: false },
            //{ title: "Email", name: "Email", required: false, editable: false },
            { title: "Role", name: "RoleShortName", required: true, editable: true,
                editor: new KGrid.Editor.Selection(selRoleEditorOptions), formatter: new KGrid.Formatter.Selection(selRoleFormattorOptions),
                onChanged: onRoleItemChangedHandler
            },
            { title: "Status", name: "Status", required: true, editable: true,
                editor: new KGrid.Editor.Selection(g_selStatusEditorOption), formatter: new KGrid.Formatter.Selection(selStatusFormattorOptions),
                onChanged: onStatusItemChangedHandler
            },
            ];
            var mainGridDef = {
                editable: true,
                enableAddBlankRow: false,
                onRowSelected: onRowSelected,
                onRowDeselected: onRowDeselected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            g_selUserSearch = $("#txtSearchKeyword").selectize({
                create: true, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                maxItems: 1,
                load: function(query, callback) {
                    if (!query.length || $.trim(query).length < 3) 
                        return callback();
                    query = $.trim(query);
                    retrieveAjaxDataAsync(
                        "BusinessService.svc", "SearchUserByEmail", query,
                        function (items) {
                            callback(items);
                            //recalculateQtyAfter(pCell, pVal);
                        },
                        function (txt) { // error
                            callback();
                        });
                },
                onChange: function (val) {
                    //g_selRole.enable();
                }
            })[0].selectize;

            g_selRole = $("#selRole").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_roleList
            })[0].selectize;
            g_selRole.setValue("sal"); // Default Role to Sales

            // Initialize elements' behaviors
            g_mainGrid.lock();
            //g_selRole.disable();
            $("#btnDelete").disable();

            //$("#btnSave").addClass("disable").disable();
            $("#btnVoid").addClass("disable").disable();
            $("#btnPrint").addClass("disable").disable();
            $("#btnClear").addClass("disable").disable();

            retrieveMainGridData(null);
        }

        function changeState(pageState) {
            g_pageState = pageState;
            switch (g_pageState) {
                case ePageState.mainView:
                    $("#btnEdit").enable();
                    g_mainGrid.lock();
                    break;
                case ePageState.mainEdit:
                    $("#btnEdit").disable();
                    g_mainGrid.unlock();
                    break;
            }
        }

        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveAllBusinessStaff", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                });
        }

        function onSendInvitation() {
            var targetUser = $.trim(g_selUserSearch.getValue());
            // If there is a selectedRow form the mainGrid, use the selected user for the sendInvitiation (The admin/manager might want to resend invitation email to existing staff)
            var pRow = g_mainGrid.selectedRow;            
            if (pRow != null && targetUser == "") {
                var status = pRow.data["Status"];
                if (status != "waiting" && status != "declined") {
                    displayError("Can only send invitation to staff with status=[waiting] or status=[declined]");
                    return;
                }
            } else {
                // Check if a new email has been entered of the user selects another existing user.
                if (!isIntNumber(targetUser)) {
                    // This means an email has been entered
                    if (!validateEmail(targetUser)) {
                        displayError("Invalid Email");
                        return;
                    }
                }
            }

            var item = constructDtoItem();
            
            sendAjaxUpdate( "BusinessService.svc", "SendInvitation", item,
                function(rsp) {
                    location.reload();
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function onRowSelected(pRow) {
            activateRadioOnSelected(pRow);

            if (g_mainGrid.selectedRow != null)
                $("#btnDelete").enable();

            // Update available status list so that if the Staff Status is 'waiting', the available statuses to select will be only [waiting];
            var gridCache = g_mainGrid.cacheSelection;
            if (gridCache == undefined || gridCache == null) {
                g_mainGrid.cacheSelection = {};
                gridCache = g_mainGrid.cacheSelection;
            }
            var staffCurrentStatus = pRow.data['Status'];
            var gridCacheStatus = gridCache["Status"];
            if (staffCurrentStatus == 'waiting') {
                gridCacheStatus = $.extend(true, [], g_statusListForWaiting);
            }
            else if (staffCurrentStatus == 'declined') {
                gridCacheStatus = $.extend(true, [], g_statusListForDeclined);
            }
            else {
                gridCacheStatus = $.extend(true, [], g_statusListForNormal);
            }
        }
        function onRowDeselected(pRow) {
            activateRadioOnDeselected(pRow);

            if (g_mainGrid.selectedRow != null)
                $("#btnDelete").disable();
        }
        function onItemSelect() {
        }
        function onRoleTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_roleList, "Value", pVal, "Text");
        }
        function onStatusTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_statusList, "Value", pVal, "Text");
        }
        function onRoleItemChangedHandler(pCell, pVal) {
            var pRow = g_mainGrid.selectedRow;
        }
        function onStatusItemChangedHandler(pCell, pVal) {
            var pRow = g_mainGrid.selectedRow;
        }
   
        function onBtnEditClicked() {
            changeState(ePageState.mainEdit);
        }

        function onBtnDeleteClicked() {
            var pRow = g_mainGrid.selectedRow;
            if (pRow == null) {
                displayError("Select a staff first");
                return;
            }
            if (pRow.data["Status"] != "inactive") {
                displayError("Can only delete a staff with status=\"Inactive\"");
                return;
            }
            if (pRow.rowState != KGrid.ItemState.originalItem){
                displayError("Please save the changes first");
                //return;
            }

            pRow.delete();
        }

        function constructDtoItem() {
            var item = {};
            var pRow = g_mainGrid.selectedRow;
            if (pRow != null) { // SendInvitation email again to selectedStaff from the mainGrid
                item["UserId"] = pRow.data["UserId"];
                item["StaffId"] = pRow.data["StaffId"];
                item["Target"] = pRow.data["Email"];
                item["Role"] = pRow.data["Role"];
                item["ItemState"] = KGrid.ItemState.originalItem;
            } else {
                var fExistingUser = false;
                if (isIntNumber(g_selUserSearch.getValue()))
                    fExistingUser = true;

                item["UserId"] = fExistingUser ? g_selUserSearch.getValue() : -1;
                item["StaffId"] = -1;
                item["Target"] = g_selUserSearch.getValue().toLowerCase();
                item["Role"] = g_selRole.getValue();
                item["ItemState"] = KGrid.ItemState.newItem;
            }

            return item;
        }

        function constructStaffListDtoItem() {
            var items = [];
            for (var i = 0; i < g_mainGrid.rows.length; ++i) {
                var pRow = g_mainGrid.rows[i];
                if (pRow.rowState == KGrid.ItemState.modifiedItem || pRow.rowState == KGrid.ItemState.deletedItem) {
                    var item = {};
                    item["StaffId"] = pRow.data["StaffId"];
                    item["Role"] = pRow.data["RoleShortName"];
                    item["Status"] = pRow.data["Status"];
                    item["ItemState"] = pRow.rowState;
                    items.push(item);
                }
            }
            return items;
        }
        
        function Save() {
            var items = constructStaffListDtoItem();
            if (items.length == 0) 
                return;

            sendAjaxUpdate( "BusinessService.svc", "UpdateStaffRoleAndStatus", items,
                function(rsp) {
                    location.reload();
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-blue">
        <ul id="tab" class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Add User</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tabAddUser">
                <div id="divInviteFriends" class="section">
                    <table class="form single">
                        <tr>
                            <td class="cell-header" colspan="2">Invite Friends: (Admin Only)</td>
                        </tr>
                        <tr>
                            <td>Username / Email:</td>
                            <td>
                                <input type="text" id="txtSearchKeyword" placeholder="-- type keyword --" class="search width-350" />
                            </td>
                        </tr>
                        <tr>
                            <td>Role:</td>
                            <td>
                                <select id="selRole" class="width-350"></select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="button" id="btnSendInvitation" class="btn-action-big btn-blue" value="Send Invitation" onclick="onSendInvitation();">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div id="divUserList" class="section">
                    <div>
                        <table class="form single table-stafflist">
                            <tr>
                                <td class="cell-header" style="text-align: left;">User List: (Admin Only)
                                </td>
                                <td style="text-align: right;">
                                    <input type="button" id="btnEdit" value="Edit" class="btn-action txt-blue" onclick="onBtnEditClicked();"/>
                                    <input type="button" id="btnDelete" value="Delete" class="btn-action txt-blue" onclick="onBtnDeleteClicked();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 10px;">
                        <div id="mainGrid" class="" style="height: 180px;">
                        </div>
                    </div>
                </div>
                <div class="common-control">
                    <div>
                        <div id="btnBackward" onclick="GoBack();">&lt;</div>
                        <div id="btnSave" onclick="Save();">Save</div>
                        <div id="btnVoid" onclick="Void();">Void</div>
                        <div id="btnClear" onclick="Clear();">Clear</div>
                        <div id="btnPrint" onclick="Print();">Print</div>
                        <div id="btnCancel" onclick="doDefaultCancel();">Cancel</div>
                        <div id="btnForward" onclick="GoNext();">&gt;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
