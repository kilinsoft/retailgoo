﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class Expense : System.Web.UI.Page
    {
        public string SupplierListString { get; set; }
        public string ExpenseCategoryListString { get; set; }
        public string TaxFactor { get; set; }
        public int DocumentTemplate { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspSupplier = SupplierAdapter.RetrieveSupplierList();
            if (rspSupplier.Success && rspSupplier.ResponseItem != null && rspSupplier.ResponseItem.Count > 0)
                SupplierListString = HtmlSelectHelper.ConvertToListString(rspSupplier.ResponseItem);
            else
                SupplierListString = "[{Value:-1,Text:'<anonymous>'}]";

            var rspExpenseCategoryType = PurchaseAdapter.RetrieveExpenseCategoryList();
            if (rspExpenseCategoryType.Success && rspExpenseCategoryType.ResponseItem != null && rspExpenseCategoryType.ResponseItem.Count > 0)
                ExpenseCategoryListString = HtmlSelectHelper.ConvertToListString(rspExpenseCategoryType.ResponseItem);
            else
            {
                ExpenseCategoryListString = "[]";
                Logger.SessionFatal("Could not retrieve ExpenseCategoryList. Check database schema or data retrieval process");
            }

            TaxFactor = SessionHelper.BusinessInfo.Tax.ToString();
            DocumentTemplate = SessionHelper.BusinessInfo.DocSetting[BusinessScreenString.PurchaseOrder];
        }
    }
}