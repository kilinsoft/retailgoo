﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="PurchaseOrder.aspx.cs" Inherits="RetailGOO.Page.PurchaseOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .nav-tabs > li {
            width: 160px !important;
        }
    </style>
    <link rel="stylesheet" href="<%# ResolveClientUrl("~/css/jquery-ui-1.10.4.custom.min.css") %>" type="text/css" />
    <script src="<%# ResolveClientUrl("~/js/jquery-ui-1.10.4.custom.min.js") %>"></script>
    <script src="<%# ResolveClientUrl("~/js/newitemdialog.js") %>"></script>
    <script type="text/javascript">

        // Control variables
        var g_subtotal, g_tax, g_total;
        var g_selSupplier;
        var g_selPaymentType;
        var g_docTemplate = <%=DocumentTemplate%>;

        // Data variables
        var g_supplierList = <%=SupplierListString%>;
        var g_paymentTypeList = <%=PaymentTypeListString%>;
        var g_taxFactor = <%=TaxFactor%>;
        var g_itemList = null;
        var g_cell = null;
        
        // Support pagination
        var g_pageIndex = 0;
        var g_mainGridSortingType = "DateCreated";
        var RECORD_PER_PAGE = 10;

        // Support main search
        var g_mainSearch = null;

        function pageLoad() {
               
            preventNavigationOnUnsaved(true);

            // Initialize variables
            var selEditorOptions = {
                //create: true,
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                onLoad: onLoadHandler,
                options: [],
                create: function(input, callback) {
                    doNewItemDialog(input);
                }
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Item", name: "ItemId", width: 300, required: true, editable: true, 
                editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions),
                onChanged: onItemChangedHandler
            },
            //{ title: "Description", name: "Description", required: false, editable: true },
            { title: "Qty", name: "Quantity", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0), 
                onChanged: recalculateAmount },
            { title: "UOM", name: "UnitOfMeasure", required: false, editable: false },
            { title: "Price", name: "Price", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2), 
                onChanged: recalculateAmount },     
            { title: "Amount", name: "Amount", required: true, editable: false, formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) }
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected,
                onRowMouseEnter: sgOnRowMouseEnter,
                onRowMouseLeave: sgOnRowMouseLeave
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Number", name: "PurchaseOrderNo", required: false, editable: false },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Status", name: "Status", required: false, editable: false },
            { title: "Supplier Name", name: "SupplierName", required: false, editable: false },
            { title: "Amount", name: "Total", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                displayPagination: true,
                rowPerPage: RECORD_PER_PAGE,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                //onRowMouseEnter: mgOnRowMouseEnter,
                //onRowMouseLeave: mgOnRowMouseLeave,
                onPageSelected: mgOnPageSelected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            $("#txtDate").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);
            $("#txtDateDelivery").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            g_selSupplier = $("#selSupplier").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_supplierList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveSupplierInfo(val);
                    }
                }
            })[0].selectize;

            g_selPaymentType = $("#selPaymentType").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_paymentTypeList
            })[0].selectize;

            g_mainSearch = new MainSearch(
                $("#txtMainSearch"), // jQuery for main search textbox from the MasterPage
                $("#btnMainSearch"), // jQuery for the search button from the MasterPage
                onMainSearchQuery   // callback function to be activated when user enters query text
            );

            // Summary calculation
            var funcOnTaxChanged = function($e, val) {
                var tax = parseFloat(val);
                var subtotal = parseFloat(g_subtotal.getValue());
                var total = tax + subtotal;
                g_total.setValue(total);
            };
            var funcOnSubtotalChanged = function($e, val) { 
                var subtotal = parseFloat(val);
                var taxedAmount = parseFloat(g_taxFactor) * subtotal;
                g_tax.setValue(taxedAmount);
                funcOnTaxChanged(g_tax.$element, taxedAmount); // Call onTaxChanged next
            };
            g_subtotal = new Util.Input.Amount("#txtSubtotal", 2, funcOnSubtotalChanged);
            g_tax = new Util.Input.Amount("#txtTax", 2, funcOnTaxChanged);
            g_total = new Util.Input.Amount("#txtTotal", 2);

            initGridSorter(g_subGrid, "#selSubSelectedBy",
                [{ title: "Item", name: "ItemFullName", numberComparison: false }, 
                { title: "Qty", name: "Quantity", valueCompare: true },
                { title: "UOM", name: "UnitOfMeasure", valueCompare: false },
                { title: "Price", name: "Price", valueCompare: true },
                { title: "Amount", name: "Amount", valueCompare: true }]);
            initGridSorterAjax(
                "#selMainSelectedBy",
                //[{ title: "Number", name: "PurchaseOrderNo", valueCompare: false }, 
                //{ title: "Date", name: "Date", valueCompare: false },
                //{ title: "Status", name: "Status", valueCompare: false },
                //{ title: "Supplier Name", name: "SupplierName", valueCompare: false },
                //{ title: "Amount", name: "Total", valueCompare: true }]);
                [
                    { title: "New Item", name: "DateCreated", valueCompare: false }, 
                    { title: "Recent Updated", name: "DateModified", valueCompare: false }
                ],
                function (sortingType) {
                    g_mainGridSortingType = sortingType;
                    retrieveMainGridData();
                });


            // Init elements' behaviors
            $("#txtStatus").readOnly(true);
            $("#btnSubEdit").disable();

            // Init element's values

            // Init state
            changeState(ePageState.mainNew);

            // Behaviors
            g_subtotal.disable();
            //g_tax.disable();
            g_total.disable();

            retrieveMainGridData();
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#txtNote").readOnly(true);
            g_selSupplier.disable();
            g_selPaymentType.disable();
            $("#txtDate, #txtDateDelivery").prop('disabled', true);
            g_subtotal.disable();
            g_tax.disable();
            g_total.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtStatus").readOnly(true);
            $("#txtPoNo").readOnly(true);
            $("#txtDate, #txtDateDelivery").prop('disabled', false);
            $("#txtNote").readOnly(false);
            g_selSupplier.enable();
            g_selPaymentType.enable();
            //g_subtotal.enable(); // disable Subtotal
            g_tax.enable();
            //g_total.enable(); // disable total
        }

        function setFormData(item) {
            g_selSupplier.setValue(item["SupplierId"]);
            $("#txtRef").val(item["Reference"]);
            $("#txtContactPerson").val(item["ContactPerson"]);
            $("#txtPoNo").val(item["PurchaseOrderNo"]);
            $("#txtDate").datepicker("setDate", new Date(item["Date"]));
            $("#txtDateDelivery").datepicker("setDate", new Date(item["DateDelivery"]));
            $("#txtAddr1").val(item["Address1"]);
            $("#txtAddr2").val(item["Address2"]);
            $("#txtAddr3").val(item["Address3"]);
            $("#txtStatus").val(item["Status"]);
            $("#txtPhone").val(item["Phone"]);
            g_selPaymentType.setValue(item["PaymentTypeId"]);
            $("#txtCreditTerm").val(item["CreditTerm"]);
            $("#txtNote").val(item["Note"]);
            showVoid(item["Status"].toLowerCase() == "void");

            g_subtotal.setValue(item["Subtotal"]);
            g_tax.setValue(item["Tax"]);
            g_total.setValue(item["Total"]);
        }
        function clearFormData() {
            if (g_supplierList.length > 0)
                g_selSupplier.setValue(-1);
            $("#tblForm input[type=text]").val("");
            $("#txtStatus").val("New");
            $("#txtNote").val("");
            $("#txtDate").datepicker("setDate", new Date());
            $("#txtDateDelivery").datepicker("setDate", new Date());
            if (g_paymentTypeList.length > 0)
                g_selPaymentType.setValue(g_paymentTypeList[0]["Value"]);

            g_subtotal.setValue(0);
            g_tax.setValue(0);
            g_total.setValue(0);
        }

        function retrieveSubGridData(purchaseOrderId, funcOnSuccess) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllPurchaseOrderItem", purchaseOrderId,
                function (items) {
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllPurchaseOrder", 
                { 
                    'FilterKeyword': g_mainSearch.searchText,
                    'SortBy': g_mainGridSortingType,
                    'PageIndex': g_pageIndex,
                    'RecordPerPage' : RECORD_PER_PAGE
                }, 
                function (items, nVirtualItemCount) {
                    setGridDataWithPage(g_mainGrid, items, nVirtualItemCount, g_pageIndex);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(id) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrievePurchaseOrder", id,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveSupplierInfo(supplierId) {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveSupplierInfo", supplierId,
                function (item) {
                    $("#txtContactPerson").val(item["ContactPerson"]);
                    $("#txtAddr1").val(item["Address1"]);
                    $("#txtAddr2").val(item["Address2"]);
                    $("#txtAddr3").val(item["Address3"]);
                    $("#txtPhone").val(item["Phone"]);
                });
        }
        //function setGridData(grid, data) {
        //    if (grid === g_mainGrid) {
        //        g_mainItems = data;
        //        //$("#selMainSelectedBy option:first-child").prop("selected", true);
        //    }
        //    else if (grid === g_subGrid) {
        //        g_subItems = data;
        //        $("#selSubSelectedBy option:first-child").prop("selected", true);
        //    }
        //    grid.setData(data);
        //}

        function displayMainItemData(item) {
            setFormData(item);
            retrieveSubGridData(item["PurchaseOrderId"]);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            return true;
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    showVoid(false);
                    enableSaveClear();

                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formLock();
                    subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }

        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable();
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if (g_selSupplier.getValue() == "") {
                displayError("Please specify a supplier");
                return false;
            }
            if ($("#txtDate").val() == "") {
                displayError("Please specify the Date");
                return false;
            }
            if (!validateFloat(g_subtotal.getValue()) || !g_subtotal.isValid()) {
                displayError("Invalid Subtotal");
                return false;
            }
            if (!validateFloat(g_tax.getValue()) || !g_tax.isValid()) {
                displayError("Invalid Tax");
                return false;
            }
            if (!validateFloat(g_total.getValue()) || !g_total.isValid()) {
                displayError("Invalid Total");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on row [" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            item["PurchaseOrderId"] = row == null ? -1 : row.data["PurchaseOrderId"];
            item["BusinessId"] = -1;
            item["PurchaseOrderNo"] = row == null ? -1 : row.data["PurchaseOrderNo"];
            item["IsCashPurchase"] = row == null ? false : row.data["IsCashPurchase"];
            item["CashPurchaseNo"] = row == null ? -1 : row.data["CashPurchaseNo"];
            item["SupplierId"] = g_selSupplier.getValue();
            item["SupplierName"] = g_selSupplier.getOption(g_selSupplier.getValue())[0].innerText;
            item["Reference"] = $("#txtRef").val();
            item["ContactPerson"] = $("#txtContactPerson").val();
            item["Address1"] = $("#txtAddr1").val();
            item["Address2"] = $("#txtAddr2").val();
            item["Address3"] = $("#txtAddr3").val();
            item["Date"] = convertToUTCDateTime($("#txtDate").val());
            item["DateDelivery"] = convertToUTCDateTime($("#txtDateDelivery").val());
            item["AmountTypeId"] = -1;
            item["AmountTypeName"] = "";
            item["PaymentTypeId"] = g_selPaymentType.getValue();
            item["PaymentTypeName"] = g_selPaymentType.getOption(g_selPaymentType.getValue())[0].innerText;
            item["Status"] = "New";
            item["Phone"] = $("#txtPhone").val();
            item["CreditTerm"] = $("#txtCreditTerm").val();
            item["Note"] = $("#txtNote").val();
            item["Subtotal"] = g_subtotal.getValue();
            item["Tax"] = g_tax.getValue();
            item["Total"] = g_total.getValue();
            item["ItemState"] = (g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem : KGrid.ItemState.modifiedItem);
            item["Version"] = row == null ? "" : row.data["Version"];
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                //if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                if (row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    subItem["PurchaseOrderItemId"] = row.data["PurchaseOrderItemId"] == null ? -1 : parseInt(row.data["PurchaseOrderItemId"], 10);
                    subItem["ItemId"] = !isNaN(parseInt(row.data["ItemId"], 10)) ? parseInt(row.data["ItemId"], 10) : -1;
                    subItem["ItemFullName"] = row.data["ItemFullName"];
                    subItem["Description"] = row.data["Description"] == null ? "" : row.data["Description"];
                    subItem["Quantity"] = parseFloat(row.data["Quantity"]);
                    subItem["UnitOfMeasure"] = row.data["UnitOfMeasure"];
                    subItem["Price"] = parseFloat(row.data["Price"]);
                    subItem["Amount"] = parseFloat(row.data["Amount"]);
                    subItem["ItemState"] = row.rowState;
                    item["Items"].push(subItem);
                }
            }
            return item;
        }

        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "Purchasing.svc", "ManagePurchaseOrder", item,
                function(rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        if (selectedRow != null)
                            selectedRow.select(null, false);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            doDefaultVoid("Purchasing.svc", "VoidPurchaseOrder", "PurchaseOrderId");
        }
        function doClear() {
            if (preventNavigationOnUnsaved() && isPageDirty())
                doDefaultClear();
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/PurchaseOrderA.aspx";
            var docTemplateBUrl = "../Printable/PurchaseOrderB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Purchase Order", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('CashPurchase.aspx');
        }
        function goNext() {
            changeUrl('ReceiveOrder.aspx');
        }

        // Handlers
        function onMainSearchCallback(criteriaName, criteriaText) {
            retrieveMainGridData();
        }
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["PurchaseOrderId"]);
                }    
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function sgOnRowMouseEnter(pRow) {
            delayDisplayTooltip(displayTableView, pRow.data["PurchaseOrderId"]);
        }
        function sgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function displayTableView(value) {
            //sendAjax("../Service/Purchasing.svc/RetrievePurchaseOrder", value,
            //    function (rsp) {
            //        var item = rsp.ResponseItem;
            //        var tooltipContent;
            //        tooltipContent = 'Date: ' + item['Date'] + '</br>';
            //        tooltipContent += 'Delivery Date: ' + item['DateDelivery'] + '</br>';
            //        //tooltipContent += 'Amount Type: ' + g_selAmountType.getOption(item['AmountTypeId'])[0].innerText + '</br>';
            //        tooltipContent += 'Payment Method: ' + g_selPaymentType.getOption(item['PaymentTypeId'])[0].innerText;
            //        $('#divDynamicTooltip').html(tooltipContent);
            //        $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
            //    },
            //    function (type, err, errThrown) {
            //    });
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        function sgOnItemChanged(pCell, val) {
        }
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_subItems, "ItemId", pVal, "ItemFullName");
        }
        function onLoadHandler(pCell, pCallback) {
            g_cell = pCell;
            retrieveAjaxDataAsync( 
                "ItemManagement.svc", "RetrieveItemList", {},
                function(items) {
                    g_itemList = items;
                    pCallback(g_itemList);
                },
                function (txt) {
                    displayError(txt);
                    pCallback();
                });
        }
        function onItemChangedHandler(pCell, pVal) {
            var row = pCell.parentRow;
            var uomCell = row.getCellByColumnName("UnitOfMeasure");
            var qtyCell = row.getCellByColumnName("Quantity");
            var priceCell = row.getCellByColumnName("Price");
            var amtCell = row.getCellByColumnName("Amount");

            if (pVal === "") {
                if (row.rowState == KGrid.ItemState.newItem) {
                    uomCell.setValue("");
                    qtyCell.setValue("");
                    priceCell.setValue("");
                    amtCell.setValue("");
                }
                else
                    return;
            } else if (isNaN(parseInt(pVal, 10))) { // user specify non-existent item
                uomCell.setValue("Unit");
                qtyCell.setValue(0);
                priceCell.setValue(0);
                row.data["ItemFullName"] = pVal;

                recalculateAmount(pCell, pVal);
            } else {
                retrieveAjaxDataAsync(
                    "ItemManagement.svc", "RetrieveItem", pVal,
                    function(item) {
                        uomCell.setValue(item.UnitOfMeasure);
                        qtyCell.setValue(0);
                        priceCell.setValue(item.Price);
                        row.data["ItemFullName"] = item["ItemFullName"];

                        recalculateAmount(pCell, pVal);
                    },
                    function(txt) {
                        displayError(txt);
                    });
            }
        }
        function onMainSearchQuery(queryTxt) {
            retrieveMainGridData(null);
        }
        function mgOnPageSelected(pageIndex) {
            g_pageIndex = pageIndex;
            retrieveMainGridData(null);
        }

        function recalculateAmount(pCell, pVal) {
            var row = pCell.parentRow;
            var qtyCell = row.getCellByColumnName("Quantity");
            var priceCell = row.getCellByColumnName("Price");
            var amtCell = row.getCellByColumnName("Amount");
            var qty = qtyCell.value;
            var price = priceCell.value;
            if (qty === "")
                qty = 0;
            if (price === "")
                price = 0;
            qtyCell.setValue(qty);
            priceCell.setValue(price);
            amtCell.setValue( parseFloat(qty) * parseFloat(price) );

            recalculateSummary();
        }
        function recalculateSummary() {
            var d = g_subGrid.data;
            var subtotal = 0;
            for (var i = 0; i < d.length; ++i) {
                var val = parseFloat(d[i]["Amount"]);
                if (!isNaN(val))
                    subtotal += val;
            }
            g_subtotal.setValue(subtotal);
            var taxedAmount = parseFloat(g_taxFactor) * parseFloat(subtotal);
            g_tax.setValue(taxedAmount);
            g_total.setValue(taxedAmount + subtotal);
        }
        function showVoid(fShow) {
            if (fShow)
                $("#tdVoid").empty().append("<div>VOID</div>");
            else
                $("#tdVoid").empty();
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                    $("#btnMail").disable();
                    $("#btnReceive").disable();
                }
                else {
                    var rowData = pRow.data;
                    var fVoid = (rowData["Status"].toLowerCase() == "void");
                    if (fVoid) {
                        $("#btnMainEdit").disable();
                        $("#btnMainDelete").enable();
                        $("#btnMail").disable();
                        $("#btnReceive").disable();
                    } else {
                        $("#btnMainEdit").enable();
                        $("#btnMainDelete").disable();
                        $("#btnMail").enable();
                        $("#btnReceive").enable();
                    }
                }
            }
            else { // subGrid
                if (pRow == null) {
                    //$("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    //if (pRow.readOnly)
                    //    $("#btnSubEdit").enable();
                    //else
                    //    $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }

        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            //$("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();
            
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            if (!canEditMainItem(g_activeMainItem)) {
                displayInfo("Cannot edit this type of item");
                return;
            }
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();

            if (selectedRow.data["Status"].toLowerCase() == "void") {
                if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?"))
                {
                    var data = selectedRow.data;
                    var item = {};
                    item["PurchaseOrderId"] = data["PurchaseOrderId"];
                    item["Version"] = data["Version"];
                    item["ItemState"] = KGrid.ItemState.deletedItem;
                    //item["Items"] = [];

                    sendAjaxUpdate( "Purchasing.svc", "ManagePurchaseOrder", item,
                        function(rsp) {
                            setPageDirty(false);
                            retrieveMainGridData();
                            changeState(ePageState.mainNew);
                        },
                        function(rsp) {
                            displayError(rsp.StatusText);
                        });
                }
            }
            else {
                displayError("Please VOID the item before deleting it");
            }
        }
        function canEditMainItem(item) {
            // Pro : ToDo
            return true;
        }

        function onSendMail () {
            displayError("To be implemented");
        }
        function onDoReceive () {
            displayError("To be implemented");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-yellow">
        <ul class="nav nav-tabs">
            <li class="inactive"><a href="#tab1" data-toggle="tab" onclick="changeUrl('CashPurchase.aspx');">Cash Purchase</a></li>
            <li class="active"><a href="#" data-toggle="tab">Purchase Order</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveOrder.aspx');">Receive Order</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('OutgoingPayment.aspx');">Outgoing Payment</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('Expense.aspx');">Expense</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div id="divForm" class="section">
                    <table id="tblForm" class="form double">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <select id="selSupplier" placeholder="-- search --">
                                    </select>
                                </td>
                                <td>Reference:</td>
                                <td>
                                    <input type="text" id="txtRef" />
                                </td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>
                                    <input type="text" id="txtContactPerson" />
                                </td>
                                <td>PO Number:</td>
                                <td>
                                    <input type="text" id="txtPoNo" />
                                </td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>
                                    <input type="text" id="txtAddr1" />
                                </td>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate" style="width: 195px;" />
                                    <img src="../images/icon_calendar.png" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtAddr2" />
                                    <input type="text" id="txtAddr3" hidden />
                                </td>
                                <td>Status:</td>
                                <td>
                                    <input type="text" id="txtStatus" />
                                </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <input type="text" id="txtPhone" />
                                </td>
                                <td>Payment Method:</td>
                                <td>
                                    <select id="selPaymentType">
                                        <option value="1">Cash</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Delivery Date:</td>
                                <td>
                                    <input type="text" id="txtDateDelivery" style="width: 195px;" />
                                    <img src="../images/icon_calendar.png" />
                                </td>
                                <td>Credit Term:</td>
                                <td>
                                    <input type="text" id="txtCreditTerm" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- Form Pane -->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <!-- SubGrid Pane -->
                <div id="subGridPane" class="section">
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="btnSubEditOnClick();" />
                            <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="btnSubDeleteOnClick();" />
                            <input type="button" id="btnSubUndoDelete" class="btn-action" value="Undo" onclick="btnSubUndoDeleteOnClick();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="subGrid" style="height: 190px;">
                    </div>
                    <div class="amount-summary">
                        <table id="tblSummary" class="form summary">
                            <tr>
                                <td class="caption">Note:</td>
                                <td class="control">
                                    <input type="text" id="txtNote" />
                                </td>
                                <td class="caption-amount">Sub total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtSubtotal" />
                                </td>
                            </tr>
                            <tr>
                                <td id="tdVoid" class="pane-void" colspan="2" rowspan="2"></td>
                                <td class="caption-amount">Tax:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTax" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption-amount">Total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTotal" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="header-search">Search</div>
                    <div class="grid-command search">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                            <input type="button" id="btnMail" class="btn-action text-teal" value="Mail" onclick="onSendMail();" />
                            <input type="button" id="btnReceive" class="btn-action text-teal" value="Receive" onclick="onDoReceive;" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 210px;">
                    </div>
                </div>
            </div>
            <!--tab-pane-->
        </div>
        <!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>
