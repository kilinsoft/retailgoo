﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="DocSetting.aspx.cs" Inherits="RetailGOO.Page.DocSetting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .form input[type=text] {
            width: 100%;
        }
        .form td {
            padding-top: 10px;
        }
        .prefix { width: 80px; }
        .nextnumber { width: 180px; }
        .suffix { width: 80px; }
        .btn-template { width: 140px; }
        .row-border-bottom { 
            border-bottom: 1px solid #d0d0d0; 
        }
        .row-border-bottom > td {
            padding-bottom: 10px;
        }
    </style>
    <script type="text/javascript">
        // Variables declaration

        function pageLoad() {

            preventNavigationOnUnsaved(true);

            // Initialize behaviors
            $("#txtNumberSo").attr("disabled", true);
            $("#txtNumberIv").attr("disabled", true);
            $("#txtNumberRc").attr("disabled", true);
            $("#txtNumberSa").attr("disabled", true);
            $("#txtNumberRo").attr("disabled", true);
            $("#txtNumberCp").attr("disabled", true);
            $("#txtNumberCs").attr("disabled", true);
            $("#txtNumberPo").attr("disabled", true);
            $("#txtNumberPm").attr("disabled", true);
            $("#txtNumberEp").attr("disabled", true);

            $("#btnVoid").addClass('disable').disable();
            $("#btnPrint").addClass('disable').disable();
            $("#btnClear").addClass('disable').disable();

            // Init state
            //changeState(ePageState.mainEdit);

            // Ajax calls to initialize mandatory variables
            retrieveMainData();
        }
        function retrieveMainData(funcOnSuccess) {
            retrieveAjaxDataSync("Settings.svc", "RetrieveDocSetting", {},
                function (items) {
                    g_activeMainItem = items;
                    displayMainItemData(g_activeMainItem);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function displayMainItemData(item) {
            setFormData(item);
        }
        function recalculatePreview() {
            return item;
        }
        function setFormData(item) {
            $("#txtPrefixSo").val(item.SalesOrderPrefix);
            $("#chkYearSo").prop('checked', item.SalesOrderYear);
            $("#chkMonthSo").prop('checked', item.SalesOrderMonth);
            $("#txtNumberSo").val(item.SalesOrderNextNo);
            $("#txtSuffixSo").val(item.SalesOrderSuffix);
            //$("#txtPreviewSo").val(item.SalesOrderPreview);

            $("#txtPrefixIv").val(item.InvoicePrefix);
            $("#chkYearIv").prop('checked', item.InvoiceYear);
            $("#chkMonthIv").prop('checked', item.InvoiceMonth);
            $("#txtNumberIv").val(item.InvoiceNextNo);
            $("#txtSuffixIv").val(item.InvoiceSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixRc").val(item.ReceiptPrefix);
            $("#chkYearRc").prop('checked', item.ReceiptYear);
            $("#chkMonthRc").prop('checked', item.ReceiptMonth);
            $("#txtNumberRc").val(item.ReceiptNextNo);
            $("#txtSuffixRc").val(item.ReceiptSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixSa").val(item.StockAdjPrefix);
            $("#chkYearSa").prop('checked', item.StockAdjYear);
            $("#chkMonthSa").prop('checked', item.StockAdjMonth);
            $("#txtNumberSa").val(item.StockAdjNextNo);
            $("#txtSuffixSa").val(item.StockAdjSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixRo").val(item.ReceiveOrderPrefix);
            $("#chkYearRo").prop('checked', item.ReceiveOrderYear);
            $("#chkMonthRo").prop('checked', item.ReceiveOrderMonth);
            $("#txtNumberRo").val(item.ReceiveOrderNextNo);
            $("#txtSuffixRo").val(item.ReceiveOrderSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixCp").val(item.CashPurchasePrefix);
            $("#chkYearCp").prop('checked', item.CashPurchaseYear);
            $("#chkMonthCp").prop('checked', item.CashPurchaseMonth);
            $("#txtNumberCp").val(item.CashPurchaseNextNo);
            $("#txtSuffixCp").val(item.CashPurchaseSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixCs").val(item.CashSalesPrefix);
            $("#chkYearCs").prop('checked', item.CashSalesYear);
            $("#chkMonthCs").prop('checked', item.CashSalesMonth);
            $("#txtNumberCs").val(item.CashSalesNextNo);
            $("#txtSuffixCs").val(item.CashSalesSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixPo").val(item.PurchaseOrderPrefix);
            $("#chkYearPo").prop('checked', item.PurchaseOrderYear);
            $("#chkMonthPo").prop('checked', item.PurchaseOrderMonth);
            $("#txtNumberPo").val(item.PurchaseOrderNextNo);
            $("#txtSuffixPo").val(item.PurchaseOrderSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixPm").val(item.PaymentPrefix);
            $("#chkYearPm").prop('checked', item.PaymentYear);
            $("#chkMonthPm").prop('checked', item.PaymentMonth);
            $("#txtNumberPm").val(item.PaymentNextNo);
            $("#txtSuffixPm").val(item.PaymentSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            $("#txtPrefixEp").val(item.ExpensePrefix);
            $("#chkYearEp").prop('checked', item.ExpenseYear);
            $("#chkMonthEp").prop('checked', item.ExpenseMonth);
            $("#txtNumberEp").val(item.ExpenseNextNo);
            $("#txtSuffixEp").val(item.ExpenseSuffix);
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);
        }
        function clearFormData() {

        }
        function constructDtoItem() {
            var item = {};
            var row = g_activeMainItem;

            item["DocSettingId"] = row == null ? -1 : row.DocSettingId;
            item["SalesOrderPrefix"] = $("#txtPrefixSo").val();
            item["SalesOrderYear"] = $("#chkYearSo").is(':checked');
            item["SalesOrderMonth"] = $("#chkMonthSo").is(':checked');
            item["SalesOrderNextNo"] = $("#txtNumberSo").val();
            item["SalesOrderSuffix"] = $("#txtSuffixSo").val();
            //$("#txtPreviewSo").val(item.SalesOrderPreview);

            item["InvoicePrefix"] = $("#txtPrefixIv").val();
            item["InvoiceYear"] = $("#chkYearIv").is(':checked');
            item["InvoiceMonth"] = $("#chkMonthIv").is(':checked');
            item["InvoiceNextNo"] = $("#txtNumberIv").val();
            item["InvoiceSuffix"] = $("#txtSuffixIv").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["ReceiptPrefix"] = $("#txtPrefixRc").val();
            item["ReceiptYear"] = $("#chkYearRc").is(':checked');
            item["ReceiptMonth"] = $("#chkMonthRc").is(':checked');
            item["ReceiptNextNo"] = $("#txtNumberRc").val();
            item["ReceiptSuffix"] = $("#txtSuffixRc").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["StockAdjPrefix"] = $("#txtPrefixSa").val();
            item["StockAdjYear"] = $("#chkYearSa").is(':checked');
            item["StockAdjMonth"] = $("#chkMonthSa").is(':checked');
            item["StockAdjNextNo"] = $("#txtNumberSa").val();
            item["StockAdjSuffix"] = $("#txtSuffixSa").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["ReceiveOrderPrefix"] = $("#txtPrefixRo").val();
            item["ReceiveOrderYear"] = $("#chkYearRo").is(':checked');
            item["ReceiveOrderMonth"] = $("#chkMonthRo").is(':checked');
            item["ReceiveOrderNextNo"] = $("#txtNumberRo").val();
            item["ReceiveOrderSuffix"] = $("#txtSuffixRo").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["CashPurchasePrefix"] = $("#txtPrefixCp").val();
            item["CashPurchaseYear"] = $("#chkYearCp").is(':checked');
            item["CashPurchaseMonth"] = $("#chkMonthCp").is(':checked');
            item["CashPurchaseNextNo"] = $("#txtNumberCp").val();
            item["CashPurchaseSuffix"] = $("#txtSuffixCp").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["CashSalesPrefix"] = $("#txtPrefixCs").val();
            item["CashSalesYear"] = $("#chkYearCs").is(':checked');
            item["CashSalesMonth"] = $("#chkMonthCs").is(':checked');
            item["CashSalesNextNo"] = $("#txtNumberCs").val();
            item["CashSalesSuffix"] = $("#txtSuffixCs").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["PurchaseOrderPrefix"] = $("#txtPrefixPo").val();
            item["PurchaseOrderYear"] = $("#chkYearPo").is(':checked');
            item["PurchaseOrderMonth"] = $("#chkMonthPo").is(':checked');
            item["PurchaseOrderNextNo"] = $("#txtNumberPo").val();
            item["PurchaseOrderSuffix"] = $("#txtSuffixPo").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["PaymentPrefix"] = $("#txtPrefixPm").val();
            item["PaymentYear"] = $("#chkYearPm").is(':checked');
            item["PaymentMonth"] = $("#chkMonthPm").is(':checked');
            item["PaymentNextNo"] = $("#txtNumberPm").val();
            item["PaymentSuffix"] = $("#txtSuffixPm").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            item["ExpensePrefix"] = $("#txtPrefixEp").val();
            item["ExpenseYear"] = $("#chkYearEp").is(':checked');
            item["ExpenseMonth"] = $("#chkMonthEp").is(':checked');
            item["ExpenseNextNo"] = $("#txtNumberEp").val();
            item["ExpenseSuffix"] = $("#txtSuffixEp").val();
            //$("#txtPrefixSo").val(item.SalesOrderTemplate);

            return item;
        }
        function doSave() {
            //if (!validateForm())
            //    return false;

            var item = constructDtoItem();

            sendAjaxUpdate("Settings.svc", "ManageDocSetting", item,
                function (rsp) {
                    setPageDirty(false);
                    //if (g_pageState == ePageState.mainEdit) {
                    //    changeState(ePageState.mainView);
                    //    var selectedRow = g_mainGrid.selectedRow;
                    //    g_mainGrid.clearSelection();
                    //    if (selectedRow != null)
                     //       selectedRow.select(null, false);
                    //}
                    //else {
                        //changeState(ePageState.mainNew);
                        //retrieveMainGridData();
                    //}
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doClear() {
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('FinancialSetting.aspx');
        }
        function goNext() {
            changeUrl('HardwareSetting.aspx');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-red">
        <ul id="tab0" class="nav nav-tabs first-row">
            <%--<li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('HardwareSetting.aspx');">Hardware Setting</a></li>--%>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('Roles.aspx');">Roles</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('User.aspx')">User</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('BusinessInfo.aspx');">Business Info</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('FinancialSetting.aspx');">Financial Setting</a></li>
            <li class="active"><a href="#" data-toggle="tab" onclick="GoToUrl('DocSetting.aspx')">Doc Setting</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tabAddUser">
                <div class="section"><!-- Financial Setting section -->
                    <table class="form">
                        <tr>
                            <td class="cell-header cell-name left" style="background-color: white;" colspan="8">
                                Document Setting:
                            </td>
                        </tr>
                        <tr class="row-border-bottom">
                            <td class="cell-name left" style="width: 120px;">Doc Type:</td>
                            <td class="cell-name center" style="width: 80px;">Prefix:</td>
                            <td class="cell-name center" style="width: 30px;">YY:</td>
                            <td class="cell-name center" style="width: 30px;">MM:</td>
                            <td class="cell-name center">Next Number:</td>
                            <td class="cell-name center" style="width: 80px;">Suffix:</td>
                            <td class="cell-name center" style="width: 120px;">Preview:</td>
                            <td class="cell-name center">Default Doc.Template:</td>
                        </tr>
                        <tr class="row-first">
                            <td class="cell-name left">Sales Order:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixSo" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearSo" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthSo" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberSo" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixSo" class="" /></td>
<!--                            <td class="cell-control center"><input type="text" id="txtPreviewSo" class="" /></td> -->
                            <td class="cell-control center">SO-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateSo" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Invoice:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixIv" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearIv" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthIv" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberIv" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixIv" class="" /></td>
                            <td class="cell-control center">IV-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateIv" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Receipt:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixRc" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearRc" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthRc" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberRc" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixRc" class="" /></td>
                            <td class="cell-control center">RV-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateRc" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Stock Adj No:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixSa" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearSa" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthSa" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberSa" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixSa" class="" /></td>
                            <td class="cell-control center">SA-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateSa" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Receive Order:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixRo" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearRo" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthRo" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberRo" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixRo" class="" /></td>
                            <td class="cell-control center">RO-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateRo" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Cash Purchase:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixCp" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearCp" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthCp" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberCp" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixCp" class="" /></td>
                            <td class="cell-control center">CP-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateCp" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Cash Sales:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixCs" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearCs" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthCs" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberCs" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixCs" class="" /></td>
                            <td class="cell-control center">CS-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateCs" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Purchase Order:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixPo" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearPo" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthPo" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberPo" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixPo" class="" /></td>
                            <td class="cell-control center">PO-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplatePo" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr>
                            <td class="cell-name left">Payment:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixPm" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearPm" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthPm" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberPm" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixPm" class="" /></td>
                            <td class="cell-control center">PV-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplatePm" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                        <tr class="row-border-bottom">
                            <td class="cell-name left">Expense:</td>
                            <td class="cell-control center"><input type="text" id="txtPrefixEp" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkYearEp" class="" /></td>
                            <td class="cell-control center"><input type="checkbox" id="chkMonthEp" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtNumberEp" class="" /></td>
                            <td class="cell-control center"><input type="text" id="txtSuffixEp" class="" /></td>
                            <td class="cell-control center">EX-00001</td>
                            <td class="cell-control center"><input type="button" id="btnTemplateEp" value="Select Template" class="btn-action-big btn-red btn-template" /></td>
                        </tr>
                    </table>
                </div>
                <div>
                    <div class="section common-control">
                        <div>
                            <div id="btnBackward" onclick="goBack();">&lt;</div>
                            <div id="btnSave" onclick="doSave();">Save</div>
                            <div id="btnVoid" onclick="doVoid();">Void</div>
                            <div id="btnClear" onclick="doClear();">Clear</div>
                            <div id="btnPrint" onclick="doPrint();">Print</div>
                            <div id="btnCancel" onclick="doCancel();">Cancel</div>
                            <div id="btnForward" onclick="goNext();">&gt;</div>
                        </div>
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div><!--subpage-->
</asp:Content>
