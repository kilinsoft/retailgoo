﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="IncomingPayment.aspx.cs" Inherits="RetailGOO.Page.IncomingPayment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .nav-tabs > li {
            /*width: 160px !important;*/
        }
        .auto-style1 {
            height: 34px;
        }
    </style>
    <script type="text/javascript">

        // Control variables
        var g_subtotal, g_tax, g_total;
        var g_selCustomer, g_selPaymentType;
        var g_docTemplate = <%=DocumentTemplate%>;

        // Data variables
        var g_customerList = <%=CustomerListString%>;
        var g_paymenttypeList = <%=PaymentTypeListString%>;
        var g_taxFactor = <%=TaxFactor%>;
        var g_itemList = null;
        
        function pageLoad() {
               
            preventNavigationOnUnsaved(true);

            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Receipt No.", name: "IncomingPaymentNo", required: false, editable: false },
            { title: "Reference", name: "Reference", required: false, editable: false },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Status", name: "Status", required: false, editable: false },
            { title: "Name", name: "CustomerName", required: false, editable: false },
            { title: "Amount", name: "Amount", width: 150, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                onRowMouseEnter: mgOnRowMouseEnter,
                onRowMouseLeave: mgOnRowMouseLeave
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            var selEditorOptions = {
                create: true,
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                onLoad: onLoadHandler,
                options: []
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Customer Invoice", name: "SalesInvoiceId", width: 300, required: true, editable: true, 
                editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions),
                onChanged: onItemChangedHandler
            },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Amount", name: "Amount", required: true, editable: false, formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) },
            { title: "Due", name: "Due", required: false, editable: false, formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) },
            { title: "Pay", name: "Pay", required: false, editable: true, formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) },
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            $("#txtDate").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            g_selCustomer = $("#selCustomer").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_customerList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveCustomerInfo(val);
                    }
                },
            })[0].selectize;

            g_selPaymentType = $("#selPaymentType").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_paymenttypeList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrievePaymentTypeInfo(val);
                    }
                },
            })[0].selectize;

            // Summary calculation
            var funcOnTaxChanged = function($e, val) {
                var tax = parseFloat(val);
                var subtotal = parseFloat(g_subtotal.getValue());
                var total = tax + subtotal;
                g_total.setValue(total);
            };
            var funcOnSubtotalChanged = function($e, val) { 
                var subtotal = parseFloat(val);
                var taxedAmount = parseFloat(g_taxFactor) * subtotal;
                g_tax.setValue(taxedAmount);
                funcOnTaxChanged(g_tax.$element, taxedAmount); // Call onTaxChanged next
            };
            g_subtotal = new Util.Input.Amount("#txtSubtotal", 2, funcOnSubtotalChanged);
            g_tax = new Util.Input.Amount("#txtTax", 2, funcOnTaxChanged);
            g_total = new Util.Input.Amount("#txtTotal", 2);

            initGridSorter(g_subGrid, "#selSubSelectedBy",
                [{ title: "Customer Invoice", name: "SalesInvoiceId", valueCompare: false }, 
                { title: "Date", name: "Date", valueCompare: true },
                { title: "Amount", name: "Amount", valueCompare: false },
                { title: "Due", name: "Due", valueCompare: true },
                { title: "Pay", name: "Pay", valueCompare: true }]);
            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "Receipt No.", name: "IncomingPaymentNo", valueCompare: false }, 
                { title: "Reference", name: "Reference", valueCompare: false },
                { title: "Date", name: "Date", valueCompare: false },
                { title: "Status", name: "Status", valueCompare: false },
                { title: "Name", name: "CustomerName", valueCompare: false },
                { title: "Amount", name: "Total", valueCompare: true }]);

            // Init elements' behaviors
            //$("#selPaymentMethod").disable();
            $("#btnSubEdit").disable();
            
            $("#selAmountCal").disable();

            // Init element's values

            // Init state
            changeState(ePageState.mainNew);

            // Behaviors
            g_subtotal.disable();
            //g_tax.disable();
            g_total.disable();

            retrieveMainGridData();
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#txtNote").readOnly(true);
            g_selCustomer.disable();
            g_selPaymentType.disable();
            $("#txtDate").prop('disabled', true);
            g_subtotal.disable();
            g_tax.disable();
            g_total.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtIpNo").readOnly(true);
            //$("#selPaymentMethod").prop("disabled", true);
            $("#txtDate").prop('disabled', false);
            $("#txtNote").readOnly(false);
            g_selCustomer.enable();
            g_selPaymentType.enable();
            //g_subtotal.enable(); // disable Subtotal
            g_tax.enable();
            //g_total.enable(); // disable total
        }

        function setFormData(item) {
            g_selCustomer.setValue(item["CustomerId"]);
            $("#txtRef").val(item.Reference);
            $("#txtIpNo").val(item.IncomingPaymentNo);
            $("#txtDate").datepicker("setDate", new Date(item["Date"]));
            $("#txtAddr1").val(item.Address1);
            $("#txtAddr2").val(item.Address2);
            $("#txtAddr3").val(item.Address3);
            $("#txtPhone").val(item.Phone);
            $("#txtCreditTerm").val(item.CreditTerm);
            $("#txtNote").val(item.Note);
            $("#txtBankAccNo").val(item.BankAccNo);
            $("#txtStatus").val(item.Status);
            g_selPaymentType.setValue(item["PaymentTypeId"]);
            showVoid(item["Status"].toLowerCase() == "void");

            g_subtotal.setValue(item.Subtotal);
            g_tax.setValue(item.Tax);
            g_total.setValue(item.Total);
        }
        function clearFormData() {
            if (g_customerList.length > 0)
                g_selCustomer.setValue(-1);
            if (g_paymenttypeList.length > 0)
                g_selPaymentType.setValue(1);
            $("#tblForm input[type=text]").val("");
            $("#txtNote").val("");
            $("#txtDate").datepicker("setDate", new Date());

            g_subtotal.setValue(0);
            g_tax.setValue(0);
            g_total.setValue(0);
        }

        function retrieveSubGridData(incomingpaymentId, funcOnSuccess) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveAllIncomingPaymentItem", incomingpaymentId,
                function (items) {
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveAllIncomingPayment", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(id) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveIncomingPayment", id,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveCustomerInfo(customerId)
        {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveCustomerInfo", customerId,
                function (item) {
                    $("#txtContact").val(item.ContactPerson);
                    $("#txtAddr1").val(item.AddressBilling1);
                    $("#txtAddr2").val(item.AddressBilling2);
                    $("#txtAddr3").val(item.AddressBilling3);
                    $("#txtPhone").val(item.Phone);
                });
        }
        function retrievePaymentTypeInfo(paymenttypeid)
        {
            // HIM: to be implemented
        }
        function setGridData(grid, data) {
            if (grid === g_mainGrid) {
                g_mainItems = data;
                $("#selMainSelectedBy option:first-child").prop("selected", true);
            }
            else if (grid === g_subGrid) {
                g_subItems = data;
                $("#selSubSelectedBy option:first-child").prop("selected", true);
            }
            grid.setData(data);
        }

        function displayMainItemData(item) {
            setFormData(item);
            retrieveSubGridData(item["IncomingPaymentId"]);
        }
        function setSubGridData(aoData) {
            g_subItems = aoData;
            setGridData(g_subGrid, aoData);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            return true;
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    showVoid(false);
                    enableSaveClear();

                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formLock();
                    subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }

        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable(); //already active
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable(); //already active
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function initGridSorter(grid, selId, options) {
            var $sel = $(""+selId);
            for (var i = 0; i < options.length; ++i)
                $sel.append($("<option></option>").attr("value", options[i]["name"]).text(options[i]["title"]));
            $sel.on("change.gridSort", function (e) {
                var val = $sel.val();
                var fValueCompare = getAssocValue(options, "name", val, "valueCompare"); 
                for (var i = grid.data.length - 1; i >= 0; --i)
                    if (grid.data[i].itemState == KGrid.ItemState.blankItem)
                        grid.data.splice(i, 1);
                grid.data.sort(sortByKey(val, fValueCompare));
                grid.invalidate();
                if (grid === g_mainGrid)
                    changeState(ePageState.mainNew);
            });
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if (g_selCustomer.getValue() == "") {
                displayError("Please specify a supplier");
                return false;
            }
            if ($("#txtDate").val() == "") {
                displayError("Please specify the Date");
                return false;
            }
            if (!validateFloat(g_subtotal.getValue()) || !g_subtotal.isValid()) {
                displayError("Invalid Subtotal");
                return false;
            }
            if (!validateFloat(g_tax.getValue()) || !g_tax.isValid()) {
                displayError("Invalid Subtotal");
                return false;
            }
            if (!validateFloat(g_total.getValue()) || !g_total.isValid()) {
                displayError("Invalid Subtotal");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on row [" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            item["IncomingPaymentId"] = row == null ? -1 : row.data["IncomingPaymentId"];
            item["BusinessId"] = -1;
            item["IncomingPaymentNo"] = row == null ? -1 : row.data["IncomingPaymentNo"];
            item["CustomerId"] = g_selCustomer.getValue();
            item["CustomerName"] = g_selCustomer.getOption(g_selCustomer.getValue())[0].innerText;
            item["Reference"] = $("#txtRef").val();
            item["PaymentTypeId"] = g_selPaymentType.getValue();
            item["PaymentTypeName"] = g_selPaymentType.getOption(g_selPaymentType.getValue())[0].innerText;
            item["BankId"] = 1;
            item["BankAccNo"] = $("#txtBankAccNo").val();
            item["Address1"] = $("#txtAddr1").val();
            item["Address2"] = $("#txtAddr2").val();
            item["Address3"] = $("#txtAddr3").val();
            item["Phone"] = $("#txtPhone").val();
            item["Date"] = convertToUTCDateTime($("#txtDate").val());
            item["Note"] = $("#txtNote").val();
            item["Subtotal"] = g_subtotal.getValue();
            item["Tax"] = g_tax.getValue();
            item["Total"] = g_total.getValue();
            item["Status"] = "New";
            item["ItemState"] = (g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem : KGrid.ItemState.modifiedItem);
            item["Version"] = row == null ? "" : row.data["Version"];
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                //if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                if (row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    //subItem["IncomingPaymentItemId"] = row.data["IncomingPaymentItemId"] == null ? -1 : parseInt(row.data["IncomingPaymentItemId"], 10);
                    subItem["IncomingPaymentId"] = row.data["IncomingPaymentId"] == null ? -1 : parseInt(row.data["IncomingPaymentId"], 10);
                    subItem["SalesInvoiceId"] = row.data["SalesInvoiceId"] == null ? -1 : parseInt(row.data["SalesInvoiceId"], 10);
                    subItem["SalesInvoiceNo"] = row.data["SalesInvoiceNo"];
                    subItem["Date"] = row.data["Date"];
                    subItem["Amount"] = parseFloat(row.data["Amount"]);
                    subItem["Due"] = parseFloat(row.data["Due"]);
                    subItem["Pay"] = parseFloat(row.data["Pay"]);
                    subItem["ItemState"] = row.rowState;
                    item["Items"].push(subItem);
                }
            }
            return item;
        }

        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "Sales.svc", "ManageIncomingPayment", item,
                function(rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        if (selectedRow != null)
                            selectedRow.select(null, false);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            var row = g_mainGrid.selectedRow;
            if (row == null) {
                displayError("Please select an item first");
                return;
            }
            if (row.data["IsVoided"]) {
                displayError("The item has already been marked as VOID");
                return; 
            }
            var fContinue = window.confirm("Voiding the item will affect all of its related items as well.\n\nDo you want to continue?");
            if (fContinue) {
                var idVoid = parseInt(row.data["IncomingPaymentId"], 10);
                sendAjaxUpdate( "Sales.svc", "VoidIncomingPayment", idVoid,
                    function(rsp) {
                        setPageDirty(false);
                        changeState(ePageState.mainView);
                        var iRow = g_mainGrid.selectedRow.rowIndex;
                        retrieveMainGridData(function() {
                            g_mainGrid.selectRow(iRow);
                        });
                    },
                    function(rsp) {
                        displayError(rsp.StatusText);
                    });
            }
        }
        function doClear() {
            var fClear = window.confirm("Your unsaved data will be lost.\nDo you want to continue?");
            if (fClear) {
                setPageDirty(false);
                switch(g_pageState) {
                    case ePageState.mainNew:
                        changeState(ePageState.mainNew);
                        break;
                    case ePageState.mainView:
                        break;
                    case ePageState.mainEdit:
                        if (g_mainGrid.data.length > 0)
                            setFormData(g_mainGrid.data[g_mainGrid.selectedRow.rowIndex]);
                        setGridData(g_subGrid, g_subItems);
                        g_subGrid.unlock();
                        changeState(ePageState.mainEdit);
                        //changeState(ePageState.mainNew);
                        break;
                    default:
                        break;
                }
            }
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/IncomingPaymentA.aspx";
            var docTemplateBUrl = "../Printable/IncomingPaymentB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Incoming Payment", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('SalesInvoice.aspx');
        }
        function goNext() {
            changeUrl('CashSales.aspx');
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["IncomingPaymentId"]);
                }
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function mgOnRowMouseEnter(pRow) {
            delayDisplayTooltip(displayTableView, pRow.data["IncomingPaymentId"]);
        }
        function mgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function displayTableView(value) {
            //$("#divDynamicTooltip").appendTo("body"); // So the div does not display relatively to its parent but the body instead
            //retrieveAjaxDataSync("Sales.svc", "RetrieveIncomingPayment", value,
            sendAjax("../Service/Sales.svc/RetrieveIncomingPayment", value,
                function (rsp) {
                    var item = rsp.ResponseItem;
                    var tooltipContent;
                    tooltipContent = 'Bank No: ' + item['BankAccNo'] + '</br>';
                    tooltipContent += 'Date: ' + item["Date"] + '</br>';
                    tooltipContent += 'Payment Method: ' + g_selPaymentType.getOption(item['PaymentTypeId'])[0].innerText;
                    $('#divDynamicTooltip').html(tooltipContent);
                    $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
                },
                function (type, err, errThrown) {
                });
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        function sgOnItemChanged(pCell, val) {
        }
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_subItems, "SalesInvoiceId", pVal, "SalesInvoiceNo");
        }
        function onLoadHandler(pCell, pCallback) {
            retrieveAjaxDataAsync( 
                "Sales.svc", "RetrieveSalesInvoiceList", {},
                function(items) {
                    g_itemList = items;
                    pCallback(g_itemList);
                },
                function (txt) {
                    displayError(txt);
                    pCallback();
                });
        }
        function onItemChangedHandler(pCell, pVal) {
            var row = pCell.parentRow;
            var dateCell = row.getCellByColumnName("Date");
            var amtCell = row.getCellByColumnName("Amount");
            var dueCell = row.getCellByColumnName("Due");
            var payCell = row.getCellByColumnName("Pay");

            if (pVal === "") {
                if (row.rowState == KGrid.ItemState.newItem) {
                    dateCell.setValue("");
                    amtCell.setValue("");
                }
                else
                    return;
            } else if (isNaN(parseInt(pVal, 10))) { // user specify non-existent item
                //row.data["SalesInvoiceNo"] = pVal;

                recalculateSummary(pCell, pVal);
            } else {
                retrieveAjaxDataAsync(
                    "Sales.svc", "RetrieveSalesInvoice", pVal,
                    function(item) {
                        dateCell.setValue(item.Date);
                        amtCell.setValue(item.Total);
                        dueCell.setValue(item.Total - item.AmountPaid);
                        payCell.setValue(0);
                        //row.data["SalesInvoiceNo"] = item.SalesInvoiceNo;

                        recalculateSummary(pCell, pVal);
                    },
                    function(txt) {
                        displayError(txt);
                    });
            }
        }
        function recalculateSummary(pCell, pVal) {
            var d = g_subGrid.data;
            var subtotal = 0;
            for (var i = 0; i < d.length; ++i) {
                var val = parseFloat(d[i]["Amount"]);
                if (!isNaN(val))
                    subtotal += val;
            }
            g_subtotal.setValue(subtotal);
            var taxedAmount = parseFloat(subtotal) * parseFloat(g_taxFactor) / 100;
            g_tax.setValue(taxedAmount);
            g_total.setValue(subtotal - taxedAmount);
        }
        function showVoid(fShow) {
            if (fShow)
                $("#tdVoid").empty().append("<div>VOID</div>");
            else
                $("#tdVoid").empty();
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                    $("#btnMail").disable();
                    $("#btnReceive").disable();
                }
                else {
                    var rowData = pRow.data;
                    var fVoid = rowData["IsVoided"];
                    if (fVoid) {
                        $("#btnMainEdit").disable();
                        $("#btnMainDelete").enable();
                        $("#btnMail").disable();
                        $("#btnReceive").disable();
                    } else {
                        $("#btnMainEdit").enable();
                        $("#btnMainDelete").disable();
                        $("#btnMail").enable();
                        $("#btnReceive").enable();
                    }
                }
            }
            else { // subGrid
                if (pRow == null) {
                    $("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    if (pRow.readOnly)
                        $("#btnSubEdit").enable();
                    else
                        $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }

        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            $("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();
            
            $("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();

            if (selectedRow.data["IsVoided"]) {
                if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?"))
                {
                    var data = selectedRow.data;
                    var item = {};
                    item["Id"] = data["Id"];
                    item["Version"] = data["Version"];
                    item["ItemState"] = KGrid.ItemState.deletedItem;
                    //item["Items"] = [];

                    sendAjaxUpdate( "Purchasing.svc", "ManagePurchaseOrder", item,
                        function(rsp) {
                            setPageDirty(false);
                            retrieveMainGridData();
                            changeState(ePageState.mainNew);
                        },
                        function(rsp) {
                            displayError(rsp.StatusText);
                        });
                }
            }
            else {
                displayError("Please VOID the item before deleting it");
            }
        }

        function onSendMail () {
            displayError("To be implemented");
        }
        function onDoReceive () {
            displayError("To be implemented");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-green">
        <ul class="nav nav-tabs">
            <li class="inactive"><a href="#tab1" data-toggle="tab" onclick="changeUrl('CashSales.aspx');">Cash Sales</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('SalesInvoice.aspx');">Sales Invoice</a></li>
            <li class="active"><a href="#" data-toggle="tab">Incoming Payment</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <!-- Form Pane -->
                <div id="divForm" class="section">
                    <table id="tblForm" class="form double">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <select id="selCustomer" placeholder="-- search --" tabindex="1">
                                    </select>
                                </td>
                                <td>Reference:</td>
                                <td>
                                    <input type="text" id="txtRef" />
                                </td>
                            </tr>
                            <tr>
                                <td>Detail:</td>
                                <td>
                                    <input type="text" id="txtAddr1" />
                                </td>
                                <td>Receipt No:</td>
                                <td>
                                    <input type="text" id="txtIpNo" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtAddr2" />
                                </td>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate" style="width: 195px;" />
                                    <img src="../images/icon_calendar.png" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtAddr3" />
                                </td>
                                <td>Status:</td>
                                <td>
                                    <input type="text" id="txtStatus" /></td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <input type="text" id="txtPhone" />
                                </td>
                                <td>Payment Method:</td>
                                <td>
                                    <select id="selPaymentType" placeholder="-- search --">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Bank No:</td>
                                <td>
                                    <input type="text" id="txtBankAccNo" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- Form Pane -->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div id="subGridPane">
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="onSubEdit();" />
                            <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="onSubDelete();" />
                            <input type="button" id="btnSubUndoDelete" class="btn-action" value="Undo" onclick="onSubUndoDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="subGrid" style="height: 180px;">
                    </div>
                    <div class="amount-summary">
                        <table id="tblSummary" class="form summary">
                            <tr>
                                <td class="caption">Note:</td>
                                <td class="control">
                                    <input type="text" id="txtNote" />
                                </td>
                                <td class="caption-amount">Sub total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtSubtotal" />
                                </td>
                            </tr>
                            <tr>
                                <td id="tdVoid" class="pane-void" colspan="2" rowspan="2"></td>
                                <td class="caption-amount">Tax:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTax" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption-amount">Total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTotal" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="header-search">Search</div>
                    <div class="grid-command search">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>
