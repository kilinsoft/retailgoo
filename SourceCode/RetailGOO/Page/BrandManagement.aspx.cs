﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class BrandManagement : System.Web.UI.Page
    {
        public string ItemBrandListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspItemBrand = ItemAdapter.RetrieveItemBrandList();
            if (rspItemBrand.Success && rspItemBrand.ResponseItem != null && rspItemBrand.ResponseItem.Count > 0)
                ItemBrandListString = HtmlSelectHelper.ConvertToListString(rspItemBrand.ResponseItem);
            else
            //    ItemBrandListString = "[{Value:-1,Text:'<anonymous>'}]";
                ItemBrandListString = "[]";

        }
    }
}