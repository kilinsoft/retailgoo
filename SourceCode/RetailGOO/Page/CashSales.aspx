﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master"
    AutoEventWireup="true" CodeBehind="CashSales.aspx.cs" Inherits="RetailGOO.Page.CashSales" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .nav-tabs > li {
            /*width: 160px !important;*/
        }
    </style>
    <link rel="stylesheet" href="<%# ResolveClientUrl("~/css/jquery-ui-1.10.4.custom.min.css") %>" type="text/css" />
    <script src="<%# ResolveClientUrl("~/js/jquery-ui-1.10.4.custom.min.js") %>"></script>
    <script src="<%# ResolveClientUrl("~/js/newitemdialog.js") %>"></script>
    <script type="text/javascript">

        // Control variables
        var g_subtotal, g_tax, g_total, g_discount, g_charge;
        var g_selCustomer, g_selDeliveryTo, g_selAmountType, g_selPaymentType, g_selSalesPerson;
        var g_docTemplate = <%=DocumentTemplate%>;

        // Data variables
        var g_customerList = <%=CustomerListString%>;
        var g_deliverytoList = <%=DeliveryToListString%>;
        var g_amounttypeList = <%=AmountTypeListString%>;
        var g_paymenttypeList = <%=PaymentTypeListString%>;
        var g_salespersonList = <%=SalesPersonListString%>;
        var g_taxFactor = <%=TaxFactor%>;
        var g_itemList = null;
        var g_cell = null;
        
        function pageLoad() {
               
            preventNavigationOnUnsaved(true);

            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "CS Number", name: "CashSalesNo", required: false, editable: false },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Status", name: "Status", required: false, editable: false },
            { title: "Customer Name", name: "CustomerName", required: false, editable: false },
            { title: "Amount", name: "Total", width: 150, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) },
            //{ title: "Due", name: "Due", required: false, editable: false }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                onRowMouseEnter: mgOnRowMouseEnter,
                onRowMouseLeave: mgOnRowMouseLeave
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            var selEditorOptions = {
                //create: true,
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                onLoad: onLoadHandler,
                options: [],
                create: function(input, callback) {
                    doNewItemDialog(input);
                }
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Item", name: "ItemId", width: 300, required: true, editable: true, 
                editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions),
                onChanged: onItemChangedHandler
            },
            { title: "Qty", name: "Quantity", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0), 
                onChanged: recalculateAmount },
            { title: "Price", name: "Price", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2), 
                onChanged: recalculateAmount },     
            { title: "Amount", name: "Amount", required: true, editable: false, formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) }
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            $("#txtDate").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);
            $("#txtDateDelivery").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            g_selCustomer = $("#selCustomer").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_customerList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveCustomerInfo(val);
                    }
                },
            })[0].selectize;

            g_selDeliveryTo = $("#selDeliveryTo").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_deliverytoList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveDeliveryToInfo(val);
                    }
                },
            })[0].selectize;

            g_selAmountType = $("#selAmountType").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_amounttypeList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveAmountTypeInfo(val);
                    }
                },
            })[0].selectize;

            g_selPaymentType= $("#selPaymentType").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_paymenttypeList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrievePaymentTypeInfo(val);
                    }
                },
            })[0].selectize;

            g_selSalesPerson = $("#selSalesPerson").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_salespersonList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew)
                            retrieveSalesPersonInfo(val);
                    }
                },
            })[0].selectize;

            // Summary calculation
            var funcOnTaxChanged = function($e, val) {
                var tax = parseFloat(val);
                var subtotal = parseFloat(g_subtotal.getValue());
                var total = tax + subtotal;
                g_total.setValue(total);
            };
            var funcOnSubtotalChanged = function($e, val) { 
                var subtotal = parseFloat(val);
                var discount = parseFloat(g_discount.getValue());
                var charge = parseFloat(g_charge.getValue());
                var taxedAmount = parseFloat(g_taxFactor) * (subtotal + charge - discount);
                g_tax.setValue(taxedAmount);
                funcOnTaxChanged(g_tax.$element, taxedAmount); // Call onTaxChanged next
            };
            var funcOnDiscountChanged = function($e, val) { 
                var subtotal = parseFloat(g_subtotal.getValue());
                var discount = parseFloat(val);
                var charge = parseFloat(g_charge.getValue());
                var taxedAmount = parseFloat(g_taxFactor) * (subtotal + charge - discount);
                g_tax.setValue(taxedAmount);
                funcOnTaxChanged(g_tax.$element, taxedAmount); // Call onTaxChanged next
            };
            var funcOnChargeChanged = function($e, val) { 
                var subtotal = parseFloat(g_subtotal.getValue());
                var discount = parseFloat(g_discount.getValue());
                var charge = parseFloat(val);
                var taxedAmount = parseFloat(g_taxFactor) * (subtotal + charge - discount);
                g_tax.setValue(taxedAmount);
                funcOnTaxChanged(g_tax.$element, taxedAmount); // Call onTaxChanged next
            };
            g_subtotal = new Util.Input.Amount("#txtSubtotal", 2, funcOnSubtotalChanged);
            g_tax = new Util.Input.Amount("#txtTax", 2, funcOnTaxChanged);
            g_total = new Util.Input.Amount("#txtTotal", 2);
            g_discount = new Util.Input.Amount("#txtDiscount", 2, funcOnDiscountChanged);
            g_charge = new Util.Input.Amount("#txtCharge", 2, funcOnChargeChanged);
            
            initGridSorter(g_subGrid, "#selSubSelectedBy",
                [{ title: "Item", name: "OptionName", numberComparison: false }, 
                { title: "Qty", name: "Quantity", valueCompare: true },
                { title: "Price", name: "Price", valueCompare: true },
                { title: "Amount", name: "Amount", valueCompare: true }]);
            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "CS Number", name: "CashSalesNo", valueCompare: false }, 
                { title: "Date", name: "Date", valueCompare: false },
                { title: "Status", name: "Status", valueCompare: false },
                { title: "Customer Name", name: "CustomerName", valueCompare: false },
                { title: "Amount", name: "Total", valueCompare: true },
                { title: "Due", name: "Due", valueCompare: true }]);

            // Init elements' behaviors
            $("#selPaymentMethod").disable();
            $("#btnSubEdit").disable();
            //$("#selAmountCal").disable();

            // Init element's values

            // Init state
            changeState(ePageState.mainNew);

            // Behaviors
            g_subtotal.disable();
            //g_tax.disable();
            g_total.disable();

            retrieveMainGridData();
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#txtNote").readOnly(true);
            g_selCustomer.disable();
            g_selDeliveryTo.disable();
            g_selAmountType.disable();
            g_selPaymentType.disable();
            g_selSalesPerson.disable();
            $("#txtDate, #txtDateDelivery").prop('disabled', true);
            g_subtotal.disable();
            g_discount.disable();
            g_charge.disable();
            g_tax.disable();
            g_total.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtCsNo").readOnly(true);
            //$("#selPaymentMethod").prop("disabled", true);
            $("#txtDate, #txtDateDelivery").prop('disabled', false);
            $("#txtNote").readOnly(false);
            g_selCustomer.enable();
            g_selDeliveryTo.enable();
            g_selAmountType.enable();
            g_selPaymentType.enable();
            g_selSalesPerson.enable();
            //g_subtotal.enable(); // disable Subtotal
            g_discount.enable();
            g_charge.enable();
            g_tax.enable();
            //g_total.enable(); // disable total
        }

        function setFormData(item) {
            g_selCustomer.setValue(item["CustomerId"]);
            $("#txtRef").val(item.Reference);
            $("#txtContactPerson").val(item.ContactPerson);
            $("#txtCsNo").val(item.CashSalesNo);
            $("#txtDate").datepicker("setDate", new Date(item["Date"]));
            $("#txtAddr1").val(item.Address1);
            $("#txtAddr2").val(item.Address2);
            $("#txtAddr3").val(item.Address3);
            $("#txtPhone").val(item.Phone);
            $("#txtCreditTerm").val(item.CreditTerm);
            $("#txtNote").val(item.Note);
            $("#txtStatus").val(item["Status"]);
            $("#txtDateDelivery").datepicker("setDate", new Date(item["DateDelivery"]));
            g_selDeliveryTo.setValue(item["DestinationTypeId"]);
            g_selAmountType.setValue(item["AmountTypeId"])
            g_selPaymentType.setValue(item["PaymentTypeId"])
            g_selSalesPerson.setValue(item["SalesPersonId"]);

            showVoid(item["Status"].toLowerCase() == "void");

            g_subtotal.setValue(item.Subtotal);
            g_discount.setValue(item.Discount);
            g_charge.setValue(item.Charge);
            g_tax.setValue(item.Tax);
            g_total.setValue(item.Total);
        }
        function clearFormData() {
            if (g_customerList.length > 0)
                g_selCustomer.setValue(-1);
            $("#tblForm input[type=text]").val("");
            $("#txtStatus").val("New");
            $("#txtNote").val("");
            $("#txtDate").datepicker("setDate", new Date());
            $("#txtDateDelivery").datepicker("setDate", new Date());
            if (g_deliverytoList.length > 0)
                g_selDeliveryTo.setValue(1);
            if (g_amounttypeList.length > 0)
                g_selAmountType.setValue(1);
            if (g_paymenttypeList.length > 0)
                g_selPaymentType.setValue(1);
            if (g_salespersonList.length > 0)
                g_selSalesPerson.setValue(1);
            
            g_subtotal.setValue(0);
            g_discount.setValue(0);
            g_charge.setValue(0);
            g_tax.setValue(0);
            g_total.setValue(0);
        }

        function retrieveSubGridData(SalesInvoiceId, funcOnSuccess) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveAllCashSalesItem", SalesInvoiceId,
                function (items) {
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveAllCashSales", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(SalesInvoiceId) {
            retrieveAjaxDataSync("Sales.svc", "RetrieveCashSales", SalesInvoiceId,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveCustomerInfo(customerId)
        {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveCustomerInfo", customerId,
                function (item) {
                    $("#txtContactPerson").val(item.ContactPerson);
                    $("#txtAddr1").val(item.AddressBilling1);
                    $("#txtAddr2").val(item.AddressBilling2);
                    $("#txtAddr3").val(item.AddressBilling3);
                    $("#txtPhone").val(item.Phone);
                });
        }
        function retrieveSalesPersonInfo(salespersonId)
        {
            // HIM: to be implemented
        }
        function retrieveDeliveryToInfo(deliverytoId)
        {
            // HIM: to be implemented
        }
        function retrieveAmountTypeInfo(amounttypeId)
        {
            // HIM: to be implemented
        }
        function retrievePaymentTypeInfo(paymenttypeid)
        {
            // HIM: to be implemented
        }
        function displayMainItemData(item) {
            setFormData(item);
            retrieveSubGridData(item["SalesInvoiceId"]);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            return true;
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    showVoid(false);
                    enableSaveClear();

                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formLock();
                    subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }

        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable(); //already active
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable(); //already active
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if (g_selCustomer.getValue() == "") {
                displayError("Please specify a customer");
                return false;
            }
            if (g_selDeliveryTo.getValue() == "") {
                displayError("Please specify a delivery to");
                return false;
            }
            if (g_selAmountType.getValue() == "") {
                displayError("Please specify a amount");
                return false;
            }
            if (g_selPaymentType.getValue() == "") {
                displayError("Please specify a payment");
                return false;
            }
            if (g_selSalesPerson.getValue() == "") {
                displayError("Please specify a sales person");
                return false;
            }
            if ($("#txtDate").val() == "") {
                displayError("Please specify the Date");
                return false;
            }
            if (!validateFloat(g_subtotal.getValue()) || !g_subtotal.isValid()) {
                displayError("Invalid Subtotal");
                return false;
            }
            if (!validateFloat(g_tax.getValue()) || !g_tax.isValid()) {
                displayError("Invalid Tax");
                return false;
            }
            if (!validateFloat(g_total.getValue()) || !g_total.isValid()) {
                displayError("Invalid Total");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on row [" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            item["SalesInvoiceId"] = row == null ? -1 : row.data["SalesInvoiceId"];
            item["IsCashSales"] = row == null ? true : row.data["IsCashSales"];
            item["CashSalesNo"] = row == null ? -1 : row.data["CashSalesNo"];
            item["CustomerId"] = g_selCustomer.getValue();
            item["CustomerName"] = g_selCustomer.getOption(g_selCustomer.getValue())[0].innerText;
            item["Reference"] = $("#txtRef").val();
            item["ContactPerson"] = $("#txtContactPerson").val();
            item["Address1"] = $("#txtAddr1").val();
            item["Address2"] = $("#txtAddr2").val();
            item["Address3"] = $("#txtAddr3").val();
            item["Phone"] = $("#txtPhone").val();
            item["Date"] = convertToUTCDateTime($("#txtDate").val());
            item["DateDelivery"] = convertToUTCDateTime($("#txtDateDelivery").val());
            item["DestinationTypeId"] = g_selDeliveryTo.getValue();
            item["DestinationTypeName"] = g_selDeliveryTo.getOption(g_selDeliveryTo.getValue())[0].innerText;
            item["AmountTypeId"] = g_selAmountType.getValue();
            item["AmountTypeName"] = g_selAmountType.getOption(g_selAmountType.getValue())[0].innerText;
            item["PaymentTypeId"] = g_selPaymentType.getValue();
            item["PaymentTypeName"] = (g_selPaymentType.getValue() == 0) ? "<anonymous>" : g_selPaymentType.getOption(g_selPaymentType.getValue())[0].innerText;
            item["Note"] = $("#txtNote").val();
            item["SalesPersonId"] = g_selSalesPerson.getValue();
            item["SalesPersonName"] = g_selSalesPerson.getOption(g_selSalesPerson.getValue())[0].innerText;
            item["Subtotal"] = g_subtotal.getValue();
            item["Discount"] = g_discount.getValue();
            item["Charge"] = g_charge.getValue();
            item["Tax"] = g_tax.getValue();
            item["Total"] = g_total.getValue();
            item["AmountPaid"] = 0;
            item["Status"] = "New";
            item["ItemState"] = (g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem : KGrid.ItemState.modifiedItem);
            item["Version"] = row == null ? "" : row.data["Version"];
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                //if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                if (row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    subItem["SalesInvoiceItemId"] = row.data["SalesInvoiceItemId"] == null ? -1 : parseInt(row.data["SalesInvoiceItemId"], 10);
                    subItem["SalesInvoiceId"] = row.data["SalesInvoiceId"] == null ? -1 : parseInt(row.data["SalesInvoiceId"], 10);
                    subItem["ItemId"] = !isNaN(parseInt(row.data["ItemId"], 10)) ? parseInt(row.data["ItemId"], 10) : -1;
                    subItem["ItemFullName"] = row.data["ItemFullName"];
                    subItem["Quantity"] = row.data["Quantity"];
                    subItem["Price"] = parseFloat(row.data["Price"]);
                    subItem["Amount"] = parseFloat(row.data["Amount"]);
                    subItem["ItemState"] = row.rowState;
                    item["Items"].push(subItem);
                }
            }
            return item;
        }

        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "Sales.svc", "ManageCashSales", item,
                function(rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        if (selectedRow != null)
                            selectedRow.select(null, false);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            var row = g_mainGrid.selectedRow;
            if (row == null) {
                displayError("Please select an item first");
                return;
            }
            if (row.data["IsVoided"]) {
                displayError("The item has already been marked as VOID");
                return; 
            }
            var fContinue = window.confirm("Voiding the item will affect all of its related items as well.\n\nDo you want to continue?");
            if (fContinue) {
                var idVoid = parseInt(row.data["SalesInvoiceId"], 10);
                sendAjaxUpdate( "Sales.svc", "VoidCashSales", idVoid,
                    function(rsp) {
                        setPageDirty(false);
                        changeState(ePageState.mainView);
                        var iRow = g_mainGrid.selectedRow.rowIndex;
                        retrieveMainGridData(function() {
                            g_mainGrid.selectRow(iRow);
                        });
                    },
                    function(rsp) {
                        displayError(rsp.StatusText);
                    });
            }
        }
        function doClear() {
            var fClear = window.confirm("Your unsaved data will be lost.\nDo you want to continue?");
            if (fClear) {
                setPageDirty(false);
                switch(g_pageState) {
                    case ePageState.mainNew:
                        changeState(ePageState.mainNew);
                        break;
                    case ePageState.mainView:
                        break;
                    case ePageState.mainEdit:
                        if (g_mainGrid.data.length > 0)
                            setFormData(g_mainGrid.data[g_mainGrid.selectedRow.rowIndex]);
                        setGridData(g_subGrid, g_subItems);
                        g_subGrid.unlock();
                        changeState(ePageState.mainEdit);
                        //changeState(ePageState.mainNew);
                        break;
                    default:
                        break;
                }
            }
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/CashSalesA.aspx";
            var docTemplateBUrl = "../Printable/CashSalesB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Cash Sales", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('IncomingPayment.aspx');
        }
        function goNext() {
            changeUrl('SalesInvoice.aspx');
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["SalesInvoiceId"]);
                }
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function mgOnRowMouseEnter(pRow) {
            delayDisplayTooltip(displayTableView, pRow.data["SalesInvoiceId"]);
        }
        function mgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function displayTableView(value) {
            //$("#divDynamicTooltip").appendTo("body"); // So the div does not display relatively to its parent but the body instead
            //retrieveAjaxDataSync("Sales.svc", "RetrieveCashSales", value,
            sendAjax("../Service/Sales.svc/RetrieveCashSales", value,
                function (items) {
                    var item = items;
                    var tooltipContent;
                    tooltipContent = 'Delivery To: ' + g_selDeliveryTo.getOption(item['DestinationTypeId'])[0].innerText + '</br>';
                    tooltipContent += 'Delivery Date: ' + item["DateDelivery"] + '</br>';
                    tooltipContent += 'Sales Person: ' + g_selSalesPerson.getOption(item['SalesPersonId'])[0].innerText;
                    $('#divDynamicTooltip').html(tooltipContent);
                    $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
                },
                function (type, err, errThrown) {
                });
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        function sgOnItemChanged(pCell, val) {
        }
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_subItems, "ItemId", pVal, "ItemFullName");
        }
        function onLoadHandler(pCell, pCallback) {
            g_cell = pCell;
            retrieveAjaxDataAsync( 
                "ItemManagement.svc", "RetrieveItemList", {},
                function(items) {
                    g_itemList = items;
                    pCallback(g_itemList);
                },
                function (txt) {
                    displayError(txt);
                    pCallback();
                });
        }
        function onItemChangedHandler(pCell, pVal) {
            var row = pCell.parentRow;
            var qtyCell = row.getCellByColumnName("Quantity");
            var priceCell = row.getCellByColumnName("Price");
            var amtCell = row.getCellByColumnName("Amount");

            if (pVal === "") {
                if (row.rowState == KGrid.ItemState.newItem) {
                    qtyCell.setValue("");
                    priceCell.setValue("");
                    amtCell.setValue("");
                }
                else
                    return;
            } else if (isNaN(parseInt(pVal, 10))) { // user specify non-existent item
                qtyCell.setValue(0);
                priceCell.setValue(0);
                amtCell.setValue(0);
                //row.data["OptionName"] = pVal;

                recalculateAmount(pCell, pVal);
            } else {
                retrieveAjaxDataAsync(
                    "ItemManagement.svc", "RetrieveItem", pVal,
                    function(item) {
                        qtyCell.setValue(0);
                        priceCell.setValue(item.Price);
                        //row.data["OptionName"] = item.OptionName;

                        recalculateAmount(pCell, pVal);
                    },
                    function(txt) {
                        displayError(txt);
                    });
            }
        }
        function recalculateAmount(pCell, pVal) {
            var row = pCell.parentRow;
            var qtyCell = row.getCellByColumnName("Quantity");
            var priceCell = row.getCellByColumnName("Price");
            var amtCell = row.getCellByColumnName("Amount");
            var qty = qtyCell.value;
            var price = priceCell.value;
            if (qty === "")
                qty = 0;
            if (price === "")
                price = 0;
            qtyCell.setValue(qty);
            priceCell.setValue(price);
            amtCell.setValue( parseFloat(qty) * parseFloat(price) );

            recalculateSummary();
        }
        function recalculateSummary() {
            var d = g_subGrid.data;
            var subtotal = 0;
            for (var i = 0; i < d.length; ++i) {
                var val = parseFloat(d[i]["Amount"]);
                if (!isNaN(val))
                    subtotal += val;
            }
            g_subtotal.setValue(subtotal);
            var discount = g_discount.getValue();
            var charge = g_charge.getValue();
            var taxedAmount = parseFloat(g_taxFactor) * (parseFloat(subtotal) + parseFloat(charge) - parseFloat(discount));
            g_tax.setValue(taxedAmount);
            g_total.setValue(taxedAmount + subtotal);
        }
        function showVoid(fShow) {
            if (fShow)
                $("#tdVoid").empty().append("<div>VOID</div>");
            else
                $("#tdVoid").empty();
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                    $("#btnMail").disable();
                    $("#btnReceive").disable();
                }
                else {
                    var rowData = pRow.data;
                    var fVoid = rowData["IsVoided"];
                    if (fVoid) {
                        $("#btnMainEdit").disable();
                        $("#btnMainDelete").enable();
                        $("#btnMail").disable();
                        $("#btnReceive").disable();
                    } else {
                        $("#btnMainEdit").enable();
                        $("#btnMainDelete").disable();
                        $("#btnMail").enable();
                        $("#btnReceive").enable();
                    }
                }
            }
            else { // subGrid
                if (pRow == null) {
                    //$("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    //if (pRow.readOnly)
                    //    $("#btnSubEdit").enable();
                    //else
                    //    $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }

        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            //$("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();
            
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("SalesInvoiceId"));
                //$firstInput.focus();
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();

            if (selectedRow.data["IsVoided"]) {
                if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?"))
                {
                    var data = selectedRow.data;
                    var item = {};
                    item["SalesInvoiceId"] = data["SalesInvoiceId"];
                    item["Version"] = data["Version"];
                    item["ItemState"] = KGrid.ItemState.deletedItem;
                    //item["Items"] = [];

                    sendAjaxUpdate( "Sales.svc", "ManageCashSales", item,
                        function(rsp) {
                            setPageDirty(false);
                            retrieveMainGridData();
                            changeState(ePageState.mainNew);
                        },
                        function(rsp) {
                            displayError(rsp.StatusText);
                        });
                }
            }
            else {
                displayError("Please VOID the item before deleting it");
            }
        }

        function onSendMail () {
            displayError("To be implemented");
        }
        function onDoReceive () {
            displayError("To be implemented");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-blue">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Cash Sales</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('SalesInvoice.aspx');">Sales Invoice</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('IncomingPayment.aspx')">Incoming Payment</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <!-- Form Pane -->
                <div id="divForm" class="section">
                    <table id="tblForm" class="form double">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <select id="selCustomer" placeholder="-- search --">
                                    </select>
                                </td>
                                <td>Reference:</td>
                                <td>
                                    <input type="text" id="txtRef" />
                                </td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>
                                    <input type="text" id="txtContactPerson" />
                                </td>
                                <td>CS Number:</td>
                                <td>
                                    <input type="text" id="txtCsNo" />
                                </td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>
                                    <input type="text" id="txtAddr1" />
                                </td>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate" style="width: 195px;" />
                                    <img src="../images/icon_calendar.png" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtAddr2" />
                                </td>
                                <td>Amount:</td>
                                <td>
                                    <select id="selAmountType" placeholder="-- search --">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtAddr3" />
                                </td>
                                <td>Payment Method:</td>
                                <td>
                                    <select id="selPaymentType" placeholder="-- search --">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <input type="text" id="txtPhone" />
                                </td>
                                <td>Sales Person</td>
                                <td>
                                    <select id="selSalesPerson" placeholder="-- search --">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Delivery To:</td>
                                <td>
                                    <select id="selDeliveryTo" placeholder="-- search --">
                                    </select>
                                </td>
                                <td></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>Delivery Date:</td>
                                <td>
                                    <input type="text" id="txtDateDelivery" style="width: 195px;" />
                                    <img src="../images/icon_calendar.png" />
                                <td></td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- Form Pane -->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div id="subGridPane">
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="onSubEdit();" />
                            <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="onSubDelete();" />
                            <input type="button" id="btnSubUndoDelete" class="btn-action" value="Undo" onclick="onSubUndoDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="subGrid" style="height: 180px;">
                    </div>
                    <div class="amount-summary">
                        <table id="tblSummary" class="form summary">
                            <tr>
                                <td class="caption">Note:</td>
                                <td class="control">
                                    <input type="text" id="txtNote" />
                                </td>
                                <td class="caption-amount">Sub total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtSubtotal" />
                                </td>
                            </tr>
                            <tr>
                                <td id="tdVoid" class="pane-void" colspan="2" rowspan="4"></td>
                                <td class="caption-amount">Discount:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtDiscount"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="caption-amount">Charge:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtCharge"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="caption-amount">Tax:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTax" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption-amount">Total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTotal" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="header-search">Search</div>
                    <div class="grid-command search">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                            <input type="button" id="btnMail" class="btn-action text-teal" value="Mail" onclick="onSendMail();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>
