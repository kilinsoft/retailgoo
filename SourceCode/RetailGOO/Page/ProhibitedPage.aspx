﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NoAuth.Master" AutoEventWireup="true" CodeBehind="ProhibitedPage.aspx.cs" Inherits="RetailGOO.Page.ProhibitedPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setTimeout(function () { window.location = "Dashboard.aspx"; }, 3000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div>
        <h3>
            This screen is prohibited. You will be redirected to the main screen in 3 seconds.
        </h3>
    </div>
</asp:Content>
