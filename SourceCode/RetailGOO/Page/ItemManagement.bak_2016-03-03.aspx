﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="ItemManagement.aspx.cs" Inherits="RetailGOO.Page.ItemManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .custom-file {
            float: left;
            position: relative;
            height: 20px;
        }

            .custom-file #file {
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
                height: 20px;
                opacity: 0;
            }

            .custom-file span {
                float: left;
                font-size: 11px;
                height: 18px;
            }

                .custom-file span.text {
                    width: 100px;
                }

                .custom-file span.button {
                    border: 1px solid gray;
                }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#file').change(function(){
                $(this).siblings('.text').text(this.value || 'Nothing selected')
            });
        })

        // Data variables\
        var g_categoryList = <%=CategoryListString%>;
        var g_brandList = <%=BrandListString%>;

        // Control variables
        var g_selCategory;
        var g_selBrand;
        var g_cost;
        var g_price;

        function pageLoad() {
               
            preventNavigationOnUnsaved(true);

            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Item Code", name: "ItemGroupId", required: false, editable: false },
            { title: "Item Name", name: "ItemGroupName", required: false, editable: false },
            { title: "Item Category", name: "ItemCategoryName", required: false, editable: false },
            { title: "Brand", name: "ItemBrandName", required: false, editable: false },
            { title: "Quantity", name: "Quantity", required: false, editable: false, formatter: KGrid.Formatter.Amount(0) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            //{ title: "Item Code", name: "ItemGroupCode", required: false, editable: false },
            { title: "Option Name", name: "ItemName", required: true, editable: true },
            { title: "Barcode", name: "Barcode", required: false, editable: true },
            { title: "Quantity", name: "Quantity", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0) },     
            { title: "Cost", name: "Cost", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) },
            { title: "Price", name: "Price", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2) }
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected,
                onRowChanged: sgOnRowChanged
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            g_selCategory = $("#selCategory").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_categoryList
            })[0].selectize;

            g_selBrand = $("#selBrand").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_brandList
            })[0].selectize;

            g_cost = new Util.Input.Amount("#txtCost", 2, null);
            g_price = new Util.Input.Amount("#txtPrice", 2, null);

            initGridSorter(g_subGrid, "#selSubSelectedBy",
                [{ title: "Option Name", name: "ItemName", numberComparison: false }, 
                { title: "Quantity", name: "Quantity", valueCompare: true },
                { title: "Cost", name: "Cost", valueCompare: true },
                { title: "Price", name: "Price", valueCompare: true }]);
            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "Item Code", name: "ItemGroupCode", valueCompare: false }, 
                { title: "Item Category", name: "ItemCategoryName", valueCompare: false },
                { title: "Brand", name: "ItemBrandName", valueCompare: false },
                { title: "Quantity", name: "Quantity", valueCompare: true }]);

            // Init element's values

            // Init state
            changeState(ePageState.mainNew);

            // Behaviors
            $("#txtItemCode").readOnly(true);
            $("#btnSubEdit").disable();
            $("#btnVoid").addClass('disable').disable();

            retrieveMainGridData();
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#btnChooseFile").disable();
            g_selCategory.disable();
            g_selBrand.disable();
            g_cost.disable();
            g_price.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtItemCode").readOnly(true);
            $("#btnChooseFile").enable();
            g_selCategory.enable();
            g_selBrand.enable();
            g_cost.enable();
            g_price.enable();
        }

        function setFormData(item) {
            $("#txtItemName").val(item["ItemGroupName"]);
            //$("#txtItemCode").val(item["ItemGroupCode"]);
            $("#txtItemCode").val(item["ItemGroupId"]);
            g_selBrand.setValue(item["ItemBrandId"]);
            g_selCategory.setValue(item["ItemCategoryId"]);
            g_cost.setValue(item["Cost"]);
            g_price.setValue(item["Price"]);
            $("#txtUom").val(item["UnitOfMeasure"]);
            $("#chkForSales").prop('checked', item["IsForSales"]);
        }
        function clearFormData() {
            $("#txtItemName").val("");
            $("#txtItemCode").val("");
            g_selBrand.setValue(-1);
            g_selCategory.setValue(-1);
            g_cost.setValue(0);
            g_price.setValue(0);
            $("#txtUom").val("");
            $("#chkForSales").prop('checked', false);
        }

        function retrieveSubGridData(itemGroupId, funcOnSuccess) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveAllItem", itemGroupId,
                function (items) {
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveAllItemGroup", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(id) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveItemGroup", id,
                function (item) {
                    displayMainItemData(item);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });

            retrieveAjaxDataSync("ItemManagement.svc", "LoadItemImage", id,
                function (item) {
                    var dto = item;
                    var imgitem = document.getElementById("imgItem");
                    $("#imgItem").attr("src", dto["UrlData"]);
                    $('#imgName').val(dto['ItemFileName']);
                });
        }

        function displayMainItemData(item) {
            setFormData(item);
            retrieveSubGridData(item["ItemGroupId"], null);
        }

        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            var fCanDelete = false;
            var iItems = 0;
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.deletedItem)
                    ++iItems;
            }
            if (iItems > 1) // At least 2 items left
                fCanDelete = true;
            return fCanDelete;
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    //showVoid(false);
                    break;
                case ePageState.mainView:
                    formLock();
                    subSectionLock();
                    mainSectionUnlock();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    subSectionUnlock();
                    mainSectionLock();
                    break;
            }
        }

        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable();
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }

        function validateForm() {
            $("#tblForm tblForm2").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if ($("#txtItemName").val() === "") {
                displayError("Please specify Item Name");
                return false;
            }
            if ($("#txtUom").val() === "") {
                displayError("Please specify Unit of Measurement");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on SubGrid row[" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;

            item["ItemGroupId"] = row != null ? row.data["ItemGroupId"] : -1;
            item["ItemGroupName"] = $("#txtItemName").val();
            item["ItemGroupCode"] = '';
            item["ItemCategoryId"] = g_selCategory.getValue() != "" ? g_selCategory.getValue() : -1;
            item["ItemBrandId"] = g_selBrand.getValue() != "" ? g_selBrand.getValue() : -1;
            item["Cost"] = g_cost.getValue();
            item["Price"] = g_price.getValue();
            item["IsForSales"] = ($("#chkForSales").prop('checked') == true);
            //item["ImagePath"] = "./sago.jpg"; // Pro : ToDo : Upload Image
            item["UnitOfMeasure"] = $("#txtUom").val();
            item["ItemState"] = g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem :
                g_pageState == ePageState.mainEdit ? KGrid.ItemState.modifiedItem : KGrid.ItemState.deletedItem;
            item["Version"] = row != null ? row.data["Version"] : "";
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    subItem["ItemId"] = row.data["ItemId"] != null ? row.data["ItemId"] : -1;
                    subItem["ItemName"] = row.data["ItemName"];
                    subItem["Barcode"] = row.data["Barcode"];
                    subItem["Quantity"] = parseFloat(row.data["Quantity"]);
                    subItem["Cost"] = parseFloat(row.data["Cost"]);
                    subItem["Price"] = parseFloat(row.data["Price"]);
                    subItem["ItemState"] = row.rowState;
                    item["Items"].push(subItem);
                }
            }

            return item;
        }
        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "ItemManagement.svc", "ManageItemGroup", item,
                function (rsp) {
                    // convert the image to the base64 string
                    var jpegUrl = document.getElementById("imgItem").src;

                    var imageitem = {};
                    imageitem["ItemGroupId"] = item["ItemGroupId"];
                    imageitem["ItemFileName"] = $('#imgName').val();
                    imageitem["UrlData"] = jpegUrl;

                    retrieveAjaxDataSync("ItemManagement.svc", "SaveItemImage", imageitem,
                       function (item) {
                       });

                    setPageDirty(false);
                    changeState(ePageState.mainNew);
                    retrieveMainGridData();
                    //displayInfo("Success");
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            // Not support on this screen
        }
        function doClear() {
            doDefaultClear();
        }
        function doPrint() {

        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            // do nothing
        }
        function goNext() {
            changeUrl('CategoryManagement.aspx');
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["ItemGroupId"]);
                }    
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        function sgOnRowChanged(pRow, pCell) {
            //var itemGroupCodeCell = pRow.getCellByColumnName("ItemGroupCode");
            //if (pRow.rowState == KGrid.ItemState.newItem)
            //    itemGroupCodeCell.setValue(g_activeMainItem["ItemGroupCode"]);
            //else if (pRow.rowState == KGrid.ItemState.blankItem)
            //    itemGroupCodeCell.setValue("");
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                }
                else {
                    $("#btnMainEdit").enable();
                    $("#btnMainDelete").enable();
                }
            }
            else { // subGrid
                if (pRow == null) {
                    //$("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    //if (pRow.readOnly)
                    //    $("#btnSubEdit").enable();
                    //else
                    //    $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }

        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            //$("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();
            
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?")) {
                $("#btnMainDelete").disable();
                g_pageState = ePageState.mainDelete;
                doSave();
            }

            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();
        }
        
        function btnChooseFileOnClick() {
            var uploadItemUrl = "../Upload/UploadItemImage.aspx";
            var popup = window.open(uploadItemUrl, "Upload Item Image", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = document.getElementById("imgItem").src;
            popup.dto = dto;
        }
        function btnChooseFileOnClick2() {
            var $inputFile = $("#imageFile");
            if ($inputFile.isInit != true) {
                //$inputFile.change(function() {
                //    var inputElement = $inputFile[0];
                //    if (inputElement.files && inputElement.files[0]) {
                //        var reader = new FileReader();
                //        reader.onload = function(e) {
                //            $('#imgItem').attr('src', e.target.result);

                //            var imgProperty = getImagePropertyFromFileInput(inputElement);
                //        }

                //        reader.readAsDataURL(inputElement.files[0]);
                //    }
                //});

                var _URL = window.URL || window.webkitURL;

                $inputFile.change(function(e) {
                    var file, img;
                    if ((file = this.files[0])) {
                        img = new Image();
                        img.onload = function() {
                            alert(this.width + " " + this.height);
                        };
                        img.onerror = function() {
                            alert( "not a valid file: " + file.type);
                        };
                        img.src = _URL.createObjectURL(file);

                    }

                });

                $inputFile.isInit = true;
            }
            $inputFile.click();

            
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-blue">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('AdjustStock.aspx');">Adjust Stock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('TransferStock.aspx');">Transfer Stock</a></li>
            <li class="inactive" style="width: 304px;"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveTransferStock.aspx')">Receive TransferStock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReorderStock.aspx');">Reorder Stock</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="active"><a href="#" data-toggle="tab" onclick="changeUrl('ItemManagement.aspx');">Item Management</a></li>
            <li class="inactive" style="width: 184px;"><a href="#" data-toggle="tab" onclick="changeUrl('CategoryManagement.aspx');">Category Management</a></li>
            <li class="inactive" style="width: 180px;"><a href="#" data-toggle="tab" onclick="changeUrl('BrandManagement.aspx')">Brand Management</a></li>
            <li class="inactive" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CountSheet.aspx');">Count Sheet</a></li>
            <li class="inactive" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CurrentStock.aspx');">Current Stock</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <!--form-->
                    <table id="tblForm" class="form double">
                        <tr>
                            <td>Item Name:</td>
                            <td>
                                <input type="text" id="txtItemName" tabindex="1" />
                            </td>
                            <td rowspan="6" class="cell-control left" style="width: 160px;">
                                <div style="height: 168px; width: 100%; background-color: #DDDDDD">
                                    <img src="" id="imgItem" />
                                </div>
                            </td>
                            <td rowspan="6" class="cell-control left" style="vertical-align: bottom;">
                                <input type="file" id="imageFile" style="visibility: hidden;" />

                                <input type="button" id="btnChooseFile" value="Choose File"
                                    class="btn-action-big btn-blue" onclick="btnChooseFileOnClick();"
                                    tabindex="7" />
                                <input type="text" id="imgName" hidden />
                            </td>
                        </tr>
                        <tr>
                            <td>Item Code:</td>
                            <td>
                                <input type="text" id="txtItemCode" name="formItemCode" tabindex="2" />
                            </td>
                        </tr>
                        <tr>
                            <td>Category:</td>
                            <td>
                                <select id="selCategory" tabindex="3">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Brand:</td>
                            <td>
                                <select id="selBrand" tabindex="4">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Cost:</td>
                            <td>
                                <input type="text" id="txtCost" name="formCost" tabindex="5" />
                            </td>
                        </tr>
                        <tr>
                            <td>Price:</td>
                            <td>
                                <input type="text" id="txtPrice" name="formPrice" tabindex="6" />
                            </td>
                        </tr>
                        <tr>
                            <td>Unit of Measurement:</td>
                            <td>
                                <input type="text" id="txtUom" tabindex="7" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>For Sales:</td>
                            <td>
                                <input type="checkbox" id="chkForSales" tabindex="8" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <!--form/end-->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <!-- SubGrid Pane -->
                <div id="subGridPane" class="section">
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="onSubEdit();" />
                            <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="onSubDelete();" />
                            <input type="button" id="btnSubUndoDelete" class="btn-action" value="Undo" onclick="onSubUndoDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="subGrid" style="height: 180px;">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="header-search">Search</div>
                    <div class="grid-command search">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>

            </div>
            <!--tab-pane-->
        </div>
        <!--tab-content-->
    </div>
</asp:Content>
