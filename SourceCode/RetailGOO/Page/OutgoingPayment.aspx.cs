﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class OutgoingPayment : System.Web.UI.Page
    {
        public string SupplierListString { get; set; }
        public string PaymentTypeListString { get; set; }
        public string TaxFactor { get; set; }
        public int DocumentTemplate { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspSupplier = SupplierAdapter.RetrieveSupplierList();
            if (rspSupplier.Success && rspSupplier.ResponseItem != null && rspSupplier.ResponseItem.Count > 0)
                SupplierListString = HtmlSelectHelper.ConvertToListString(rspSupplier.ResponseItem);
            else
                SupplierListString = "[{Value:-1,Text:'<anonymous>'}]";

            var rspPaymentType = PurchaseAdapter.RetrievePaymentTypeList();
            if (rspPaymentType.Success && rspPaymentType.ResponseItem != null && rspPaymentType.ResponseItem.Count > 0)
                PaymentTypeListString = HtmlSelectHelper.ConvertToListString(rspPaymentType.ResponseItem);
            else
            {
                // Should not reach here
                Logger.SessionFatal("Could not retrieve PaymentTypeList. Check database schema or data retrieval process");
            }

            TaxFactor = SessionHelper.BusinessInfo.Tax.ToString();
            DocumentTemplate = SessionHelper.BusinessInfo.DocSetting[BusinessScreenString.PurchaseOrder];
        }
    }
}