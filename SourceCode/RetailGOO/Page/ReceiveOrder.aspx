﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="ReceiveOrder.aspx.cs" Inherits="RetailGOO.Page.ReceiveOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .nav-tabs > li {
            width: 160px !important;
        }
    </style>
    <link rel="stylesheet" href="<%# ResolveClientUrl("~/css/jquery-ui-1.10.4.custom.min.css") %>" type="text/css" />
    <script src="<%# ResolveClientUrl("~/js/jquery-ui-1.10.4.custom.min.js") %>"></script>
    <script src="<%# ResolveClientUrl("~/js/newitemdialog.js") %>"></script>
    <script type="text/javascript">

        // Control variables
        var g_subtotal, g_tax, g_total;
        var g_selSupplier, g_selAddPo;
        var g_docTemplate = <%=DocumentTemplate%>;

        // Data variables
        var g_purchaseOrderList = [];
        var g_supplierList = <%=SupplierListString%>;
        var g_taxFactor = <%=TaxFactor%>;
        var g_itemList = <%=ItemListString%>;
        var g_cell = null;

        function pageLoad() {
               
            preventNavigationOnUnsaved(true);

            // Initialize variables

            var selEditorOptions = {
                //create: true,
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                options: g_itemList,
                create: function(input, callback) {
                    doNewItemDialog(input);
                }
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "PO", name: "PurchaseOrderNo", width: 100, required: false, editable: false },
            { title: "Item", name: "ItemId", width: 250, required: true, editable: true, 
                editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions),
                onChanged: onItemChangedHandler },
            //{ title: "Description", name: "Description", required: false, editable: false },
            { title: "PO Qty", name: "QuantityOrdered", required: false, editable: false, formatter: KGrid.Formatter.Amount(0) },
            { title: "Qty", name: "Quantity", required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0),
                onChanged: recalculateAmount },
            { title: "UOM", name: "UnitOfMeasure", required: false, editable: false },
            { title: "Price", name: "Price", width: 100, required: true, editable: true, 
                formatter: KGrid.Formatter.Amount(2), editor: KGrid.Editor.Amount(2), 
                onChanged: recalculateAmount },     
            { title: "Amount", name: "Amount", width: 100, required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "RO Number", name: "ReceiveOrderNo", required: false, editable: false },
            { title: "Date", name: "Date", required: false, editable: false },
            { title: "Status", name: "Status", required: false, editable: false },
            { title: "Supplier Name", name: "SupplierName", required: false, editable: false },
            { title: "Total", name: "Total", required: false, editable: false, formatter: KGrid.Formatter.Amount(2) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                onRowMouseEnter: mgOnRowMouseEnter,
                onRowMouseLeave: mgOnRowMouseLeave
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            $("#txtDate").datepicker({autoclose: true, dateFormat: "yy-mm-dd", todayBtn: "linked" }).prop("readonly", true);

            g_selSupplier = $("#selSupplier").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_supplierList,
                onChange: function (val) {
                    if (val != "" && val != -1) {
                        if (g_pageState == ePageState.mainNew) {
                            retrieveSupplierInfo(val);
                            retrievePurchaseOrderList(val);
                        }
                    }
                }
            })[0].selectize;

            g_selAddPo = $("#selAddPo").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_purchaseOrderList
            })[0].selectize;

            // Summary calculation
            var funcOnTaxChanged = function($e, val) {
                var tax = parseFloat(val);
                var subtotal = parseFloat(g_subtotal.getValue());
                var total = tax + subtotal;
                g_total.setValue(total);
            };
            var funcOnSubtotalChanged = function($e, val) { 
                var subtotal = parseFloat(val);
                var taxedAmount = parseFloat(g_taxFactor) * subtotal;
                g_tax.setValue(taxedAmount);
                funcOnTaxChanged(g_tax.$element, taxedAmount); // Call onTaxChanged next
            };
            g_subtotal = new Util.Input.Amount("#txtSubtotal", 2, funcOnSubtotalChanged);
            g_tax = new Util.Input.Amount("#txtTax", 2, funcOnTaxChanged);
            g_total = new Util.Input.Amount("#txtTotal", 2);

            initGridSorter(g_subGrid, "#selSubSelectedBy",
                [{ title: "PO", name: "PurchaseOrderNo", valueCompare: false }, 
                { title: "Item", name: "Date", valueCompare: false },
                { title: "PO Qty", name: "QuantityOrdered", valueCompare: true },
                { title: "Qty", name: "Quantity", valueCompare: true },
                { title: "Amount", name: "Total", valueCompare: true }]);
            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "PO Number", name: "PurchaseOrderNo", numberComparison: false }, 
                { title: "Date", name: "Date", valueCompare: false },
                { title: "Status", name: "Status", valueCompare: false },
                { title: "Supplier Name", name: "SupplierName", valueCompare: false },
                { title: "Total", name: "Total", valueCompare: true }]);

            // Init elements' behaviors
            $("#txtStatus").readOnly(true);
            $("#btnSubEdit").disable();

            // Init element's values

            // Init state
            changeState(ePageState.mainNew);

            // Behaviors
            g_subtotal.disable();
            //g_tax.disable();
            g_total.disable();

            retrieveMainGridData();
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#txtNote").readOnly(true);
            g_selSupplier.disable();
            g_selAddPo.disable();
            $("#btnAddPo").disable();
            $("#txtDate").prop('disabled', true);
            g_subtotal.disable();
            g_tax.disable();
            g_total.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#txtStatus").readOnly(true);
            $("#txtRoNo").readOnly(true);
            $("#txtDate").prop('disabled', false);
            $("#txtNote").readOnly(false);
            g_selSupplier.enable();
            g_selAddPo.enable();
            $("#btnAddPo").enable();
            //g_subtotal.enable();
            g_tax.enable();
            //g_total.enable();
        }

        function setFormData(item) {
            g_selSupplier.setValue(item["SupplierId"]);
            $("#txtRef").val(item.Reference);
            $("#txtContactPerson").val(item.ContactPerson);
            $("#txtRoNo").val(item.ReceiveOrderNo);
            $("#txtDate").datepicker("setDate", new Date(item["Date"]));
            $("#txtAddr1").val(item.Address1);
            $("#txtAddr2").val(item.Address2);
            $("#txtAddr3").val(item.Address3);
            $("#txtStatus").val(item.Status);
            $("#txtPhone").val(item.Phone);
            $("#txtPaymentTerm").val(item.PaymentTerm);
            $("#txtNote").val(item.Note);
            showVoid(item["Status"].toLowerCase() == "void");

            g_subtotal.setValue(item.Subtotal);
            g_tax.setValue(item.Tax);
            g_total.setValue(item.Total);
        }
        function clearFormData() {
            if (g_supplierList.length > 0)
                g_selSupplier.setValue(-1);
            $("#tblForm input[type=text]").val("");
            $("#txtStatus").val("New");
            $("#txtNote").val("");
            $("#txtDate").datepicker("setDate", new Date());

            g_subtotal.setValue(0);
            g_tax.setValue(0);
            g_total.setValue(0);
        }

        function retrieveSubGridData(id, funcOnSuccess) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllReceiveOrderItem", id,
                function (items) {
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllReceiveOrder", {}, 
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(id) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveReceiveOrder", id,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveSupplierInfo(supplierId) {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveSupplierInfo", supplierId,
                function (item) {
                    $("#txtContactPerson").val(item["ContactPerson"]);
                    $("#txtAddr1").val(item["Address1"]);
                    $("#txtAddr2").val(item["Address2"]);
                    $("#txtAddr3").val(item["Address3"]);
                    $("#txtPhone").val(item["Phone"]);
                });
        }
        function retrievePurchaseOrderList(supplierId) {
            retrieveAjaxDataSync("Purchasing.svc", "RetrievePurchaseOrderList", supplierId,
                function (items) {
                    setComboBoxData(g_selAddPo, items, null);
                });
        }
        function displayMainItemData(item) {
            setFormData(item);
            retrieveSubGridData(item["ReceiveOrderId"]);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            return true;
        }
        function preventOriginalPoItemEdition() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState == KGrid.ItemState.originalItem)
                {
                    var poNoCell = row.getCellByColumnName("PurchaseOrderNo");
                    if (poNoCell != null && poNoCell.value !== "") { // Item belongs to PO
                        var itemIdCell = row.getCellByColumnName("ItemId");
                        var priceCell = row.getCellByColumnName("Price");
                        itemIdCell.setReadOnly(true);
                        priceCell.setReadOnly(true);
                    }
                }
            }
        }
        function addPoItemsToGrid() {
            if (g_selAddPo.getValue() === "") {
                displayInfo("Please select a PurchaseOrder item to add");
                return;
            }
            var id = parseInt(g_selAddPo.getValue());
            if (isNaN(id)) {
                displayError("Invalid PurchaseOrder");
                return;
            }
            var poNo = g_selAddPo.getOption(g_selAddPo.getValue())[0].innerText;
            g_selAddPo.setValue("");
            retrieveAjaxDataSync("Purchasing.svc", "RetrieveAllPurchaseOrderItem", id,
                function(items) {
                    // Check for existing items in the subGrid
                    var d = g_subGrid.data;
                    var oldLen = d.length;
                    for (var iItems = 0; iItems < items.length; ++iItems) {
                        item = items[iItems];
                        var fExists = false;
                        for (var i = 0; i < d.length; ++i) {
                            var ri = d[i];
                            if (item["PurchaseOrderItemId"] == ri["PurchaseOrderItemId"] &&
                                item["ItemId"] == ri["ItemId"]) {
                                fExists = true;
                                break;
                            }
                        }
                        if (!fExists) { // Add to subGrid
                            var tmpItem = item;
                            tmpItem["ReceiveOrderItemId"] = -1;
                            tmpItem["QuantityOrdered"] = item["Quantity"]; // Convert Qty from PurchaseOrderItem to PoQty in ReceiveOrderItem
                            //tmpItem["Quantity"] = 0;
                            tmpItem["PurchaseOrderItemId"] = item["PurchaseOrderItemId"];
                            tmpItem["PurchaseOrderNo"] = poNo;
                            tmpItem.itemState = KGrid.ItemState.newItem;
                            //g_subGrid.data.push(tmpItem);
                            var newRow = g_subGrid.addRow(tmpItem);

                            // Prohibit changing of Item and Price
                            var itemIdCell = newRow.getCellByColumnName("ItemId");
                            var priceCell = newRow.getCellByColumnName("Price");
                            itemIdCell.setReadOnly(true);
                            priceCell.setReadOnly(true);
                        }
                    }
                    if (oldLen != d.length) {
                        g_subItems = g_subGrid.data;
                        //g_subGrid.setData(g_subItems);
                        recalculateSummary();
                    }
                },
                function(rsp) {
                    displayError("Could not retrieve PurchaseOrder items");
                });
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    showVoid(false);
                    enableSaveClear();

                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formLock();
                    subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }

        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable(); //already active
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable(); //already active
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();  
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if (g_selSupplier.getValue() == "") {
                displayError("Please specify a supplier");
                return false;
            }
            if ($("#txtDate").val() == "") {
                displayError("Please specify the Date");
                return false;
            }
            if (!validateFloat(g_subtotal.getValue()) || !g_subtotal.isValid()) {
                displayError("Invalid Subtotal");
                return false;
            }
            if (!validateFloat(g_tax.getValue()) || !g_tax.isValid()) {
                displayError("Invalid Tax");
                return false;
            }
            if (!validateFloat(g_total.getValue()) || !g_total.isValid()) {
                displayError("Invalid Total");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on row [" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            item["ReceiveOrderId"] = row == null ? -1 : row.data["ReceiveOrderId"];
            item["BusinessId"] = -1;
            item["ReceiveOrderNo"] = row == null ? -1 : row.data["ReceiveOrderNo"];
            item["SupplierId"] = g_selSupplier.getValue();
            item["SupplierName"] = g_selSupplier.getOption(g_selSupplier.getValue())[0].innerText;
            item["Reference"] = $("#txtRef").val();
            item["ContactPerson"] = $("#txtContactPerson").val();
            item["Address1"] = $("#txtAddr1").val();
            item["Address2"] = $("#txtAddr2").val();
            item["Address3"] = $("#txtAddr3").val();
            item["Date"] = convertToUTCDateTime($("#txtDate").val());
            item["Status"] = "New";
            item["Phone"] = $("#txtPhone").val();
            item["PaymentTerm"] = $("#txtPaymentTerm").val();
            item["Note"] = $("#txtNote").val();
            item["Subtotal"] = g_subtotal.getValue();
            item["Tax"] = g_tax.getValue();
            item["Total"] = g_total.getValue();
            item["ItemState"] = (g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem : KGrid.ItemState.modifiedItem);
            item["Version"] = row == null ? "" : row.data["Version"];
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                //if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                if (row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    subItem["ReceiveOrderItemId"] = row.data["ReceiveOrderItemId"] == null ? -1 : parseInt(row.data["ReceiveOrderItemId"], 10);
                    subItem["PurchaseOrderItemId"] = parseInt(row.data["PurchaseOrderItemId"], 10);
                    subItem["PurchaseOrderNo"] = row.data["PurchaseOrderNo"];
                    subItem["ItemId"] = !isNaN(parseInt(row.data["ItemId"], 10)) ? parseInt(row.data["ItemId"], 10) : -1;
                    subItem["ItemFullName"] = row.data["ItemFullName"] == null ? "" : row.data["ItemFullName"];
                    //subItem["PoQuantity"] = row.data["PoQuantity"];
                    //subItem["Description"] = row.data["Description"] == null ? "" : row.data["Description"];
                    subItem["QuantityOrdered"] = parseFloat(row.data["QuantityOrdered"]);
                    subItem["Quantity"] = parseFloat(row.data["Quantity"]);
                    subItem["UnitOfMeasure"] = row.data["UnitOfMeasure"];
                    subItem["Price"] = parseFloat(row.data["Price"]);
                    subItem["Amount"] = parseFloat(row.data["Amount"]);
                    subItem["ItemState"] = row.rowState;
                    item["Items"].push(subItem);
                }
            }
            return item;
        }
        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "Purchasing.svc", "ManageReceiveOrder", item,
                function(rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        selectedRow.select(null, true);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
            doDefaultVoid("Purchasing.svc", "VoidReceiveOrder", "ReceiveOrderId");
        }
        function doClear() {
            if (preventNavigationOnUnsaved() && isPageDirty())
                doDefaultClear();
        }
        function doPrint() {
            var docTemplateAUrl = "../Printable/ReceiveOrderA.aspx";
            var docTemplateBUrl = "../Printable/ReceiveOrderB.aspx";
            var docTemplateUrl = docTemplateAUrl;
            if (g_docTemplate == 1)
                docTemplateUrl = docTemplateBUrl;
            var popup = window.open(docTemplateUrl, "Receive Order", "resizable=0,status=0,titlebar=0,toolbar=0");
            var dto = constructDtoItem();
            popup.dto = dto;
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('PurchaseOrder.aspx');
        }
        function goNext() {
            changeUrl('OutgoingPayment.aspx');
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["ReceiveOrderId"]);
                }    
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function mgOnRowMouseEnter(pRow) {
            delayDisplayTooltip(displayTableView, pRow.data["ReceiveOrderId"]);
        }
        function mgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function displayTableView(value) {
            //$("#divDynamicTooltip").appendTo("body"); // So the div does not display relatively to its parent but the body instead
            //retrieveAjaxDataSync("Purchasing.svc", "RetrieveReceiveOrder", value,
            sendAjax("../Service/Purchasing.svc/RetrieveReceiveOrder", value,
                function (rsp) {
                    var item = rsp.ResponseItem;
                    var tooltipContent;
                    tooltipContent = 'Date: ' + item['Date'] + '</br>';
                    tooltipContent += 'Payment Term: ' + item['PaymentTerm'];
                    $('#divDynamicTooltip').html(tooltipContent);
                    $("#divDynamicTooltip").css("left", g_mousePos.x).css("top", g_mousePos.y).show();
                },
                function (type, err, errThrown) {
                });
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        //function sgOnItemChanged(pCell, val) {
        //}
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_itemList, "Value", pVal, "Text");
        }
        function onItemChangedHandler(pCell, pVal) {
            var row = pCell.parentRow;
            var uomCell = row.getCellByColumnName("UnitOfMeasure");
            var qtyCell = row.getCellByColumnName("Quantity");
            var priceCell = row.getCellByColumnName("Price");
            var amtCell = row.getCellByColumnName("Amount");
            var poQtyCell = row.getCellByColumnName("QuantityOrdered");
            var poNoCell = row.getCellByColumnName("PurchaseOrderNo");

            if (pVal === "") {
                if (row.rowState == KGrid.ItemState.newItem) {
                    uomCell.setValue("");
                    qtyCell.setValue("");
                    priceCell.setValue("");
                    amtCell.setValue("");
                    poQtyCell.setValue("");
                    poNoCell.setValue("");
                }
                else
                    return;
            } else if (isNaN(parseInt(pVal, 10))) { // user specify non-existent item
                uomCell.setValue("Unit");
                qtyCell.setValue(0);
                priceCell.setValue(0);
                poQtyCell.setValue(0);
                poNoCell.setValue("");
                row.data["ItemFullName"] = pVal;
                row.data["PurchaseOrderItemId"] = -1;

                recalculateAmount(pCell, pVal);
            } else {
                retrieveAjaxDataAsync(
                    "ItemManagement.svc", "RetrieveItem", pVal,
                    function(item) {
                        uomCell.setValue(item.UnitOfMeasure);
                        qtyCell.setValue(0);
                        priceCell.setValue(item.Price);
                        poQtyCell.setValue(0);
                        poNoCell.setValue("");
                        row.data["ItemFullName"] = item["ItemFullName"];
                        row.data["PurchaseOrderItemId"] = -1;

                        recalculateAmount(pCell, pVal);
                    },
                    function(txt) {
                        displayError(txt);
                    });
            }
        }
        function recalculateAmount(pCell, pVal) {
            var row = pCell.parentRow;
            var qtyCell = row.getCellByColumnName("Quantity");
            var priceCell = row.getCellByColumnName("Price");
            var amtCell = row.getCellByColumnName("Amount");
            var qty = qtyCell.value;
            var price = priceCell.value;
            if (qty === "")
                qty = 0;
            if (price === "")
                price = 0;
            amtCell.setValue( parseFloat(qty) * parseFloat(price) );

            recalculateSummary();
        }
        function recalculateSummary() {
            var d = g_subGrid.data;
            var subtotal = 0;
            for (var i = 0; i < d.length; ++i) {
                var val = parseFloat(d[i]["Amount"]);
                if (!isNaN(val))
                    subtotal += val;
            }
            g_subtotal.setValue(subtotal);
            var taxedAmount = parseFloat(g_taxFactor) * parseFloat(subtotal);
            g_tax.setValue(taxedAmount);
            g_total.setValue(taxedAmount + subtotal);
        }
        function showVoid(fShow) {
            if (fShow)
                $("#tdVoid").empty().append("<div>VOID</div>");
            else
                $("#tdVoid").empty();
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                    $("#btnPay").disable();
                    //$("#btnMail").disable();
                    //$("#btnReceive").disable();
                }
                else {
                    var rowData = pRow.data;
                    var fVoid = (rowData["Status"].toLowerCase() == "void");
                    if (fVoid) {
                        $("#btnMainEdit").disable();
                        $("#btnMainDelete").enable();
                        $("#btnPay").disable();
                        //$("#btnMail").disable();
                        //$("#btnReceive").disable();
                    } else {
                        $("#btnMainEdit").enable();
                        $("#btnMainDelete").disable();
                        $("#btnPay").enable();
                        //$("#btnMail").enable();
                        //$("#btnReceive").enable();
                    }
                }
            }
            else { // subGrid
                if (pRow == null) {
                    //$("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    //if (pRow.readOnly)
                    //    $("#btnSubEdit").enable();
                    //else
                    //    $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }

        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            //$("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();
            
            //$("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            if (!canEditMainItem(g_activeMainItem)) {
                displayInfo("Cannot edit this type of item");
                return;
            }
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
            
            preventOriginalPoItemEdition();
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();

            if (selectedRow.data["Status"].toLowerCase() == "void") {
                if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?"))
                {
                    var data = selectedRow.data;
                    var item = {};
                    item["ReceiveOrderId"] = data["ReceiveOrderId"];
                    item["Version"] = data["Version"];
                    item["ItemState"] = KGrid.ItemState.deletedItem;
                    //item["Items"] = [];

                    sendAjaxUpdate( "Purchasing.svc", "ManageReceiveOrder", item,
                        function(rsp) {
                            setPageDirty(false);
                            retrieveMainGridData();
                            changeState(ePageState.mainNew);
                        },
                        function(rsp) {
                            displayError(rsp.StatusText);
                        });
                }
            }
            else {
                displayError("Please VOID the item before deleting it");
            }
        }
        function canEditMainItem(item) {
            // Pro : ToDo
            return true;
        }

        function onPay () {
            displayError("To be implemented");
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-red">
        <ul class="nav nav-tabs">
            <li class="inactive"><a href="#tab1" data-toggle="tab" onclick="changeUrl('CashPurchase.aspx');">Cash Purchase</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('PurchaseOrder.aspx');">Purchase Order</a></li>
            <li class="active"><a href="#" data-toggle="tab">Receive Order</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('OutgoingPayment.aspx')">Outgoing Payment</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('Expense.aspx');">Expense</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <!-- Form Pane -->
                <div id="divForm" class="section">
                    <table id="tblForm" class="form double">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <select id="selSupplier" placeholder="-- search --">
                                        <option value="1">Select Supplier</option>
                                    </select>
                                </td>
                                <td>Reference:</td>
                                <td>
                                    <input type="text" id="txtRef" />
                                </td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>
                                    <input type="text" id="txtContactPerson" />
                                </td>
                                <td>RO Number:</td>
                                <td>
                                    <input type="text" id="txtRoNo" />
                                </td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>
                                    <input type="text" id="txtAddr1" />
                                </td>
                                <td>Date:</td>
                                <td>
                                    <input type="text" id="txtDate" style="width: 195px;" />
                                    <img src="../images/icon_calendar.png" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" id="txtAddr2" />
                                </td>
                                <td>Status:</td>
                                <td>
                                    <input type="text" id="txtStatus" />
                                </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>
                                    <input type="text" id="txtPhone" />
                                </td>
                                <td>Payment Term:</td>
                                <td>
                                    <input type="text" id="txtPaymentTerm" />
                                </td>
                            </tr>
    <%--                        <tr>
                                <td>Delivery Date:</td>
                                <td>
                                    <input type="text" id="txtDateDelivery" />
                                </td>
                                <td>Credit Term:</td>
                                <td>
                                    <input type="text" id="txtNo" />
                                </td>
                            </tr>--%>
                            <tr>
                                <td>Purchase Order</td>
                                <td>
                                    <%--<textarea id="txtPurchaseOrder" rows="3" style="height: 60px; resize: none;"></textarea>--%>
                                    <select id="selAddPo" placeholder="-- search --"></select>
                                </td>
                                <td></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: right;">
                                     <input type="button" id="btnAddPo" class="btn-action-big btn-red" value="Add to List" onclick="addPoItemsToGrid();" />
                                </td>
                                <td></td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- Form Pane -->
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div id="subGridPane" class="section">
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="onSubEdit();" />
                            <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="onSubDelete();" />
                            <input type="button" id="btnSubUndoDelete" class="btn-action" value="Undo" onclick="onSubUndoDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="subGrid" style="height: 180px;">
                    </div>
                    <div class="amount-summary">
                        <table id="tblSummary" class="form summary">
                            <tr>
                                <td class="caption">Note:</td>
                                <td class="control">
                                    <input type="text" id="txtNote" />
                                </td>
                                <td class="caption-amount">Sub total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtSubtotal" />
                                </td>
                            </tr>
                            <tr>
                                <td id="tdVoid" class="pane-void" colspan="2" rowspan="2"></td>
                                <td class="caption-amount">Tax:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTax" />
                                </td>
                            </tr>
                            <tr>
                                <td class="caption-amount">Total:
                                </td>
                                <td class="control-amount">
                                    <input type="text" id="txtTotal" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="header-search">Search</div>
                    <div class="grid-command search">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                            <input type="button" id="btnPay" class="btn-action text-teal" value="Pay" onclick="onPay();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" class="search" style="height: 180px;">
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>
