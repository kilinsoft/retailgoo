﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="CategoryManagement.aspx.cs" Inherits="RetailGOO.Page.CategoryManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>

    </style>
    <link rel="stylesheet" href="<%# ResolveClientUrl("~/css/jquery-ui-1.10.4.custom.min.css") %>" type="text/css" />
    <script src="<%# ResolveClientUrl("~/js/jquery-ui-1.10.4.custom.min.js") %>"></script>
    <script src="<%# ResolveClientUrl("~/js/newitemdialog.js") %>"></script>
    <script type="text/javascript">
        // Control variables
        var g_selCategory;

        // Data variables
        var g_categoryList = <%=ItemCategoryListString%>;

        // Variables declaration
        function pageLoad() {

            preventNavigationOnUnsaved(true);

            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "Category ID", name: "ItemCategoryId", required: false, editable: false },
            { title: "Category Name", name: "ItemCategoryName", required: false, editable: false },
            { title: "Item Qty", name: "ItemQuantity", width: 150, required: false, editable: false, formatter: KGrid.Formatter.Amount(0) }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected,
                onRowDeselected: mgOnRowDeselected,
                onRowMouseEnter: mgOnRowMouseEnter,
                onRowMouseLeave: mgOnRowMouseLeave
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            var selEditorOptions = {
                //create: true,
                loadOnDemand: true,
                valueField: "Value",
                textField: "Text",
                getValueFromTextField: false,
                onTextRequested: onTextRequestedHandler,
                onLoad: onLoadHandler,
                options: [],
                create: function (input, callback) {
                    doNewItemDialog(input);
                }
            };
            var selFormattorOptions = {
                valueField: "Value",
                textField: "Text",
                onTextRequested: onTextRequestedHandler
            };
            var subGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            {
                title: "ItemCode", name: "ItemGroupId", width: 300, required: true, editable: true,
                editor: KGrid.Editor.Selection(selEditorOptions), formatter: KGrid.Formatter.Selection(selFormattorOptions),
                onChanged: onItemChangedHandler
            },
            { title: "Brand", name: "ItemBrandName", required: false, editable: false },
            { title: "Item Qty", name: "ItemQuantity", required: false, editable: false, formatter: KGrid.Formatter.Amount(0), editor: KGrid.Editor.Amount(0) }
            ];
            var subGridDef = {
                editable: true,
                enableAddBlankRow: true,
                onRowSelected: sgOnRowSelected
            }
            g_subGrid = new KGrid.Grid("#subGrid", subGridDef, subGridColsDef, []);

            g_selCategory = $("#selCategory").selectize({
                create: true, valueField: 'Value', labelField: 'Text', searchField: ['Text'],
                options: g_categoryList,
                onChange: function (val) {
                    if (val != "")
                        retrieveMainItemData(val, /*fPreventDropDownListUpdate*/ true);
                },
            })[0].selectize;

            initGridSorter(g_subGrid, "#selSubSelectedBy",
                [{ title: "ItemCode", name: "ItemGroupId", valueCompare: false },
                { title: "Brand", name: "ItemBrandName", valueCompare: false },
                { title: "Item Qty", name: "ItemQuantity", valueCompare: false }]);
            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "Category ID", name: "ItemCategoryId", valueCompare: false },
                { title: "Category Name", name: "ItemCategoryName", valueCompare: false }]);

            // Init state
            changeState(ePageState.mainNew);

            // Initialize behaviors
            $("#btnVoid").addClass('disable').disable();

            // Ajax calls to initialize mandatory variables
            retrieveMainGridData();
        }
        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            g_selCategory.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            g_selCategory.enable();
        }
        function setFormData(item) {
            g_selCategory.setValue(item["ItemCategoryId"]);
        }
        function clearFormData() {
            if (g_categoryList.length > 0)
                g_selCategory.setValue(-1);
        }
        function retrieveSubGridData(ItemCategoryId, funcOnSuccess) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveAllItemCategoryItem", ItemCategoryId,
                function (items) {
                    for (var i = 0; i < items.length; ++i) {
                        var item = items[i];
                        if (item["ItemBrandName"] == "")
                            item["ItemBrandName"] = "-";
                    }
                    setGridData(g_subGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveAllItemCategory", {},
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(ItemCategoryId, fPreventDropDownListUpdate) {
            retrieveAjaxDataSync("ItemManagement.svc", "RetrieveItemCategory", ItemCategoryId,
                function (item) {
                    g_activeMainItem = item;
                    displayMainItemData(g_activeMainItem, fPreventDropDownListUpdate);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function setGridData(grid, data) {
            if (grid === g_mainGrid) {
                g_mainItems = data;
                $("#selMainSelectedBy option:first-child").prop("selected", true);
            }
            else if (grid === g_subGrid) {
                g_subItems = data;
                $("#selSubSelectedBy option:first-child").prop("selected", true);
            }
            grid.setData(data);
        }
        function displayMainItemData(item, fPreventDropDownListUpdate) {
            if (!fPreventDropDownListUpdate)
                setFormData(item);
            retrieveSubGridData(item["ItemCategoryId"]);
        }
        function setSubGridData(aoData) {
            g_subItems = aoData;
            setGridData(g_subGrid, aoData);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function clearSubGrid() {
            setGridData(g_subGrid, []);
        }
        function canDeleteSubItem(iRowIndex) {
            return true;
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            updateCommandButtons("sub", g_subGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    clearSubGrid();
                    subSectionUnlock();
                    mainSectionUnlock();
                    //showVoid(false);
                    enableSaveClear();

                    g_activeMainItem = null;

                    break;
                case ePageState.mainView:
                    formUnlock();
                    subSectionLock();
                    mainSectionUnlock();
                    disableSaveClear();
                    break;
                case ePageState.mainEdit:
                    formLock();
                    subSectionUnlock();
                    mainSectionLock();
                    enableSaveClear();
                    break;
            }
        }
        // Grid controls
        function subSectionLock() {
            g_subGrid.lock();
            //$("#btnSubEdit").disable(); //already active
            $("#btnSubDelete").disable();
            $("#selSubSelectedBy").disable();
        }
        function subSectionUnlock() {
            g_subGrid.unlock();
            //$("#btnSubEdit").enable(); //already active
            //$("#btnSubDelete").enable();
            $("#selSubSelectedBy").enable();
        }
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        function validateForm() {
            $("#tblForm").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if (g_selCategory.getValue() == "") {
                displayError("Please specify a category");
                return false;
            }
            return true;
        }
        function validateSubItems() {
            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                if (row.rowState != KGrid.ItemState.blankItem &&
                    row.rowState != KGrid.ItemState.originalItem) {
                    if (!row.validate()) {
                        displayError("A data on row [" + (i+1) + "] is missing or invalid");
                        return false;
                    }
                }
            }
            return true;
        }
        function constructDtoItem() {
            var item = {};
            var row = g_mainGrid.selectedRow;
            item["ItemCategoryId"] = row == null ? -1 : row.data["ItemCategoryId"];
            item["ItemCategoryName"] = g_selCategory.getOption(g_selCategory.getValue())[0].innerText;
            item["ItemState"] = (g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem : KGrid.ItemState.modifiedItem);
            item["Items"] = [];

            for (var i = 0; i < g_subGrid.rows.length; ++i) {
                var row = g_subGrid.rows[i];
                //if (row.rowState != KGrid.ItemState.originalItem && row.rowState != KGrid.ItemState.blankItem) {
                if (row.rowState != KGrid.ItemState.blankItem) {
                    var subItem = {};
                    subItem["ItemGroupId"] = row.data["ItemGroupId"] == null ? -1 : parseInt(row.data["ItemGroupId"], 10);
                    //subItem["ItemBrandName"] = row.data["ItemBrandName"];
                    //subItem["ItemQuantity"] = row.data["ItemQuantity"];
                    subItem["ItemState"] = row.rowState != KGrid.ItemState.deletedItem ? KGrid.ItemState.modifiedItem : KGrid.ItemState.deletedItem;
                    item["Items"].push(subItem);
                }
            }
            return item;
        }
        function doSave() {
            if (!validateForm())
                return false;
            if (!validateSubItems())
                return false;

            var item = constructDtoItem();

            sendAjaxUpdate( "ItemManagement.svc", "ManageItemCategory", item,
                function(rsp) {
                    setPageDirty(false);
                    if (g_pageState == ePageState.mainEdit) {
                        changeState(ePageState.mainView);
                        var selectedRow = g_mainGrid.selectedRow;
                        g_mainGrid.clearSelection();
                        if (selectedRow != null)
                            selectedRow.select(null, false);
                    }
                    else {
                        changeState(ePageState.mainNew);
                        retrieveMainGridData();
                    }
                },
                function(rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doVoid() {
        }
        function doClear() {
            var fClear = window.confirm("Your unsaved data will be lost.\nDo you want to continue?");
            if (fClear) {
                setPageDirty(false);
                switch(g_pageState) {
                    case ePageState.mainNew:
                        changeState(ePageState.mainNew);
                        break;
                    case ePageState.mainView:
                        break;
                    case ePageState.mainEdit:
                        if (g_mainGrid.data.length > 0)
                            setFormData(g_mainGrid.data[g_mainGrid.selectedRow.rowIndex]);
                        setGridData(g_subGrid, g_subItems);
                        g_subGrid.unlock();
                        changeState(ePageState.mainEdit);
                        //changeState(ePageState.mainNew);
                        break;
                    default:
                        break;
                }
            }
        }
        function doPrint() {
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('ItemManagement.aspx');
        }
        function goNext() {
            changeUrl('BrandManagement.aspx');
        }
        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["ItemCategoryId"]);
                }
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }
        function mgOnRowDeselected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", false);
        }
        function mgOnRowMouseEnter(pRow) {
            //delayDisplayTooltip(displayTableView, pRow.data["ItemCategoryId"]);
        }
        function mgOnRowMouseLeave(pRow) {
            cancelDisplayTooltip();
            $("#divDynamicTooltip").hide();
        }
        function sgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            updateCommandButtons("sub", g_subGrid.selectedRow);
        }
        function onTextRequestedHandler(pCell, pVal) {
            return KGrid.Function.getAssocValue(g_subItems, "ItemGroupId", pVal, "ItemGroupName");
        }
        function onLoadHandler(pCell, pCallback) {
            retrieveAjaxDataAsync(
                "ItemManagement.svc", "RetrieveItemGroupList", {},
                function (items) {
                    g_itemList = items;
                    pCallback(g_itemList);
                },
                function (txt) {
                    displayError(txt);
                    pCallback();
                });
        }
        function onItemChangedHandler(pCell, pVal) {
            var row = pCell.parentRow;
            var brandCell = row.getCellByColumnName("ItemBrandName");
            var itemQtyCell = row.getCellByColumnName("ItemQuantity");

            if (pVal === "") {
                if (row.rowState == KGrid.ItemState.newItem) {
                    brandCell.setValue("");
                    itemQtyCell.setValue("");
                }
                else
                    return;
            } else if (isNaN(parseInt(pVal, 10))) { // user specify non-existent item
                itemQtyCell.setValue(0);

                //recalculateQtyAfter(pCell, pVal);
            } else {
                retrieveAjaxDataAsync(
                    "ItemManagement.svc", "RetrieveItemGroupInfo", pVal,
                    function (item) {
                        brandCell.setValue(item.ItemBrandName);
                        itemQtyCell.setValue(item.ItemQuantity);

                        //recalculateQtyAfter(pCell, pVal);
                    },
                    function (txt) {
                        displayError(txt);
                    });
            }
        }
        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow == null) {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                    $("#btnMail").disable();
                    $("#btnReceive").disable();
                }
                else {
                    $("#btnMainEdit").enable();
                    $("#btnMainDelete").enable();
                    $("#btnMail").enable();
                    $("#btnReceive").enable();
                }
            }
            else { // subGrid
                if (pRow == null) {
                    $("#btnSubEdit").disable();
                    $("#btnSubDelete").show();
                    $("#btnSubDelete").disable();
                    $("#btnSubUndoDelete").hide();
                } else {
                    if (pRow.readOnly)
                        $("#btnSubEdit").enable();
                    else
                        $("#btnSubEdit").disable();
                    if (pRow.rowState == KGrid.ItemState.deletedItem) {
                        $("#btnSubDelete").hide();
                        $("#btnSubUndoDelete").show();
                    }
                    else if (pRow.rowState == KGrid.ItemState.blankItem) {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").disable();
                        $("#btnSubUndoDelete").hide();
                    }
                    else {
                        $("#btnSubDelete").show();
                        $("#btnSubDelete").enable();
                        $("#btnSubUndoDelete").hide();
                    }
                }
            }
        }
        function onSubEdit() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.setReadOnly(false);
            selectedRow.triggerNextRequiredCell();
            //$("#btnSubEdit").disable();
        }
        function onSubDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (!canDeleteSubItem(selectedRow.rowIndex))
                return;
            if (selectedRow.rowState == KGrid.ItemState.newItem) {
                g_subGrid.clearSelection();
            }
            else
                $("#btnSubUndoDelete").show();

            //$("#btnSubEdit").disable();
            $("#btnSubDelete").hide();
            selectedRow.delete();
        }
        function onSubUndoDelete() {
            var selectedRow = g_subGrid.selectedRow;
            if (selectedRow == null)
                return;
            selectedRow.undoDelete();
            $("#btnSubDelete").show();
            $("#btnSubUndoDelete").hide();
        }
        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
                //$firstInput.focus();
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            $("#btnMainDelete").disable();

            if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?")) {
                var data = selectedRow.data;
                var item = {};
                item["Id"] = data["Id"];
                item["Version"] = data["Version"];
                item["ItemState"] = KGrid.ItemState.deletedItem;
                //item["Items"] = [];

                sendAjaxUpdate("Sales.svc", "ManageSalesInvoice", item,
                    function (rsp) {
                        setPageDirty(false);
                        retrieveMainGridData();
                        changeState(ePageState.mainNew);
                    },
                    function (rsp) {
                        displayError(rsp.StatusText);
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-yellow">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('AdjustStock.aspx');">Adjust Stock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('TransferStock.aspx');">Transfer Stock</a></li>
            <li class="inactive" style="width: 304px;"><a href="#" data-toggle="tab" onclick="changeUrl('ReceiveTransferStock.aspx')">Receive TransferStock</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ReorderStock.aspx');">Reorder Stock</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="changeUrl('ItemManagement.aspx');">ItemManagement</a></li>
            <li class="active" style="width: 184px;"><a href="#" data-toggle="tab" onclick="changeUrl('CategoryManagement.aspx');">Category Management</a></li>
            <li class="inactive" style="width: 180px;"><a href="#" data-toggle="tab" onclick="changeUrl('BrandManagement.aspx')">Brand Management</a></li>
            <%--<li class="inactive" style="width: 140px;"><a href="#" data-toggle="tab" onclick="changeUrl('CountSheet.aspx');">Count Sheet</a></li>--%>
            <li class="inactive" style="width: 280px;"><a href="#" data-toggle="tab" onclick="changeUrl('CurrentStock.aspx');">Current Stock</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <!-- Form Pane -->
                <div id="divForm" class="section">
                    <table id="tblForm" class="form single">
                        <tr>
                            <td>Category Name:</td>
                            <td>
                                <select id="selCategory" placeholder="-- search --" tabindex="1">
                                </select>
                            </td>
                        </tr>
                    </table>
                </div> <!-- Form Pane -->
                <div>
                    <table class="form single">
                        <tr>
                            <td class="cell-header">Item List:</td>
                        </tr>
			        </table>
                </div>
                <div id="subGridPane">
                    <div class="grid-command no-border">
                        <div>
                            <input type="button" id="btnSubEdit" class="btn-action" value="Edit" onclick="onSubEdit();" />
                            <input type="button" id="btnSubDelete" class="btn-action" value="Delete" onclick="onSubDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selSubSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                </div>

                <div id="subGrid" style="height: 180px;">
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div>
                    <table class="form single">
                        <tr>
                            <td class="cell-header">Category List:</td>
                        </tr>
                    </table>
                </div>
                <div class="grid-command no-border">
                    <div>
                        <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                        <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                    </div>
                    <div>
                        <span>Selected by:</span>
                        <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                        </select>
                    </div>
                </div>
                <div id="mainGrid" class="search" style="height: 180px;">
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>

            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div>
    <div id="divDynamicTooltip" class="dynamic-tooltip">
    </div>
</asp:Content>
