﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class CategoryManagement : System.Web.UI.Page
    {
        public string ItemCategoryListString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var rspItemCategory = ItemAdapter.RetrieveItemCategoryList();
            if (rspItemCategory.Success && rspItemCategory.ResponseItem != null && rspItemCategory.ResponseItem.Count > 0)
                ItemCategoryListString = HtmlSelectHelper.ConvertToListString(rspItemCategory.ResponseItem);
            else
                //ItemCategoryListString = "[{Value:-1,Text:'<anonymous>'}]";
                ItemCategoryListString = "[]";

        }
    }
}