﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RetailGOO.BusinessLayer;
using RetailGOO.Util;
using RetailGOO.Dto;
using RetailGOO.Class;
using RetailGOO.Util.Constant;

namespace RetailGOO.Page
{
    public partial class User : System.Web.UI.Page
    {
        public string StaffListString { get; set; }
        public string CountryListString { get; set; }
        public string RoleListString { get; set; }
        public long CurrentStaffId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // If the Staff is not admin or manager. Redirect to ProhibitedPage
            if (SessionHelper.GetCurrentStaff().Role != Staff.GetRoleFromString(StaffRoleString.Admin) &&
                SessionHelper.GetCurrentStaff().Role != Staff.GetRoleFromString(StaffRoleString.Manager))
                Response.Redirect(RetailGooPageUrl.ProhibitedPage);

            var rspStaff = Business.RetrieveStaffList();
            if (rspStaff.Success && rspStaff.ResponseItem != null && rspStaff.ResponseItem.Count > 0)
                StaffListString = HtmlSelectHelper.ConvertToListString(rspStaff.ResponseItem);
            else
                StaffListString = "[]";

            var rspCountry = Business.RetrieveCountryList();
            if (rspCountry.Success && rspCountry.ResponseItem != null && rspCountry.ResponseItem.Count > 0)
                CountryListString = HtmlSelectHelper.ConvertToListString(rspCountry.ResponseItem);
            else
                CountryListString = "[]";

            var rspRole = Business.RetrieveRoleList();
            if (rspRole.Success && rspRole.ResponseItem != null && rspRole.ResponseItem.Count > 0)
                RoleListString = HtmlSelectHelper.ConvertToListString(rspRole.ResponseItem);
            else
                RoleListString = "[]";

            CurrentStaffId = SessionHelper.GetCurrentStaff().StaffId;
        }
    }
}