﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="SupplierInfo.aspx.cs" Inherits="RetailGOO.Page.SupplierInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        .remark
        {
            height: 100px;
        }
    </style>
    <script type="text/javascript">

        // Data variables
        var g_countryList = [];
        var g_supplierGroupList = [];

        // Control vairables
        var g_selSupplierGroup;
        var g_selCountry;

        function pageLoad() {

            preventNavigationOnUnsaved(true);

            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "ID", name: "SupplierId", required: false, editable: false },
            { title: "Name", name: "SupplierName", width: 300, required: false, editable: false },
            { title: "Contact", name: "ContactPerson", required: false, editable: false },
            { title: "Business Tel", name: "Phone", required: false, editable: false },
            { title: "Type", name: "SupplierGroupName", required: false, editable: false }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            g_selSupplierGroup = $("#selSupplierGroup").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            g_selCountry = $("#selCountry").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "Id", name: "SupplierId", valueCompare: true },
                { title: "Name", name: "SupplierName", valueCompare: false },
                { title: "Type", name: "SupplierGroupName", valueCompare: false }]);

            // Initialize behaviors
            $("#btnVoid").addClass("disable");

            // Init state
            changeState(ePageState.mainNew);

            // Ajax calls to initialize mandatory variables
            retrieveSupplierGroupList();
            retrieveCountryList();
            retrieveMainGridData();
            updateCommandButtons("main", null);
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#tblForm2 input[type=text]").readOnly(true);
            $("#txtBtRemark").prop("disabled", true);
            g_selSupplierGroup.disable();
            g_selCountry.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#tblForm2 input[type=text]").readOnly(false);
            $("#txtSupplierNo").readOnly(true);
            $("#txtBtRemark").prop("disabled", false);
            g_selSupplierGroup.enable();
            g_selCountry.enable();
        }

        function setFormData(item) {
            $("#txtName").val(item["SupplierName"]);
            $("#txtSupplierNo").val(item["SupplierId"]);
            g_selSupplierGroup.setValue(item["SupplierGroupId"])
            $("#txtTaxNo").val(item["TaxNo"]);
            //$("#selDefaultPrice").val(selectedItem.dataItem["DefaultPrice"]);
            $("#txtContactPerson").val(item["ContactPerson"]);
            $("#txtEmail").val(item["Email"]);
            $("#txtPhone").val(item["Phone"]);
            $("#txtFax").val(item["Fax"]);
            $("#txtCreditTerm").val(item["CreditTerm"]);
            //$("#txtCreditLimit").val(selectedItem.dataItem["CreditLimit"]);
            $("#txtBtAddr1").val(item["Address1"]);
            $("#txtBtAddr2").val(item["Address2"]);
            $("#txtBtAddr3").val(item["Address3"]);
            $("#txtBtCity").val(item["City"]);
            $("#selBtCountry").val(item["CountryId"]);
            $("#txtBtZipCode").val(item["ZipCode"]);
            $("#txtBtRemark").val(item["Remark"]);
            //$("#txtStAddr1").val(selectedItem.dataItem["ShipToAddress1"]);
            //$("#txtStAddr2").val(selectedItem.dataItem["ShipToAddress2"]);
            //$("#txtStAddr3").val(selectedItem.dataItem["ShipToAddress3"]);
            //$("#txtStCity").val(selectedItem.dataItem["ShipToCity"]);
            //$("#selStCountry").val(selectedItem.dataItem["ShipToCountry"]);
            //$("#txtStZipCode").val(selectedItem.dataItem["ShipToZipCode"]);
            //$("#txtStRemark").val(selectedItem.dataItem["ShipToRemark"]);
        }
        function clearFormData() {
            $("#tblForm input[type=text]").val("");
            $("#tblForm2 input[type=text]").val("");
            $("#txtBtRemark").val("");
            if (g_supplierGroupList.length > 0) // At first start, this collection will be accessed without its data fully loaded.
                g_selSupplierGroup.setValue(-1);
            if (g_countryList.length > 0)
                g_selCountry.setValue(-1);
        }

        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveAllSupplier", {},
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(id) {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveSupplier", id,
                function (item) {
                    displayMainItemData(item);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveSupplierGroupList() {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveSupplierGroupList", {},
                function (items) {
                    g_supplierGroupList = items;
                    setComboBoxData(g_selSupplierGroup, g_supplierGroupList, -1);
                });
        }
        function retrieveCountryList() {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveCountryList", {},
                function (items) {
                    g_countryList = items;
                    setComboBoxData(g_selCountry, g_countryList, -1);
                });
        }

        function displayMainItemData(item) {
            setFormData(item);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    mainSectionUnlock();
                    break;
                case ePageState.mainView:
                    formLock();
                    mainSectionUnlock();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    mainSectionLock();
                    break;
            }
        }

        // Grid controls
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }
        
        function validateForm() {
            $("#tblForm tblForm2").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if ($("#txtName").val() === "") {
                displayError("Please specify Supplier Name");
                return false;
            }
            return true;
        }

        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;

            var item = {};

            var row = g_mainGrid.selectedRow;
            item["SupplierId"] = row != null ? row.data["SupplierId"] : -1;
            item["SupplierName"] = $("#txtName").val();
            item["SupplierNo"] = "";
            item["SupplierGroupId"] = g_selSupplierGroup.getValue() != "" ? g_selSupplierGroup.getValue() : -1;
            item["SupplierGroupName"] = "";
            item["TaxNo"] = $("#txtTaxNo").val();
            item["ContactPerson"] = $("#txtContactPerson").val();
            item["Email"] = $("#txtEmail").val();
            item["Phone"] = $("#txtPhone").val();
            item["Fax"] = $("#txtFax").val();
            item["CreditTerm"] = $("#txtCreditTerm").val();
            item["Address1"] = $("#txtBtAddr1").val();
            item["Address2"] = $("#txtBtAddr2").val();
            item["Address3"] = $("#txtBtAddr3").val();
            item["City"] = $("#txtBtCity").val();
            item["CountryId"] = g_selCountry.getValue() != "" ? g_selCountry.getValue() : -1;
            item["CountryName"] = "";
            item["ZipCode"] = $("#txtBtZipCode").val();
            item["Remark"] = $("#txtBtRemark").val();
            item["ItemState"] = g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem :
                g_pageState == ePageState.mainEdit ? KGrid.ItemState.modifiedItem : KGrid.ItemState.deletedItem;
            item["Version"] = row != null ? row.data["Version"] : "";

            sendAjaxUpdate("PartnerManagement.svc", "ManageSupplier", item,
                function (rsp) {
                    setPageDirty(false);
                    changeState(ePageState.mainNew);
                    retrieveMainGridData();
                    //displayInfo("Success");
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doClear() {
            if (preventNavigationOnUnsaved() && isPageDirty())
                doDefaultClear();
        }
        function doPrint() {
            doDefaultPrint();
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
            changeUrl('CustomerInfo.aspx');
        }
        function goNext() {
        }

        function Save() {
        }
        function Void() {
        }
        function Clear() {
        }
        function Print() {
        }
        function Cancel() {
        }
        function GoBack() {
        }
        function GoNext() {
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["SupplierId"]);
                }    
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow != null) {
                    $("#btnMainEdit").enable();
                    $("#btnMainDelete").enable();
                }
                else {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                }
            }
        }

        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?")) {
                $("#btnMainDelete").disable();
                g_pageState = ePageState.mainDelete;
                doSave();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-red">
        <ul id="tab0" class="nav nav-tabs">
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('CustomerInfo.aspx');">Customer Info</a></li>
            <li class="active"><a href="#" data-toggle="tab" onclick="GoToUrl('SupplierInfo.aspx');">Supplier Info</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table id="tblForm" class="form double">
                        <tr>
                            <td colspan="4" class="cell-header">Supplier Management:</td>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td>
                                <input type="text" id="txtName" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Supplier No:</td>
                            <td>
                                <input type="text" id="txtSupplierNo" />
                            </td>
                            <td>Supplier Group:</td>
                            <td>
                                <select id="selSupplierGroup" class="" placeHolder="supplier group">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Tax Number:</td>
                            <td>
                                <input type="text" id="txtTaxNo" />
                            </td>
                            <td></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Contact Person:</td>
                            <td>
                                <input type="text" id="txtContactPerson" />
                            </td>
                            <td>Email:</td>
                            <td>
                                <input type="text" id="txtEmail" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td>
                                <input type="text" id="txtPhone" />
                            </td>
                            <td>Fax:</td>
                            <td>
                                <input type="text" id="txtFax" />
                            </td>
                        </tr>
                        <tr>
                            <td>Credit Term:</td>
                            <td>
                                <input type="text" id="txtCreditTerm" />
                            </td>
                            <td></td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <table id="tblForm2" class="form double">
                        <tr>
                            <td>Bill To:</td>
                            <td>
                                <input type="text" id="txtBtAddr1" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtBtAddr2" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtBtAddr3" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>
                                <input type="text" id="txtBtCity" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>
                                <select id="selCountry" runat="server"></select>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Zip Code:</td>
                            <td>
                                <input type="text" id="txtBtZipCode" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Remark:</td>
                            <td>
                                <textarea id="txtBtRemark" class="remark"></textarea>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" style="height: 180px;">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div><!--subpage-->
</asp:Content>