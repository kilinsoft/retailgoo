﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="customerInfo.aspx.cs" Inherits="RetailGOO.Page.CustomerInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style type="text/css">
        .remark
        {
            height: 100px;
        }
    </style>
    <script type="text/javascript">

        // Variables
        var g_countryList = [];
        var g_customerGroupList = [];
        var g_defaultPriceList = [];

        // Control variables
        var g_selcustomerGroup;
        var g_selDefaultPrice;
        var g_selCountryBilling;
        var g_selCountryShipping;

        // Data variables

        function pageLoad() {

            preventNavigationOnUnsaved(true);

            // Initialize variables
            var mainGridColsDef = [
            { title: "", name: "", width: 30, required: false, editable: false, formatter: KGrid.Formatter.Radio },
            { title: "ID", name: "CustomerId", required: false, editable: false },
            { title: "Name", name: "CustomerName", width: 300, required: false, editable: false },
            { title: "Contact", name: "ContactPerson", required: false, editable: false },
            { title: "Business Tel", name: "Phone", required: false, editable: false },
            { title: "Type", name: "CustomerGroupName", required: false, editable: false }
            ];
            var mainGridDef = {
                editable: false,
                enableAddBlankRow: false,
                onRowSelected: mgOnRowSelected
            }
            g_mainGrid = new KGrid.Grid("#mainGrid", mainGridDef, mainGridColsDef, []);

            g_selCustomerGroup = $("#selCustomerGroup").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            g_selCountryBilling = $("#selBtCountry").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            g_selCountryShipping = $("#selStCountry").selectize({
                create: false, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            g_selDefaultPrice = $("#selDefaultPrice").selectize({
                create: false, readOnly: true, valueField: 'Value', labelField: 'Text', searchField: ['Text']
            })[0].selectize;

            initGridSorter(g_mainGrid, "#selMainSelectedBy",
                [{ title: "Id", name: "CustomerId", valueCompare: true },
                { title: "Name", name: "CustomerName", valueCompare: false },
                { title: "Type", name: "CustomerGroupName", valueCompare: false }]);

            // Initialize behaviors
            $("#btnVoid").addClass("disable");

            // Init state
            changeState(ePageState.mainNew);

            // Ajax calls to initialize mandatory variables
            retrieveCustomerGroupList();
            retrieveCountryList();
            retrieveMainGridData();
            retrieveDefaultPriceList();
            updateCommandButtons("main", null);
        }

        function formLock() {
            $("#tblForm input[type=text]").readOnly(true);
            $("#tblForm2 input[type=text]").readOnly(true);
            $("#txtBtRemark").prop("disabled", true);
            $("#txtStRemark").prop("disabled", true);
            g_selCustomerGroup.disable();
            g_selDefaultPrice.disable();
            g_selCountryBilling.disable();
            g_selCountryShipping.disable();
        }
        function formUnlock() {
            $("#tblForm input[type=text]").readOnly(false);
            $("#tblForm2 input[type=text]").readOnly(false);
            $("#txtCustomerNo").readOnly(true);
            $("#txtBtRemark").prop("disabled", false);
            $("#txtStRemark").prop("disabled", false);
            g_selCustomerGroup.enable();
            g_selDefaultPrice.enable();
            g_selCountryBilling.enable();
            g_selCountryShipping.enable();
        }

        function setFormData(item) {
            $("#txtCustomerName").val(item["CustomerName"]);
            $("#txtCustomerNo").val(item["CustomerId"]);
            g_selCustomerGroup.setValue(item["CustomerGroupId"])
            $("#txtTaxNo").val(item["TaxNo"]);
            g_selDefaultPrice.setValue(item["DefaultPriceTypeId"]);
            $("#txtContactPerson").val(item["ContactPerson"]);
            $("#txtEmail").val(item["Email"]);
            $("#txtPhone").val(item["Phone"]);
            $("#txtFax").val(item["Fax"]);
            $("#txtCreditTerm").val(item["CreditTerm"]);
            $("#txtCreditLimit").val(item["CreditLimit"]);
            $("#txtBtAddr1").val(item["AddressBilling1"]);
            $("#txtBtAddr2").val(item["AddressBilling2"]);
            $("#txtBtAddr3").val(item["AddressBilling3"]);
            $("#txtBtCity").val(item["CityBilling"]);
            g_selCountryBilling.setValue(item["CountryBillingId"]);
            $("#txtBtZipCode").val(item["ZipCodeBilling"]);
            $("#txtBtRemark").val(item["RemarkBilling"]);
            $("#txtStAddr1").val(item["AddressShipping1"]);
            $("#txtStAddr2").val(item["AddressShipping2"]);
            $("#txtStAddr3").val(item["AddressShipping3"]);
            $("#txtStCity").val(item["CityShipping"]);
            g_selCountryShipping.setValue(item["CountryShippingId"]);
            $("#txtStZipCode").val(item["ZipCodeShipping"]);
            $("#txtStRemark").val(item["RemarkShipping"]);
        }
        function clearFormData() {
            $("#tblForm input[type=text]").val("");
            $("#tblForm2 input[type=text]").val("");
            $("#txtBtRemark").val("");
            $("#txtStRemark").val("");
            if (g_customerGroupList.length > 0) // At first start, this collection will be accessed without its data fully loaded.
                g_selCustomerGroup.setValue(-1);
            if (g_countryList.length > 0) {
                g_selCountryBilling.setValue(-1);
                g_selCountryShipping.setValue(-1);
            }
            if (g_defaultPriceList.length > 0)
                g_selDefaultPrice.setValue(g_defaultPriceList[0]["Value"]);
        }

        function retrieveMainGridData(funcOnSuccess) {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveAllCustomer", {},
                function (items) {
                    setGridData(g_mainGrid, items);
                    if (funcOnSuccess != null)
                        funcOnSuccess();
                });
        }
        function retrieveMainItemData(id) {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveCustomer", id,
                function (items) {
                    displayMainItemData(items);
                    changeState(ePageState.mainView);
                    updateCommandButtons("main", g_mainGrid.selectedRow);
                });
        }
        function retrieveCustomerGroupList() {
            retrieveAjaxDataSync("PartnerManagement.svc", "RetrieveCustomerGroupList", {},
                function (items) {
                    g_CustomerGroupList = items;
                    setComboBoxData(g_selCustomerGroup, g_CustomerGroupList, -1);
                });
        }
        function retrieveCountryList() {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveCountryList", {},
                function (items) {
                    g_countryList = items;
                    setComboBoxData(g_selCountryBilling, g_countryList, -1);
                    setComboBoxData(g_selCountryShipping, g_countryList, -1);
                });
        }
        function retrieveDefaultPriceList() {
            retrieveAjaxDataSync("BusinessService.svc", "RetrieveDefaultPriceList", {},
                function (items) {
                    g_defaultPriceList = items;
                    setComboBoxData(g_selDefaultPrice, g_defaultPriceList, g_defaultPriceList[0]["Value"]);
                });
        }

        function displayMainItemData(item) {
            setFormData(item);
        }
        function clearMainGrid() {
            setGridData(g_mainGrid, []);
        }
        function changeState(pageState) {
            g_pageState = pageState;
            updateCommandButtons("main", g_mainGrid.selectedRow);
            switch (g_pageState) {
                case ePageState.mainNew:
                    clearFormData();
                    formUnlock();
                    mainSectionUnlock();
                    break;
                case ePageState.mainView:
                    formLock();
                    mainSectionUnlock();
                    break;
                case ePageState.mainEdit:
                    formUnlock();
                    mainSectionLock();
                    break;
            }
        }

        // Grid controls
        function mainSectionLock() {
            g_mainGrid.lock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").disable();
        }
        function mainSectionUnlock() {
            g_mainGrid.unlock();
            $("#btnMainEdit").disable();
            $("#btnMainDelete").disable();
            $("#selMainSelectedBy").enable();
        }

        function validateForm() {
            $("#tblForm tblForm2").find("input[type=text]").each(function () {
                $(this).val($.trim($(this).val()))
            });
            if ($("#txtCustomerName").val() === "") {
                displayError("Please specify Customer Name");
                return false;
            }
            if (g_selDefaultPrice.getValue() == -1 || g_selDefaultPrice.getValue() == "") {
                displayError("Please specify Default Price");
                return false;
            }
            return true;
        }

        // Common Controls
        function doSave() {
            if (!validateForm())
                return false;

            var item = {};

            var row = g_mainGrid.selectedRow;
            item["CustomerId"] = row != null ? row.data["CustomerId"] : -1;
            item["CustomerName"] = $("#txtCustomerName").val();
            item["CustomerNo"] = "";
            item["CustomerGroupId"] = g_selCustomerGroup.getValue() != "" ? g_selCustomerGroup.getValue() : -1;
            item["CustomerGroupName"] = "";
            item["DefaultPriceTypeId"] = g_selDefaultPrice.getValue();
            item["TaxNo"] = $("#txtTaxNo").val();
            item["ContactPerson"] = $("#txtContactPerson").val();
            item["Email"] = $("#txtEmail").val();
            item["Phone"] = $("#txtPhone").val();
            item["Fax"] = $("#txtFax").val();
            item["CreditTerm"] = $("#txtCreditTerm").val();
            item["CreditLimit"] = $("#txtCreditLimit").val();
            item["AddressBilling1"] = $("#txtBtAddr1").val();
            item["AddressBilling2"] = $("#txtBtAddr2").val();
            item["AddressBilling3"] = $("#txtBtAddr3").val();
            item["CityBilling"] = $("#txtBtCity").val();
            item["CountryBillingId"] = g_selCountryBilling.getValue() != "" ? g_selCountryBilling.getValue() : -1;
            item["CountryBillingName"] = "";
            item["ZipCodeBilling"] = $("#txtBtZipCode").val();
            item["RemarkBilling"] = $("#txtBtRemark").val();
            item["AddressShipping1"] = $("#txtStAddr1").val();
            item["AddressShipping2"] = $("#txtStAddr2").val();
            item["AddressShipping3"] = $("#txtStAddr3").val();
            item["CityShipping"] = $("#txtStCity").val();
            item["CountryShippingId"] = g_selCountryShipping.getValue() != "" ? g_selCountryShipping.getValue() : -1;
            item["CountryShippingName"] = "";
            item["ZipCodeShipping"] = $("#txtBtZipCode").val();
            item["RemarkShipping"] = $("#txtBtRemark").val();
            item["ItemState"] = g_pageState == ePageState.mainNew ? KGrid.ItemState.newItem :
                g_pageState == ePageState.mainEdit ? KGrid.ItemState.modifiedItem : KGrid.ItemState.deletedItem;
            item["Version"] = row != null ? row.data["Version"] : "";

            sendAjaxUpdate("PartnerManagement.svc", "ManageCustomer", item,
                function (rsp) {
                    setPageDirty(false);
                    changeState(ePageState.mainNew);
                    retrieveMainGridData();
                    //displayInfo("Success");
                },
                function (rsp) {
                    displayError(rsp.StatusText);
                });
        }
        function doClear() {
            if (preventNavigationOnUnsaved() && isPageDirty())
                doDefaultClear();
        }
        function doPrint() {
            doDefaultPrint();
        }
        function doCancel() {
            doDefaultCancel();
        }
        function goBack() {
        }
        function goNext() {
            changeUrl('SupplierInfo.aspx');
        }

        function Save() {
        }
        function Void() {
        }
        function Clear() {
        }
        function Print() {
        }
        function Cancel() {
        }
        function GoBack() {
        }
        function GoNext() {
        }

        // Handlers
        function mgOnRowSelected(pRow) {
            pRow.$element.find("input[type=radio]:first-child").prop("checked", true);
            if (!isPageDirty()) {
                if (pRow.rowState == KGrid.ItemState.originalItem) {
                    retrieveMainItemData(g_mainGrid.selectedRow.data["CustomerId"]);
                }
            } else {
                alertClearFirst();
                if (g_mainGrid.selectedRow != null)
                    g_mainGrid.selectedRow.deselect();
            }
        }

        // Grid Commands
        function updateCommandButtons(gridType, pRow) {
            if (gridType == "main") { // mainGrid
                if (pRow != null) {
                    $("#btnMainEdit").enable();
                    $("#btnMainDelete").enable();
                }
                else {
                    $("#btnMainEdit").disable();
                    $("#btnMainDelete").disable();
                }
            }
        }

        function onMainEdit() {
            changeState(ePageState.mainEdit);
            var $firstInput = $("#tblForm input:first");
            if ($firstInput.length) { // found an input
                scrollToElement($firstInput.attr("id"));
            }
        }
        function onMainDelete() {
            var selectedRow = g_mainGrid.selectedRow;
            if (selectedRow == null)
                return;
            if (window.confirm("\nThe item will be deleted permanently.\n\nDo you want to continue?")) {
                $("#btnMainDelete").disable();
                g_pageState = ePageState.mainDelete;
                doSave();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-blue">
        <ul id="tab0" class="nav nav-tabs">
            <li class="active"><a href="#" data-toggle="tab" onclick="GoToUrl('CustomerInfo.aspx');">Customer Info</a></li>
            <li class="inactive"><a href="#" data-toggle="tab" onclick="GoToUrl('SupplierInfo.aspx');">Supplier Info</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table id="tblForm" class="form double">
                        <tr>
                            <td colspan="4" class="cell-header">Customer Management:</td>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td>
                                <input type="text" id="txtCustomerName" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Customer No:</td>
                            <td>
                                <input type="text" id="txtCustomerNo" />
                            </td>
                            <td>Customer Group:</td>
                            <td>
                                <select id="selCustomerGroup" class="select-width-150" placeHolder="customer group">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Tax Number:</td>
                            <td>
                                <input type="text" id="txtTaxNo" />
                            </td>
                            <td>Default Price:</td>
                            <td>
                                <select id="selDefaultPrice" class="select-width-150">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Contact Person:</td>
                            <td>
                                <input type="text" id="txtContactPerson" />
                            </td>
                            <td>Email:</td>
                            <td>
                                <input type="text" id="txtEmail" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td>
                                <input type="text" id="txtPhone" />
                            </td>
                            <td>Fax:</td>
                            <td>
                                <input type="text" id="txtFax" />
                            </td>
                        </tr>
                        <tr>
                            <td>Credit Term:</td>
                            <td>
                                <input type="text" id="txtCreditTerm" />
                            </td>
                            <td>Credit Limit:</td>
                            <td>
                                <input type="text" id="txtCreditLimit" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <table id="tblForm2" class="form double">
                        <tr>
                            <td>Bill To:</td>
                            <td>
                                <input type="text" id="txtBtAddr1" />
                            </td>
                            <td>Ship To:</td>
                            <td>
                                <input type="text" id="txtStAddr1" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtBtAddr2" />
                            </td>
                            <td></td>
                            <td>
                                <input type="text" id="txtStAddr2" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="txtBtAddr3" />
                            </td>
                            <td></td>
                            <td>
                                <input type="text" id="txtStAddr3" />
                            </td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>
                                <input type="text" id="txtBtCity" />
                            </td>
                            <td>City:</td>
                            <td>
                                <input type="text" id="txtStCity" />
                            </td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>
                                <select id="selBtCountry" runat="server"></select>
                            </td>
                            <td>Country:</td>
                            <td>
                                <select id="selStCountry" runat="server"></select>
                            </td>
                        </tr>
                        <tr>
                            <td>Zip Code:</td>
                            <td>
                                <input type="text" id="txtBtZipCode" />
                            </td>
                            <td>Zip Code:</td>
                            <td>
                                <input type="text" id="txtStZipCode" />
                            </td>
                        </tr>
                        <tr>
                            <td>Remark:</td>
                            <td>
                                <textarea id="txtBtRemark" class="remark"></textarea>
                            </td>
                            <td>Remark:</td>
                            <td>
                                <textarea id="txtStRemark" class="remark"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <!-- MainGrid -->
                    <div class="grid-command">
                        <div>
                            <input type="button" id="btnMainEdit" class="btn-action" value="Edit" onclick="onMainEdit();" />
                            <input type="button" id="btnMainDelete" class="btn-action" value="Delete" onclick="onMainDelete();" />
                        </div>
                        <div>
                            <span>Selected by:</span>
                            <select id="selMainSelectedBy" class="width-200 not-cause-dirty">
                            </select>
                        </div>
                    </div>
                    <div id="mainGrid" style="height: 180px;">
                    </div>
                </div>
                <div class="section common-control">
                    <div>
                        <div id="btnBackward" onclick="goBack();">&lt;</div>
                        <div id="btnSave" onclick="doSave();">Save</div>
                        <div id="btnVoid" onclick="doVoid();">Void</div>
                        <div id="btnClear" onclick="doClear();">Clear</div>
                        <div id="btnPrint" onclick="doPrint();">Print</div>
                        <div id="btnCancel" onclick="doCancel();">Cancel</div>
                        <div id="btnForward" onclick="goNext();">&gt;</div>
                    </div>
                </div>
            </div><!--tab-pane-->
        </div><!--tab-content-->
    </div><!--subpage-->
</asp:Content>