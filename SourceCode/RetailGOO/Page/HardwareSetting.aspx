﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Auth.Master" AutoEventWireup="true" CodeBehind="HardwareSetting.aspx.cs" 
    Inherits="RetailGOO.Page.HardwareSetting" ClientIDMode="Static"%>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <script type="text/javascript">
        // Variables declaration

        function pageLoad() {

            preventNavigationOnUnsaved();

            // Initialize behaviors

            // Ajax calls to initialize mandatory variables
        }
        function Save() {
        }
        function Void() {
        }
        function Clear() {
        }
        function Print() {
        }
        function Cancel() {
        }
        function GoBack() {
        }
        function GoNext() {
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-green">
        <ul id="tab0" class="nav nav-tabs first-row">
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('BusinessInfo.aspx');">Business Info</a></li>
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('FinancialSetting.aspx');">Financial Setting</a></li>
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('DocSetting.aspx')">Doc Setting</a></li>
        </ul>
        <ul id="tab1" class="nav nav-tabs no-border-bottom">
            <li class="green active width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('HardwareSetting.aspx');">Hardware Setting</a></li>
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('Roles.aspx');">Roles</a></li>
            <li class="inactive width-150"><a href="#" data-toggle="tab" onclick="GoToUrl('User.aspx')">User</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="section">
                    <table class="form">
                        <tr>
                            <td colspan="2" class="cell-header">
                                POS Hardware Setting:
                            </td>
                        </tr>
                        <tr>
                            <td>Roles:</td>
                            <td>
                                <select id="selRole" class="width-200">
                                    <option value="0">Manager</option>
                                    <option value="1">Purchasing</option>
                                    <option value="2">Stocking</option>
                                    <option value="3">Sales</option>
                                </select>
                            </td>
                        </tr>
			        </table>
                </div>
                <div class="section">
                    <div class="line">
                    </div>
                </div>
                <div class="section">
                    <div id="divCommonControls" style="text-align: center; margin-top: 10px;">
                        <div style="display: inline-block;">
                            <div class="btn-nav" id="btnBackward" onclick="GoBack();">&lt;</div>
                            <div class="btn-save" id="btnSave" onclick="Save();">Save</div>
                            <div class="btn-void" id="btnVoid" onclick="Void();">Void</div>
                            <div class="btn-clear" id="btnClear" onclick="Clear();">Clear</div>
                            <div class="btn-print" id="btnPrint" onclick="Print();">Print</div>
                            <div class="btn-cancel" id="btnCancel" onclick="Cancel();">Cancel</div>
                            <div class="btn-nav" id="btnForward" onclick="GoNext();">&gt;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--tab-content-->
    </div><!--subpage-->
</asp:Content>