﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NoAuth.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="RetailGOO.CreateUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <style>
        td + td {
            padding-left: 10px;
        }
    </style>
    <script type="text/javascript">
        var username = "";
        var email = "";
        var pwd = "";
        var confirmPwd = "";
        var fFbUser = false;
        var accessToken = "";

        function pageLoad()
        {
            
        }

        function BtnSubmitOnClick() {
            if (!formValid())
                return;

            //username = $("#txtUsername").val().trim().toLowerCase();
            email = $("#txtEmail").val().trim().toLowerCase();
            pwd = $("#txtPwd").val().trim();
            confirmPwd = $("#txtConfirmPwd").val().trim();
            if (email == "" || pwd == "")
                displayError("Please supply all information");
            else if (!isValidEmail(email))
                displayError("Invalid email");
            else if (!isValidPassword(pwd))
                displayError("The password must be 5-32 characters");
            else if (pwd != confirmPwd)
                displayError("The passwords do not match");
            else
                SubmitCreateUser();
        }

        function SubmitCreateUser()
        {
            $("#btnSubmit").sendAjax(
                "./Service/Security.svc/CreateUser",
                {
                    'Username' : email,
                    'Password' : pwd,
                    'Email' : email,
                    'IsFacebookUser': fFbUser,
                    'AccessToken' : accessToken
                },
                function (resp) {
                    if (resp.Success)
                        GoToUrl("./Page/BusinessSelection.aspx");
                    else
                        displayError(resp.StatusText);
                },
                function (type, err, errThrown) {
                    displayError('Error\n type: ' + type + '\nerr: ' + err + '\nerrThrown: ' + errThrown);
                }
                );
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4">
            <table>
                <tbody>
                    <tr>
                        <td >
                            Email
                        </td>
                        <td>
                            <input type="text" id="txtEmail"  class="auto-validate-email"/>
                        </td>
                    </tr>
 <%--                   <tr>
                        <td >
                            Username
                        </td>
                        <td>
                            <input type="text" id="txtUsername" value="user_a1" class="auto-validate-username" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td >
                            Password
                        </td>
                        <td>
                            <input type="password" id="txtPwd" class="auto-validate-password" />
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Confirm Password
                        </td>
                        <td>
                            <input type="password" id="txtConfirmPwd" class="auto-validate-password" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right; padding-top: 10px;">
                            <input type="button" id="btnSubmit" value="Create User" class="btn-action-big btn-blue txt-blue" onclick="BtnSubmitOnClick();" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-2"></div>
    </div>
</asp:Content>
