﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NoAuth.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="RetailGOO.Login" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageHead" runat="server">
    <link rel="stylesheet" href="./css/login.css" type="text/css" />
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <script type="text/javascript">

        var g_fbUserId = "";
        var g_fbAccessToken = "";
        var g_authData = {};

        function pageLoad() {
            $("#txtPassword").on("keypress", function(e) {
                if (e.which == 13 || e.which == 9) { // ENTER or TAB
                    e.preventDefault();
                    $("#btnSubmit").click();
                }
            })
        }

        function submit() {
            g_authData = {
                'Username': $.trim($("#txtUsername").val().toLowerCase()),
                'Password': $.trim($("#txtPassword").val()),
                'Email': $.trim($("#txtUsername").val().toLowerCase()),
                'IsFacebookUser': false,
                'AccessToken': ''
            };
            if ( formValid() )
                authenticate();
        }

        function authenticate() {
            $('#btnSubmit').sendAjax(
                "./Service/Security.svc/Authenticate",
                g_authData,
                function (resp) {
                    if (resp.Success)
                        changeUrl("./Page/BusinessSelection.aspx");
                    else {
                        // Pro : ToDo : Check between 1.Invalid user, and 2. Valid FB user that has no RetailGOO account.
                        displayError(resp.StatusText);
                    }
                },
                function (type, err, errThrown) {
                    displayError('Error\n type: ' + type + '\nerr: ' + err + '\nerrThrown: ' + errThrown);
                });
        }

        <%-- Facebook API --%>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        function statusChangeCallback(response) {
            // The response object is returned with a status field that lets the
            // app know the current login status of the person.
            // Full docs on the response object can be found in the documentation
            // for FB.getLoginStatus().
            if (response.status === 'connected') {
                g_fbUserId = response.authResponse.userID;
                g_fbAccessToken = response.authResponse.accessToken;
                //alert(g_fbUserId + " " + g_fbAccessToken);
                FB.api('/me', function (fbUser) {
                    g_authData = {
                        'Username': g_fbUserId,
                        'Password': '',
                        'Email': fbUser.email,
                        'IsFacebookUser': true,
                        'AccessToken': g_fbAccessToken
                    };
                    authenticate();
                });
            } else if (response.status === 'not_authorized') {
                // The person is logged into Facebook, but not your app.
                document.getElementById('status').innerHTML = 'Please log ' +
                  'into this app.';
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
                document.getElementById('status').innerHTML = 'Please log ' +
                  'into Facebook.';
            }
        }
        function checkLoginState() {
            FB.getLoginStatus(function (response) {
                statusChangeCallback(response);
            });
        }
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1469838753250593',
                cookie: true,  // enable cookies to allow the server to access 
                // the session
                xfbml: true,  // parse social plugins on this page
                version: 'v2.0' // use version 2.0
            });
            /*
            FB.getLoginStatus(function (response) {
                statusChangeCallback(response);
            });
            */
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageBody" runat="server">
    <div class="theme-blue" style="margin: 0 auto;">
        <div class="section section-last" style="padding: 50px 0px;">
            <table class="form single">
                <tr>
                    <td class="cell-name right" style="vertical-align: top;">
                        <img src="./images/logo00.png" />
                    </td>
                    <td style="text-align: left;">
                        <table class="form single">                             
                            <tr>
                                <td style="width:150px;">
                                    <label for="txtUsername">Username</label>
                                </td>
                                <td>
                                    <input type="text" id="txtUsername" class="width-250 auto-validate-username" autofocus />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="txtPwd"  style="width:150px;">Password</label>
                                </td>
                                <td>
                                    <input type="password" id="txtPassword" class="width-250 auto-validate-password" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td class="cell-control left" style="padding-top:10px;">
                                    <p>
                                        <input type="button" id="btnSubmit" class="btn-action-big btn-blue" value="Log in" onclick="submit();" />
                                    </p>
                                    <p>
                                        <a href="CreateUser.aspx"><span style="font-size: 9pt;">Create User</span></a><br />
                                        <a href="forgottenpassword.aspx"><span style="font-size: 9pt;">Forgotten password?</span></a><br />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="padding-left: 50px;">
                                    --- or ---
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td class="cell-control left" style="padding-top: 20px;">                                  
                                    <%--<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">Login with Facebook</fb:login-button>--%>
                                    <div class="fb-login-button" data-onlogin="checkLoginState();" data-scope="public_profile,email">Login with Facebook</div>
                                    <div id="fb-root"></div>
                                    <div id="status" style="visibility: hidden;"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
