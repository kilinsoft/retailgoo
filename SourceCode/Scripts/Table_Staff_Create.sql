USE [retailgoo]
GO

/****** Object:  Table [dbo].[Staff]    Script Date: 6/10/2014 8:41:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Staff](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[StaffId] [nvarchar](50) NOT NULL,
	[Role] [varchar](10) NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


