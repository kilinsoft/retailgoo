USE [retailgoo]
GO

INSERT INTO [dbo].[Business]
           ([BusinessName]
           ,[CreatedBy]
           ,[Type]
           ,[LogoPath]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[Country]
           ,[ZipCode]
           ,[Phone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[FacebookUrl]
           ,[TaxId]
           ,[DateCreated]
           ,[DateExpired]
           ,[DateModified]
           ,[UpdatedBy])
     VALUES
           ('Business A2'
           ,'user_a1'
           ,'1'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,'user_a1@retailgoo.com'
           ,''
           ,''
           ,''
           ,GETDATE()
           ,DATEADD(month,2,GETDATE())
           ,GETDATE()
           ,'user_a1')
GO


