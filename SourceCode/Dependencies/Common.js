﻿// This file must be used in conjuction with js/lib/ajax 
// and js/jquery library



/************************************/
/* Javascript Helping Methods */
/************************************/

function ToJSDate(value) {
    if (value) {

        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(value);
        return new Date(parseFloat(results[1]));
    }
    return "";
}

function ToWCFDate(value) {
    if (value) {
        var d = new Date(value);
        var date = '\/Date(' + d.getTime() + '-0000)\/';
        return date;
    }
    return "";
}

/************************************/
/* Javascript Error Logging Methods */
/************************************/
window.onerror = errorHandler;

function errorHandler(message, url, line) {
    var wsUrl = "WCF/UserManagement/SaveUserDetails";
    var param = {
        Message: message,
        Url: url,
        Line: line,
        BrowserInformation: getBrowserInformation()
    };

    Ajax.post(
                wsUrl,
                param,
                function (response, data) {
                    if (response.IsSuccessful) {

                    }
                },
                function (data, XMLHttpRequest, textStatus, errorThrown) {

                }
            );
}

function getBrowserInformation() {
    var browser;
    if ($.browser.mozilla)
        browser = "Firefox";
    else if ($.browser.msie)
        browser = "Internet Explorer";
    else if ($.browser.opera)
        browser = "Opera";
    else if ($.browser.safari)
        browser = "Safari";
    else
        browser = "Unknown";
    browser += " ver." + $.browser.version;
    return browser;
}

//function checkString(str) {    
//    var output = "";
//    for (var i = 0; i < str.length; i++) {
//        output = output + "&#" + str.charCodeAt(i) + ";"
//    }
//    return output;
//}

//function cleanupClipBoardData() {
//    try {
//        var formattedData = window.clipboardData.getData("text").replace(/\s+/g, ' ');
//        formattedData = formattedData.toUpperCase();
//        window.clipboardData.setData("Text", formattedData);
//    }
//    catch (e) { }
//}

//function setInitialFocus() {
//    var bFound = false;
//    //if(document.activeElement == null)
//    //{		
//    // for each form
//    for (f = 0; f < document.forms.length; f++) {
//        // for each element in each form
//        for (i = 0; i < document.forms[f].length; i++) {
//            if (document.forms[f][i].tagName == "SELECT" || document.forms[f][i].tagName == "INPUT") {
//                // if it's not a hidden element
//                if (document.forms[f][i].type != "hidden") {
//                    // and it's not disabled
//                    if (document.forms[f][i].disabled != true) {
//                        // set the focus to it
//                        try {
//                            document.forms[f][i].focus();
//                            bFound = true;
//                        }
//                        catch (e) {
//                            //ignore control
//                            bFound = false;
//                        }
//                    }
//                }
//                // if found in this element, stop looking
//                if (bFound == true)
//                    break;
//            }
//            // if found in this form, stop looking
//            if (bFound == true)
//                break;
//        }
//    }
//    //}
//}


//function getICHtml(icId) {
//    var icHtml = document.getElementById(icId + "pnlInputControl").innerHTML;
//    HandlerBase.SaveInputControlSettings(encodeURI(icHtml), icId);
//}

//function persistanceLoadUp(icId) {
//    if (document.getElementById("icText").innerHTML != "") {
//        document.getElementById(icId + "pnlInputControl").innerHTML = decodeURI(document.getElementById("icText").innerHTML);
//    }
//}

//function showBusy() {
//    showDialog("NONE", '<center><div style="margin-left:20px; margin-top:3px; font-size:13px;font-weight:bold;" >Processing... Please wait</div></center>', null, null, 'AJAX');
//}

//function clearNoResults(gridID) {
//    if (rdsGrid_GetTotalRows(gridID) == 0) {
//        document.getElementById(gridID + "_EmptyDataText").innerText = "";
//    }
//}

//function clearText(elementName) {
//    document.getElementById(elementName).value = "";
//}

//function processValues(ctrl) {
//    var selVal = ctrl.value;
//    selVal = selVal.toUpperCase();
//    selVal = getSortedValues(selVal);
//    //trim spaces
//    selVal = selVal.replace(/^\s*/, "").replace(/\s*$/, ""); ;
//    ctrl.value = selVal;
//    return selVal;
//}

//function getSortedValues(selValue) {
//    var i;
//    selValue = selValue.replace(/(\r\n|[\r\n])/g, ' ');
//    var arrCountries = selValue.split(' ');
//    arrCountries = arrCountries.sort();
//    arrCountries = removeDuplicates(arrCountries);
//    selValue = '';
//    for (i = 0; i < arrCountries.length - 1; i++) {
//        selValue += arrCountries[i] + ' ';
//    }
//    //do the last one without space
//    selValue += arrCountries[i];
//    return selValue;
//}

//function removeDuplicates(arVals) {
//    var r = new Array();
//    o: for (var i = 0, n = arVals.length; i < n; i++) {
//        for (var x = 0, y = r.length; x < y; x++) {
//            if (r[x] == arVals[i]) {
//                continue o;
//            }
//        }
//        r[r.length] = arVals[i];
//    }
//    return r;
//}

//function nonExistingRDS(redirectPage) {
//    document.location.href = redirectPage;
//}


///********************/
///* KeyPress Methods */
///********************/

//function getKeyCode(e) {
//    var iKeyCode = 0;
//    if (window.event) {
//        iKeyCode = window.event.keyCode;
//    }
//    else {
//        if (e) iKeyCode = e.which;
//    }
//    return iKeyCode;
//}

//function stopReturn(e) {
//    if (getKeyCode(e) == 13) {
//        return false;
//    }
//    else {
//        return true;
//    }
//}

//function ToUpperCase_KeyPress(e) {
//    var iKeyCode = getKeyCode(e);
//    //lowercase letter
//    if (stopReturn(e)) {
//        if (iKeyCode > 96 && iKeyCode < 123) {
//            //convert to uppercase
//            window.event.keyCode = (iKeyCode - 32);
//        }
//        return true;
//    }
//    else {
//        return false;
//    }
//}

//function numbersOnly_KeyPress(e) {
//    var iKeyCode = getKeyCode(e);
//    if (iKeyCode >= 48 && iKeyCode <= 57) {
//        return true;
//    }
//    else {
//        return false;
//    }

//}

//function lettersOnly_KeyPress(e) {
//    var iKeyCode = getKeyCode(e);
//    if (iKeyCode >= 65 && iKeyCode <= 90) {
//        return true;
//    }
//    else if (iKeyCode >= 97 && iKeyCode <= 122) {
//        return true;
//    }
//    else if (iKeyCode == 32) {
//        return true;
//    }
//    else {
//        return false;
//    }
//}

//function uppercaseLettersOnly_KeyPress(e) {
//    if (lettersOnly_KeyPress(e)) {
//        return ToUpperCase_KeyPress(e);
//    }
//    else {
//        return false;
//    }
//}




///*********************************/
///* button enable/disable methods */
///*********************************/
//function enableButton(buttonId, enable) {
//    if (buttonId != null && typeof buttonId != 'undefined') {
//        var button = document.getElementById(buttonId);
//        if (button != null) {
//            button.disabled = !enable;
//        }
//    }
//}

//function logoutUserSession() {
//    window.location.href = timeoutpage;
//    return;
//}

//function unloadPage(evt) {
//    evt = window.event;
//    if (evt.clientY < 0) {
//        closeButton = true;
//        return pageTitle;
//    }
//    else
//        closeButton = false;
//}

//function onUnload() {
//    if (closeButton) {
//        HandlerBase.LogoutUser();
//    }
//}