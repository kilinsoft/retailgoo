USE [retailgoo]
GO
/****** Object:  UserDefinedTableType [dbo].[tBillItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tBillItem] AS TABLE(
	[BillItemId] [bigint] NOT NULL,
	[BillId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tBillPaymentItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tBillPaymentItem] AS TABLE(
	[BillPaymentItemId] [bigint] NOT NULL,
	[BillId] [bigint] NOT NULL,
	[PaymentTypeId] [tinyint] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tExpenseItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tExpenseItem] AS TABLE(
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ExpenseNo] [nvarchar](50) NOT NULL,
	[ExpenseName] [nvarchar](100) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tIdTable]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tIdTable] AS TABLE(
	[Id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tIncomingPaymentItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tIncomingPaymentItem] AS TABLE(
	[IncomingPaymentItemId] [bigint] NOT NULL,
	[IncomingPaymentId] [bigint] NOT NULL,
	[SalesInvoiceId] [bigint] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tItem] AS TABLE(
	[ItemId] [bigint] NOT NULL,
	[ItemGroupId] [bigint] NOT NULL,
	[ItemName] [nvarchar](100) NOT NULL,
	[Barcode] [varchar](50) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Cost] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[IsForSales] [bit] NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tItemAdjustmentItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tItemAdjustmentItem] AS TABLE(
	[AdjustmentItemId] [bigint] NOT NULL,
	[AdjustmentId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[QuantityBefore] [decimal](18, 4) NOT NULL,
	[QuantityAfter] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tItemBrandItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tItemBrandItem] AS TABLE(
	[ItemGroupId] [bigint] NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tItemTransfer]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tItemTransfer] AS TABLE(
	[TransferItemId] [bigint] NOT NULL,
	[TransferId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tOutgoingPaymentItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tOutgoingPaymentItem] AS TABLE(
	[OutgoingPaymentItemId] [bigint] NOT NULL,
	[ReceiveOrderId] [bigint] NOT NULL,
	[SupplierInvoice] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tPurchaseOrderItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tPurchaseOrderItem] AS TABLE(
	[PurchaseOrderItemId] [bigint] NOT NULL,
	[PurchaseOrderId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tReceiveOrderItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tReceiveOrderItem] AS TABLE(
	[ReceiveOrderItemId] [bigint] NOT NULL,
	[ReceiveOrderId] [bigint] NOT NULL,
	[PurchaseOrderItemId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tSalesInvoiceItem]    Script Date: 1/8/2016 1:02:37 AM ******/
CREATE TYPE [dbo].[tSalesInvoiceItem] AS TABLE(
	[SalesInvoiceItemId] [bigint] NOT NULL,
	[SalesInvoiceId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[ItemState] [char](1) NOT NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemBrandId]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetItemBrandId](@BusinessId BIGINT, @ItemBrandName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemBrandId BIGINT;
		SET @ItemBrandId = 0;

	SELECT TOP 1 @ItemBrandId = ib.ItemBrandId
	FROM ItemBrand ib
	WHERE ib.BusinessId = @BusinessId
		AND ib.ItemBrandName = @ItemBrandName

	RETURN @ItemBrandId;
END








GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemCategoryId]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetItemCategoryId](@BusinessId BIGINT, @ItemCategoryName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemCategoryId BIGINT;
		SET @ItemCategoryId = 0;

	SELECT TOP 1 @ItemCategoryId = ic.ItemCategoryId
	FROM ItemCategory ic
	WHERE ic.BusinessId = @BusinessId
		AND ic.ItemCategoryName = @ItemCategoryName

	RETURN @ItemCategoryId;
END








GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemFullName]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetItemFullName](@ItemGroupCode varchar(50), @ItemGroupName nvarchar(100), @ItemName nvarchar(100))
	RETURNS nvarchar(250) 
AS
BEGIN
	declare @fullname nvarchar(250);
		set @fullname = N'<undefined>';
	if (@ItemGroupName = @ItemName) 
		begin
			set @fullname = concat( cast(@ItemGroupCode as nvarchar(50)), N' ', @ItemGroupName );
		end
	else
		begin
			set @fullname = concat( cast(@ItemGroupCode as nvarchar(50)), N' ', @ItemGroupName, N' - ', @ItemName);
		end

	return @fullname;
END












GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemGroupId]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetItemGroupId](@BusinessId BIGINT, @ItemGroupCode VARCHAR(50), @ItemGroupName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemGroupId BIGINT;
		SET @ItemGroupId = 0;

	SELECT TOP 1 @ItemGroupId = ig.ItemGroupId
	FROM ItemGroup ig
	WHERE ig.BusinessId = @BusinessId
		AND ig.ItemGroupCode = @ItemGroupCode
		AND ig.ItemGroupName = @ItemGroupName

	RETURN @ItemGroupId;
END








GO
/****** Object:  UserDefinedFunction [dbo].[fnGetItemId]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetItemId](@BusinessId BIGINT, @ItemGroupCode VARCHAR(50), @ItemGroupName NVARCHAR(100), @ItemName NVARCHAR(100))
	RETURNS BIGINT
AS
BEGIN
	DECLARE @ItemId BIGINT;
		SET @ItemId = 0;

	SELECT TOP 1 @ItemId = i.ItemId
	FROM Item i
	INNER JOIN ItemGroup ig
		ON	ig.ItemGroupId = i.ItemGroupId
	WHERE i.ItemName = @ItemName
		AND ig.BusinessId = @BusinessId
		AND ig.ItemGroupCode = @ItemGroupCode
		AND ig.ItemGroupName = @ItemGroupName

	RETURN @ItemId;
END








GO
/****** Object:  UserDefinedFunction [dbo].[fnIsIncomingPaymentEditable]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnIsIncomingPaymentEditable](@IncomingPaymentId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void IncomingPayment item that has been created by CashSales. Need to do it from CashSales screen.
	SELECT @blockingItem = si.CashSalesNo
	FROM [SalesInvoice] si
	INNER JOIN [IncomingPaymentItem] ipi
		ON si.SalesInvoiceId = ipi.SalesInvoiceId
			AND ipi.IncomingPaymentId = @IncomingPaymentId 
	WHERE	si.IsCashSales = 1;

	IF @blockingItem IS NULL
		SET @blockingItem = N'';

	return @blockingItem;
END









GO
/****** Object:  UserDefinedFunction [dbo].[fnIsOutgoingPaymentEditable]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnIsOutgoingPaymentEditable](@OutgoingPaymentId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void OutgoingPayment item that has been created by CashPurchase. Need to do it from CashPurchase screen.
	SELECT TOP 1 @blockingItem = po.CashPurchaseNo
	FROM [PurchaseOrder] po
	INNER JOIN [PurchaseOrderItem] poi
		ON	po.PurchaseOrderId = poi.PurchaseOrderId
			AND poi.IsDisabled <> 1
	INNER JOIN [ReceiveOrderItem] roi
		ON	roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
			AND roi.IsDisabled <> 1
	INNER JOIN [ReceiveOrder] ro
		ON	roi.ReceiveOrderId = ro.ReceiveOrderId
			AND ro.IsDisabled <> 1
			AND ro.Status <> 'Void'
	INNER JOIN [OutgoingPaymentItem] opi
		ON	ro.ReceiveOrderId = opi.ReceiveOrderId
			AND opi.IsDisabled <> 1
			AND opi.OutgoingPaymentId = @OutgoingPaymentId
	WHERE	po.IsCashPurchase = 1;

	IF @blockingItem IS NULL
		SET @blockingItem = N'';

	return @blockingItem;
END









GO
/****** Object:  UserDefinedFunction [dbo].[fnIsPurchaseOrderEditable]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnIsPurchaseOrderEditable](@PurchaseOrderId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void PurchaseOrder item that has been created by CashPurchase. Need to do it from CashPurchase screen.
	SELECT TOP 1 @blockingItem = po.CashPurchaseNo
	FROM [PurchaseOrder] po
	WHERE	po.PurchaseOrderId = @PurchaseOrderId
		AND po.IsCashPurchase = 1;

	IF @blockingItem IS NULL
		BEGIN
			-- The PurchaseOrder item cannot be edited if it has been referred to by any active ReceiveOrder item and that ReceiveOrder item
			-- has also been refered to by active OutgoingPayment item.
			SELECT @blockingItem = ro.ReceiveOrderId
			FROM [ReceiveOrder] ro
			INNER JOIN [ReceiveOrderItem] roi
				ON ro.ReceiveOrderId = roi.ReceiveOrderId
					AND ro.IsDisabled <> 1
					AND ro.Status <> 'Void'
					AND roi.IsDisabled <> 1
			INNER JOIN [PurchaseOrderItem] poi
				ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
					AND poi.IsDisabled <> 1
					AND poi.PurchaseOrderId = @PurchaseOrderId
			INNER JOIN [OutgoingPaymentItem] opi
				ON opi.ReceiveOrderId = ro.ReceiveOrderId
					AND opi.IsDisabled <> 1
			INNER JOIN [OutgoingPayment] op
				ON opi.OutgoingPaymentId = op.OutgoingPaymentId
					AND op.IsDisabled <> 1
					AND op.Status <> 'Void';

			IF @blockingItem IS NULL
				SET @blockingItem = N'';
		END

	return @blockingItem;
END









GO
/****** Object:  UserDefinedFunction [dbo].[fnIsReceiveOrderEditable]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnIsReceiveOrderEditable](@ReceiveOrderId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void ReceiveOrder item that has been created by CashPurchase. Need to do it from CashPurchase screen.
	SELECT TOP 1 @blockingItem = po.CashPurchaseNo
	FROM [PurchaseOrder] po
	INNER JOIN [PurchaseOrderItem] poi
		ON	po.PurchaseOrderId = poi.PurchaseOrderId
			AND poi.IsDisabled <> 1
	INNER JOIN [ReceiveOrderItem] roi
		ON	roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
			AND roi.IsDisabled <> 1
			AND roi.ReceiveOrderId = @ReceiveOrderId
	WHERE	po.IsCashPurchase = 1;

	IF @blockingItem IS NULL
		BEGIN
			SELECT TOP 1 @blockingItem = op.OutgoingPaymentNo
			FROM [OutgoingPayment] op
			INNER JOIN [OutgoingPaymentItem] opi
				ON op.OutgoingPaymentId = opi.OutgoingPaymentId
					AND op.IsDisabled <> 1
					AND op.Status <> 'Void'
					AND opi.IsDisabled <> 1
			WHERE	opi.ReceiveOrderId = @ReceiveOrderId;

			IF @blockingItem IS NULL
				SET @blockingItem = N'';
		END

	return @blockingItem;
END









GO
/****** Object:  UserDefinedFunction [dbo].[fnIsSalesInvoiceEditable]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnIsSalesInvoiceEditable](@SalesInvoiceId BIGINT)
	RETURNS NVARCHAR(50)
AS
BEGIN

	DECLARE @blockingItem NVARCHAR(50);
		SET @blockingItem = NULL;
	
	-- User cannot directly edit/void SalesInvoice that has been created by CashSales. Need to do it from CashSales screen.
	SELECT @blockingItem = si.CashSalesNo
	FROM [SalesInvoice] si
	WHERE	si.IsCashSales = 1 
		AND si.SalesInvoiceId = @SalesInvoiceId;

	IF @blockingItem IS NULL
	BEGIN
		--User cannot edit SalesInvoice item if it has already been referred to by any active IncomingPayment item 
		SELECT TOP 1 @blockingItem = ip.IncomingPaymentNo
		FROM [IncomingPaymentItem] ipi
		INNER JOIN [SalesInvoice] si
			ON ipi.SalesInvoiceId = si.SalesInvoiceId
		INNER JOIN [IncomingPayment] ip
			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
		WHERE	si.SalesInvoiceId = @SalesInvoiceId
			AND si.IsDisabled <> 1
			AND si.Status <> 'Void'
			AND ip.IsDisabled <> 1
			AND ip.Status <> 'Void'
			AND ipi.IsDisabled <> 1;

		IF @blockingItem IS NULL
			SET @blockingItem = N'';
	END

	return @blockingItem;
END









GO
/****** Object:  UserDefinedFunction [dbo].[fnStaffValidate]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnStaffValidate] (@businessId bigint, @UserId bigint)
RETURNS bit
WITH EXECUTE AS CALLER
AS
BEGIN
	if not exists( select top 1 StaffId from dbo.Staff where BusinessId = @businessId and UserId = @UserId )
		return 1;
	return 0;
END;












GO
/****** Object:  UserDefinedFunction [dbo].[GetUsername]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetUsername](@UserId bigint)
	RETURNS VARCHAR(50) 
AS
BEGIN
	declare @Username varchar(50);
		SET @Username = (SELECT TOP 1 Username FROM [dbo].[User] WHERE UserId = @UserId);
	return @Username;
END












GO
/****** Object:  Table [dbo].[AmountType]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AmountType](
	[AmountTypeId] [tinyint] NOT NULL,
	[AmountTypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_AmountType] PRIMARY KEY CLUSTERED 
(
	[AmountTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bill]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bill](
	[BillId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[CustomerId] [bigint] NULL,
	[DiscountAmount] [decimal](18, 4) NOT NULL,
	[DiscountPercent] [decimal](18, 4) NOT NULL,
	[IsDiscountPercent] [bit] NOT NULL,
	[Total] [decimal](18, 4) NOT NULL,
	[Subtotal] [decimal](18, 4) NOT NULL,
	[Tax] [decimal](18, 4) NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[Note] [nvarchar](200) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BillItem]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillItem](
	[BillItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[BillId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
 CONSTRAINT [PK_BillItem] PRIMARY KEY CLUSTERED 
(
	[BillItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BillPaymentItem]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillPaymentItem](
	[BillPaymentItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[BillId] [bigint] NOT NULL,
	[PaymentTypeId] [tinyint] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
 CONSTRAINT [PK_PaymentItem] PRIMARY KEY CLUSTERED 
(
	[BillPaymentItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Business]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Business](
	[BusinessId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessName] [nvarchar](100) NOT NULL,
	[BusinessTypeId] [tinyint] NULL,
	[ImagePath] [varchar](max) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[CountryId] [tinyint] NULL,
	[ZipCode] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Fax] [nvarchar](50) NOT NULL,
	[Email] [varchar](254) NOT NULL,
	[Website] [nvarchar](max) NOT NULL,
	[FacebookUrl] [nvarchar](max) NOT NULL,
	[TaxIdInfo] [nvarchar](50) NOT NULL,
	[DateExpired] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
 CONSTRAINT [PK_Business] PRIMARY KEY CLUSTERED 
(
	[BusinessId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BusinessRelation]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessRelation](
	[RelationId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[RootBusinessId] [bigint] NOT NULL,
	[ParentBusinessId] [bigint] NOT NULL,
 CONSTRAINT [PK_BusinessRelation] PRIMARY KEY CLUSTERED 
(
	[RelationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BusinessType]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessType](
	[BusinessTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessTypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_BusinessType] PRIMARY KEY CLUSTERED 
(
	[BusinessTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CashManagement]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CashManagement](
	[CashManagementId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[Source] [varchar](50) NOT NULL,
	[Destination] [varchar](50) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_CashManagement] PRIMARY KEY CLUSTERED 
(
	[CashManagementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CostingType]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CostingType](
	[CostingTypeId] [bigint] NOT NULL,
	[CostingTypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_CostingType] PRIMARY KEY CLUSTERED 
(
	[CostingTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [bigint] NOT NULL,
	[CountryShortName] [varchar](5) NOT NULL,
	[CountryName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyId] [bigint] NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL,
	[CurrencySymbol] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[CustomerName] [nvarchar](100) NOT NULL,
	[CustomerNo] [nvarchar](50) NOT NULL,
	[CustomerGroupId] [tinyint] NULL,
	[TaxNo] [varchar](50) NOT NULL,
	[DefaultPriceTypeId] [tinyint] NOT NULL,
	[ContactPerson] [nvarchar](100) NOT NULL,
	[Email] [varchar](320) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Fax] [nvarchar](50) NOT NULL,
	[CreditTerm] [nvarchar](max) NOT NULL,
	[CreditLimit] [nvarchar](max) NOT NULL,
	[AddressBilling1] [nvarchar](100) NOT NULL,
	[AddressBilling2] [nvarchar](100) NOT NULL,
	[AddressBilling3] [nvarchar](100) NOT NULL,
	[CityBilling] [nvarchar](50) NOT NULL,
	[CountryBillingId] [tinyint] NULL,
	[ZipCodeBilling] [nvarchar](50) NOT NULL,
	[RemarkBilling] [nvarchar](max) NOT NULL,
	[AddressShipping1] [nvarchar](100) NOT NULL,
	[AddressShipping2] [nvarchar](100) NOT NULL,
	[AddressShipping3] [nvarchar](100) NOT NULL,
	[CityShipping] [nvarchar](50) NOT NULL,
	[CountryShippingId] [tinyint] NULL,
	[ZipCodeShipping] [nvarchar](50) NOT NULL,
	[RemarkShipping] [nvarchar](max) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerGroup]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerGroup](
	[CustomerGroupId] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerGroupName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_CustomerGroup] PRIMARY KEY CLUSTERED 
(
	[CustomerGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DebugTable]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DebugTable](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[Field1] [nvarchar](max) NULL,
	[Field2] [nvarchar](max) NULL,
	[Field3] [nvarchar](max) NULL,
	[Field4] [nvarchar](max) NULL,
	[Field5] [nvarchar](max) NULL,
 CONSTRAINT [PK_DebugTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DefaultPriceType]    Script Date: 1/8/2016 1:02:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DefaultPriceType](
	[DefaultPriceTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[DefaultPriceTypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_DefaultPrice] PRIMARY KEY CLUSTERED 
(
	[DefaultPriceTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DestinationType]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DestinationType](
	[DestinationTypeId] [bigint] NOT NULL,
	[DestinationTypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_DestinationType] PRIMARY KEY CLUSTERED 
(
	[DestinationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocSetting]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocSetting](
	[DocSettingId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[SalesOrderFormat] [nvarchar](50) NOT NULL,
	[SalesOrderNo] [int] NOT NULL,
	[SalesOrderTemplate] [tinyint] NOT NULL,
	[InvoiceFormat] [nvarchar](50) NOT NULL,
	[InvoiceNo] [int] NOT NULL,
	[InvoiceTemplate] [tinyint] NOT NULL,
	[ReceiptFormat] [nvarchar](50) NOT NULL,
	[ReceiptNo] [int] NOT NULL,
	[ReceiptTemplate] [tinyint] NOT NULL,
	[StockAdjFormat] [nvarchar](50) NOT NULL,
	[StockAdjNo] [int] NOT NULL,
	[StockAdjTemplate] [tinyint] NOT NULL,
	[ReceiveOrderFormat] [nvarchar](50) NOT NULL,
	[ReceiveOrderNo] [int] NOT NULL,
	[ReceiveOrderTemplate] [tinyint] NOT NULL,
	[CashPurchaseFormat] [nvarchar](50) NOT NULL,
	[CashPurchaseNo] [int] NOT NULL,
	[CashPurchaseTemplate] [tinyint] NOT NULL,
	[CashSalesFormat] [nvarchar](50) NOT NULL,
	[CashSalesNo] [int] NOT NULL,
	[CashSalesTemplate] [tinyint] NOT NULL,
	[PurchaseOrderFormat] [nvarchar](50) NOT NULL,
	[PurchaseOrderNo] [int] NOT NULL,
	[PurchaseOrderTemplate] [tinyint] NOT NULL,
	[PaymentFormat] [nvarchar](50) NOT NULL,
	[PaymentNo] [int] NOT NULL,
	[PaymentTemplate] [tinyint] NOT NULL,
	[ExpenseFormat] [nvarchar](50) NOT NULL,
	[ExpenseNo] [int] NOT NULL,
	[ExpenseTemplate] [tinyint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_DocSetting] PRIMARY KEY CLUSTERED 
(
	[DocSettingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Expense]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Expense](
	[ExpenseId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ExpenseNo] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[UseWht] [bit] NOT NULL,
	[WhtPercent] [decimal](18, 4) NOT NULL,
	[WhtAmount] [decimal](18, 4) NOT NULL,
	[SupplierId] [bigint] NOT NULL,
	[ExpenseCategoryId] [bigint] NOT NULL,
	[Reference] [nvarchar](50) NULL,
	[Contact] [nvarchar](100) NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[Address3] [nvarchar](100) NULL,
	[Phone] [varchar](50) NULL,
	[Date] [date] NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[PaymentTerm] [nvarchar](max) NULL,
	[Subtotal] [decimal](18, 4) NULL,
	[Tax] [decimal](18, 4) NULL,
	[Total] [decimal](18, 4) NULL,
	[IsVoided] [bit] NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Expense] PRIMARY KEY CLUSTERED 
(
	[ExpenseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExpenseCategory]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpenseCategory](
	[ExpenseCategoryId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ExpenseCategoryName] [nvarchar](100) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
 CONSTRAINT [PK_ExpenseCategory] PRIMARY KEY CLUSTERED 
(
	[ExpenseCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExpenseItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExpenseItem](
	[ExpenseItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ExpenseNo] [nvarchar](50) NOT NULL,
	[ExpenseName] [nvarchar](100) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ExpenseItem] PRIMARY KEY CLUSTERED 
(
	[ExpenseItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FinancialSetting]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FinancialSetting](
	[FinancialSettingId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[CurrencyId] [tinyint] NOT NULL,
	[DefaultPriceTypeId] [tinyint] NOT NULL,
	[UseServiceCharge] [bit] NOT NULL,
	[ServiceChargeAmount] [decimal](18, 4) NOT NULL,
	[SalesTaxTypeId] [tinyint] NOT NULL,
	[SalesTaxAmount] [decimal](18, 4) NOT NULL,
	[UseStockItemControl] [bit] NOT NULL,
	[CostingTypeId] [tinyint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_FinancialSetting] PRIMARY KEY CLUSTERED 
(
	[FinancialSettingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IncomingPayment]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IncomingPayment](
	[IncomingPaymentId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[IncomingPaymentNo] [nvarchar](50) NOT NULL,
	[IsCashSales] [bit] NOT NULL,
	[CustomerId] [bigint] NULL,
	[Reference] [nvarchar](50) NOT NULL,
	[PaymentTypeId] [tinyint] NOT NULL,
	[BankId] [bigint] NOT NULL,
	[BankAccNo] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Date] [date] NOT NULL,
	[Vat] [bit] NOT NULL,
	[Wht] [bit] NOT NULL,
	[WhtPercent] [decimal](18, 4) NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Subtotal] [decimal](18, 4) NOT NULL,
	[Tax] [decimal](18, 4) NOT NULL,
	[Total] [decimal](18, 4) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_IncomingPayment] PRIMARY KEY CLUSTERED 
(
	[IncomingPaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IncomingPaymentItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IncomingPaymentItem](
	[IncomingPaymentItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[IncomingPaymentId] [bigint] NOT NULL,
	[SalesInvoiceId] [bigint] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[IsDisabled] [decimal](18, 4) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_IncomingPaymentItem] PRIMARY KEY CLUSTERED 
(
	[IncomingPaymentItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Item]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[ItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemGroupId] [bigint] NOT NULL,
	[ItemName] [nvarchar](100) NOT NULL,
	[Barcode] [varchar](50) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Cost] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[Price02] [decimal](18, 4) NULL,
	[Price03] [decimal](18, 4) NULL,
	[Price04] [decimal](18, 4) NULL,
	[IsForSales] [bit] NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemAdjustment]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemAdjustment](
	[AdjustmentId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[AdjustmentName] [nvarchar](max) NOT NULL,
	[AdjustmentReasonId] [tinyint] NOT NULL,
	[AdjustmentNo] [nvarchar](50) NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Status] [varchar](16) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ItemAdjustment_1] PRIMARY KEY CLUSTERED 
(
	[AdjustmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemAdjustmentItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemAdjustmentItem](
	[AdjustmentItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[AdjustmentId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[QuantityBefore] [decimal](18, 4) NOT NULL,
	[QuantityAfter] [decimal](18, 4) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ItemAdjustmentItem] PRIMARY KEY CLUSTERED 
(
	[AdjustmentItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemAdjustmentReason]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemAdjustmentReason](
	[AdjustmentReasonId] [bigint] NOT NULL,
	[AdjustmentReasonName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_ItemAdjustmentReason] PRIMARY KEY CLUSTERED 
(
	[AdjustmentReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ItemBrand]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemBrand](
	[ItemBrandId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ItemBrandName] [nvarchar](100) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[ItemBrandId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemCategory]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemCategory](
	[ItemCategoryId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ItemCategoryName] [nvarchar](100) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModify] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[ItemCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemGroup]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemGroup](
	[ItemGroupId] [bigint] IDENTITY(100000000,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ItemGroupCode] [varchar](50) NOT NULL,
	[ItemGroupName] [nvarchar](100) NOT NULL,
	[ImagePath] [varchar](max) NOT NULL,
	[ItemCategoryId] [bigint] NULL,
	[ItemBrandId] [bigint] NULL,
	[Cost] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[Uom] [nvarchar](50) NOT NULL,
	[IsForSales] [bit] NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ItemGroup] PRIMARY KEY CLUSTERED 
(
	[ItemGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemRestocking]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemRestocking](
	[RestockingId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Threshold] [decimal](18, 4) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ItemRestocking] PRIMARY KEY CLUSTERED 
(
	[RestockingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemTransfer]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemTransfer](
	[TransferId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransferName] [nvarchar](200) NOT NULL,
	[SourceBusinessId] [bigint] NOT NULL,
	[TargetBusinessId] [bigint] NOT NULL,
	[Reference] [nvarchar](100) NOT NULL,
	[DateSent] [datetime] NOT NULL,
	[DateReceived] [datetime] NOT NULL,
	[TransferStatusTypeId] [tinyint] NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ItemTransfer] PRIMARY KEY CLUSTERED 
(
	[TransferId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemTransferItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemTransferItem](
	[TransferItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[TransferId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[ItemGroupCode] [varchar](50) NOT NULL,
	[ItemGroupName] [nvarchar](100) NOT NULL,
	[ItemName] [nvarchar](100) NOT NULL,
	[Uom] [nvarchar](100) NOT NULL,
	[ImagePath] [varchar](max) NOT NULL,
	[ItemCategoryName] [nvarchar](100) NOT NULL,
	[ItemBrandName] [nvarchar](100) NOT NULL,
	[Barcode] [varchar](50) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Cost] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[IsForSales] [bit] NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ItemTransferItem] PRIMARY KEY CLUSTERED 
(
	[TransferItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemTransferStatusType]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemTransferStatusType](
	[TransferStatusTypeId] [tinyint] NOT NULL,
	[TransferStatusTypeName] [varchar](20) NOT NULL,
	[IsForTransferItem] [bit] NOT NULL,
	[IsForReceiveTransferItem] [bit] NOT NULL,
 CONSTRAINT [PK_ItemTransferStatusType] PRIMARY KEY CLUSTERED 
(
	[TransferStatusTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OutgoingPayment]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OutgoingPayment](
	[OutgoingPaymentId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[OutgoingPaymentNo] [nvarchar](50) NOT NULL,
	[IsCashPurchase] [bit] NOT NULL,
	[SupplierId] [bigint] NULL,
	[Reference] [nvarchar](50) NOT NULL,
	[ContactPerson] [nvarchar](100) NOT NULL,
	[Detail1] [nvarchar](100) NOT NULL,
	[Detail2] [nvarchar](100) NOT NULL,
	[Detail3] [nvarchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Date] [date] NOT NULL,
	[PaymentTypeId] [tinyint] NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[TaxIdInfo] [nvarchar](max) NOT NULL,
	[Subtotal] [decimal](18, 4) NOT NULL,
	[Tax] [decimal](18, 4) NOT NULL,
	[Total] [decimal](18, 4) NOT NULL,
	[Status] [varchar](16) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_OutgoingPayment] PRIMARY KEY CLUSTERED 
(
	[OutgoingPaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OutgoingPaymentItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OutgoingPaymentItem](
	[OutgoingPaymentItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[OutgoingPaymentId] [bigint] NOT NULL,
	[ReceiveOrderId] [bigint] NOT NULL,
	[SupplierInvoice] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_OutgoingPaymentItem] PRIMARY KEY CLUSTERED 
(
	[OutgoingPaymentItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentType]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentType](
	[PaymentTypeId] [tinyint] NOT NULL,
	[PaymentTypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_PaymentMethodType] PRIMARY KEY CLUSTERED 
(
	[PaymentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchaseOrder]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseOrder](
	[PurchaseOrderId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[PurchaseOrderNo] [nvarchar](50) NOT NULL,
	[IsCashPurchase] [bit] NOT NULL,
	[CashPurchaseNo] [nvarchar](50) NOT NULL,
	[SupplierId] [bigint] NULL,
	[Reference] [nvarchar](50) NOT NULL,
	[ContactPerson] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Date] [date] NOT NULL,
	[DateDelivery] [date] NOT NULL,
	[AmountTypeId] [tinyint] NULL,
	[PaymentTypeId] [tinyint] NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[CreditTerm] [nvarchar](max) NOT NULL,
	[Subtotal] [decimal](18, 4) NOT NULL,
	[Tax] [decimal](18, 4) NOT NULL,
	[Total] [decimal](18, 4) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_PurchaseOrder] PRIMARY KEY CLUSTERED 
(
	[PurchaseOrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseOrderItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseOrderItem](
	[PurchaseOrderItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[PurchaseOrderId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[QuantityReceived] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PurchaseOrderItem] PRIMARY KEY CLUSTERED 
(
	[PurchaseOrderItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReceiveOrder]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReceiveOrder](
	[ReceiveOrderId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[ReceiveOrderNo] [nvarchar](50) NOT NULL,
	[IsCashPurchase] [bit] NOT NULL,
	[SupplierId] [bigint] NULL,
	[Reference] [nvarchar](50) NOT NULL,
	[ContactPerson] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Date] [date] NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[PaymentTerm] [nvarchar](max) NOT NULL,
	[Subtotal] [decimal](18, 4) NOT NULL,
	[Tax] [decimal](18, 4) NOT NULL,
	[Total] [decimal](18, 4) NOT NULL,
	[AmountPaid] [decimal](18, 4) NOT NULL,
	[Status] [varchar](16) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ReceiveOrder] PRIMARY KEY CLUSTERED 
(
	[ReceiveOrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReceiveOrderItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReceiveOrderItem](
	[ReceiveOrderItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[ReceiveOrderId] [bigint] NOT NULL,
	[PurchaseOrderItemId] [bigint] NULL,
	[ItemId] [bigint] NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_ReceiveOrderItem] PRIMARY KEY CLUSTERED 
(
	[ReceiveOrderItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesInvoice]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesInvoice](
	[SalesInvoiceId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[SalesInvoiceNo] [nvarchar](50) NOT NULL,
	[IsCashSales] [bit] NOT NULL,
	[CashSalesNo] [nvarchar](50) NOT NULL,
	[CustomerId] [bigint] NULL,
	[Reference] [nvarchar](50) NOT NULL,
	[ContactPerson] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Date] [date] NOT NULL,
	[DateDelivery] [date] NOT NULL,
	[DestinationTypeId] [tinyint] NOT NULL,
	[PriceTypeId] [tinyint] NULL,
	[AmountTypeId] [tinyint] NOT NULL,
	[PaymentTypeId] [tinyint] NULL,
	[Note] [nvarchar](max) NOT NULL,
	[SalesPersonId] [bigint] NOT NULL,
	[Subtotal] [decimal](18, 4) NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Charge] [decimal](18, 4) NOT NULL,
	[Tax] [decimal](18, 4) NOT NULL,
	[Total] [decimal](18, 4) NOT NULL,
	[AmountPaid] [decimal](18, 4) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_SalesInvoice] PRIMARY KEY CLUSTERED 
(
	[SalesInvoiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesInvoiceItem]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesInvoiceItem](
	[SalesInvoiceItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[SalesInvoiceId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Price] [decimal](18, 4) NOT NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_SalesInvoiceItem] PRIMARY KEY CLUSTERED 
(
	[SalesInvoiceItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesTaxType]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesTaxType](
	[SalesTaxTypeId] [bigint] NOT NULL,
	[SalesTaxTypeName] [nvarchar](100) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScreenPermission]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScreenPermission](
	[ScreenPermissionId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[Admin] [varchar](max) NOT NULL,
	[Manager] [varchar](max) NOT NULL,
	[Stocking] [varchar](max) NOT NULL,
	[Purchase] [varchar](max) NOT NULL,
	[Sales] [varchar](max) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ScreenPermission] PRIMARY KEY CLUSTERED 
(
	[ScreenPermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Staff](
	[StaffId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Title] [varchar](4) NULL,
	[Firstname] [nvarchar](50) NOT NULL,
	[Lastname] [nvarchar](50) NOT NULL,
	[Birthdate] [date] NULL,
	[Gender] [char](1) NULL,
	[Identification] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Fax] [nvarchar](50) NOT NULL,
	[Email] [varchar](254) NOT NULL,
	[FacebookId] [nvarchar](50) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[CountryId] [tinyint] NULL,
	[ZipCode] [nvarchar](50) NOT NULL,
	[Role] [varchar](10) NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[Guid] [varchar](50) NOT NULL,
	[DateGuidExpired] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Supplier](
	[SupplierId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessId] [bigint] NOT NULL,
	[SupplierName] [nvarchar](100) NOT NULL,
	[SupplierNo] [nvarchar](50) NOT NULL,
	[SupplierGroupId] [tinyint] NULL,
	[TaxNo] [nvarchar](50) NOT NULL,
	[ContactPerson] [nvarchar](100) NOT NULL,
	[Email] [varchar](320) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Fax] [nvarchar](50) NOT NULL,
	[CreditTerm] [nvarchar](max) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[CountryId] [tinyint] NULL,
	[ZipCode] [nvarchar](50) NOT NULL,
	[Remark] [nvarchar](max) NOT NULL,
	[IsDisabled] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[CreatedBy] [varchar](254) NOT NULL,
	[UpdatedBy] [varchar](254) NOT NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierGroup]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierGroup](
	[SupplierGroupId] [bigint] IDENTITY(1,1) NOT NULL,
	[SupplierGroupName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_SupplierGroup] PRIMARY KEY CLUSTERED 
(
	[SupplierGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[System_BusinessRole]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[System_BusinessRole](
	[BusinessRoleId] [int] NOT NULL,
	[BusinessRoleName] [varchar](50) NOT NULL,
	[BusinessRoleShortName] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](254) NOT NULL,
	[Password] [varbinary](50) NULL,
	[Salt] [varbinary](50) NULL,
	[Email] [varchar](254) NOT NULL,
	[Guid] [varchar](50) NOT NULL,
	[DateGuidExpired] [datetime] NULL,
	[IsDisabled] [bit] NOT NULL,
	[IsFacebookUser] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserPending]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPending](
	[PendingUserItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DateCreated] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [bigint] NOT NULL,
	[Title] [varchar](4) NULL,
	[Firstname] [nvarchar](50) NOT NULL,
	[Lastname] [nvarchar](50) NOT NULL,
	[Birthdate] [date] NULL,
	[Gender] [char](1) NULL,
	[Identification] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Fax] [nvarchar](50) NOT NULL,
	[FacebookId] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Address3] [nvarchar](100) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[CountryId] [tinyint] NULL,
	[ZipCode] [nvarchar](50) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
 CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AmountType] ([AmountTypeId], [AmountTypeName]) VALUES (1, N'Include Vat 7%')
INSERT [dbo].[AmountType] ([AmountTypeId], [AmountTypeName]) VALUES (2, N'Other')
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([BillId], [BusinessId], [CustomerId], [DiscountAmount], [DiscountPercent], [IsDiscountPercent], [Total], [Subtotal], [Tax], [Status], [Note], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (67, 1, NULL, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 0, CAST(5.3500 AS Decimal(18, 4)), CAST(10.0000 AS Decimal(18, 4)), CAST(0.3500 AS Decimal(18, 4)), N'Done', N'', 0, CAST(N'2015-11-23 04:20:39.630' AS DateTime), CAST(N'2015-11-23 04:25:49.567' AS DateTime), N'user_a1', N'Nov 23 2015  4:25AM')
INSERT [dbo].[Bill] ([BillId], [BusinessId], [CustomerId], [DiscountAmount], [DiscountPercent], [IsDiscountPercent], [Total], [Subtotal], [Tax], [Status], [Note], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (68, 1, NULL, CAST(50.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 0, CAST(16.0500 AS Decimal(18, 4)), CAST(65.0000 AS Decimal(18, 4)), CAST(1.0500 AS Decimal(18, 4)), N'Done', N'', 0, CAST(N'2015-11-24 01:11:19.287' AS DateTime), CAST(N'2015-11-24 01:11:52.640' AS DateTime), N'user_a1', N'Nov 24 2015  1:11AM')
INSERT [dbo].[Bill] ([BillId], [BusinessId], [CustomerId], [DiscountAmount], [DiscountPercent], [IsDiscountPercent], [Total], [Subtotal], [Tax], [Status], [Note], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10058, 1, NULL, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 0, CAST(3.2100 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), CAST(0.2100 AS Decimal(18, 4)), N'Done', N'', 0, CAST(N'2015-11-24 01:44:47.267' AS DateTime), CAST(N'2015-11-24 01:44:47.267' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Bill] ([BillId], [BusinessId], [CustomerId], [DiscountAmount], [DiscountPercent], [IsDiscountPercent], [Total], [Subtotal], [Tax], [Status], [Note], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10059, 1, NULL, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 0, CAST(1.0700 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(0.0700 AS Decimal(18, 4)), N'Done', N'', 0, CAST(N'2015-11-24 01:46:47.900' AS DateTime), CAST(N'2015-11-24 01:46:47.900' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Bill] ([BillId], [BusinessId], [CustomerId], [DiscountAmount], [DiscountPercent], [IsDiscountPercent], [Total], [Subtotal], [Tax], [Status], [Note], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10060, 1, NULL, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 0, CAST(1.0700 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(0.0700 AS Decimal(18, 4)), N'Done', N'', 0, CAST(N'2015-11-24 01:49:02.833' AS DateTime), CAST(N'2015-11-24 01:49:02.833' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Bill] ([BillId], [BusinessId], [CustomerId], [DiscountAmount], [DiscountPercent], [IsDiscountPercent], [Total], [Subtotal], [Tax], [Status], [Note], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10061, 1, 1, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 0, CAST(4.2800 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), CAST(0.2800 AS Decimal(18, 4)), N'Open', N'', 0, CAST(N'2015-11-24 02:04:22.143' AS DateTime), CAST(N'2015-11-24 02:04:29.150' AS DateTime), N'user_a1', N'Nov 24 2015  2:04AM')
SET IDENTITY_INSERT [dbo].[Bill] OFF
SET IDENTITY_INSERT [dbo].[BillItem] ON 

INSERT [dbo].[BillItem] ([BillItemId], [BillId], [ItemId], [Quantity], [Discount], [Price], [IsDisabled]) VALUES (86, 67, 23, CAST(10.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillItem] ([BillItemId], [BillId], [ItemId], [Quantity], [Discount], [Price], [IsDisabled]) VALUES (87, 68, 13, CAST(5.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillItem] ([BillItemId], [BillId], [ItemId], [Quantity], [Discount], [Price], [IsDisabled]) VALUES (10068, 10058, 10, CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillItem] ([BillItemId], [BillId], [ItemId], [Quantity], [Discount], [Price], [IsDisabled]) VALUES (10069, 10059, 6, CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillItem] ([BillItemId], [BillId], [ItemId], [Quantity], [Discount], [Price], [IsDisabled]) VALUES (10070, 10060, 23, CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillItem] ([BillItemId], [BillId], [ItemId], [Quantity], [Discount], [Price], [IsDisabled]) VALUES (10071, 10061, 12, CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0)
SET IDENTITY_INSERT [dbo].[BillItem] OFF
SET IDENTITY_INSERT [dbo].[BillPaymentItem] ON 

INSERT [dbo].[BillPaymentItem] ([BillPaymentItemId], [BillId], [PaymentTypeId], [Amount], [IsDisabled]) VALUES (1, 67, 1, CAST(6.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillPaymentItem] ([BillPaymentItemId], [BillId], [PaymentTypeId], [Amount], [IsDisabled]) VALUES (2, 68, 1, CAST(500.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillPaymentItem] ([BillPaymentItemId], [BillId], [PaymentTypeId], [Amount], [IsDisabled]) VALUES (10002, 10058, 1, CAST(7.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillPaymentItem] ([BillPaymentItemId], [BillId], [PaymentTypeId], [Amount], [IsDisabled]) VALUES (10003, 10059, 1, CAST(5.0000 AS Decimal(18, 4)), 0)
INSERT [dbo].[BillPaymentItem] ([BillPaymentItemId], [BillId], [PaymentTypeId], [Amount], [IsDisabled]) VALUES (10004, 10060, 1, CAST(9.0000 AS Decimal(18, 4)), 0)
SET IDENTITY_INSERT [dbo].[BillPaymentItem] OFF
SET IDENTITY_INSERT [dbo].[Business] ON 

INSERT [dbo].[Business] ([BusinessId], [BusinessName], [BusinessTypeId], [ImagePath], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Phone], [Fax], [Email], [Website], [FacebookUrl], [TaxIdInfo], [DateExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (1, N'Business A1', 4, N'0', N'101/434 Rama 9 Rd.,', N'test2', N'test3', N'Huay-Kwang', NULL, N'10310', N'+66894352744', N'fax2222', N'user_a1@retailgoo.com', N'web.com', N'facebook.com/fb', N'15030403341232312', CAST(N'2014-06-10 00:00:00.000' AS DateTime), CAST(N'2014-06-10 07:31:40.287' AS DateTime), CAST(N'2015-09-13 23:55:00.330' AS DateTime), 1, 1)
INSERT [dbo].[Business] ([BusinessId], [BusinessName], [BusinessTypeId], [ImagePath], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Phone], [Fax], [Email], [Website], [FacebookUrl], [TaxIdInfo], [DateExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (2, N'Business A2', 1, N'', N'', N'', N'', N'', NULL, N'', N'', N'', N'user_a1@retailgoo.com', N'', N'', N'', CAST(N'2014-08-10 07:35:43.753' AS DateTime), CAST(N'2014-06-10 07:35:43.753' AS DateTime), CAST(N'2014-06-10 07:35:43.753' AS DateTime), 1, 1)
INSERT [dbo].[Business] ([BusinessId], [BusinessName], [BusinessTypeId], [ImagePath], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Phone], [Fax], [Email], [Website], [FacebookUrl], [TaxIdInfo], [DateExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10003, N'Business B1', 1, N'', N'', N'', N'', N'', NULL, N'', N'', N'', N'business_b1@retailgoo.com', N'', N'', N'', CAST(N'2014-08-10 00:00:00.000' AS DateTime), CAST(N'2014-08-10 00:00:00.000' AS DateTime), CAST(N'2014-08-10 00:00:00.000' AS DateTime), 1, 1)
INSERT [dbo].[Business] ([BusinessId], [BusinessName], [BusinessTypeId], [ImagePath], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Phone], [Fax], [Email], [Website], [FacebookUrl], [TaxIdInfo], [DateExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10004, N'test2', 255, N'', N'', N'', N'', N'', NULL, N'', N'', N'', N'', N'', N'', N'', CAST(N'2015-10-22 23:49:06.550' AS DateTime), CAST(N'2015-08-23 23:49:06.550' AS DateTime), CAST(N'2015-08-23 23:49:06.550' AS DateTime), 3, 3)
INSERT [dbo].[Business] ([BusinessId], [BusinessName], [BusinessTypeId], [ImagePath], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Phone], [Fax], [Email], [Website], [FacebookUrl], [TaxIdInfo], [DateExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10005, N'TestBusiness2', NULL, N'', N'', N'', N'', N'', NULL, N'', N'', N'', N'', N'', N'', N'', CAST(N'2015-11-12 23:09:45.283' AS DateTime), CAST(N'2015-09-13 23:09:45.290' AS DateTime), CAST(N'2015-09-13 23:09:45.290' AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[Business] OFF
SET IDENTITY_INSERT [dbo].[BusinessRelation] ON 

INSERT [dbo].[BusinessRelation] ([RelationId], [BusinessId], [RootBusinessId], [ParentBusinessId]) VALUES (1, 1, 1, 1)
INSERT [dbo].[BusinessRelation] ([RelationId], [BusinessId], [RootBusinessId], [ParentBusinessId]) VALUES (2, 2, 1, 1)
SET IDENTITY_INSERT [dbo].[BusinessRelation] OFF
SET IDENTITY_INSERT [dbo].[BusinessType] ON 

INSERT [dbo].[BusinessType] ([BusinessTypeId], [BusinessTypeName]) VALUES (1, N'Book Store')
INSERT [dbo].[BusinessType] ([BusinessTypeId], [BusinessTypeName]) VALUES (2, N'Cafe')
INSERT [dbo].[BusinessType] ([BusinessTypeId], [BusinessTypeName]) VALUES (3, N'Restaurant')
INSERT [dbo].[BusinessType] ([BusinessTypeId], [BusinessTypeName]) VALUES (4, N'Retail Shop')
INSERT [dbo].[BusinessType] ([BusinessTypeId], [BusinessTypeName]) VALUES (5, N'Grocery')
INSERT [dbo].[BusinessType] ([BusinessTypeId], [BusinessTypeName]) VALUES (6, N'Mini Mart')
INSERT [dbo].[BusinessType] ([BusinessTypeId], [BusinessTypeName]) VALUES (7, N'Other')
SET IDENTITY_INSERT [dbo].[BusinessType] OFF
INSERT [dbo].[CostingType] ([CostingTypeId], [CostingTypeName]) VALUES (1, N'Normal')
INSERT [dbo].[Country] ([CountryId], [CountryShortName], [CountryName]) VALUES (1, N'TH', N'Thailand')
INSERT [dbo].[Country] ([CountryId], [CountryShortName], [CountryName]) VALUES (2, N'US', N'United States')
INSERT [dbo].[Currency] ([CurrencyId], [CurrencyName], [CurrencySymbol]) VALUES (1, N'Thai Baht', N'฿')
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([CustomerId], [BusinessId], [CustomerName], [CustomerNo], [CustomerGroupId], [TaxNo], [DefaultPriceTypeId], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [CreditLimit], [AddressBilling1], [AddressBilling2], [AddressBilling3], [CityBilling], [CountryBillingId], [ZipCodeBilling], [RemarkBilling], [AddressShipping1], [AddressShipping2], [AddressShipping3], [CityShipping], [CountryShippingId], [ZipCodeShipping], [RemarkShipping], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (1, 1, N'Customer AAAA', N'', 1, N'A0000001', 1, N'Person A1', N'personA1@email.com', N'054-223001', N'053-333222', N'A1 credit term', N'A1 credit limit', N'321/1', N'Suraj Rg.', N'Mueng', N'Bangkok', 2, N'52000', N'A1 Remark', N'666-555/111', N'ToNight Road.', N'Khaosarn', N'Chiang Mai', 2, N'52000', N'A1 Remark', 0, CAST(N'2014-06-14 00:00:00.000' AS DateTime), CAST(N'2015-01-08 19:50:00.590' AS DateTime), N'system', N'1')
INSERT [dbo].[Customer] ([CustomerId], [BusinessId], [CustomerName], [CustomerNo], [CustomerGroupId], [TaxNo], [DefaultPriceTypeId], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [CreditLimit], [AddressBilling1], [AddressBilling2], [AddressBilling3], [CityBilling], [CountryBillingId], [ZipCodeBilling], [RemarkBilling], [AddressShipping1], [AddressShipping2], [AddressShipping3], [CityShipping], [CountryShippingId], [ZipCodeShipping], [RemarkShipping], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (2, 1, N'Customer A2', N'CUST_A02', 2, N'09452111', 1, N'David Moyes', N'dm@email.com', N'089-4357222, 02-2223311', N'02-4567333', N'this is a test credit term', N'this is a test limit', N'44', N'Suanluang Rd.', N'Huay-Kwang', N'Bkk', 2, N'10311', N'Note A2', N'998', N'U Chu LIang bldg.', N'Bang-Rak', N'Bangkok', 2, N'10300', N'Test ShipTo', 0, CAST(N'2014-06-14 00:00:00.000' AS DateTime), CAST(N'2014-06-14 00:00:00.000' AS DateTime), N'system', N'user_a1')
INSERT [dbo].[Customer] ([CustomerId], [BusinessId], [CustomerName], [CustomerNo], [CustomerGroupId], [TaxNo], [DefaultPriceTypeId], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [CreditLimit], [AddressBilling1], [AddressBilling2], [AddressBilling3], [CityBilling], [CountryBillingId], [ZipCodeBilling], [RemarkBilling], [AddressShipping1], [AddressShipping2], [AddressShipping3], [CityShipping], [CountryShippingId], [ZipCodeShipping], [RemarkShipping], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (4, 1, N'Test Customer1', N'', 1, N'ddf', 0, N'dfdf', N'df', N'dfdf', N'df', N'dfdf', N'dfdfdf', N'df', N'df', N'df', N'dfdf', NULL, N'', N'', N'', N'', N'', N'', NULL, N'', N'', 1, CAST(N'2015-01-08 19:55:49.447' AS DateTime), CAST(N'2015-01-08 19:55:57.447' AS DateTime), N'1', N'1')
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET IDENTITY_INSERT [dbo].[CustomerGroup] ON 

INSERT [dbo].[CustomerGroup] ([CustomerGroupId], [CustomerGroupName]) VALUES (1, N'Telecommunication')
INSERT [dbo].[CustomerGroup] ([CustomerGroupId], [CustomerGroupName]) VALUES (2, N'Restaurant')
INSERT [dbo].[CustomerGroup] ([CustomerGroupId], [CustomerGroupName]) VALUES (3, N'Retail Shop')
INSERT [dbo].[CustomerGroup] ([CustomerGroupId], [CustomerGroupName]) VALUES (4, N'Cafe')
INSERT [dbo].[CustomerGroup] ([CustomerGroupId], [CustomerGroupName]) VALUES (5, N'Book Store')
INSERT [dbo].[CustomerGroup] ([CustomerGroupId], [CustomerGroupName]) VALUES (6, N'Manufacturer')
SET IDENTITY_INSERT [dbo].[CustomerGroup] OFF
SET IDENTITY_INSERT [dbo].[DefaultPriceType] ON 

INSERT [dbo].[DefaultPriceType] ([DefaultPriceTypeId], [DefaultPriceTypeName]) VALUES (1, N'Normal')
SET IDENTITY_INSERT [dbo].[DefaultPriceType] OFF
INSERT [dbo].[DestinationType] ([DestinationTypeId], [DestinationTypeName]) VALUES (1, N'Billing Address')
INSERT [dbo].[DestinationType] ([DestinationTypeId], [DestinationTypeName]) VALUES (2, N'Shipping Address')
INSERT [dbo].[DestinationType] ([DestinationTypeId], [DestinationTypeName]) VALUES (3, N'Other')
SET IDENTITY_INSERT [dbo].[DocSetting] ON 

INSERT [dbo].[DocSetting] ([DocSettingId], [BusinessId], [SalesOrderFormat], [SalesOrderNo], [SalesOrderTemplate], [InvoiceFormat], [InvoiceNo], [InvoiceTemplate], [ReceiptFormat], [ReceiptNo], [ReceiptTemplate], [StockAdjFormat], [StockAdjNo], [StockAdjTemplate], [ReceiveOrderFormat], [ReceiveOrderNo], [ReceiveOrderTemplate], [CashPurchaseFormat], [CashPurchaseNo], [CashPurchaseTemplate], [CashSalesFormat], [CashSalesNo], [CashSalesTemplate], [PurchaseOrderFormat], [PurchaseOrderNo], [PurchaseOrderTemplate], [PaymentFormat], [PaymentNo], [PaymentTemplate], [ExpenseFormat], [ExpenseNo], [ExpenseTemplate], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (1, 1, N'SO-{XXXXXXX}', 0, 0, N'IV-{XXXXXXX}', 4, 0, N'RV-{XXXXXXX}', 0, 0, N'SA-{XXXXXXX}', 0, 0, N'RO-{XXXXXXX}', 39, 0, N'CP-{XXXXXXX}', 22, 0, N'CS-{XXXXXXX}', 3, 0, N'PO-{XXXXXXX}', 296, 0, N'PV-{XXXXXXX}', 32, 0, N'EX-{XXXXXXX}', 5, 0, CAST(N'2014-10-20 21:46:31.283' AS DateTime), CAST(N'2014-10-20 21:46:31.283' AS DateTime), N'system', N'user_a1')
INSERT [dbo].[DocSetting] ([DocSettingId], [BusinessId], [SalesOrderFormat], [SalesOrderNo], [SalesOrderTemplate], [InvoiceFormat], [InvoiceNo], [InvoiceTemplate], [ReceiptFormat], [ReceiptNo], [ReceiptTemplate], [StockAdjFormat], [StockAdjNo], [StockAdjTemplate], [ReceiveOrderFormat], [ReceiveOrderNo], [ReceiveOrderTemplate], [CashPurchaseFormat], [CashPurchaseNo], [CashPurchaseTemplate], [CashSalesFormat], [CashSalesNo], [CashSalesTemplate], [PurchaseOrderFormat], [PurchaseOrderNo], [PurchaseOrderTemplate], [PaymentFormat], [PaymentNo], [PaymentTemplate], [ExpenseFormat], [ExpenseNo], [ExpenseTemplate], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 2, N'SO-{XXXXXXX}', 0, 0, N'IV-{XXXXXXX}', 0, 0, N'RV-{XXXXXXX}', 0, 0, N'SA-{XXXXXXX}', 0, 0, N'RO-{XXXXXXX}', 0, 0, N'CP-{XXXXXXX}', 0, 0, N'CS-{XXXXXXX}', 0, 0, N'PO-{XXXXXXX}', 0, 0, N'PV-{XXXXXXX}', 0, 0, N'EX-{XXXXXXX}', 0, 0, CAST(N'2014-01-01 00:00:00.000' AS DateTime), CAST(N'2014-06-01 00:00:00.000' AS DateTime), N'system', N'user_a1')
INSERT [dbo].[DocSetting] ([DocSettingId], [BusinessId], [SalesOrderFormat], [SalesOrderNo], [SalesOrderTemplate], [InvoiceFormat], [InvoiceNo], [InvoiceTemplate], [ReceiptFormat], [ReceiptNo], [ReceiptTemplate], [StockAdjFormat], [StockAdjNo], [StockAdjTemplate], [ReceiveOrderFormat], [ReceiveOrderNo], [ReceiveOrderTemplate], [CashPurchaseFormat], [CashPurchaseNo], [CashPurchaseTemplate], [CashSalesFormat], [CashSalesNo], [CashSalesTemplate], [PurchaseOrderFormat], [PurchaseOrderNo], [PurchaseOrderTemplate], [PaymentFormat], [PaymentNo], [PaymentTemplate], [ExpenseFormat], [ExpenseNo], [ExpenseTemplate], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (6, 10005, N'SO-{XXXXXX}', 0, 0, N'IV-{XXXXXX}', 0, 0, N'RV-{XXXXXX}', 0, 0, N'SA-{XXXXXX}', 0, 0, N'RO-{XXXXXX}', 0, 0, N'CP-{XXXXXX}', 0, 0, N'CS-{XXXXXX}', 0, 0, N'PO-{XXXXXX}', 0, 0, N'PV-{XXXXXX}', 0, 0, N'EX-{XXXXXX}', 0, 0, CAST(N'2015-09-13 23:09:45.290' AS DateTime), CAST(N'2015-09-13 23:09:45.290' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[DocSetting] OFF
SET IDENTITY_INSERT [dbo].[Expense] ON 

INSERT [dbo].[Expense] ([ExpenseId], [BusinessId], [ExpenseNo], [Amount], [UseWht], [WhtPercent], [WhtAmount], [SupplierId], [ExpenseCategoryId], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [IsVoided], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 1, N'EX-0000001', CAST(500.0000 AS Decimal(18, 4)), 1, CAST(5.0000 AS Decimal(18, 4)), CAST(25.0000 AS Decimal(18, 4)), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2014-11-11' AS Date), N'my note1', NULL, NULL, NULL, NULL, 0, 1, CAST(N'2014-11-12 09:24:01.653' AS DateTime), CAST(N'2014-11-12 09:51:28.720' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Expense] ([ExpenseId], [BusinessId], [ExpenseNo], [Amount], [UseWht], [WhtPercent], [WhtAmount], [SupplierId], [ExpenseCategoryId], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [IsVoided], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (6, 1, N'EX-0000002', CAST(500.0000 AS Decimal(18, 4)), 1, CAST(5.0000 AS Decimal(18, 4)), CAST(25.0000 AS Decimal(18, 4)), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2014-11-11' AS Date), N'noteeeee', NULL, NULL, NULL, NULL, 0, 0, CAST(N'2014-11-12 09:27:23.387' AS DateTime), CAST(N'2014-11-12 09:27:23.387' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Expense] ([ExpenseId], [BusinessId], [ExpenseNo], [Amount], [UseWht], [WhtPercent], [WhtAmount], [SupplierId], [ExpenseCategoryId], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [IsVoided], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (8, 1, N'EX-0000003', CAST(10.0000 AS Decimal(18, 4)), 0, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2014-11-12' AS Date), N'This bill for water', NULL, NULL, NULL, NULL, 0, 0, CAST(N'2014-11-12 09:40:51.087' AS DateTime), CAST(N'2014-11-12 09:40:51.087' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Expense] ([ExpenseId], [BusinessId], [ExpenseNo], [Amount], [UseWht], [WhtPercent], [WhtAmount], [SupplierId], [ExpenseCategoryId], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [IsVoided], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (9, 1, N'EX-0000004', CAST(25.0000 AS Decimal(18, 4)), 1, CAST(5.0000 AS Decimal(18, 4)), CAST(1250.0000 AS Decimal(18, 4)), 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2014-11-12' AS Date), N'Test3 Note', NULL, NULL, NULL, NULL, 0, 1, CAST(N'2014-11-12 09:42:46.443' AS DateTime), CAST(N'2015-01-10 16:27:53.263' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Expense] ([ExpenseId], [BusinessId], [ExpenseNo], [Amount], [UseWht], [WhtPercent], [WhtAmount], [SupplierId], [ExpenseCategoryId], [Reference], [Contact], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [IsVoided], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10, 1, N'EX-0000005', CAST(900.0000 AS Decimal(18, 4)), 1, CAST(5.0000 AS Decimal(18, 4)), CAST(45.0000 AS Decimal(18, 4)), 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2015-02-05' AS Date), N'OPPPP', NULL, NULL, NULL, NULL, 0, 0, CAST(N'2015-02-05 07:35:50.040' AS DateTime), CAST(N'2015-02-05 07:35:50.040' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[Expense] OFF
SET IDENTITY_INSERT [dbo].[ExpenseCategory] ON 

INSERT [dbo].[ExpenseCategory] ([ExpenseCategoryId], [BusinessId], [ExpenseCategoryName], [IsDisabled]) VALUES (1, 1, N'Electricity bill', 0)
INSERT [dbo].[ExpenseCategory] ([ExpenseCategoryId], [BusinessId], [ExpenseCategoryName], [IsDisabled]) VALUES (2, 1, N'Water supply bill', 0)
INSERT [dbo].[ExpenseCategory] ([ExpenseCategoryId], [BusinessId], [ExpenseCategoryName], [IsDisabled]) VALUES (3, 1, N'Peripheral', 0)
SET IDENTITY_INSERT [dbo].[ExpenseCategory] OFF
SET IDENTITY_INSERT [dbo].[FinancialSetting] ON 

INSERT [dbo].[FinancialSetting] ([FinancialSettingId], [BusinessId], [CurrencyId], [DefaultPriceTypeId], [UseServiceCharge], [ServiceChargeAmount], [SalesTaxTypeId], [SalesTaxAmount], [UseStockItemControl], [CostingTypeId], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (1, 1, 1, 1, 1, CAST(10.0000 AS Decimal(18, 4)), 1, CAST(7.0000 AS Decimal(18, 4)), 1, 1, CAST(N'2015-06-01 00:00:00.000' AS DateTime), CAST(N'2015-06-01 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[FinancialSetting] ([FinancialSettingId], [BusinessId], [CurrencyId], [DefaultPriceTypeId], [UseServiceCharge], [ServiceChargeAmount], [SalesTaxTypeId], [SalesTaxAmount], [UseStockItemControl], [CostingTypeId], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (2, 10005, 1, 1, 1, CAST(10.0000 AS Decimal(18, 4)), 1, CAST(7.0000 AS Decimal(18, 4)), 0, 1, CAST(N'2015-09-13 23:09:45.290' AS DateTime), CAST(N'2015-09-13 23:09:45.290' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[FinancialSetting] OFF
SET IDENTITY_INSERT [dbo].[IncomingPayment] ON 

INSERT [dbo].[IncomingPayment] ([IncomingPaymentId], [BusinessId], [IncomingPaymentNo], [IsCashSales], [CustomerId], [Reference], [PaymentTypeId], [BankId], [BankAccNo], [Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 1, N'CS-0000003', 1, 2, N'', 1, 1, N'', N'44', N'Suanluang Rd.', N'Huay-Kwang', N'089-4357222, 02-2223311', CAST(N'2015-02-10' AS Date), 0, 0, CAST(0.0000 AS Decimal(18, 4)), N'', CAST(21.0000 AS Decimal(18, 4)), CAST(1.4700 AS Decimal(18, 4)), CAST(22.4700 AS Decimal(18, 4)), N'Done', 0, CAST(N'2015-02-10 08:01:14.257' AS DateTime), CAST(N'2015-02-10 08:01:14.257' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[IncomingPayment] ([IncomingPaymentId], [BusinessId], [IncomingPaymentNo], [IsCashSales], [CustomerId], [Reference], [PaymentTypeId], [BankId], [BankAccNo], [Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (7, 1, N'PV-0000032', 0, 2, N'', 1, 1, N'', N'44', N'Suanluang Rd.', N'Huay-Kwang', N'089-4357222, 02-2223311', CAST(N'2015-02-10' AS Date), 0, 0, CAST(0.0000 AS Decimal(18, 4)), N'', CAST(34.2400 AS Decimal(18, 4)), CAST(0.0200 AS Decimal(18, 4)), CAST(34.2200 AS Decimal(18, 4)), N'Paid', 0, CAST(N'2015-02-10 08:02:12.263' AS DateTime), CAST(N'2015-02-10 08:02:12.263' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[IncomingPayment] OFF
SET IDENTITY_INSERT [dbo].[IncomingPaymentItem] ON 

INSERT [dbo].[IncomingPaymentItem] ([IncomingPaymentItemId], [IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 5, 9, CAST(22.4700 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(N'2015-02-10 08:01:14.257' AS DateTime), CAST(N'2015-02-10 08:01:14.257' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[IncomingPaymentItem] ([IncomingPaymentItemId], [IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (6, 7, 10, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(N'2015-02-10 08:02:12.263' AS DateTime), CAST(N'2015-02-10 08:02:12.263' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[IncomingPaymentItem] OFF
SET IDENTITY_INSERT [dbo].[Item] ON 

INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (2, 100000002, N'Polo shirts', N'12345678', CAST(-353.0000 AS Decimal(18, 4)), CAST(9.0000 AS Decimal(18, 4)), CAST(1.5000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (3, 100000002, N'Hoolahoop', N'23456789', CAST(-388.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (4, 100000022, N'Smart Purse', N'34567890', CAST(-352.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 100000023, N'T-Shirt XXL', N'44556677', CAST(-395.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (6, 100000025, N'Pants', N'', CAST(-396.0000 AS Decimal(18, 4)), CAST(7.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10, 100000026, N'Hat', N'', CAST(-387.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (12, 100000028, N'Halloween stick', N'', CAST(-380.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (13, 100000029, N'Sugar', N'66789035', CAST(-404.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (14, 100000030, N'Salt', N'13459934', CAST(-408.0000 AS Decimal(18, 4)), CAST(6.0000 AS Decimal(18, 4)), CAST(34.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2012-08-02 00:00:00.000' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (19, 100000035, N'X''Mas trousers', N'66894545', CAST(-358.0000 AS Decimal(18, 4)), CAST(7.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2014-11-12 02:23:54.310' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (20, 100000036, N'Boogeyman', N'22467545', CAST(-341.0000 AS Decimal(18, 4)), CAST(2.0000 AS Decimal(18, 4)), CAST(45.0000 AS Decimal(18, 4)), NULL, NULL, NULL, 1, 0, CAST(N'2014-11-12 02:41:40.907' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (23, 100000038, N'HarryPotter Vol 2', N'', CAST(-369.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 0, CAST(N'2014-12-09 07:33:35.513' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'1', N'1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (24, 100000039, N'Polo shirts', N'12345678', CAST(0.0000 AS Decimal(18, 4)), CAST(9.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 0, CAST(N'2015-03-25 00:30:04.607' AS DateTime), CAST(N'2015-03-25 00:30:04.607' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (25, 100000039, N'Hoolahoop', N'23456789', CAST(0.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 0, CAST(N'2015-03-25 06:58:16.953' AS DateTime), CAST(N'2015-03-25 06:58:16.953' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (26, 100000040, N'Smart Purse', N'34567890', CAST(0.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 0, CAST(N'2015-03-25 06:58:16.953' AS DateTime), CAST(N'2015-03-25 06:58:16.953' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (27, 100000041, N'Halloween stick', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 0, CAST(N'2015-03-25 06:58:16.953' AS DateTime), CAST(N'2015-03-25 06:58:16.953' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Item] ([ItemId], [ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (28, 100000042, N'Pants', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(7.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 0, CAST(N'2015-03-25 07:27:57.203' AS DateTime), CAST(N'2015-03-25 07:27:57.203' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[Item] OFF
INSERT [dbo].[ItemAdjustmentReason] ([AdjustmentReasonId], [AdjustmentReasonName]) VALUES (1, N'Miscount')
INSERT [dbo].[ItemAdjustmentReason] ([AdjustmentReasonId], [AdjustmentReasonName]) VALUES (2, N'Manual restock')
SET IDENTITY_INSERT [dbo].[ItemBrand] ON 

INSERT [dbo].[ItemBrand] ([ItemBrandId], [BusinessId], [ItemBrandName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (1, 1, N'Uniqlo', 0, CAST(N'2014-11-11 00:00:00.000' AS DateTime), CAST(N'2014-11-11 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[ItemBrand] ([ItemBrandId], [BusinessId], [ItemBrandName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (2, 1, N'Levi', 0, CAST(N'2014-11-11 00:00:00.000' AS DateTime), CAST(N'2014-11-11 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[ItemBrand] ([ItemBrandId], [BusinessId], [ItemBrandName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (3, 2, N'Uniqlo', 0, CAST(N'2015-03-25 00:30:04.607' AS DateTime), CAST(N'2015-03-25 00:30:04.607' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemBrand] ([ItemBrandId], [BusinessId], [ItemBrandName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (4, 1, N'TestBrand', 0, CAST(N'2015-11-28 19:07:43.220' AS DateTime), CAST(N'2015-11-28 19:07:43.220' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[ItemBrand] OFF
SET IDENTITY_INSERT [dbo].[ItemCategory] ON 

INSERT [dbo].[ItemCategory] ([ItemCategoryId], [BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy], [UpdatedBy]) VALUES (1, 1, N'Home Appliance', 0, CAST(N'2014-11-11 00:00:00.000' AS DateTime), CAST(N'2014-11-11 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[ItemCategory] ([ItemCategoryId], [BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy], [UpdatedBy]) VALUES (2, 1, N'Decoration', 0, CAST(N'2014-11-11 00:00:00.000' AS DateTime), CAST(N'2014-11-11 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[ItemCategory] ([ItemCategoryId], [BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy], [UpdatedBy]) VALUES (4, 1, N'Electronics', 0, CAST(N'2014-11-11 00:00:00.000' AS DateTime), CAST(N'2014-11-11 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[ItemCategory] ([ItemCategoryId], [BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy], [UpdatedBy]) VALUES (5, 2, N'Home Appliance', 0, CAST(N'2015-03-25 00:30:04.607' AS DateTime), CAST(N'2015-03-25 00:30:04.607' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemCategory] ([ItemCategoryId], [BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy], [UpdatedBy]) VALUES (10001, 1, N'Fashion', 0, CAST(N'2014-11-01 00:00:00.000' AS DateTime), CAST(N'2014-11-01 00:00:00.000' AS DateTime), N'system', N'system')
SET IDENTITY_INSERT [dbo].[ItemCategory] OFF
SET IDENTITY_INSERT [dbo].[ItemGroup] ON 

INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000002, 1, N'100000002', N'NIKE Avatar', N' ', 1, 1, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-12 02:48:47.473' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000022, 1, N'100000022', N'Levi''s collection', N'', 10001, 2, CAST(1.0000 AS Decimal(18, 4)), CAST(2.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-11 00:07:47.113' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000023, 0, N'100000023', N'Ubisoft goods', N'', 4, 1, CAST(5.0000 AS Decimal(18, 4)), CAST(6.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-02 23:23:28.927' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000025, 1, N'100000025', N'Coach', N'', 1, 2, CAST(1.0000 AS Decimal(18, 4)), CAST(2.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-11 03:22:39.830' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000026, 1, N'100000026', N'GUCCI Winter Pouch', N'', 10001, 1, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-12 17:34:35.627' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000028, 1, N'100000028', N'Chanel Stockings', N'', 4, 1, CAST(12.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-12 18:54:20.587' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000029, 1, N'100000029', N'UNIQLO T-Shirts', N'', 10001, 1, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-12 19:01:51.793' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000030, 1, N'100000030', N'MBK T-Shirts', N'', 2, 1, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-09-12 19:02:59.677' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000035, 1, N'100000035', N'Vitamins', N'', 2, 2, CAST(0.0000 AS Decimal(18, 4)), CAST(777.0000 AS Decimal(18, 4)), N'Unit', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-11-12 02:23:54.310' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000036, 1, N'100000036', N'Jeans', N'', 1, 1, CAST(0.0000 AS Decimal(18, 4)), CAST(3333.0000 AS Decimal(18, 4)), N'Unit', 1, 0, CAST(N'2014-09-12 02:48:47.473' AS DateTime), CAST(N'2014-11-12 02:41:40.907' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000038, 1, N'100000038', N'X''Mas trousers', N'', 10001, 2, CAST(20.0000 AS Decimal(18, 4)), CAST(666.0000 AS Decimal(18, 4)), N'Unit', 1, 0, CAST(N'2014-12-09 07:46:34.547' AS DateTime), CAST(N'2014-12-09 07:46:34.547' AS DateTime), N'1', N'1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000039, 2, N'100000002', N'NIKE Avatar', N' ', 5, 3, CAST(9.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2015-03-25 00:30:04.607' AS DateTime), CAST(N'2015-03-25 00:30:04.607' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000040, 2, N'100000022', N'Levi''s collection', N'', 5, 3, CAST(20.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2015-03-25 06:58:16.953' AS DateTime), CAST(N'2015-03-25 06:58:16.953' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000041, 2, N'100000028', N'Chanel Stockings', N'', 5, 3, CAST(4.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2015-03-25 06:58:16.953' AS DateTime), CAST(N'2015-03-25 06:58:16.953' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemGroup] ([ItemGroupId], [BusinessId], [ItemGroupCode], [ItemGroupName], [ImagePath], [ItemCategoryId], [ItemBrandId], [Cost], [Price], [Uom], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (100000042, 2, N'100000025', N'Coach', N'', 5, 3, CAST(7.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), N'Piece', 1, 0, CAST(N'2015-03-25 07:27:57.203' AS DateTime), CAST(N'2015-03-25 07:27:57.203' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[ItemGroup] OFF
SET IDENTITY_INSERT [dbo].[ItemTransfer] ON 

INSERT [dbo].[ItemTransfer] ([TransferId], [TransferName], [SourceBusinessId], [TargetBusinessId], [Reference], [DateSent], [DateReceived], [TransferStatusTypeId], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (12, N'tttttt', 1, 2, N'', CAST(N'2015-03-25 00:00:00.000' AS DateTime), CAST(N'2015-03-25 06:58:16.927' AS DateTime), 3, 0, CAST(N'2015-03-25 06:53:11.943' AS DateTime), CAST(N'2015-03-25 06:58:16.953' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransfer] ([TransferId], [TransferName], [SourceBusinessId], [TargetBusinessId], [Reference], [DateSent], [DateReceived], [TransferStatusTypeId], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (13, N'Test3', 1, 2, N'', CAST(N'2015-03-25 00:00:00.000' AS DateTime), CAST(N'2015-03-25 07:04:45.200' AS DateTime), 3, 0, CAST(N'2015-03-25 07:04:20.100' AS DateTime), CAST(N'2015-03-25 07:04:47.360' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransfer] ([TransferId], [TransferName], [SourceBusinessId], [TargetBusinessId], [Reference], [DateSent], [DateReceived], [TransferStatusTypeId], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (14, N'SSSS1', 1, 2, N'', CAST(N'2015-03-25 00:00:00.000' AS DateTime), CAST(N'2015-03-25 07:06:55.800' AS DateTime), 3, 0, CAST(N'2015-03-25 07:06:35.630' AS DateTime), CAST(N'2015-03-25 07:06:58.333' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransfer] ([TransferId], [TransferName], [SourceBusinessId], [TargetBusinessId], [Reference], [DateSent], [DateReceived], [TransferStatusTypeId], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (15, N'TS-000001', 1, 2, N'', CAST(N'2015-03-25 00:00:00.000' AS DateTime), CAST(N'2015-03-25 07:27:57.203' AS DateTime), 3, 0, CAST(N'2015-03-25 07:26:39.567' AS DateTime), CAST(N'2015-03-25 07:27:57.203' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[ItemTransfer] OFF
SET IDENTITY_INSERT [dbo].[ItemTransferItem] ON 

INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 12, 3, N'100000002', N'NIKE Avatar', N'Hoolahoop', N'Piece', N' ', N'Home Appliance', N'Uniqlo', N'23456789', CAST(1.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 06:53:11.943' AS DateTime), CAST(N'2015-03-25 06:53:11.943' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (6, 12, 4, N'100000022', N'Levi''s collection', N'Smart Purse', N'Piece', N'', N'Home Appliance', N'Uniqlo', N'34567890', CAST(2.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 06:53:11.943' AS DateTime), CAST(N'2015-03-25 06:53:11.943' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (7, 12, 12, N'100000028', N'Chanel Stockings', N'Halloween stick', N'Piece', N'', N'Home Appliance', N'Uniqlo', N'', CAST(3.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 06:53:11.943' AS DateTime), CAST(N'2015-03-25 06:53:11.943' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (8, 13, 2, N'100000002', N'NIKE Avatar', N'Polo shirts', N'Piece', N' ', N'Home Appliance', N'Uniqlo', N'12345678', CAST(1.0000 AS Decimal(18, 4)), CAST(9.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 07:04:20.100' AS DateTime), CAST(N'2015-03-25 07:04:20.100' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (9, 13, 3, N'100000002', N'NIKE Avatar', N'Hoolahoop', N'Piece', N' ', N'Home Appliance', N'Uniqlo', N'23456789', CAST(2.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 07:04:20.100' AS DateTime), CAST(N'2015-03-25 07:04:20.100' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10, 14, 3, N'100000002', N'NIKE Avatar', N'Hoolahoop', N'Piece', N' ', N'Home Appliance', N'Uniqlo', N'23456789', CAST(5.0000 AS Decimal(18, 4)), CAST(5.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 07:06:35.630' AS DateTime), CAST(N'2015-03-25 07:06:35.630' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (11, 15, 4, N'100000022', N'Levi''s collection', N'Smart Purse', N'Piece', N'', N'Home Appliance', N'Uniqlo', N'34567890', CAST(1.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 07:26:39.567' AS DateTime), CAST(N'2015-03-25 07:26:39.567' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ItemTransferItem] ([TransferItemId], [TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (12, 15, 6, N'100000025', N'Coach', N'Pants', N'Piece', N'', N'Home Appliance', N'Uniqlo', N'', CAST(4.0000 AS Decimal(18, 4)), CAST(7.0000 AS Decimal(18, 4)), CAST(1.0000 AS Decimal(18, 4)), 0, 0, CAST(N'2015-03-25 07:26:39.567' AS DateTime), CAST(N'2015-03-25 07:26:39.567' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[ItemTransferItem] OFF
INSERT [dbo].[ItemTransferStatusType] ([TransferStatusTypeId], [TransferStatusTypeName], [IsForTransferItem], [IsForReceiveTransferItem]) VALUES (1, N'Transferring', 1, 1)
INSERT [dbo].[ItemTransferStatusType] ([TransferStatusTypeId], [TransferStatusTypeName], [IsForTransferItem], [IsForReceiveTransferItem]) VALUES (2, N'Canceled', 1, 0)
INSERT [dbo].[ItemTransferStatusType] ([TransferStatusTypeId], [TransferStatusTypeName], [IsForTransferItem], [IsForReceiveTransferItem]) VALUES (3, N'Received', 0, 1)
INSERT [dbo].[ItemTransferStatusType] ([TransferStatusTypeId], [TransferStatusTypeName], [IsForTransferItem], [IsForReceiveTransferItem]) VALUES (4, N'Rejected', 0, 1)
SET IDENTITY_INSERT [dbo].[OutgoingPayment] ON 

INSERT [dbo].[OutgoingPayment] ([OutgoingPaymentId], [BusinessId], [OutgoingPaymentNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Detail1], [Detail2], [Detail3], [Phone], [Date], [PaymentTypeId], [Note], [TaxIdInfo], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (31, 1, N'CP-0000022', 1, 13, N'', N'', N'', N'', N'', N'089-111-2345', CAST(N'2015-02-10' AS Date), 1, N'', N'', CAST(21.0000 AS Decimal(18, 4)), CAST(1.4700 AS Decimal(18, 4)), CAST(22.4700 AS Decimal(18, 4)), N'Done', 0, CAST(N'2015-02-10 07:59:36.200' AS DateTime), CAST(N'2015-02-10 07:59:36.200' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[OutgoingPayment] ([OutgoingPaymentId], [BusinessId], [OutgoingPaymentNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Detail1], [Detail2], [Detail3], [Phone], [Date], [PaymentTypeId], [Note], [TaxIdInfo], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (32, 1, N'PV-0000031', 0, 1, N'', N'', N'', N'', N'', N'02-62493333', CAST(N'2015-02-10' AS Date), 1, N'', N'', CAST(48.0000 AS Decimal(18, 4)), CAST(3.3600 AS Decimal(18, 4)), CAST(51.3600 AS Decimal(18, 4)), N'Done', 0, CAST(N'2015-02-10 08:00:55.193' AS DateTime), CAST(N'2015-02-10 08:00:55.193' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[OutgoingPayment] OFF
SET IDENTITY_INSERT [dbo].[OutgoingPaymentItem] ON 

INSERT [dbo].[OutgoingPaymentItem] ([OutgoingPaymentItemId], [OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [Amount], [Description], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (30, 31, 40, N'', CAST(22.4700 AS Decimal(18, 4)), N'', 0, CAST(N'2015-02-10 07:59:36.200' AS DateTime), CAST(N'2015-02-10 07:59:36.200' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[OutgoingPaymentItem] ([OutgoingPaymentItemId], [OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [Amount], [Description], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (31, 32, 41, N'KFC-SI-000', CAST(48.0000 AS Decimal(18, 4)), N'Paid already', 0, CAST(N'2015-02-10 08:00:55.193' AS DateTime), CAST(N'2015-02-10 08:00:55.193' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[OutgoingPaymentItem] OFF
INSERT [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (1, N'Cash')
INSERT [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (2, N'Cheque')
INSERT [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (3, N'Credit')
INSERT [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (4, N'Transfer')
SET IDENTITY_INSERT [dbo].[PurchaseOrder] ON 

INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (64, 1, N'', 1, N'CP-0000022', 13, N'', N'', N'', N'', N'', N'089-111-2345', CAST(N'2015-02-10' AS Date), CAST(N'2015-02-10' AS Date), 1, 1, N'', N'', CAST(21.0000 AS Decimal(18, 4)), CAST(1.4700 AS Decimal(18, 4)), CAST(22.4700 AS Decimal(18, 4)), N'Done', 0, CAST(N'2015-02-10 07:59:36.200' AS DateTime), CAST(N'2015-02-10 07:59:36.200' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (65, 1, N'PO-0000278', 0, N'', 1, N'', N'Mr.Suradef', N'67 Suthep Rd.', N'Mueng', N'Nakorn', N'02-62493333', CAST(N'2015-02-10' AS Date), CAST(N'2015-02-10' AS Date), 255, 1, N'', N'', CAST(48.0000 AS Decimal(18, 4)), CAST(3.3600 AS Decimal(18, 4)), CAST(51.3600 AS Decimal(18, 4)), N'Done', 0, CAST(N'2015-02-10 07:59:48.377' AS DateTime), CAST(N'2015-02-10 07:59:48.377' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (66, 1, N'PO-0000279', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:13.910' AS DateTime), CAST(N'2015-11-28 19:15:13.910' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (67, 1, N'PO-0000280', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:17.943' AS DateTime), CAST(N'2015-11-28 19:15:17.943' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (68, 1, N'PO-0000281', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:20.897' AS DateTime), CAST(N'2015-11-28 19:15:20.897' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (69, 1, N'PO-0000282', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:24.283' AS DateTime), CAST(N'2015-11-28 19:15:24.283' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (70, 1, N'PO-0000283', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:29.010' AS DateTime), CAST(N'2015-11-28 19:15:29.010' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (71, 1, N'PO-0000284', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:31.573' AS DateTime), CAST(N'2015-11-28 19:15:31.573' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (72, 1, N'PO-0000285', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:36.117' AS DateTime), CAST(N'2015-11-28 19:15:36.117' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (73, 1, N'PO-0000286', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:54.840' AS DateTime), CAST(N'2015-11-28 19:15:54.840' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (74, 1, N'PO-0000287', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:57.690' AS DateTime), CAST(N'2015-11-28 19:15:57.690' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (75, 1, N'PO-0000288', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:15:59.613' AS DateTime), CAST(N'2015-11-28 19:15:59.613' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (76, 1, N'PO-0000289', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:16:01.573' AS DateTime), CAST(N'2015-11-28 19:16:01.573' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (77, 1, N'PO-0000290', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:16:04.080' AS DateTime), CAST(N'2015-11-28 19:16:04.080' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (78, 1, N'PO-0000291', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:16:06.270' AS DateTime), CAST(N'2015-11-28 19:16:06.270' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (79, 1, N'PO-0000292', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:16:08.323' AS DateTime), CAST(N'2015-11-28 19:16:08.323' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (80, 1, N'PO-0000293', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:16:10.587' AS DateTime), CAST(N'2015-11-28 19:16:10.587' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (81, 1, N'PO-0000294', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:16:18.620' AS DateTime), CAST(N'2015-11-28 19:16:18.620' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (82, 1, N'PO-0000295', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:17:43.227' AS DateTime), CAST(N'2015-11-28 19:17:43.227' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrder] ([PurchaseOrderId], [BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (83, 1, N'PO-0000296', 0, N'', NULL, N'', N'', N'', N'', N'', N'', CAST(N'2015-11-28' AS Date), CAST(N'2015-11-28' AS Date), 255, 1, N'', N'', CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-11-28 19:17:45.760' AS DateTime), CAST(N'2015-11-28 19:17:45.760' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[PurchaseOrder] OFF
SET IDENTITY_INSERT [dbo].[PurchaseOrderItem] ON 

INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (47, 64, 5, CAST(7.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 07:59:36.200' AS DateTime), CAST(N'2015-02-10 07:59:36.200' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (48, 65, 3, CAST(8.0000 AS Decimal(18, 4)), CAST(8.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 07:59:48.377' AS DateTime), CAST(N'2015-02-10 07:59:48.377' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (49, 65, 10, CAST(8.0000 AS Decimal(18, 4)), CAST(8.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 07:59:48.377' AS DateTime), CAST(N'2015-02-10 07:59:48.377' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (50, 66, 10, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:13.910' AS DateTime), CAST(N'2015-11-28 19:15:13.910' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (51, 67, 12, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:17.943' AS DateTime), CAST(N'2015-11-28 19:15:17.943' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (52, 68, 13, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:20.897' AS DateTime), CAST(N'2015-11-28 19:15:20.897' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (53, 69, 20, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(45.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:24.283' AS DateTime), CAST(N'2015-11-28 19:15:24.283' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (54, 70, 13, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:29.010' AS DateTime), CAST(N'2015-11-28 19:15:29.010' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (55, 71, 13, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:31.573' AS DateTime), CAST(N'2015-11-28 19:15:31.573' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (56, 72, 12, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:36.117' AS DateTime), CAST(N'2015-11-28 19:15:36.117' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (57, 73, 3, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:54.840' AS DateTime), CAST(N'2015-11-28 19:15:54.840' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (58, 74, 10, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:57.690' AS DateTime), CAST(N'2015-11-28 19:15:57.690' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (59, 75, 12, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:15:59.613' AS DateTime), CAST(N'2015-11-28 19:15:59.613' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (60, 76, 12, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:16:01.573' AS DateTime), CAST(N'2015-11-28 19:16:01.573' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (61, 77, 14, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(34.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:16:04.080' AS DateTime), CAST(N'2015-11-28 19:16:04.080' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (62, 78, 14, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(34.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:16:06.270' AS DateTime), CAST(N'2015-11-28 19:16:06.270' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (63, 79, 4, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:16:08.323' AS DateTime), CAST(N'2015-11-28 19:16:08.323' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (64, 80, 12, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:16:10.587' AS DateTime), CAST(N'2015-11-28 19:16:10.587' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (65, 81, 4, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:16:18.620' AS DateTime), CAST(N'2015-11-28 19:16:18.620' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (66, 82, 3, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:17:43.227' AS DateTime), CAST(N'2015-11-28 19:17:43.227' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[PurchaseOrderItem] ([PurchaseOrderItemId], [PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (67, 83, 12, CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(4.0000 AS Decimal(18, 4)), 0, CAST(N'2015-11-28 19:17:45.760' AS DateTime), CAST(N'2015-11-28 19:17:45.760' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[PurchaseOrderItem] OFF
SET IDENTITY_INSERT [dbo].[ReceiveOrder] ON 

INSERT [dbo].[ReceiveOrder] ([ReceiveOrderId], [BusinessId], [ReceiveOrderNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (40, 1, N'CP-0000022', 1, 13, N'', N'', N'', N'', N'', N'089-111-2345', CAST(N'2015-02-10' AS Date), N'', N'', CAST(21.0000 AS Decimal(18, 4)), CAST(1.4700 AS Decimal(18, 4)), CAST(22.4700 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'Done', 0, CAST(N'2015-02-10 07:59:36.200' AS DateTime), CAST(N'2015-02-10 07:59:36.200' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ReceiveOrder] ([ReceiveOrderId], [BusinessId], [ReceiveOrderNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (41, 1, N'RO-0000039', 0, 1, N'', N'Mr.Suradef', N'67 Suthep Rd.', N'Mueng', N'', N'02-62493333', CAST(N'2015-02-10' AS Date), N'', N'', CAST(48.0000 AS Decimal(18, 4)), CAST(3.3600 AS Decimal(18, 4)), CAST(51.3600 AS Decimal(18, 4)), CAST(96.0000 AS Decimal(18, 4)), N'Done', 0, CAST(N'2015-02-10 08:00:25.637' AS DateTime), CAST(N'2015-02-10 08:00:25.637' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[ReceiveOrder] OFF
SET IDENTITY_INSERT [dbo].[ReceiveOrderItem] ON 

INSERT [dbo].[ReceiveOrderItem] ([ReceiveOrderItemId], [ReceiveOrderId], [PurchaseOrderItemId], [ItemId], [Quantity], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (65, 40, 47, 5, CAST(7.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 07:59:36.200' AS DateTime), CAST(N'2015-02-10 07:59:36.200' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ReceiveOrderItem] ([ReceiveOrderItemId], [ReceiveOrderId], [PurchaseOrderItemId], [ItemId], [Quantity], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (66, 41, 48, 3, CAST(8.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 08:00:25.637' AS DateTime), CAST(N'2015-02-10 08:00:25.637' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[ReceiveOrderItem] ([ReceiveOrderItemId], [ReceiveOrderId], [PurchaseOrderItemId], [ItemId], [Quantity], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (67, 41, 49, 10, CAST(8.0000 AS Decimal(18, 4)), CAST(3.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 08:00:25.637' AS DateTime), CAST(N'2015-02-10 08:00:25.637' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[ReceiveOrderItem] OFF
SET IDENTITY_INSERT [dbo].[SalesInvoice] ON 

INSERT [dbo].[SalesInvoice] ([SalesInvoiceId], [BusinessId], [SalesInvoiceNo], [IsCashSales], [CashSalesNo], [CustomerId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [DestinationTypeId], [PriceTypeId], [AmountTypeId], [PaymentTypeId], [Note], [SalesPersonId], [Subtotal], [Discount], [Charge], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (9, 1, N'', 1, N'CS-0000003', 2, N'', N'David Moyes', N'44', N'Suanluang Rd.', N'Huay-Kwang', N'089-4357222, 02-2223311', CAST(N'2015-02-10' AS Date), CAST(N'2015-02-10' AS Date), 1, 0, 1, 1, N'', 1, CAST(21.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(1.4700 AS Decimal(18, 4)), CAST(22.4700 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-02-10 08:01:14.257' AS DateTime), CAST(N'2015-02-10 08:01:14.257' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[SalesInvoice] ([SalesInvoiceId], [BusinessId], [SalesInvoiceNo], [IsCashSales], [CashSalesNo], [CustomerId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [DestinationTypeId], [PriceTypeId], [AmountTypeId], [PaymentTypeId], [Note], [SalesPersonId], [Subtotal], [Discount], [Charge], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10, 1, N'IV-0000004', 0, N'', 2, N'', N'David Moyes', N'44', N'Suanluang Rd.', N'Huay-Kwang', N'089-4357222, 02-2223311', CAST(N'2015-02-10' AS Date), CAST(N'2015-02-10' AS Date), 1, 0, 1, 1, N'', 1, CAST(32.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), CAST(2.2400 AS Decimal(18, 4)), CAST(34.2400 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'New', 0, CAST(N'2015-02-10 08:01:27.370' AS DateTime), CAST(N'2015-02-10 08:01:27.370' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[SalesInvoice] OFF
SET IDENTITY_INSERT [dbo].[SalesInvoiceItem] ON 

INSERT [dbo].[SalesInvoiceItem] ([SalesInvoiceItemId], [SalesInvoiceId], [ItemId], [Price], [Quantity], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (8, 9, 3, CAST(3.0000 AS Decimal(18, 4)), CAST(7.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 08:01:14.257' AS DateTime), CAST(N'2015-02-10 08:01:14.257' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[SalesInvoiceItem] ([SalesInvoiceItemId], [SalesInvoiceId], [ItemId], [Price], [Quantity], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (9, 10, 4, CAST(4.0000 AS Decimal(18, 4)), CAST(8.0000 AS Decimal(18, 4)), 0, CAST(N'2015-02-10 08:01:27.370' AS DateTime), CAST(N'2015-02-10 08:01:27.370' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[SalesInvoiceItem] OFF
INSERT [dbo].[SalesTaxType] ([SalesTaxTypeId], [SalesTaxTypeName]) VALUES (1, N'Include Tax')
INSERT [dbo].[SalesTaxType] ([SalesTaxTypeId], [SalesTaxTypeName]) VALUES (1, N'Include Tax')
SET IDENTITY_INSERT [dbo].[ScreenPermission] ON 

INSERT [dbo].[ScreenPermission] ([ScreenPermissionId], [BusinessId], [Admin], [Manager], [Stocking], [Purchase], [Sales], [DateCreated], [DateModified], [CreatedBy], [ModifiedBy]) VALUES (1, 1, N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,cs:a,siv:a,ip:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,cs:a,siv:a,ip:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', CAST(N'2014-12-02 00:00:00.000' AS DateTime), CAST(N'2015-09-13 23:56:00.000' AS DateTime), N'system', N'user_a1')
INSERT [dbo].[ScreenPermission] ([ScreenPermissionId], [BusinessId], [Admin], [Manager], [Stocking], [Purchase], [Sales], [DateCreated], [DateModified], [CreatedBy], [ModifiedBy]) VALUES (5, 2, N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', CAST(N'2014-02-01 00:00:00.000' AS DateTime), CAST(N'2014-02-01 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[ScreenPermission] ([ScreenPermissionId], [BusinessId], [Admin], [Manager], [Stocking], [Purchase], [Sales], [DateCreated], [DateModified], [CreatedBy], [ModifiedBy]) VALUES (6, 10005, N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', N'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a', CAST(N'2015-09-13 23:09:45.290' AS DateTime), CAST(N'2015-09-13 23:09:45.290' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[ScreenPermission] OFF
SET IDENTITY_INSERT [dbo].[Staff] ON 

INSERT [dbo].[Staff] ([StaffId], [BusinessId], [UserId], [Title], [Firstname], [Lastname], [Birthdate], [Gender], [Identification], [Phone], [Fax], [Email], [FacebookId], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Role], [Status], [Guid], [DateGuidExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (1, 1, 1, N'', N'Somchat', N'Kemchad', CAST(N'2015-09-01' AS Date), N'M', N'150393993059', N'089-234-1111', N'02-539-2345', N'Somchat@email.com', N'User.A1', N'30 Stok street', N'Test1', N'TestC', N'Mueang', NULL, N'10310', N'adm', N'active', N'', NULL, CAST(N'2014-06-10 08:43:21.237' AS DateTime), CAST(N'2015-09-18 00:58:31.173' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Staff] ([StaffId], [BusinessId], [UserId], [Title], [Firstname], [Lastname], [Birthdate], [Gender], [Identification], [Phone], [Fax], [Email], [FacebookId], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Role], [Status], [Guid], [DateGuidExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (2, 2, 1, N'MR', N'Kanok', N'Manote', CAST(N'2015-09-01' AS Date), N'M', N'150393993059', N'089-234-1111', N'02-539-2345', N'', N'FB.A', N'30 Stok street', N'Test1', N'TestC', N'Mueang', 1, N'', N'adm', N'active', N'', NULL, CAST(N'2014-06-01 00:00:00.000' AS DateTime), CAST(N'2015-09-17 00:09:36.303' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Staff] ([StaffId], [BusinessId], [UserId], [Title], [Firstname], [Lastname], [Birthdate], [Gender], [Identification], [Phone], [Fax], [Email], [FacebookId], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Role], [Status], [Guid], [DateGuidExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (3, 10005, 1, N'MR', N'Tephitak', N'Saran', CAST(N'2015-09-01' AS Date), N'M', N'150393993059', N'089-234-1111', N'02-539-2345', N'', N'Hello.World', N'30 Stok street', N'Test1', N'TestC', N'Mueang', 1, N'', N'adm', N'active', N'', NULL, CAST(N'2015-09-13 23:09:45.290' AS DateTime), CAST(N'2015-09-17 00:09:36.303' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Staff] ([StaffId], [BusinessId], [UserId], [Title], [Firstname], [Lastname], [Birthdate], [Gender], [Identification], [Phone], [Fax], [Email], [FacebookId], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Role], [Status], [Guid], [DateGuidExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 1, 4, N'', N'', N'', NULL, NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'mgr', N'active', N'', NULL, CAST(N'2015-09-01 00:00:00.000' AS DateTime), CAST(N'2015-09-18 00:58:10.247' AS DateTime), N'user_a1', N'user_a1')
INSERT [dbo].[Staff] ([StaffId], [BusinessId], [UserId], [Title], [Firstname], [Lastname], [Birthdate], [Gender], [Identification], [Phone], [Fax], [Email], [FacebookId], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Role], [Status], [Guid], [DateGuidExpired], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (9, 1, -1, NULL, N'', N'', NULL, NULL, N'', N'', N'', N'user2@email.com', N'', N'', N'', N'', N'', NULL, N'', N'sal', N'waiting', N'', NULL, CAST(N'2016-01-08 00:29:43.680' AS DateTime), CAST(N'2016-01-08 00:29:43.680' AS DateTime), N'user_a1', N'user_a1')
SET IDENTITY_INSERT [dbo].[Staff] OFF
SET IDENTITY_INSERT [dbo].[Supplier] ON 

INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (1, 1, N'KFC', N'', 2, N'56340012', N'Mr.Suradef', N'suradej@email.com', N'02-62493333', N'02-6432222', N'TermCredit', N'67 Suthep Rd.', N'Mueng', N'Nakorn', N'Chiang Mai', NULL, N'53000', N'Bill remark. this is a note', 0, CAST(N'2014-06-15 00:00:00.000' AS DateTime), CAST(N'2014-12-07 01:25:50.960' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (2, 1, N'ChestersGrill', N'', 1, N'10000321', N'K.Kingkaew', N'kk@cg.com', N'089-4332134', N'02-4562134', N'On Sunday ducks go swimming', N'99 Naratiwat Rd.', N'Samsen', N'Talingchan', N'Bangkok', NULL, N'10330', N'Test item', 0, CAST(N'2014-06-15 00:00:00.000' AS DateTime), CAST(N'2014-12-07 01:25:28.667' AS DateTime), N'user_a1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (5, 1, N'<default>', N' ', 1, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', 1, CAST(N'2014-06-15 00:00:00.000' AS DateTime), CAST(N'2014-12-06 23:17:24.960' AS DateTime), N'system', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (10, 1, N'ABC company', N'SUPPL007', 1, N'44455555', N'Mr. Ace', N'ace@email.com', N'+77 333 2212', N'+77 333 2214', N'No term at all', N'1 Street', N'City-A', N'District-B', N'Province-C', NULL, N'50000', N'no rm at all', 0, CAST(N'2014-11-11 00:00:00.000' AS DateTime), CAST(N'2014-11-11 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (12, 1, N'Anonymous', N'', 1, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', 0, CAST(N'2014-11-11 00:00:00.000' AS DateTime), CAST(N'2014-11-11 00:00:00.000' AS DateTime), N'system', N'system')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (13, 1, N'Minor Internation', N'', 1, N'123-3-444-5', N'คุณ เอกราช', N'eakarat@minor.co.th', N'089-111-2345', N'02-444-1568', N'Should not deliver it late', N'120/2 ถนนประชาอุทิศ', N'แขวงวังทองหลาง', N'เขตวังทองหลาง', N'กรุงเทพ', NULL, N'10301', N'Remark 2', 0, CAST(N'2014-12-06 19:48:49.243' AS DateTime), CAST(N'2014-12-06 19:48:49.243' AS DateTime), N'1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (14, 1, N'77777', N'', NULL, N'asdf', N'sadf', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', 1, CAST(N'2014-12-06 19:51:15.690' AS DateTime), CAST(N'2014-12-06 23:17:16.063' AS DateTime), N'1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (15, 1, N'hhhhh', N'', NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', 1, CAST(N'2014-12-06 22:46:19.250' AS DateTime), CAST(N'2014-12-06 23:17:13.700' AS DateTime), N'1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (16, 1, N'oooo', N'', NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', 1, CAST(N'2014-12-06 22:46:47.103' AS DateTime), CAST(N'2014-12-06 23:17:10.667' AS DateTime), N'1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (17, 1, N'test567', N'', NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', 1, CAST(N'2014-12-06 23:17:03.330' AS DateTime), CAST(N'2014-12-06 23:17:07.500' AS DateTime), N'1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (18, 1, N'Test 343434', N'', 2, N'sadf4', N'asdf2', N'df', N'asdf42', N'dsfasd', N'asdf3', N'asdf4', N'asdff', N'asdfq', N'adsfsdafsdf', NULL, N's', N'xt', 0, CAST(N'2014-12-07 23:48:22.377' AS DateTime), CAST(N'2014-12-07 23:48:22.377' AS DateTime), N'1', N'1')
INSERT [dbo].[Supplier] ([SupplierId], [BusinessId], [SupplierName], [SupplierNo], [SupplierGroupId], [TaxNo], [ContactPerson], [Email], [Phone], [Fax], [CreditTerm], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [Remark], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy]) VALUES (19, 1, N'adfdsf', N'', NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', 1, CAST(N'2014-12-07 23:49:29.120' AS DateTime), CAST(N'2014-12-07 23:49:34.793' AS DateTime), N'1', N'1')
SET IDENTITY_INSERT [dbo].[Supplier] OFF
SET IDENTITY_INSERT [dbo].[SupplierGroup] ON 

INSERT [dbo].[SupplierGroup] ([SupplierGroupId], [SupplierGroupName]) VALUES (1, N'Manufacturer')
INSERT [dbo].[SupplierGroup] ([SupplierGroupId], [SupplierGroupName]) VALUES (2, N'Agriculture')
SET IDENTITY_INSERT [dbo].[SupplierGroup] OFF
INSERT [dbo].[System_BusinessRole] ([BusinessRoleId], [BusinessRoleName], [BusinessRoleShortName]) VALUES (1, N'Admin', N'adm')
INSERT [dbo].[System_BusinessRole] ([BusinessRoleId], [BusinessRoleName], [BusinessRoleShortName]) VALUES (2, N'Manager', N'mgr')
INSERT [dbo].[System_BusinessRole] ([BusinessRoleId], [BusinessRoleName], [BusinessRoleShortName]) VALUES (3, N'Sales', N'sal')
INSERT [dbo].[System_BusinessRole] ([BusinessRoleId], [BusinessRoleName], [BusinessRoleShortName]) VALUES (4, N'Clerk', N'clk')
INSERT [dbo].[System_BusinessRole] ([BusinessRoleId], [BusinessRoleName], [BusinessRoleShortName]) VALUES (5, N'Purchase', N'pur')
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [Username], [Password], [Salt], [Email], [Guid], [DateGuidExpired], [IsDisabled], [IsFacebookUser], [DateCreated], [DateModified]) VALUES (1, N'user_a1', 0xD9618F03A4A3D27167AD8A87D95A771E52B22581269AAF02AEA481E35A95382D, 0x6F6F6F, N'user_a1@retailgoo.com', N'9d211c80-eb8d-4838-af16-f46d860bee63', CAST(N'2014-06-24 06:57:17.227' AS DateTime), 0, 0, CAST(N'2014-06-10 06:57:17.227' AS DateTime), CAST(N'2014-06-10 06:57:17.227' AS DateTime))
INSERT [dbo].[User] ([UserId], [Username], [Password], [Salt], [Email], [Guid], [DateGuidExpired], [IsDisabled], [IsFacebookUser], [DateCreated], [DateModified]) VALUES (3, N'user1@email.com', 0x429AD07256A322D2D88987A1E6511319200D723EA7D05F8E3C83B08EFB55F411, 0x0A37D0A3F015519858F4928804D0E13A, N'user1@email.com', N'dec880f3-9baf-47a1-88fa-9674122fd43b', CAST(N'2015-09-02 04:12:34.233' AS DateTime), 0, 0, CAST(N'2015-08-19 04:12:34.270' AS DateTime), CAST(N'2015-08-19 04:12:34.270' AS DateTime))
INSERT [dbo].[User] ([UserId], [Username], [Password], [Salt], [Email], [Guid], [DateGuidExpired], [IsDisabled], [IsFacebookUser], [DateCreated], [DateModified]) VALUES (4, N'mitsumete@hotmail.com', NULL, NULL, N'mitsumete@hotmail.com', N'', NULL, 0, 1, CAST(N'2015-08-19 04:36:33.347' AS DateTime), CAST(N'2015-08-19 04:36:33.347' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
INSERT [dbo].[UserProfile] ([UserId], [Title], [Firstname], [Lastname], [Birthdate], [Gender], [Identification], [Phone], [Fax], [FacebookId], [Address1], [Address2], [Address3], [City], [CountryId], [ZipCode], [DateCreated], [DateModified]) VALUES (1, N'Mr  ', N'Somchai', N'Kemkrad', NULL, NULL, N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', CAST(N'2015-09-01 00:00:00.000' AS DateTime), CAST(N'2015-09-01 00:00:00.000' AS DateTime))
/****** Object:  StoredProcedure [dbo].[spAmountTypeListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAmountTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select AmountTypeId AS Value, AmountTypeName AS Text
	from AmountType

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spBillItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBillItemAllRetrieve]
	@BillId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		bi.BillItemId, bi.BillId, bi.ItemId, bi.Quantity, bi.Discount, i.Price,
		dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName
	FROM [BillItem] bi
	INNER JOIN [Item] i
		ON bi.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	INNER JOIN [Bill] b
		ON bi.BillId = b.BillId
	WHERE	b.BillId = @BillId
		AND bi.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spBillManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBillManage]
	@BusinessId bigint,
	@CustomerId bigint,
	@BillId	bigint,
    @DiscountAmount decimal(18,4),
    @DiscountPercent decimal(18,4),
    @IsDiscountPercent bit,
	@Subtotal decimal(18,4),
    @Total decimal(18,4),
	@Tax decimal(18,4),
	@Note nvarchar(200),
	@Status varchar(10),
	@BillItems tBillItem readonly,
	@BillPaymentItems tBillPaymentItem readonly,
	@UserId bigint,
	@ItemState char(1),
	@Version datetime,
	@NewBillId bigint OUTPUT
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @Username varchar(50);
	DECLARE @billStatus VARCHAR(10);
		SET	@billStatus = @Status;

	DECLARE @targetId BIGINT;
		SET	@targetId = @BillId;

	exec @Username = GetUsername @UserId = @UserId;

	-- 1. Manage [Bill] table
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Bill]
			   ([BusinessId]
			   ,[CustomerId]
			   ,[DiscountAmount]
			   ,[DiscountPercent]
			   ,[IsDiscountPercent]
			   ,[Subtotal]
			   ,[Total]
			   ,[Tax]
			   ,[Status]
			   ,[Note]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
				(@BusinessId, @CustomerId, @DiscountAmount, @DiscountPercent, @IsDiscountPercent, @Subtotal, @Total, @Tax, @billStatus, @Note, 0, @dateNow, @dateNow, @Username, @Username);

			SET @targetId = SCOPE_IDENTITY();
			SELECT @NewBillId = @targetId;
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @billStatus = @Status;
			UPDATE [dbo].[Bill]
			   SET [CustomerId] = @CustomerId
				  ,[DiscountAmount] = @DiscountAmount
				  ,[DiscountPercent] = @DiscountPercent
				  ,[IsDiscountPercent] = @IsDiscountPercent
				  ,[Subtotal] = @Subtotal
				  ,[Total] = @Total
				  ,[Tax] = @Tax
				  ,[Status] = @billStatus
				  ,[Note] = @Note
				  ,[DateModified] = @dateNow
				  ,[UpdatedBy] = @dateNow
			 WHERE BillId = @BillId;
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Bill]
			SET    
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @Username
			WHERE  
				BillId = @BillId
		END


	-- 2. Manage BillItems
	---- Insert 
	INSERT INTO [dbo].[BillItem]
           ([BillId]
           ,[ItemId]
           ,[Quantity]
		   ,[Discount]
		   ,[Price]
           ,[IsDisabled])
	SELECT BillId = @targetId, ItemId, Quantity, Discount, Price, 0
		FROM @BillItems
		WHERE ItemState = 'N';

	---- Update 
	UPDATE bi
	SET bi.Quantity = i.Quantity, bi.Discount = i.Discount
		FROM [dbo].[BillItem] AS bi
		INNER JOIN @BillItems AS i
			ON bi.BillItemId = i.BillItemId 
		WHERE i.ItemState = 'M'

	---- Disable 
	UPDATE bi
	SET bi.IsDisabled = 1
		FROM [dbo].[BillItem] AS bi
		INNER JOIN @BillItems AS bi2
			ON bi.BillItemId = bi2.BillItemId 
		WHERE bi2.ItemState = 'D'

	-- 3. Manage BillPaymentItems
	---- Insert 
	INSERT INTO [dbo].[BillPaymentItem]
           ([BillId]
			,[PaymentTypeId]
			,[Amount]
			,[IsDisabled])
	SELECT BillId = @targetId, PaymentTypeId, Amount, 0
		FROM @BillPaymentItems
		WHERE ItemState = 'N';

	---- Update: There is no GUI to update BillPaymentItems

	---- Disable: 
	UPDATE bpi
	SET bpi.IsDisabled = 1
		FROM [dbo].[BillPaymentItem] AS bpi
		INNER JOIN @BillPaymentItems AS bpi2
			ON bpi.BillPaymentItemId = bpi2.BillPaymentItemId 
		WHERE bpi2.ItemState = 'D'

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;



GO
/****** Object:  StoredProcedure [dbo].[spBillPaymentItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBillPaymentItemAllRetrieve]
	@BillId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		bpi.*
	FROM BillPaymentItem bpi
	INNER JOIN Bill b
		ON bpi.BillId = b.BillId
	WHERE	bpi.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spBillRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBillRetrieve]
	@BillId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		b.*, c.CustomerName,
		CASE bpi_result.AmountReceived
			WHEN NULL THEN 0
			ELSE bpi_result.AmountReceived
		END AS AmountTendered
	FROM [Bill] b
	LEFT JOIN 
		( 
		SELECT bpi.BillId, SUM(bpi.Amount) As AmountReceived
		FROM [BillPaymentItem] bpi 
		WHERE bpi.BillId = @BillId
			AND bpi.IsDisabled <> 1
		GROUP BY bpi.BillId 
		) bpi_result
		ON b.BillId = bpi_result.BillId
	LEFT JOIN [Customer] c
		ON b.CustomerId = c.CustomerId
	WHERE	b.BillId = @BillId

SET @ReturnValue = 0;
RETURN @ReturnValue;







GO
/****** Object:  StoredProcedure [dbo].[spBusinessCreate]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBusinessCreate]
	@UserId bigint,
	@BusinessName nvarchar(100),
	@BusinessTypeId tinyint,
	--@UseTemplateBusiness bit,
	--@ImmediateBusinessId bigint,
	--@RootBusinessId bigint,
	@ImagePath varchar(MAX),
	@Address1 nvarchar(100),
	@Address2 nvarchar(100),
	@Address3 nvarchar(100),
	@City nvarchar(50),
	@CountryId tinyint,
	@ZipCode nvarchar(50),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@Email varchar(254),
	@Website nvarchar(MAX),
	@FacebookUrl nvarchar(MAX),
	@TaxIdInfo nvarchar(50),
	@DateExpired datetime
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @newBusinessId BIGINT;
		SET @newBusinessId = -1;
	
	declare @Username varchar(50);
	exec @Username = GetUsername @UserId = @UserId;

	declare @currencyId int;
		set @currencyId = 1;

	-- 1. Insert new business
	INSERT INTO [dbo].[Business]
           ([BusinessName]
           ,[BusinessTypeId]
           --,[UseTemplateBusiness]
           --,[ImmediateBusinessId]
           --,[RootBusinessId]
           ,[ImagePath]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[Phone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[FacebookUrl]
           ,[TaxIdInfo]
           ,[DateExpired]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@BusinessName
		   ,@BusinessTypeId
		   --,@UseTemplateBusiness
		   --,@ImmediateBusinessId
		   --,@RootBusinessId
		   ,@ImagePath
		   ,@Address1
		   ,@Address2
		   ,@Address3
		   ,@City
		   ,@CountryId
		   ,@ZipCode
		   ,@Phone
		   ,@Fax
		   ,@Email
		   ,@Website
		   ,@FacebookUrl
		   ,@TaxIdInfo
		   ,@DateExpired
		   ,@dateNow
		   ,@dateNow
		   ,@UserId
		   ,@UserId)
	SET @newBusinessId = SCOPE_IDENTITY();

	-- 2. Insert ScreenPermission for this business
	DECLARE @defaultPermission VARCHAR(MAX);
		SET @defaultPermission = 'cp:a,po:a,ro:a,op:a,ex:a,cs:a,siv:a,ip:a,im:a,cgm:a,bm:a,csh:a,cst:a,as:a,ts:a,rts:a,rs:a,ci:a,si:a,cam:a';
	INSERT INTO [dbo].[ScreenPermission]
           ([BusinessId]
           ,[Admin]
           ,[Manager]
           ,[Stocking]
           ,[Purchase]
           ,[Sales]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[ModifiedBy])
     VALUES
           (@newBusinessId
           ,@defaultPermission
           ,@defaultPermission
           ,@defaultPermission
           ,@defaultPermission
           ,@defaultPermission
           ,@dateNow
           ,@dateNow
           ,@Username
           ,@Username)

	-- 3. Insert default DocSettings
	INSERT INTO [dbo].[DocSetting]
           ([BusinessId]
           ,[SalesOrderFormat]
           ,[SalesOrderNo]
           ,[SalesOrderTemplate]
           ,[InvoiceFormat]
           ,[InvoiceNo]
           ,[InvoiceTemplate]
           ,[ReceiptFormat]
           ,[ReceiptNo]
           ,[ReceiptTemplate]
           ,[StockAdjFormat]
           ,[StockAdjNo]
           ,[StockAdjTemplate]
           ,[ReceiveOrderFormat]
           ,[ReceiveOrderNo]
           ,[ReceiveOrderTemplate]
           ,[CashPurchaseFormat]
           ,[CashPurchaseNo]
           ,[CashPurchaseTemplate]
           ,[CashSalesFormat]
           ,[CashSalesNo]
           ,[CashSalesTemplate]
           ,[PurchaseOrderFormat]
           ,[PurchaseOrderNo]
           ,[PurchaseOrderTemplate]
           ,[PaymentFormat]
           ,[PaymentNo]
           ,[PaymentTemplate]
           ,[ExpenseFormat]
           ,[ExpenseNo]
           ,[ExpenseTemplate]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@newBusinessId
		   ,'SO-{XXXXXX}'
		   ,0
		   ,0
		   ,'IV-{XXXXXX}'
		   ,0
		   ,0
		   ,'RV-{XXXXXX}'
		   ,0
		   ,0
		   ,'SA-{XXXXXX}'
		   ,0
		   ,0
		   ,'RO-{XXXXXX}'
		   ,0
		   ,0
		   ,'CP-{XXXXXX}'
		   ,0
		   ,0
		   ,'CS-{XXXXXX}'
		   ,0
		   ,0
		   ,'PO-{XXXXXX}'
		   ,0
		   ,0
		   ,'PV-{XXXXXX}'
		   ,0
		   ,0
		   ,'EX-{XXXXXX}'
		   ,0
		   ,0
		   ,@dateNow
		   ,@dateNow
		   ,@Username
		   ,@Username
		   )

	-- 4. Insert Financial Settings
	INSERT INTO [dbo].[FinancialSetting]
           ([BusinessId]
           ,[CurrencyId]
           ,[DefaultPriceTypeId]
           ,[UseServiceCharge]
           ,[ServiceChargeAmount]
           ,[SalesTaxTypeId]
           ,[SalesTaxAmount]
           ,[UseStockItemControl]
           ,[CostingTypeId]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@newBusinessId, @currencyId, 1, 1, 10.00, 1, 7.00, 0, 1, @dateNow, @dateNow, @Username, @Username )

	-- 5. Insert the current user to the first busienss staff list
	INSERT INTO [dbo].[Staff]
           ([BusinessId]
           ,[UserId]
           ,[Title]
           ,[Firstname]
           ,[Lastname]
           ,[Birthdate]
           ,[Gender]
           ,[Identification]
           ,[Phone]
           ,[Fax]
		   ,[Email]
           ,[FacebookId]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[Role]
           ,[Status]
           ,[Guid]
           ,[DateGuidExpired]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@newBusinessId, @UserId, NULL, '', '', NULL, NULL, '', '', '', '', ''
		   , '', '', '', '', NULL, '', 'admin', 'active', '', NULL, @dateNow, @dateNow, @Username, @Username
		   )

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;





GO
/****** Object:  StoredProcedure [dbo].[spBusinessInfoManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBusinessInfoManage]
    @BusinessId bigint,
	@BusinessName nvarchar(100),
	@BusinessTypeId tinyint,
	--@UseTemplateBusiness bit,
	--@ImmediateBusinessId bigint,
	--@RootBusinssId bigint,
    @ImagePath varchar(max),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @City nvarchar(50),
    @CountryId tinyint,
    @ZipCode nvarchar(50),
    @Phone nvarchar(50),
    @Fax nvarchar(50),
    @Email varchar(320),
    @Website nvarchar(max),
    @FacebookUrl nvarchar(max),
    @TaxIdInfo varchar(50),
    @DateExpired datetime,
	@ItemState char(1),
	@UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N' BEGIN
		INSERT INTO [dbo].[Business]
           ([BusinessName]
           ,[BusinessTypeId]
           ,[ImagePath]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[Phone]
           ,[Fax]
           ,[Email]
           ,[Website]
           ,[FacebookUrl]
           ,[TaxIdInfo]
           ,[DateExpired]
           ,[DateCreated]
           ,[DateModified]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (@BusinessName
           ,@BusinessTypeId
           ,@ImagePath
           ,@Address1
           ,@Address2
           ,@Address3
           ,@City
           ,@CountryId
           ,@ZipCode
           ,@Phone
           ,@Fax
           ,@Email
           ,@Website
           ,@FacebookUrl
           ,@TaxIdInfo
           ,@DateExpired
           ,@dateNow
           ,@dateNow
           ,@username
           ,@username)

	END ELSE IF @ItemState = 'M' BEGIN
		
		UPDATE [dbo].[Business]
		   SET [BusinessName] = @BusinessName
			  ,[BusinessTypeId] = @BusinessTypeId
			  ,[ImagePath] = @ImagePath
			  ,[Address1] = @Address1
			  ,[Address2] = @Address2
			  ,[Address3] = @Address3
			  ,[City] = @City
			  ,[CountryId] = @CountryId
			  ,[ZipCode] = @ZipCode
			  ,[Phone] = @Phone
			  ,[Fax] = @Fax
			  ,[Email] = @Email
			  ,[Website] = @Website
			  ,[FacebookUrl] = @FacebookUrl
			  ,[TaxIdInfo] = @TaxIdInfo
			  ,[DateExpired] = @DateExpired
			  ,[DateModified] = @dateNow
			  ,[UpdatedBy] = @UserId
		 WHERE [BusinessId] = @BusinessId;

	END ELSE IF @ItemState = 'D' BEGIN
		DECLARE @temp varchar; -- Do Nothing as of now
	END

SET @ReturnValue = 0;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spBusinessInfoRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBusinessInfoRetrieve]
    @BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT b.*, btype.BusinessTypeName, c.CountryName, c.CountryShortName
	FROM Business b
	INNER JOIN BusinessType btype 
		ON b.BusinessTypeId = btype.BusinessTypeId
	LEFT JOIN Country c
		ON b.CountryId = c.CountryId
	WHERE
		b.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spBusinessRoleListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBusinessRoleListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		sbr.BusinessRoleId AS Value,
		sbr.BusinessRoleName AS Text
	FROM [System_BusinessRole] sbr

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
/****** Object:  StoredProcedure [dbo].[spBusinessStaffAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBusinessStaffAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT *
	FROM [Staff] stf
	INNER JOIN [System_BusinessRole] sys_role
		ON stf.Role = sys_role.BusinessRoleShortName
	--INNER JOIN dbo.[User] usr
	--	ON stf.UserId = usr.UserId
	WHERE	stf.BusinessId = @BusinessId
	ORDER BY stf.DateModified DESC;

	
SET @ReturnValue = @@ROWCOUNT;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spBusinessStaffManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBusinessStaffManage]
	@BusinessId bigint,
	@StaffUserId bigint,
	@StaffId bigint,
	@Email varchar(254),
    @Role varchar(10),
	@Status varchar(10),
	@UserId bigint,
	@ItemState varchar(1)
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
	   EXEC @username = GetUsername @UserId = @UserId;
	
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Staff]
				   ([BusinessId]
				   ,[UserId]
				   ,[Title]
				   ,[Firstname]
				   ,[Lastname]
				   ,[Birthdate]
				   ,[Gender]
				   ,[Identification]
				   ,[Phone]
				   ,[Fax]
				   ,[Email]
				   ,[FacebookId]
				   ,[Address1]
				   ,[Address2]
				   ,[Address3]
				   ,[City]
				   ,[CountryId]
				   ,[ZipCode]
				   ,[Role]
				   ,[Status]
				   ,[Guid]
				   ,[DateGuidExpired]
				   ,[DateCreated]
				   ,[DateModified]
				   ,[CreatedBy]
				   ,[UpdatedBy])
			 VALUES
				   (@BusinessId, @StaffUserId, NULL, '', '', NULL, NULL, '', '', '', @Email
				   ,'', '', '', '', '', NULL, '', @Role, @Status, '', NULL, @dateNow, @dateNow, @username, @username );

			-- Create a user for this staff but mark this user as pending. This is to wait for the user register with retailgoo system first.
			DECLARE @newUserId BIGINT;
			exec @newUserId = dbo.spUserCreate 
				@Username = @Email,
				@Password = NULL,
				@Salt = NULL,
				@Email = @Email,
				@Guid = '',
				@DateGuidExpired = NULL,
				@IsDisabled = 0,
				@IsFacebookUser = 0;

			-- Mark this newly created user as pending
			INSERT INTO [dbo].[UserPending]
				   ([UserId]
				   ,[DateCreated])
			 VALUES
				   (@newUserId, @dateNow);

		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET [Status] = @Status
				  ,[Role] = @Role
				  ,[DateModified] = @dateNow
			 WHERE StaffId = @StaffId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET [Status] = 'inactive'
			 WHERE StaffId = @StaffId;
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;


GO
/****** Object:  StoredProcedure [dbo].[spBusinessTypeListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBusinessTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select b.BusinessTypeId AS Value, b.BusinessTypeName AS Text
	from BusinessType b

SET @ReturnValue = 0;
RETURN @ReturnValue;




GO
/****** Object:  StoredProcedure [dbo].[spCashPurchaseAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCashPurchaseAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		po.*,
		s.SupplierName
	FROM [PurchaseOrder] po
	LEFT JOIN [Supplier] s
		ON po.SupplierId = s.SupplierId
	WHERE	po.BusinessId = @BusinessId
		AND po.IsCashPurchase = 1
		AND po.IsDisabled <> 1
	ORDER BY po.PurchaseOrderId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spCashPurchaseVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCashPurchaseVoid]
    @PurchaseOrderId BIGINT,
	@UserId BIGINT
AS
	DECLARE @ReturnValue INT;
	DECLARE @status VARCHAR(10);
		SET @status = 'Void';
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	SET NOCOUNT ON;

	-- 1. VOID PO, RO, and OP
	UPDATE [dbo].[PurchaseOrder]
	SET Status = @status, DateModified = @dateNow, UpdatedBy = @username
	WHERE	PurchaseOrderId = @PurchaseOrderId;

	UPDATE [dbo].[ReceiveOrder]
	SET Status = @status, DateModified = @dateNow, UpdatedBy = @username
	FROM [ReceiveOrder] 
	INNER JOIN 
	(
		SELECT ro.* 
		FROM [ReceiveOrder] ro
		INNER JOIN [ReceiveOrderItem] roi 
			ON ro.ReceiveOrderId = roi.ReceiveOrderId
		INNER JOIN [PurchaseOrderItem] poi
			ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
		WHERE poi.PurchaseOrderId = @PurchaseOrderId
	) ref1
	ON ref1.ReceiveOrderId = [ReceiveOrder].ReceiveOrderId

	UPDATE [dbo].[OutgoingPayment]
	SET Status = @status, DateModified = @dateNow, UpdatedBy = @username
	FROM [OutgoingPayment]
	INNER JOIN 
	(
		SELECT op.* FROM [OutgoingPayment] op
		INNER JOIN [OutgoingPaymentItem] opi
			ON op.OutgoingPaymentId = opi.OutgoingPaymentId
		INNER JOIN [ReceiveOrder] ro
			ON opi.ReceiveOrderId = ro.ReceiveOrderId
		INNER JOIN [ReceiveOrderItem] roi 
			ON ro.ReceiveOrderId = roi.ReceiveOrderId
		INNER JOIN [PurchaseOrderItem] poi
			ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
		WHERE poi.PurchaseOrderId = @PurchaseOrderId
	) ref2
	ON ref2.OutgoingPaymentId = [OutgoingPayment].OutgoingPaymentId

	-- 2. Update Item.Quantity to reflect SubItems of this void'ed CashPurchase item
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity - roi.Quantity
	FROM [ReceiveOrderItem] roi
	INNER JOIN [PurchaseOrderItem] po
		ON	roi.PurchaseOrderItemId = po.PurchaseOrderItemId
	WHERE	po.PurchaseOrderId = @PurchaseOrderId;

	
SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spCashSalesAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCashSalesAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT SI.*, CM.CustomerName
	FROM [SalesInvoice] SI
	LEFT JOIN [Customer] CM
		ON SI.CustomerId = CM.CustomerId
	WHERE SI.BusinessId = @BusinessId
		AND SI.IsDisabled <> 1
		AND SI.IsCashSales = 1
	ORDER BY SI.SalesInvoiceId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spCashSalesItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCashSalesItemAllRetrieve]
    @SalesInvoiceId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT siItem.*, dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName, i.Price, si.Discount, si.SalesInvoiceId
	FROM [dbo].[SalesInvoiceItem] siItem
		, [dbo].[SalesInvoice] si
		, [dbo].[Item] i
		INNER JOIN [dbo].[ItemGroup] ig
		    ON i.ItemGroupId = ig.ItemGroupId
	WHERE siItem.SalesInvoiceId = @SalesInvoiceId
	   AND siItem.ItemId = i.ItemId
	   AND siItem.SalesInvoiceId = si.SalesInvoiceId
	   AND siItem.IsDisabled <> 1
	
SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spCashSalesVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCashSalesVoid]
	@SalesInvoiceId BIGINT,
	@UserId BIGINT
AS

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @IncomingPaymentId BIGINT;
		SET @IncomingPaymentId = -1;

	-- Search and void IncomingPayment, first
	SELECT TOP 1 @IncomingPaymentId = IncomingPaymentId
	FROM [IncomingPaymentItem]
	WHERE SalesInvoiceId = @SalesInvoiceId;

	IF @IncomingPaymentId <> -1
		BEGIN
			UPDATE [dbo].[IncomingPayment]
			SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
			WHERE IncomingPaymentId = @IncomingPaymentId;
		END

	-- Void SalesInvoice
	UPDATE [dbo].[SalesInvoice]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE SalesInvoiceId = @SalesInvoiceId;

	-- Update Item.Quantity due to void'ed SalesInvoice
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + sii.Quantity
	FROM [SalesInvoiceItem] sii
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @SalesInvoiceId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spCostingTypeListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCostingTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		CostingTypeId AS Value, 
		CostingTypeName As Text 
	from 
		CostingType

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spCountryListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCountryListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select c.CountryId as Value, c.CountryName as Text
	from Country c
	--select c.CountryShortName as Value, c.CountryName as Text
	--from Country c

SET @ReturnValue = 0;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spCurrencyListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCurrencyListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		CurrencyId AS Value, 
		CurrencyName As Text 
	from 
		Currency

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spCurrentStockRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCurrentStockRetrieve]
    @BusinessId bigint,
	@ItemGroupId bigint,
	@ItemBrandId bigint,
	@ItemCategoryId bigint
AS
	DECLARE @ReturnValue INT;
	DECLARE @sqlStr AS NVARCHAR(MAX);

	SET @sqlStr = 'SELECT ig.ItemGroupCode, ic.ItemCategoryName, ib.ItemBrandName, SUM(i.Quantity) AS Quantity';
	--SET @sqlStr = 'SELECT *, fnGetItemFullName( ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName, ig.ItemGroupName, ic.ItemCategoryName FROM [Item] i';
	SET @sqlStr = @sqlStr + ' FROM [ItemGroup] ig';
	SET @sqlStr = @sqlStr + ' INNER JOIN [Item] i ON ig.ItemGroupId = i.ItemGroupId';
	SET @sqlStr = @sqlStr + ' LEFT JOIN [ItemCategory] ic on ig.ItemCategoryId = ic.ItemCategoryId';
	SET @sqlStr = @sqlStr + ' LEFT JOIN [ItemBrand] ib on ig.ItemGroupId = ib.ItemBrandId';
	--SET @sqlStr = @sqlStr + ' WHERE ig.BusinessId = @BusinessId';
	SET @sqlStr = @sqlStr + ' WHERE ig.BusinessId = ' + CAST(@BusinessId AS NVARCHAR);

	IF (@ItemGroupId IS NOT NULL) BEGIN
		SET @sqlStr = @sqlStr + ' AND ig.ItemGroupId = ' + CAST(@ItemGroupId AS NVARCHAR);
	END

	IF (@ItemBrandId IS NOT NULL) BEGIN
		SET @sqlStr = @sqlStr + ' AND ib.ItemBrandId = ' + CAST(@ItemBrandId AS NVARCHAR);
	END

	IF (@ItemCategoryId IS NOT NULL) BEGIN
		SET @sqlStr = @sqlStr + ' AND ic.ItemCategoryId = ' + CAST(@ItemCategoryId AS NVARCHAR);
	END

	SET @sqlStr = @sqlStr + ' GROUP BY ig.ItemGroupCode, ic.ItemCategoryName, ib.ItemBrandName';

	EXECUTE sp_ExecuteSQL @sqlStr

SET @ReturnValue = 0;
RETURN @ReturnValue;




GO
/****** Object:  StoredProcedure [dbo].[spCustomerAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomerAllRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.CustomerId, s.CustomerName, s.CustomerNo, s.ContactPerson, s.Phone, sg.CustomerGroupName
	from Customer s
	left join CustomerGroup sg
		on s.CustomerGroupId = sg.CustomerGroupId
	where 
			s.BusinessId = @BusinessId
		and	s.IsDisabled = 0

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spCustomerGroupListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomerGroupListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select sg.CustomerGroupId AS Value, sg.CustomerGroupName AS Text
	from CustomerGroup sg

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spCustomerListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomerListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT s.CustomerId AS Value, s.CustomerName AS Text
	FROM [dbo].[Customer] s
	WHERE
		s.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spCustomerManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomerManage]
	@BusinessId bigint,
	@CustomerId bigint,
	@CustomerName nvarchar(50),
	@CustomerNo nvarchar(50),
	@CustomerGroupId tinyint,
	@DefaultPriceTypeId tinyint,
	@TaxNo nvarchar(50),
	@ContactPerson nvarchar(100),
	@Email varchar(320),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@CreditTerm nvarchar(max),
	@CreditLimit nvarchar(max),
	@AddressBilling1 nvarchar(100),
	@AddressBilling2 nvarchar(100),
	@AddressBilling3 nvarchar(100),
	@CityBilling nvarchar(50),
	@CountryBillingId tinyint,
	@ZipCodeBilling nvarchar(50),
	@RemarkBilling nvarchar(max),
	@AddressShipping1 nvarchar(100),
	@AddressShipping2 nvarchar(100),
	@AddressShipping3 nvarchar(100),
	@CityShipping nvarchar(50),
	@CountryShippingId tinyint,
	@ZipCodeShipping nvarchar(50),
	@RemarkShipping nvarchar(max),
	@UserId bigint,
	@ItemState char(1),
	@Version datetime
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @Username varchar(50);
	
	exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Customer]
			   ([BusinessId]
			   ,[CustomerName]
			   ,[CustomerNo]
			   ,[CustomerGroupId]
			   ,[TaxNo]
			   ,[ContactPerson]
			   ,[Email]
			   ,[Phone]
			   ,[Fax]
			   ,[CreditTerm]
			   ,[CreditLimit]
			   ,[DefaultPriceTypeId]
			   ,[AddressBilling1]
			   ,[AddressBilling2]
			   ,[AddressBilling3]
			   ,[CityBilling]
			   ,[CountryBillingId]
			   ,[ZipCodeBilling]
			   ,[RemarkBilling]
			   ,[AddressShipping1]
			   ,[AddressShipping2]
			   ,[AddressShipping3]
			   ,[CityShipping]
			   ,[CountryShippingId]
			   ,[ZipCodeShipping]
			   ,[RemarkShipping]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
				(@BusinessId, @CustomerName, @CustomerNo, @CustomerGroupId, @TaxNo, @ContactPerson, @Email, @Phone, @Fax, @CreditTerm, @CreditLimit, @DefaultPriceTypeId,
				@AddressBilling1, @AddressBilling2, @AddressBilling3, @CityBilling, @CountryBillingId, @ZipCodeBilling, @RemarkBilling, 
				@AddressShipping1, @AddressShipping2, @AddressShipping3, @CityShipping, @CountryShippingId, @ZipCodeShipping, @RemarkShipping, 
				0, @dateNow, @dateNow, @Username, @Username);
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Customer]
			SET   
				CustomerName = @CustomerName, CustomerNo = @CustomerNo, CustomerGroupId = @CustomerGroupId, TaxNo = @TaxNo, ContactPerson = @ContactPerson, 
				Email = @Email, Phone = @Phone, Fax = @Fax, CreditTerm = @CreditTerm, CreditLimit = @CreditLimit, DefaultPriceTypeId = @DefaultPriceTypeId,
				AddressBilling1 = @AddressBilling1, AddressBilling2 = @AddressBilling2, AddressBilling3 = @AddressBilling3, CityBilling = @CityBilling, 
				CountryBillingId = @CountryBillingId, ZipCodeBilling = @ZipCodeBilling, RemarkBilling = @RemarkBilling, 
				AddressShipping1 = @AddressShipping1, AddressShipping2 = @AddressShipping2, AddressShipping3 = @AddressShipping3, CityShipping = @CityShipping, 
				CountryShippingId = @CountryShippingId, ZipCodeShipping = @ZipCodeShipping, RemarkShipping = @RemarkShipping, 
				IsDisabled = 0, DateModified = @dateNow, UpdatedBy = @Username
			WHERE 
				CustomerId = @CustomerId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Customer]
			SET    
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @Username
			WHERE  
				CustomerId = @CustomerId
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;













GO
/****** Object:  StoredProcedure [dbo].[spCustomerRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCustomerRetrieve]
    @CustomerId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.*, sg.CustomerGroupName, c.CountryName AS CountryBillingName, c2.CountryName AS CountryShippingName
	from Customer s
	left join Country c
		on s.CountryBillingId = c.CountryId
	left join Country c2
	on s.CountryShippingId = c2.CountryId
	left join CustomerGroup sg
		on s.CustomerGroupId = sg.CustomerGroupId
	where 
		s.CustomerId = @CustomerId

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spDefaultPriceTypeListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDefaultPriceTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select d.DefaultPriceTypeId AS Value, d.DefaultPriceTypeName AS Text
	from DefaultPriceType d

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spDestinationTypeListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDestinationTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT
		dt.DestinationTypeId AS VALUE,
		dt.DestinationTypeName AS TEXT
	FROM [DestinationType] dt;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spDocSettingManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDocSettingManage]
	@DocSettingId bigint,
	@BusinessId bigint,
	@SalesOrderFormat nvarchar(50),
	@SalesOrderNo int,
	@SalesOrderTemplate tinyint,
	@InvoiceFormat nvarchar(50),
	@InvoiceNo int,
	@InvoiceTemplate tinyint,
	@ReceiptFormat nvarchar(50),
	@ReceiptNo int,
	@ReceiptTemplate tinyint,
	@StockAdjFormat nvarchar(50),
	@StockAdjNo int,
	@StockAdjTemplate tinyint,
	@ReceiveOrderFormat nvarchar(50),
	@ReceiveOrderNo int,
	@ReceiveOrderTemplate tinyint,
	@CashPurchaseFormat nvarchar(50),
	@CashPurchaseNo int,
	@CashPurchaseTemplate tinyint,
	@CashSalesFormat nvarchar(50),
	@CashSalesNo int,
	@CashSalesTemplate tinyint,
	@PurchaseOrderFormat nvarchar(50),
	@PurchaseOrderNo int,
	@PurchaseOrderTemplate tinyint,
	@PaymentFormat nvarchar(50),
	@PaymentNo int,
	@PaymentTemplate tinyint,
	@ExpenseFormat nvarchar(50),
	@ExpenseNo int,
	@ExpenseTemplate tinyint,
    @UserId bigint,
	@Version datetime,
	@ItemState char(1)
AS 
/*
BEGIN TRY
BEGIN TRANSACTION
*/
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[DocSetting]
			SET
			[SalesOrderFormat] = @SalesOrderFormat, [SalesOrderNo] = @SalesOrderNo, [SalesOrderTemplate] = @SalesOrderTemplate,
			[InvoiceFormat] = @InvoiceFormat, [InvoiceNo] = @InvoiceNo, [InvoiceTemplate] = @InvoiceTemplate,
			[ReceiptFormat] = @ReceiptFormat, [ReceiptNo] = @ReceiptNo, [ReceiptTemplate] = @ReceiptTemplate,
			[StockAdjFormat] = @StockAdjFormat, [StockAdjNo] = @StockAdjNo, [StockAdjTemplate] = @StockAdjTemplate,
			[ReceiveOrderFormat] = @ReceiveOrderFormat, [ReceiveOrderNo] = @ReceiveOrderNo, [ReceiveOrderTemplate] = @ReceiveOrderTemplate,
			[CashPurchaseFormat] = @CashPurchaseFormat, [CashPurchaseNo] = @CashPurchaseNo, [CashPurchaseTemplate] = @CashPurchaseTemplate,
			[CashSalesFormat] = @CashSalesFormat, [CashSalesNo] = @CashSalesNo, [CashSalesTemplate] = @CashSalesTemplate,
			[PurchaseOrderFormat] = @PurchaseOrderFormat, [PurchaseOrderNo] = @PurchaseOrderNo, [PurchaseOrderTemplate] = @PurchaseOrderTemplate,
			[PaymentFormat] = @PaymentFormat, [PaymentNo] = @PaymentNo, [PaymentTemplate] = @PaymentTemplate,
			[ExpenseFormat] = @ExpenseFormat, [ExpenseNo] = @ExpenseNo, [ExpenseTemplate] = @ExpenseTemplate,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[DocSettingId] = @DocSettingId
		END

/*COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;
*/







GO
/****** Object:  StoredProcedure [dbo].[spDocSettingRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDocSettingRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1 * 
	FROM [DocSetting]
	WHERE BusinessId = @BusinessId

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spExpenseAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spExpenseAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT e.*, s.SupplierName, ec.ExpenseCategoryName
	FROM [Expense] e
	INNER JOIN [ExpenseCategory] ec
		ON e.ExpenseCategoryId = ec.ExpenseCategoryId
	INNER JOIN [Supplier] s 
		ON e.SupplierId = s.SupplierId
	WHERE e.BusinessId = @BusinessId
		AND e.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spExpenseCategoryListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spExpenseCategoryListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT
		ExpenseCategoryId AS Value,
		ExpenseCategoryName As Text
	FROM [ExpenseCategory]
	WHERE BusinessId = @BusinessId
		AND IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spExpenseManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spExpenseManage]
	@BusinessId bigint,
	@ExpenseId bigint,
	@ExpenseNo nvarchar(100),
	@SupplierId bigint,
    @ExpenseCategoryId bigint,
    @Amount decimal(18,4),
	@Date date,
	@Note nvarchar(max),
	@UseWht bit,
	@WhtPercent decimal(18,4),
	@WhtAmount decimal(18,4),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1)
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @insertedItemId BIGINT;
	SET @insertedItemId = @ExpenseId;

	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextItemName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Expense', @RetDocName = @nextItemName OUT;

			-- Insert 
			INSERT INTO [dbo].[Expense] ([BusinessId], [ExpenseNo], [Amount], [UseWht], [WhtPercent], [WhtAmount], [SupplierId], [ExpenseCategoryId], [Note], [Date], [IsVoided], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextItemName, @Amount, @UseWht, @WhtPercent, @WhtAmount, @SupplierId, @ExpenseCategoryId, @Note, @Date, 0, 0, @dateNow, @dateNow, @username, @username
	
			SET @insertedItemId = SCOPE_IDENTITY();
		END
	--ELSE IF @ItemState = 'M'
	--	BEGIN
	--		UPDATE [dbo].[OutgoingPayment]
	--		SET    
	--		[PayeeId] = @PayeeId, [Reference] = @Reference, [Contact] = @Contact, [Detail1] = @Detail1, [Detail2] = @Detail2, 
	--		[Detail3] = @Detail3, [Phone] = @Phone, [Date] = @Date, [PaymentMethod] = @PaymentMethod, [Note] = @Note, 
	--		[TaxId] = @TaxId, [Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total, [Status] = @Status, [IsVoided] = @IsVoided, 
	--		[IsDeleted] = @IsDeleted, [DateModified] = @dateNow, [UpdatedBy] = @User
	--		WHERE  
	--		[OutgoingPaymentId] = @OutgoingPaymentId
	--	END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Expense]
			SET    
			[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[ExpenseId] = @ExpenseId
		END

	---- Manage subItems
	------ Insert Subitems
	--INSERT INTO [dbo].[OutgoingPaymentItem] 
	--	([BusinessId], [OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [AmountPay], [Description], [IsDeleted], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	--SELECT 
	--	@BusinessId, @insertedItemId, ReceiveOrderId, SupplierInvoice, AmountPay, Description, IsDeleted, @dateNow, @dateNow, @User, @User
	--	FROM @Items
	--	WHERE ItemState = 'N';

	------ Update Subitems
	--UPDATE opi
	--SET	
	--	opi.SupplierInvoice = i.SupplierInvoice, 
	--	opi.ReceiveOrderId = i.ReceiveOrderId, 
	--	opi.Description = i.Description, 
	--	opi.AmountPay = i.AmountPay, 
	--	opi.DateModified = @dateNow, 
	--	opi.UpdatedBy = @User
	--FROM [dbo].[OutgoingPaymentItem] AS opi
	--	INNER JOIN @Items AS i
	--	ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId
	--	WHERE i.ItemState = 'M'

	------ Disable Subitems
	--UPDATE opi
	--SET opi.IsDeleted = 1, opi.DateModified = @dateNow, opi.UpdatedBy = @User
	--FROM [dbo].[OutgoingPaymentItem] AS opi
	--	INNER JOIN @Items AS i
	--	ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId 
	--	WHERE i.ItemState = 'D'

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;













GO
/****** Object:  StoredProcedure [dbo].[spExpenseRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spExpenseRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT e.*, s.SupplierName As VendorName, ec.ExpenseCategoryName
	FROM [Expense] e
	INNER JOIN [ExpenseCategory] ec
		ON e.ExpenseCategoryId = ec.ExpenseCategoryId
	INNER JOIN [Supplier] s 
		ON e.SupplierId = s.SupplierId
	WHERE e.BusinessId = @BusinessId
		AND e.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spFinancialSettingManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFinancialSettingManage]
    @BusinessId bigint,
    @CurrencyId tinyint,
    @DefaultPriceTypeId tinyint,
    @UseServiceCharge bit,
    @ServiceChargeAmount decimal(18,4),
    @SalesTaxTypeId tinyint,
    @SalesTaxAmount decimal(18,4),
    @UseStockItemControl bit,
    @CostingTypeId tinyint,
	@ItemState char(1),
    @UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N' BEGIN

		INSERT INTO [dbo].[FinancialSetting]
			   ([BusinessId]
			   ,[CurrencyId]
			   ,[DefaultPriceTypeId]
			   ,[UseServiceCharge]
			   ,[ServiceChargeAmount]
			   ,[SalesTaxTypeId]
			   ,[SalesTaxAmount]
			   ,[UseStockItemControl]
			   ,[CostingTypeId]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
		 VALUES
			   (@BusinessId 
			   ,@CurrencyId
			   ,@DefaultPriceTypeId
			   ,@UseServiceCharge
			   ,@ServiceChargeAmount
			   ,@SalesTaxTypeId
			   ,@SalesTaxAmount
			   ,@UseStockItemControl
			   ,@CostingTypeId
			   ,@dateNow
			   ,@dateNow
			   ,@username
			   ,@username)

	END ELSE IF @ItemState = 'M' BEGIN

		UPDATE [dbo].[FinancialSetting]
		   SET 
			  [CurrencyId] = @CurrencyId
			  ,[DefaultPriceTypeId] = @DefaultPriceTypeId
			  ,[UseServiceCharge] = @UseServiceCharge
			  ,[ServiceChargeAmount] = @ServiceChargeAmount
			  ,[SalesTaxTypeId] = @SalesTaxTypeId
			  ,[SalesTaxAmount] = @SalesTaxAmount
			  ,[UseStockItemControl] = @UseStockItemControl
			  ,[CostingTypeId] = @CostingTypeId
			  ,[DateModified] = @dateNow
			  ,[UpdatedBy] = @username
		 WHERE [BusinessId] = @BusinessId

	END ELSE IF @ItemState = 'D' BEGIN
		DECLARE @tmp varchar;
	END

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spFinancialSettingRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFinancialSettingRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1 fs.*, c.CurrencyName, dp.DefaultPriceTypeName, stt.SalesTaxTypeName, ct.CostingTypeName
	FROM [FinancialSetting] fs
	INNER JOIN [Currency] c
		ON fs.CurrencyId = c.CurrencyId
	INNER JOIN [DefaultPriceType] dp
		ON fs.DefaultPriceTypeId = dp.DefaultPriceTypeId
	INNER JOIN [SalesTaxType] stt
		ON fs.SalesTaxTypeId = stt.SalesTaxTypeId
	INNER JOIN [CostingType] ct
		ON fs.CostingTypeId = ct.CostingTypeId
	WHERE BusinessId = @BusinessId

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spGetItemPath]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetItemPath]
    @BusinessId bigint,
	@ItemGroupId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT ImagePath
	FROM ItemGroup
	WHERE BusinessId = @BusinessId
		AND ItemGroupId = @ItemGroupId

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spGetNextDocId]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetNextDocId]
    @BusinessId BIGINT,
    @DocType VARCHAR(50)
AS

DECLARE @colName VARCHAR(50);
DECLARE @tmpSql NVARCHAR(MAX);
DECLARE @numCurrent BIGINT;
DECLARE @tblNumCurrent table (PurchaseNo INT);

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRY
BEGIN TRANSACTION
	SELECT TOP 1 * 
	INTO #tmpDoc
	FROM [DocSetting]
	WITH (ROWLOCK) 
	WHERE BusinessId = @BusinessId

	IF NOT EXISTS(SELECT * FROM #tmpDoc)
	BEGIN
		RAISERROR( 'DocSetting item is missing for this business', 16, 1 );
	END
	
	SET @colName = @DocType + 'No';
	SET @tmpSql = N'SELECT @numCurrent = ' + @colName + N'+ 1 FROM #tmpDoc;';
	
	EXEC sp_executesql @tmpSql, N'@numCurrent int output', @numCurrent output;
	-- Record the next value to be the current value in the database
	EXEC (N'UPDATE [DocSetting] SET ' + @colName + N' = ' + @numCurrent + N' WHERE BusinessId = ' + @BusinessId);

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

RETURN @numCurrent;











GO
/****** Object:  StoredProcedure [dbo].[spGetNextDocName]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetNextDocName]
    @BusinessId BIGINT,
    @DocType VARCHAR(50),
	@RetDocName NVARCHAR(50) OUTPUT
AS
BEGIN TRY
	DECLARE @nextName NVARCHAR(50);
	DECLARE @nextNo BIGINT;
	EXEC @nextNo = spGetNextDocId @BusinessId = @BusinessId, @Doctype = @Doctype;
	--SET @nextName = convert(NVARCHAR(50), @nextNo);

	DECLARE @colName VARCHAR(50)
	DECLARE @tmpSql NVARCHAR(MAX)
	DECLARE @tmpName NVARCHAR(50)
	SET @colName = @DocType + 'Format';
	SET @tmpSql = N'SELECT @tmpName = ' + CONVERT(NVARCHAR(MAX), @colName) + N' FROM DocSetting WHERE BusinessId = ' + CONVERT(NVARCHAR(50), @BusinessId) + N';';

	EXEC sp_executesql @tmpSql, N'@tmpName NVARCHAR(50) output', @tmpName output;

	--@tmpName
	DECLARE @numFormat NVARCHAR(50)
	DECLARE @docName NVARCHAR(50)
	DECLARE @numPart NVARCHAR(50)
	DECLARE @posStart INT
	DECLARE @posEnd INT
	DECLARE @pad NVARCHAR(50)
	SET @posStart = CHARINDEX('{', @tmpName);
	SET @posEnd = CHARINDEX('}', @tmpName) - 1;
	SET @numFormat = SUBSTRING(@tmpName, CHARINDEX('{', @tmpName) + 1, @posEnd - @posStart);
	SET @pad = REPLACE(@numFormat, 'X', '0');
	SET @numPart = RIGHT(@pad + CONVERT(NVARCHAR(50), @nextNo), LEN(@pad));
	SET @docName = REPLACE(@tmpName, N'{' + @numFormat + N'}', @numPart); 

	--SELECT @tmpName;
	--SELECT @docName AS DocumentName;

	SET @RetDocName = @docName;

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

RETURN @nextNo;









GO
/****** Object:  StoredProcedure [dbo].[spIncomingPaymentAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spIncomingPaymentAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT ip.*,
			cust.CustomerName
	FROM [IncomingPayment] ip
	LEFT JOIN [Customer] cust
		ON ip.CustomerId = cust.CustomerId
	WHERE ip.BusinessId = @BusinessId
		AND ip.IsDisabled <> 1
		AND ip.IsCashSales <> 1
	ORDER BY ip.IncomingPaymentId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spIncomingPaymentItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spIncomingPaymentItemAllRetrieve]
	@IncomingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	--SELECT ipItem.*, ip.Date, ip.Subtotal, ip.Tax,
	--	CASE WHEN si.IsCashSales <> 1
	--	    THEN si.SalesInvoiceNo
	--		ELSE si.CashSalesNo
	--	END AS SalesInvoiceNo
	--FROM [dbo].[IncomingPaymentItem] ipItem
	--	INNER JOIN [dbo].[IncomingPayment] ip
	--		ON ip.IncomingPaymentId = ipItem.IncomingPaymentId
	--	INNER JOIN [dbo].[SalesInvoice] si
	--		ON si.SalesInvoiceId = ipItem.SalesInvoiceId
	--WHERE ipItem.IncomingPaymentId = @IncomingPaymentId
	--    AND ipItem.IsDisabled <> 1

	SELECT 
		ipi.SalesInvoiceId,
		si.SalesInvoiceNo,
		si.Date,
		si.Total,
		si.Tax,
		si.Subtotal,
		(si.Total - si.AmountPaid) AS AmountDue,
		ipi.Amount AS AmountPaid
	FROM [IncomingPaymentItem] ipi
	INNER JOIN [IncomingPayment] ip
		ON ipi.IncomingPaymentId = ip.IncomingPaymentId
	INNER JOIN [SalesInvoice] si
		ON ipi.SalesInvoiceId = si.SalesInvoiceId
	WHERE	ipi.IncomingPaymentId = @IncomingPaymentId
		AND ipi.IsDisabled <> 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;









GO
/****** Object:  StoredProcedure [dbo].[spIncomingPaymentManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spIncomingPaymentManage]
	@IncomingPaymentId bigint,
	@BusinessId bigint,
    @CustomerId bigint,
    @Reference nvarchar(50),
	@PaymentTypeId tinyint,
	@BankId bigint,
	@BankAccNo nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
	@Vat bit,
	@Wht bit,
	@WhtPercent decimal(18, 4),
    @Note nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
	--@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tIncomingPaymentItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetIpId BIGINT;
		SET @targetIpId = @IncomingPaymentId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextIpName NVARCHAR(50);
				SET @nextIpName = '';
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextIpName OUT;

			-- Insert Incoming Payment
			INSERT INTO [dbo].[IncomingPayment] 
				([BusinessId], [IncomingPaymentNo], [IsCashSales], [CustomerId], [Reference], [PaymentTypeId], [BankId], [BankAccNo],[Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextIpName, 0, @CustomerId, @Reference, @PaymentTypeId, @BankId, @BankAccNo, @Address1, @Address2, @Address3, @Phone, @Date, @Vat, @Wht, @WhtPercent, @Note, @Subtotal, @Tax, @Total, 'Paid', 0, @dateNow, @dateNow, @username, @username

			SET @targetIpId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[IncomingPayment]
			SET    
			[CustomerId] = @CustomerId, [Reference] = @Reference, [PaymentTypeId] = @PaymentTypeId,
			[BankId] = @BankId, [BankAccNo] = @BankAccNo,
			[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, [Phone] = @Phone, [Date] = @Date,
			[Vat] = @Vat, [Wht] = @Wht, [WhtPercent] = @WhtPercent, [Note] = @Note,
			[Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total, /*[Status] = @Status,*/
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[IncomingPaymentId] = @IncomingPaymentId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[IncomingPayment]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[IncomingPaymentId] = @IncomingPaymentId
		END

	-- Manage Items
	---- Insert Incoming Payment's items
	INSERT INTO [dbo].[IncomingPaymentItem] 
		([IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT IncomingPaymentId = @targetIpId, SalesInvoiceId, Amount, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Incoming Payment's items
	UPDATE ipItem
	SET ipItem.SalesInvoiceId = i.SalesInvoiceId, ipItem.Amount = i.Amount, 
	    ipItem.DateModified = @dateNow, ipItem.UpdatedBy = @username
	FROM [dbo].[IncomingPaymentItem] AS ipItem
		INNER JOIN @Items AS i
		ON ipItem.IncomingPaymentItemId = i.IncomingPaymentItemId 
		WHERE i.ItemState = 'M'

	---- Disable Incoming Payment's items
	UPDATE ipItem
	SET ipItem.IsDisabled = 1, ipItem.DateModified = @dateNow, ipItem.UpdatedBy = @username
	FROM [dbo].[IncomingPaymentItem] AS ipItem
		INNER JOIN @Items AS i
		ON ipItem.IncomingPaymentItemId = i.IncomingPaymentItemId 
		WHERE i.ItemState = 'D'

	-- Update SalesInvoice.Status of the SalesInvoice item that this IncomingPayment item refers to
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT result.SalesInvoiceId AS Id FROM (
		SELECT DISTINCT
			si.SalesInvoiceId
		FROM [SalesInvoice] si
		INNER JOIN [IncomingPaymentItem] ipi
			ON si.SalesInvoiceId = ipi.SalesInvoiceId
				AND si.IsDisabled <> 1
				AND si.Status <> 'Void'
		INNER JOIN [IncomingPayment] ip
			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
				AND ip.IsDisabled <> 1
				AND ip.Status <> 'Void'
		WHERE ip.IncomingPaymentId = @targetIpId
	) result;

	EXEC dbo.spSalesInvoiceStatusUpdate @updateItemList;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spIncomingPaymentRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spIncomingPaymentRetrieve]
    @IncomingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		ip.*,
		cust.CustomerName,
		pt.PaymentTypeName
	FROM [IncomingPayment] ip
	LEFT JOIN [Customer] cust
		ON ip.CustomerId = cust.CustomerId
	INNER JOIN [PaymentType] pt
		ON ip.PaymentTypeId = pt.PaymentTypeId
	WHERE	ip.IncomingPaymentId = @IncomingPaymentId;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spIncomingPaymentVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spIncomingPaymentVoid]
	@IncomingPaymentId BIGINT,
	@UserId BIGINT
AS
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
		SET @blockingItem = dbo.fnIsIncomingPaymentEditable(@IncomingPaymentId);
	DECLARE @ErrTxt NVARCHAR(MAX);

	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	UPDATE [dbo].[IncomingPayment]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE IncomingPaymentId = @IncomingPaymentId;

	-- Update SalesInvoice.Status of the SalesInvoice item that this IncomingPayment item refers to
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT result.SalesInvoiceId FROM (
		SELECT DISTINCT
			si.SalesInvoiceId
		FROM [SalesInvoice] si
		INNER JOIN [IncomingPaymentItem] ipi
			ON si.SalesInvoiceId = ipi.SalesInvoiceId
				AND si.IsDisabled <> 1
				AND si.Status <> 'Void'
		INNER JOIN [IncomingPayment] ip
			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
				AND ip.IsDisabled <> 1
				AND ip.Status <> 'Void'
		WHERE ip.IncomingPaymentId = @IncomingPaymentId
	) result;

	EXEC dbo.spSalesInvoiceStatusUpdate @updateItemList;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spItemAdjustmentAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemAdjustmentAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		ia.*
	FROM [ItemAdjustment] ia
	WHERE	ia.BusinessId = @BusinessId
		AND ia.IsDisabled <> 1
	ORDER BY ia.AdjustmentId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spItemAdjustmentItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemAdjustmentItemAllRetrieve]
	@AdjustmentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT iaItem.*, dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName, (iaItem.QuantityAfter - iaItem.QuantityBefore) AS 'Difference'
	FROM [ItemAdjustmentItem] iaItem
		,[Item] i
	INNER JOIN [dbo].[ItemGroup] ig
	    ON i.ItemGroupId = ig.ItemGroupId
	WHERE iaItem.AdjustmentId = @AdjustmentId
		AND iaItem.ItemId = i.ItemId
		AND iaItem.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;









GO
/****** Object:  StoredProcedure [dbo].[spItemAdjustmentManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemAdjustmentManage]
	@AdjustmentId bigint,
	@BusinessId bigint,
    @AdjustmentName nvarchar(MAX),
	@AdjustmentReasonId tinyint,
    @Note nvarchar(MAX),
	@Date datetime,
	--@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tItemAdjustmentItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetAmId BIGINT;
		SET @targetAmId = @AdjustmentId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	-- 1. Manage records in [ItemAdjustment] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextAmName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'StockAdj', @RetDocName = @nextAmName OUT;
			-- Insert Item Adjustment
			INSERT INTO [dbo].[ItemAdjustment] 
				([BusinessId],[AdjustmentName],[AdjustmentReasonId], [AdjustmentNo], [Note], [Date], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @AdjustmentName, @AdjustmentReasonId, @nextAmName, @Note, @Date, 'New', 0, @dateNow, @dateNow, @username, @username
			SET @targetAmId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[ItemAdjustment]
			SET    
			[AdjustmentName] = @AdjustmentName, [AdjustmentReasonId] = @AdjustmentReasonId, 
			[Note] = @Note, [Date] = @Date,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[AdjustmentId] = @AdjustmentId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[ItemAdjustment]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[AdjustmentId] = @AdjustmentId
		END

	-- 1. Manage SubItems
	---- Insert Item Adjustment's items
	INSERT INTO [dbo].[ItemAdjustmentItem]
	    ([AdjustmentId], [ItemId], [QuantityBefore], [QuantityAfter], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT AdjustmentId = @targetAmId, ItemId, QuantityBefore, QuantityAfter, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Sales Invoice's items
	UPDATE iaItem
	SET iaItem.ItemId = i.ItemId, iaItem.QuantityBefore = i.QuantityBefore, iaItem.QuantityAfter = i.QuantityAfter,
	    iaItem.DateModified = @dateNow, iaItem.UpdatedBy = @username
	FROM [dbo].[ItemAdjustmentItem] AS iaItem
		INNER JOIN @Items AS i
		ON iaItem.AdjustmentId = i.AdjustmentId 
		WHERE i.ItemState = 'M'

	---- Disable Sales Invoice's items
	UPDATE iaItem
	SET iaItem.IsDisabled = 1, iaItem.DateModified = @dateNow, iaItem.UpdatedBy = @username
	FROM [dbo].[ItemAdjustmentItem] AS iaItem
		INNER JOIN @Items AS i
		ON iaItem.AdjustmentId = i.AdjustmentId 
		WHERE i.ItemState = 'D'

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spItemAdjustmentReasonListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemAdjustmentReasonListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT AdjustmentReasonId AS Value, AdjustmentReasonName AS Text
	FROM ItemAdjustmentReason

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spItemAdjustmentRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemAdjustmentRetrieve]
	@AdjustmentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		ia.*
	FROM [ItemAdjustment] ia
	WHERE ia.AdjustmentId = @AdjustmentId

SET @ReturnValue = 0;
RETURN @ReturnValue;









GO
/****** Object:  StoredProcedure [dbo].[spItemAdjustmentVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemAdjustmentVoid]
	@AdjustmentId BIGINT,
	@UserId BIGINT
AS

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	UPDATE [dbo].[ItemAdjustment]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE AdjustmentId = @AdjustmentId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemAllRetrieve]
	@ItemGroupId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select i.*
	from Item i
	where
			i.ItemGroupId = @ItemGroupId
		and	i.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spItemBrandAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemBrandAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ib.*
	from ItemBrand ib
	where
		ib.BusinessId = @BusinessId
		AND	ib.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spItemBrandItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemBrandItemAllRetrieve]
	@BusinessId	BIGINT,
	@ItemBrandId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, sum(i.Quantity) as ItemQuantity, ig.DateModified
	from Item i
	left join ItemGroup ig
		on ig.ItemGroupId = i.ItemGroupId
	left join ItemCategory ic
		on ic.ItemCategoryId = ig.ItemCategoryId
	full outer join ItemBrand ib
		on ib.ItemBrandId = ig.ItemBrandId
	where
		ig.ItemBrandId = @ItemBrandId
		AND ig.BusinessId = @BusinessId
		AND	ig.IsDisabled <> 1
	group by ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, ig.DateModified

SET @ReturnValue = 0;
RETURN @ReturnValue;


GO
/****** Object:  StoredProcedure [dbo].[spItemBrandListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemBrandListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ItemBrandId AS Value, 
		ItemBrandName As Text 
	from 
		ItemBrand
	where 
			BusinessId = @BusinessId
		and IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spItemBrandManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemBrandManage]
	@ItemBrandId bigint,
	@BusinessId bigint,
    @ItemBrandName nvarchar(100),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tItemBrandItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetIbId BIGINT;
		SET @targetIbId = @ItemBrandId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	--DECLARE @updateItemList tIdTable;

	-- Manage records in [ItemBrand] table
	IF @ItemState = 'N'
		BEGIN
			-- Insert ItemBrand
			INSERT INTO [dbo].[ItemBrand] 
				([BusinessId],[ItemBrandName],[IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @ItemBrandName, 0, @dateNow, @dateNow, @username, @username
			SET @targetIbId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			-- Update ItemBrand
			UPDATE [dbo].[ItemBrand]
			SET    
				[ItemBrandName] = @ItemBrandName,
				[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[ItemBrandId] = @ItemBrandId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			-- Delete ItemBrand
			UPDATE [dbo].[ItemBrand]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[ItemBrandId] = @ItemBrandId
		END

	---- Insert ItemGroup
	--INSERT INTO [dbo].[ItemGroup]
	--    ([BusinessId], [ItemBrandId], [ItemGroupCode], [ItemGroupName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	--SELECT BusinessId = @BusinessId, ItemBrandId = @targetIbId, ItemGroupCode, ItemGroupName, 0, @dateNow, @dateNow, @username, @username
	--	FROM @Items
	--	WHERE ItemState = 'N';

	---- Update ItemGroup
	UPDATE igItem
	SET igItem.ItemBrandId = @targetIbId, 
	    igItem.DateModified = @dateNow, igItem.UpdatedBy = @username
	FROM [dbo].[ItemGroup] AS igItem
		INNER JOIN @Items AS i
		ON igItem.ItemGroupId = i.ItemGroupId 
		WHERE i.ItemState = 'M'

	---- Disable ItemGroup
	UPDATE igItem
	SET igItem.IsDisabled = 1, igItem.DateModified = @dateNow, igItem.UpdatedBy = @username
	FROM [dbo].[ItemGroup] AS igItem
		INNER JOIN @Items AS i
		ON igItem.ItemGroupId = i.ItemGroupId
		WHERE i.ItemState = 'D'


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;



GO
/****** Object:  StoredProcedure [dbo].[spItemBrandRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemBrandRetrieve]
	@BusinessId BIGINT,
	@ItemBrandId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ib.*
	from ItemBrand ib
	where
		ib.ItemBrandId = @ItemBrandId
		AND ib.BusinessId = @BusinessId
		AND	ib.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spItemCategoryAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemCategoryAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ic.ItemCategoryId, ic.ItemCategoryName--, sum(i.Quantity) as ItemQuantity
	from ItemCategory ic
/*	inner join ItemGroup ig
	on ig.ItemCategoryId = ic.ItemCategoryId
	left join Item i
	on i.ItemGroupId = ig.ItemGroupId */
	where
		ic.BusinessId = @BusinessId
		AND	ic.IsDisabled <> 1
	--group by ic.ItemCategoryId, ic.ItemCategoryName

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spItemCategoryItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemCategoryItemAllRetrieve]
	@BusinessId	BIGINT,
	@ItemCategoryId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, sum(i.Quantity) as ItemQuantity, ig.DateModified
	from Item i
	left join ItemGroup ig
		on ig.ItemGroupId = i.ItemGroupId
	left join ItemCategory ic
		on ic.ItemCategoryId = ig.ItemCategoryId
	full outer join ItemBrand ib
		on ib.ItemBrandId = ig.ItemBrandId
	where
		ig.ItemCategoryId = @ItemCategoryId
		AND ig.BusinessId = @BusinessId
		AND	ig.IsDisabled <> 1
	group by ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, ig.DateModified

SET @ReturnValue = 0;
RETURN @ReturnValue;


GO
/****** Object:  StoredProcedure [dbo].[spItemCategoryListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemCategoryListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ItemCategoryId AS Value, 
		ItemCategoryName As Text 
	from 
		ItemCategory
	where 
			BusinessId = @BusinessId
		and IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spItemCategoryRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemCategoryRetrieve]
	@BusinessId BIGINT,
	@ItemCategoryId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ic.*
	from ItemCategory ic
	where
		ic.ItemCategoryId = @ItemCategoryId
		AND ic.BusinessId = @BusinessId
		AND	ic.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spItemGroupAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemGroupAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, iCate.ItemCategoryName, iBrand.ItemBrandName, SUM(i.Quantity) AS Quantity
	from Item i 
	inner join ItemGroup ig
		on ig.ItemGroupId = i.ItemGroupId
	left join ItemCategory iCate
		on ig.ItemCategoryId = iCate.ItemCategoryId
	left join ItemBrand iBrand
		on ig.ItemBrandId = iBrand.ItemBrandId
	where
			ig.BusinessId = @BusinessId
		AND	ig.IsDisabled <> 1
	group by ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, iCate.ItemCategoryName, iBrand.ItemBrandName

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spItemGroupCreateInline]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemGroupCreateInline]
	@BusinessId bigint,
	@ItemGroupName bigint,
	@UserId bigint
AS
BEGIN TRY
BEGIN TRANSACTION
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	declare @dateNow datetime;
		set @dateNow = getdate();
	declare @username varchar(50);
		exec @username = GetUsername @UserId = @UserId;
	declare @targetItemGroupId bigint;

	insert into ItemGroup
		([BusinessId]
		,[ItemGroupCode]
		,[ItemGroupName]
		,[ImagePath]
		,[ItemCategoryId]
		,[ItemBrandId]
		,[Cost]
		,[Price]
		,[Uom]
		,[IsForSales]
		,[IsDisabled]
		,[DateCreated]
		,[DateModified]
		,[CreatedBy]
		,[UpdatedBy])
	VALUES 
	( 
		@BusinessId, N'', @ItemGroupName, '', NULL, NULL, 0, 0, 'Unit', 0, 0, 
		@dateNow, @dateNow, @username, @username
	);
	SET @targetItemGroupId = SCOPE_IDENTITY();


	--- Insert Item
	INSERT INTO Item
        ([ItemGroupId]
        ,[ItemName]
        ,[Barcode]
        ,[Quantity]
        ,[Cost]
        ,[Price]
        ,[Price02]
        ,[Price03]
        ,[Price04]
        ,[IsForSales]
        ,[IsDisabled]
        ,[DateCreated]
        ,[DateModified]
        ,[CreatedBy]
        ,[UpdatedBy])
	VALUES 
	(
		@targetItemGroupId, @ItemGroupName, '', 0, 0, 0, 0,  0, 0,
		0, 0, @dateNow, @dateNow, @username, @username 
	);

	SET @ReturnValue = 0;
	RETURN @ReturnValue;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;













GO
/****** Object:  StoredProcedure [dbo].[spItemGroupCreation]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemGroupCreation]
	@BusinessId bigint,
    @ItemGroupName nvarchar(50),
	@ItemGroupCode nvarchar(50),
	@ItemName nvarchar(100),
    @ItemCategoryName nvarchar(100) = null,
    @ItemBrandName nvarchar(100) = null,
	@Quantity decimal(18,4) = 1,
    @Cost decimal(18,4) = 1,
    @Price decimal(18,4) = 1,
    @Uom nvarchar(50) = 1,
    @IsForSales bit = TRUE,
	@ItemId bigint = -1 out,
    @UserId bigint,
	@ItemState char(1),
	@Version datetime
AS
BEGIN TRY

	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	declare @dateNow datetime;
		set @dateNow = getdate();
	declare @username varchar(50);
		exec @username = GetUsername @UserId = @UserId;
	declare @targetItemGroupId bigint;
		set @targetItemGroupId = null;
	--declare @targetItemBrandId bigint;
	--	set @targetItemBrandId = null;
	--declare @targetItemCategoryId bigint;
	--	set @targetItemCategoryId = null;

	IF @ItemState = 'N'
		--SELECT TOP 1 @targetItemCategoryId = ItemCategoryId
		--FROM ItemCategory
		--WHERE BusinessId = @BusinessId
		--	AND ItemCategoryName = @ItemCategoryName;
		--IF @@ROWCOUNT = 0
		--BEGIN
		--	---- Insert ItemsCategory
		--	INSERT INTO ItemCategory
		--		([BusinessId]
		--		,[ItemCategoryName]
		--		,[IsDisabled]
		--		,[DateCreated]
		--		,[DateModify]
		--		,[CreatedBy]
		--		,[UpdatedBy])
		--	VALUES 
		--	( 
		--		@BusinessId, @ItemCategoryName, 0,
		--		@dateNow, @dateNow, @username, @username
		--	);
		--	SET @targetItemCategoryId = SCOPE_IDENTITY();
		--END

		--SELECT TOP 1 @targetItemBrandId = ItemBrandId
		--FROM ItemBrand
		--WHERE BusinessId = @BusinessId
		--	AND ItemBrandName = @ItemBrandName;
		--IF @@ROWCOUNT = 0
		--BEGIN
		--	---- Insert ItemsBrand
		--	INSERT INTO ItemBrand
		--		([BusinessId]
		--		,[ItemBrandName]
		--		,[IsDisabled]
		--		,[DateCreated]
		--		,[DateModified]
		--		,[CreatedBy]
		--		,[UpdatedBy])
		--	VALUES 
		--	( 
		--		@BusinessId, @ItemBrandName, 0,
		--		@dateNow, @dateNow, @username, @username
		--	);
		--	SET @targetItemBrandId = SCOPE_IDENTITY();
		--END

		--SELECT 1 
		--FROM ItemGroup
		--WHERE BusinessId = @BusinessId
		--	AND ItemCategoryId = @targetItemCategoryId
		--	AND ItemBrandId = @targetItemBrandId
		--	AND ItemGroupName = @ItemGroupName;
		--IF @@ROWCOUNT <> 0
		--BEGIN
		--	RAISERROR( 'Itemname already exists', 16, 1 );
		--	RETURN 1;
		--END

		SELECT TOP 1 @targetItemGroupId = ItemGroupId
		FROM ItemGroup
		WHERE BusinessId = @BusinessId
			AND ItemGroupName = @ItemGroupName
			AND ItemGroupCode = @ItemGroupCode;
		IF @@ROWCOUNT = 0
		BEGIN
			---- Insert ItemsGroup
			INSERT INTO ItemGroup
				([BusinessId]
				,[ItemGroupCode]
				,[ItemGroupName]
				,[ImagePath]
				,[ItemCategoryId]
				,[ItemBrandId]
				,[Cost]
				,[Price]
				,[Uom]
				,[IsForSales]
				,[IsDisabled]
				,[DateCreated]
				,[DateModified]
				,[CreatedBy]
				,[UpdatedBy])
			VALUES 
			( 
				@BusinessId, @ItemGroupCode, @ItemGroupName, '',
				null, null, 
				@Cost, @Price, @Uom, @IsForSales, 0, 
				@dateNow, @dateNow, @username, @username
			);
			SET @targetItemGroupId = SCOPE_IDENTITY();
		END

		SELECT TOP 1 *
		FROM Item
		WHERE ItemGroupId = @targetItemGroupId
			AND ItemName = @ItemName;
		IF @@ROWCOUNT <> 0
		BEGIN
			RAISERROR( 'Itemname already exists', 16, 1 );
			RETURN 1;
		END
		ELSE
		BEGIN
			---- Insert Items
			INSERT INTO Item
				([ItemGroupId]
				,[ItemName]
				,[Barcode]
				,[Quantity]
				,[Cost]
				,[Price]
				,[Price02]
				,[Price03]
				,[Price04]
				,[IsForSales]
				,[IsDisabled]
				,[DateCreated]
				,[DateModified]
				,[CreatedBy]
				,[UpdatedBy])
			VALUES
			(
				@targetItemGroupId, @ItemName, '',
				@Quantity, @Cost, @Price, 0, 0, 0,
				@IsForSales, 0,
				@dateNow, @dateNow, @username, @username 
			);
		END

		SET @ItemId = SCOPE_IDENTITY();

		SET @ReturnValue = 0;
		RETURN @ReturnValue;
END TRY
BEGIN CATCH

	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spItemGroupInfoRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemGroupInfoRetrieve]
	@BusinessId	BIGINT,
	@ItemGroupId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, sum(i.Quantity) as ItemQuantity, ig.DateModified
	from Item i
	left join ItemGroup ig
		on ig.ItemGroupId = i.ItemGroupId
	full outer join ItemBrand ib
		on ib.ItemBrandId = ig.ItemBrandId
	where
		ig.ItemGroupId = @ItemGroupId
		AND ig.BusinessId = @BusinessId
		AND	ig.IsDisabled <> 1
	group by ig.ItemGroupId, ig.ItemGroupCode, ig.ItemGroupName, ib.ItemBrandName, ig.DateModified

SET @ReturnValue = 0;
RETURN @ReturnValue;


GO
/****** Object:  StoredProcedure [dbo].[spItemGroupListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemGroupListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		ItemGroupId AS Value, 
		ItemGroupName As Text 
	from 
		ItemGroup
	where 
			BusinessId = @BusinessId
		and IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spItemGroupManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemGroupManage]
	@BusinessId bigint,
	@ItemGroupId bigint,
    @ItemGroupCode varchar(50),
    @ItemGroupName nvarchar(50),
    --@ImagePath varchar(max),
    @ItemCategoryId bigint,
    @ItemBrandId bigint,
    @Cost decimal(18,4),
    @Price decimal(18,4),
    @Uom nvarchar(50),
    @IsForSales bit,
    @UserId bigint,
	@ItemState char(1),
	@Version datetime,
	@Items tItem readonly
AS
BEGIN TRY

	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	declare @dateNow datetime;
		set @dateNow = getdate();
	declare @username varchar(50);
		exec @username = GetUsername @UserId = @UserId;
	declare @targetItemGroupId bigint;
		set @targetItemGroupId = @ItemGroupId

	IF @ItemState = 'N'
		BEGIN
			insert into ItemGroup
				([BusinessId]
			   ,[ItemGroupCode]
			   ,[ItemGroupName]
			   --,[ImagePath]
			   ,[ItemCategoryId]
			   ,[ItemBrandId]
			   ,[Cost]
			   ,[Price]
			   ,[Uom]
			   ,[IsForSales]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
		   VALUES 
		   ( 
			   @BusinessId, @ItemGroupCode, @ItemGroupName,
			   --@ImagePath,
			   @ItemCategoryId, @ItemBrandId, @Cost, @Price, @Uom, @IsForSales, 0, 
			   @dateNow, @dateNow, @username, @username
			);
			SET @targetItemGroupId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			update ItemGroup
			set 
			   ItemGroupCode = @ItemGroupCode, ItemGroupName = @ItemGroupName,
			   --ImagePath = @ImagePath,
			   ItemCategoryId = @ItemCategoryId, 
			   ItemBrandId = @ItemBrandId, Cost = @Cost, Price = @Price, Uom = @Uom, IsForSales = @IsForSales,
			   DateCreated = @dateNow, DateModified = @dateNow, UpdatedBy = @username
			where
				ItemGroupId = @ItemGroupId;
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			update ItemGroup
			set
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @username
			where
				ItemGroupId = @ItemGroupId
		END

	-- Manage Items
	---- Insert Items
	INSERT INTO Item
        ([ItemGroupId]
        ,[ItemName]
        ,[Barcode]
        ,[Quantity]
        ,[Cost]
        ,[Price]
        ,[Price02]
        ,[Price03]
        ,[Price04]
        ,[IsForSales]
        ,[IsDisabled]
        ,[DateCreated]
        ,[DateModified]
        ,[CreatedBy]
        ,[UpdatedBy])
		SELECT 
			ItemGroupId = @targetItemGroupId, ItemName, Barcode, Quantity, Cost, Price, Price02 = 0, Price03 = 0, Price04 = 0,
			IsForSales, 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username 
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Items
	UPDATE Item
	SET
		ItemName = i.ItemName, Barcode = i.Barcode, Quantity = i.Quantity, Cost = i.Cost, Price = i.Price, IsForSales = i.IsForSales, IsDisabled = 0,
		DateModified = @dateNow, UpdatedBy = @username
	FROM @Items i
		WHERE 
			ItemState = 'M'
		And i.ItemGroupId = @ItemGroupId

	---- Delete Items
	UPDATE Item
	SET
		IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @username
	FROM @Items i
		WHERE ItemState = 'D'
		And i.ItemGroupId = @ItemGroupId


	SET @ReturnValue = 0;
	RETURN @ReturnValue;


END TRY
BEGIN CATCH

	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spItemGroupRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemGroupRetrieve]
	@ItemGroupId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ig.*, iCate.ItemCategoryName, iBrand.ItemBrandName
	from ItemGroup ig
	left join ItemCategory iCate
		on ig.ItemCategoryId = iCate.ItemCategoryId
	left join ItemBrand iBrand
		on ig.ItemBrandId = iBrand.ItemBrandId
	where
			ig.ItemGroupId = @ItemGroupId
		AND	ig.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spItemListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	--SELECT 
	--	i.ItemId AS Value, 
	--	CASE 
	--		WHEN ig.ItemGroupName = i.ItemName THEN CONCAT(ig.ItemGroupCode, N' ', ig.ItemGroupName) 
	--		ELSE CONCAT(ig.ItemGroupCode, N': ', ig.ItemGroupName, N' - ', i.ItemName) 
	--	END AS Text
	--FROM [ITEM] i
	--	INNER JOIN [ItemGroup] ig
	--	ON i.ItemGroupId = ig.ItemGroupId
	--WHERE 
	--		i.IsDisabled <> 1
	--	AND ig.IsDisabled <> 1

	SELECT 
		i.ItemId, i.ItemName, ig.ItemGroupName, ig.ItemGroupCode
	FROM [ITEM] i
		INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	WHERE 
			i.IsDisabled <> 1
		AND ig.IsDisabled <> 1
		AND ig.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spItemReceiveTransferAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemReceiveTransferAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @sqlCommand AS NVARCHAR(MAX);
	DECLARE @filterSql AS NVARCHAR(250);
	DECLARE @sortingSql AS NVARCHAR(250);
	DECLARE @OutputColumnSQL AS NVARCHAR(250);

	SELECT 
		it.TransferId,
		it.TransferName,
		it.DateSent,
		its.TransferStatusTypeName
		--it.Status
	FROM [ItemTransfer] it
	LEFT JOIN [ItemTransferStatusType] its
		ON its.TransferStatusTypeId = it.TransferStatusTypeId
	WHERE	it.TargetBusinessId = @BusinessId
		AND it.IsDisabled <> 1
	ORDER BY it.TransferId DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;







GO
/****** Object:  StoredProcedure [dbo].[spItemReceiveTransferItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemReceiveTransferItemAllRetrieve]
	@TransferId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT itItem.*,
		dbo.fnGetItemFullName(itItem.ItemGroupCode, itItem.ItemGroupName, itItem.ItemName) AS ItemFullName
	FROM [dbo].[ItemTransferItem] itItem
	WHERE itItem.TransferId = @TransferId
		AND itItem.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spItemReceiveTransferManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemReceiveTransferManage]
	@BusinessId BIGINT,
	@TransferId BIGINT,
	@TransferName NVARCHAR(200),
	@SourceBusinessId BIGINT,
	@TargetBusinessId BIGINT,
	@Reference NVARCHAR(100),
	@DateSent DATETIME,
	@DateReceived DATETIME,
	@TransferStatusTypeId TINYINT,
	@SubItems tItemTransfer READONLY,
	@UserId BIGINT,
	@ItemState CHAR(1),
	@Version DATETIME
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetTfId BIGINT;
		SET @targetTfId = @TransferId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	--DECLARE @status VARCHAR(10);
	--	SET @status = 'New';

	--DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	--DECLARE @updateItemList tIdTable;

	DECLARE @AmendingItem BIT;
		SET @AmendingItem = 0;

	-- 1. Manage [ItemTransfer] table
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[ItemTransfer]
			   ([TransferName]
			   ,[SourceBusinessId]
			   ,[TargetBusinessId]
			   ,[Reference]
			   ,[DateSent]
			   ,[DateReceived]
			   ,[TransferStatusTypeId]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
			   (@TransferName
			   ,@SourceBusinessId
			   ,@TargetBusinessId
			   ,@Reference
			   ,@DateSent
			   ,@DateReceived
			   ,1 --'Transferring'
			   ,0
			   ,@dateNow
			   ,@dateNow
			   ,@username
			   ,@username)
			SET @targetTfId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			-- Check that only one of the following conditions of the TransferStatus is met, otherwise, raise error.
			-- 1. Transferring -> Transferring (Status can remain the same but there are some other changes to the record instead)
			-- 2. Transferring -> Canceled
			-- 3. Transferring -> Received
			-- 4. Transferring -> Rejected
			DECLARE @currentStatusTypeId TINYINT;
				SET @currentStatusTypeId = NULL;

			SELECT TOP 1 @currentStatusTypeId = it.TransferStatusTypeId
			FROM [ItemTransfer] it
			WHERE	it.TransferId = @TransferId;

			IF @currentStatusTypeId IS NULL
			BEGIN
				SET @ErrTxt = N'Missing ItemTransfer with TransferId = ' + CONVERT(NVARCHAR(20), @TransferId);
				RAISERROR( @ErrTxt, 16, 1 );
			END
		
			IF (@currentStatusTypeId <> 1) AND (@currentStatusTypeId <> @TransferStatusTypeId)
			BEGIN
				SET @ErrTxt = N'The status of this item can no longer be changed';
				RAISERROR( @ErrTxt, 16, 1 );
			END

			-- the transfer status changes from [Transferring] to [Received]
			IF (@currentStatusTypeId = 1) AND (@TransferStatusTypeId = 3)
				SET @AmendingItem = 1;

			UPDATE [dbo].[ItemTransfer]
			   SET [Reference] = @Reference
				  ,[DateReceived] = @DateReceived
				  ,[TransferStatusTypeId] = @TransferStatusTypeId
				  ,[DateModified] = @dateNow
				  ,[UpdatedBy] = @username
			 WHERE	[TransferId] = @TransferId;
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[ItemTransfer]
			   SET [IsDisabled] = 1
				  ,[UpdatedBy] = @username
			 WHERE	[TransferId] = @TransferId;
		END

	-- 2. Manage SubItems
	---- Insert Item Transfer's items
	INSERT INTO [dbo].[ItemTransferItem]
        ([TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT 
		TransferId = @targetTfId,
		ItemId = subItem.ItemId,
		ItemGroupCode = ig.ItemGroupCode, ItemGroupName = ig.ItemGroupName, ItemName = i.ItemName,
        Uom = ig.Uom, ImagePath = ig.ImagePath, ItemCategoryName = ic.ItemCategoryName, ItemBrandName = ib.ItemBrandName,
        Barcode = i.Barcode, Quantity = subItem.Quantity, Cost = i.Cost, Price = i.Price, IsForSales = i.IsForSales,
        IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
	FROM @SubItems subItem
	INNER JOIN [Item] i
		ON subItem.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		ON ig.ItemBrandId = ib.ItemBrandId
	WHERE subItem.ItemState = 'N';

	---- Update Item Transfer's items
	UPDATE itItem
	SET
		itItem.ItemGroupCode = ig.ItemGroupCode, itItem.ItemGroupName = ig.ItemGroupName, itItem.ItemName = i.ItemName,
        itItem.Uom = ig.Uom, itItem.ImagePath = ig.ImagePath, itItem.ItemCategoryName = ic.ItemCategoryName, itItem.ItemBrandName = ib.ItemBrandName,
        itItem.Barcode = i.Barcode, itItem.Quantity = subItem.Quantity, itItem.Cost = i.Cost, itItem.Price = i.Price, itItem.IsForSales = i.IsForSales,
	    itItem.DateModified = @dateNow, itItem.UpdatedBy = @username
	FROM [dbo].[ItemTransferItem] AS itItem
	INNER JOIN @SubItems subItem
		ON itItem.TransferId = subItem.TransferId
	INNER JOIN [Item] i
		ON subItem.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		ON ig.ItemBrandId = ib.ItemBrandId
	WHERE subItem.ItemState = 'M'

	---- Disable Item Transfer's items
	UPDATE itItem
	SET itItem.IsDisabled = 1, itItem.DateModified = @dateNow, itItem.UpdatedBy = @username
	FROM [dbo].[ItemTransferItem] AS itItem
	INNER JOIN @SubItems subItem
		ON itItem.TransferId = subItem.TransferId
	WHERE subItem.ItemState = 'D'

	IF (@AmendingItem = 1)
	BEGIN
		-- Adjust [Item].Quantity if Status changes from [Transferring] to [Received]
		-- ToDo : 1. Create Item if it does not exist ( if Brand and Category do not exist, assign <null> to them )
		--BEGIN TRANSACTION

		INSERT INTO ItemCategory
			([BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemCategoryName = itItem.ItemCategoryName,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemCategoryId(@TargetBusinessId, itItem.ItemCategoryName) = 0

		INSERT INTO ItemBrand
			([BusinessId], [ItemBrandName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemBrandName = itItem.ItemBrandName,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemBrandId(@TargetBusinessId, itItem.ItemBrandName) = 0

		--COMMIT TRANSACTION;
		--BEGIN TRANSACTION

		INSERT INTO ItemGroup
			([BusinessId] ,[ItemGroupCode] ,[ItemGroupName] ,[ImagePath] ,[ItemCategoryId] ,[ItemBrandId] ,[Cost] ,[Price] ,[Uom] ,[IsForSales] ,[IsDisabled] ,[DateCreated] ,[DateModified] ,[CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemGroupCode = itItem.ItemGroupCode, ItemGroupName = itItem.ItemGroupName, ImagePath = itItem.ImagePath,
			ItemCategoryId = dbo.fnGetItemCategoryId(@TargetBusinessId, itItem.ItemCategoryName),
			ItemBrandId = dbo.fnGetItemBrandId(@TargetBusinessId, itItem.ItemBrandName),
			Cost = itItem.Cost, Price = itItem.Price, Uom = itItem.Uom, IsForSales = itItem.IsForSales,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemGroupId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName) = 0

		--COMMIT TRANSACTION;
		--BEGIN TRANSACTION
		
		INSERT INTO [dbo].[Item]
           ([ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
        SELECT
			ItemGroupId = dbo.fnGetItemGroupId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName),
			ItemName = itItem.ItemName,
			Barcode = itItem.Barcode, Quantity = 0, Cost = itItem.Cost,
			Price = itItem.Price, Price02 = 0, Price03 = 0, Price04 = 0,
			IsForSales = itItem.IsForSales, 
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName, itItem.ItemName) = 0

		--COMMIT TRANSACTION;

		-- ToDo : 2. Adjust Item.Quantity
		-- recrease the item quantity from source business
		UPDATE i
			SET i.Quantity = i.Quantity - itItem.Quantity
		FROM [Item] i
		INNER JOIN ItemTransferItem itItem
			ON itItem.ItemName = i.ItemName
		INNER JOIN @SubItems subItem
			ON subItem.TransferItemId = itItem.TransferItemId
		INNER JOIN ItemGroup ig
			ON ig.BusinessId = @SourceBusinessId
			AND ig.ItemGroupId = i.ItemGroupId

		-- increase the item quantity from target business
		UPDATE i
		SET i.Quantity = i.Quantity + itItem.Quantity
		FROM [Item] i
		INNER JOIN ItemTransferItem itItem
			ON itItem.ItemName = i.ItemName
		INNER JOIN @SubItems subItem
			ON subItem.TransferItemId = itItem.TransferItemId
		INNER JOIN ItemGroup ig
			ON ig.BusinessId = @TargetBusinessId
			AND ig.ItemGroupId = i.ItemGroupId
	END


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spItemReceiveTransferRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemReceiveTransferRetrieve]
    @TransferId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		it.*,
		b1.BusinessName AS SourceBusinessName,
		b2.BusinessName AS TargetBusinessName,
		itst.TransferStatusTypeName
	FROM [ItemTransfer] it
	INNER JOIN [Business] b1
		ON	it.SourceBusinessId = b1.BusinessId
	INNER JOIN [Business] b2
		ON	it.TargetBusinessId = b2.BusinessId
	INNER JOIN [ItemTransferStatusType] itst
		ON	it.TransferStatusTypeId = itst.TransferStatusTypeId
	WHERE	it.TransferId = @TransferId;

SET @ReturnValue = 0;
RETURN @ReturnValue;







GO
/****** Object:  StoredProcedure [dbo].[spItemRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemRetrieve]
    @ItemId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT i.*, ig.Uom, ig.ItemGroupName, ig.ItemGroupCode, ig.ImagePath, ig.IsForSales as IsItemGroupForSales, 
			ic.ItemCategoryName, ib.ItemBrandName
	FROM [ITEM] i
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		on ig.ItemBrandId = ib.ItemBrandId
	WHERE i.ItemId = @ItemId

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spItemTransferAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemTransferAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @sqlCommand AS NVARCHAR(MAX);
	DECLARE @filterSql AS NVARCHAR(250);
	DECLARE @sortingSql AS NVARCHAR(250);
	DECLARE @OutputColumnSQL AS NVARCHAR(250);

	SELECT 
		it.TransferId,
		it.TransferName,
		it.DateSent,
		its.TransferStatusTypeName
		--it.Status
	FROM [ItemTransfer] it
	LEFT JOIN [ItemTransferStatusType] its
		ON its.TransferStatusTypeId = it.TransferStatusTypeId
	WHERE	it.SourceBusinessId = @BusinessId
		AND it.IsDisabled <> 1
	ORDER BY it.TransferId DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;







GO
/****** Object:  StoredProcedure [dbo].[spItemTransferItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemTransferItemAllRetrieve]
	@TransferId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT itItem.*,
		dbo.fnGetItemFullName(itItem.ItemGroupCode, itItem.ItemGroupName, itItem.ItemName) AS ItemFullName
	FROM [dbo].[ItemTransferItem] itItem
	WHERE itItem.TransferId = @TransferId
		AND itItem.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spItemTransferManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemTransferManage]
	@BusinessId BIGINT,
	@TransferId BIGINT,
	@TransferName NVARCHAR(200),
	@SourceBusinessId BIGINT,
	@TargetBusinessId BIGINT,
	@Reference NVARCHAR(100),
	@DateSent DATETIME,
	@DateReceived DATETIME,
	@TransferStatusTypeId TINYINT,
	@SubItems tItemTransfer READONLY,
	@UserId BIGINT,
	@ItemState CHAR(1),
	@Version DATETIME
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetTfId BIGINT;
		SET @targetTfId = @TransferId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	--DECLARE @status VARCHAR(10);
	--	SET @status = 'New';

	--DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	--DECLARE @updateItemList tIdTable;

	DECLARE @AmendingItem BIT;
		SET @AmendingItem = 0;

	-- 1. Manage [ItemTransfer] table
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[ItemTransfer]
			   ([TransferName]
			   ,[SourceBusinessId]
			   ,[TargetBusinessId]
			   ,[Reference]
			   ,[DateSent]
			   ,[DateReceived]
			   ,[TransferStatusTypeId]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
			   (@TransferName
			   ,@SourceBusinessId
			   ,@TargetBusinessId
			   ,@Reference
			   ,@DateSent
			   ,@DateReceived
			   ,1 --'Transferring'
			   ,0
			   ,@dateNow
			   ,@dateNow
			   ,@username
			   ,@username)
			SET @targetTfId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			-- Check that only one of the following conditions of the TransferStatus is met, otherwise, raise error.
			-- 1. Transferring -> Transferring (Status can remain the same but there are some other changes to the record instead)
			-- 2. Transferring -> Canceled
			-- 3. Transferring -> Received
			-- 4. Transferring -> Rejected
			DECLARE @currentStatusTypeId TINYINT;
				SET @currentStatusTypeId = NULL;

			SELECT TOP 1 @currentStatusTypeId = it.TransferStatusTypeId
			FROM [ItemTransfer] it
			WHERE	it.TransferId = @TransferId;

			IF @currentStatusTypeId IS NULL
			BEGIN
				SET @ErrTxt = N'Missing ItemTransfer with TransferId = ' + CONVERT(NVARCHAR(20), @TransferId);
				RAISERROR( @ErrTxt, 16, 1 );
			END
		
			IF (@currentStatusTypeId <> 1) AND (@currentStatusTypeId <> @TransferStatusTypeId)
			BEGIN
				SET @ErrTxt = N'The status of this item can no longer be changed';
				RAISERROR( @ErrTxt, 16, 1 );
			END

			-- the transfer status changes from [Transferring] to [Received]
			IF (@currentStatusTypeId = 1) AND (@TransferStatusTypeId = 3)
				SET @AmendingItem = 1;

			UPDATE [dbo].[ItemTransfer]
			   SET [Reference] = @Reference
				  ,[DateReceived] = @DateReceived
				  ,[TransferStatusTypeId] = @TransferStatusTypeId
				  ,[DateModified] = @dateNow
				  ,[UpdatedBy] = @username
			 WHERE	[TransferId] = @TransferId;
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[ItemTransfer]
			   SET [IsDisabled] = 1
				  ,[UpdatedBy] = @username
			 WHERE	[TransferId] = @TransferId;
		END

	-- 2. Manage SubItems
	---- Insert Item Transfer's items
	INSERT INTO [dbo].[ItemTransferItem]
        ([TransferId], [ItemId], [ItemGroupCode], [ItemGroupName], [ItemName], [Uom], [ImagePath], [ItemCategoryName], [ItemBrandName], [Barcode], [Quantity], [Cost], [Price], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT 
		TransferId = @targetTfId,
		ItemId = subItem.ItemId,
		ItemGroupCode = ig.ItemGroupCode, ItemGroupName = ig.ItemGroupName, ItemName = i.ItemName,
        Uom = ig.Uom, ImagePath = ig.ImagePath, ItemCategoryName = ic.ItemCategoryName, ItemBrandName = ib.ItemBrandName,
        Barcode = i.Barcode, Quantity = subItem.Quantity, Cost = i.Cost, Price = i.Price, IsForSales = i.IsForSales,
        IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
	FROM @SubItems subItem
	INNER JOIN [Item] i
		ON subItem.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		ON ig.ItemBrandId = ib.ItemBrandId
	WHERE subItem.ItemState = 'N';

	---- Update Item Transfer's items
	UPDATE itItem
	SET
		itItem.ItemGroupCode = ig.ItemGroupCode, itItem.ItemGroupName = ig.ItemGroupName, itItem.ItemName = i.ItemName,
        itItem.Uom = ig.Uom, itItem.ImagePath = ig.ImagePath, itItem.ItemCategoryName = ic.ItemCategoryName, itItem.ItemBrandName = ib.ItemBrandName,
        itItem.Barcode = i.Barcode, itItem.Quantity = subItem.Quantity, itItem.Cost = i.Cost, itItem.Price = i.Price, itItem.IsForSales = i.IsForSales,
	    itItem.DateModified = @dateNow, itItem.UpdatedBy = @username
	FROM [dbo].[ItemTransferItem] AS itItem
	INNER JOIN @SubItems subItem
		ON itItem.TransferId = subItem.TransferId
	INNER JOIN [Item] i
		ON subItem.ItemId = i.ItemId
	INNER JOIN [ItemGroup] ig
		ON i.ItemGroupId = ig.ItemGroupId
	LEFT JOIN [ItemCategory] ic
		ON ig.ItemCategoryId = ic.ItemCategoryId
	LEFT JOIN [ItemBrand] ib
		ON ig.ItemBrandId = ib.ItemBrandId
	WHERE subItem.ItemState = 'M'

	---- Disable Item Transfer's items
	UPDATE itItem
	SET itItem.IsDisabled = 1, itItem.DateModified = @dateNow, itItem.UpdatedBy = @username
	FROM [dbo].[ItemTransferItem] AS itItem
	INNER JOIN @SubItems subItem
		ON itItem.TransferId = subItem.TransferId
	WHERE subItem.ItemState = 'D'

	IF (@AmendingItem = 1)
	BEGIN
		-- Adjust [Item].Quantity if Status changes from [Transferring] to [Received]
		-- ToDo : 1. Create Item if it does not exist ( if Brand and Category do not exist, assign <null> to them )
		--BEGIN TRANSACTION

		INSERT INTO ItemCategory
			([BusinessId], [ItemCategoryName], [IsDisabled], [DateCreated], [DateModify], [CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemCategoryName = itItem.ItemCategoryName,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemCategoryId(@TargetBusinessId, itItem.ItemCategoryName) = 0

		INSERT INTO ItemBrand
			([BusinessId], [ItemBrandName], [IsDisabled], [DateCreated], [DateModified], [CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemBrandName = itItem.ItemBrandName,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemBrandId(@TargetBusinessId, itItem.ItemBrandName) = 0

		--COMMIT TRANSACTION;
		--BEGIN TRANSACTION

		INSERT INTO ItemGroup
			([BusinessId] ,[ItemGroupCode] ,[ItemGroupName] ,[ImagePath] ,[ItemCategoryId] ,[ItemBrandId] ,[Cost] ,[Price] ,[Uom] ,[IsForSales] ,[IsDisabled] ,[DateCreated] ,[DateModified] ,[CreatedBy] ,[UpdatedBy])
		SELECT 
			BusinessId = @TargetBusinessId,
			ItemGroupCode = itItem.ItemGroupCode, ItemGroupName = itItem.ItemGroupName, ImagePath = itItem.ImagePath,
			ItemCategoryId = dbo.fnGetItemCategoryId(@TargetBusinessId, itItem.ItemCategoryName),
			ItemBrandId = dbo.fnGetItemBrandId(@TargetBusinessId, itItem.ItemBrandName),
			Cost = itItem.Cost, Price = itItem.Price, Uom = itItem.Uom, IsForSales = itItem.IsForSales,
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemGroupId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName) = 0

		--COMMIT TRANSACTION;
		--BEGIN TRANSACTION
		
		INSERT INTO [dbo].[Item]
           ([ItemGroupId], [ItemName], [Barcode], [Quantity], [Cost], [Price], [Price02], [Price03], [Price04], [IsForSales], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
        SELECT
			ItemGroupId = dbo.fnGetItemGroupId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName),
			ItemName = itItem.ItemName,
			Barcode = itItem.Barcode, Quantity = 0, Cost = itItem.Cost,
			Price = itItem.Price, Price02 = 0, Price03 = 0, Price04 = 0,
			IsForSales = itItem.IsForSales, 
			IsDisabled = 0, DateCreated = @dateNow, DateModified = @dateNow, CreatedBy = @username, UpdatedBy = @username
		FROM @SubItems subItem
		INNER JOIN ItemTransferItem itItem
			ON itItem.TransferItemId = subItem.TransferItemId
		WHERE dbo.fnGetItemId(@TargetBusinessId, itItem.ItemGroupCode, itItem.ItemGroupName, itItem.ItemName) = 0

		--COMMIT TRANSACTION;

		-- ToDo : 2. Adjust Item.Quantity
		-- recrease the item quantity from source business
		UPDATE i
			SET i.Quantity = i.Quantity - itItem.Quantity
		FROM [Item] i
		INNER JOIN ItemTransferItem itItem
			ON itItem.ItemName = i.ItemName
		INNER JOIN @SubItems subItem
			ON subItem.TransferItemId = itItem.TransferItemId
		INNER JOIN ItemGroup ig
			ON ig.BusinessId = @SourceBusinessId
			AND ig.ItemGroupId = i.ItemGroupId

		-- increase the item quantity from target business
		UPDATE i
		SET i.Quantity = i.Quantity + itItem.Quantity
		FROM [Item] i
		INNER JOIN ItemTransferItem itItem
			ON itItem.ItemName = i.ItemName
		INNER JOIN @SubItems subItem
			ON subItem.TransferItemId = itItem.TransferItemId
		INNER JOIN ItemGroup ig
			ON ig.BusinessId = @TargetBusinessId
			AND ig.ItemGroupId = i.ItemGroupId
	END


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spItemTransferRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemTransferRetrieve]
    @TransferId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		it.*,
		b1.BusinessName AS SourceBusinessName,
		b2.BusinessName AS TargetBusinessName,
		itst.TransferStatusTypeName
	FROM [ItemTransfer] it
	INNER JOIN [Business] b1
		ON	it.SourceBusinessId = b1.BusinessId
	INNER JOIN [Business] b2
		ON	it.TargetBusinessId = b2.BusinessId
	INNER JOIN [ItemTransferStatusType] itst
		ON	it.TransferStatusTypeId = itst.TransferStatusTypeId
	WHERE	it.TransferId = @TransferId;

SET @ReturnValue = 0;
RETURN @ReturnValue;







GO
/****** Object:  StoredProcedure [dbo].[spItemTransferStatusListAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemTransferStatusListAllRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		TransferStatusTypeId AS Value,
		TransferStatusTypeName AS Text
	FROM [ItemTransferStatusType];

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spItemTransferStatusListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemTransferStatusListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		TransferStatusTypeId AS Value,
		TransferStatusTypeName AS Text
	FROM [ItemTransferStatusType]
	WHERE	IsForTransferItem = 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spItemTransferVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemTransferVoid]
	@TransferId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	SET NOCOUNT ON;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	UPDATE [dbo].[ItemTransfer]
	--SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	SET DateModified = GETDATE(), UpdatedBy = @username
	WHERE 
		TransferId = @TransferId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;










GO
/****** Object:  StoredProcedure [dbo].[spItemViewRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spItemViewRetrieve]
	@BusinessId BIGINT,
	@ItemId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT TOP 1 
		i.Quantity as NumRemaining,
		bi.Price as LastSalePrice
	FROM dbo.[Item] i
	LEFT JOIN BillItem bi
		ON bi.ItemId = i.ItemId
	INNER JOIN Bill b
		ON bi.BillId = b.BillId
	WHERE	i.ItemId = @ItemId
		AND bi.IsDisabled <> 1
		AND b.IsDisabled <> 1
	ORDER BY b.DateModified DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
/****** Object:  StoredProcedure [dbo].[spOutgoingPaymentAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spOutgoingPaymentAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select op.*, s.SupplierName
	from [OutgoingPayment]op
	left join [Supplier] s
		on op.SupplierId = s.SupplierId
	where	op.BusinessId = @BusinessId
		and	op.IsDisabled <> 1
		AND op.IsCashPurchase <> 1
	order by op.OutgoingPaymentId desc;

	/*
	SELECT op.*, sp.SupplierName AS PayeeName
	FROM [dbo].[OutgoingPayment] op
	INNER JOIN [dbo].[Supplier] sp
		ON op.SupplierId = sp.SupplierId
	WHERE op.BusinessId = @BusinessId
		AND op.IsDisabled <> 1
	ORDER BY op.OutgoingPaymentId DESC
	*/

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spOutgoingPaymentItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spOutgoingPaymentItemAllRetrieve]
	@OutgoingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		opi.*, 
		ro.ReceiveOrderNo, 
		ro.Total, 
		ro.Subtotal,
		ro.Tax,
		ro.AmountPaid,
		--(ro.Total - ro.AmountPaid) AS AmountDue
		ABS(ro.Subtotal - ro.AmountPaid) AS AmountDue
	FROM [dbo].[OutgoingPaymentItem] opi
	INNER JOIN [dbo].[ReceiveOrder] ro
		ON opi.ReceiveOrderId = ro.ReceiveOrderId
	WHERE	opi.OutgoingPaymentId = @OutgoingPaymentId
		AND opi.IsDisabled <> 1;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spOutgoingPaymentManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spOutgoingPaymentManage]
	@OutgoingPaymentId bigint,
	@BusinessId bigint,
    @OutgoingPaymentNo nvarchar(50),
    @SupplierId bigint,
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Detail1 nvarchar(100),
    @Detail2 nvarchar(100),
    @Detail3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @PaymentTypeId tinyint,
    @Note nvarchar(MAX),
    @TaxIdInfo nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
    --@Status varchar(16),
    --@IsVoided bit,
    --@IsDeleted bit,
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tOutgoingPaymentItem READONLY
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetOpId BIGINT;
		SET @targetOpId = @OutgoingPaymentId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextItemName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextItemName OUT;

			-- Insert 
			INSERT INTO [dbo].[OutgoingPayment] 
				([BusinessId], [OutgoingPaymentNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Detail1], [Detail2], [Detail3], [Phone], [Date], [PaymentTypeId], [Note], [TaxIdInfo], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@BusinessId, @nextItemName, 0, @SupplierId, @Reference, @ContactPerson, @Detail1, @Detail2, @Detail3, @Phone, @Date, @PaymentTypeId, @Note, @TaxIdInfo, @Subtotal, @Tax, @Total, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @targetOpId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[OutgoingPayment]
			SET    
			[SupplierId] = @SupplierId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, [Detail1] = @Detail1, [Detail2] = @Detail2, 
			[Detail3] = @Detail3, [Phone] = @Phone, [Date] = @Date, [PaymentTypeId] = @PaymentTypeId, [Note] = @Note, 
			[TaxIdInfo] = @TaxIdInfo, [Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[OutgoingPaymentId] = @OutgoingPaymentId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[OutgoingPayment]
			SET    
			[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[OutgoingPaymentId] = @OutgoingPaymentId
		END

	-- Manage subItems
	---- Insert Subitems
	INSERT INTO [dbo].[OutgoingPaymentItem] 
		([OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [Amount], [Description], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT 
		@targetOpId, ReceiveOrderId, SupplierInvoice, Amount, Description, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Subitems
	UPDATE opi
	SET	
		opi.SupplierInvoice = i.SupplierInvoice, 
		opi.ReceiveOrderId = i.ReceiveOrderId, 
		opi.Description = i.Description, 
		opi.Amount = i.Amount, 
		opi.DateModified = @dateNow, 
		opi.UpdatedBy = @username
	FROM [dbo].[OutgoingPaymentItem] AS opi
		INNER JOIN @Items AS i
		ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId
		WHERE i.ItemState = 'M'

	---- Disable Subitems
	UPDATE opi
	SET opi.IsDisabled = 1, opi.DateModified = @dateNow, opi.UpdatedBy = @username
	FROM [dbo].[OutgoingPaymentItem] AS opi
		INNER JOIN @Items AS i
		ON opi.OutgoingPaymentItemId = i.OutgoingPaymentItemId 
		WHERE i.ItemState = 'D'


	-- Update ReceiveOrder.Status of the ReceiveOrder item that this OutgoingPayment item refers to
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT DISTINCT ro.ReceiveOrderId
		FROM [ReceiveOrder] ro
		INNER JOIN [OutgoingPaymentItem] opi
			ON ro.ReceiveOrderId = opi.ReceiveOrderId
				AND ro.IsDisabled <> 1
				AND ro.Status <> 'Void'
				AND opi.IsDisabled <> 1
				AND opi.OutgoingPaymentId = @targetOpId;

	EXEC dbo.spReceiveOrderStatusUpdate @updateItemList;


COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spOutgoingPaymentRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spOutgoingPaymentRetrieve]
    @OutgoingPaymentId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		op.*, 
		s.SupplierName,
		pt.PaymentTypeName
	FROM [OutgoingPayment] op
	LEFT JOIN [Supplier] s
		ON op.SupplierId = s.SupplierId
	INNER JOIN [PaymentType] pt
		ON op.PaymentTypeId = pt.PaymentTypeId
	WHERE	op.OutgoingPaymentId = @OutgoingPaymentId;

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spOutgoingPaymentVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spOutgoingPaymentVoid]
	@OutgoingPaymentId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	SET NOCOUNT ON;

	-- 1. Check if this item is voidable
	DECLARE @blockingItem NVARCHAR(100);
		SET @blockingItem = dbo.fnIsOutgoingPaymentEditable(@OutgoingPaymentId);
	DECLARE @ErrTxt NVARCHAR(MAX);

	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId

	-- 2. VOID the item
	UPDATE [dbo].[OutgoingPayment]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE OutgoingPaymentId = @OutgoingPaymentId;


	-- 3. Update corresponding ReceiveOrder.Status and ReceiveOrder.AmountPaid
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT result.ReceiveOrderId FROM (
		SELECT DISTINCT ro.ReceiveOrderId
		FROM [ReceiveOrder] ro
		INNER JOIN [OutgoingPaymentItem] opi
			ON ro.ReceiveOrderId = opi.ReceiveOrderId
				AND ro.IsDisabled <> 1
				AND ro.Status <> 'Void'
		WHERE opi.OutgoingPaymentId = @OutgoingPaymentId
	) result;

	EXEC dbo.spReceiveOrderStatusUpdate @updateItemList;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spPaymentTypeListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPaymentTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select PaymentTypeId AS Value, PaymentTypeName AS Text
	from PaymentType

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spPendingBillAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPendingBillAllRetrieve]
	@BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		b.BillId, b.CustomerId, c.CustomerName, b.Total, b.DateModified,
		SUM(bi.Quantity) As Quantity
	FROM [Bill] b
	LEFT JOIN [BillItem] bi
		ON b.BillId = bi.BillId
	LEFT JOIN [Customer] c
		ON b.CustomerId = c.CustomerId
	WHERE	b.Status <> 'Done'
		AND b.IsDisabled <> 1
		AND bi.IsDisabled <> 1
		AND	b.BusinessId = @BusinessId
	GROUP BY b.BillId, b.CustomerId, c.CustomerName, b.Total, b.DateModified;

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
/****** Object:  StoredProcedure [dbo].[spPurchaseOrderAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurchaseOrderAllRetrieve]
    @BusinessId BIGINT,
	--@FilterColumn VARCHAR(50) = NULL,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50),
	--@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT,
	@MaxCount INT
	--@DateFrom Date = NULL,
	--@DateTo  Date = NULL
AS
	--SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	--DECLARE @sqlCommand AS NVARCHAR(MAX);
	--DECLARE @filterSql AS NVARCHAR(250);
	--DECLARE @sortingSql AS NVARCHAR(250);
	--DECLARE @OutputColumnSQL AS NVARCHAR(250);
	
	--DECLARE @TmpKeyword NVARCHAR(MAX);

	IF @FilterKeyword IS NOT NULL
		BEGIN
			SELECT @FilterKeyword = '%' + LTRIM(RTRIM(@FilterKeyword)) + '%';
		END

	DECLARE @TempTable TABLE 
	(
		PurchaseOrderId	BIGINT,
		PurchaseOrderNo NVarChar(100),
		Date Date,
		Status VarChar(10),
		SupplierName NVarChar(100),
		Total Decimal(18,4),
		DateCreated DateTime,
		DateModified DateTime
	);

	INSERT INTO 
		@TempTable
	SELECT
		po.PurchaseOrderId, po.PurchaseOrderNo, po.Date, po.Status, s.SupplierName, po.Total, po.DateCreated, po.DateModified
	FROM [PurchaseOrder] po
	LEFT JOIN [Supplier] s
		ON po.SupplierId = s.SupplierId
	INNER JOIN [PurchaseOrderItem] poi
		ON poi.PurchaseOrderId = po.PurchaseOrderId
	INNER JOIN [Item] item
		ON poi.ItemId = item.ItemId
	INNER JOIN [ItemGroup] itemGroup
		ON item.ItemGroupId = itemGroup.ItemGroupId
	WHERE	po.BusinessId = @BusinessId
		AND po.IsDisabled <> 1 
		AND po.IsCashPurchase <> 1
		AND (	(po.PurchaseOrderNo LIKE @FilterKeyword OR @FilterKeyword is NULL) OR 
				(s.SupplierName LIKE @FilterKeyword OR @FilterKeyword is NULL) OR 
				(item.ItemName LIKE @FilterKeyword OR @FilterKeyword IS NULL) OR
				(itemGroup.ItemGroupName LIKE @FilterKeyword OR @FilterKeyword IS NULL)
			)
	GROUP BY po.PurchaseOrderId, po.PurchaseOrderNo, po.Date, po.Status, s.SupplierName, po.Total, po.DateCreated, po.DateModified;
	
	SET @ReturnValue = @@ROWCOUNT;

	SELECT *
	FROM @TempTable
	ORDER BY 
		(
			CASE @SortingColumn
				WHEN 'DateCreated' THEN [DateCreated]
				WHEN 'DateModified' THEN [DateModified]
			END
		) DESC
	OFFSET @IndexStart ROWS
	FETCH NEXT @MaxCount ROWS ONLY;

	RETURN @ReturnValue;
	--ORDER BY po.PurchaseOrderId DESC;

	--SELECT
	--	po.PurchaseOrderId, po.PurchaseOrderNo, po.Date, po.Status, s.SupplierName,
	--	CASE 
	--		WHEN tmpSumPoi.PurchaseOrderId IS NULL THEN 0
	--		ELSE CONVERT( DECIMAL(18,4), tmpSumPoi.Amount )
	--	END AS Amount
	--FROM [PurchaseOrder] po
	--LEFT JOIN [Supplier] s
	--	ON po.SupplierId = s.SupplierId
	--LEFT JOIN (
	--	SELECT poi.PurchaseOrderId, SUM(poi.Quantity * poi.Price) as Amount
	--	FROM [PurchaseOrderItem] poi
	--	INNER JOIN [PurchaseOrder] po
	--		ON poi.PurchaseOrderId = po.PurchaseOrderId
	--	WHERE	po.BusinessId = @BusinessId
	--		AND poi.IsDisabled <> 1
	--	GROUP BY poi.PurchaseOrderId
	--) tmpSumPoi
	--	ON po.PurchaseOrderId = tmpSumPoi.PurchaseOrderId
	--WHERE	po.BusinessId = @BusinessId
	--	AND po.IsDisabled <> 1 
	

--DECLARE @SQLCommand AS NVARCHAR(750);
--DECLARE @FilterColumnSQL AS NVARCHAR(250);
--DECLARE @SortingSQL AS NVARCHAR(250);
--DECLARE @OutputColumnSQL AS NVARCHAR(250);

--SET @FilterColumnSQL = ' WHERE POITbl.BusinessId='+CONVERT(varchar(30),@BusinessId)

--SET @OutputColumnSQL =' POTbl.PurchaseOrderId,POTbl.PurchaseOrderNo,POTbl.Date ,POTbl.Status,POTbl.SupplierId,SPTbl.SupplierName '
--SET @OutputColumnSQL = @OutputColumnSQL + ' , ( SELECT SUM(Amount) FROM [dbo].[PurchaseOrderItem]  POITbl WITH (NOLOCK)  WHERE POTbl.PurchaseOrderId = POITbl.PurchaseOrderId )  As Amount '

--SET @OutputColumnSQL = @OutputColumnSQL + 	' FROM [dbo].[PurchaseOrder]  POTbl INNER JOIN [dbo].[Supplier] SPTbl ON POTbl.SupplierId=SPTbl.SupplierId '
--	-- Compose Filter Column 
--	IF ( @DateFrom IS NOT NULL)
--	BEGIN
--		SET @FilterColumnSQL = @FilterColumnSQL + ' AND ( POTbl.DateCreated BETWEEN CONVERT(datetime,'+ CONVERT(nvarchar(30),@DateFrom) +') AND CONVERT(datetime,'+ CONVERT(nvarchar(30),@DateTo) +') ) '
--	END

--	IF (@MaxCount > 0 ) 
--	BEGIN
--		SET @FilterColumnSQL = @FilterColumnSQL + ' AND (POTbl.ROWNUMBERS BETWEEN ' + CONVERT(nvarchar(30),@IndexStart) + ' AND ' + CONVERT(nvarchar(30),@MaxCount) + ' ) '
--	END

--	IF ( @FilterColumn IS NOT NULL)
--	BEGIN
--		SET @FilterColumnSQL = @FilterColumnSQL + ' AND '+ @FilterColumn +' LIKE ' +@FilterKeyword
--	END	
	
	
--	-- Compose Sorting 
--	IF (@SortingColumn IS NOT NULL)
--	BEGIN
--		SET @SortingSQL =' ORDER BY ' + @SortingColumn + @SortingDirection;
--	END 

--	SET @SQLCommand = 'SELECT ' + @OutputColumnSQL + @FilterColumnSQL + @SortingSQL

--	EXEC (@SQLCommand)








	---- 1. Select base PO
	--SELECT 
	--	*, 
	--	CONVERT(DECIMAL(18,4), 0) AS Quantity, 
	--	CONVERT(DECIMAL(18,4), 0) AS QuantityDue, 
	--	CONVERT(VARCHAR(16), 'New') AS Status2 INTO #tmpPo
	--FROM [PurchaseOrder]
	--WHERE BusinessId = @BusinessId
	--	AND IsDeleted <> 1
	--ORDER BY Id DESC

	---- 2. Select base POI
	--SELECT 
	--	PurchaseOrderId, 
	--	CONVERT(DECIMAL(18,4), Sum(Quantity)) AS Quantity, 
	--	CONVERT(DECIMAL(18,4), 0) AS QuantityReceived, 
	--	CONVERT(VARCHAR(16), 'New') AS Status2 INTO #tmpPoi
	--FROM [PurchaseOrderItem]
	--WHERE BusinessId = @BusinessId
	--GROUP BY PurchaseOrderId

	---- 3. Select Sum(QuantityReceived) from ROI
	--SELECT 
	--	roi.PurchaseOrderId, 
	--	CONVERT(DECIMAL(18,4), SUM(roi.Quantity)) AS QuantityReceived INTO #tmpRoiAmt
	--FROM [ReceiveOrderItem] roi
	--INNER JOIN [ReceiveOrder] ro
	--	ON roi.ReceiveOrderId = ro.Id
	--WHERE roi.BusinessId = @BusinessId
	--	AND ro.IsVoided <> 1
	--	AND roi.IsDeleted <> 1
	--GROUP BY roi.PurchaseOrderId;

	---- 4. Update tmpPoi with Sum(QuantityReceived) from tmpRoiAmt
	--UPDATE #tmpPoi
	--SET
	--	QuantityReceived = roi.QuantityReceived
	--FROM #tmpRoiAmt roi
	--WHERE #tmpPoi.PurchaseOrderId = roi.PurchaseOrderId

	---- 5. Update tmpPo
	--UPDATE #tmpPo
	--SET 
	--	Quantity = poiAmt.Quantity,
	--	QuantityDue = (poiAmt.Quantity - poiAmt.QuantityReceived)
	--FROM #tmpPoi poiAmt
	--WHERE #tmpPo.Id = poiAmt.PurchaseOrderId

	---- 6. Update Status2
	--UPDATE #tmpPo
	--SET
	--	STATUS2 = 
	--		CASE 
	--			WHEN (IsVoided = 1) THEN 'Void'
	--			WHEN (QuantityDue <= 0) THEN 'Done'
	--			WHEN (QuantityDue >= Quantity) THEN 'New'
	--			ELSE 'Partial'
	--		END;


	--SELECT * FROM #tmpPo;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
/****** Object:  StoredProcedure [dbo].[spPurchaseOrderItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurchaseOrderItemAllRetrieve]
	@PurchaseOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT 
		poi.*, 
		dbo.fnGetItemFullName( ig.ItemGroupCode, ig.ItemGroupName, i.ItemName ) AS ItemFullName,
		ig.Uom, 
		(poi.Quantity * poi.Price) AS Amount
	FROM PurchaseOrder po
	INNER JOIN PurchaseOrderItem poi
		ON poi.PurchaseOrderId = po.PurchaseOrderId
	INNER JOIN Item i
		ON poi.ItemId = i.ItemId
	INNER JOIN ItemGroup ig
		ON i.ItemGroupId = ig.ItemGroupId
	WHERE
		po.PurchaseOrderId = @PurchaseOrderId;


SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spPurchaseOrderListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurchaseOrderListRetrieve]
    @BusinessId BIGINT,
	@SupplierId BIGINT = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT
		po.PurchaseOrderId AS Value,
		po.PurchaseOrderNo As Text
	FROM [PurchaseOrder] po
	WHERE BusinessId = @BusinessId
		AND (po.Status = 'New' OR po.Status = 'Partial')
		AND IsDisabled <> 1
		AND po.SupplierId = @SupplierId --Retrieve according to supplied SupplierId

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spPurchaseOrderManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurchaseOrderManage]
	@PurchaseOrderId bigint,
	@BusinessId bigint,
    @IsCashPurchase bit,
    @SupplierId bigint,
    --@SupplierName nvarchar(100),
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @DateDelivery date,
    @AmountTypeId tinyint,
    @PaymentTypeId tinyint,
    @Note nvarchar(MAX),
    @CreditTerm nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
    --@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tPurchaseOrderItem READONLY
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetPoId BIGINT;
		SET @targetPoId = @PurchaseOrderId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	DECLARE @updateItemList tIdTable;

	-- 1. Manage [PurchaseOrder] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextPoName NVARCHAR(50);
				SET @nextPoName = '';
			DECLARE @nextCpName NVARCHAR(50);
				SET @nextCpName = '';
			
			IF @IsCashPurchase = 1
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'CashPurchase', @RetDocName = @nextCpName OUT;
				END
			ELSE
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'PurchaseOrder', @RetDocName = @nextPoName OUT;
				END

			-- Insert Purchase Order
			INSERT INTO [dbo].[PurchaseOrder] 
				([BusinessId], [PurchaseOrderNo], [IsCashPurchase], [CashPurchaseNo], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [AmountTypeId], [PaymentTypeId], [Note], [CreditTerm], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextPoName, @IsCashPurchase, @nextCpName, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, @DateDelivery, @AmountTypeId, @PaymentTypeId, @Note, @CreditTerm, @Subtotal, @Tax, @Total, 
				CASE /* Status */
					WHEN @IsCashPurchase = 1 THEN 'Done'
					ELSE @status
				END, 
				0, @dateNow, @dateNow, @username, @username
			SET @targetPoId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @blockingItem = dbo.fnIsPurchaseOrderEditable(@PurchaseOrderId);

			IF @blockingItem <> N''
				BEGIN
					SET @ErrTxt = N'The opearation is blocked by ' + @blockingItem;
					RAISERROR( @ErrTxt, 16, 1 );
				END

			UPDATE [dbo].[PurchaseOrder]
			SET    
			[SupplierId] = @SupplierId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, 
			[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, [Phone] = @Phone, [Date] = @Date, [DateDelivery] = @DateDelivery, 
			[AmountTypeId] = @AmountTypeId, [PaymentTypeId] = @PaymentTypeId, [Note] = @Note, [CreditTerm] = @CreditTerm, [Subtotal] = @Subtotal, 
			[Tax] = @Tax, [Total] = @Total,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[PurchaseOrderId] = @PurchaseOrderId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[PurchaseOrder]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[PurchaseOrderId] = @PurchaseOrderId
		END


	-- 2. Manage SubItems
	---- Insert Purchase Order's items
	INSERT INTO [dbo].[PurchaseOrderItem] ([PurchaseOrderId], [ItemId], [Quantity], [QuantityReceived], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT PurchaseOrderId = @targetPoId, ItemId, Quantity, 0, Price, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Purchase Order's items
	UPDATE poi
	SET poi.ItemId = i.ItemId, poi.Quantity = i.Quantity,
		poi.Price = i.Price, poi.DateModified = @dateNow, poi.UpdatedBy = @username
	FROM [dbo].[PurchaseOrderItem] AS poi
		INNER JOIN @Items AS i
		ON poi.PurchaseOrderItemId = i.PurchaseOrderItemId 
		WHERE i.ItemState = 'M'

	---- Disable Purchase Order's items
	UPDATE poi
	SET poi.IsDisabled = 1, poi.DateModified = @dateNow, poi.UpdatedBy = @username
	FROM [dbo].[PurchaseOrderItem] AS poi
		INNER JOIN @Items AS i
		ON poi.PurchaseOrderItemId = i.PurchaseOrderItemId 
		WHERE i.ItemState = 'D'


	-- 3. Re-calculate PurchaseOrder.Status again in case PurchaseOrderItem.Quantity changes
	IF @ItemState = 'M'
		BEGIN
			INSERT INTO @updateItemList
			SELECT po.PurchaseOrderId FROM [PurchaseOrder] po WHERE po.PurchaseOrderId = @targetPoId;

			EXEC dbo.spPurchaseOrderStatusUpdate @updateItemList;
		END
		

	-- 4. In case of IsCasePurchase=1, we have to create ReceiveOrder and OutgoingPayment items associated to this PurchaseOrder as well
	IF @IsCashPurchase = 1
	BEGIN
		IF @ItemState = 'N'
		BEGIN
		
			-- Create ReceiveOrder item
			--DECLARE @nextItemName NVARCHAR(50);
			DECLARE @newRoId BIGINT;
			DECLARE @newOpId BIGINT;
			--EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'ReceiveOrder', @RetDocName = @nextItemName OUT;

			-- Insert ReceiveOrder
			INSERT INTO [dbo].[ReceiveOrder] 
				([BusinessId], [ReceiveOrderNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextCpName, 1, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, @Note, N'', @Subtotal, @Tax, @Total, 0.00, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @newRoId = SCOPE_IDENTITY();

			INSERT INTO [ReceiveOrderItem]
				([ReceiveOrderId], [PurchaseOrderItemId], [ItemId], [Quantity], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@newRoId, poi.PurchaseOrderItemId, poi.ItemId, poi.Quantity, poi.Price, 0, @dateNow, @dateNow, @username, @username
			FROM [PurchaseOrderItem] poi
			WHERE	poi.PurchaseOrderId = @targetPoId;

			-- Update Item Quantity in the Item table
			UPDATE [Item]
			SET [Item].Quantity = ([Item].Quantity + newItems.Quantity)
			FROM 
				( 
					SELECT roi.Quantity, roi.ItemId
					FROM [ReceiveOrderItem] roi
					INNER JOIN [PurchaseOrderItem] poi
						ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
					INNER JOIN [PurchaseOrder] po
						ON poi.PurchaseOrderId = po.PurchaseOrderId
					WHERE	po.PurchaseOrderId = @targetPoId
				) newItems
			WHERE	[Item].ItemId = newItems.ItemId;

			-- Create OutgoingPayment item
			--EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextItemName OUT;

			INSERT INTO [dbo].[OutgoingPayment] 
				([BusinessId], [OutgoingPaymentNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Detail1], [Detail2], [Detail3], [Phone], [Date], [PaymentTypeId], [Note], [TaxIdInfo], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextCpName, 1, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, 1 /*Cash*/, @Note, N'', @Subtotal, @Tax, @Total, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @newOpId = SCOPE_IDENTITY();

			INSERT INTO [dbo].[OutgoingPaymentItem] 
				([OutgoingPaymentId], [ReceiveOrderId], [SupplierInvoice], [Amount], [Description], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@newOpId, @newRoId, N'', @Total, N'', 0, @dateNow, @dateNow, @username, @username;

		END
	END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spPurchaseOrderRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurchaseOrderRetrieve]
    @PurchaseOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT po.*, s.SupplierName, pt.PaymentTypeName, at.AmountTypeName
	FROM [PurchaseOrder] po
	LEFT JOIN [Supplier] s
		ON po.SupplierId = s.SupplierId
	INNER JOIN [PaymentType] pt
		ON po.PaymentTypeId = pt.PaymentTypeId
	LEFT JOIN [AmountType] at
		ON po.AmountTypeId = at.AmountTypeId
	WHERE
		po.PurchaseOrderId = @PurchaseOrderId;
	
SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spPurchaseOrderStatusUpdate]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurchaseOrderStatusUpdate]
	@IdTable tIdTable READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	SELECT 
		po.PurchaseOrderId, po.Status
	INTO #Temp 
	FROM @IdTable ai
	INNER JOIN [PurchaseOrder] po
	ON ai.Id = po.PurchaseOrderId
		AND po.IsDisabled <> 1
		AND po.Status <> 'Void';

	IF EXISTS ( SELECT TOP 1 * FROM #Temp ) 
		BEGIN
			--DECLARE @qtyOrdered DECIMAL (18 ,4);
			--	SET @qtyOrdered = 0;
			--DECLARE @qtyReceived DECIMAL (18, 4);
			--	SET @qtyReceived = 0;	

			-- Update QuantityReceived in PurchaseOrderItem table
			UPDATE [PurchaseOrderItem]
			SET 
				QuantityReceived = 
					CASE 
						WHEN ret.QuantityReceived IS NULL THEN 0
						ELSE ret.QuantityReceived
					END
			FROM 
			(
				SELECT 
					poi.PurchaseOrderItemId, 
					SUM(result.Quantity) AS QuantityReceived
				FROM #Temp tmp
				INNER JOIN [PurchaseOrderItem] poi
					ON tmp.PurchaseOrderId = poi.PurchaseOrderId
						AND poi.IsDisabled <> 1
				LEFT JOIN (
					SELECT 
						roi.PurchaseOrderItemId,
						roi.Quantity
					FROM [ReceiveOrderItem] roi
					INNER JOIN [ReceiveOrder] ro
						ON roi.ReceiveOrderId = ro.ReceiveOrderId
							AND ro.IsDisabled <> 1
							AND ro.Status <> 'Void'
							AND roi.IsDisabled <> 1
				) result
					ON poi.PurchaseOrderItemId = result.PurchaseOrderItemId
				GROUP BY poi.PurchaseOrderItemId
			) ret
			WHERE [PurchaseOrderItem].PurchaseOrderItemId = ret.PurchaseOrderItemId;


			UPDATE [PurchaseOrder]
			SET 
				Status = CASE 
							WHEN (result.NumAllRow IS NULL OR result.NumDoneRow IS NULL) THEN 'New'
							WHEN result.NumDoneRow >= result.NumAllRow THEN 'Done'
							ELSE 'Partial'
						 END
			FROM 
			(
				SELECT 
					po.PurchaseOrderId,
					groupAllSubItem.NumRow AS NumAllRow,
					groupDoneSubItem.NumRow AS NumDoneRow
				FROM [PurchaseOrder] po
				INNER JOIN #Temp tmp
					ON po.PurchaseOrderId = tmp.PurchaseOrderId
				LEFT JOIN
				(
					SELECT
						po.PurchaseOrderId,
						COUNT(*) AS NumRow
					FROM [PurchaseOrder] po
					INNER JOIN [PurchaseOrderItem] poi
						ON	po.PurchaseOrderId = poi.PurchaseOrderId
							AND po.IsDisabled <> 1
							AND po.Status <> 'Void'
							AND poi.IsDisabled <> 1
					GROUP BY po.PurchaseOrderId
				) groupAllSubItem
					ON po.PurchaseOrderId = groupAllSubItem.PurchaseOrderId
				LEFT JOIN
				(
					SELECT 
						poi.PurchaseOrderId,
						COUNT(*) AS NumRow
					FROM [PurchaseOrderItem] poi
					WHERE	poi.IsDisabled <> 1
							AND poi.QuantityReceived >= poi.Quantity
					GROUP BY poi.PurchaseOrderId
				) groupDoneSubItem
					ON po.PurchaseOrderId = groupDoneSubItem.PurchaseOrderId

			) result
			WHERE [PurchaseOrder].PurchaseOrderId = result.PurchaseOrderId;

		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spPurchaseOrderVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurchaseOrderVoid]
	@PurchaseOrderId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	SET NOCOUNT ON;

	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	/* Return error if this PO is already referred by any RO items */
	DECLARE @roNo VARCHAR(50);

	--SELECT @roNo = ro.ReceiveOrderNo
	--	FROM [ReceiveOrder] ro
	--	INNER JOIN [ReceiveOrderItem] roi
	--		ON ro.ReceiveOrderId = roi.ReceiveOrderId
	--	INNER JOIN [PurchaseOrderItem] poi
	--		ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
	--	WHERE	roi.PurchaseOrderItemId = @PurchaseOrderId
	--		AND	ro.IsDisabled <> 1
	--		AND roi.IsDisabled <> 1

	SET @roNo = dbo.fnIsPurchaseOrderEditable( @PurchaseOrderId );

	if @roNo <> N''
	begin
		raiserror (N'Could not VOID the item because it is already referred to by %s', -- Message text.
               16, -- Severity.
               1, -- State.
               @roNo);
	end

	UPDATE [dbo].[PurchaseOrder]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE 
		PurchaseOrderId = @PurchaseOrderId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;













GO
/****** Object:  StoredProcedure [dbo].[spReceiveItemTransferAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveItemTransferAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @sqlCommand AS NVARCHAR(MAX);
	DECLARE @filterSql AS NVARCHAR(250);
	DECLARE @sortingSql AS NVARCHAR(250);
	DECLARE @OutputColumnSQL AS NVARCHAR(250);

	SELECT 
		it.TransferId,
		it.TransferName,
		it.DateSent
		--it.Status
	FROM [ItemTransfer] it
	WHERE	it.TargetBusinessId = @BusinessId
		AND it.IsDisabled <> 1
	ORDER BY it.TransferId DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spReceiveItemTransferStatusListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveItemTransferStatusListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		TransferStatusTypeId AS Value,
		TransferStatusTypeName AS Text
	FROM [ItemTransferStatusType]
	WHERE	IsForReceiveTransferItem = 1;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spReceiveOrderAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveOrderAllRetrieve]
    @BusinessId BIGINT,
	@FilterKeyword NVARCHAR(MAX) = NULL,
	@SortingColumn VARCHAR(50) = NULL,
	@SortingDirection VARCHAR(50) = NULL,
	@IndexStart INT = NULL,
	@MaxCount INT = NULL,
	@DateFrom Date = NULL,
	@DateTo  Date = NULL
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select ro.*, s.SupplierName
	from [ReceiveOrder] ro
	left join [Supplier] s
		on ro.SupplierId = s.SupplierId
	where	ro.BusinessId = @BusinessId
		and ro.IsDisabled <> 1
		AND ro.IsCashPurchase <> 1
	order by ro.ReceiveOrderNo desc;

	---- 1. Select base RO
	--SELECT *, AmountDue = Total, CONVERT(VARCHAR(16), 'New') AS Status2 INTO #tmpRo
	--FROM [ReceiveOrder]
	--WHERE BusinessId = @BusinessId
	--	AND IsDisabled <> 1
	--ORDER BY ReceiveOrderId DESC

	---- 2. Select Sum(AmountPay) from OPI
	--SELECT opi.ReceiveOrderId, SUM(opi.Amount) AS AmountPay
	--INTO #tmpOpiAmt
	--FROM OutgoingPaymentItem opi
	--INNER JOIN OutgoingPayment op
	--	ON op.OutgoingPaymentId = opi.OutgoingPaymentId
	--WHERE 
	--		opi.IsDisabled <> 1
	--	AND op.BusinessId = @BusinessId
	--GROUP BY opi.ReceiveOrderId;

	---- 3. Update tmpRo with Sum(AmountPay) from tmpOpiAmt
	--UPDATE #tmpRo
	--SET 
	--	AmountDue = (Total - opi.AmountPay)
	--FROM #tmpOpiAmt opi
	--WHERE #tmpRo.ReceiveOrderId = opi.ReceiveOrderId;

	---- 4. Update Status2
	--UPDATE #tmpRo
	--SET
	--	Status2 = 
	--		CASE 
	--			WHEN AmountDue <= 0 THEN 'Done'
	--			WHEN AmountDue >= Total THEN 'New'
	--			ELSE 'Partial'
	--		END;

	--SELECT * FROM #tmpRo

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spReceiveOrderItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveOrderItemAllRetrieve]
	@ReceiveOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT 
		roi.*,
		CASE 
			WHEN roi.PurchaseOrderItemId IS NULL THEN N''
			ELSE po.PurchaseOrderNo
		END AS PurchaseOrderNo,
		(roi.Quantity * roi.Price) AS Amount, 
		dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName,
		CASE
			WHEN roi.PurchaseOrderItemId IS NULL THEN 0
			ELSE poi.Quantity
		END AS QuantityOrdered,
		ig.Uom
	FROM [dbo].[ReceiveOrderItem] roi
		INNER JOIN [dbo].[Item] i
			ON roi.ItemId = i.ItemId
		INNER JOIN [dbo].[ItemGroup] ig
			ON i.ItemGroupId = ig.ItemGroupId
		LEFT JOIN [dbo].[PurchaseOrderItem] poi
			ON roi.PurchaseOrderItemId = poi.PurchaseOrderItemId
		LEFT JOIN [dbo].[PurchaseOrder] po
			ON poi.PurchaseOrderId = po.PurchaseOrderId
	WHERE	roi.ReceiveOrderId = @ReceiveOrderId
		AND roi.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spReceiveOrderListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveOrderListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT
		ro.ReceiveOrderId AS Value,
		ro.ReceiveOrderNo As Text
	FROM [ReceiveOrder] ro
	WHERE BusinessId = @BusinessId
		AND (ro.Status = 'New' OR ro.Status = 'Partial')
		AND IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spReceiveOrderManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveOrderManage]
	@ReceiveOrderId bigint,
	@BusinessId bigint,
    @SupplierId bigint,
    --@SupplierName nvarchar(100),
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @Note nvarchar(MAX),
    @PaymentTerm nvarchar(MAX),
    @Subtotal decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
    --@Status varchar(50),
    --@IsVoided bit,
    --@IsDeleted bit,
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tReceiveOrderItem READONLY
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetRoId BIGINT;
		SET @targetRoId = @ReceiveOrderId;
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;
	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	DECLARE @updateItemList tIdTable;


	-- 1. Manage records in [ReceiveOrder] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextItemName NVARCHAR(50);
			EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'ReceiveOrder', @RetDocName = @nextItemName OUT;

			-- Insert ReceiveOrder
			INSERT INTO [dbo].[ReceiveOrder] 
				([BusinessId], [ReceiveOrderNo], [IsCashPurchase], [SupplierId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], 
				[Phone], [Date], [Note], [PaymentTerm], [Subtotal], [Tax], [Total], [AmountPaid], [Status], [IsDisabled],
				[DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@BusinessId, @nextItemName, 0, @SupplierId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, 
				@Phone, @Date, @Note, @PaymentTerm, @Subtotal, @Tax, @Total, 0.00, @status, 0, 
				@dateNow, @dateNow, @username, @username

			SET @targetRoId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @blockingItem = dbo.fnIsReceiveOrderEditable(@ReceiveOrderId);

			IF @blockingItem <> N''
				BEGIN
					SET @ErrTxt = N'The opearation is blocked by ' + @blockingItem;
					RAISERROR( @ErrTxt, 16, 1 );
				END

			UPDATE [dbo].[ReceiveOrder]
			SET
				[SupplierId] = @SupplierId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, 
				[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, 
				[Phone] = @Phone, [Date] = @Date, [Note] = @Note, [PaymentTerm] = @PaymentTerm, 
				[Subtotal] = @Subtotal, [Tax] = @Tax, [Total] = @Total,
				[DateCreated] = @dateNow, [DateModified] = @dateNow, [CreatedBy] = @username, [UpdatedBy] = @username
			WHERE	[ReceiveOrderId] = @ReceiveOrderId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[ReceiveOrder]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[ReceiveOrderId] = @ReceiveOrderId
		END


	-- 2. Revert [Item].Quantity for [ReceiveOrderItem] records in case they are modified. We will update with new
	--    ReceiveOrderItem.Quantity below
	UPDATE [Item]
	SET
		[Item].Quantity = ([Item].Quantity - subItems.Quantity)
	FROM @Items subItems
	WHERE	[Item].ItemId = subItems.ItemId
		AND (subItems.ItemState = 'M' OR subItems.ItemState = 'D');


	-- 3. Manage SubItems
	---- Insert Subitems
	INSERT INTO [dbo].[ReceiveOrderItem] 
		([ReceiveOrderId], [PurchaseOrderItemId], [ItemId], [Quantity], [Price], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT 
		@targetRoId, i.PurchaseOrderItemId, i.ItemId, i.Quantity, i.Price, 0, @dateNow, @dateNow, @username, @username
		FROM @Items i
		WHERE ItemState = 'N';

	---- Update Subitems
	UPDATE [ReceiveOrderItem]
	SET
		ItemId = subItems.ItemId, Quantity = subItems.Quantity, Price = subItems.Price,
		DateModified = @dateNow, UpdatedBy = @username
	FROM [dbo].[ReceiveOrderItem] roi
		INNER JOIN @Items subItems
			ON roi.ItemId = subItems.ItemId
		WHERE subitems.ItemState = 'M'

	---- Disable Subitems
	UPDATE [ReceiveOrderItem]
	SET 
		IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @username
	FROM [dbo].[ReceiveOrderItem] roi
		INNER JOIN @Items subItems
			ON roi.ItemId = subItems.ItemId 
		WHERE subItems.ItemState = 'D';

	-- Disable this because there is no way that the change of ReceiveOrderItem can cause ReceiveOrder.Status to change
	---- 4. Re-calculate the ReceiveOrder.Status again in case that the ReceiveOrderItem item is changed
	--IF @ItemState = 'M'
	--	BEGIN
	--		INSERT INTO @updateItemList
	--		SELECT ro.ReceiveOrderId FROM [ReceiveOrder] ro WHERE ro.ReceiveOrderId = @targetRoId;

	--		EXEC dbo.spReceiveOrderStatusUpdate @updateItemList;
	--	END

	-- 5. Re-calculate the PurchaseOrder.Status of the PurchaseOrder items that are refered to by this ReceiveOrder item
	DELETE FROM @updateItemList;

	INSERT INTO @updateItemList
	SELECT DISTINCT po.PurchaseOrderId AS Id
	FROM [PurchaseOrder] po
	INNER JOIN [PurchaseOrderItem] poi
		ON	po.PurchaseOrderId = poi.PurchaseOrderId
			AND po.IsDisabled <> 1
			AND po.Status <> 'Void'
			AND poi.IsDisabled <> 1
	INNER JOIN [ReceiveOrderItem] roi
		ON	poi.PurchaseOrderItemId = roi.PurchaseOrderItemId
			AND roi.IsDisabled <> 1
			AND roi.ReceiveOrderId = @targetRoId;

	EXEC dbo.spPurchaseOrderStatusUpdate @updateItemList;
		

	-- 6. Update Item.Quantity to reflect SubItems with ItemState = 'N' or ItemState = 'M'
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + roi.Quantity
	FROM [ReceiveOrderItem] roi
	INNER JOIN @Items i
		ON roi.ReceiveOrderItemId = i.ReceiveOrderItemId
			AND (i.ItemState = 'N' OR i.ItemState = 'M')
	WHERE	roi.IsDisabled <> 1
		AND roi.ReceiveOrderId = @targetRoId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spReceiveOrderRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveOrderRetrieve]
    @ReceiveOrderId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	SELECT ro.*, s.SupplierName
	FROM [ReceiveOrder] ro
	LEFT JOIN [Supplier] s
		ON ro.SupplierId = s.SupplierId
	WHERE	ro.ReceiveOrderId = @ReceiveOrderId;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spReceiveOrderStatusUpdate]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveOrderStatusUpdate]
	@IdTable tIdTable READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	UPDATE [ReceiveOrder]
	SET
		AmountPaid =
			CASE 
				WHEN result.AmountPaid IS NULL THEN 0
				ELSE result.AmountPaid
			END,
		Status = 
			CASE 
				WHEN result.AmountPaid IS NULL OR result.AmountPaid = 0 THEN 'New'
				--WHEN result.AmountPaid >= result.Total THEN 'Done'
				WHEN result.AmountPaid >= result.Subtotal THEN 'Done'
				ELSE 'Partial'
			END
	FROM 
	(
		SELECT 
			ro.ReceiveOrderId,
			ro.Total,
			ro.Subtotal,
			SUM(result.Amount) AS AmountPaid
		FROM @IdTable it
		INNER JOIN [ReceiveOrder] ro
			ON it.Id = ro.ReceiveOrderId
		INNER JOIN [ReceiveOrderItem] roi
			ON ro.ReceiveOrderId = roi.ReceiveOrderId
				AND ro.IsDisabled <> 1
				AND ro.Status <> 'Void'
				AND roi.IsDisabled <> 1
		LEFT JOIN (
			SELECT 
				opi.ReceiveOrderId,
				opi.Amount
			FROM [OutgoingPaymentItem] opi
			INNER JOIN [OutgoingPayment] op
				ON opi.OutgoingPaymentId = op.OutgoingPaymentId
					AND op.IsDisabled <> 1
					AND op.Status <> 'Void'
					AND opi.IsDisabled <> 1
		) result
			ON ro.ReceiveOrderId = result.ReceiveOrderId
		GROUP BY ro.ReceiveOrderId, ro.Total, ro.Subtotal
	) result
	WHERE 
		[ReceiveOrder].ReceiveOrderId = result.ReceiveOrderId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spReceiveOrderVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReceiveOrderVoid]
	@ReceiveOrderId BIGINT,
	@UserId BIGINT
AS

DECLARE @ReturnValue INT;

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	SET @blockingItem = dbo.fnIsReceiveOrderEditable(@ReceiveOrderId);
	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	UPDATE [dbo].[ReceiveOrder]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE ReceiveOrderId = @ReceiveOrderId;

	-- Update corresponding PurchaseOrder.Status and PurchaseOrderItem.QuantityReceived
	DECLARE @updateItemList tIdTable;

	INSERT INTO @updateItemList
	SELECT DISTINCT result.PurchaseOrderId AS Id FROM (
		SELECT DISTINCT po.PurchaseOrderId
			FROM [PurchaseOrder] po
			INNER JOIN [PurchaseOrderItem] poi
				ON po.PurchaseOrderId = poi.PurchaseOrderId
					AND po.IsDisabled <> 1
					AND po.Status <> 'Void'
					AND poi.IsDisabled <> 1
			INNER JOIN [ReceiveOrderItem] roi
				ON poi.PurchaseOrderItemId = roi.PurchaseOrderItemId
					AND roi.ReceiveOrderId = @ReceiveOrderId
	) result;

	EXEC dbo.spPurchaseOrderStatusUpdate @updateItemList;


	-- Update Item.Quantity
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity - roi.Quantity
	FROM [ReceiveOrderItem] roi
	WHERE	roi.IsDisabled <> 1
		AND roi.ReceiveOrderId = @ReceiveOrderId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spReportOutstandingRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReportOutstandingRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @NumPendingPaymentSalesInvoice INT;
	DECLARE @NumOverdueSalesInvoice INT;
	DECLARE @NumItemToReorder INT;
	DECLARE @NumPendingReceivingPurchaseOrder INT;
	DECLARE @NumOverduePurchaseOrder INT;
	DECLARE @NumPendingPaymentReceiveOrder INT;
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	-- 1. Pending Sales Invoice
	SELECT 
		@NumPendingPaymentSalesInvoice = Count(si.SalesInvoiceId)
	FROM SalesInvoice si
	WHERE	si.Status = 'New'
		AND	si.BusinessId = @BusinessId;

	-- 2. Overdue Sales Invoice
	--Overdue Sales Invoice = SI with TodayDate > SI.Date+30
	SELECT 
		@NumOverdueSalesInvoice = Count(si.SalesInvoiceId) 
	FROM SalesInvoice si
	WHERE	@dateNow > DATEADD(DAY, 30, si.Date)
		AND si.Status = 'New'
		AND	si.BusinessId = @BusinessId;

	-- 3. Re-order Item
	SELECT 
		@NumItemToReorder = Count(i.ItemId) 
	FROM Item i
	INNER JOIN ItemGroup ig
		ON i.ItemGroupId = ig.ItemGroupId
	INNER JOIN ItemRestocking istock
		ON i.ItemId = istock.ItemId
	WHERE	i.Quantity < istock.Threshold
		AND	ig.BusinessId = @BusinessId;
	
	
	-- 4. Purchase Order that has not been received yet
	SELECT 
		@NumPendingReceivingPurchaseOrder = Count(po.PurchaseOrderId)
	FROM PurchaseOrder po
	WHERE	po.Status = 'New'
		AND	po.BusinessId = @BusinessId;

	-- 5. Overdue Purchase Order = PO with TodayDate > PO.DeliveryDate
	SELECT 
		@NumOverduePurchaseOrder = Count(po.PurchaseOrderId)
	FROM PurchaseOrder po
	WHERE	@dateNow > po.DateDelivery
		AND	po.BusinessId = @BusinessId;

	-- 6. Receive Order that has not been paid yet
	SELECT 
		@NumPendingPaymentReceiveOrder = Count(ro.ReceiveOrderId)
	FROM ReceiveOrder ro
	WHERE	ro.Status = 'New'
		AND	ro.BusinessId = @BusinessId;

	SELECT
		@NumPendingPaymentSalesInvoice AS PendingPaymentSalesInvoiceCount,
		@NumOverdueSalesInvoice AS OverdueSalesInvoiceCount,
		@NumItemToReorder AS ItemToReorderCount,
		@NumPendingReceivingPurchaseOrder AS PendingReceivingPurchaseOrderCount,
		@NumOverduePurchaseOrder AS OverduePurchaseOrderCount,
		@NumPendingPaymentReceiveOrder AS PendingPaymentReceiveOrderCount;

DECLARE @ReturnValue INT;
SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
/****** Object:  StoredProcedure [dbo].[spReportTopItemsRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReportTopItemsRetrieve]
    @BusinessId BIGINT,
	@NumItem INT,
	@NumHistoryRange INT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	
	DECLARE @dateNow DATE;
	SET @dateNow = GETDATE();

	SELECT TOP(@NumItem) 
		i.ItemId, 
		dbo.fnGetItemFullName( ig.ItemGroupCode, ig.ItemGroupName, i.ItemName ) AS ItemFullName, 
		SUM(bi.Quantity * bi.Price) AS Amount
	FROM dbo.Item i
	INNER JOIN ItemGroup ig
		ON i.ItemGroupId = ig.ItemGroupId
	INNER JOIN BillItem bi
		ON bi.ItemId = i.ItemId
	INNER JOIN Bill b
		ON bi.BillId = b.BillId
	WHERE	ig.BusinessId = @BusinessId
		AND	b.Status = 'Done'
		AND b.DateCreated > DATEADD( DAY, -@NumHistoryRange, @dateNow)
	GROUP BY i.ItemId, i.ItemName, ig.ItemGroupCode, ig.ItemGroupName
	ORDER BY Amount DESC;

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
/****** Object:  StoredProcedure [dbo].[spRoleListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRoleListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		BusinessRoleShortName as Value,
		BusinessRoleName as Text
	FROM [System_BusinessRole];

SET @ReturnValue = 0;
RETURN @ReturnValue;


GO
/****** Object:  StoredProcedure [dbo].[spRolesManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRolesManage]
	@BusinessId bigint,
	@RolesColumnName nvarchar(MAX),
	@RolesParameter nvarchar(MAX),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1)
AS 

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'M'
		BEGIN
			declare @sqlStr nvarchar (MAX);
			set @sqlStr = 'UPDATE [dbo].[ScreenPermission] set ' + CAST(@RolesColumnName AS NVARCHAR) + ' = ''' + CAST(@RolesParameter AS NVARCHAR(MAX)) + ''',';
			set @sqlStr = @sqlStr + ' [DateModified] = ''' + CAST(@dateNow AS NVARCHAR) + ''',';
			set @sqlStr = @sqlStr + ' [ModifiedBy] = ''' + CAST(@username AS NVARCHAR) + '''';
			set @sqlStr = @sqlStr + ' WHERE [BusinessId] = ' + CAST(@BusinessId AS NVARCHAR);

			exec sp_executesql @sqlStr;
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spRolesRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRolesRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1 * 
	FROM [ScreenPermission]
	WHERE BusinessId = @BusinessId

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spSalesInvoiceAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesInvoiceAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		si.*, 
		cust.CustomerName,
		CONVERT(DECIMAL(18,4), si.Total - si.AmountPaid) AS AmountDue	
	FROM [SalesInvoice] si
	LEFT JOIN [Customer] cust
		ON si.CustomerId = cust.CustomerId
	WHERE	si.BusinessId = @BusinessId
		AND si.IsDisabled <> 1
		AND si.IsCashSales <> 1
	ORDER BY si.SalesInvoiceId DESC
	
SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spSalesInvoiceItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesInvoiceItemAllRetrieve]
	@SalesInvoiceId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT siItem.*, dbo.fnGetItemFullName(ig.ItemGroupCode, ig.ItemGroupName, i.ItemName) AS ItemFullName, i.Price, si.Discount, si.SalesInvoiceId
	FROM [dbo].[SalesInvoiceItem] siItem
		, [dbo].[SalesInvoice] si
		, [dbo].[Item] i
		INNER JOIN [dbo].[ItemGroup] ig
		    ON i.ItemGroupId = ig.ItemGroupId
	WHERE siItem.SalesInvoiceId = @SalesInvoiceId
	    AND siItem.ItemId = i.ItemId
	    AND siItem.SalesInvoiceId = si.SalesInvoiceId
		AND siItem.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;









GO
/****** Object:  StoredProcedure [dbo].[spSalesInvoiceManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesInvoiceManage]
	@SalesInvoiceId bigint,
	@BusinessId bigint,
    @IsCashSales bit,
    @CustomerId bigint,
    @Reference nvarchar(50),
    @ContactPerson nvarchar(100),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @Phone varchar(50),
    @Date date,
    @DateDelivery date,
    @DestinationTypeId tinyint,
	@PriceTypeId tinyint,
	@AmountTypeId tinyint,
	@PaymentTypeId tinyint = 1,
    @Note nvarchar(MAX),
	@SalesPersonId bigint,
    @Subtotal decimal(18, 4),
	@Discount decimal(18, 4),
	@Charge decimal(18, 4),
    @Tax decimal(18, 4),
    @Total decimal(18, 4),
	@AmountPaid decimal(18, 4),
	--@Status varchar(50),
    @UserId bigint,
	@Version datetime,
	@ItemState char(1),
	@Items tSalesInvoiceItem READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @targetSiId BIGINT;
		SET @targetSiId = @SalesInvoiceId;
	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);
	DECLARE @updateItemList tIdTable;

	-- 1. Manage records in [SalesInvoice] table
	IF @ItemState = 'N'
		BEGIN
			-- Get the next Document Number first
			DECLARE @nextSiName NVARCHAR(50);
				SET @nextSiName = '';
			DECLARE @nextCsName NVARCHAR(50);
				SET @nextCsName = '';
			IF @IsCashSales = 1
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'CashSales', @RetDocName = @nextCsName OUT;
				END
			ELSE
				BEGIN
					EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Invoice', @RetDocName = @nextSiName OUT;
				END

			-- Insert Sales Invoice
			INSERT INTO [dbo].[SalesInvoice] 
				([BusinessId],[SalesInvoiceNo],[IsCashSales], [CashSalesNo], [CustomerId], [Reference], [ContactPerson], [Address1], [Address2], [Address3], [Phone], [Date], [DateDelivery], [DestinationTypeId], [PriceTypeId],[AmountTypeId], [PaymentTypeId], [Note], [SalesPersonId], [Subtotal], [Discount], [Charge], [Tax], [Total], [AmountPaid], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextSiName, @IsCashSales, @nextCsName, @CustomerId, @Reference, @ContactPerson, @Address1, @Address2, @Address3, @Phone, @Date, @DateDelivery, @DestinationTypeId, @PriceTypeId, @AmountTypeId, @PaymentTypeId, @Note, @SalesPersonId, @Subtotal, @Discount, @Charge, @Tax, @Total, @AmountPaid, 'New', 0, @dateNow, @dateNow, @username, @username
			SET @targetSiId = SCOPE_IDENTITY();
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			SET @blockingItem = dbo.fnIsSalesInvoiceEditable(@SalesInvoiceId);

			IF @blockingItem <> N''
				BEGIN
					SET @ErrTxt = N'The opearation is blocked by ' + @blockingItem;
					RAISERROR( @ErrTxt, 16, 1 );
				END

			UPDATE [dbo].[SalesInvoice]
			SET    
			[CustomerId] = @CustomerId, [Reference] = @Reference, [ContactPerson] = @ContactPerson, 
			[Address1] = @Address1, [Address2] = @Address2, [Address3] = @Address3, [Phone] = @Phone, [Date] = @Date, [DateDelivery] = @DateDelivery, 
			[DestinationTypeId] = @DestinationTypeId, [PriceTypeId] = @PriceTypeId, [AmountTypeId] = @AmountTypeId, [PaymentTypeId] = @PaymentTypeId,
			[Note] = @Note, [SalesPersonId] = @SalesPersonId, [Subtotal] = @Subtotal, [Discount] = @Discount, [Charge] = @Charge,
			[Tax] = @Tax, [Total] = @Total,
			[DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
			[SalesInvoiceId] = @SalesInvoiceId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[SalesInvoice]
			SET    
				[IsDisabled] = 1, [DateModified] = @dateNow, [UpdatedBy] = @username
			WHERE  
				[SalesInvoiceId] = @SalesInvoiceId
		END

	-- 2. Revert [Item].Quantity for [SalesInvoiceItem] records in case they are modified. We will update with new
	--    SalesInvoiceItem.Quantity below
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + sii.Quantity
	FROM [SalesInvoiceItem] sii
	INNER JOIN @Items i
		ON sii.SalesInvoiceItemId = i.SalesInvoiceItemId
			AND (i.ItemState = 'M' OR i.ItemState = 'D')
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @SalesInvoiceId;


	-- 3. Manage SubItems
	---- Insert Sales Invoice's items
	INSERT INTO [dbo].[SalesInvoiceItem]
	    ([SalesInvoiceId], [ItemId], [Price], [Quantity], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
	SELECT SalesInvoiceId = @targetSiId, ItemId, Price, Quantity, 0, @dateNow, @dateNow, @username, @username
		FROM @Items
		WHERE ItemState = 'N';

	---- Update Sales Invoice's items
	UPDATE siItem
	SET siItem.ItemId = i.ItemId, siItem.Price = i.Price, siItem.Quantity = i.Quantity, 
	    siItem.DateModified = @dateNow, siItem.UpdatedBy = @username
	FROM [dbo].[SalesInvoiceItem] AS siItem
		INNER JOIN @Items AS i
		ON siItem.SalesInvoiceId = i.SalesInvoiceId 
		WHERE i.ItemState = 'M'

	---- Disable Sales Invoice's items
	UPDATE siItem
	SET siItem.IsDisabled = 1, siItem.DateModified = @dateNow, siItem.UpdatedBy = @username
	FROM [dbo].[SalesInvoiceItem] AS siItem
		INNER JOIN @Items AS i
		ON siItem.SalesInvoiceId = i.SalesInvoiceId 
		WHERE i.ItemState = 'D'


	-- 4. Re-calculate the SalesInvoice.Status again in case that the SalesInvoiceItem item is changed
	IF @ItemState = 'M'
		BEGIN
			INSERT INTO @updateItemList
			SELECT si.SalesInvoiceId FROM [SalesInvoice] si WHERE si.SalesInvoiceId = @targetSiId;

			EXEC dbo.spSalesInvoiceStatusUpdate @updateItemList;
		END

	-- 5. In case of IsCaseSales=1, we have to create IncomingPayment items associated to this SalesInvoice as well
	IF @IsCashSales = 1
	BEGIN
		IF @ItemState = 'N'
		BEGIN
		
			DECLARE @nextIpItemName NVARCHAR(50);
			DECLARE @newIpId BIGINT;

			-- Create OutgoingPayment item
			--EXEC [dbo].[spGetNextDocName] @BusinessId = @BusinessId, @Doctype = 'Payment', @RetDocName = @nextIpItemName OUT;

			INSERT INTO [dbo].[IncomingPayment] 
				([BusinessId], [IncomingPaymentNo], [IsCashSales], [CustomerId], [Reference], [PaymentTypeId], [BankId], [BankAccNo], [Address1], [Address2], [Address3], [Phone], [Date], [Vat], [Wht], [WhtPercent], [Note], [Subtotal], [Tax], [Total], [Status], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT @BusinessId, @nextCsName, 1, @CustomerId, @Reference, @PaymentTypeId, 1, N'', @Address1, @Address2, @Address3, @Phone, @Date, 0, 0, 0, @Note, @Subtotal, @Tax, @Total, 'Done', 0, @dateNow, @dateNow, @username, @username
	
			SET @newIpId = SCOPE_IDENTITY();

			INSERT INTO [dbo].[IncomingPaymentItem] 
				([IncomingPaymentId], [SalesInvoiceId], [Amount], [IsDisabled], [DateCreated], [DateModified], [CreatedBy], [UpdatedBy])
			SELECT 
				@newIpId, @targetSiId, @Total, 0, @dateNow, @dateNow, @username, @username;

		END
	END

	-- 6. Update Item.Quantity to reflect SubItems with ItemState = 'N' or ItemState = 'M'
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity - sii.Quantity
	FROM [SalesInvoiceItem] sii
	INNER JOIN @Items i
		ON sii.SalesInvoiceItemId = i.SalesInvoiceItemId
			AND (i.ItemState = 'N' OR i.ItemState = 'M')
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @targetSiId;

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spSalesInvoiceRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesInvoiceRetrieve]
	@SalesInvoiceId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		si.*, 
		cust.CustomerName,
		at.AmountTypeName,
		pt.PaymentTypeName,
		dt.DestinationTypeName,
		stf.Identification /* SalesPersonName */
	FROM [SalesInvoice] si
	LEFT JOIN [Customer] cust
		ON si.CustomerId = cust.CustomerId
	INNER JOIN [AmountType] at
		ON si.AmountTypeId = at.AmountTypeId
	LEFT JOIN [PaymentType] pt
		ON si.PaymentTypeId = pt.PaymentTypeId
	INNER JOIN [DestinationType] dt
		ON si.DestinationTypeId = dt.DestinationTypeId
	INNER JOIN [Staff] stf
		ON si.SalesPersonId = stf.StaffId
	WHERE si.SalesInvoiceId = @SalesInvoiceId


SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spSalesInvoiceStatusUpdate]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesInvoiceStatusUpdate]
	@IdTable tIdTable READONLY
AS 

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	DECLARE @status VARCHAR(10);
		SET @status = 'New';

	UPDATE [SalesInvoice] 
	SET
		AmountPaid = 
			CASE 
				WHEN res1.AmtPaid IS NULL THEN 0
				ELSE res1.AmtPaid
			END,
		Status = 
			CASE
				WHEN res1.AmtPaid IS NULL OR res1.AmtPaid <= 0 THEN 'New'
				WHEN res1.AmtPaid >= si.Total THEN 'Done'
				ELSE 'Partial'
			END
	FROM [SalesInvoiceItem] sii
	INNER JOIN [SalesInvoice] si
		ON sii.SalesInvoiceId = si.SalesInvoiceId
	INNER JOIN @IdTable it
		ON it.Id = sii.SalesInvoiceId
	LEFT JOIN
		(
			SELECT 
				ipi.SalesInvoiceId,
				SUM(ipi.Amount) AS AmtPaid
			FROM [IncomingPaymentItem] ipi
			INNER JOIN [SalesInvoice] si
				ON ipi.SalesInvoiceId = si.SalesInvoiceId
					AND ipi.IsDisabled <> 1
			INNER JOIN [IncomingPayment] ip
				ON ipi.IncomingPaymentId = ip.IncomingPaymentId
					AND ip.IsDisabled <> 1
					AND ip.Status <> 'Void'
			GROUP BY ipi.SalesInvoiceId
		) res1
		ON sii.SalesInvoiceId = res1.SalesInvoiceId;
	
	--SELECT @status = si.Status
	--FROM [SalesInvoice] si
	--WHERE	si.SalesInvoiceId = @SalesInvoiceId
	--	AND si.IsDisabled <> 1;

	--IF (@status IS NOT NULL AND @status <> 'Void') -- Only Update item that is not disabled and is not VOID'ed
	--	BEGIN
	--		DECLARE @amtPaid DECIMAL (18 ,4);
	--			SET @amtPaid = 0;
	--		DECLARE @amtOwed DECIMAL (18, 4);
	--			SET @amtOwed = 0;

	--		-- Get @amtPaid
	--		SELECT 
	--			@amtOwed = si.Total,
	--			@amtPaid = SUM(ipi.Amount)
	--		FROM [SalesInvoice] si
	--		INNER JOIN [IncomingPaymentItem] ipi
	--			ON si.SalesInvoiceId = ipi.SalesInvoiceId
	--				AND ipi.IsDisabled <> 1
	--		INNER JOIN [IncomingPayment] ip
	--			ON ipi.IncomingPaymentId = ip.IncomingPaymentId
	--				AND ip.IsDisabled <> 1
	--				AND ip.Status <> 'Void'
	--		WHERE	si.SalesInvoiceId = @SalesInvoiceId
	--		GROUP BY si.Total;

	--		IF (@amtPaid IS NULL) -- Set Status=New for item that has not been paid at all
	--			BEGIN
	--				SET @status = 'New';
	--				SET @amtPaid = 0;
	--			END
	--		ELSE IF (@amtPaid > 0 AND @amtPaid < @amtOwed) -- Set Status=Partial for item that has been partially paid
	--			BEGIN
	--				SET @status = 'Partial'
	--			END
	--		ELSE -- Set Status=Paid for item that has been paid
	--			BEGIN
	--				SET @status = 'Paid'
	--			END

	--		-- Update the AmounPaid and the Status of the SalesInvoice item
	--		UPDATE [SalesInvoice]
	--		SET AmountPaid = @amtPaid, Status = @status
	--		WHERE SalesInvoiceId = @SalesInvoiceId;
	--	END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;










GO
/****** Object:  StoredProcedure [dbo].[spSalesInvoiceVoid]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesInvoiceVoid]
	@SalesInvoiceId BIGINT,
	@UserId BIGINT
AS

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();

	declare @username varchar(50);
		exec @Username = GetUsername @UserId = @UserId;

	DECLARE @blockingItem NVARCHAR(100);
	DECLARE @ErrTxt NVARCHAR(MAX);

	SET @blockingItem = dbo.fnIsSalesInvoiceEditable(@SalesInvoiceId);
	IF @blockingItem <> N''
		BEGIN
			SET @ErrTxt = N'The operation is blocked by ' + @blockingItem;
			RAISERROR( @ErrTxt, 16, 1 );
		END

	UPDATE [dbo].[SalesInvoice]
	SET Status = 'Void', DateModified = GETDATE(), UpdatedBy = @username
	WHERE SalesInvoiceId = @SalesInvoiceId;

	-- Update Item.Quantity
	UPDATE [Item]
	SET Item.Quantity = Item.Quantity + sii.Quantity
	FROM [SalesInvoiceItem] sii
	WHERE	sii.IsDisabled <> 1
		AND sii.SalesInvoiceId = @SalesInvoiceId;
	
COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;








GO
/****** Object:  StoredProcedure [dbo].[spSalesItemAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesItemAllRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT 
		i.ItemId, i.ItemName, i.Price, i.Barcode, i.ItemGroupId, ig.ItemGroupName, ig.ItemGroupCode, ig.Uom, ig.ImagePath,
		ic.ItemCategoryId, ic.ItemCategoryName, ib.ItemBrandId, ib.ItemBrandName
	FROM [ITEM] i
		INNER JOIN [ItemGroup] ig
			ON i.ItemGroupId = ig.ItemGroupId
		LEFT JOIN [ItemBrand] ib
			ON ig.ItemBrandId = ib.ItemBrandId
		LEFT JOIN [ItemCategory] ic
			ON ig.ItemCategoryId = ic.ItemCategoryId
	WHERE 
			i.IsDisabled <> 1
		AND ig.IsDisabled <> 1
		AND ig.IsForSales = 1
		AND i.IsForSales = 1
		AND ig.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spSalesTaxTypeListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSalesTaxTypeListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select 
		SalesTaxTypeId AS Value, 
		SalesTaxTypeName As Text 
	from 
		SalesTaxType

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spSearchUserByEmail]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSearchUserByEmail]
	@FilterKeyword VarChar(252)
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	DECLARE @Filter VarChar(255);
	
	SET @Filter = '%' + @FilterKeyword + '%';

	SELECT TOP 20 *
	FROM dbo.[User] usr
	WHERE	usr.Email LIKE @Filter
		AND usr.IsDisabled <> 1;

SET @ReturnValue = @@ROWCOUNT;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spSetImagePath]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetImagePath]
    @BusinessId bigint,
	@ItemGroupId bigint,
	@ImagePath nvarchar(MAX),
	@ItemState char(1),
	@UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
		EXEC @username = GetUsername @UserId = @UserId;

	IF @ItemState = 'M'
	BEGIN
		UPDATE [dbo].[ItemGroup]
			SET [ImagePath] = @ImagePath
				,[DateModified] = @dateNow
				,[UpdatedBy] = @username
			WHERE [BusinessId] = @BusinessId
				AND [ItemGroupId] = @ItemGroupId
	END

SET @ReturnValue = 0;
RETURN @ReturnValue;






GO
/****** Object:  StoredProcedure [dbo].[spSourceBusinessListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSourceBusinessListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT TOP 1
		other.BusinessId AS Value,
		b.BusinessName AS Text
	FROM 
	(
		SELECT
			br.BusinessId,
			br.RootBusinessId
		FROM [BusinessRelation] br
		WHERE br.BusinessId = @BusinessId -- retrieve the business, itself
	) other
	INNER JOIN [Business] b
		ON	other.BusinessId = b.BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;







GO
/****** Object:  StoredProcedure [dbo].[spStaffAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStaffAllRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT s.*, u.Username, u.Email FROM [dbo].[Staff] s
	INNER JOIN [dbo].[User] u
		ON s.UserId = u.UserId
	WHERE s.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;




GO
/****** Object:  StoredProcedure [dbo].[spStaffBusinessAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStaffBusinessAllRetrieve]
    @UserId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT Business.BusinessId, Business.BusinessName, Business.DateExpired, Staff.Role
	FROM Staff
	INNER JOIN Business
		ON Staff.BusinessId = Business.BusinessId
	WHERE Staff.UserId = @UserId
		AND Staff.Status = 'active'

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spStaffInfoManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStaffInfoManage]
	@BusinessId bigint,
	@UserId bigint,
	@StaffId bigint,
	@Title varchar(4),
    @Firstname nvarchar(50),
    @Lastname nvarchar(50),
    @Birthdate date,
    @Gender char(1),
	@Identification nvarchar(50),
    @Phone nvarchar(50),
    @Fax nvarchar(50),
	@Email varchar(254),
    @FacebookId nvarchar(50),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @City nvarchar(50),
    @CountryId tinyint,
    @ZipCode nvarchar(50),
    @Role varchar(10),
	@ItemState varchar(1)
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @username VARCHAR(50);
	   EXEC @username = GetUsername @UserId = @UserId;
	
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Staff]
				   ([BusinessId]
				   ,[UserId]
				   ,[Title]
				   ,[Firstname]
				   ,[Lastname]
				   ,[Birthdate]
				   ,[Gender]
				   ,[Identification]
				   ,[Phone]
				   ,[Fax]
				   ,[Email]
				   ,[FacebookId]
				   ,[Address1]
				   ,[Address2]
				   ,[Address3]
				   ,[City]
				   ,[CountryId]
				   ,[ZipCode]
				   ,[Role]
				   ,[Status]
				   ,[Guid]
				   ,[DateGuidExpired]
				   ,[DateCreated]
				   ,[DateModified]
				   ,[CreatedBy]
				   ,[UpdatedBy])
			 VALUES
				   (@BusinessId, @UserId, @Title, @Firstname, @Lastname, @Birthdate, @Gender, @Identification, @Phone, @Fax, @Email
				   ,@FacebookId, @Address1, @Address2, @Address3, @City, @CountryId, @ZipCode, @Role, 'waiting', '', NULL, @dateNow, @dateNow, @username, @username )
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET [Title] = @Title
				  ,[Firstname] = @Firstname
				  ,[Lastname] = @Lastname
				  ,[Birthdate] = @Birthdate
				  ,[Gender] = @Gender
				  ,[Identification] = @Identification
				  ,[Phone] = @Phone
				  ,[Fax] = @Fax
				  ,[FacebookId] = @FacebookId
				  ,[Email] = @Email
				  ,[Address1] = @Address1
				  ,[Address2] = @Address2
				  ,[Address3] = @Address3
				  ,[City] = @City
				  ,[CountryId] = @CountryId
				  ,[ZipCode] = @ZipCode
				  ,[Role] = @Role
				  ,[DateModified] = @dateNow
			 WHERE StaffId = @StaffId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Staff]
			   SET [Status] = 'inactive'
			 WHERE StaffId = @StaffId;
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;





GO
/****** Object:  StoredProcedure [dbo].[spStaffInfoRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStaffInfoRetrieve]
    @StaffId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT TOP 1 s.*, c.CountryName, c.CountryShortName
	FROM [Staff] s
	LEFT JOIN [Country] c
		ON s.CountryId = c.CountryId
	WHERE StaffId = @StaffId;

SET @ReturnValue = 0;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spStaffListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStaffListRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT s.StaffId AS Value, u.Username AS Text FROM [dbo].[Staff] s
	INNER JOIN [dbo].[User] u
		ON s.UserId = u.UserId
	WHERE s.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;




GO
/****** Object:  StoredProcedure [dbo].[spStaffRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStaffRetrieve]
    @UserId bigint,
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	--declare @Role nvarchar(100);

	--SELECT TOP 1 @Role = Staff.Role
	--FROM Staff
	--WHERE Staff.UserId = 1
	--	AND Staff.BusinessId = 1
	--	AND Staff.Status = 'active'

	--declare @sqlStr nvarchar (MAX);
	--set @sqlStr = 'SELECT Staff.*, Business.BusinessName, ScreenPermission.' + CAST(@Role AS NVARCHAR) + ' AS RolePermission';
	--set @sqlStr = @sqlStr + ' FROM Staff';
	--set @sqlStr = @sqlStr + ' INNER JOIN Business';
	--set @sqlStr = @sqlStr + ' ON Staff.BusinessId = Business.BusinessId';
	--set @sqlStr = @sqlStr + ' INNER JOIN ScreenPermission';
	--set @sqlStr = @sqlStr + ' ON Staff.BusinessId = ScreenPermission.BusinessId';
	--set @sqlStr = @sqlStr + ' WHERE Staff.UserId = 1';
	--set @sqlStr = @sqlStr + ' AND Staff.BusinessId = 1';
	--set @sqlStr = @sqlStr + ' AND Staff.Status = ''active''';

	--exec sp_executesql @sqlStr;

	SELECT TOP 1 
		*,
		Role AS RolePermission
	FROM [Staff] stf
	INNER JOIN [Business] b
		ON stf.BusinessId = b.BusinessId
	INNER JOIN [ScreenPermission] sp
		ON stf.BusinessId = sp.BusinessId
	INNER JOIN [System_BusinessRole] sys_role
		ON stf.Role = sys_role.BusinessRoleShortName
	WHERE	stf.UserId = @UserId
		AND stf.BusinessId = @BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;

GO
/****** Object:  StoredProcedure [dbo].[spSupplierAllRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSupplierAllRetrieve]
	@BusinessId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.SupplierId, s.SupplierName, s.SupplierNo, s.ContactPerson, s.Phone, sg.SupplierGroupName
	from Supplier s
	left join SupplierGroup sg
		on s.SupplierGroupId = sg.SupplierGroupId
	where 
			s.BusinessId = @BusinessId
		and	s.IsDisabled = 0

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spSupplierGroupListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSupplierGroupListRetrieve]
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select sg.SupplierGroupId AS Value, sg.SupplierGroupName AS Text
	from SupplierGroup sg

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spSupplierListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSupplierListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT s.SupplierId AS Value, s.SupplierName AS Text
	FROM [dbo].[Supplier] s
	WHERE
		s.IsDisabled <> 1

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
/****** Object:  StoredProcedure [dbo].[spSupplierManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSupplierManage]
	@BusinessId bigint,
	@SupplierId bigint,
	@SupplierName nvarchar(50),
	@SupplierNo nvarchar(50),
	@SupplierGroupId tinyint,
	@TaxNo nvarchar(50),
	@ContactPerson nvarchar(100),
	@Email varchar(320),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@CreditTerm nvarchar(max),
	@Address1 nvarchar(100),
	@Address2 nvarchar(100),
	@Address3 nvarchar(100),
	@City nvarchar(50),
	@CountryId tinyint,
	@ZipCode nvarchar(50),
	@Remark nvarchar(max),
	@UserId bigint,
	@ItemState char(1),
	@Version datetime
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	declare @Username varchar(50);
	
	exec @Username = GetUsername @UserId = @UserId;

	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[Supplier]
			   ([BusinessId]
			   ,[SupplierName]
			   ,[SupplierNo]
			   ,[SupplierGroupId]
			   ,[TaxNo]
			   ,[ContactPerson]
			   ,[Email]
			   ,[Phone]
			   ,[Fax]
			   ,[CreditTerm]
			   ,[Address1]
			   ,[Address2]
			   ,[Address3]
			   ,[City]
			   ,[CountryId]
			   ,[ZipCode]
			   ,[Remark]
			   ,[IsDisabled]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[CreatedBy]
			   ,[UpdatedBy])
			VALUES
				(@BusinessId, @SupplierName, @SupplierNo, @SupplierGroupId, @TaxNo, @ContactPerson, @Email, @Phone, @Fax, @CreditTerm, @Address1, @Address2, @Address3,
				@City, @CountryId, @ZipCode, @Remark, 0, @dateNow, @dateNow, @Username, @Username);
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[Supplier]
			SET   
				SupplierName = @SupplierName, SupplierNo = @SupplierNo, SupplierGroupId = @SupplierGroupId, TaxNo = @TaxNo, ContactPerson = @ContactPerson, 
				Email = @Email, Phone = @Phone, Fax = @Fax, CreditTerm = @CreditTerm, Address1 = @Address1, Address2 = @Address2, Address3 = @Address3, City = @City,
				CountryId = @CountryId, ZipCode = @ZipCode, Remark = @Remark, IsDisabled = 0, DateModified = @dateNow, UpdatedBy = @Username
			WHERE 
				SupplierId = @SupplierId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			UPDATE [dbo].[Supplier]
			SET    
				IsDisabled = 1, DateModified = @dateNow, UpdatedBy = @Username
			WHERE  
				SupplierId = @SupplierId
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;





GO
/****** Object:  StoredProcedure [dbo].[spSupplierRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSupplierRetrieve]
    @SupplierId bigint
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	select s.*, sg.SupplierGroupName, c.CountryName, c.CountryShortName
	from Supplier s
	left join SupplierGroup sg
		on s.SupplierGroupId = sg.SupplierGroupId
	left join [Country] c
		on s.CountryId = c.CountryId
	where 
		s.SupplierId = @SupplierId

SET @ReturnValue = 0;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spTargetBusinessListRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spTargetBusinessListRetrieve]
    @BusinessId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;

	SELECT
		other.BusinessId AS Value,
		b.BusinessName AS Text
	FROM 
	(
		SELECT
			br.BusinessId,
			br.RootBusinessId
		FROM [BusinessRelation] br
		WHERE br.BusinessId <> @BusinessId -- Ignore self
	) other
	INNER JOIN [Business] b
		ON	other.BusinessId = b.BusinessId;

SET @ReturnValue = 0;
RETURN @ReturnValue;








GO
/****** Object:  StoredProcedure [dbo].[spUserCreate]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUserCreate]
	@Username varchar(254),
	@Password varbinary(50),
	@Salt varbinary(50),
	@Email varchar(254),
	@Guid varchar(50),
	@DateGuidExpired datetime,
	@IsDisabled bit,
	@IsFacebookUser bit
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	DECLARE @newUserId BIGINT;
		SET @newUserId = -1;
	
	INSERT INTO [dbo].[User]
			([Username]
			,[Password]
			,[Salt]
			,[Email]
			,[Guid]
			,[DateGuidExpired]
			,[IsDisabled]
			,[IsFacebookUser]
			,[DateCreated]
			,[DateModified])
		VALUES
			(@Username, @Password, @Salt, @Email, @Guid, @DateGuidExpired, 
			@IsDisabled, @IsFacebookUser, @dateNow, @dateNow)
	SET @newUserId = SCOPE_IDENTITY();

	-- Create the user profile
	INSERT INTO [dbo].[UserProfile]
           ([UserId]
		   ,[Title]
           ,[Firstname]
           ,[Lastname]
           ,[Birthdate]
           ,[Gender]
           ,[Identification]
           ,[Phone]
           ,[Fax]
           ,[FacebookId]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[CountryId]
           ,[ZipCode]
           ,[DateCreated]
           ,[DateModified])
     VALUES
           (@newUserId, NULL, '', '', NULL, NULL, '', '', ''
		   ,'', '', '', '', '', NULL, '', @dateNow, @dateNow )

COMMIT TRANSACTION;

RETURN @newUserId;

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;


GO
/****** Object:  StoredProcedure [dbo].[spUserProfileManage]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUserProfileManage]
	@UserId bigint,
	@Title varchar(4),
    @Firstname nvarchar(50),
    @Lastname nvarchar(50),
    @Birthdate date,
    @Gender char(1),
	@Identification nvarchar(50),
    @Phone nvarchar(50),
    @Fax nvarchar(50),
    @FacebookId varchar(50),
    @Address1 nvarchar(100),
    @Address2 nvarchar(100),
    @Address3 nvarchar(100),
    @City nvarchar(50),
    @CountryId tinyint,
    @ZipCode nvarchar(50),
	@ItemState varchar(1)
AS 
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @dateNow DATETIME;
		SET @dateNow = GETDATE();
	
	IF @ItemState = 'N'
		BEGIN
			INSERT INTO [dbo].[UserProfile]
				   ([UserId]
				   ,[Title]
				   ,[Firstname]
				   ,[Lastname]
				   ,[Birthdate]
				   ,[Gender]
				   ,[Identification]
				   ,[Phone]
				   ,[Fax]
				   ,[FacebookId]
				   ,[Address1]
				   ,[Address2]
				   ,[Address3]
				   ,[City]
				   ,[CountryId]
				   ,[ZipCode]
				   ,[DateCreated]
				   ,[DateModified])
			 VALUES
				   (@UserId, @Title, @Firstname, @Lastname, @Birthdate, @Gender, @Identification, @Phone, @Fax
				   ,@FacebookId, @Address1, @Address2, @Address3, @City, @CountryId, @ZipCode, @dateNow, @dateNow )
		END
	ELSE IF @ItemState = 'M'
		BEGIN
			UPDATE [dbo].[UserProfile]
			   SET [Title] = @Title
				  ,[Firstname] = @Firstname
				  ,[Lastname] = @Lastname
				  ,[Birthdate] = @Birthdate
				  ,[Gender] = @Gender
				  ,[Identification] = @Identification
				  ,[Phone] = @Phone
				  ,[Fax] = @Fax
				  ,[FacebookId] = @FacebookId
				  ,[Address1] = @Address1
				  ,[Address2] = @Address2
				  ,[Address3] = @Address3
				  ,[City] = @City
				  ,[CountryId] = @CountryId
				  ,[ZipCode] = @ZipCode
				  ,[DateModified] = @dateNow
			 WHERE UserId = @UserId
		END
	ELSE IF @ItemState = 'D'
		BEGIN
			DELETE FROM [dbo].[UserProfile]
			 WHERE UserId = @UserId
		END

COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	DECLARE @ErrMsg VARCHAR(2048), @ErrServ TINYINT, @ErrState TINYINT;
	SET @ErrMsg = ERROR_MESSAGE();
	SET @ErrServ = ERROR_SEVERITY();
	SET @ErrState = ERROR_STATE();
	RAISERROR( @ErrMsg, @ErrServ, @ErrState );
END CATCH;





GO
/****** Object:  StoredProcedure [dbo].[spUserProfileRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUserProfileRetrieve]
    @UserId BIGINT
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT TOP 1 *, c.CountryName, c.CountryShortName
	FROM [UserProfile] uprofile
	LEFT JOIN [Country] c
		ON uprofile.CountryId = c.CountryId	
	WHERE UserId = @UserId;

SET @ReturnValue = 0;
RETURN @ReturnValue;



GO
/****** Object:  StoredProcedure [dbo].[spUserRetrieve]    Script Date: 1/8/2016 1:02:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUserRetrieve]
    @UserId BIGINT,
	@Username VARCHAR(50),
	@Email VARCHAR(100)
AS
	SET NOCOUNT ON;
	DECLARE @ReturnValue INT;
	-- Check for existing username
	SELECT * 
	FROM [User]
	WHERE
		(@UserId IS NULL OR (UserId = @UserId)) AND
		(@Username IS NULL OR (Username = @Username)) AND
		(@Email IS NULL OR (Email = @Email));

SET @ReturnValue = 0;
RETURN @ReturnValue;











GO
